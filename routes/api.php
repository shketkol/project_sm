<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix'    => 'data-table',
    'as'        => 'dataTable.',
    'middleware' => 'auth:web,admin'
], function () {

    /**
     * Single entry point for all datatables
     */
    Route::get('/{table}', [
        'uses' => 'DataTableController@index',
        'as'   => 'index',
    ]);

    /**
     * Single entry point for all filters
     */
    Route::get('/filters/{filter}', [
        'uses' => 'DataTableController@filters',
        'as'   => 'filters',
    ]);

    /**
     * Single entry point for all table cards
     */
    Route::get('/{table}/cards', [
        'uses' => 'DataTableController@cards',
        'as'   => 'cards',
    ]);


    /**
     * Single entry point for all external cards
     */
    Route::get('/cards/{card}', [
        'uses' => 'ExternalCardController',
        'as'   => 'external-cards',
    ]);

    /**
     * Single entry point for all table cards
     */
    Route::post('/{table}/check-statuses', [
        'uses' => 'DataTableController@checkStatuses',
        'as'   => 'checkStatuses',
    ]);
});
