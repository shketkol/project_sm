const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const glob = require('glob');
const mix = require('laravel-mix');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

const config = {
  npmDir: './node_modules',
  public: './public',
  resources: './resources',
  vueSource: './resources/js/',
};

const wasmFile = path.resolve(
  __dirname,
  'node_modules',
  'mediainfo.js',
  'dist',
  'MediaInfoModule.wasm'
);

/**
 * Words blocked by Ad Blocker
 * @type {string[]}
 */
const blackList = [
  'advertiser',
];

/**
 * Excluded modules
 * @type {string[]}
 */
const excludedProdModules = [
  'mockups',
];

// Add all modules
for (const file of glob.sync(config.vueSource + 'modules/*/app.js')) {
  const segments = file.split(path.sep);
  let name = segments[segments.length - 2];

  if (~blackList.indexOf(name)) {
    name = name.split("").reverse().join("");
  }

  if (mix.inProduction() && ~excludedProdModules.indexOf(name)) {
    continue;
  }

  mix.js(file, config.public + '/js/' + name + '.js').vue();
}

// All vendor files will be stored in a separate bundle.
// List of all libraries.
const vendors = [
  'axios',
  'chart.js',
  'element-ui',
  'es6-promise/auto',
  'evaporate',
  'filesize',
  'form-data',
  'js-sha256',
  'lodash',
  'moment',
  'qs',
  'spark-md5',
  'tiptap',
  'tiptap-extensions',
  'vee-validate',
  'vue',
  'vue-chartjs',
  'vue-router',
  'vue-scrollto',
  'vue-the-mask',
  'vuex',
  'vuex-map-fields',
  '@sentry/browser',
  '@sentry/integrations',
  '@aws-amplify/auth',
  '@aws-amplify/core',
];

mix.extract(vendors, 'public/js/vendor.js')
  .autoload({
    lodash: ['_'],
  });

// Styles.
mix.sass(
  config.resources + '/sass/app.scss',
  config.public + '/css/styles.css'
);

// PDF document styles
mix.sass(
  config.resources + '/sass/pdf/app.scss',
  config.public + '/css/pdf.css'
);

// Charts styles.
mix.sass(
  config.resources + '/sass/charts/fonts.scss',
  config.public + '/css/charts.css'
);

// Fonts
mix.copyDirectory(
  config.resources + '/fonts',
  config.public + '/fonts'
);

// Images
mix.copyDirectory(
  config.resources + '/images',
  config.public + '/images'
);

if (mix.inProduction()) {
  mix.version();
}

mix.webpackConfig({
  devServer: {
    overlay: true,
  },
  resolve: {
    fallback: {
      fs: false
    },
    alias: {
      App: path.resolve(__dirname, 'resources/js'),
      Config: path.resolve(__dirname, 'resources/js/config'),
      Foundation: path.resolve(__dirname, 'resources/js/foundation'),
      Modules: path.resolve(__dirname, 'resources/js/modules'),
      Static: path.resolve(__dirname, 'resources/static'),
    },
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        exclude: /node_modules/,
        loader: 'eslint-loader',
        test: /\.js|\.vue|\.jsx$/,
      },
      {
        test: /\.js$/,
        // exclude: /node_modules/,
        loader: 'babel-loader',
      }
    ],
  },
  plugins: [
    new ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
    new CopyPlugin({
      patterns: [
        { from: wasmFile, to: './js' },
      ],
    }),
  ],
  externals : {
    worldpay: 'Worldpay'
  },
});
