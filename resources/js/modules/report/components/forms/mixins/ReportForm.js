import moment from "moment";

export default {
  /**
   * Props.
   */
  props: {
    /**
     * Report to edit id
     */
    currentId: {
      type: Number,
      default: null,
    },
  },

  /**
   * Computed properties
   */
  computed: {
    /**
     * Picker options for datepicker.
     *
     * @returns {{disabledDate: (default.methods.checkDateIsDisabled|(function(*=): boolean))}}
     */
    pickerOptions () {
      return {
        disabledDate: this.checkDateIsDisabled,
      };
    },

    /**
     * Validation rules.
     */
    validation () {
      return {
        complete_date_range: null,
        ...this.validationRules.report.date,
      };
    },
  },

  /**
   * Created hook
   */
  async created () {
    if (this.currentId) {
      await this.show(this.currentId);
    }
  },

  /**
   * Before destroy hook
   */
  beforeDestroy () {
    this.clear();
    this.$validator.errors.clear(this.scope);
  },

  /**
   * Component methods
   */
  methods: {
    /**
     * Submit form.
     */
    async submit () {
      const validated = await this.validate();

      if (!validated) {
        return;
      }

      await this.$call(
        this.save(),
        { scope: this.scope }
      );

      if (!this.currentId) {
        this.$trackAction('create_report');
      }

      this.clear();
      this.$emit('close');
      this.$emit('created');
    },

    /**
     * Save api call
     */
    save () {
      if (this.currentId) {
        return this.update({
          id: this.currentId,
          data: this.formData,
        });
      }

      return this.store(this.formData);
    },

    /**
     * Returns true if date should be available to select in datepicker.
     * @param date
     * @returns {boolean}
     */
    checkDateIsDisabled (date) {
      const momentDate = moment(date);
      return momentDate.isAfter(moment());
    },
  },
}
