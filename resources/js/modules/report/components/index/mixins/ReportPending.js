export default {
  /**
   * Methods.
   */
  methods: {
    /**
     * Check report is pending.
     */
    isPending (row) {
      return this.$config.report.pending_status_ids.includes(row.status_id) &&
        this.$config.report.downloadable_type_ids.includes(row.type_id);
    },
  }
}
