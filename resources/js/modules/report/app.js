import 'Foundation/bootstrap';
import IndexPage from './pages/TheIndex';
import AdminIndexPage from "Modules/report/pages/admin/TheAdminIndex";
import { getRouter } from 'Foundation/services/routing/factory';
import routes from './router/routes';
import store from './store';
import Vue from 'vue';

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    IndexPage,
    AdminIndexPage,
  },

  /**
   * Vue router.
   */
  router: getRouter({
    mode: 'history',
    routes,
  }),

  /**
   * Store
   */
  store,
}));
