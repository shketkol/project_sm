import axios from 'axios';
import route from 'Foundation/services/routing/route';
import ReportMapper from './mappers/ReportMapper';

export default {
  /**
   * Store schedule report data
   *
   * @returns {AxiosPromise<any>}
   */
  storeSchedule (data) {
    return axios.post(
      route('api.reports.schedule.store'),
      data
    );
  },

  /**
   * Store pending campaigns report data
   *
   * @returns {AxiosPromise<any>}
   */
  storePendingCampaigns (data) {
    return axios.post(
      route('api.reports.pending-campaigns.store'),
      data
    );
  },

  /**
   * Store download report data
   *
   * @returns {AxiosPromise<any>}
   */
  storeDownload (data) {
    return axios.post(
      route('api.reports.download.store'),
      ReportMapper.mapObjectToApi(data)
    );
  },

  /**
   * Store advertisers report data
   *
   * @returns {AxiosPromise<any>}
   */
  storeAdvertisers (data) {
    return axios.post(
      route('api.reports.advertisers.store'),
      ReportMapper.mapObjectToApi(data)
    );
  },

  /**
   * Store missing ads report data.
   *
   * @returns {AxiosPromise<any>}
   */
  storeMissingAds (data) {
    return axios.post(
      route('api.reports.missing-ads.store'),
      ReportMapper.mapObjectToApi(data)
    );
  },

  /**
   * Show schedule report data
   *
   * @returns {AxiosPromise<any>}
   */
  showSchedule (id) {
    return axios.get(
      route('api.reports.schedule.show', {report: id}),
    );
  },

  /**
   * Show download report data
   *
   * @returns {AxiosPromise<any>}
   */
  showDownload (id) {
    return axios.get(
      route('api.reports.download.show', {report: id}),
    );
  },

  /**
   * Show advertisers report data
   *
   * @returns {AxiosPromise<any>}
   */
  showAdvertisers (id) {
    return axios.get(
      route('api.reports.advertisers.show', {report: id}),
    );
  },

  /**
   * Show pending campaigns report data
   *
   * @returns {AxiosPromise<any>}
   */
  showPendingCampaigns (id) {
    return axios.get(
      route('api.reports.pending-campaigns.show', {report: id}),
    );
  },

  /**
   * Show missing ads report data
   *
   * @returns {AxiosPromise<any>}
   */
  showMissingAds (id) {
    return axios.get(
      route('api.reports.missing-ads.show', {report: id}),
    );
  },

  /**
   * Update schedule report data
   *
   * @returns {AxiosPromise<any>}
   */
  updateSchedule (data) {
    return axios.patch(
      route('api.reports.schedule.update', {report: data.id}),
      data.data
    );
  },

  /**
   * Update download report data
   *
   * @returns {AxiosPromise<any>}
   */
  updateDownload (data) {
    return axios.patch(
      route('api.reports.download.update', {report: data.id}),
      ReportMapper.mapObjectToApi(data.data)
    );
  },

  /**
   * Update advertisers report data
   *
   * @returns {AxiosPromise<any>}
   */
  updateAdvertisers (data) {
    return axios.patch(
      route('api.reports.advertisers.update', {report: data.id}),
      ReportMapper.mapObjectToApi(data.data)
    );
  },

  /**
   * Update pending campaigns report data
   *
   * @returns {AxiosPromise<any>}
   */
  updatePendingCampaigns (data) {
    return axios.patch(
      route('api.reports.pending-campaigns.update', {report: data.id}),
      ReportMapper.mapObjectToApi(data.data)
    );
  },

  /**
   * Update missing ads report data
   *
   * @returns {AxiosPromise<any>}
   */
  updateMissingAds (data) {
    return axios.patch(
      route('api.reports.missing-ads.update', {report: data.id}),
      data.data
    );
  },

  /**
   * Pause the report.
   *
   * @param report
   * @returns {AxiosPromise<any>}
   */
  pause (report) {
    return axios.patch(route('api.reports.pause', { report }));
  },

  /**
   * Resume the report.
   *
   * @param report
   * @returns {AxiosPromise<any>}
   */
  resume (report) {
    return axios.patch(route('api.reports.resume', { report }));
  },

  /**
   * Delete the specified resource.
   *
   * @param report
   * @returns {AxiosPromise}
   */
  destroy (report) {
    return axios.delete(route('api.reports.destroy', { report }));
  },
};
