import axios from 'axios';
import route from 'Foundation/services/routing/route';

export default {
  /**
   * Get list of all available delivery frequency options.
   *
   * @returns {AxiosPromise<any>}
   */
  index () {
    return axios.get(
      route('api.reports.deliveryFrequencies.index')
    );
  },
};
