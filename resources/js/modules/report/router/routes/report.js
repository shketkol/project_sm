import ReportTable from "Modules/report/components/index/ReportTable";

export default [
  {
    path: '/reports/:type/:reportId',
    name: 'reports.show',
    component: ReportTable,
  },
  {
    path: '/reports',
    name: 'reports.index',
    component: ReportTable,
  }
];
