import cloneDeep from 'lodash/cloneDeep';
import storeHelper from 'Foundation/helpers/store';
import { getField } from 'vuex-map-fields';
import { report as api } from '../../api';
import translator from 'Foundation/services/translations/locale';
import moment from "moment";

/**
 * Types
 */
const types = {
  STORE_REPORT: storeHelper.createAsyncMutation('STORE_REPORT'),
  UPDATE_REPORT: storeHelper.createAsyncMutation('UPDATE_REPORT'),
  DELETE_REPORT: storeHelper.createAsyncMutation('DELETE_REPORT'),
  SHOW_SCHEDULE_REPORT: storeHelper.createAsyncMutation('SHOW_SCHEDULE_REPORT'),
  SHOW_DOWNLOAD_REPORT: storeHelper.createAsyncMutation('SHOW_DOWNLOAD_REPORT'),
  SHOW_ADVERTISERS_REPORT: storeHelper.createAsyncMutation('SHOW_ADVERTISERS_REPORT'),
  SHOW_PENDING_CAMPAIGNS_REPORT: storeHelper.createAsyncMutation('SHOW_PENDING_CAMPAIGNS_REPORT'),
  SHOW_MISSING_ADS_REPORT: storeHelper.createAsyncMutation('SHOW_MISSING_ADS_REPORT'),
};

/**
 * Default advertiser option
 * @type {{}}
 */
const defaultAdvertiser = {
  label: translator.get('report::labels.all_advertisers'),
  value: null,
};

/**
 * Default pending campaigns option
 * @type {{}}
 */
const defaultPendingCampaign = {
  label: translator.get('report::labels.pending_campaigns'),
  value: null,
};

/**
 * Default schedule report state
 * @type {{}}
 */
const defaultScheduleReport = {
  name: null,
  frequency: null,
  day: null,
  emails: [window.userConfig.email],
  advertiser: defaultAdvertiser,
  pendingCampaign: defaultPendingCampaign
};

/**
 * Default download report state
 * @type {{}}
 */
const defaultDownloadReport = {
  name: null,
  dateRange: {
    start: null,
    end: null,
  },
  advertiser: defaultAdvertiser
};

/**
 * Default missing ads report state.
 * @type {{}}
 */
const defaultMissingAdsReport = {
  isScheduled: false,
  name: null,
  frequency: null,
  day: null,
  emails: [window.userConfig.email],
  dateRange: {
    start: moment(window.config.general.launch_date, 'YYYY-MM-DD').endOf('day').format(),
    end: moment().add(window.config.report.count_days_fom_missing_ads_report, 'days').endOf('day').format()
  }
};

/**
 * Default advertisers report state
 * @type {{}}
 */
const defaultAdvertisersReport = {
  name: null
};

/**
 * Default pending campaign report state
 * @type {{}}
 */
const defaultPendingCampaignReport = {
  name: translator.get('report::labels.pending_campaigns'),
  dateRange: {
    start: moment(window.config.general.launch_date,       'YYYY-MM-DD').endOf('day').format(),
    end: moment().endOf('day').format()
  }
};

/**
 * Set default advertiser for the report if advertiser is null.
 * @param {{}} report
 */
const handleEmptyAdvertiser = (report) => {
  if (!report.advertiser) {
    report.advertiser = defaultAdvertiser;
  }
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.STORE_REPORT,
    types.SHOW_SCHEDULE_REPORT,
    types.SHOW_DOWNLOAD_REPORT,
    types.SHOW_ADVERTISERS_REPORT,
    types.SHOW_PENDING_CAMPAIGNS_REPORT,
    types.SHOW_MISSING_ADS_REPORT,
    types.UPDATE_REPORT,
    types.DELETE_REPORT,
  ]),

  /**
   * Schedule report data.
   */
  scheduleReport: cloneDeep(defaultScheduleReport),

  /**
   * Download report data.
   */
  downloadReport: cloneDeep(defaultDownloadReport),


  /**
   * Advertisers report data.
   */
  advertisersReport: cloneDeep(defaultAdvertisersReport),

  /**
   * Campaigns report data.
   */
  pendingCampaignsReport: cloneDeep(defaultPendingCampaignReport),

  /**
   * Default advertiser select option
   */
  defaultAdvertiser: cloneDeep(defaultAdvertiser),

  /**
   * Default pending campaigns select option
   */
  defaultPendingCampaign: cloneDeep(defaultPendingCampaign),

  /**
   * Missing ads report state.
   */
  missingAdsReport: cloneDeep(defaultMissingAdsReport),
};

const getters = {
  /**
   * @param state
   * @returns {boolean}
   */
  isPendingState (state) {
    return state.storeReportPending ||
      state.updateReportPending ||
      state.showScheduleReportPending ||
      state.showDownloadReportPending ||
      state.showAdvertisersReportPending ||
      state.showPendingCampaignsReportPending ||
      state.showMissingAdsReportPending;
  },

  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Store schedule report
   *
   * @param state, payload
   */
  storeSchedule: storeHelper.createAction(types.STORE_REPORT, {
    call: api.storeSchedule,
  }),

  /**
   * Store download report
   *
   * @param state, payload
   */
  storeDownload: storeHelper.createAction(types.STORE_REPORT, {
    call: api.storeDownload,
  }),

  /**
   * Store advertisers report
   *
   * @param state, payload
   */
  storeAdvertisers: storeHelper.createAction(types.STORE_REPORT, {
    call: api.storeAdvertisers,
  }),

  /**
   * Store pending campaigns report
   *
   * @param state, payload
   */
  storePendingCampaigns: storeHelper.createAction(types.STORE_REPORT, {
    call: api.storePendingCampaigns,
  }),

  /**
   * Store missing ads report.
   *
   * @param state, payload
   */
  storeMissingAds: storeHelper.createAction(types.STORE_REPORT, {
    call: api.storeMissingAds,
  }),

  /**
   * Show schedule report
   *
   * @param state, payload
   */
  showSchedule: storeHelper.createAction(types.SHOW_SCHEDULE_REPORT, {
    call: api.showSchedule,
  }),

  /**
   * Show download report
   *
   * @param state, payload
   */
  showDownload: storeHelper.createAction(types.SHOW_DOWNLOAD_REPORT, {
    call: api.showDownload,
  }),

  /**
   * Show advertisers report
   *
   * @param state, payload
   */
  showAdvertisers: storeHelper.createAction(types.SHOW_ADVERTISERS_REPORT, {
    call: api.showAdvertisers,
  }),

  /**
   * Show pending campaigns report
   *
   * @param state, payload
   */
  showPendingCampaigns: storeHelper.createAction(types.SHOW_PENDING_CAMPAIGNS_REPORT, {
    call: api.showPendingCampaigns,
  }),

  /**
   * Show missing-ads campaigns report
   *
   * @param state, payload
   */
  showMissingAds: storeHelper.createAction(types.SHOW_MISSING_ADS_REPORT, {
    call: api.showMissingAds,
  }),

  /**
   * Update schedule report
   *
   * @param state, payload
   */
  updateSchedule: storeHelper.createAction(types.UPDATE_REPORT, {
    call: api.updateSchedule,
  }),

  /**
   * Update download report
   *
   * @param state, payload
   */
  updateDownload: storeHelper.createAction(types.UPDATE_REPORT, {
    call: api.updateDownload,
  }),

  /**
   * Update advertisers report
   *
   * @param state, payload
   */
  updateAdvertisers: storeHelper.createAction(types.UPDATE_REPORT, {
    call: api.updateAdvertisers,
  }),

  /**
   * Update pending campaigns report
   *
   * @param state, payload
   */
  updatePendingCampaigns: storeHelper.createAction(types.UPDATE_REPORT, {
    call: api.updatePendingCampaigns,
  }),


  /**
   * Update missing ads report
   *
   * @param state, payload
   */
  updateMissingAds: storeHelper.createAction(types.UPDATE_REPORT, {
    call: api.updateMissingAds,
  }),


  /**
   * Delete the report.
   */
  deleteReport: storeHelper.createAction(types.DELETE_REPORT, {
    call: api.destroy,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Store report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.STORE_REPORT),

  /**
   * Update report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.UPDATE_REPORT),

  /**
   * Show download report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SHOW_DOWNLOAD_REPORT, {
    success(state, { data }) {
      state.downloadReport = data;
      handleEmptyAdvertiser(state.downloadReport);
    }
  }),

  /**
   * Show schedule report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SHOW_SCHEDULE_REPORT, {
    success(state, { data }) {
      state.scheduleReport = data;
      handleEmptyAdvertiser(state.scheduleReport);
    }
  }),

  /**
   * Show advertisers report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SHOW_ADVERTISERS_REPORT, {
    success(state, { data }) {
      state.advertisersReport = data;
    }
  }),

  /**
   * Show campaigns report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SHOW_PENDING_CAMPAIGNS_REPORT, {
    success(state, { data }) {
      state.pendingCampaignsReport = data;
    }
  }),

  /**
   * Show missing ads report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SHOW_MISSING_ADS_REPORT, {
    success(state, { data }) {
      state.missingAdsReport = data;
    }
  }),

  /**
   * Clear download report data
   *
   * @param state
   */
  clearDownload(state) {
    state.downloadReport = cloneDeep(defaultDownloadReport);
  },

  /**
   * Clear schedule report data
   *
   * @param state
   */
  clearSchedule(state) {
    state.scheduleReport = cloneDeep(defaultScheduleReport);
  },

  /**
   * Clear advertisers report data
   *
   * @param state
   */
  clearAdvertisers(state) {
    state.advertisersReport = cloneDeep(defaultAdvertisersReport);
  },

  /**
   * Clear pending campaigns report data
   *
   * @param state
   */
  clearPendingCampaigns(state) {
    state.pendingCampaignsReport = cloneDeep(defaultPendingCampaignReport);
  },

  /**
   * Clear missing ads report data
   *
   * @param state
   */
  clearMissingAds(state) {
    state.missingAdsReport = cloneDeep(defaultMissingAdsReport);
  },

  /**
   * Delete the report.
   */
  ...storeHelper.createMutation(types.DELETE_REPORT),
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
  getters,
};


