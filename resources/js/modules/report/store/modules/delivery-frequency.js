import storeHelper from 'Foundation/helpers/store';
import { deliveryFrequency as api } from '../../api';
import {getField} from "vuex-map-fields";

/**
 * Types
 */
const types = {
  INDEX_FREQUENCY: storeHelper.createAsyncMutation('INDEX_FREQUENCY'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Index frequency loading.
   */
  [types.INDEX_FREQUENCY.loadingKey]: null,

  /**
   * Frequency list data.
   */
  frequencyList: [],

  /**
   * Weekly report list data.
   */
  weeklyReportDays: [],

  /**
   * If data is loaded.
   */
  isLoaded: false,
};

const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Get order details
   *
   * @param state, orderId
   */
  list: storeHelper.createAction(types.INDEX_FREQUENCY, {
    call () {
      return api.index();
    },
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  ...storeHelper.createMutation(types.INDEX_FREQUENCY, {
    success(state, { data, reportDays }) {
      state.frequencyList = data;
      state.weeklyReportDays = reportDays;
      state.isLoaded = true;
    },
  }),
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
  getters,
};


