import deliveryFrequency from './modules/delivery-frequency';
import report from './modules/report'
import checkStatuses from 'Foundation/components/dataTable/store/check-statuses'
import notifications from "Modules/notification/store/modules/notifications";
import search from "Modules/advertiser/store/modules/search";
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    deliveryFrequency,
    report,
    notifications,
    search,
    checkStatuses,
  },
});

export default store;
