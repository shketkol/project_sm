import 'Foundation/bootstrap';
import TheAdminBroadcastIndex from "Modules/broadcast/pages/admin/TheAdminBroadcastIndex";
import { getRouter } from 'Foundation/services/routing/factory';
import routes from './router/routes';
import store from './store';
import Vue from 'vue';

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    TheAdminBroadcastIndex,
  },

  /**
   * Vue router.
   */
  router: getRouter({
    mode: 'history',
    routes,
  }),

  /**
   * Store
   */
  store,
}));
