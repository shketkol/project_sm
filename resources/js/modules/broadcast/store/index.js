import Vue from 'vue';
import Vuex from 'vuex';
import notifications from 'Modules/notification/store/modules/notifications';
import broadcast from './modules/broadcast';
import search from 'Modules/advertiser/store/modules/search';
import checkStatuses from 'Foundation/components/dataTable/store/check-statuses';

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    notifications,
    broadcast,
    search,
    checkStatuses
  },
});

export default store;
