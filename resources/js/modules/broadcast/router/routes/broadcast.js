import TheAdminBroadcastIndex from "Modules/broadcast/pages/admin/TheAdminBroadcastIndex";

export default [
  {
    path: '/broadcast/:broadcastId',
    name: 'broadcast.show',
    component: TheAdminBroadcastIndex,
  },
  {
    path: '/broadcast',
    name: 'broadcast.index',
    component: TheAdminBroadcastIndex,
  }
];
