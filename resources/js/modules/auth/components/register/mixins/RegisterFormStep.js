import { TextField } from 'Foundation/components/form';

export default {
  /**
   * Child components.
   */
  components: {
    TextField,
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Placeholders.
     */
    placeholders () {
      return {
        firstName: this.$trans('labels.placeholder_first_name'),
        lastName: this.$trans('labels.placeholder_last_name'),
        email: this.$trans('labels.enter_your_email'),
        inviteCode: this.$trans('labels.enter_your_invite_code'),
        companyName: this.$trans('labels.enter_your_business_name'),
        phone: this.$trans('labels.enter_your_phone'),
        street: this.$trans('labels.enter_your_address'),
        city: this.$trans('labels.enter_your_city'),
        state: this.$trans('labels.enter_your_state'),
        zip: this.$trans('labels.enter_5_digit_zip'),
      };
    },

    /**
     * Validation.
     */
    validation () {
      return this.validationRules;
    },
  },

  /**
   * Created hook.
   */
  created () {
    this.$nextTick(this.focus);
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Focus element.
     */
    focus () {
      const ref = this.focused ? this.$refs[this.focused] : null;

      if (ref) {
        ref.focus();
      }
    },

    /**
     * Emit submit event
     */
    submit () {
      this.$emit('enter');
    },
  },
};
