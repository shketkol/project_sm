import createPassword from './modules/create-password';
import forgotPassword from './modules/forgot-password';
import login from './modules/login';
import register from './modules/register';
import notifications from 'Modules/notification/store/modules/notifications';
import location from 'Modules/user/store/modules/location';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    createPassword,
    forgotPassword,
    login,
    register,
    notifications,
    location
  },
});

export default store;
