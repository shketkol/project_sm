import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import Auth from '@aws-amplify/auth';
import Log from "Foundation/services/log/log";
import {Severity} from "@sentry/types/dist/severity";

/**
 * Types
 */
const types = {
  FORGOT_PASSWORD: storeHelper.createAsyncMutation('FORGOT_PASSWORD'),
  RESET_PASSWORD: storeHelper.createAsyncMutation('RESET_PASSWORD'),
};

const log = new Log();

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.FORGOT_PASSWORD,
    types.RESET_PASSWORD,
  ]),

  /**
   * Forgot form.
   */
  forgotForm: {
    email: null,
  },

  /**
   * Reset form.
   */
  resetForm: {
    confirmationCode: null,
    password: null,
    passwordConfirmation: null,
  },
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Send email to API.
   *
   * @param commit
   * @param state
   * @returns {Promise<*>}
   */
  async forgotPassword ({ commit, state }) {
    commit(types.FORGOT_PASSWORD.PENDING);
    try {
      const response = await Auth.forgotPassword(state.forgotForm.email);
      return Promise.resolve(response);
    } catch (error) {
      log.set('Fail cognito forgot password', error, Severity.Info);

      commit(types.FORGOT_PASSWORD.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Send email to API.
   *
   * @param commit
   * @param state
   * @returns {Promise<*>}
   */
  async resetPassword ({ commit, state }) {
    commit(types.RESET_PASSWORD.PENDING);
    try {
      const response = await Auth.forgotPasswordSubmit(
        state.forgotForm.email,
        state.resetForm.confirmationCode,
        state.resetForm.password
      );
      return Promise.resolve(response);
    } catch (error) {
      if (error.code !== 'CodeMismatchException') {
        log.set('Cognito reset password failed', error);
      }
      commit(types.RESET_PASSWORD.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Forgot password mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.FORGOT_PASSWORD),

  /**
   * Reset password mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.RESET_PASSWORD),

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


