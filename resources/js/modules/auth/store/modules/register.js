import cloneDeep from 'lodash/cloneDeep';
import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { register as api } from '../../api';

const user = {
  firstName: null,
  lastName: null,
  email: null,
  inviteCode: null,
  companyName: null,
  phone: null,
  address: {
    streetLine1: null,
    streetLine2: null,
    city: null,
    state: null,
    zip: null,
  },
  recaptcha: null,
};

/**
 * Types
 */
const types = {
  STORE_USER: storeHelper.createAsyncMutation('STORE_USER'),
  RESET: 'RESET',
  VALIDATE_INVITE_CODE: storeHelper.createAsyncMutation('VALIDATE_INVITE_CODE'),
};

/**
 * State
 * @type {{storeUserPending: boolean, success: boolean}}
 */
const state = {
  /**
   * Store user status
   */
  success: false,

  /**
   * Store user processing
   */
  storeUserPending: false,

  /**
   * User name.
   */
  user: cloneDeep(user),

  /**
   * Is invite code validation failed
   */
  inviteCodeError: false
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * User.
   *
   * @param state
   * @returns {state.user|{name, email, companyName, phone, address}|getters.user|(function(*))|*|boolean}
   */
  user (state) {
    return state.user;
  },
};

/**
 * Actions
 */

const actions = {
  /**
   * Store user
   * @param commit
   * @param serviceTypeId
   * @returns {Promise<void>}
   */
  async storeUser ({ commit, state }) {
    commit(types.STORE_USER.PENDING);

    try {
      const response = await api.store(state.user);
      commit(types.STORE_USER.SUCCESS);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.STORE_USER.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Reset the state.
   *
   * @param commit
   */
  reset ({ commit }) {
    commit(types.RESET);
  },

  /**
   * Validate invite code
   *
   * @param state, payload
   */
  validateInviteCode: storeHelper.createAction(types.VALIDATE_INVITE_CODE, {
    call: api.validateInviteCode
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Store user success
   * @param state
   */
  [types.STORE_USER.SUCCESS]: (state) => {
    state[types.STORE_USER.loadingKey] = false;
    state.success = true;
  },

  /**
   * Store user pending
   * @param state
   */
  [types.STORE_USER.PENDING] (state) {
    state[types.STORE_USER.loadingKey] = true;
    state.success = false;
  },

  /**
   * Store user items failure
   * @param state
   */
  [types.STORE_USER.FAILURE] (state) {
    state[types.STORE_USER.loadingKey] = false;
    state.success = false;
  },

  /**
   * Reset the state.
   * @param state
   */
  [types.RESET] (state) {
    state.user = cloneDeep(user);
    state.success = false;
  },

  /**
   * Store invite code mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.VALIDATE_INVITE_CODE, {
    success(state, payload) {
      state.user.email = payload.email;
    },
    failure(state) {
      state.inviteCodeError = true;
    },
  }),

  /**
   * Set invite code error
   */
  setInviteCodeError () {
    state.inviteCodeError = true;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};


