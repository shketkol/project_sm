import RegisterAddress from '../../components/register/RegisterAddress';
import RegisterCompanyName from '../../components/register/RegisterCompanyName';
import RegisterEmail from '../../components/register/RegisterEmail';
import RegisterPhone from '../../components/register/RegisterPhone';
import RegisterSuccess from '../../components/register/RegisterSuccess';
import { isDemo } from 'Modules/demo/helpers/demo';

export default [
  {
    path: '/signup',
    name: 'register',
    component: () => isDemo() ?
      import('../../../demo/modules/auth/components/register/RegisterName') :
      import('../../components/register/RegisterName'),
  },
  {
    path: '/signup/email',
    name: 'register.email',
    component: RegisterEmail,
  },
  {
    path: '/signup/company-name',
    name: 'register.companyName',
    component: RegisterCompanyName,
  },
  {
    path: '/signup/phone',
    name: 'register.phone',
    component: RegisterPhone,
  },
  {
    path: '/signup/address',
    name: 'register.address',
    component: RegisterAddress,
  },
  {
    path: '/signup/success',
    name: 'register.success',
    component: RegisterSuccess,
  }
]
