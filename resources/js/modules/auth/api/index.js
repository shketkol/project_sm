import axios from 'axios';
import RegisterMapper from './mappers/RegisterMapper';
import LoginMapper from './mappers/LoginMapper';
import Router from '../../../foundation/services/routing/router';
import routes from 'Static/routes.json';
import location from 'Modules/user/api/location';

const router = new Router(routes);

const register = {
  /**
   * Store advertiser
   * @param data
   * @returns {AxiosPromise}
   */
  store (data) {
    return axios.post(
      router.route('api.auth.register'),
      RegisterMapper.mapObjectToApi(data)
    );
  },

  /**
   * Validate invite code
   * @param data
   * @returns {AxiosPromise}
   */
  validateInviteCode (data) {
    return axios.post(
      router.route('api.invitations.validateCode'),
      data
    );
  },
};

const login = {
  /**
   * Login to the system.
   *
   * @param data
   * @returns {AxiosPromise<any>}
   */
  login (data) {
    return axios.post(
      router.route('api.auth.login'),
      LoginMapper.mapObjectToApi(data)
    );
  },

  /**
   * First login to the system.
   *
   * @param data
   * @returns {AxiosPromise<any>}
   */
  firstLogin (data) {
    return axios.post(
      router.route('api.auth.first-login'),
      LoginMapper.mapObjectToApi(data)
    );
  },
};

export {
  register,
  login,
  location
}
