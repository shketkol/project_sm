export default class RegisterMapper {
  /**
   * Returns object that should be sent to API.
   *
   * @param user
   * @returns {{}}
   */
  static mapObjectToApi (user) {
    return {
      'email': user.email,
      'invite_code': user.inviteCode,
      'first_name': user.firstName,
      'last_name': user.lastName,
      'phone_number': user.phone,
      'company_name': user.companyName,
      'company_address': {
        'line1': user.address.streetLine1,
        'line2': user.address.streetLine2,
        'city': user.address.city,
        'state': user.address.state,
        'zipcode': user.address.zip,
        'country': 'US',
      },
      'recaptcha': user.recaptcha,
    };
  }
}
