import 'Foundation/bootstrap';
import 'Foundation/services/amplify'
import InvalidResetPasswordTokenPage from './pages/TheInvalidResetPasswordToken';
import RegisterFormPage from './pages/TheRegisterForm';
import LockPage from './pages/TheBrowserLock';
import MaintenancePage from './pages/TheMaintenance';
import LoginFailedPage from './pages/TheLoginFailed';
import { getRouter } from 'Foundation/services/routing/factory';
import routes from './router/routes';
import store from './store';
import Vue from 'vue';
import AdminLoginRestrictedPage from "Modules/auth/pages/TheAdminLoginRestricted";
import { isDemo } from 'Modules/demo/helpers/demo';

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    'ForgotPasswordPage': () => isDemo() ?
      import('../demo/modules/auth/pages/TheForgotPassword') :
      import('./pages/TheForgotPassword'),
    'LoginFormPage': () => isDemo() ?
      import('../demo/modules/auth/pages/TheLoginForm') :
      import('./pages/TheLoginForm'),
    InvalidResetPasswordTokenPage,
    RegisterFormPage,
    LockPage,
    LoginFailedPage,
    AdminLoginRestrictedPage,
    MaintenancePage
  },

  /**
   * Vue router.
   */
  router: getRouter({
    mode: 'history',
    routes,
  }),

  /**
   * Store
   */
  store,
}));
