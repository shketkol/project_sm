import each from 'lodash/each';
import findIndex from 'lodash/findIndex';
import map from 'lodash/map';
import find from 'lodash/findKey';
import findKey from 'lodash/findKey';
import values from 'lodash/values';
import translator from 'Foundation/services/translations/locale';
import { getField, updateField } from 'vuex-map-fields';
import validator from 'Modules/campaign/store/helpers/validator';
import storeHelper from 'Foundation/helpers/store';
import {campaign as api} from 'Modules/campaign/api';

/**
 * Types
 */
const types = {
  UPDATE_STEP: 'UPDATE_STEP',
  UPDATE_FILLED: 'UPDATE_FILLED',
  SET_ACTIVITY_STEP: storeHelper.createAsyncMutation('SET_ACTIVITY_STEP'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  groups: {
    details: {
      title: translator.get('campaign::labels.setup'),
      items: {
        name: {
          route: {
            name: 'campaigns.create.details.name',
            params: {},
          },
          title: translator.get('labels.name'),
          validator: validator.validateName,
          done: false,
          saved: false,
          filled: false,
        },
        schedule: {
          route: {
            name: 'campaigns.create.details.schedule',
            params: {},
          },
          title: translator.get('labels.dates'),
          validator: validator.validateDateRange,
          done: false,
          saved: false,
          filled: false
        },
      },
    },
    budget: {
      title: translator.get('labels.budget'),
      items: {
        budget: {
          route: {
            name: 'campaigns.create.budget',
            params: {},
          },
          title: translator.get('labels.budget'),
          validator: validator.validateBudget,
          done: false,
          saved: false,
          filled: false,
        },
      },
    },
    targeting: {
      title: translator.get('labels.targeting'),
      items: {
        locations: {
          route: {
            name: 'campaigns.create.targeting.locations',
            params: {},
          },
          title: translator.get('labels.locations'),
          validator: validator.validateLocations,
          done: false,
          saved: false,
          filled: false,
        },
        ages: {
          route: {
            name: 'campaigns.create.targeting.ages',
            params: {},
          },
          title: translator.get('labels.gender_and_age'),
          validator: validator.validateAgeGroups,
          done: false,
          saved: false,
          filled: false,
        },
        audiences: {
          route: {
            name: 'campaigns.create.targeting.audiences',
            params: {},
          },
          title: translator.get('labels.audiences'),
          validator: validator.validateAudiences,
          done: false,
          saved: false,
          filled: false,
        },
        devices: {
          route: {
            name: 'campaigns.create.targeting.devices',
            params: {},
          },
          title: translator.get('labels.platforms'),
          validator: validator.validateDevices,
          done: false,
          saved: false,
          filled: false,
        },
        genres: {
          route: {
            name: 'campaigns.create.targeting.genres',
            params: {},
          },
          title: translator.get('labels.show_genres'),
          validator: validator.validateGenres,
          done: false,
          saved: false,
          filled: false,
        },
      },
    },
    creative: {
      title: translator.get('campaign::labels.creative_step'),
      items: {
        creative: {
          route: {
            name: 'campaigns.create.creative',
            params: {},
          },
          title: translator.get('labels.creative'),
          done: false,
          saved: false,
          validator: validator.validateCreative,
          filled: false,
        },
      },
    },
    reviewAndPayment: {
      title: translator.get('labels.review'),
      items: {
        review: {
          route: {
            name: 'campaigns.create.reviewAndPayment.review',
            params: {},
          },
          title: translator.get('labels.review'),
          done: false,
          savable: false,
          hideSidebar: true,
        },
        payment: {
          route: {
            name: 'campaigns.create.reviewAndPayment.payment',
            params: {},
          },
          title: translator.get('labels.payment'),
          done: false,
          savable: false,
          hideSidebar: true,
        },
      },
    },
  },
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Get all item routes.
   *
   * @param state
   * @returns {Array}
   */
  routes (state, getters) {
    return map(getters.steps, 'route');
  },

  /**
   * Get all step groups.
   *
   * @param state
   * @returns {*}
   */
  groups (state) {
    return state.groups;
  },

  /**
   * Get all steps.
   *
   * @param state
   * @returns {Array}
   */
  steps (state) {
    const steps = [];

    each(state.groups, groups => {
      Array.prototype.push.apply(steps, values(groups.items));
    });

    return steps;
  },

  /**
   * Get step from state.
   *
   * @param state
   * @returns {function(*): *}
   */
  getStep (state) {
    return (name) => {
      const keys = name.split('.');

      return state.groups[keys[0]].items[keys[1]];
    };
  },

  /**
   * Get step by its route name.
   * @param state
   * @returns {function(*=): *}
   */
  getStepByRoute (state) {
    return (name) => {
      let step = null;

      each(state.groups, groups => {
        each(groups.items, item => {
          if (item.route.name === name) {
            step = item;
          }
        });
      });

      return step;
    };
  },

  /**
   * Get index of group by route of item.
   * @param state
   * @returns {function(*=): *}
   */
  getGroupIndexByRoute (state) {
    return (name) => {
      return findIndex(values(state.groups), group => {
        return findIndex(values(group.items), item => {
          return item.route.name === name;
        }) !== -1;
      });
    };
  },

  /**
   * Get group name by the route.
   * @param state
   * @returns {function(*=): string}
   */
  getGroupKeyByRoute (state) {
    return (route) => {
      return findKey(state.groups, group =>
        find(group.items, item =>
          item.route.name === route
        ) !== undefined
      );
    };
  },

  /**
   * Check if the sidebar should be shown for specific route.
   * @param state
   * @param getters
   * @returns {Function}
   */
  stepHasSidebar (state, getters) {
    /**
     * @param routeName
     * @returns {boolean}
     */
    return (routeName) => {
      const step = getters.getStepByRoute(routeName);
      return step && !step.hideSidebar;
    };
  },
};

/**
 * Actions.
 *
 * @type {{}}
 */
const actions = {
  /**
   * Update step.
   *
   * @param commit
   * @param payload
   */
  updateStep ({ commit }, payload) {
    commit(types.UPDATE_STEP, payload);
  },

  /**
   * Set filled state of the step by it's key.
   * @param commit
   * @param payload
   */
  setFilled ({ commit }, payload) {
    commit(types.UPDATE_FILLED, {
      path: payload.path,
      filled: payload.filled,
    });
  },

  /**
   * Update filled state of the step by validating it's value.
   * @param commit
   * @param state
   * @param payload
   * @returns {Promise<void>}
   */
  async updateFilled ({ commit, state }, payload) {
    const keys = payload.path.split('.');

    const item = state.groups[keys[0]].items[keys[1]];
    if (item.validator !== undefined) {
      let valid = false;
      const result = item.validator(payload.value);

      if (result instanceof Promise) {
        ({ valid } = await result);
      } else if (typeof result === 'boolean') {
        valid = result;
      }

      commit(types.UPDATE_FILLED, {
        path: payload.path,
        filled: valid,
      });
    }
  },

  /**
   * Send campaign activity step for analytic.
   */
  sendActivity: storeHelper.createAction(types.SET_ACTIVITY_STEP, {
    call: api.sendActivity,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Fields mapping.
   */
  updateField,

  /**
   * Update steps filled state mutation.
   * @param state
   * @param payload
   */
  [types.UPDATE_FILLED] (state, payload) {
    const keys = payload.path.split('.');
    if (state.groups[keys[0]] !== undefined) {
      state.groups[keys[0]].items[keys[1]].filled = payload.filled;
    }
  },

  /**
   * Update step value.
   *
   * @param state
   * @param payload
   */
  [types.UPDATE_STEP] (state, payload) {
    const keys = payload.path.split('.');
    if (state.groups[keys[0]] !== undefined) {
      state.groups[keys[0]].items[keys[1]][keys[2]] = payload.value;
    }
  },

  /**
   * Fill the state.
   *
   * @param state
   * @param data
   */
  fill (state, data) {
    const completed = data.step.split('.');
    let fill = true;

    each(state.groups, (groups, groupName) => {
      each(groups.items, (item, itemName) => {
        item.route.name = item.route.name.replace('create', 'edit');
        item.route.params = { campaignId: data.id };

        if (fill) {
          item.done = true;
          item.saved = true;
        }

        if (groupName === completed[0] && itemName === completed[1]) {
          fill = false;
        }
      });
    });
  },

  /**
   * Set campaign activity step for analytic
   */
  ...storeHelper.createMutation(types.SET_ACTIVITY_STEP),
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};


