import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { campaign as api } from '../../../api';
import flatMap from 'lodash/flatMap';

/**
 * Types
 */
const types = {
  VALIDATE_CAMPAIGN: storeHelper.createAsyncMutation('VALIDATE_CAMPAIGN'),
  CLEAR_ERRORS: 'CLEAR_ERRORS',
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Validate campaign loading.
   */
  [types.VALIDATE_CAMPAIGN.loadingKey]: null,

  /**
   * Validation errors.
   */
  errors: null,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Get error message.
   *
   * @param state
   * @returns {array}
   */
  errors (state) {
    return flatMap(state.errors, errors => errors);
  },
};

/**
 * Actions
 */
const actions = {
  /**
   * Validate campaign on backend.
   *
   * @param commit
   * @param rootState
   * @returns {Promise<*|undefined>}
   */
  async validateCampaign({ commit, rootState }) {
    commit(types.VALIDATE_CAMPAIGN.PENDING);

    try {
      const response = await api.validate(rootState.wizard.id);
      commit(
        types.VALIDATE_CAMPAIGN.SUCCESS,
        response.data
      );
      return Promise.resolve(response);
    } catch (error) {
      commit(types.VALIDATE_CAMPAIGN.FAILURE, error && error.response);
      const data = error.response.data;
      if (data.is_action_validation !== undefined && data.is_action_validation) {
        commit('wizard/targeting/fillTargetingErrors', data, { root: true });
      }
      return Promise.reject(error);
    }
  },

  /**
   * Clear all errors.
   *
   * @param commit
   */
  clearErrors ({ commit }) {
    commit(types.CLEAR_ERRORS);
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Success.
   *
   * @param state
   */
  [types.VALIDATE_CAMPAIGN.SUCCESS] (state) {
    state[types.VALIDATE_CAMPAIGN.loadingKey] = false;
  },

  /**
   * Pending.
   *
   * @param state
   */
  [types.VALIDATE_CAMPAIGN.PENDING] (state) {
    state[types.VALIDATE_CAMPAIGN.loadingKey] = true;
  },

  /**
   * Failure.
   *
   * @param state
   * @param commit
   * @param data
   */
  [types.VALIDATE_CAMPAIGN.FAILURE] (state, { data }) {
    state[types.VALIDATE_CAMPAIGN.loadingKey] = false;
    state.errors = data.errors;
  },

  /**
   * Clear all errors.
   *
   * @param state
   */
  [types.CLEAR_ERRORS] (state) {
    state.errors = null;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


