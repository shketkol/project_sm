import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { campaign as api } from '../../../api';
import CampaignMapper from '../../../api/mappers/CampaignMapper';

/**
 * Types
 */
const types = {
  UPDATE_SCHEDULE: storeHelper.createAsyncMutation('UPDATE_SCHEDULE'),
  UPDATE_NAME: storeHelper.createAsyncMutation('UPDATE_NAME'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Update campaign schedule loading.
   */
  [types.UPDATE_SCHEDULE.loadingKey]: null,

  /**
   * Update campaign name loading.
   */
  [types.UPDATE_NAME.loadingKey]: null,

  /**
   * Campaign name.
   */
  name: null,

  /**
   * Campaign status.
   */
  status: null,

  /**
   * Schedule.
   */
  date: {
    start: null,
    end: null,
  },
  /**
   * Timezone id.
   */
  timezone_id: null,

  /**
   * Timezone
   */
  timezone: null,

  /**
   * Campaign's response message.
   */
  message: null,

  /**
   * Campaign's step.
   */
  step: null,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Load account info from API.
   *
   * @param commit
   * @param rootState
   * @param state
   * @returns {Promise<*|undefined>}
   */
  async updateName({ commit, rootState, state }) {
    commit(types.UPDATE_NAME.PENDING);

    try {
      const response = await api.name.update(rootState.wizard.id, CampaignMapper.mapObjectToApi({
        name: state.name,
      }));
      commit(types.UPDATE_NAME.SUCCESS);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_NAME.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Update schedule.
   *
   * @param commit
   * @param rootState
   * @param state
   * @returns {Promise<*|undefined>}
   */
  async updateSchedule({ commit, rootState, state }) {
    commit(types.UPDATE_SCHEDULE.PENDING);

    try {
      const { date, timezone_id } = state;
      const response = await api.schedule.update(rootState.wizard.id, CampaignMapper.mapObjectToApi({
        date,
        timezone_id
      }));
      commit(types.UPDATE_SCHEDULE.SUCCESS, response);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_SCHEDULE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Campaign name is successfully loaded.
   *
   * @param state
   */
  [types.UPDATE_NAME.SUCCESS] (state) {
    state[types.UPDATE_NAME.loadingKey] = false;
  },

  /**
   * Campaign name is loading.
   *
   * @param state
   */
  [types.UPDATE_NAME.PENDING](state) {
    state[types.UPDATE_NAME.loadingKey] = true;
  },

  /**
   * Campaign name was not loaded.
   *
   * @param state
   */
  [types.UPDATE_NAME.FAILURE](state) {
    state[types.UPDATE_NAME.loadingKey] = false;
  },

  /**
   * Campaign date_range information was successfully stored.
   *
   * @param state
   * @param timezone
   */
  [types.UPDATE_SCHEDULE.SUCCESS](state, { data: { data: { timezone } } }) {
    state[types.UPDATE_SCHEDULE.loadingKey] = false;
    state.timezone = timezone;
  },

  /**
   * Campaign date_range information is storing.
   *
   * @param state
   */
  [types.UPDATE_SCHEDULE.PENDING](state) {
    state[types.UPDATE_SCHEDULE.loadingKey] = true;
  },

  /**
   * Campaign date_range information was not stored.
   *
   * @param state
   */
  [types.UPDATE_SCHEDULE.FAILURE](state) {
    state[types.UPDATE_SCHEDULE.loadingKey] = false;
  },

  /**
   * Fill the state.
   * @param state
   * @param status
   * @param name
   * @param date
   * @param timezone_id
   * @param timezone
   * @param message
   * @param step
   */
  fill (state, { status, name, date, timezone_id, timezone, message, step }) {
    state.status = status;
    state.name = name;
    state.date = date;
    state.timezone_id = timezone_id;
    state.timezone = timezone;
    state.message = message;
    state.step = step;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


