import { getField, updateField } from 'vuex-map-fields';

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Targeting errors.
   */
  targetingError: {},
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Get targeting action-error options.
   *
   * @param state
   * @returns {array}
   */
  targetingError (state) {
    return state.targetingError;
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Fill the targeting error state.
   *
   * @param state
   * @param data
   */
  fillTargetingErrors (state, data) {
    state.targetingError = data;
  },

  /**
   * Clear all targeting errors.
   *
   * @param state
   */
  clearTargetingErrors (state) {
    state.targetingError = {};
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
};


