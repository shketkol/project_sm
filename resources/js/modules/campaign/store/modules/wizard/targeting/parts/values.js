import storeHelper from 'Foundation/helpers/store';
import { filter } from 'lodash';
import { getField, updateField } from 'vuex-map-fields';
import { campaign as api } from '../../../../../api';
import TargetingMapper from "Modules/campaign/api/mappers/TargetingMapper";
import translator from 'Foundation/services/translations/locale';
import Locations from "Modules/campaign/api/helpers/Locations";

/**
 * Helper function. It is used to divide included and excluded targeting.
 * @param items
 * @param included
 * @returns {Array}
 */
const filterIncluded = (items, included) => {
  if (items === null) {
    return [];
  }
  return filter(items, item => included ? item.included : !item.included);
};

/**
 * Types
 */
const types = {
  UPDATE_AGES: storeHelper.createAsyncMutation('UPDATE_AGES'),
  UPDATE_GROUPS: storeHelper.createAsyncMutation('UPDATE_GROUPS'),
  UPDATE_PLATFORMS: storeHelper.createAsyncMutation('UPDATE_PLATFORMS'),
  UPDATE_LOCATIONS: storeHelper.createAsyncMutation('UPDATE_LOCATIONS'),
  UPDATE_GENRES: storeHelper.createAsyncMutation('UPDATE_GENRES'),
  UPDATE_DEVICES: storeHelper.createAsyncMutation('UPDATE_DEVICES'),
  UPDATE_AUDIENCES: storeHelper.createAsyncMutation('UPDATE_AUDIENCES'),
  SEARCH_ZIP: storeHelper.createAsyncMutation('LOAD_ZIP_OPTIONS'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.UPDATE_AGES,
    types.UPDATE_GROUPS,
    types.UPDATE_PLATFORMS,
    types.UPDATE_LOCATIONS,
    types.UPDATE_GENRES,
    types.UPDATE_DEVICES,
    types.UPDATE_AUDIENCES,
    types.SEARCH_ZIP,
  ]),

  /**
   * Campaign.
   */
  targeting: {
    gender: {
      value: null,
      label: null,
    },
    ages: [],
    locations: {
      zip: null,
      city: null,
      fail: []
    },
    genres: null,
    deviceGroups: null,
    audiences: null,
  },

  forceQuitZipSearch: false,

  minCountItems: 0,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Get selected gender label (translated).
   * @param state
   * @returns {string}
   */
  selectedGenderLabel (state) {
    const gender = state.targeting.gender;
    return gender.label ? translator.get(`targeting::labels.genders.${gender.label}`) : '-';
  },

  /**
   * Get selected gender value.
   * @param state
   * @returns {Number|Null}
   */
  selectedGenderId (state) {
    return state.targeting.gender.value;
  },

  /**
   * Get all selected ages.
   * @param state
   * @returns {Array}
   */
  ages (state) {
    return state.targeting.ages;
  },

  /**
   * Get all selected locations.
   * @param state
   * @returns {Array}
   */
  locations (state) {
    return state.targeting.locations;
  },

  /**
   * Get fail zip codes.
   * @param state
   * @returns {Array}
   */
  failZipCodes (state) {
    return state.targeting.locations.fail;
  },

  /**
   * Return only included locations.
   * @param state
   * @returns {Array}
   */
  includedLocations (state) {
    return filterIncluded(state.targeting.locations.city, true);
  },

  /**
   * Return only excluded locations.
   * @param state
   * @returns {Array}
   */
  excludedLocations (state) {
    return filterIncluded(state.targeting.locations.city, false);
  },

  /**
   * Return only included zip codes.
   * @param state
   * @returns {Array}
   */
  includedZipCodes (state) {
    return filterIncluded(state.targeting.locations.zip, true);
  },

  /**
   * Return only excluded zip codes.
   * @param state
   * @returns {Array}
   */
  excludedZipCodes (state) {
    return filterIncluded(state.targeting.locations.zip, false);
  },

  /**
   * Get all selected genres.
   * @param state
   * @returns {Array}
   */
  genres (state) {
    return state.targeting.genres;
  },

  /**
   * Get all selected device groups.
   * @param state
   * @returns {Array}
   */
  deviceGroups (state) {
    return state.targeting.deviceGroups;
  },

  /**
   * Return only included genres.
   * @param state
   * @returns {Array}
   */
  includedGenres (state) {
    return filterIncluded(state.targeting.genres, true);
  },

  /**
   * Return only excluded genres.
   * @param state
   * @returns {Array}
   */
  excludedGenres (state) {
    return filterIncluded(state.targeting.genres, false);
  },

  /**
   * Get all selected audiences.
   * @param state
   * @returns {(Array|null)}
   */
  audiences (state) {
    return state.targeting.audiences;
  },

  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Update targeting ages with gender information for the existing campaign.
   *
   * @param commit
   * @param rootState
   * @param payload
   * @returns {Promise<*|undefined>}
   */
  async updateAges({ commit, rootState }, payload) {
    commit(types.UPDATE_AGES.PENDING);

    try {
      const response = await api.targeting.ages.update(rootState.wizard.id, payload);
      commit(types.UPDATE_AGES.SUCCESS);
      commit('wizard/budget/fill', response.data.data, { root: true });
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_AGES.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Update campaign locations targeting.
   */
  updateLocations: storeHelper.createAction(types.UPDATE_LOCATIONS, {
    call ({ campaign, values }) {
      return api.targeting.locations.update(
        campaign,
        TargetingMapper.mapZipObjectToApi(values.zip),
        TargetingMapper.mapLocationsObjectToApi(values.city),
      );
    },
    after: ({ commit }, data) => {
      commit('wizard/budget/fill', data.data, { root: true });
    }
  }),

  /**
   * Update campaign genres targeting.
   */
  updateGenres: storeHelper.createAction(types.UPDATE_GENRES, {
    call ({ campaign, values }) {
      return api.targeting.genres.update(
        campaign,
        TargetingMapper.mapGenresObjectToApi(values)
      );
    },
    after: ({ commit }, data) => {
      commit('wizard/budget/fill', data.data, { root: true });
    }
  }),

  /**
   * Update campaign devices targeting.
   */
  updateDeviceGroups: storeHelper.createAction(types.UPDATE_DEVICES, {
    call ({ campaign, values }) {
      return api.targeting.devices.update(
        campaign,
        TargetingMapper.mapDevicesObjectToApi(values)
      );
    },
    after: ({ commit }, data) => {
      commit('wizard/budget/fill', data.data, { root: true });
    }
  }),

  /**
   * Update campaign audiences targeting.
   */
  updateAudiences: storeHelper.createAction(types.UPDATE_AUDIENCES, {
    call ({ campaign, values }) {
      return api.targeting.audiences.update(
        campaign,
        TargetingMapper.mapAudiencesObjectToApi(values)
      );
    },
    after: ({ commit }, data) => {
      commit('wizard/budget/fill', data.data, { root: true });
    }
  }),

  /**
   * Search Zip codes from HULU.
   *
   * @param commit
   * @param rootState
   * @param codes
   * @returns {Promise<*|undefined>}
   */
  async searchZip({ commit, rootState }, codes) {
    commit(types.SEARCH_ZIP.PENDING);

    try {
      const response = await api.targeting.locations.searchZip(rootState.wizard.id, codes);
      if (rootState.wizard.targeting.forceQuitZipSearch) {
        throw new Error('Zip code search has been cancelled');
      }
      commit(types.SEARCH_ZIP.SUCCESS, response.data.data);
      return Promise.resolve(response.data);
    } catch (error) {
      commit(types.SEARCH_ZIP.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Upload csv file
   *
   * @param commit
   * @param rootState
   * @param file
   * @returns {Promise<*|undefined>}
   */
  async uploadCsv({ commit, rootState }, file) {
    commit(types.SEARCH_ZIP.PENDING);

    try {
      const response = await api.targeting.locations.uploadCsv(rootState.wizard.id, file);

      if (rootState.wizard.targeting.forceQuitZipSearch) {
        throw new Error('Zip codes upload has been cancelled');
      }

      commit(types.SEARCH_ZIP.SUCCESS, response.data.data);
      return Promise.resolve(response.data);
    } catch (error) {
      commit(types.SEARCH_ZIP.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Fill targeting genders information.
   *
   * @param state
   * @param data
   */
  fill (state, data) {
    state.targeting = TargetingMapper.fillValues(state.targeting, data);
  },

  /**
   * Campaign targeting genders options is successfully updated
   *
   * @param state
   */
  [types.UPDATE_AGES.SUCCESS](state) {
    state[types.UPDATE_AGES.loadingKey] = false;
  },

  /**
   * Campaign targeting genders options updating
   *
   * @param state
   */
  [types.UPDATE_AGES.PENDING](state) {
    state[types.UPDATE_AGES.loadingKey] = true;
  },

  /**
   * Campaign targeting genders options was not updated.
   *
   * @param state
   */
  [types.UPDATE_AGES.FAILURE](state) {
    state[types.UPDATE_AGES.loadingKey] = false;
  },

  /**
   * Campaign targeting groups options is successfully updated
   *
   * @param state
   */
  [types.UPDATE_GROUPS.SUCCESS](state) {
    state[types.UPDATE_GROUPS.loadingKey] = false;
  },

  /**
   * Campaign targeting groups options updating
   *
   * @param state
   */
  [types.UPDATE_GROUPS.PENDING](state) {
    state[types.UPDATE_GROUPS.loadingKey] = true;
  },

  /**
   * Campaign targeting groups options was not updated.
   *
   * @param state
   */
  [types.UPDATE_GROUPS.FAILURE](state) {
    state[types.UPDATE_GROUPS.loadingKey] = false;
  },

  /**
   * Campaign targeting platforms options is successfully updated
   *
   * @param state
   */
  [types.UPDATE_PLATFORMS.SUCCESS](state) {
    state[types.UPDATE_PLATFORMS.loadingKey] = false;
  },

  /**
   * Campaign targeting platforms options updating
   *
   * @param state
   */
  [types.UPDATE_PLATFORMS.PENDING](state) {
    state[types.UPDATE_PLATFORMS.loadingKey] = true;
  },

  /**
   * Campaign targeting platforms options was not updated.
   *
   * @param state
   */
  [types.UPDATE_PLATFORMS.FAILURE](state) {
    state[types.UPDATE_PLATFORMS.loadingKey] = false;
  },

  /**
   * Create mutation for targeting locations
   *
   */
  ...storeHelper.createMutation(types.UPDATE_LOCATIONS),

  /**
   * Update genres mutations.
   */
  ...storeHelper.createMutation(types.UPDATE_GENRES),

  /**
   * Update devices mutations.
   */
  ...storeHelper.createMutation(types.UPDATE_DEVICES),

  /**
   * Update audiences mutations.
   */
  ...storeHelper.createMutation(types.UPDATE_AUDIENCES),

  /**
   * Add single audience
   */
  addAudience (state, payload) {
    const issetValue = state.targeting.audiences.findIndex(audience => audience.value === payload.value);

    if (issetValue === -1) {
      state.targeting.audiences.push(payload);
    }
  },

  /**
   * Remove single audience
   */
  removeAudience (state, payload) {
    state.targeting.audiences = state.targeting.audiences.filter(audience => {
      return audience.value !== payload.value;
    });
  },

  /**
   * Create mutation for targeting zip codes
   *
   */
  ...storeHelper.createMutation(types.SEARCH_ZIP, {
    success(state, payload) {
      state.targeting.locations.fail = Locations.sortValues(TargetingMapper.mapLocationsOptions(payload.not_found), state.targeting.locations.fail);
      state.targeting.locations.zip = Locations.sortValues(TargetingMapper.mapLocationsOptions(payload.found), state.targeting.locations.zip);
    },
  }),

  /**
   * Remove single location
   */
  removeLocation (state, payload) {
    let currentCount = state.targeting.locations.zip.length + state.targeting.locations.city.length;
    console.log(currentCount - 1, state.minCountItems)

    if (currentCount - 1 >= state.minCountItems) {
      state.targeting.locations[payload.type].splice(payload.index, 1);
    }
  },

  setMinItems (state, payload) {
    state.minCountItems = payload;
  },

  /**
   * Remove fail zip code
   */
  removeFailZipCode (state, payload) {
    state.targeting.locations.fail.splice(payload.index, 1);
  },

  /**
   * Update location type
   */
  updateLocationType (state, payload) {
    state.targeting.locations[payload.type][payload.index].included = payload.value;
  },

  /**
   * Remove all locations
   */
  clearLocations (state, type) {
    state.targeting.locations[type] = [];
  },

  /**
   * Remove genre
   */
  removeGenre (state, payload) {
    state.targeting.genres.splice(payload, 1);
  },

  /**
   * Update genre item
   */
  updateGenre (state, payload) {
    state.targeting.genres[payload.index].included = payload.value;
  },

  /**
   * Clear genres
   */
  clearGenres (state) {
    state.targeting.genres = [];
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};
