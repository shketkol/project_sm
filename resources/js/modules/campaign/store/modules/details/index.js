import storeHelper from 'Foundation/helpers/store';
import { campaign as api } from '../../../api';
import { getField } from "vuex-map-fields";
import cloneDeep from 'lodash/cloneDeep';
import TargetingMapper from "Modules/campaign/api/mappers/TargetingMapper";

/**
 * Types
 */
const types = {
  LOAD_DETAILS: storeHelper.createAsyncMutation('LOAD_DETAILS'),
  UPDATE_STATUS: storeHelper.createAsyncMutation('UPDATE_STATUS'),
  CLONE_CAMPAIGN: storeHelper.createAsyncMutation('CLONE_CAMPAIGN'),
};

const defaultTargeting = {
    gender: {
      value: null,
      label: null,
    },
    ages: [],
    locations: {
      zip: null,
      city: null,
      fail: []
    },
    genres: null,
    audiences: null,
    deviceGroups: null,
};

/**
 * Campaign details state
 * @type {{campaign: {}}}
 */
const state = {

  /**
   * Campaign details
   */
  campaign: {
    details: {
      budget: null,
      cost: null,
      discounted_cost: null,
      discount_money: null,
      cpm: null,
      discounted_cpm: null,
      date_end: null,
      date_start: null,
      id: null,
      impressions: null,
      name: null,
      status_id: null,
      status: null,
      timezone_id: null,
      targeting: cloneDeep(defaultTargeting),
      creative: {
        created_at: null,
        duration: null,
        extension: null,
        filesize: null,
        height: null,
        id: null,
        oreder_id: null,
        name: null,
        width: null,
      },
      promocode: null,
      countUploadedAds: 0
    },

    states: {
      can_pause: false,
      can_cancel: false,
      can_resume: false,
      can_edit: false,
      can_delete: false,
      can_duplicate: false,
      can_download_report: false,
    },

    permissions: {
      canManageCreative: false,
      canOnlyReplaceCreative: false,
    }
  },

  /**
   * Async mutation state.
   */
  pendingBeforeRedirect: false,

  /**
   * Detect if details request is being loaded.
   */
  [types.LOAD_DETAILS.loadingKey]: false,

  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.CLONE_CAMPAIGN,
  ]),
};

const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {

  /**
   * Handle all errors
   */
  handleError ({ commit }, payload) {
    commit(types.LOAD_DETAILS.FAILURE, payload);
  },

  /**
   * Validate and store s3 link on BE
   */
  async load ({ commit }, id) {
    commit(types.LOAD_DETAILS.PENDING);
    try {
      const response = await api.show(id);
      commit(types.LOAD_DETAILS.SUCCESS, response.data);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.LOAD_DETAILS.FAILURE, error);
      return Promise.reject(error.response);
    }
  },

  /**
   * Update status.
   *
   * @param commit
   * @param status
   * @returns {Promise<*|undefined>}
   */
  async updateStatus ({ commit }, status) {
    try {
      const response = await api.status.update(state.campaign.details.id, status);
      commit(types.UPDATE_STATUS.SUCCESS, response.data.data);
      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  },

  /**
   * Remove campaign
   * @param state
   * @param campaignId
   */
  async remove (state, campaignId) {
    try {
      const response = await api.remove(campaignId);
      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  },

  /**
   * Clone campaign action.
   */
  clone: storeHelper.createAction(types.CLONE_CAMPAIGN, {
    call: api.clone,
  }),

  /**
   * This action cancels inventory check Get request
   */
  cancelInventoryCheckRequest() {
    api.cancelInventoryCheckGetRequest();
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {

  /**
   * Fill creative state
   * @param state
   * @param payload
   */
  fillCreative: (state, payload) => {
    state.campaign.details.status_id = payload.status_id;
    state.campaign.details.status = payload.status;
    state.campaign.details.creative = payload.creative
    state.campaign.permissions = payload.permissions;
    state.campaign.states = payload.states
  },

  /**
   * Load campaign details success
   * @param state
   * @param payload
   */
  [types.LOAD_DETAILS.SUCCESS]: (state, payload) => {
    state[types.LOAD_DETAILS.loadingKey] = false;
    state.campaign.details = payload.data.details;
    state.campaign.details.targeting =
      TargetingMapper.fillValues(defaultTargeting, payload.data.details.targeting);
    state.campaign.states = payload.data.states;
    state.campaign.permissions = payload.data.permissions;
  },

  /**
   * Load campaign details pending
   * @param state
   */
  [types.LOAD_DETAILS.PENDING] (state) {
    state[types.LOAD_DETAILS.loadingKey] = true;
  },

  /**
   * Load campaign details failure
   * @param state
   */
  [types.LOAD_DETAILS.FAILURE] (state) {
    state[types.LOAD_DETAILS.loadingKey] = false;
    state.success = false;
  },

  /**
   * Load campaign details failure
   * @param state
   * @param payload
   */
  [types.UPDATE_STATUS.SUCCESS] (state, payload) {
    state.campaign.details = payload.details;
    state.campaign.details.targeting =
      TargetingMapper.fillValues(defaultTargeting, payload.details.targeting);
    state.campaign.states = payload.states;
  },

  /**
   * Set campaign data
   *
   * @param state
   * @param id
   * @param name
   */
  setCampaignData(state, id, name) {
    state.campaign.details.id = id;
    state.campaign.details.name = name;
  },

  /**
   * remove Creative
   *
   * @param state
   */
  removeCreative (state) {
    state.campaign.details.creative = null;
  },

  /**
   * Clone campaign mutation
   */
  ...storeHelper.createMutation(types.CLONE_CAMPAIGN, {
    success(state) {
      state.pendingBeforeRedirect = true;
    },
  }),
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
  getters,
};


