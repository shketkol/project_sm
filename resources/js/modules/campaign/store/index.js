import Vue from 'vue';
import Vuex from 'vuex';
import wizard from './modules/wizard';
import details from './modules/details';
import metrics from './modules/metrics';
import creative from 'Modules/creative/store/modules/creative'
import payment from 'Modules/payment/store/modules/payment'
import notifications from 'Modules/notification/store/modules/notifications';

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    wizard,
    creative,
    payment,
    details,
    metrics,
    notifications,
  },
});

export default store;
