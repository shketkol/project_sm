import 'Foundation/bootstrap';
import 'Foundation/services/plyr';
import AdminIndexPage from './pages/admin/TheAdminIndex';
import CreatePage from './pages/TheCreate';
import EditPage from './pages/TheEdit';
import IndexPage from './pages/TheIndex.vue';
import routes from './router/routes';
import { getRouter } from 'Foundation/services/routing/factory';
import store from './store';
import Vue from 'vue';
import SidebarItem from 'Modules/campaign/components/wizard/sidebar/items/SidebarItem';
import SidebarItemNumeric from 'Modules/campaign/components/wizard/sidebar/items/SidebarItemNumeric';
import SidebarDateRangeItem from 'Modules/campaign/components/wizard/sidebar/items/SidebarDateRangeItem';
import SidebarCollapsableItem from 'Modules/campaign/components/wizard/sidebar/items/SidebarCollapsableItem';
import SidebarTargetingIncludeItem from 'Modules/campaign/components/wizard/sidebar/items/SidebarTargetingIncludeItem';
import SidebarTargetingLocations from 'Modules/campaign/components/wizard/sidebar/items/SidebarTargetingLocations';
import SidebarTargetingItem from 'Modules/campaign/components/wizard/sidebar/items/SidebarTargetingItem';
import SidebarTargetingAudiences from 'Modules/campaign/components/wizard/sidebar/items/SidebarTargetingAudiences';
import SidebarCost from 'Modules/campaign/components/wizard/sidebar/items/SidebarCost';
import SidebarCreative from 'Modules/campaign/components/wizard/sidebar/items/SidebarCreative';

Vue.component('SidebarItem', SidebarItem);
Vue.component('SidebarItemNumeric', SidebarItemNumeric);
Vue.component('SidebarDateRangeItem', SidebarDateRangeItem);
Vue.component('SidebarCollapsableItem', SidebarCollapsableItem);
Vue.component('SidebarTargetingIncludeItem', SidebarTargetingIncludeItem);
Vue.component('SidebarTargetingItem', SidebarTargetingItem);
Vue.component('SidebarTargetingAudiences', SidebarTargetingAudiences);
Vue.component('SidebarCost', SidebarCost);
Vue.component('SidebarCreative', SidebarCreative);
Vue.component('SidebarTargetingLocations', SidebarTargetingLocations);

(new Vue({

  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * App name.
   */
  name: 'Campaign',

  /**
   * Child components.
   */
  components: {
    AdminIndexPage,
    CreatePage,
    EditPage,
    IndexPage,
  },

  /**
   * Vue router.
   */
  router: getRouter({
    mode: 'history',
    routes,
  }),

  /**
   * Store
   */
  store,
}));
