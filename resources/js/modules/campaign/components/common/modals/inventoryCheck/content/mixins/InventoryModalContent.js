import {mapGetters} from 'vuex';
import { escape } from 'lodash';

export default {
  /**
   * Computed properties
   */
  computed: {
    /**
     * Vuex (steps).
     */
    ...mapGetters('wizard/steps', ['groups']),

    /**
     * Route for "Targeting" step.
     */
    targetingRoute () {
      return this.groups.targeting.items.locations.route;
    },
  },

  methods: {
    /**
     * This method handles click by button "Expand targeting".
     */
    clickExpandTargeting() {
      this.$pushRoute(this.targetingRoute);
      this.handleClose();
    },

    /**
     * This method is called before close modal.
     *
     */
    handleClose() {
      this.$emit('close');
    },

    /**
     * Get budget value.
     */
    getEscapedValue (value) {
      return escape(value);
    }
  }
}
