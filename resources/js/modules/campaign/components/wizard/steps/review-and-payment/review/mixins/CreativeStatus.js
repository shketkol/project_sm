import { mapState } from 'vuex';

export default {
   computed: {
    /**
     * Vuex (state).
     */
    ...mapState('wizard/creative', [
      'creative',
    ]),

    /**
     * Check creative status
     * @returns {boolean}
     */
    hasApproved () {
      const statusApproved = this.$trans('creative::labels.creative.statuses.approved');
      return this.creative && this.creative.creative_status === statusApproved.toLowerCase();
    }
  }
};
