import {mapGetters, mapMutations, mapState} from "vuex";

export default {
  /**
   * Component props
   */
  props: {
    /**
     * Audience item.
     */
    item: {
      type: Object,
      required: true,
    },
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Vuex (state).
     */
    ...mapGetters('wizard/targeting', {
      values: 'audiences',
    }),

    /**
     * Vuex (fields).
     */
    ...mapState('wizard/targeting', {
      minCountItems: 'minCountItems'
    }),
  },

  /**
   * Watchers
   */
  watch: {
    /**
     * Item watcher
     */
    item : {
      handler () {
        this.setChecked();
      },
      deep: true,
    },

    /**
     * Values watcher
     */
    values : {
      handler () {
        this.setChecked();
      },
      deep: true,
    },

    /**
     * Checked watcher
     */
    checked (value, old) {
      console.log(value, old)
      // this.$emit('checked-change', value ? 1 : -1);
    },
  },

  /**
   * Mounted hook
   */
  mounted () {
    this.setChecked();
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Map mutations.
     */
    ...mapMutations('wizard/targeting', [
      'addAudience',
      'removeAudience',
    ]),

    /**
     * Check if value is present in local data
     */
    setChecked () {
      if (Array.isArray(this.values) && this.values.some(audience => audience.value === this.item.value)) {
        return this.checked = true;
      }

      return this.checked = false;
    },

    /**
     * Native change value handler
     */
    changeValue (checked) {
      if (checked) {
        this.addAudience(this.item);
        return;
      }

      let currentCount = this.values;
      if (currentCount - 1 >= this.minCountItems) {
        this.removeAudience(this.item);
      }


    },
  },
};
