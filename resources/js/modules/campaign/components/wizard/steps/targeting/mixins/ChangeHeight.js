export default {
  /**
   * Methods.
   */
  methods: {
    /**
     * Handler of <el-collapse> 'change' event.
     * @param {(string|Array)} activeName
     */
    onCollapseChange (activeName) {
      this.$nextTick(() => {
        this.emitChangeHeight(activeName && activeName.length !== 0);
      });
    },

    /**
     * Emit 'change-height' event.
     * @param {boolean} toChange Identifies if container's height should be expanded.
     */
    emitChangeHeight (toChange) {
      this.$emit('change-height', toChange);
    },
  },
}
