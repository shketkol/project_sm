import {mapActions} from 'vuex';

export default {
  methods: {
    /**
     * Vuex (actions).
     */
    ...mapActions('wizard/budget', ['setInventoryChecked']),

    /**
     * This method closes inventory check modal.
     */
    closeInventoryCheckModal() {
      this.showWarningModal = false;
    },
  }
};
