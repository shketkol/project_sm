import CreateCampaignFormStep from './CreateCampaignFormStep';
import mapValues from 'lodash/mapValues';

export default {
  /**
   * Mixins.
   */
  mixins: [
    CreateCampaignFormStep,
  ],

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Get all options for the select.
     */
    formattedOptions () {
      if (!this.options) {
        return {};
      }

      return mapValues(this.options, option => {
        return {
          label: option.label,
          value: {
            value: option.value,
            label: option.label,
            included: true,
          },
        };
      });
    },
  },

  /**
   * Watchers.
   */
  watch: {
    value: {
      deep: true,
      handler: 'onUpdate',
    },
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * This method is called when search box (select) is opened or closed.
     *
     * @param visible
     */
    onVisibleChange (visible) {
      if (!visible) {
        this.options = [];
      }
    },
  },
};
