import { mapActions, mapState, mapGetters } from 'vuex';
import { Popover } from 'Foundation/components/notice';
import { TextField, Radio, SelectField, CheckboxGroup } from 'Foundation/components/form';
import SetCampaignActivityStep from './SetCampaignActivityStep';
import { debounce, isEqual, cloneDeep } from "lodash";
import TargetingValidation from '../../mixins/TargetingValidation';

export default {
  /**
   * Child components.
   */
  components: {
    CheckboxGroup,
    Popover,
    Radio,
    SelectField,
    TextField,
  },

  /**
   * Mixins.
   */
  mixins: [SetCampaignActivityStep, TargetingValidation],

  /**
   * Data.
   */
  data () {
    return {
      doNativeCheckFilled: true,
      shouldTriggerInventory: true,
      shouldTriggerAutoSave: true,
      saveDebounceTimeout: 1500,// 1.5 seconds
      defaultOptionsAreSet: false,
      stepLastValue: null,


      oldValue: null
    }
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Map state.
     */
    ...mapState('wizard', {campaignId: 'id'}),

    /**
     * Map getters.
     */
    ...mapGetters('wizard/steps', ['getStep']),

    /**
     * Check if user has special ads category.
     * @returns {boolean}
     */
    isSpecialAds () {
      return this.$user.states.is_special_ads;
    },

    /**
     * Skip inventory if values don't changed
     * @returns {boolean}
     */
    skipInventory () {
      return isEqual(this.getCombinedStepValue, this.stepLastValue);
    },

    /**
     * Getter for summarized step value.
     * @returns {{}}
     */
    getCombinedStepValue () {
      return this.value;
    }
  },

  /**
   * Created hook.
   */
  created() {
    this.$nextTick(this.focus);
  },

  /**
   * Mounted hook.
   */
  mounted () {
    this.addStepButtonsHandlers();
    this.setActivityMethod();
    this.saveWithInventoryDebounced = debounce(this.saveWithInventory, this.saveDebounceTimeout);
  },

  /**
   * Before route leave hook.
   *
   * @param to
   * @param from
   * @param next
   */
  beforeRouteLeave (to, from, next) {
    this.onLeave()
      .then(() => { next(); })
      .catch(() => { next(false); });
  },

  /**
   * Methods.
   */
  methods: {
    ...mapActions('wizard', {
      saveCampaign: 'saveCampaign',
      updateStep: 'steps/updateStep',
      updateFilled: 'steps/updateFilled',
      sendActivity: 'steps/sendActivity',
      getDynamicInventoryCheck: 'inventory/getDynamicInventoryCheck',
    }),

    /**
     * Focus element.
     */
    focus () {
      const ref = this.focused ? this.$refs[this.focused] : null;

      if (ref) {
        ref.focus();
      }
    },

    /**
     * This method is called each time the step is changed to next.
     * @returns {*}
     */
    beforeNext () {
      return this.validate();
    },

    /**
     * Validate step.
     *
     * @returns {*}
     */
    validate () {
      return this.$validateScope();
    },

    /**
     * Sets default step values
     */
    setStepLastValue (value) {
      this.stepLastValue = cloneDeep(value);
      this.defaultOptionsAreSet = true;
    },

    /**
     * This method is called each time step is left.
     *
     * @returns {Promise<any>}
     */
    onLeave () {
      if (!this.getStep(this.key).saved && !this.savePending) {
        this.saveWithInventoryDebounced.cancel();
        this.disableButtons();
        return this.saveWithInventory().catch(error => {
          this.enableButtons();
          throw error;
        });
      } else {
        return Promise.resolve(true);
      }
    },

    /**
     * Save + Dynamic inventory entry point.
     *
     * @returns {Promise<any>}
     */
    saveWithInventory () {
      return this.save()
        .then(this.setSaved)
        .then(this.setDone)
        .then(() => {
          if (this.shouldTriggerInventory && !this.skipInventory) {
            this.$call(this.getDynamicInventoryCheck(this.campaignId), {notify: false, muteException: true});
            this.setStepLastValue(this.getCombinedStepValue);
          }
        })
        .catch();
    },

    /**
     * This method is called each time user changes data on the step.
     * @param value
     */
    onUpdate (value = null) {
      this.setUnsaved();

      if (this.doNativeCheckFilled) {
        this.checkFilled(value);
      }

      if (this.shouldTriggerAutoSave && this.defaultOptionsAreSet) {
        this.saveWithInventoryDebounced();
      }
    },

    /**
     * Update the "filled" state of the step by it's value.
     * @param {object} payload
     */
    checkFilled (payload) {
      if (this.key) {
        this.updateFilled({
          path: this.key,
          value: payload,
        });
      }
    },

    /**
     * Default save method (it should be changed for all steps).
     * @returns {Promise<boolean>}
     */
    save () {
      return Promise.resolve(true);
    },

    /**
     * Mark the step as "done".
     */
    setDone () {
      if (this.key) {
        this.updateStep({
          path: `${this.key}.done`,
          value: true,
        })
      }
    },

    /**
     * Mark the step as "saved".
     */
    setSaved () {
      if (this.key) {
        this.setLatestActivity(this.activity)

        this.updateStep({
          path: `${this.key}.saved`,
          value: true,
        });
      }
    },

    /**
     * Mark the step as "unsaved".
     */
    setUnsaved () {
      if (this.key) {
        this.updateStep({
          path: `${this.key}.saved`,
          value: false,
        });
      }
    },

    /**
     * Disables buttons before saving.
     */
    disableButtons () {
      this.$emit('update:buttons-disabled', true);
    },

    /**
     * Enables buttons after saving.
     */
    enableButtons () {
      this.$emit('update:buttons-disabled', false);
    },

    /**
     * Get campaign-step component and add handlers for its prev/next events.
     * NOTE: This will work only if <campaign-step> is located at the first level of the particular wizards step.
     */
    addStepButtonsHandlers () {
      const campaignStepComponent = this.$children.find(child => (child.$options.name === 'CampaignStep'));
      if (campaignStepComponent) {
        campaignStepComponent.$on('next', () => this.$emit('next'));
        campaignStepComponent.$on('previous', () => this.$emit('previous'));
      }
    },
  },
};
