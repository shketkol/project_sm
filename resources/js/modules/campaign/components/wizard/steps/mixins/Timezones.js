import { mapFields } from 'vuex-map-fields';
import timezones from 'Static/timezones.json';

export default {

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Vuex map fields.
     */
    ...mapFields('wizard/details', [
      'timezone_id',
    ]),
  },

  methods: {
    /**
     * Set stored variable timezone_id.
     */
    setTimezoneId () {
      if (this.timezone_id) {
        return;
      }
      const defaultTimezone = timezones.find(timezone => timezone.code === this.$config.date.default_timezone_code);
      if (defaultTimezone) {
        this.timezone_id = defaultTimezone.id;
      }
    },
  },
};
