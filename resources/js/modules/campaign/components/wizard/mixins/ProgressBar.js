import {filter} from "lodash";

export default {
  methods: {
    /**
     * Total items of the current section.
     * @returns {number}
     */
    totalItems (group) {
      return Object.keys(group.items).length;
    },

    /**
     * Total filled steps of the current section.
     * @returns {*}
     */
    totalFilledItems (group) {
      return filter(group.items, (item) => item.filled).length;
    },
  }
};
