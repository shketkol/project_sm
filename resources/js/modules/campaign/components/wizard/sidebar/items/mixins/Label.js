export default {
  /**
   * Methods.
   */
  methods: {
    /**
     * Get label name.
     */
    getLabel (label, items) {
      const index = Object.entries(items).map((item) => (item[0])).findIndex(item => item === label);
      if (Object.keys(items).length === 1) {
        return label
      }

      if (Object.keys(items).length <= 2) {
        return index !== 0 ? label  : label + '&#160;' + this.$trans('labels.or');
      }

      if (index === Object.keys(items).length - 1) {
        return label;
      }

      return index === Object.keys(items).length - 2 ? label + ',&#160;' +  this.$trans('labels.or') : label + ','
    }
  }
}
