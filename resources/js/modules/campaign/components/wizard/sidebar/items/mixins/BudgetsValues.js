import { mapState } from 'vuex';

export default {
  /**
   * Computed properties.
   */
  computed: {
    /**
     * Vuex (state).
     */
    ...mapState('wizard/budget', ['cost']),
  },

  methods: {
    /**
     * Returns cost in correct format.
     *
     * @returns {string}
     */
    formattedCost (value) {
      const currency = this.$user.currency.code;

      return `${this.$formatCurrency(value, currency)}`;
    },
  }
}
