import CampaignDetails from '../../components/CampaignDetails';
import CampaignIndex from "Modules/campaign/components/CampaignIndex";

export default [
  {
    path: '/campaigns',
    name: 'campaigns.index',
    component: CampaignIndex,
  },
  {
    path: '/campaigns/:campaignId',
    name: 'campaigns.show',
    component: CampaignDetails,
  },
];
