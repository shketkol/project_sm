/**
 * Map reminders data to be used be the client.
 *
 * @param {Array} data
 * @returns {*}
 */
const mapRemindersToClient = (data) => {
  return data.map((item) => {
    return {
      id: item.id,
      reminderType: item.reminder_type,
      messageType: item.message_type,
      customClasses: item.custom_classes,
      title: item.title,
      message: item.message,
      ctaItems: item.ctaItems,
      payload: item.payload,
    }
  });
};

export default class NotificationsMapper {
  /**
   * Map widgets data to be used be the client.
   *
   * @param {{}} data
   * @returns {{}}
   */
  static mapWidgetsDataToClient ({ data }) {
    return {
      notifications: data.notifications,
      reminders: mapRemindersToClient(data.reminders),
    };
  }
}
