export default {
  /**
   * Computed values
   */
  methods: {
    /**
     * Define card brand image
     */
    cardImage (brand) {
      return `/images/cards/${this.$config.cards.icons.svg[brand]}`;
    },
  }
};
