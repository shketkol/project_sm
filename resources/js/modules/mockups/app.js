import 'Foundation/bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router'
import App from './App.vue';
import routes from './router';

Vue.use(VueRouter);

const router = new VueRouter({
  routes
});

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    App
  },
  router,
}));
