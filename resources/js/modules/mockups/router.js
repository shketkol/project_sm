import TheMockupsHome from './pages/TheMockupsHome';
import TheLandingPage from './pages/TheLandingPage';
import TheStyleMap from './pages/TheStylemap';
import TheForgotPassword from './pages/TheForgotPassword';
import TheAdvertiserProfile from './pages/TheAdvertiserProfile';
import TheCampaigns from './pages/TheCampaigns';
import TheAdminCampaigns from './pages/TheAdminCampaigns';
import TheCampaignCreation from './pages/TheCampaignCreation';
import TheSinglePageLayout from './pages/TheSinglePageLayout';

export default [
  { path: '/', component: TheMockupsHome },
  { path: '/landing', component: TheLandingPage },
  { path: '/stylemap', component: TheStyleMap },
  { path: '/forgot-password', component: TheForgotPassword },
  { path: '/advertiser-profile', component: TheAdvertiserProfile },
  { path: '/campaigns', component: TheCampaigns },
  { path: '/admin-campaigns', component: TheAdminCampaigns },
  { path: '/create-campaign', component: TheCampaignCreation },
  { path: '/single-page-layout', component: TheSinglePageLayout },
];
