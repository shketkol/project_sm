import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { location as api } from '../../api';
import LocationMapper from 'Modules/user/api/mappers/LocationMapper';

/**
 * Types
 */
const types = {
  SEARCH_ZIP_CODES: storeHelper.createAsyncMutation('SEARCH_ZIP_CODES'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * ZipCodes
   */
  zipCodes: []
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  searchZipCodes: storeHelper.createAction(types.SEARCH_ZIP_CODES, {
    call: api.searchZipCodes
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Search zip codes mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SEARCH_ZIP_CODES, {
    success (state, { data }) {
      state.zipCodes = LocationMapper.mapApiToObjectZipCodes(data);
    },
  }),

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};
