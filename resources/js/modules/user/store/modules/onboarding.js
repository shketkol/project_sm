import storeHelper from 'Foundation/helpers/store';
import { onboarding as api } from '../../api';

/**
 * Types
 */
const types = {
  COMPLETE: storeHelper.createAsyncMutation('COMPLETE'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Complete onboarding loading.
   */
  [types.COMPLETE.loadingKey]: null,
};

/**
 * Actions
 */
const actions = {
  /**
   * Complete onboarding.
   *
   * @param commit
   * @returns {Promise<*|undefined>}
   */
  async complete ({ commit }) {
    commit(types.COMPLETE.PENDING);

    try {
      const response = await api.complete();
      commit(types.COMPLETE.SUCCESS, response);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.COMPLETE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Onboarding is completed.
   *
   * @param state
   */
  [types.COMPLETE.SUCCESS]: (state) => {
    state[types.COMPLETE.loadingKey] = false;
  },

  /**
   * Loading.
   *
   * @param state
   */
  [types.COMPLETE.PENDING] (state) {
    state[types.COMPLETE.loadingKey] = true;
  },

  /**
   * Onboarding failed.
   *
   * @param state
   */
  [types.COMPLETE.FAILURE] (state) {
    state[types.COMPLETE.loadingKey] = false;
  },
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
};


