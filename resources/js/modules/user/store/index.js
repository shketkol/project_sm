import onboarding from './modules/onboarding';
import profile from './modules/profile';
import location from './modules/location';
import Vue from 'vue';
import Vuex from 'vuex';
import payment from "Modules/payment/store/modules/payment";
import notifications from "Modules/notification/store/modules/notifications";

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    payment,
    onboarding,
    profile,
    notifications,
    location,
  },
});

export default store;
