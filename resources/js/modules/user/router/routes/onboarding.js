import Step1 from '../../components/onboarding/steps/Step1';
import Step2 from '../../components/onboarding/steps/Step2';
import Step3 from '../../components/onboarding/steps/Step3';

export default [
  {
    path: '/users/onboarding/steps/one',
    component: Step1,
    name: 'onboarding.steps.one',
  },
  {
    path: '/users/onboarding/steps/two',
    component: Step2,
    name: 'onboarding.steps.two',
  },
  {
    path: '/users/onboarding/steps/three',
    component: Step3,
    name: 'onboarding.steps.three',
  },
]
