import TransactionHistoryTable from '../../../payment/components/TransactionHistoryTable';
import { isDemo } from "Modules/demo/helpers/demo";

export default [
  {
    path: '/users/profile/transactions',
    component: TransactionHistoryTable,
    name: 'profile.transactionHistory',
    children: [
      {
        path: ':orderId',
        name: 'profile.transactionHistory.show',
        component: TransactionHistoryTable,
      },
    ]
  },
  {
    path: '/users/profile/settings',
    component: () => isDemo() ?
      import('../../../demo/modules/user/components/profile/settings/SettingsTab') :
      import('../../components/profile/settings/SettingsTab'),
    name: 'profile.settings',
    children: [
      {
        path: 'contact',
        name: 'profile.settings.contact',
        component: () => isDemo() ?
          import('../../../demo/modules/user/components/profile/settings/SettingsTab') :
          import('../../components/profile/settings/SettingsTab'),
      },
    ]
  },
]
