import { map } from 'lodash';

export default class LocationMapper {
  static mapApiToObjectZipCodes (data) {
    return data.map(option => ({
        value: option.zip_code,
        label: option.zip_code,
        states: option.states
      })
    );
  }

  static mapStates (data) {
    return map(data, 'name');
  }
}
