import axios from 'axios';
import route from 'Foundation/services/routing/route';

export default {
  /**
   * Search zipcodes
   * @return {AxiosPromise<any>}
   */
  searchZipCodes ({query, state}) {
    return axios.get(
      route('api.locations.zipcodes.search', {
        state: state,
        query
      }),
    );
  },
}
