/**
 * Check if App has demo env enabled
 */
const isDemo = () => {
  return window.config.general.demo.enabled
};

export {
  isDemo
}
