import TheCreativesIndex from "Modules/creative/pages/TheCreativesIndex";

export default [
  {
    path: '/creatives',
    name: 'creatives.index',
    component: TheCreativesIndex
  },
  {
    path: '/creatives/:creative',
    name: 'creatives.show',
    component: TheCreativesIndex
  },
];
