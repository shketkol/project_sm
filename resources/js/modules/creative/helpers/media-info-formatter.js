import { findIndex, set, get } from 'lodash';

/**
 * Media-info sections.
 */
const MI_SECTION_GENERAL = 'General';
const MI_SECTION_VIDEO = 'Video';
const MI_SECTION_AUDIO = 'Audio';

/**
 * Convert value to int.
 * @param {*} val
 * @returns {(number|null)}
 */
const toInt = val => isNaN(val) ? null : parseInt(val);

/**
 * Convert value to float.
 * @param {*} val
 * @returns {(number|null)}
 */
const toFloat = val => isNaN(val) ? null : parseFloat(val);

const fieldsFormatters = {
  'general.VideoCount': toInt,
  'general.AudioCount': toInt,
  'general.FileSize': toInt,
  'general.Duration': toFloat,
  'video.BitRate': toInt,
  'video.Width': toInt,
  'video.Height': toInt,
  'video.FrameRate': toFloat,
  'video.BitDepth': toInt,
  'audio.Duration': toFloat,
  'audio.BitRate_Nominal': toInt,
  'audio.Channels': toInt,
  'audio.SamplingRate': toInt,
};

/**
 * Apply formatters to raw media-info
 * @param {*} mediaInfoData
 * @param {*} file
 */
const formatMediaInfo = (mediaInfoData, file) => {
  const mediaInfo = getMediaInfo(mediaInfoData);
  applyFormatters(mediaInfo);
  addMandatoryData(mediaInfo, file);
  return mediaInfo;
};

/**
 * Get media-info grouped by specific sections.
 * @param {Array} mediaInfoData
 * @returns {{general: *, video: *, audio: *}}
 */
const getMediaInfo = (mediaInfoData) => {
  return {
    general: getMediaAttributes(mediaInfoData, MI_SECTION_GENERAL),
    video: getMediaAttributes(mediaInfoData, MI_SECTION_VIDEO),
    audio: getMediaAttributes(mediaInfoData, MI_SECTION_AUDIO),
  };
};

/**
 * Get media-attributes of the specific section (track).
 * @param {Array} mediaInfoData
 * @param {string} section
 * @returns {(*|null)}
 */
const getMediaAttributes = (mediaInfoData, section) => {
  const key = findIndex(mediaInfoData, ['@type', section]);
  return key !== -1 ? mediaInfoData[key] : null;
};

/**
 * Apply formatters to raw media-info
 * @param {*} mediaInfo
 */
const applyFormatters = (mediaInfo) => {
  for (let path in fieldsFormatters) {
    const val = get(mediaInfo, path);
    const formatter = fieldsFormatters[path];
    if (val !== undefined && typeof formatter === 'function') {
      set(mediaInfo, path, formatter(val));
    }
  }
};

/**
 * Add required info in case if it missing in mediaInfo output
 * @param {*} mediaInfo
 * @param {*} file
 */
const addMandatoryData = (mediaInfo, file) => {
  if (!mediaInfo.general.Extension) {
    set(mediaInfo, 'general.Extension', file.extension);
  }

  if (!mediaInfo.general.Format) {
    set(mediaInfo, 'general.Format', file.data.type);
  }

  if (!mediaInfo.general.FileSize) {
    set(mediaInfo, 'general.FileSize', file.size);
  }
}

export {
  formatMediaInfo,
}
