import { get, cloneDeep } from 'lodash';

const MAP_SOURCE_RULE = {
  'general.VideoCount': 'creative.general.video_cnt',
  'general.AudioCount': 'creative.general.audio_cnt',
  'general.Format': 'creative.general.format',
  'general.Extension': 'creative.general.extension',
  'general.FileSize': 'creative.general.file_size',
  'video.Duration': 'creative.general.duration',
  'video.CodecID': 'creative.general.codec',
  'video.BitRate': 'creative.general.video_bit_rate',
  'video.Width': 'creative.general.width',
  'video.Height': 'creative.general.height',
  'video.DisplayAspectRatio': 'creative.general.display_aspect_ratio',
  'video.FrameRate_Mode': 'creative.general.frame_rate_mode',
  'video.FrameRate': 'creative.general.frame_rate',
  'video.ColorSpace': 'creative.general.color_space',
  'video.ChromaSubsampling': 'creative.general.chroma_subsampling',
  'video.BitDepth': 'creative.general.bit_depth',
  'video.ScanType': 'creative.general.scan_type',
  'audio.Format': 'creative.audio.format',
  'audio.Duration': 'creative.audio.duration',
  'audio.BitRate': 'creative.audio.bit_rate',
  'audio.Channels': 'creative.audio.channels',
  'audio.SamplingRate': 'creative.audio.sampling_rate',
};

const MANDATORY_FIELDS = [
  'creative.general.format',
  'creative.general.extension',
  'creative.general.file_size'
];

export default {
  /**
   * Computed properties
   */
  computed: {

    /**
     * Return available validation rules
     */
    validation () {
      return cloneDeep(this.validationRules);
    },

    /**
     * Form errors collection
     */
    formErrors () {
      return this.$validator.errors.collect('*', this.scope);
    },

    /**
     * Check if any errors in ErrorBag
     */
    errorsAny () {
      return this.$validator.errors.any(this.scope);
    },

    /**
     * Validation scope
     * @returns {string}
     */
    scope () {
      return '__creative__';
    },
  },

  methods: {
    /**
     * Use same mechanism for be validation
     */
    handleBeErrors (errorBag) {
      for (let field in errorBag) {
        for (let error in errorBag[field])
          this.$validator.errors.add({
              field: field,
              msg: errorBag[field][error],
              scope: this.scope,
            }
          );
      }
    },

    /**
     * Manually verify vuex file state. Push errors to ErrorsBag.
     * @param params
     * @returns {Promise<any>}
     */
    verify (params = null) {
      // if no params validate all file keys
      params = params ? params : Object.keys(MAP_SOURCE_RULE);

      // validate if backend rule is available
      let rules = this.collectValidationRules(params);

      const promises = rules.promises;
      let fields = rules.fields;

      // push failed rules to errorBag
      return new Promise((resolve) => {
        Promise.all(promises)
          .then((results) => {
            for (let result in results) {
              if (!results[result].valid) {
                for (let error in results[result].errors) {
                  this.$validator.errors.add({
                      field: fields[result],
                      msg: results[result].errors[error],
                      scope: this.scope,
                    }
                  );
                }
              }
            }
            resolve();
          })
          .catch((error) => {
            this.$log('Varification error', error);
            resolve();
          });
      });
    },

    /**
     * Collect Validation rules based on params
     * @returns {*}
     */
    collectValidationRules (params) {
      let promises = [];
      let fields = [];

      for (let key in MAP_SOURCE_RULE) {
        const fieldKey = MAP_SOURCE_RULE[key];
        const rules = this.getValidationRules(fieldKey);
        if (
          rules !== undefined &&
          params.indexOf(key) !== -1
        ) {
          fields.push(fieldKey);
          promises.push(
            this.$validator.verify(
              get(this.mediaInfoData, key),
              rules,
              { name: fieldKey }
            ),
          );
        }
      }

      return {
        promises: promises,
        fields: fields
      };
    },

    /**
     * Clear all error instances
     */
    clearAllErrors () {
      this.errors.clear(this.scope);
      this.$validator.errors.clear(this.scope);
    },

    /**
     * Remove required validation and return updated rules.
     * @param {string} fieldKey
     * @returns {(null|*)}
     */
    getValidationRules (fieldKey) {
      const rules = get(this.validation, fieldKey, null);
      if (rules === null) {
        return null;
      }
      if (rules.required === true && !MANDATORY_FIELDS.includes(fieldKey)) {
        rules.required = false;
      }
      return rules;
    },
  }
};
