<template>
  <div class="upload-creative">
    <modal-creative-upload-limit
      :visible.sync="showCreativesLimitModal"
    />
    <el-upload
      class="upload-creative__input"
      drag
      :show-file-list="false"
      :auto-upload="false"
      :on-change="initiateUploadProcess"
      :multiple="false"
      accept=".mp4,.mov"
      action=""
      :disabled="checkUploadAbilityPending"
    >
      <div class="el-upload__text">
        <span
          class="eyebrow"
          v-html="$trans('creative::content.drag_drop_area')"
        />
      </div>
      <el-button
        v-loading="checkUploadAbilityPending"
        class="upload-creative__button"
        :disabled="checkUploadAbilityPending"
      >
        {{ $trans('creative::content.upload_video') }}
      </el-button>
      <div class="upload-creative__tip">
        <small class="upload-creative__tip-item">
          {{ $trans('creative::content.format') }}: .mp4 {{ $trans('creative::content.or') }} .mov
        </small>
        <small class="upload-creative__tip-item">
          {{ $trans('creative::content.max_file_size') }}:
          {{ formatFilesize($config.rules.creative.max_file_size) }}
        </small>
        <small class="upload-creative__tip-item">
          {{ $trans('creative::content.duration') }}:
          {{ $config.rules.creative.min_duration_label }}-{{ $config.rules.creative.max_duration_label }}
          {{ $trans('creative::content.seconds') }}
        </small>
      </div>
    </el-upload>
    <errors-modal
      :visible="errorsAny"
      :validation-errors="formErrors"
      @proceed-upload="proceedUpload"
      @close="clearAllErrors"
    />
    <upload-progress
      ref="uploader"
      class="loader-lg"
      :title="progressTitle"
      :visible="showUploadProgress"
      :loading="storeCreativePending"
      :modal-loading="processingValidation"
      @abort="abortProcess"
      @pause="pauseProcess"
      @resume="resumeProcess"
    />
  </div>
</template>

<script>
  import { flatMap, throttle } from 'lodash';
  import {mapFields} from 'vuex-map-fields';
  import {mapActions, mapState, mapMutations} from 'vuex';
  import { formatMediaInfo } from '../helpers/media-info-formatter';
  import UploadProgress from './modals/UploadProgress';
  import Errors from './modals/Errors';
  import Validation from '../mixins/Validation';
  import Loading from 'Foundation/mixins/Loading';
  import FileHash from 'Foundation/mixins/File/FileHash';
  import MediaInfo from 'Foundation/mixins/File/MediaInfo';
  import formatFilesize from 'filesize';
  import ModalCreativeUploadLimit from 'Modules/advertiser/components/common/modals/ModalCreativeUploadLimit';

  /**
   * Minimal interval between progress bar updates (in milliseconds).
   */
  const MIN_INTERVAL_BETWEEN_PROGRESS_UPDATES = 200;

  /**
   * Upload process phases states.
   */
  const UPLOAD_PHASE_CLEAR = 'clear';
  const UPLOAD_PHASE_VALIDATION = 'validation';
  const UPLOAD_PHASE_UPLOAD = 'upload';

  /**
   * Upload process UI states.
   */
  const UI_STATE_RELEASED = 'released';
  const UI_STATE_PAUSED = 'paused';
  const UI_STATE_ABORTED = 'aborted';

  export default {
    /**
     * Component name
     */
    name: 'UploadCreative',

    /**
     * Used components
     */
    components: {
      modalCreativeUploadLimit: ModalCreativeUploadLimit,
      uploadProgress: UploadProgress,
      errorsModal: Errors
    },

    /**
     * Mixins
     */
    mixins: [
      Validation,
      Loading,
      FileHash,
      MediaInfo,
    ],

    /**
     * Component properties.
     */
    props: {
      /**
       * Campaign id
       */
      campaignId: {
        type: Number,
        default: null,
      },
    },

    /**
     * Data values
     */
    data() {
      return {
        retries: 0,
        formatFilesize: formatFilesize,
        showCreativesLimitModal: false,
        uploadPhase: UPLOAD_PHASE_CLEAR,
        uiState: UI_STATE_RELEASED,
        progressPaused: false,
        validated: false,
        mediaInfoData: {},
        readingMediaInfo: false,
        skipDuplicateCheck: false,
      }
    },

    /**
     * Computed properties
     */
    computed: {
      /**
       * Fields mapping
       */
      ...mapFields('creative', {
        file: 'file',
        data: 'file.data',
        name: 'file.name',
        file_path: 'file.file_path',
        extension: 'file.extension',
        file_size: 'file.fileSize',
        duration: 'file.duration',
        width: 'file.width',
        height: 'file.height',
        hashSum: 'file.hashSum',
      }),

      /**
       * Fields mapping
       */
      ...mapFields('creative/evaporate', {
        uploadStats: 'uploadStats',
        bucket: 'config.s3Bucket',
        maxRetries: 'config.maxUploadRetries'
      }),

      /**
       * State mapping
       */
      ...mapState('creative', [
        'uploadCreativePending',
        'storeCreativePending',
        'checkUploadAbilityPending',
      ]),

      /**
       * State mapping
       */
      ...mapState('creative/evaporate', [
        'getUploadConfigurationPending',
      ]),

      /**
       * Check if upload bar should be shown.
       * @returns {boolean}
       */
      showUploadProgress () {
        return this.uploadStats.visible || this.uploadPhase === UPLOAD_PHASE_VALIDATION;
      },

      /**
       * Get title of the progress modal box.
       */
      progressTitle () {
        switch (this.uploadPhase) {
          case UPLOAD_PHASE_VALIDATION:
            return this.$trans('creative::labels.validating');
          case UPLOAD_PHASE_UPLOAD:
            return this.$trans('creative::labels.uploading');
          default:
            return '';
        }
      },

      /**
       * Show loader on progress modal during loading of mediaInfo module and upload-configuration.
       * @returns {boolean}
       */
      processingValidation () {
        return this.readingMediaInfo || this.getUploadConfigurationPending;
      }
    },

    /**
     * Watchers
     */
    watch: {
      /**
       * Large loader on creative store pending
       */
      storeCreativePending (value) {
        if (value) {
          document.body.classList.add('loader-lg');
        } else {
          document.body.classList.remove('loader-lg');
        }
      },

      /**
       * Fired when validation is failed.
       */
      errorsAny (val) {
        if (val) {
          this.trackErrors();
        }
      },
    },

    /**
     * Component methods
     */
    methods: {
      /**
       * Map creative actions
       */
      ...mapActions('creative', [
        'store',
        'storeRejected',
      ]),

      ...mapActions('creative/evaporate', {
        startUpload: 'start',
        abortUpload: 'abort',
        pauseUpload: 'pause',
        resumeUpload: 'resume',
        handleError: 'handleError',
        checkUpload: 'checkUpload',
        getUploadConfiguration: 'getUploadConfiguration',
      }),

      /**
       * Map mutations
       */
      ...mapMutations('creative', [
        'clearFileData'
      ]),

      /**
       * Map mutations
       */
      ...mapMutations('creative/evaporate', [
        'clearUploadStats'
      ]),

      /**
       * Check ability to upload files.
       */
      async checkAbilityToUpload() {
        const result = await this.checkUpload(this.campaignId);
        return result.creativeDraftAllowed;
      },

      /**
       * Initialize upload process.
       * Calculate hashSum based on file-contents.
       * Pre validation of basic file params.
       * Validation metatags params when file metadata loaded.
       * Initialize and starts s3Upload.
       * @returns {(boolean|undefined)}
       */
      async initiateUploadProcess(e) {
        if (!await this.checkAbilityToUpload()) {
          return this.showCreativesLimitModal = true;
        }

        const file = e.raw;
        if (file === undefined) {
          return;
        }

        this.resetState();

        this.validated = await this.initiateValidationPhase(file);

        if (!this.validated) {
          this.uploadPhase = UPLOAD_PHASE_CLEAR;
          return;
        }

        if (this.canUpload()) {
          await this.initiateUploadPhase();
        }
      },

      /**
       * Check if upload phase can be started.
       * @returns {boolean}
       */
      canUpload () {
        return this.uiState === UI_STATE_RELEASED && this.validated;
      },

      /**
       * Validates the creative file.
       * @returns {boolean}
       */
      async initiateValidationPhase (file) {
        this.uploadPhase = UPLOAD_PHASE_VALIDATION;
        try {
          this.hashSum = await this.getFileHash(file, this.updateProgress);
        } catch (error) {
          this.$log('An error occurred during generating file hash of the creative file', error);
          return false;
        }

        // Fill basic file data.
        this.fillFile(file);
        try {
          await this.setMediaInfo(file);
        } catch (error) {
          this.$log('An error occurred during processing Creative file', error);
        }

        // Validate creative.
        await this.verify();
        if (this.errorsAny) {
          await this.handleRejectedCreative();
          return false;
        }

        try {
          await this.$call(
            this.getUploadConfiguration({ hashSum: this.hashSum }),
            { notify: false, scope: this.scope }
          );
        } catch (e) {
          return false;
        }

        return true;
      },

      /**
       * Initiate upload phase.
       */
      async initiateUploadPhase () {
        this.uploadPhase = UPLOAD_PHASE_UPLOAD;
        this.resetProgress();
        this.$trackAction('ad_upload');
        this.retries = 0;

        //get upload configuration any way in case if file is force-uploaded
        if (this.skipDuplicateCheck) {
          await this.$call(
            this.getUploadConfiguration({ hashSum: null }),
            { notify: false, scope: this.scope }
          );
        }
        this.upload();
      },

      /**
       * Set media-info data.
       * @param {File} file
       * @returns {Promise}
       */
      setMediaInfo (file) {
        return new Promise((resolve, reject) => {
          this.readingMediaInfo = true;
          this.getMediaInfo(file)
            .then(mediaInfo => {
              this.mediaInfoData = formatMediaInfo(mediaInfo, this.file);
              this.width = this.mediaInfoData.video.Width;
              this.height = this.mediaInfoData.video.Height;
              this.duration = this.mediaInfoData.video.Duration;
              resolve();
            })
            .catch(error => {
              reject(error);
            })
            .finally(() => {
              this.readingMediaInfo = false;
            })
          ;
        });
      },

      /**
       * Initialize s3 multipart upload process.
       */
      upload () {
        return this.startUpload().then(
           (awsS3ObjectKey) => {
             this.storePlatformEntity(awsS3ObjectKey)
          },
          () => {
            //retry if token expired or unrecoverable upload error occurred.
            if (this.retries < this.maxRetries && !this.uploadStats.cancelled) {
              this.pauseUpload();
              this.upload();
              this.retries++;
            } else {
              this.skipDuplicateCheck = false;
            }
          }
        );
      },

      /**
       * Store uploaded video as platform entity
       */
      storePlatformEntity (awsObjectKey) {
        this.store({
          campaignId: this.campaignId,
          s3FileKey: awsObjectKey
        }).then((r) => {
          this.$notify.success({
            title: this.$trans('messages.success'),
            message: this.$trans('creative::messages.success_title')
          });

          this.$emit('finish', r.data.data);

        }).catch((e) => {
          this.$notify.error({
            title: this.$trans('labels.error') + '!',
            message: this.$trans('creative::messages.validation_error')
          });
          this.handleBeErrors(e.data.errors)
        }).finally(() => {
          localStorage.removeItem('awsUploads');
          this.skipDuplicateCheck = false;
        });
      },

      /**
       * Process and prevalidation for uploaded file
       */
      processFile (file) {
        return new Promise((resolve, reject) => {
          let reader = document.createElement('video');
          reader.onloadedmetadata = async () => {
            this.fillReader(reader);
            resolve();
          };
          reader.onerror = reject;
          reader.src = URL.createObjectURL(file);
        })
      },

      /**
       * Proceed upload with ignoring duplicate check
       */
      async proceedUpload () {
        this.uploadPhase = UPLOAD_PHASE_CLEAR;
        this.uiState = UI_STATE_RELEASED;
        this.validated = true;
        this.skipDuplicateCheck = true;
        await this.initiateUploadPhase();
      },

      /**
       * Clear process variables on new upload
       */
      resetState() {
        this.uploadPhase = UPLOAD_PHASE_CLEAR;
        this.uiState = UI_STATE_RELEASED;
        this.validated = false;
        this.skipDuplicateCheck = false;
        this.mediaInfoData = {};
        this.clearUploadStats();
        this.clearFileData();
        this.clearAllErrors();
      },

      /**
       * Fill and validate vuex file instance before reading metadata
       */
      fillFile(file) {
        this.name = file.name;
        this.data = file;
        this.extension = this.formExtension(file.name);
        this.file_path = file.file_path;
        this.file_size = file.size;
      },

      /**
       * Fill and validate vuex file instance after reading metadata
       */
      fillReader(reader) {
        this.duration = reader.duration;
        this.width = reader.videoWidth;
        this.height = reader.videoHeight;
      },

      /**
       * @return {string} file extension
       */
      formExtension(name) {
        const parts = name.split('.');
        return parts[parts.length - 1].toLowerCase();
      },

      /**
       * Handle rejected creative (check possibility to save errors on BE)
       */
      async handleRejectedCreative () {
        try {
          if (
            this.file.extension &&
            this.file.duration &&
            this.file.width &&
            this.file.height &&
            this.file.fileSize &&
            this.file.hashSum
          ) {
            return await this.$call(this.storeRejected({
              'file': this.file,
              'rawData': this.mediaInfoData,
              'errors': this.formErrors,
              campaign_id: this.campaignId
            }), { notify: false });
          } else {
            this.$logDebug('Rejected creative cannot be stored in DB', this.mediaInfoData);
          }
        } catch (error) {
          return error;
        }
      },

      /**
       * Track validation errors.
       */
      trackErrors () {
        this.$trackError('Creative wasn\'t uploaded', flatMap(this.formErrors));
      },

      /**
       * Reset progress bar state.
       */
      resetProgress () {
        this.$refs.uploader.resetProgress(this.uploadStats);
      },

      /**
       * Update progress bar state.
       * @param {number} percent
       */
      updateProgress: throttle(function (percent) {
        this.uploadStats.percent = percent;
      }, MIN_INTERVAL_BETWEEN_PROGRESS_UPDATES),

      /**
       * Abort upload process from UI.
       */
      abortProcess () {
        if (this.uploadPhase === UPLOAD_PHASE_UPLOAD) {
          this.abortUpload();
        }
        this.uiState = UI_STATE_ABORTED;
        this.uploadPhase = UPLOAD_PHASE_CLEAR;
      },

      /**
       * Pause upload process during user confirmation.
       */
      pauseProcess () {
        this.uiState = UI_STATE_PAUSED;
        if (this.uploadPhase === UPLOAD_PHASE_UPLOAD) {
          this.pauseUpload();
        }
      },

      /**
       * Resume upload process after user denial of aborting process.
       */
      resumeProcess () {
        this.uiState = UI_STATE_RELEASED;
        this.progressPaused = false;

        if (
          this.uploadPhase === UPLOAD_PHASE_VALIDATION &&
          this.validated
        ) {
          this.initiateUploadPhase();
          return;
        }

        if (this.uploadPhase === UPLOAD_PHASE_UPLOAD) {
          this.resumeUpload();
        }
      },
    },
  }
</script>
