export default class CreativeMapper {
  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapObjectToApi (data) {
    return {
      s3Key: data.s3FileKey,
      name: data.file.name,
      hashSum: data.file.hashSum,
      campaign_id: data.campaignId,
    };
  }

  /**
   * Returns object that should be used in Application.
   *
   * @param data
   * @returns {{}}
   */
  static mapApiToObject (data) {
    return {
      id: data.id,
      name: data.name,
      file_size: data.filesize,
      duration: data.duration,
      extension: data.extension,
      preview_url: data.preview_url,
      poster_key: data.poster_key
    };
  }
}
