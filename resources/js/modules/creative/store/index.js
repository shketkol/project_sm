import Vue from 'vue';
import Vuex from 'vuex';
import creative from './modules/creative'
import notifications from "Modules/notification/store/modules/notifications";

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    creative,
    notifications,
  },
});

export default store;
