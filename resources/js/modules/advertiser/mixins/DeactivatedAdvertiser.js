import ModalDeactivated from 'Modules/advertiser/components/common/modals/ModalDeactivated';

export default {
  /**
   * Available properties.
   */
  props: {
    /**
     * True if deactivated modal should be visible by default.
     */
    modalDeactivatedVisible: {
      type: Boolean,
      default: false,
    },
  },

  /**
   * Child components.
   */
  components: {
    ModalDeactivated,
  },

  /**
   * Reactive data.
   *
   * @returns {{modalDeactivatedOpened: boolean}}
   */
  data () {
    return {
      modalDeactivatedOpened: this.modalDeactivatedVisible,
      showModalDeactivatedAdvertiser: false,
    };
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Returns true if advertiser is deactivated.
     *
     * @returns {*}
     */
    isAdvertiserDeactivated () {
      return this.$user.states.is_deactivated;
    },
  },

  /**
   * Available methods.
   */
  methods: {
    /**
     * Display modal.
     */
    openModalDeactivated () {
      this.modalDeactivatedOpened = true;
    },

    /**
     * Display Pending Modal.
     */
    openModalAdvertiserDeactivated () {
      this.showModalDeactivatedAdvertiser = true;
    },

    /**
     * This method is only a helper to keep logic of opening the modal inside this mixin.
     *
     * @param callback
     */
    ifAdvertiserActivated (callback) {
      if (this.orderCampaignStep && this.isAdvertiserDeactivated) {
        this.openModalAdvertiserDeactivated();
      } else if (this.isAdvertiserDeactivated) {
        this.openModalDeactivated();
      } else {
        return callback();
      }

      return Promise.resolve(false);
    },
  },
};
