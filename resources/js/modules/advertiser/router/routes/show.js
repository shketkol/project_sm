import CampaignsTab from '../../components/show/tabs/CampaignsTab';
import CreativesTab from '../../components/show/tabs/CreativesTab';
import TransactionsTab from '../../components/show/tabs/TransactionsTab';
import CampaignDetails from 'Modules/campaign/components/CampaignDetails'
import AdvertiserShow from "Modules/advertiser/components/show/AdvertiserShow";

export default [
  {
    path: '/users/:userId',
    component: AdvertiserShow,
    children: [
      {
        path: 'ads',
        component: CreativesTab,
        name: 'show.creatives',
      },
      {
        path: 'transactions',
        component: TransactionsTab,
        name: 'show.transactions',
      },
      {
        path: '',
        component: CampaignsTab,
        name: 'show.campaigns',
      },
    ],
  },
  {
    path: '/users/:userId/campaigns/:campaignId',
    name: 'show.campaigns.show',
    component: CampaignDetails,
  },
]
