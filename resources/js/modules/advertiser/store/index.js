import creative from 'Modules/creative/store/modules/creative';
import details from 'Modules/campaign/store/modules/details/';
import metrics from 'Modules/campaign/store/modules/metrics/';
import payment from 'Modules/payment/store/modules/payment';
import show from './modules/show/';
import Vue from 'vue';
import Vuex from 'vuex';
import notifications from "Modules/notification/store/modules/notifications";

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    creative,
    details,
    metrics,
    show,
    notifications,
    payment,
  },
});

export default store;
