import AdvertiserMapper from '../../../api/mappers/AdvertiserMapper';
import api from '../../../api';
import storeHelper from 'Foundation/helpers/store';
import {getField} from "vuex-map-fields";

/**
 * Types
 */
const types = {
  LOAD_ADVERTISER: storeHelper.createAsyncMutation('LOAD_ADVERTISER'),
  UPDATE_STATUS: storeHelper.createAsyncMutation('UPDATE_STATUS'),
  CHANGE_ACCOUNT_TYPE: storeHelper.createAsyncMutation('CHANGE_ACCOUNT_TYPE'),
};

/**
 * State.
 * @type {{advertiser: {}}}
 */
const state = {
  /**
   * Advertiser info.
   */
  advertiser: null,

  /**
   * Create state for mutation.
   */
  ...storeHelper.createMutationState([
    types.LOAD_ADVERTISER,
    types.UPDATE_STATUS,
  ]),

  /**
   * Pending update account type.
   */
  changeAccountTypePending: false
};

const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Load advertiser data from the server.
   */
  async load ({ commit }, id) {
    commit(types.LOAD_ADVERTISER.PENDING);

    try {
      const response = await api.show(id);
      commit(types.LOAD_ADVERTISER.SUCCESS, AdvertiserMapper.mapApiToObject(response));
      return Promise.resolve(response);
    } catch ({ response }) {
      commit(types.LOAD_ADVERTISER.FAILURE, response);
      return Promise.reject(response);
    }
  },

  /**
   * Deactivate the advertiser.
   *
   * @param commit
   * @param state
   * @returns {Promise<*>}
   */
  async deactivate ({ commit, state }) {
    commit(types.UPDATE_STATUS.PENDING);

    try {
      const response = await api.deactivate(state.advertiser.id);
      commit(types.UPDATE_STATUS.SUCCESS, AdvertiserMapper.mapApiToObject(response));
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_STATUS.FAILURE, error);
      return Promise.reject(error);
    }
  },

  /**
   * Activate the advertiser.
   *
   * @param commit
   * @param state
   * @returns {Promise<*>}
   */
  async activate ({ commit, state }) {
    commit(types.UPDATE_STATUS.PENDING);

    try {
      const response = await api.activate(state.advertiser.id);
      commit(types.UPDATE_STATUS.SUCCESS, AdvertiserMapper.mapApiToObject(response));
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_STATUS.FAILURE, error);
      return Promise.reject(error.response);
    }
  },

  /**
   * Change account type.
   *
   * @param state, payload
   */
  changeAccountType: storeHelper.createAction(types.CHANGE_ACCOUNT_TYPE, {
    call: api.changeAccountType,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Load advertiser mutation.
   */
  ...storeHelper.createMutation(types.LOAD_ADVERTISER, {
    success (state, payload) {
      state.advertiser = payload;
    },
    failure (state, { data }) {
      state[types.LOAD_ADVERTISER.ERROR] = data.message;
    },
  }),

  /**
   * Update status mutation.
   */
  ...storeHelper.createMutation(types.UPDATE_STATUS, {
    success (state, payload) {
      state.advertiser.status = payload.status || state.advertiser.status;
      state.advertiser.states = payload.states || state.advertiser.states;
    },
  }),

  /**
   * Change account type mutation.
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.CHANGE_ACCOUNT_TYPE, {
    success (state, payload) {
      state.advertiser.specialAccountType = payload.data.specialAccountType;
    },
  }),
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};


