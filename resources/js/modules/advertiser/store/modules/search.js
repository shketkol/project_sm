import storeHelper from 'Foundation/helpers/store';
import api from '../../api';
import {getField} from "vuex-map-fields";

/**
 * Types
 */
const types = {
  SEARCH_ADVERTISERS: storeHelper.createAsyncMutation('SEARCH_ADVERTISER'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Search loading.
   */
  [types.SEARCH_ADVERTISERS.loadingKey]: null,

  /**
   * Advertisers list
   */
  options: [],
};

const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Search advertisers
   *
   * @param state, orderId
   */
  searchAdvertisers: storeHelper.createAction(types.SEARCH_ADVERTISERS, {
    call (needle) {
      return api.search(needle);
    },
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  ...storeHelper.createMutation(types.SEARCH_ADVERTISERS, {
    success(state, { data }) {
      state.options = data;
    },
  }),
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
  getters,
};


