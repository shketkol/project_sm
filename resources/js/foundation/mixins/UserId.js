export default {
  /**
   * Available properties.
   */
  props: {
    /**
     * Show data table for chosen user
     */
    userId: {
      type: Number,
      default: null,
    },
  },
}
