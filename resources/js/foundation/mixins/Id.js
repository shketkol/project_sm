export default {
  /**
   * Available properties.
   */
  props: {
    /**
     * Component resource id.
     */
    id: {
      type: Number,
      default: null,
    },
  },
}
