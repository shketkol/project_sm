import StatusBadge from 'Foundation/components/badges/StatusBadge';

export default {
  /**
   * Child components
   */
  components: { StatusBadge },

  /**
   * Props
   */
  props: {
    /**
     * Status value.
     */
    cellValue: {
      type: String,
      default: '',
    },
  },

  /**
   * Component data.
   */
  data () {
    return {
      /**
       * Cell type mapping.
       * Must be overwritten in the specific cell-component
       */
      typeStatusMapping: {
        'default': [],
        'success' : [],
        'info': [],
        'warning': [],
        'danger': [],
      }
    }
  },

  /**
   * Computed props.
   */
  computed: {
    /**
     * Cell status class.
     * @return {string}
     */
    cellClass () {
      return `is-${this.formattedCellValue}`;
    },

    /**
     * Format cell value.
     * @return {string}
     */
    formattedCellValue () {
      if (!this.cellValue) {
        return '';
      }

      return this.cellValue.toLowerCase().replace(' ', '_');
    },

    /**
     * Returns status-badge type.
     * @return {string}
     */
    type () {
      for (let type in this.typeStatusMapping) {
        if (this.typeStatusMapping[type].includes(this.formattedCellValue)) {
          return type;
        }
      }
      return 'primary';
    }
  },
}
