export default {
  /**
   * Available properties.
   */
  props: {
    /**
     * True if component should display loading state.
     */
    loading: {
      type: Boolean,
      default () {
        return false;
      },
    },
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * True if loading. This property can be overwritten in a component.
     * @returns {*}
     */
    isLoading () {
      return this.loading;
    },
  },

  /**
   * Watchers.
   */
  watch: {
    /**
     * Loading.
     */
    isLoading: {
      immediate: true,
      handler (loading) {
        if (loading) {
          this._loading = this.$loading();
        } else {
          this._loading && this._loading.close();
        }
      },
    },
  },
}
