export default {
  /**
   * Available properties.
   */
  props: {
    /**
     * True if component should be visible.
     */
    visible: {
      type: Boolean,
      default () {
        return false;
      },
    },
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Close the form.
     */
    close () {
      this.$emit('update:visible', false);
      this.emitClose();
    },

    /**
     * Fire close event.
     */
    emitClose () {
      this.$emit('close');
    },
  },
}
