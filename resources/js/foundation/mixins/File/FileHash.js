import SparkMD5 from 'spark-md5';

/**
 * Calculate percent.
 * @param {number} totalChunks
 * @param {number} currentChunk
 * @returns {number}
 */
const calculatePercent = (totalChunks, currentChunk) => {
  return Math.round(currentChunk / totalChunks * 100);
};

/**
 * Fire progress callback if possible.
 * @param {function} progressCb
 * @param {number} totalChunks
 * @param {number} currentChunk
 * @returns {void}
 */
const handleProgress = (progressCb, totalChunks, currentChunk) => {
  if (typeof progressCb === 'function') {
    progressCb(calculatePercent(totalChunks, currentChunk));
  }
};

export default {
  /**
   * Methods.
   */
  methods: {
    /**
     * Generate hash for uploaded file (by file content).
     * @param {File} file
     * @param {function} progressCb
     * @returns {Promise<any>}
     */
    getFileHash (file, progressCb) {
      return new Promise((resolve, reject) => {
        const
          blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice,
          // Read in chunks of 2MB.
          chunkSize = 2097152,
          chunks = Math.ceil(file.size / chunkSize),
          spark = new SparkMD5.ArrayBuffer(),
          fileReader = new FileReader();
        let currentChunk = 0;

        fileReader.onload = function (e) {
          // Append array buffer.
          spark.append(e.target.result);
          currentChunk++;

          if (currentChunk < chunks) {
            handleProgress(progressCb, chunks, currentChunk);
            loadNext();
          } else {
            handleProgress(progressCb, 1, 1);
            resolve(spark.end());
          }
        };

        fileReader.onerror = reject;

        /**
         * Process file chunk.
         */
        function loadNext() {
          const
            start = currentChunk * chunkSize,
            end = ((start + chunkSize) >= file.size) ? file.size : start + chunkSize;

          fileReader.readAsArrayBuffer(blobSlice.call(file, start, end));
        }

        loadNext();
      });
    },
  },
}
