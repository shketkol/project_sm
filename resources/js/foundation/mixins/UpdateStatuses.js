import {mapActions, mapState} from 'vuex';
import map from 'lodash/map';

export default {
  /**
   * Data attributes
   */
  data () {
    return {
      /**
       * Interval timer Id.
       */
      checkStatusesTimerId: null,
    };
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Map state.
     */
    ...mapState('checkStatuses', ['rowsStatuses']),
  },

  /**
   * Mounted
   */
  mounted () {
    this.checkStatusesTimerId = setInterval(this.checkStatuses, this.$config.datatables.check_statuses_interval);

  },

  /**
   * Before destroy actions.
   */
  beforeDestroy () {
    clearInterval(this.checkStatusesTimerId);
  },

  /**
   * Watchers.
   */
  watch: {
    /**
     * Update rows for entities with updated statuses.
     * @param {Array} rows
     */
    rowsStatuses (rows) {
      rows.forEach((row) => this.updateRow(row));
    },
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Map actions.
     */
    ...mapActions('checkStatuses', {
      checkStatusesAction: 'checkStatuses'
    }),

    /**
     * Check for entities statuses updates.
     */
    checkStatuses () {
      const ids = this.getPendingIds();
      if (ids.length) {
        this.$call(this.checkStatusesAction({
          table: this.tableName,
          ids:ids
        }));
      }
    },

    /**
     * Get pending entities ids.
     * @return {Array}
     */
    getPendingIds () {
      if (this.$refs.dataTable !== undefined) {
        const rows = this.$refs.dataTable.tableData.filter(row => (
          this.isPending(row)
        ));
        return map(rows, 'id');
      }

      return [];
    }
  }
}
