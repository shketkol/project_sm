import translations from 'Static/translations.json';
import Translator from './translator';

const translator = new Translator(translations, 'en');

export default translator;
