import Vue from 'vue';
import Client from './client';

Vue.use({
  /**
   * Install plugin.
   * @param Vue
   * @returns {*}
   */
  install (Vue) {
    Vue.mixin({
      /**
       * Created hook.
       */
      created () {
        this.$apiClient = new Client(this);
      },

      /**
       * Available methods.
       */
      methods: {
        /**
         * Send request to backend.
         *
         * @param action
         * @param params
         * @returns {Promise<any>}
         */
        $call (action, params) {
          return this.$apiClient.call(action, params);
        },
      },
    });
  },
});
