export default class BaseService {

  /**
   * @param {BaseEventFactory} eventFactory
   */
  constructor (eventFactory) {
    this.eventFactory = eventFactory;
    this.event = {};
  }

  /**
   * @param {EventData} eventData
   */
  setEvent (eventData) {
    this.event = this.eventFactory.getEvent(eventData);
  }

  /**
   * Send event.
   */
  handleEvent () {
    if (this.isEnabled()) {
      this.event.send();
    }
  }

  /**
   * Verify if service is enabled.
   * @returns {boolean}
   */
  isEnabled () {
    return false;
  }
}
