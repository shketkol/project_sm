export default class BaseEventFactory {
  /**
   * @return {BaseEvent}
   */
  getEvent () {
    throw new Error('getEvent() must be implemented by the subclass');
  }
}
