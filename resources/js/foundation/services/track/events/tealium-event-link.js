import TealiumEvent from './tealium-event';
import {MAP_TYPE_LINK} from 'Foundation/services/track/mappers/tealium-mapper';

export default class TealiumEventLink extends TealiumEvent {
  /**
   * Send event-data.
   */
  sendEvent (payload) {
    window.utag.link(payload);
  }

  /**
   * @inheritdoc
   */
  getMapType () {
    return MAP_TYPE_LINK;
  }
}
