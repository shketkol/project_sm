export default class EventData {
  /**
   * @param {string} eventType
   * @param {(string|null)} eventName
   * @param {(object|null)} route
   * @param {object} payload
   */
  constructor (eventType, eventName = null, route = null, payload) {
    this.eventType = eventType;
    this.eventName = eventName;
    this.route = route;
    this.payload = payload;
  }
}
