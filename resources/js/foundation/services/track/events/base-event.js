import { merge, isEmpty } from 'lodash';

export const EVENT_TYPE_VIEW = 'view';
export const EVENT_TYPE_ACTION = 'action';
export const EVENT_TYPE_ERROR = 'error';

export default class BaseEvent {

  constructor (eventData) {
    this.eventName = eventData.eventName;
    this.customPayload = eventData.payload;
    this.route = eventData.route;
  }

  /**
   * Get mapper for adjusting the payload.
   * @abstract
   * @return {BaseMapper}
   */
  getMapper () {
    throw new Error('getMapper() must be implemented by the subclass');
  }

  /**
   * Get type of map. Should be used for map adjustments.
   * @returns {(null|string)}
   */
  getMapType () {
    return null;
  }

  /**
   * Get the payload to be sent.
   * @return {object}
   */
  getPayload () {
    return this.adjustPayload(this.getRawPayload());
  }

  /**
   * Adjust payload by mapping the keys and applying the callbacks where needed.
   * @param rawPayload
   * @returns {object}
   */
  adjustPayload (rawPayload) {
    return this.getMapper().adjustPayload(rawPayload);
  }

  /**
   * Get payload that wasn't mapped.
   * @returns {*}
   */
  getRawPayload () {
    return merge(
      this.getDefaultPayload(),
      this.customPayload
    );
  }

  /**
   * Get default payload that is used for all events.
   * @return {object}
   */
  getDefaultPayload () {
    const userPayload = this.isUserAuthorized() ? this.getUserPayload() : {};
    return merge(
      this.getBasicPayload(),
      userPayload
    );
  }

  /**
   * Get the basic payload that is used for all events.
   * @return {object}
   */
  getBasicPayload () {
    return {};
  }

  /**
   * Get the data that is related to the authorized user.
   * @return {object}
   */
  getUserPayload () {
    return {}
  }

  /**
   * Check if current user is logged in.
   * @return {boolean}
   */
  isUserAuthorized () {
    return !isEmpty(window.userConfig);
  }

  /**
   * Get tealium event type.
   * @return {string}
   */
  getEventName () {
    return this.eventName;
  }

  /**
   * Send event-data.
   * @abstract
   */
  send () {
    throw new Error('send() must be implemented by the subclass');
  }
}
