import TealiumEvent from './tealium-event';
import {MAP_TYPE_LINK} from 'Foundation/services/track/mappers/tealium-mapper';

export default class TealiumEventError extends TealiumEvent {

  constructor (eventData) {
    super(eventData);
    this.eventName = 'error';
  }

  /**
   * @inheritdoc
   */
  getMapType () {
    return MAP_TYPE_LINK;
  }

  /**
   * Send event-data.
   */
  sendEvent (payload) {
    window.utag.link(payload);
  }
}
