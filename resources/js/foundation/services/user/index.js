import Vue from 'vue';

Vue.use({
  /**
   * Install plugin.
   * @param Vue
   * @returns {*}
   */
  install (Vue) {
    Vue.mixin({
      /**
       * Computed properties.
       */
      computed: {
        /**
         * User configs (passed from backend).
         *
         * @returns {*}
         */
        $user () {
          return window.userConfig;
        },

        /**
         * Returns true if user is authorized (not guest).
         * @returns {boolean}
         */
        $authorized () {
          return !(this.$user instanceof Array);
        },
      },

      /**
       * Methods.
       */
      methods: {
        /**
         * Returns true if currently authenticated user has the permission.
         *
         * @param permission
         * @returns {*}
         */
        $hasPermission (permission) {
          return this.$user.permissions.includes(permission);
        },
      },
    });
  },
});
