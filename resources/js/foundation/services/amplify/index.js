import Amplify from '@aws-amplify/core';
import Auth from '@aws-amplify/auth';

Amplify.configure({
  Auth: {
    region: window.config.cognito.region,
    userPoolId: window.config.cognito.user_pool_id,
    userPoolWebClientId: window.config.cognito.user_pool_web_client_id
  }
}, Auth);
