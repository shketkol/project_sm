import storeHelper from 'Foundation/helpers/store';
import api from '../api/check-statuses';

/**
 * Types
 */
const types = {
  CHECK_STATUSES: storeHelper.createAsyncMutation('CHECK_STATUSES'),
};

/**
 * State
 */
const state = {
  /**
   * Create state for mutation.
   */
  ...storeHelper.createMutationState([
    types.CHECK_STATUSES,
  ]),

  rowsStatuses: []
};

/**
 * Actions
 */
const actions = {
  /**
   * Check reports statuses action.
   */
  checkStatuses: storeHelper.createAction(types.CHECK_STATUSES, {
    call (payload) {
      return api.checkStatus(payload);
    },
    after ({ commit }, { data }) {
      commit('updateStatuses', data);
    },
  }),
};

/**
 * Mutations
 */
const mutations = {
  /**
   * Update reports statuses.
   *
   * @param state
   * @param data
   */
  updateStatuses (state, data) {
    state.rowsStatuses = data;
  },

  /**
   * Load notification mutation.
   */
  ...storeHelper.createMutation(types.CHECK_STATUSES),
};


export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
};
