export default {

  /**
   * Methods
   */
  methods: {
    /**
     * Check existing filters
     */
    checkFilters() {
      if (localStorage[this.$parent.$parent.name]) {
        this.setInitialParam(JSON.parse(localStorage[this.$parent.$parent.name]));
      }
    }
  }
};
