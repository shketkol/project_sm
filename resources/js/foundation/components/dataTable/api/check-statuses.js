import axios from "axios";
import route from "Foundation/services/routing/route";

export default {
  /**
   * Check statuses of the provided entities.
   *
   * @param data {Array}
   * @returns {AxiosPromise<any>}
   */
  checkStatus (data) {
    return axios.post(
      route('dataTable.checkStatuses', {'table': data.table}), {
        processing_ids: data.ids
      });
  },
}
