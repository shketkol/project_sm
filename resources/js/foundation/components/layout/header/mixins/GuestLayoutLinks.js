export default {
  /**
   * Computed properties.
   */
  computed: {
    /**
     * How it works link.
     */
    howItWorksLink () {
      return this.$config.general.links.how_it_works
    },

    /**
     * Success_stories link.
     */
    successStoriesLink () {
      return this.$config.general.links.success_stories
    },

    /**
     * Resources link.
     */
    resourcesLink () {
      return this.$config.general.links.resources
    },

    /**
     * Insights link.
     */
    insightsLink () {
      return this.$config.general.links.insights
    },

    /**
     * Creative hub link.
     */
    creativeHubLink () {
      return this.$config.general.links.creative_hub
    },

    /**
     * Faq link.
     */
    faqLink () {
      return this.$config.general.links.faq.unauthorized
    },

    /**
     * Privacy policy link.
     */
    privacyPolicyLink () {
      return this.$config.general.links.privacy_policy
    },

    /**
     * Terms of use link.
     */
    termsOfUseLink () {
      return this.$config.general.links.terms_of_use
    },

    /**
     * Beta terms of use link.
     */
    betaTermsOfUseLink () {
      return this.$config.general.links.beta_terms_of_use
    }
  },
};
