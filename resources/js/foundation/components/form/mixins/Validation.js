import mapValues from 'lodash/mapValues';

export default {
  /**
   * Props.
   */
  props: {
    /**
     * Is extra validation rules enabled.
     */
    extraValidationEnabled: {
      type: Boolean,
      default: true,
    },
    /**
     * Validation scope.
     */
    scope: {
      type: String,
      default: '__global__',
    },
    /**
     * Validation rules.
     */
    validation: {
      type: [String, Object],
      default () {
        return {};
      },
    },

    /**
     * Custom validation messages for the specific field.
     * This property should be used when you need add some custom message only for the field.
     */
    validationMessages: {
      type: Object,
      default () {
        return {};
      },
    },

    /**
     * Override common vee-validate events.
     */
    validateOn: {
      type: String,
      default: "",
    },
  },
  /**
   * Computed properties.
   */
  computed: {
    /**
     * Returns text of first error from the error bag for current field.
     */
    errorText () {
      return this.error ? this.error.msg : '';
    },
    /**
     * Returns error from error bag.
     */
    error () {
      return this.errors.items.find(error => error.field === this.name && error.scope === this.scope);
    },
    /**
     * Returns extra validation rules.
     */
    extraValidation () {
      return {};
    },
    /**
     * Returns true if error bag has error for current field.
     */
    hasError () {
      return !!this.error;
    },
    /**
     * Returns true if required rule was set.
     */
    isRequired () {
      if (typeof this.validation === 'string') {
        return this.validation.includes('required');
      }

      return Object.prototype.hasOwnProperty.call(this.validation, 'required') ? this.validation.required : false;
    },
    /**
     * Returns pretty field name for validation.
     */
    validateAs () {
      return this.label.toLowerCase();
    },
    /**
     * Get the validation rules.
     */
    validationRules () {
      if (typeof this.validation === 'string') {
        return this.validation;
      }

      return this.extraValidationEnabled ? Object.assign({}, this.validation, this.extraValidation) : this.validation;
    }
  },

  /**
   * Created hook.
   */
  created () {
    this.$validator.localize({
      [this.$translator.locale]: {
        custom: {
          [this.name]: {
            ...this.validationMessages,
          },
        },
      },
    });
  },

  /**
   * Destroyed hook.
   */
  destroyed () {
    this.$validator.localize({
      [this.$translator.locale]: {
        custom: {
          [this.name]: {
            ...mapValues(this.validationMessages, () => (null)),
          },
        },
      },
    });
  },

  /**
   * Methods
   */
  methods: {
    /**
     * Add error to error bag
     * @param message
     */
    addError (message) {
      this.errors.add({
        field: this.name,
        msg: message,
        scope: this.scope,
      });
    },

    /**
     * Reset all errors
     */
    resetErrors() {
      this.errors.clear(this.scope);
    },

    /**
     * Reset all errors
     */
    resetFieldErrors() {
      this.errors.remove(this.name, this.scope);
    },
  },
};
