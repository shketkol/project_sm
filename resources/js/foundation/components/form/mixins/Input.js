export default {
  /**
   * Computed properties.
   */
  computed: {
    /**
     * Internal value
     */
    innerValue: {
      get: function () {
        return this.value;
      },
      set: function (value) {
        this.$emit('input', value);
      },
    },
  },

  /**
   * Watchers.
   */
  watch: {
    /**
     * Value is changed.
     */
    value () {
      this.$validator.errors.remove(this.name, this.scope);
    },
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Focus element.
     */
    focus () {
      const input = this.$refs.input;

      if (input) {
        input.focus();
      }
    },

    /**
     * On blur event listener.
     */
    onBlur () {
      this.$emit('blur');
    },

    /**
     * On focus event listener.
     */
    onFocus () {
      this.$emit('focus');
    },

    /**
     * On enter event listener.
     */
    onEnter () {
      this.$emit('enter');
    },

    /**
     * On input event listener.
     */
    onInput () {
      this.$emit('input-change');
    },
  },
};
