export default {
  /**
   * Computed properties.
   */
  computed: {
    /**
     * Icon component stub
     */
    iconComponent() {
      return null;
    },
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Stub icon click handler.
     */
    iconClick() {
    },
  },
};
