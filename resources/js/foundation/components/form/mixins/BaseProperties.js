export default {
  /**
   * Props.
   */
  props: {
    /**
     * True if a component should be disabled.
     */
    disabled: {
      type: Boolean,
      default: false,
    },
    /**
     * Native 'name' attribute.
     */
    name: {
      type: String,
      required: true,
    },
    /**
     * Initial value.
     */
    value: {
      type: [String, Number, Boolean, Object, Array],
      default: null,
    },
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * True if input is filled.
     *
     * @returns {boolean}
     */
    isFilled () {
      return !!this.value;
    },
  },
};
