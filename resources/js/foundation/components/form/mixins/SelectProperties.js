import BaseProperties from './BaseProperties';
import locale from './../../../services/translations/locale';

export default {
  /**
   * Mixins.
   */
  mixins: [
    BaseProperties,
  ],

  /**
   * Props.
   */
  props: {
    /**
     * Whether creating new items is allowed.
     * To use this, the property filterable must be TRUE.
     */
    allowCreate: {
      type: Boolean,
      default: false,
    },
    /**
     * Is select clearable.
     */
    clearable: {
      type: Boolean,
      default: false,
    },
    /**
     * In filterable select choose first option on enter click
     */
    defaultFirstOption: {
      type: Boolean,
      default: false,
    },
    /**
     * Is select filterable.
     */
    filterable: {
      type: Boolean,
      default: false,
    },
    /**
     * Custom filter search method works only when filterable property is true.
     */
    filterMethod: {
      type: Object,
      default: null,
    },
    /**
     * Label text.
     */
    label: {
      type: String,
      default: '',
    },
    /**
     * Is select loading data.
     */
    loading: {
      type: Boolean,
      default: false,
    },
    /**
     * Displayed text when data is loading.
     */
    loadingText: {
      type: String.code,
      default () {
        return locale.get('components.select.loading');
      },
    },
    /**
     * Displayed text when no data options.
     */
    noDataText: {
      type: String,
      default () {
        return locale.get('components.select.no_data');
      },
    },
    /**
     * Displayed text when no data matches the filtering query.
     */
    noMatchText: {
      type: String,
      default () {
        return locale.get('components.select.no_match');
      },
    },
    /**
     * Options for select.
     */
    options: {
      type: Array,
      default () {
        return [];
      },
    },
    /**
     * A placeholder of input.
     */
    placeholder: {
      type: String,
      default () {
        return locale.get('components.select.placeholder');
      },
    },
    /**
     * Custom class for select drop down.
     */
    popperClass: {
      type: String,
      default: null,
    },
    /**
     * Custom remote search method works only when filterable property is true.
     */
    remoteMethod: {
      type: Function,
      default: null,
    },
    /**
     * Size of select.
     */
    size: {
      type: String,
      default: '',
      validator (value) {
        return ['', 'large', 'medium', 'small', 'mini'].includes(value);
      },
    },
    /**
     * Index of select.
     */
    tabIndex: {
      type: Number,
      default: null,
    },
    /**
     * Is label shown.
     */
    showLabel: {
      type: Boolean,
      default: true,
    },
    /**
     * Unique identity key name for value, required when value is an object.
     */
    valueKey: {
      type: String,
      default: 'value',
    },
  },
};
