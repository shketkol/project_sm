import DateTimeFormat from 'Foundation/services/intl/DateTimeFormat';
import moment from "moment";

const dateTimeFormat = new DateTimeFormat();

/**
 * Add only business days to the date (exclude weekends).
 *
 * @param date
 * @param days
 * @returns {moment.Moment}
 */
const addBusinessDays = (date, days) => {
  return date.add(days, 'days');
};
/**
 * Format date into a common format to parse the date correctly on API.
 *
 * @param date
 * @param exactUtcTime
 * @returns {any}
 */
const formatToApi = (date, exactUtcTime = false) => {

  if (!date) {
    return undefined;
  }

  let copiedDate = (date instanceof Date) ? new Date(date.getTime()) : date;

  if (exactUtcTime) {
    return moment.utc(copiedDate).format();
  }

  return dateTimeFormat.formatDate(copiedDate);
};

export {
  addBusinessDays,
  formatToApi,
}
