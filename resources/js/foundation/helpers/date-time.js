import { parseDate } from 'element-ui/src/utils/date-util';

/**
 * Fetch date from Date/string objects using fecha.parse() method.
 *
 * @param {(Date|string|String)} date
 * @returns {(null|Date)}
 */
const fetchDate = (date) => {
  if (date instanceof Date) {
    return date;
  }

  if (typeof(date) === 'string' || date instanceof String) {
    return parseDate(date);
  }

  return null;
};

export {
  fetchDate,
}
