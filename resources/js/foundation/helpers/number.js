/**
 * Returns rounded (ceil) number with the specified precision.
 * @param number
 * @param precision
 * @returns {number}
 */
const ceil = (number, precision) => {
  return Math.ceil(number * Math.pow(10, precision)) / Math.pow(10, precision);
};

export {
  ceil,
}
