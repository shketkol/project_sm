import translator from 'Foundation/services/translations/locale';
import { Validator } from 'vee-validate';

Validator.extend('complete_date_range', {
  /**
   * Get validation message.
   * @returns {string}
   */
  getMessage () {
    return translator.get('validation.complete_date_range');
  },

  /**
   * Validate an attribute.
   *
   * @param value
   */
  validate (value) {
    return !!(Array.isArray(value) && value[0] && value[1]);
  },
});
