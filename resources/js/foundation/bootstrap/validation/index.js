import './dictionary';
import './rules';
import config from 'Config/plugins/veeValidate';
import VeeValidate from 'vee-validate';
import Vue from 'vue';
import translator from 'Foundation/services/translations/locale';

config.dictionary = {
  en: {
    attributes: translator.get('validation.attributes'),
  },
};

Vue.use(VeeValidate, config);
