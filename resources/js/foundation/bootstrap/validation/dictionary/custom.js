import translator from 'Foundation/services/translations/locale';

/**
 * Common "between" validation message for "duration" fields.
 * @param {string} field
 * @param {Array} params
 * @returns {string}
 */
const durationBetween = (field, params) => {
  let min = params[0];
  let max = params[1];
  return translator.get(
    'validation.custom.duration.between',
    {
      attribute: field,
      min: min,
      max: max
    }
  )
};

const passwordMessages = {
  regex (attribute) {
    return translator.get('validation.custom.password.regex', { attribute });
  },
  min (attribute, [min]) {
    return translator.get('validation.custom.password.min', { attribute, min });
  },
  required (attribute) {
    return translator.get('validation.custom.password.required', { attribute });
  },
  confirmed: translator.get('validation.custom.password.confirmed'),
};

const passwordConfirmationMessages = {
  required () {
    return translator.get('validation.custom.password_confirmation.required', {
      field: translator.get('validation.attributes.password'),
    });
  },
  confirmed () {
    return translator.get('validation.custom.password.confirmed');
  },
};

export default {
  first_name: {
    required: translator.get('validation.custom.first_name.required'),
    max (field, [max]) {
      return translator.get('validation.custom.first_name.max', { max });
    },
  },
  last_name: {
    required: translator.get('validation.custom.last_name.required'),
    max (field, [max]) {
      return translator.get('validation.custom.last_name.max', { max });
    },
  },
  email: {
    email () {
      return translator.get('validation.custom.email.email');
    },
  },
  phone: {
    required: translator.get('validation.custom.phone.required'),
    min () {
      return translator.get('validation.custom.phone.min');
    },
  },
  addressLine1: {
    required: translator.get('validation.custom.company_address.line1.required'),
    max (field, [max]) {
      return translator.get('validation.custom.company_address.line1.max', { max });
    },
  },
  addressLine2: {
    required: translator.get('validation.custom.company_address.line2.required'),
    max (field, [max]) {
      return translator.get('validation.custom.company_address.line2.max', { max });
    },
  },
  city: {
    required: translator.get('validation.custom.company_address.city.required'),
    max (field, [max]) {
      return translator.get('validation.custom.company_address.city.max', { max });
    },
  },
  zip: {
    required: translator.get('validation.custom.company_address.zip.required'),
    regex: translator.get('validation.custom.company_address.zip.regex'),
    max: translator.get('validation.custom.company_address.zip.max'),
  },
  state: {
    required: translator.get('validation.custom.company_address.state.required'),
  },
  password: passwordMessages,
  new_password: passwordMessages,
  password_confirmation: passwordConfirmationMessages,
  new_password_confirmation: passwordConfirmationMessages,
  width: {
    min_value: (field, params) => {
      let string = params[0];
      return translator.get(
        'validation.custom.width.min',
        {
          attribute: translator.get('validation.attributes.width'),
          min: string,
        }
      )
    },
  },
  height: {
    min_value: (field, params) => {
      let string = params[0];
      return translator.get(
        'validation.custom.height.min',
        {
          attribute: translator.get('validation.attributes.height'),
          min: string,
        }
      )
    },
  },
  ['creative.general.duration']: {
    between: durationBetween
  },
  ['creative.audio.duration']: {
    between: durationBetween
  },
  ['creative.audio.channels']: {
    included (field) {
      return translator.get(
        'validation.custom.audios_channel_included',
        {
          attribute: field,
        }
      );
    }
  },
  file_size : {
    max_value: (field, params) => {
      let string = params[0];
      return translator.get(
        'validation.custom.file_size.max',
        {
          attribute: translator.get('validation.attributes.file_size'),
          max: string,
        }
      )
    }
  },
};
