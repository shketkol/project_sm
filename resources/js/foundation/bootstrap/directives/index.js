import Vue from 'vue';

const directives = {
  install(Vue) {
    /**
     * Handle click outside element on witch directive is assigned.
     */
    Vue.directive('click-outside-directive', {
      /**
       * On bind.
       */
      bind(element, binding, vnode) {
        element.clickHandler = event => {
          if (!(element === event.target || element.contains(event.target))) {
            vnode.context[binding.expression](event);
          }
        };
        document.body.addEventListener('click', element.clickHandler);
      },

      /**
       * On unbind.
       */
      unbind(element) {
        document.body.removeEventListener('click', element.clickHandler);
      },
    });
  }
};

Vue.use(directives);
