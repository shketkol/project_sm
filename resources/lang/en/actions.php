<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Action Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to define actions.
    | e.g. save, next, apply.
    |
    */

    'add_new'             => 'Add New',
    'back'                => 'Back',
    'book_new_campaign'   => 'Book New Campaign',
    'submit_new_campaign' => 'Submit New Campaign',
    'cancel'              => 'Cancel',
    'change'              => 'Change',
    'check'               => 'Check',
    'check_inventory'     => 'Check Inventory',
    'close'               => 'Close',
    'create_campaign'     => 'Create Campaign',
    'confirm'             => 'Confirm',
    'delete'              => 'Delete',
    'done'                => 'Done',
    'edit'                => 'Edit',
    'got_it'              => 'Got It',
    'finished'            => 'Finished',
    'help'                => 'Help',
    'log_in'              => 'Log In',
    'next'                => 'Next',
    'no'                  => 'No',
    'pause'               => 'Pause',
    'place_order'         => 'Submit',
    'remove'              => 'Remove',
    'review_account'      => 'Review Account',
    'save'                => 'Save',
    'save_changes'        => 'Save Changes',
    'select_file'         => 'Select File',
    'set_new_password'    => 'Set New Password',
    'set_password'        => 'Set password',
    'sign_up'             => 'Sign Up',
    'sign_up_today'       => 'Sign Up Today',
    'submit'              => 'Submit',
    'submit_campaign'     => 'Submit Campaign',
    'update'              => 'Update',
    'yes'                 => 'Yes',
    'clear_all'           => 'Clear All',
];
