<?php

use App\Services\ValidationRulesService\ValidationRules;
use Carbon\Carbon;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;

$creativeRequiredMessage = 'It looks like your Ad is missing :attribute information.';
$creativeAudioRequiredMessage = 'Make sure your Ad has :attribute present in each audio channel.';

$affectedAdvertisersStartDate = Carbon::now()
    ->subMonths(config('activities.database.store_months'))
    ->format(config('date.format.php'));

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'                => 'The :attribute must be accepted.',
    'active_url'              => 'The :attribute is not a valid URL.',
    'after'                   => 'The :attribute must be a date after :date.',
    'after_or_equal'          => 'The :attribute must be a date after or equal to :date.',
    'alpha'                   => 'The :attribute may only contain letters.',
    'alpha_dash'              => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num'               => 'The :attribute may only contain letters and numbers.',
    'array'                   => 'The :attribute must be an array.',
    'before'                  => 'The :attribute must be a date before :date.',
    'before_or_equal'         => 'The :attribute must be a date before or equal to :date.',
    'between'                 => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'                 => 'The :attribute field must be true or false.',
    'complete_date_range'     => 'Select the date range.',
    'confirmed'               => 'The :attribute confirmation does not match.',
    'date'                    => 'The :attribute is not a valid date.',
    'date_equals'             => 'The :attribute must be a date equal to :date.',
    'date_format'             => 'The :attribute does not match the format :format.',
    'different'               => 'The :attribute and :other must be different.',
    'digits'                  => 'The :attribute must be :digits digits.',
    'digits_between'          => 'The :attribute must be between :min and :max digits.',
    'dimensions'              => 'The :attribute has invalid image dimensions.',
    'distinct'                => 'The :attribute field has a duplicate value.',
    'email'                   => 'The :attribute must be a valid email address.',
    'exists'                  => 'The selected :attribute is invalid.',
    'file'                    => 'The :attribute must be a file.',
    'filled'                  => 'The :attribute field must have a value.',
    'gt'                      => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file'    => 'The :attribute must be greater than :value kilobytes.',
        'string'  => 'The :attribute must be greater than :value characters.',
        'array'   => 'The :attribute must have more than :value items.',
    ],
    'gte'                     => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file'    => 'The :attribute must be greater than or equal :value kilobytes.',
        'string'  => 'The :attribute must be greater than or equal :value characters.',
        'array'   => 'The :attribute must have :value items or more.',
    ],
    'image'                   => 'The :attribute must be an image.',
    'in'                      => 'The selected :attribute is invalid.',
    'in_array'                => 'The :attribute field does not exist in :other.',
    'indexed_array'           => 'The :attribute field must be an indexed array.',
    'integer'                 => 'The :attribute must be an integer.',
    'ip'                      => 'The :attribute must be a valid IP address.',
    'ipv4'                    => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                    => 'The :attribute must be a valid IPv6 address.',
    'json'                    => 'The :attribute must be a valid JSON string.',
    'lt'                      => [
        'numeric' => 'The :attribute must be less than :value.',
        'file'    => 'The :attribute must be less than :value kilobytes.',
        'string'  => 'The :attribute must be less than :value characters.',
        'array'   => 'The :attribute must have less than :value items.',
    ],
    'lte'                     => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file'    => 'The :attribute must be less than or equal :value kilobytes.',
        'string'  => 'The :attribute must be less than or equal :value characters.',
        'array'   => 'The :attribute must not have more than :value items.',
    ],
    'max'                     => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute is too long (:max characters max).',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'max_duration'            => 'Campaigns can run up to :value :type.',
    'mimes'                   => 'The :attribute must be a file of type: :values.',
    'mimetypes'               => 'The :attribute must be a file of type: :values.',
    'min'                     => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'month'                   => 'month',
    'months'                  => 'months',
    'not_in'                  => 'The selected :attribute is invalid.',
    'not_regex'               => 'The :attribute format is invalid.',
    'numeric'                 => 'The :attribute must be a number.',
    'present'                 => 'The :attribute field must be present.',
    'regex'                   => 'The :attribute format is invalid.',
    'required'                => 'Enter the :attribute.',
    'required_your'           => 'Enter your :attribute.',
    'required_if'             => 'The :attribute field is required when :other is :value.',
    'required_unless'         => 'The :attribute field is required unless :other is in :values.',
    'required_with'           => 'The :attribute field is required when :values is present.',
    'required_with_all'       => 'The :attribute field is required when :values are present.',
    'required_without'        => 'The :attribute field is required when :values is not present.',
    'required_without_all'    => 'The :attribute field is required when none of :values are present.',
    'same'                    => 'The :attribute and :other must match.',
    'size'                    => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'starts_with'             => 'The :attribute must start with one of the following: :values',
    'string'                  => 'The :attribute must be a string.',
    'targeting_to_account'    => 'This account is not authorized to manage targetings.',
    'timezone'                => 'The :attribute must be a valid zone.',
    'unique'                  => 'The :attribute has already been taken.',
    'uploaded'                => 'The :attribute failed to upload.',
    'url'                     => 'The :attribute format is invalid.',
    'uuid'                    => 'The :attribute must be a valid UUID.',
    'included'                => 'Make sure the :attribute of your Ad is one of the following: :values.',
    'included_with_dimension' => 'Make sure the :attribute of your Ad is one of the following: :values :dimension.',
    'invalid'                 => 'The given data was invalid.',
    'unauthorized'            => 'Cannot authorize user for this request.',
    'max_filesize'            => 'Your file might be a bit large - please make sure it is under :format',
    'max_filesize_creative'   => 'Make sure the file size of your Ad is under the :format limit.',
    'min_video'               => 'Make sure the :attribute of your Ad is at least :min :dimension.',
    'old_password'            => 'Double-check your password and try again.',
    'formatted_min_value'     => 'Value for :field should be greater than :value',
    'formatted_max_value'     => 'Value for :field should be less than :value',
    'formatted_decimal'       => 'Value for :field should be a number',
    'unique_creative'         => 'This Ad already exists in your gallery. Please check <a href=":href" target="_blank">:name</a> in your Ad gallery. You can remove it or reuse in another campaign.',
    'unique_invite_email'     => 'This email already exists in your your invite list.',
    'unique_invite_code'      => 'This code already exists in your your invite list.',
    'payment_method'          => 'Please choose payment method',
    'valid_email'             => 'Email not valid. References through "+" allowed only for gmail.com',
    'valid_invite_code'       => 'Your invite code is invalid or expired.',
    'required_emails'         => 'Enter one or more email addresses.',
    'max_tag'                 => 'Field can not contain more then :max values',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'invites_csv'             => [
            'required' => 'Uploaded CSV file is empty. Please make sure the file contains email addresses separated by commas.',
        ],
        'type'                    => [
            'included' => ':attribute must have valid format',
        ],
        'attribute-name'          => [
            'rule-name' => 'custom-message',
        ],
        'password'                => [
            'regex'     => 'Your :attribute must include at least 1 lowercase character, 1 uppercase character, 1 numeral, and 1 special character.',
            'min'       => 'Your :attribute must be at least :min characters long.',
            'required'  => 'Enter your :attribute.',
            'confirmed' => 'Your new and confirmation passwords don\'t match. Try again.',
        ],
        'password_confirmation'   => [
            'required' => 'Re-enter your :field.',
        ],

        // Creative validation rules (start)
        'duration'                => [
            'between'  => sprintf(
                'Make sure the :attribute of your Ad is at least %d and doesn\'t exceed %d seconds.',
                ValidationRules::MIN_DURATION_LABEL,
                ValidationRules::MAX_DURATION_LABEL
            ),
            'required' => $creativeRequiredMessage,
        ],
        'video_format'            => [
            'required' => $creativeRequiredMessage,
        ],
        'codec'                   => [
            'required' => $creativeRequiredMessage,
        ],
        'width'                   => [
            'min' => 'Make sure the :attribute of your Ad is at least :min px.',
        ],
        'height'                  => [
            'min' => 'Make sure the :attribute of your Ad is at least :min px.',
        ],
        'extension'               => [
            'required' => $creativeRequiredMessage,
        ],
        'display_aspect_ratio'    => [
            'required' => $creativeRequiredMessage,
        ],
        'frame_rate_mode'         => [
            'required' => $creativeRequiredMessage,
        ],
        'frame_rate'              => [
            'required' => $creativeRequiredMessage,
        ],
        'color_space'             => [
            'required' => $creativeRequiredMessage,
        ],
        'chroma_subsampling'      => [
            'required' => $creativeRequiredMessage,
        ],
        'bit_depth'               => [
            'required' => $creativeRequiredMessage,
        ],
        'scan_type'               => [
            'required' => $creativeRequiredMessage,
        ],
        'bit_rate'                => [
            'required' => $creativeRequiredMessage,
            'min'      => 'Make sure the :attribute of your Ad is at least :min kb/s',
        ],
        'file_size'               => [
            'required' => $creativeRequiredMessage,
            'max'      => 'Your file might be a bit large - please make sure it is under :max Gb.',
        ],
        'audios.*.duration'       => [
            'required' => $creativeAudioRequiredMessage,
            'between'  => sprintf(
                'Make sure the :attribute of your Ad is at least %d and doesn\'t exceed %d.',
                ValidationRules::MIN_DURATION_LABEL,
                ValidationRules::MAX_DURATION_LABEL
            ),
        ],
        'audios.*.bit_rate'       => [
            'required' => $creativeAudioRequiredMessage,
            'min'      => 'Make sure the :attribute of your Ad is at least :min kb/s',
        ],
        'audios.*.channels'       => [
            'required' => $creativeAudioRequiredMessage,
            'included' => 'Make sure each :attribute must have only 2 channel stereo',
        ],
        // Translation for Frontend needs.
        'audios_channel_included' => 'Make sure each :attribute must have only 2 channel stereo',
        'audios.*.sampling_rate'  => [
            'required' => $creativeAudioRequiredMessage,
        ],
        'audios.*.bit_depth'      => [
            'required' => $creativeAudioRequiredMessage,
        ],
        'audios.*.format'         => [
            'required' => $creativeAudioRequiredMessage,
        ],
        // Creative validation rules (end)

        'date.start' => [
            'after'  => 'Start date can not be less than 1 day into the future from today\'s date.',
            'before' => 'The start date must be within 3 months from today\'s date.',
        ],
        'date.end'   => [
            'before' => 'Campaigns cannot be longer than 12 months from the start date.',
        ],

        'subject'         => [
            'required' => 'Select a subject for your message.',
        ],
        'message'         => [
            'required' => 'Enter a brief message.',
            'max'      => 'Let\'s try a shorter message (:max characters max).',
        ],
        'first_name'      => [
            'required' => 'Enter your first name.',
            'max'      => 'Try a shorter name (:max characters max).',
        ],
        'last_name'       => [
            'required' => 'Enter your last name.',
            'max'      => 'Try a shorter name (:max characters max).',
        ],
        'email'           => [
            'required' => 'Enter your business email address.',
            'email'    => 'Check your email for typos and try again.',
            'unique'   => 'This email has already been registered.',
        ],
        'invite_code'     => [
            'required' => 'Enter invite code.',
            'unique'   => 'This invite code has already been registered.',
        ],
        'company_name'    => [
            'required' => 'Enter the name of your business.',
            'max'      => 'Try a shorter name (:max characters max).',
        ],
        'phone'           => [
            'required' => 'Enter your business phone number.',
            'min'      => 'Enter the phone number in a 10-digit format.',
            'regex'    => 'Please enter a valid 10-digit phone number.',
        ],
        'company_address' => [
            'line1' => [
                'required' => 'Enter your business address.',
                'max'      => 'This address is too long. Please double-check it for typos and try again.',
            ],
            'line2' => [
                'max' => 'This address is too long. Please double-check it for typos and try again.',
            ],
            'city'  => [
                'required' => 'Enter your city.',
                'max'      => 'This city name is too long (:max characters max).',
            ],
            'state' => [
                'required' => 'Select your state.',
            ],
            'zip'   => [
                'required' => 'Enter your ZIP code.',
                'regex'    => 'Please enter a 5-digit ZIP code.',
                'max'      => 'Please enter a 5-digit ZIP code.',
            ],
        ],

        'campaign' => [
            'name'        => [
                'required' => 'Enter your campaign name.',
                'max'      => 'Let\'s try a shorter name (:max characters max).',
                'regex'    => 'Use only A-Z and other standard characters on your keyboard.',
            ],
            'dates'       => [
                'required' => 'Select the dates for your campaign.',
            ],
            'timezone_id' => [
                'required' => 'We need a timezone so we know when to start your campaign.',
            ],
            'cost'        => [
                'required' => 'Enter a dollar amount.',
                'min'      => 'You need a minimum budget of :min.',
                'max'      => 'Your campaign budget can\'t exceed :max. If you\'d like to start a larger campaign, contact us via the Contact Us form.',
            ],
            'impressions' => [
                'min' => 'You need a minimum of :min impressions',
            ],
        ],

        'login' => [
            'email'    => [
                'required' => 'Enter your email address.',
            ],
            'password' => [
                'required' => 'A valid password is required.',
            ],
        ],

        'report' => [
            'date'      => [
                'required' => 'Select the report date range.',
            ],
            'frequency' => [
                'required' => 'Select a delivery frequency.',
            ],
        ],

        'allowed_attributes' => 'The :attribute attribute is not allowed.',

        'haapi_date_format' => 'The :attribute attribute is not valid ISO-8601 date.',
        'zip'               => [
            'exists' => 'Please enter a valid 5-digit ZIP code.',
        ],
        'is_advertisers'    => 'Selected advertisers ids were not found: ":advertisers".',

        'valid_broadcast_date' => 'The selected send out date has expired.',

        'has_affected_advertisers' => 'Selected dates has no affected advertisers.',

        'broadcast' => [
            'segment_group' => [
                'affected_advertisers' => [
                    'count' => [
                        'min' => 'There are no affected advertisers in selected dates.',
                    ],
                ],
            ],
        ],

        'date_start' => [
            'after_or_equal' => sprintf('The start date must be a date after or equal to %s.', $affectedAdvertisersStartDate),
        ],
        'campaign_not_found' => 'Campaign id was not found',
        'user_not_found'     => 'User id was not found'
    ],

    'creative' => [
        'included_params' => [
            'CFR'  => 'Constant',
            'avc1' => 'H.264',
            'apch' => 'Prores HQ(apch)',
            'ap4h' => 'Prores HQ(ap4h)',
            'apcs' => 'Prores HQ(apcs)',
            'apcn' => 'Prores HQ(apcn)',
            'ncpa' => 'Prores HQ(ncpa)',
        ],

        'dimensions' => [
            'frame_rate'           => 'fps',
            'bit_depth'            => 'bits',
            'sampling_rate'        => 'Hz',
            'audios_bit_depth'     => 'bits',
            'audios_sampling_rate' => 'Hz',
        ],
    ],

    'promocode' => [
        'required' => 'Enter a code.',
    ],

    'location' => [
        'zipcode_must_be_in_state' => 'The ZIP code and state don\'t match. Double-check your selections and try again.',
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'audio_cnt'              => 'Count of Audio tracks',
        'audios.*.bit_depth'     => 'Audio Bit Depth',
        'audios.*.bit_rate'      => 'Audio Bit Rate',
        'audios.*.channels'      => 'Audio Channel',
        'audios.*.duration'      => 'Audio Duration',
        'audios.*.format'        => 'Audio Format',
        'audios.*.sampling_rate' => 'Audio Sampling Rate',
        'bit_depth'              => 'Bit Depth',
        'video_bit_rate'         => 'Video Bit Rate',
        'chroma_subsampling'     => 'Chroma Subsampling',
        'codec'                  => 'Video Codec',
        'color_space'            => 'Color Space',
        'date.end'               => 'End Date',
        'date.start'             => 'Start Date',
        'display_aspect_ratio'   => 'Display Aspect Ratio',
        'duration'               => 'Duration',
        'extension'              => 'File Extension',
        'file_size'              => 'File Size',
        'format'                 => 'File Format',
        'frame_rate'             => 'Frame Rate',
        'frame_rate_mode'        => 'Frame Rate Mode',
        'height'                 => 'Height',
        'new_password'           => 'new password',
        'password'               => 'password',
        'scan_type'              => 'Scan Type',
        'video_cnt'              => 'Count of Video tracks',
        'video_format'           => 'File Video Format',
        'width'                  => 'Width',

        'audios'                                => [
            'bit_depth'     => 'Audio Bit Depth',
            'bit_rate'      => 'Audio Bit Rate',
            'channels'      => 'Audio Channel',
            'duration'      => 'Audio Duration',
            'format'        => 'Audio Format',
            'sampling_rate' => 'Audio Sampling Rate',
        ],

        // Field names for the creative frontend validation.
        'creative.general.video_cnt'            => 'Count of Video tracks',
        'creative.general.audio_cnt'            => 'Count of Audio tracks',
        'creative.general.format'               => 'File Format',
        'creative.general.extension'            => 'File Extension',
        'creative.general.duration'             => 'Duration',
        'creative.general.codec'                => 'Video Codec',
        'creative.general.video_bit_rate'       => 'Video Bit Rate',
        'creative.general.width'                => 'Width',
        'creative.general.height'               => 'Height',
        'creative.general.display_aspect_ratio' => 'Display Aspect Ratio',
        'creative.general.frame_rate_mode'      => 'Frame Rate Mode',
        'creative.general.frame_rate'           => 'Frame Rate',
        'creative.general.color_space'          => 'Color Space',
        'creative.general.chroma_subsampling'   => 'Chroma Subsampling',
        'creative.general.bit_depth'            => 'Bit Depth',
        'creative.general.scan_type'            => 'Scan Type',
        'creative.audio.format'                 => 'Audio Format',
        'creative.audio.duration'               => 'Audio Duration',
        'creative.audio.bit_rate'               => 'Audio Bit Rate',
        'creative.audio.channels'               => 'Audio Channel',
        'creative.audio.sampling_rate'          => 'Audio Sampling Rate',
        'creative.audio.bit_depth'              => 'Audio Bit Depth',
        'company_address.zipcode'               => 'ZIP code',

        // Broadcast fields
        'email.payload'                         => 'email',
        'email.subject'                         => 'email subject',
        'email.header'                          => 'email header',
        'email.icon'                            => 'icon',
        'email.body'                            => 'email body',
        'notification.payload'                  => 'notification',
        'notification.notification_category_id' => 'notification category',
        'notification.body'                     => 'notification message',
        'segment_group.id'                      => 'broadcast segment',
        'segment_group.advertisers'             => 'advertiser',
        'segment_group.dates'                   => 'dates',
        'segment_group.dates.date_start'        => 'start date',
        'segment_group.dates.date_end'          => 'end date',
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Values
    |--------------------------------------------------------------------------
    */
    'values'     => [
        'channel'       => [
            'in_app_notification' => 'in-app notification',
        ],
        'segment_group' => [
            'id' => [
                BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS => 'appropriate advertiser(s)',
                BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS => 'affected advertiser(s)',
            ],
        ],
    ],
];
