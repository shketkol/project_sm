<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for messages all over the system.
    |
    */

    'nice_work'                      => 'Nice work!',
    'success'                        => 'Success!',
    'error'                          => 'Error',
    'validation_error'               => 'Validation error!',
    'warning'                        => 'Warning!',
    'welcome_back'                   => 'Welcome back!',
    'welcome_back_enter_a_password'  => 'Welcome back, please enter a password!',
    'set_your_password'              => 'Set your password',
    'went_wrong'                     => 'Oops, something went wrong, please try later.',
    'deactivation_notice'            => 'Something\'s not right!',
    'check_form'                     => 'Please check the form',
    'is_pending_notice'              => 'We\'re sorry!',
    'no_data_available'              => 'No Data available',
    'password_replace_success_title' => 'Password reset!',
    'password_replace_success'       => 'You\'ve set a new password. You can use it to log in to your account at any time.',
    'password_been_used_part1'       => 'This password set link has already been used. Try logging in with your password or ',
    'password_been_used_link_title'  => 'Reset Your Password',
    'password_been_used_part2'       => 'if you\'re having trouble logging in.',
    'youve_updated_your_password'    => 'You\'ve updated your password.',
    'if_you_have_any_questions'      => 'To apply for our beta, please visit:',
    'default_maintenance_message'    => 'We\'re working hard to make it even better. It\'ll be back soon—thanks for your patience!',
    'default_maintenance_title'      => 'Ad Manager is in the middle of an update.',
    'default_maintenance_caption'    => 'Scheduled maintenance',
    'access_forbidden'               => 'Access forbidden',
    'service_unavailable'            => 'Service Unavailable',
    'failed_set_password'            => 'Something\'s not right. We couldn\'t set a new password. Please contact the Hulu Ad Manager support.',
    'impressions_and_cpm_based'      => 'Impressions and CPM based on current targeting',
];
