<?php

return [
    'any_questions_resetting'   => 'If you have any difficulties logging in or resetting your password, please contact :email.',
    'business'                  => 'Business: :business',
    'contact_us'                => 'CONTACT US',
    'footer_signature'          => ':publisher_company_name | All rights reserved.',
    'hi'                        => 'Hi :name,',
    'if_you_have_any_questions' => 'If you have any questions, please log in and use the <b>:contact_us</b> form.',
    'logo_alt'                  => ':publisher_company_name Logo',
    'preferences'               => 'Preferences',
    'questions'                 => 'QUESTIONS? :contact_us',
    'support'                   => 'Support',
    'your_friends_at'           => 'Your friends at :company',
    'default_title'             => "Hulu Notification",
    'signature'                 => 'Thank you,</br> The :company Team',
    'company_team'              => 'The :company Team',
];
