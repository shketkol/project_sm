<?php
/**
 * Translations for data tables
 */
return [
  'next' => 'Next',
  'previous' => 'Previous',
  'entries' => 'Showing :fromPage-:toPage of :recordsFiltered entries'
];
