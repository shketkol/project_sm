@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <app></app>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/mockups.js') }}"></script>
@endpush
