<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head.head')
<body class="body-{{ md5(\Route::currentRouteAction())  }}">
    @include('layouts.top.top')
    @yield('content')
    @include('layouts.footer.footer')
</body>
</html>
