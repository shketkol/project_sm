<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>

@include('layouts.footer.user')
@include('layouts.footer.config')

@yield('scripts')
@stack('scripts')
