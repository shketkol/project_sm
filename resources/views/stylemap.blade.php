@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <stylemap-page></stylemap-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
