<img src="{{ $url }}"
     @isset($width) width="{{ $width }}" @endisset
     @isset($height) height="{{ $height }}" @endisset
     hspace="0"
     vspace="0"
     style="margin: 15px auto; border: 1px solid lightgrey;"
     alt="{{ $alt ?? '' }}"
/>
