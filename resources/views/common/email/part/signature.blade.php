<p style="font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.2; margin-top: 0; margin-bottom: 5px; font-size: 14px; mso-line-height-rule: exactly;">
    {{ __('emails.your_friends_at', ['company' => $publisherCompanyName]) }},
</p>
<img src="{{url('/images/email/email-header-logo-green.png')}}"
    width="60"
    hspace="0"
    vspace="0"
    alt="{{ __('emails.logo_alt', ['publisher_company_name' => $publisherCompanyName]) }}"
    style="
        -ms-interpolation-mode: bicubic;
        white-space: nowrap;
        border: 0;
        display: inline-block;
        margin: 0 auto;
        outline: none;
        padding: 0;
        text-decoration: none;"
>
