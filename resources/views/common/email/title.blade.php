<!--TITLE WITH ICON-->
<table width="600" cellspacing="0" cellpadding="0" style="background-color:#ffffff">
    <tr>
        <td width="520"
            align="left"
            valign="middle"
            style="
                text-align:left;
                padding-left: 30px;
                padding-right: 0;
                padding-bottom: 40px;
                padding-top: 0;
                font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;"
        >
            @if($titleIcon)
            <!-- ADD IMAGE PATH AND ALT BELOW -->
            <img src="{{ url("/images/email/icons/png/$titleIcon.png") }}"
                width="75"
                hspace="0"
                vspace="0"
                alt="{{ $title }}"
                style="
                    -ms-interpolation-mode: bicubic;
                    white-space: nowrap;
                    border: 0;
                    display: inline-block;
                    margin: 0 auto;
                    outline: none;
                    padding: 0;
                    text-decoration: none;"
            >
            @endif
            <h1 style="
                    width: 300px;
                    line-height:
                    1.15;
                    margin-bottom: 0;"
            >
                {{ $title }}
            </h1>
        </td>
    </tr>
</table>
