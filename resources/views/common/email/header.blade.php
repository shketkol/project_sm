<!--HEADER-->
<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0" style="margin:0 auto">
    <tr>
        <td align="right">
            <table width="600" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#1ce783"
                   style="background-color: #1ce783; margin:0 auto; width:100%; font-size:0; line-height:1">
                <tr>
                    <td width="520" valign="middle" align="right"
                        style="
                                font-size: 0;
                                text-align: right;
                                vertical-align: middle;
                                padding-top: 18px;
                                padding-bottom: 18px;
                                padding-right: 30px;
                                padding-left: 0;
                        "
                    >
                        <!-- ADD LINK URL AND ALIAS BELOW -->
                        <a href="{{ route('landing') }}" target="_blank" rel="noopener noreferrer">
                            <!-- ADD IMAGE PATH AND ALT BELOW -->
                            <img src="{{url('/images/email/email-header-logo.png')}}"
                                width="75"
                                height="25"
                                hspace="0"
                                vspace="0"
                                alt="{{ __('emails.logo_alt', ['publisher_company_name' => $publisherCompanyName]) }}"
                                style="
                                    display:block;
                                    -ms-interpolation-mode: bicubic;
                                    margin-left: auto;
                                    margin-right: auto;"
                            >
                        </a>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="height: 40px;"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
