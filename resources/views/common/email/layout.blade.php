<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ __('emails.head_title') }}</title>

    <!-- [if gte mso 9]> -->
    <style>
        li {
            text-align: -webkit-match-parent;
            display: list-item;
        }
    </style>
    <!-- [endif] -->

    <style type="text/css">
        table.x_email-wrapper {
            background-color: #ffffff !important;
        }

        .email-wrapper {
            width: 600px;
            max-width: 600px;
        }

        #outlook a {
            padding: 0;
        }

        body {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
            background-color: #f3f3f3 !important;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        #backgroundTable {
            margin: 0;
            padding: 0;
            width: 100% !important;
            line-height: 100% !important;
        }

        img {
            display: block;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            border: 0;
        }

        a {
            display: inline-block;
            text-decoration: none;
        }

        ol,
        ul {
            line-height: 1.5;
        }
    </style>
</head>
<body style="
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
        font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;
        line-height: 20px;
        margin: 0;
        padding: 0;
        text-align: center;
        width: 100%;
        ">
<table width="100%" style="
            width:100%;
            border-collapse:collapse;
            background:#f5f5f5;
        "
>
    <tr>
        <td style="padding-top: 30px; padding-bottom: 30px;">
            <div align="left">
                <table width="600" cellspacing="0" cellpadding="0"
                       style="margin-left: auto; margin-right: auto; background-color:#ffffff; text-align:left">
                    <tr>
                        <td>
                            @include('common.email.header')

                            {{-- TITLE SECTION --}}
                            @yield(
                                'title',
                                View::make('common.email.title', compact('title', 'titleIcon'))
                            )

                            <table width="600" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="520" align="left" valign="top"
                                        style="
                                            padding-left: 60px;
                                            padding-right: 60px;
                                            padding-bottom: 31px;
                                            padding-top: 0;
                                            font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;">
                                        {{-- BODY CONTENT --}}
                                        @yield('body')
                                    </td>
                                </tr>
                            </table>
                            <!--SIGNATURE-->
                            <table width="600" cellspacing="0" cellpadding="0" style="background-color:#ffffff">
                                <tr>
                                    <td width="520" align="left" valign="top" style="
                                            padding-left: 60px;
                                            padding-right: 60px;
                                            padding-bottom: 80px;
                                            padding-top: 0;
                                            font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;"
                                    >
                                        @yield('signature', View::make('common.email.part.signature'))
                                    </td>
                                </tr>
                            </table>
                            {{-- FOOTER --}}
                            @include('common.email.footer')
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
</body>
</html>
