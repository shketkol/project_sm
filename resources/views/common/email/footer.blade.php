<!--FOOTER-->
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="margin:0 auto">
    <tr>
        <td align="center"
            valign="top"
            style="
                display: block;
                padding-bottom: 0;
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                vertical-align: top;"
        >
            <table width="100%"
                border="0"
                cellpadding="0"
                cellspacing="0"
                style="
                    border-collapse: collapse;
                    margin-bottom: 0;
                    margin-left: auto;
                    margin-right: auto;
                    margin-top: 0;
                    max-width: 600px;
                    mso-table-lspace: 0;
                    mso-table-rspace: 0;
                    width: 100%;"
            >
                <tr>
                    <td align="left"
                        valign="top"
                        style="
                            border-top: 1px solid #adb3bf;
                            padding-bottom: 40px;
                            padding-left: 20px;
                            padding-right: 10px;
                            padding-top: 30px;
                            vertical-align: top;"
                    >
                        <table width="600"
                            border="0"
                            cellpadding="0"
                            cellspacing="0"
                            style="
                                border-collapse: collapse;
                                margin-bottom: 0;
                                margin-left: auto;
                                margin-right: auto;
                                margin-top: 0;
                                max-width: 600px;
                                mso-table-lspace: 0;
                                mso-table-rspace: 0;
                                width: 100%;"
                        >
                            <tr>
                                <td align="left"
                                    valign="top"
                                    style="
                                        padding-bottom: 0;
                                        padding-left: 0;
                                        padding-right: 75px;
                                        vertical-align: top;"
                                >
                                    <table border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        style="
                                            width: 100%;
                                            border-collapse: collapse;
                                            margin-bottom: 0;
                                            margin-left: auto;
                                            margin-right: auto;
                                            margin-top: 0;
                                            mso-table-lspace: 0;
                                            mso-table-rspace: 0;"
                                    >
                                        <tr>
                                            <td>
                                                <a target="_blank" rel="noopener noreferrer"
                                                   href="{{ route('landing') }}" alias="Header">
                                                    <img alt="{{ __('emails.logo_alt', ['publisher_company_name' => $publisherCompanyName]) }}"
                                                         src="{{url('/images/email/email-footer-logo.png')}}"
                                                         width="78"
                                                         hspace="0"
                                                         vspace="0"
                                                         style="
                                                             -ms-interpolation-mode: bicubic;
                                                             white-space: nowrap;
                                                             border: 0;
                                                             display: inline-block;
                                                             margin: 0 auto;
                                                             outline: none;
                                                             padding: 0;
                                                             text-decoration: none;
                                                             color: #666666;"
                                                    >
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left"
                                    valign="top"
                                    style="
                                        padding-left: 0;
                                        padding-right: 0;
                                        padding-top: 0;
                                        vertical-align: middle;"
                                >
                                    <table border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        style="
                                            width: 100%;
                                            border-collapse: collapse;
                                            margin-bottom: 0;
                                            margin-left: auto;
                                            margin-right: auto;
                                            margin-top: 0;
                                            mso-table-lspace: 0;
                                            mso-table-rspace: 0;"
                                    >
                                        <tr>
                                            <td style="font-size: 14px; color: #adb3bf;">
                                                <span style="color: #adb3bf; border: 0;">
                                                    {!! $publisherBusinessAddress !!}
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
