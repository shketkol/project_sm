<?php

namespace Modules\Report\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Report\Policies\ReportPolicy;
use Modules\User\Models\Role;

class ReportPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [
            ReportPolicy::PERMISSION_LIST_REPORT,
            ReportPolicy::PERMISSION_CREATE_SCHEDULED_REPORT,
            ReportPolicy::PERMISSION_CREATE_ADVERTISERS_REPORT,
            ReportPolicy::PERMISSION_CREATE_DOWNLOAD_REPORT,
            ReportPolicy::PERMISSION_CREATE_PENDING_CAMPAIGNS_REPORT,
            ReportPolicy::PERMISSION_CREATE_MISSING_ADS_REPORT,
            ReportPolicy::PERMISSION_DELETE_REPORT,
            ReportPolicy::PERMISSION_SHOW_ADVERTISERS_REPORT,
            ReportPolicy::PERMISSION_SHOW_PENDING_CAMPAIGNS_REPORT,
            ReportPolicy::PERMISSION_SHOW_DOWNLOAD_REPORT,
            ReportPolicy::PERMISSION_SHOW_SCHEDULED_REPORT,
            ReportPolicy::PERMISSION_SHOW_MISSING_ADS_REPORT,
            ReportPolicy::PERMISSION_UPDATE_PENDING_CAMPAIGNS_REPORT,
            ReportPolicy::PERMISSION_UPDATE_ADVERTISERS_REPORT,
            ReportPolicy::PERMISSION_UPDATE_DOWNLOAD_REPORT,
            ReportPolicy::PERMISSION_UPDATE_SCHEDULED_REPORT,
            ReportPolicy::PERMISSION_UPDATE_MISSING_ADS_REPORT,
            ReportPolicy::PERMISSION_PAUSE_REPORT,
            ReportPolicy::PERMISSION_RESUME_REPORT,
        ],
        Role::ID_ADMIN_READ_ONLY => [
            ReportPolicy::PERMISSION_LIST_REPORT,
            ReportPolicy::PERMISSION_CREATE_PENDING_CAMPAIGNS_REPORT,
            ReportPolicy::PERMISSION_CREATE_SCHEDULED_REPORT,
            ReportPolicy::PERMISSION_CREATE_ADVERTISERS_REPORT,
            ReportPolicy::PERMISSION_CREATE_DOWNLOAD_REPORT,
            ReportPolicy::PERMISSION_CREATE_MISSING_ADS_REPORT,
            ReportPolicy::PERMISSION_DELETE_REPORT,
            ReportPolicy::PERMISSION_SHOW_PENDING_CAMPAIGNS_REPORT,
            ReportPolicy::PERMISSION_SHOW_ADVERTISERS_REPORT,
            ReportPolicy::PERMISSION_SHOW_DOWNLOAD_REPORT,
            ReportPolicy::PERMISSION_SHOW_SCHEDULED_REPORT,
            ReportPolicy::PERMISSION_SHOW_MISSING_ADS_REPORT,
            ReportPolicy::PERMISSION_UPDATE_PENDING_CAMPAIGNS_REPORT,
            ReportPolicy::PERMISSION_UPDATE_ADVERTISERS_REPORT,
            ReportPolicy::PERMISSION_UPDATE_DOWNLOAD_REPORT,
            ReportPolicy::PERMISSION_UPDATE_SCHEDULED_REPORT,
            ReportPolicy::PERMISSION_UPDATE_MISSING_ADS_REPORT,
            ReportPolicy::PERMISSION_PAUSE_REPORT,
            ReportPolicy::PERMISSION_RESUME_REPORT,
        ],
        Role::ID_ADVERTISER      => [
            ReportPolicy::PERMISSION_LIST_REPORT,
            ReportPolicy::PERMISSION_CREATE_SCHEDULED_REPORT,
            ReportPolicy::PERMISSION_CREATE_DOWNLOAD_REPORT,
            ReportPolicy::PERMISSION_DELETE_REPORT,
            ReportPolicy::PERMISSION_SHOW_DOWNLOAD_REPORT,
            ReportPolicy::PERMISSION_SHOW_SCHEDULED_REPORT,
            ReportPolicy::PERMISSION_UPDATE_DOWNLOAD_REPORT,
            ReportPolicy::PERMISSION_UPDATE_SCHEDULED_REPORT,
            ReportPolicy::PERMISSION_PAUSE_REPORT,
            ReportPolicy::PERMISSION_RESUME_REPORT,
        ],
    ];
}
