<?php

namespace Modules\Report\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Report\Models\ReportDeliveryFrequency;

class ReportDeliveryFrequenciesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $types = [
            [
                'id'   => ReportDeliveryFrequency::ID_DAILY,
                'name' => ReportDeliveryFrequency::DAILY,
            ],
            [
                'id'   => ReportDeliveryFrequency::ID_WEEKLY,
                'name' => ReportDeliveryFrequency::WEEKLY,
            ],
            [
                'id'   => ReportDeliveryFrequency::ID_MONTHLY,
                'name' => ReportDeliveryFrequency::MONTHLY,
            ],
        ];

        foreach ($types as $type) {
            ReportDeliveryFrequency::updateOrCreate(
                ['id' => Arr::get($type, 'id')],
                $type
            );
        }
    }
}
