<?php

namespace Modules\Report\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Report\Models\ReportStatus;

class ReportStatusesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $statuses = [
            [
                'id'   => ReportStatus::ID_DRAFT,
                'name' => ReportStatus::DRAFT,
            ],
            [
                'id'   => ReportStatus::ID_SUBMITTED,
                'name' => ReportStatus::SUBMITTED,
            ],
            [
                'id'   => ReportStatus::ID_FAILED,
                'name' => ReportStatus::FAILED,
            ],
            [
                'id'   => ReportStatus::ID_COMPLETED,
                'name' => ReportStatus::COMPLETED,
            ],
            [
                'id'   => ReportStatus::ID_PAUSED,
                'name' => ReportStatus::PAUSED,
            ],
        ];

        foreach ($statuses as $status) {
            ReportStatus::updateOrCreate(
                ['id' => Arr::get($status, 'id')],
                $status
            );
        }
    }
}
