<?php

namespace Modules\Report\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Report\Models\ReportType;

class ReportTypesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        foreach (ReportType::TYPES as $typeId => $type) {
            ReportType::updateOrCreate(['id' => $typeId], [
                'id' => $typeId,
                'name' => $type,
            ]);
        }
    }
}
