<?php

namespace Modules\Report\Database\Factories;

use App\Factories\Factory;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Support\Arr;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportDays;
use Modules\Report\Models\ReportDeliveryFrequency;
use Modules\Report\Models\ReportStatus;
use Modules\Report\Models\ReportType;
use Modules\User\Models\User;

class ReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Report::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $type = $this->faker->randomElement([
            ReportType::ID_DOWNLOAD,
            ReportType::ID_SCHEDULED,
        ]);

        $attributes = [
            'name'         => $this->faker->catchPhrase,
            'type_id'      => $type,
            'user_id'      => User::factory(),
            'generated_at' => Carbon::now(),
            'status_id'    => $this->faker->randomElement([
                ReportStatus::ID_DRAFT,
                ReportStatus::ID_SUBMITTED,
                ReportStatus::ID_FAILED,
                ReportStatus::ID_COMPLETED,
                ReportStatus::ID_PAUSED,
            ]),
        ];

        // scheduled
        if ($type === ReportType::ID_SCHEDULED) {
            $attributes = $this->getScheduledAttributes($attributes);
        }

        // download
        if ($type === ReportType::ID_DOWNLOAD) {
            $attributes = $this->getDownloadedAttributes($attributes);
        }

        return $attributes;
    }

    /**
     * @param array $attributes
     *
     * @return array
     */
    private function getScheduledAttributes(array $attributes = []): array
    {
        Arr::set($attributes, 'type_id', ReportType::ID_SCHEDULED);
        Arr::set($attributes, 'status_id', ReportStatus::ID_DRAFT);
        Arr::set($attributes, 'delivery_frequency_id', $this->faker->randomElement([
            ReportDeliveryFrequency::ID_DAILY,
            ReportDeliveryFrequency::ID_WEEKLY,
            ReportDeliveryFrequency::ID_MONTHLY,
        ]));

        if (Arr::get($attributes, 'delivery_frequency_id') === ReportDeliveryFrequency::ID_WEEKLY) {
            Arr::set($attributes, 'execution_day', $this->faker->randomElement(ReportDays::REPORT_DAYS_ID));
        }

        return $attributes;
    }

    /**
     * @param array $override
     *
     * @return $this
     */
    public function scheduled(array $override = []): self
    {
        return $this->override(array_merge($this->getScheduledAttributes(), $override));
    }

    /**
     * @param array $attributes
     *
     * @return array
     */
    private function getDownloadedAttributes(array $attributes = []): array
    {
        Arr::set($attributes, 'date_start', CarbonImmutable::now()->add(rand(3, 50), ' day'));
        Arr::set($attributes, 'date_end', CarbonImmutable::now()->add(rand(10, 50), ' day'));
        Arr::set($attributes, 'status_id', ReportStatus::ID_COMPLETED);
        Arr::set($attributes, 'type_id', ReportType::ID_DOWNLOAD);
        Arr::set($attributes, 'delivery_frequency_id', null);

        return $attributes;
    }

    /**
     * @param array $override
     *
     * @return $this
     */
    public function downloaded(array $override = []): self
    {
        return $this->override(array_merge($this->getDownloadedAttributes(), $override));
    }
}
