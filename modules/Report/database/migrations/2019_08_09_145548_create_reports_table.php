<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->timestamps();

            // Type
            $table->bigInteger('type_id')->unsigned()->index();
            $table->foreign('type_id')
                ->references('id')
                ->on('report_types')
                ->onDelete('cascade');

            // Delivery Frequency
            $table->bigInteger('delivery_frequency_id')->unsigned()->nullable()->index();
            $table->foreign('delivery_frequency_id')
                ->references('id')
                ->on('report_delivery_frequencies')
                ->onDelete('cascade');

            // Status
            $table->bigInteger('status_id')->unsigned()->index();
            $table->foreign('status_id')
                ->references('id')
                ->on('report_statuses')
                ->onDelete('cascade');

            // User
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
