<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Report\Models\ReportDays;
use Modules\Report\Models\ReportDeliveryFrequency;

class SetExecutionDayDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function (Blueprint $table) {
            DB::table('reports')
                ->where('delivery_frequency_id', '=', ReportDeliveryFrequency::ID_WEEKLY)
                ->whereNull('execution_day')
                ->update(['execution_day' => ReportDays::SUNDAY_ID]);
        });
    }
}
