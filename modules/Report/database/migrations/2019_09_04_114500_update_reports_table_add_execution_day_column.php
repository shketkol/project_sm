<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Report\Models\ReportDays;

class UpdateReportsTableAddExecutionDayColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->enum('execution_day',
                [
                    ReportDays::SUNDAY_ID,
                    ReportDays::MONDAY_ID,
                    ReportDays::TUESDAY_ID,
                    ReportDays::WEDNESDAY_ID,
                    ReportDays::THURSDAY_ID,
                    ReportDays::FRIDAY_ID,
                    ReportDays::SATURDAY_ID,
                ])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->dropColumn('execution_day');
        });
    }
}
