<?php

return [
    'scheduled' => [
        'subject' => 'Report Ready: :name',
        'title'   => 'Report Ready',
        'body'    => [
            'we_are_attaching_the_report'             => 'We’re attaching the :report_name report for the :date_from - :date_to period.',
            'all_report_details_are_displayed'        => 'Please note that report information is displayed in ET.',
            'for_more_information_about_the_campaign' => 'For more information, see :campaign_listing.',
        ],
    ],
    'missing_ads_scheduled' => [
        'subject' => 'Campaigns without Ads Report Ready: :name',
        'title'   => 'Campaigns without Ads Report Ready',
        'body'    => [
            'we_are_attaching_the_report'             => 'We’re attaching the ":report_name" report for :generation_date.',
            'all_report_details_are_displayed'        => 'Please note that report information is displayed in ET.',
        ],
    ],
];
