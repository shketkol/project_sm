<?php

return [
    'advertiser' => [
        'scheduled' => 'The :report_name report is now available on the Reports page.',
    ],
    'missing_ads' => [
        'scheduled' => 'The Report ":report_name" is now available in the Reporting section.',
    ],
];
