<?php

return [
    'create_first_report'             => 'Create your first report',
    'create_report'                   => 'Create report',
    'report_was_not_deleted'          => 'Report was not deleted.',
    'report_was_successfully_created' => 'Report was successfully created',
    'downloaded_report_created'       => 'Success! Preparing your report for download...',
    'report_was_successfully_updated' => 'Report was successfully updated',
    'valid_emails'                    => 'Each of the entered emails should be valid email',
    'you_have_removed_the_report'     => 'You\'ve removed the selected report.',
    'emailed_reports'                 => "<b>Emailed Reports</b> are up-to-date reports of all your campaigns delivered on a regular basis.",
    'downloadable_reports'            => "<b>Downloadable Reports</b> let you select a custom time frame and can be downloaded immediately.",
    'advertisers_reports'             => "<b>Advertiser Reports</b> displays a list of all advertisers on the platform.",
    'campaigns_reports'               => "<b>Campaigns Drafts Reports</b> is a list of all Draft campaigns with the stage at which the Advertiser stopped.",
    'missing_ads_reports'             => "<b>Campaigns without Ads</b> are scheduled to go Live soon but do not have approved Ads.",
    'expired'                         => 'The report has expired. To generate a new one click "Update" at the report details form or "Create Report" button at the top right corner of the screen.',
];
