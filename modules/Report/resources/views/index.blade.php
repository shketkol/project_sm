@extends('layouts.app')

@section('content')
    <div class="vue-app">
        @role(\Modules\User\Models\Role::ADVERTISER)
            <index-page></index-page>
        @else
            <admin-index-page></admin-index-page>
        @endrole
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/report.js') }}"></script>
@endpush
