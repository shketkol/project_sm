<?php
/**
 * @see \Modules\Report\Exports\ReportMetricsExport\Sheets\GenresSheet::view()
 * @var \Modules\Campaign\Models\Campaign $campaign
 * @var array                             $impressions
 */
$borderStyle = 'border: 1px solid black;';
$backgroundColor = config('excel.exports.styles.fill_color.hex');
$thStyle = $borderStyle . "font-weight: bold; background-color: {$backgroundColor}; text-align: center";
$deliveryStyle = $borderStyle . 'text-align: right';
$noticeStyle = 'border-top: 1px solid black; font-size: 8px';
?>
<table>
    <thead>
    <tr>
        <th style="font-weight: bold">{{ __('report::report_metrics.sheets.genres.heading') }}</th>
    </tr>
    </thead>
    <tbody>

    <tr></tr>

    @if(empty($impressions))
        <tr>
            <td>{{ __('report::report_metrics.sheets.genres.unavailable') }}</td>
        </tr>
    @else
        <tr>
            <td style="{{ $thStyle }}">{{ __('campaign::labels.campaign_name') }}</td>
            <td style="{{ $thStyle }}">{{ __('labels.show_genre') }}</td>
            <td style="{{ $thStyle }}">{{ __('report::report_metrics.general.delivery') }}</td>
        </tr>

        @foreach($impressions as $key => $impression)
            <tr>
                {{-- print campaign name then empty cell --}}
                @if($key === 0)
                    <td>{{ $campaign->name }}</td>
                @else
                    <td></td>
                @endif

                {{-- Add "*" to "Other" genres since HAAPI does not send one --}}
                @if(\Illuminate\Support\Arr::get($impression, 'name') === __('report::report_metrics.sheets.genres.other'))
                    <td style="{{ $borderStyle }}">{{ \Illuminate\Support\Arr::get($impression, 'name') . '*' }}</td>
                @else
                    <td style="{{ $borderStyle }}">{{ \Illuminate\Support\Arr::get($impression, 'name') }}</td>
                @endif

                <td style="{{ $deliveryStyle }}">{{ \Illuminate\Support\Arr::get($impression, 'percentage') . '%' }}</td>
            </tr>
        @endforeach

        <tr>
            <td style="{{ $noticeStyle }}">{{ __('report::report_metrics.sheets.genres.other_notice') }}</td>
        </tr>
    @endif
    </tbody>
</table>
