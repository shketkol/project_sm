<?php
/**
 * @see \Modules\Report\Exports\ReportMetricsExport\Sheets\DevicesSheet::view()
 * @var \Modules\Campaign\Models\Campaign $campaign
 * @var array                             $impressions
 */
$borderStyle = 'border: 1px solid black;';
$backgroundColor = config('excel.exports.styles.fill_color.hex');
$thStyle = $borderStyle . "font-weight: bold; background-color: {$backgroundColor}; text-align: center";
$deliveryStyle = $borderStyle . 'text-align: right';
$noticeStyle = 'border-top: 1px solid black; font-size: 8px';
?>
<table>
    <thead>
    <tr>
        <th style="font-weight: bold">{{ __('report::report_metrics.sheets.devices.heading') }}</th>
    </tr>
    </thead>
    <tbody>

    <tr></tr>

    @if(empty($impressions))
        <tr>
            <td>{{ __('report::report_metrics.sheets.devices.unavailable') }}</td>
        </tr>
    @else
        <tr>
            <td style="{{ $thStyle }}">{{ __('campaign::labels.campaign_name') }}</td>
            <td style="{{ $thStyle }}">{{ __('report::report_metrics.general.platform') }}</td>
            <td style="{{ $thStyle }}">{{ __('report::report_metrics.general.delivery') }}</td>
        </tr>

        @foreach($impressions as $key => $impression)
            <tr>
                @if($key === 0)
                    <td style="vertical-align: top" rowspan={{ count($impressions) }}>{{ $campaign->name }}</td>
                @endif

                <td style="{{ $borderStyle }}">{{ \Illuminate\Support\Arr::get($impression, 'name') }}</td>
                <td style="{{ $deliveryStyle }}">{{ \Illuminate\Support\Arr::get($impression, 'percentage') . '%' }}</td>
            </tr>
        @endforeach
        <tr>
            <td style="border-top: 1px solid black">{{--border fix--}}</td>
        </tr>
    @endif
    </tbody>
</table>
