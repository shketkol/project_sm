<?php
/**
 * @see \Modules\Report\Exports\ReportMetricsExport\Sheets\ReportSheet::view()
 * @var \Modules\Campaign\Models\Campaign $campaign
 * @var string                            $submitDate
 * @var string                            $startDate
 * @var string                            $endDate
 * @var string                            $creativeStatus
 * @var int                               $deliveredImpressions
 * @var int                               $totalCost
 */
$backgroundColor = config('excel.exports.styles.fill_color.hex');
$cellStyle = "font-weight: bold; background-color: {$backgroundColor}; border: 1px solid black; text-align: center";
?>
<table>
    <thead>
    <tr>
        <th style="{{ $cellStyle }}">{{ __('campaign::labels.campaign_name') }}</th>
        <th style="{{ $cellStyle }}">{{ __('report::report_metrics.general.date_submitted') }}</th>
        <th style="{{ $cellStyle }}">{{ __('campaign::labels.status') }}</th>
        <th style="{{ $cellStyle }}">{{ __('creative::labels.status') }}</th>
        <th style="{{ $cellStyle }}">{{ __('labels.start_date') }}</th>
        <th style="{{ $cellStyle }}">{{ __('labels.end_date') }}</th>
        <th style="{{ $cellStyle }}">{{ __('report::report_metrics.general.delivered_impressions') }}</th>
        <th style="{{ $cellStyle }}">{{ __('report::report_metrics.general.target_impressions') }}</th>
        <th style="{{ $cellStyle }}">{{ __('labels.cpm') }}</th>
        <th style="{{ $cellStyle }}">{{ __('labels.total_cost') }}</th>
        <th style="{{ $cellStyle }}">{{ __('labels.budget') }}</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{ $campaign->name }}</td>
        <td>{{ $submitDate }}</td>
        <td>{{ \Illuminate\Support\Str::ucfirst($campaign->status->name) }}</td>
        <td>{{ $creativeStatus }}</td>
        <td>{{ $startDate }}</td>
        <td>{{ $endDate }}</td>
        <td>{{ $deliveredImpressions }}</td>
        <td>{{ $campaign->impressions }}</td>
        <td>{{ $campaign->getLowestCpm() }}</td>
        <td>{{ $totalCost }}</td>
        <td>{{ $campaign->getLowestBudget() }}</td>
    </tr>
    </tbody>
</table>
