<?php
/**
 * @see \Modules\Report\Exports\ReportMetricsExport\Sheets\OverviewSheet::view()
 * @var \Modules\Campaign\Models\Campaign $campaign
 * @var \Modules\User\Models\User         $user
 * @var string                            $periodStart
 * @var string                            $periodEnd
 * @var string                            $dateGenerated
 */
$headingStyle = 'font-weight: bold;';
$backgroundColor = config('excel.exports.styles.fill_color.hex');
$cellStyle = $headingStyle . "background-color: {$backgroundColor};";
?>
<table>
    <thead>
    <tr>
        <th style="{{ $headingStyle }}">{{ __('report::report_metrics.general.hulu_ad_manager_campaign_report') }}</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="{{ $cellStyle }}">{{ __('campaign::labels.campaign_name') }}</td>
        <td>{{ $campaign->name }}</td>
    </tr>
    <tr>
        <td style="{{ $cellStyle }}">{{ __('report::report_metrics.general.period_start') }}</td>
        <td>{{ $periodStart }}</td>
    </tr>
    <tr>
        <td style="{{ $cellStyle }}">{{ __('report::report_metrics.general.period_end') }}</td>
        <td>{{ $periodEnd }}</td>
    </tr>
    <tr>
        <td style="{{ $cellStyle }}">{{ __('report::report_metrics.general.report_timezone') }}</td>
        <td>{{ config('report.timezone') }}</td>
    </tr>
    <tr>
        <td style="{{ $cellStyle }}">{{ __('report::report_metrics.general.date_generated') }}</td>
        <td>{{ $dateGenerated }}</td>
    </tr>
    <tr>
        <td style="{{ $cellStyle }}">{{ __('report::report_metrics.general.generated_by') }}</td>
        <td>{{ $user->isAdmin() ? $user->email : $user->company_name }}</td>
    </tr>
    </tbody>
</table>
