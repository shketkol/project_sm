<?php
/**
 * @see \Modules\Report\Exports\ReportMetricsExport\Sheets\FrequencySheet::view()
 * @var \Modules\Campaign\Models\Campaign $campaign
 */
$borderStyle = 'border: 1px solid black;';
$backgroundColor = config('excel.exports.styles.fill_color.hex');
$thStyle = $borderStyle . "font-weight: bold; background-color: {$backgroundColor}; text-align: center";
?>
<table>
    <thead>
    <tr>
        <th style="font-weight: bold">{{ __('report::report_metrics.sheets.frequency.heading') }}</th>
    </tr>
    </thead>
    <tbody>

    <tr></tr>

    <tr>
        <td style="{{ $thStyle }}">{{ __('campaign::labels.campaign_name') }}</td>
        <td style="{{ $thStyle }}">{{ __('labels.impressions') }}</td>
        <td style="{{ $thStyle }}">{{ __('report::report_metrics.general.unique_devices') }}</td>
        <td style="{{ $thStyle }}">{{ __('labels.frequency') }}</td>
    </tr>
    <tr>
        <td style="{{ $borderStyle }}">{{ $campaign->name }}</td>
        <td style="{{ $borderStyle }}">{{ '-' }}</td>
        <td style="{{ $borderStyle }}">{{ '-' }}</td>
        <td style="{{ $borderStyle }}">{{ '-' }}</td>
    </tr>
    </tbody>
</table>
