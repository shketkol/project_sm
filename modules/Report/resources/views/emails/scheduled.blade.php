@extends($isAdmin ? 'common.email.layout-admin' : 'common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('report::emails.scheduled.body.we_are_attaching_the_report', [
            'report_name' => $reportName,
            'date_from'   => $dateFrom,
            'date_to'     => $dateTo,
        ]) }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('report::emails.scheduled.body.all_report_details_are_displayed') }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('report::emails.scheduled.body.for_more_information_about_the_campaign', [
            'campaign_listing' => view('common.email.part.link', [
                'link' => $campaignListing,
                'text' => __('campaign::labels.campaign_listing'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    @include('common.email.part.if-you-have-any-questions')
@stop
