<?php

Route::middleware(['auth:web,admin'])->group(function () {
    Route::get('/', 'IndexController')->name('index')->middleware('can:report.list');

    Route::group(['prefix' => '/scheduled', 'as' => 'scheduled.'], function () {
        Route::get('/{report}', 'IndexController')
            ->name('show');
    });

    Route::group(['prefix' => '/download', 'as' => 'download.'], function () {
        Route::get('/{report}', 'IndexController')
            ->name('show');
    });

    Route::group(['prefix' => '/advertisers', 'as' => 'advertisers.'], function () {
        Route::get('/{report}', 'IndexController')
            ->name('show');
    });

    Route::group(['prefix' => '/pending_campaigns', 'as' => 'pending_campaigns.'], function () {
        Route::get('/{report}', 'IndexController')
            ->name('show');
    });

    Route::group(['prefix' => '/missing_ads', 'as' => 'missing_ads.'], function () {
        Route::get('/{report}', 'IndexController')
             ->name('show');
    });
});
