<?php

/**
 * Statistic routes
 */
Route::group([
    'middleware' => 'auth:web,admin',
], function () {
    Route::group(['prefix' => '/schedule', 'as' => 'schedule.'], function () {
        Route::post('/', 'StoreScheduleReportController')
            ->name('store')
            ->middleware('can:report.createScheduled');

        Route::get('/{report}', 'ShowScheduleReportController')
            ->name('show')
            ->middleware('can:report.showScheduled,report');

        Route::patch('/{report}', 'UpdateScheduleReportController')
            ->name('update')
            ->middleware('can:report.updateScheduled,report');
    });

    Route::group(['prefix' => '/download', 'as' => 'download.'], function () {
        Route::post('/', 'StoreDownloadReportController')
            ->name('store')
            ->middleware('can:report.createDownload');

        Route::get('/{report}', 'ShowDownloadReportController')
            ->name('show')
            ->middleware('can:report.showDownload,report');

        Route::patch('/{report}', 'UpdateDownloadReportController')
            ->name('update')
            ->middleware('can:report.updateDownload,report');
    });

    Route::group(['prefix' => '/users', 'as' => 'advertisers.'], function () {
        Route::post('/', 'StoreAdvertisersReportController')
            ->name('store')
            ->middleware('can:report.createAdvertisers');

        Route::get('/{report}', 'ShowDownloadReportController')
            ->name('show')
            ->middleware('can:report.showAdvertisers,report');

        Route::patch('/{report}', 'UpdateAdvertisersReportController')
            ->name('update')
            ->middleware('can:report.updateAdvertisers,report');
    });

    Route::group(['prefix' => '/pending-campaigns', 'as' => 'pending-campaigns.'], function () {
        Route::post('/', 'StorePendingCampaignsReportController')
            ->name('store')
            ->middleware('can:report.createPendingCampaigns');

        Route::get('/{report}', 'ShowPendingCampaignsReportController')
            ->name('show')
            ->middleware('can:report.showPendingCampaigns,report');

        Route::patch('/{report}', 'UpdatePendingCampaignsReportController')
            ->name('update')
            ->middleware('can:report.updatePendingCampaigns,report');
    });

    Route::group(['prefix' => '/missing-ads', 'as' => 'missing-ads.'], function () {
        Route::post('/', 'StoreMissingAdsReportController')
             ->name('store')
             ->middleware('can:report.createMissingAds');

        Route::get('/{report}', 'ShowMissingAdsReportController')
             ->name('show')
             ->middleware('can:report.showMissingAds,report');

        Route::patch('/{report}', 'UpdateMissingAdsReportController')
             ->name('update')
             ->middleware('can:report.updateMissingAds,report');
    });

    Route::group(['prefix' => '/delivery-frequencies', 'as' => 'deliveryFrequencies.'], function () {
        Route::get('/', 'IndexDeliveryFrequencyController')
            ->name('index')
            ->middleware('can:report.list');
    });

    Route::group(['prefix' => '/{report}'], function () {
        // Pause
        Route::patch('/pause', 'PauseReportController')
            ->name('pause')
            ->middleware('can:report.pause,report');

        // Resume
        Route::patch('/resume', 'ResumeReportController')
            ->name('resume')
            ->middleware('can:report.resume,report');

        // Destroy
        Route::delete('/', 'DestroyReportController')
            ->name('destroy')
            ->middleware('can:report.delete,report');
    });
});
