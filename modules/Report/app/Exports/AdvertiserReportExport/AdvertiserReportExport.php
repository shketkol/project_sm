<?php

namespace Modules\Report\Exports\AdvertiserReportExport;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Modules\Report\Exports\AdvertiserReportExport\Sheets\DataSheet;
use Modules\Report\Exports\AdvertiserReportExport\Sheets\InfoSheet;
use Modules\Report\Exports\AdvertiserReportExport\Data\ReportExportData;
use Modules\Report\Exports\Traits\RemoveFormulaCharacters;

class AdvertiserReportExport implements WithMultipleSheets, WithCustomValueBinder
{
    use Exportable, RemoveFormulaCharacters;

    /**
     * @var array
     */
    protected $data;

    /**
     * @param ReportExportData $data
     */
    public function __construct(ReportExportData $data)
    {
        $this->data = $data;
    }

    /**
     * Get sheets for the file.
     *
     * @return array
     */
    public function sheets(): array
    {
        return [
            new InfoSheet($this->data),
            new DataSheet($this->data),
        ];
    }
}
