<?php

namespace Modules\Report\Exports\AdvertiserReportExport\Data;

use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * @property bool $isAdmin
 * @property Carbon $dateFrom
 * @property Carbon $dateTo
 * @property AdvertisersDataIterator $advertisers
 * @property Carbon $createdAt
 * @property string $createdBy
 * @property string $name
 */
class ReportExportData
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the value.
     *
     * @param string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return Arr::get($this->data, $key);
    }
}
