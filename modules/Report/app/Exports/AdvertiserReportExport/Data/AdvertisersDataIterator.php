<?php

namespace Modules\Report\Exports\AdvertiserReportExport\Data;

use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Admin\Account\AdminAccountSearch;
use Modules\Haapi\DataTransferObjects\Account\AdminAccountSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Iterators\PaginationIterator;
use Modules\Report\Models\Report;
use Iterator;

class AdvertisersDataIterator extends PaginationIterator implements Iterator
{
    use ActAsAdmin;

    /**
     * @var int
     */
    protected $adminId;

    /**
     * @var AdminAccountSearch
     */
    protected $accountSearch;

    /**
     * @var Report|null
     */
    protected $report;

    /**
     * @param AdminAccountSearch $accountSearch
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(AdminAccountSearch $accountSearch)
    {
        $this->accountSearch = $accountSearch;
        $this->adminId = $this->authAsAdmin();
    }

    /**
     * Set report property.
     *
     * @param Report $report
     * @return AdvertisersDataIterator
     */
    public function setReport(Report $report): self
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Send request to load data.
     *
     * @return HaapiResponse
     * @throws \App\Exceptions\BaseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    protected function load(): HaapiResponse
    {
        return $this->accountSearch->handle(
            $this->createParam(AdminAccountSearchParam::class),
            $this->adminId
        );
    }

    /**
     * Get key of data that should be iterated.
     *
     * @return string
     */
    protected function getResponseKey(): string
    {
        return 'accounts';
    }
}
