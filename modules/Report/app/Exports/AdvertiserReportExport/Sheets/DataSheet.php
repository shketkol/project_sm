<?php

namespace Modules\Report\Exports\AdvertiserReportExport\Sheets;

use App\Exports\Sheets\BaseSheet;
use App\Helpers\DateFormatHelper;
use Iterator;
use Maatwebsite\Excel\Concerns\FromIterator;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use Modules\Report\Exports\AdvertiserReportExport\Data\ReportExportData;

class DataSheet extends BaseSheet implements FromIterator, ShouldAutoSize, WithStrictNullComparison, WithTitle
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param ReportExportData $data
     */
    public function __construct(ReportExportData $data)
    {
        $this->data = $data;
    }

    /**
     * Get sheet title.
     *
     * @return string
     */
    public function title(): string
    {
        return __('report::labels.export.sheets.data.title');
    }

    /**
     * Get sheet data.
     *
     * @return Iterator
     */
    public function iterator(): Iterator
    {
        // Headings
        yield $this->getHeadings();

        // Advertisers
        foreach ($this->data->advertisers as $advertiser) {
            yield $this->mapAdvertiser($advertiser);
        }
    }

    /**
     * @param array $advertiser
     *
     * @return array
     */
    protected function mapAdvertiser(array $advertiser): array
    {
        $statusLower = strtolower($advertiser['status']);
        return [
            $advertiser['name'],
            $advertiser['email'],
            $advertiser['bookedValue'],
            DateFormatHelper::toMonthDayYear($advertiser['createdAt']),
            $advertiser['campaignCount'],
            __("user::labels.statuses.{$statusLower}"),
        ];
    }

    /**
     * Get headings for the table.
     *
     * @return array
     */
    protected function getHeadings(): array
    {
        return [
            __('labels.name'),
            __('labels.email'),
            __('labels.booked_spend'),
            __('labels.account_created'),
            __('labels.number_of_campaigns'),
            __('labels.status'),
        ];
    }

    /**
     * Add styling to the sheet.
     *
     * @param Sheet $sheet
     */
    protected function styleSheet(Sheet $sheet): void
    {
        $range = 'A1:F1';

        $this->styleBold($sheet, $range)
            ->styleFill($sheet, $range)
            ->styleBorder($sheet, $range);
    }
}
