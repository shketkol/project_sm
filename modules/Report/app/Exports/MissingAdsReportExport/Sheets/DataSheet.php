<?php

namespace Modules\Report\Exports\MissingAdsReportExport\Sheets;

use App\Exports\Sheets\BaseSheet;
use App\Helpers\DateFormatHelper;
use Iterator;
use Maatwebsite\Excel\Concerns\FromIterator;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use Modules\Report\Exports\MissingAdsReportExport\Data\ReportExportData;
use stdClass;

class DataSheet extends BaseSheet implements FromIterator, ShouldAutoSize, WithStrictNullComparison, WithTitle
{
    /**
     * @var ReportExportData
     */
    protected $data;

    /**
     * @param ReportExportData $data
     */
    public function __construct(ReportExportData $data)
    {
        $this->data = $data;
    }

    /**
     * Get sheet title.
     *
     * @return string
     */
    public function title(): string
    {
        return __('report::labels.export.sheets.data.title');
    }

    /**
     * Get sheet data.
     *
     * @return Iterator
     */
    public function iterator(): Iterator
    {
        // Headings
        yield $this->getHeadings();

        // Campaigns
        foreach ($this->data->campaigns as $campaign) {
            yield $this->mapCampaigns($campaign);
        }
    }

    /**
     * @param stdClass $campaign
     *
     * @return array
     */
    protected function mapCampaigns(stdClass $campaign): array
    {
        return [
            $campaign->company_name,
            $campaign->name,
            __("campaign::labels.campaign.statuses.{$campaign->campaign_status}"),
            DateFormatHelper::formattedDateTime24h($campaign->created_at, true),
            DateFormatHelper::toMonthDayYear($campaign->submitted_at),
            $campaign->fail_created_at
                ? DateFormatHelper::formattedDateTime24h($campaign->fail_created_at, true)
                : null,
            $campaign->success_created_at
                ? DateFormatHelper::formattedDateTime24h($campaign->success_created_at, true)
                : null,
            DateFormatHelper::toMonthDayYear($campaign->date_start),
            DateFormatHelper::toMonthDayYear($campaign->date_end),
            $campaign->budget,
            $this->getAdStatusLabel($campaign->ad_approved_status ?: $campaign->ad_rejected_status),
            $campaign->error_messages
                ? str_replace(['"],["', '["', '"]'], ['; ', '', ''], $campaign->error_messages)
                : null,
            $campaign->count_campaigns,
        ];
    }

    /**
     * Get headings for the table.
     *
     * @return array
     */
    protected function getHeadings(): array
    {
        return [
            __('labels.advertiser'),
            __('labels.campaign_name'),
            __('labels.type'),
            __('report::labels.campaign_created'),
            __('labels.submit_date'),
            __('report::labels.upload_fall_at'),
            __('report::labels.upload_success_at'),
            __('report::labels.campaign_start_date'),
            __('report::labels.campaign_end_date'),
            __('labels.discounted_budget'),
            __('report::labels.ad_status'),
            __('report::labels.upload_fail_reason'),
            __('report::labels.campaigns_to_date'),
        ];
    }

    /**
     * Add styling to the sheet.
     *
     * @param Sheet $sheet
     */
    protected function styleSheet(Sheet $sheet): void
    {
        $range = 'A1:M1';

        $this->styleBold($sheet, $range)
            ->styleFill($sheet, $range)
            ->styleBorder($sheet, $range);
    }

    /**
     * @param null|string $adStatus
     *
     * @return string
     */
    private function getAdStatusLabel(?string $adStatus): string
    {
        return $adStatus
            ? __("creative::labels.creative.statuses.{$adStatus}")
            : __('report::labels.no_ad');
    }
}
