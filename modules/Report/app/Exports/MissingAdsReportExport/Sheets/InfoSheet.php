<?php

namespace Modules\Report\Exports\MissingAdsReportExport\Sheets;

use App\Exports\Sheets\BaseSheet;
use App\Helpers\DateFormatHelper;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use Modules\Report\Exports\MissingAdsReportExport\Data\ReportExportData;

class InfoSheet extends BaseSheet implements FromArray, ShouldAutoSize, WithTitle
{
    /**
     * @var ReportExportData
     */
    protected $data;

    /**
     * @param ReportExportData $data
     */
    public function __construct(ReportExportData $data)
    {
        $this->data = $data;
    }

    /**
     * Get sheet title.
     *
     * @return string
     */
    public function title(): string
    {
        return __('report::labels.export.sheets.info.title');
    }

    /**
     * Get sheet data.
     *
     * @return array
     */
    public function array(): array
    {
        return [
            [
                __('report::labels.export.sheets.info.missing_ads_report', [
                    'company_full_name' => config('general.company_full_name'),
                ]),
            ],
            [
                __('report::labels.export.sheets.info.report_timezone'),
                config('report.timezone'),
            ],
            [
                __('report::labels.export.sheets.info.date_generated'),
                DateFormatHelper::formattedDateTime($this->data->createdAt),
            ],
            [
                __('report::labels.export.sheets.info.generated_by'),
                $this->data->createdBy
            ],
        ];
    }

    /**
     * Add styling to the sheet.
     *
     * @param Sheet $sheet
     *
     * @return void
     */
    protected function styleSheet(Sheet $sheet): void
    {
        $this->styleBold($sheet, 'A1:A4')
            ->styleFill($sheet, 'A2:A4');
    }
}
