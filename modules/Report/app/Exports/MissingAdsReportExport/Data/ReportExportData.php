<?php

namespace Modules\Report\Exports\MissingAdsReportExport\Data;

use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * @property string                 $name
 * @property bool                   $isAdmin
 * @property MissingAdsDataProvider $campaigns
 * @property Carbon                 $createdAt
 * @property string                 $createdBy
 */
class ReportExportData
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the value.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function __get(string $key)
    {
        return Arr::get($this->data, $key);
    }
}
