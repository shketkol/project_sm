<?php

namespace Modules\Report\Exports\MissingAdsReportExport\Data;

use Carbon\CarbonImmutable;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Creative\Models\CreativeStatus;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportType;

class MissingAdsDataProvider
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var Report
     */
    protected $report;

    /**
     * @param Report $report
     *
     * @return array
     */
    public function setReport(Report $report): array
    {
        $this->report = $report;
        $this->load();
        return $this->data;
    }

    /**
     * Load campaigns from DB.
     *
     * @return void
     */
    protected function load(): void
    {
        $now = CarbonImmutable::now()->startOfDay();
        $excludedAdvertisers = config('advertiser.test_advertisers_external_ids');
        $countShiftDays = config('report.count_days_fom_missing_ads_report');

        if ($this->report->type_id === ReportType::ID_MISSING_ADS_DOWNLOAD) {
            $queryDates = DB::raw("cmp.date_start BETWEEN '{$this->report->date_start->toDateTimeString()}' AND" .
                "'{$this->report->date_end->toDateTimeString()}'");
        } else {
            $queryDates = DB::raw("cmp.date_start <= '{$now->addDays($countShiftDays)->toDateTimeString()}'");
        }

        DB::table('campaigns AS cmp')
            ->select(
                'cmp.id AS campaign_id',
                'u.company_name',
                'cmp.name',
                'cmp.created_at',
                'cmps.name AS campaign_status',
                'cmp.submitted_at',
                'crer.created_at AS fail_created_at',
                DB::raw(sprintf(
                    'IF(cr_attached.status_id = %d, cc_attached.created_at, NULL) AS success_created_at',
                    CreativeStatus::ID_APPROVED
                )),
                'cmp.date_start',
                'cmp.date_end',
                'cmp.budget',
                'crs_rejected.name AS ad_rejected_status',
                'crs_approved.name AS ad_approved_status',
                DB::raw("(SELECT COUNT(id)
                                FROM campaigns AS c
                                WHERE c.user_id = cmp.user_id
                                AND c.status_id != " . CampaignStatus::ID_DRAFT . "
                                AND {$queryDates}) AS count_campaigns"),
                DB::raw('GROUP_CONCAT(crer.messages) AS error_messages')
            )
            ->join('users AS u', 'cmp.user_id', '=', 'u.id')
            ->leftJoin('campaign_statuses AS cmps', 'cmp.status_id', '=', 'cmps.id')
            ->leftJoin('creative_errors AS crer', 'cmp.id', '=', 'crer.campaign_id')
            ->leftJoin('creatives AS cr_rejected', 'crer.creative_id', '=', 'cr_rejected.id')
            ->leftJoin('creative_statuses AS crs_rejected', 'cr_rejected.status_id', '=', 'crs_rejected.id')
            ->leftJoin('campaign_creative AS cc_attached', 'cmp.id', '=', 'cc_attached.campaign_id')
            ->leftJoin('creatives AS cr_attached', 'cc_attached.creative_id', '=', 'cr_attached.id')
            ->leftJoin('creative_statuses AS crs_approved', 'cr_attached.status_id', '=', 'crs_approved.id')
            ->whereIn('cmp.status_id', [CampaignStatus::ID_PENDING_APPROVAL, CampaignStatus::ID_DRAFT])
            ->whereRaw($queryDates)
            ->where(function (Builder $query): void {
                $query
                    ->where('cr_rejected.status_id', CreativeStatus::ID_REJECTED)
                    ->orWhereNull('cr_rejected.status_id')
                    ->orWhere('cr_attached.status_id', CreativeStatus::ID_APPROVED);
            })
            ->whereNotIn('u.external_id', $excludedAdvertisers)
            ->groupBy('cmp.id', 'cr_rejected.id', 'fail_created_at', 'cr_attached.status_id', 'cc_attached.created_at')
            ->orderBy('u.company_name')
            ->orderBy('fail_created_at')
            ->chunk(100, function ($campaigns) {
                $this->data = array_merge($this->data, $campaigns->all());
            });
    }
}
