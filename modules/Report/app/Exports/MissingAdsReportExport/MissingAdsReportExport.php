<?php

namespace Modules\Report\Exports\MissingAdsReportExport;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Modules\Report\Exports\MissingAdsReportExport\Data\ReportExportData;
use Modules\Report\Exports\MissingAdsReportExport\Sheets\DataSheet;
use Modules\Report\Exports\MissingAdsReportExport\Sheets\InfoSheet;
use Modules\Report\Exports\Traits\RemoveFormulaCharacters;

class MissingAdsReportExport implements WithMultipleSheets, WithCustomValueBinder
{
    use Exportable, RemoveFormulaCharacters;

    /**
     * @var ReportExportData
     */
    protected $data;

    /**
     * @param ReportExportData $data
     */
    public function __construct(ReportExportData $data)
    {
        $this->data = $data;
    }

    /**
     * Get sheets for the file.
     *
     * @return array
     */
    public function sheets(): array
    {
        return [
            new InfoSheet($this->data),
            new DataSheet($this->data),
        ];
    }
}
