<?php

namespace Modules\Report\Exports\ReportExport;

use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Modules\Report\Exports\ReportExport\Data\ReportExportData;
use Modules\Report\Exports\ReportExport\Sheets\DataSheet;
use Modules\Report\Exports\ReportExport\Sheets\InfoSheet;
use Modules\Report\Exports\Traits\RemoveFormulaCharacters;

class ReportExport implements WithMultipleSheets, WithCustomValueBinder
{
    use RemoveFormulaCharacters;

    /**
     * @var ReportExportData
     */
    protected $data;

    /**
     * @param ReportExportData $data
     */
    public function __construct(ReportExportData $data)
    {
        $this->data = $data;
    }

    /**
     * Get sheets for the file.
     *
     * @return array
     */
    public function sheets(): array
    {
        return [
            new InfoSheet($this->data),
            new DataSheet($this->data),
        ];
    }
}
