<?php

namespace Modules\Report\Exports\ReportExport\Data;

use App\Helpers\DateFormatHelper;
use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Admin\Campaign\AdminCampaignSearch;
use Modules\Haapi\DataTransferObjects\Campaign\AdminCampaignSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Iterators\PaginationIterator;
use Modules\Report\Exceptions\NotAdminNorLiveCampaignsException;
use Modules\Report\Helpers\CampaignsHelper;
use Modules\Report\Models\Report;
use Iterator;
use Modules\Report\Models\ReportType;
use Psr\Log\LoggerInterface;

class CampaignsDataIterator extends PaginationIterator implements Iterator
{
    use ActAsAdmin;

    /**
     * @var string|null
     */
    protected $accountId;

    /**
     * @var int
     */
    protected $adminId;

    /**
     * @var AdminCampaignSearch
     */
    protected $campaignSearch;

    /**
     * @var Report|null
     */
    protected $report;

    /**
     * @var string
     */
    protected $startDate;

    /**
     * @var string
     */
    protected $endDate;

    /**
     * @var string
     */
    protected $startDateImpressions;

    /**
     * @var string
     */
    protected $endDateImpressions;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var CampaignsHelper
     */
    protected $campaignsHelper;

    /**
     * @param AdminCampaignSearch $campaignSearch
     * @param LoggerInterface     $log
     * @param CampaignsHelper     $campaignsHelper
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(
        AdminCampaignSearch $campaignSearch,
        LoggerInterface $log,
        CampaignsHelper $campaignsHelper
    ) {
        $this->campaignSearch = $campaignSearch;
        $this->adminId = $this->authAsAdmin();
        $this->log = $log;
        $this->campaignsHelper = $campaignsHelper;
    }

    /**
     * @param Report $report
     *
     * @return CampaignsDataIterator
     * @throws NotAdminNorLiveCampaignsException
     * @throws \App\Exceptions\BaseException
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function setReport(Report $report): self
    {
        $this->report = $report;
        $this->accountId = $this->getAccountId();
        $this->startDate = DateFormatHelper::haapiWithMilliseconds($this->report->getDateFrom());
        $this->endDate = DateFormatHelper::haapiWithMilliseconds($this->report->getDateTo());
        $this->startDateImpressions = $this->report->getDateFrom()->format(config('date.format.daapi'));
        $this->endDateImpressions = $this->report->getDateTo()->format(config('date.format.daapi'));

        if ($report->type_id === ReportType::ID_SCHEDULED) {
            return $this->setScheduledReport();
        }

        return $this;
    }

    /**
     * @return $this
     * @throws NotAdminNorLiveCampaignsException
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function setScheduledReport(): self
    {
        // if admin report
        if ($this->report->user->isAdmin()) {
            $this->log->info('Admin report. Dates as usual.', [
                'report_id'  => $this->report->id,
                'start_date' => $this->startDate,
                'end_date'   => $this->endDate,
            ]);

            return $this;
        }

        // if report has any live campaigns send haapi date as normal
        if ($this->campaignsHelper->hasLiveCampaigns($this->report)) {
            $this->log->info('Advertiser report has live campaigns. Dates as usual.', [
                'report_id'  => $this->report->id,
                'start_date' => $this->startDate,
                'end_date'   => $this->endDate,
            ]);

            return $this;
        }

        // if report has any recently completed campaigns send haapi date modified
        // we need this check because request could come not only from schedule command
        if ($this->campaignsHelper->hasRecentlyCompletedCampaigns($this->report)) {
            [$startDate, $endDate] = $this->campaignsHelper->getDatesForRecentlyCompletedCampaigns($this->report);
            $this->startDate = DateFormatHelper::haapiWithMilliseconds($startDate);
            $this->endDate = DateFormatHelper::haapiWithMilliseconds($endDate);

            $this->log->info('Advertiser report has only recently completed campaigns. Dates are modified.', [
                'report_id'  => $this->report->id,
                'start_date' => $this->startDate,
                'end_date'   => $this->endDate,
            ]);

            return $this;
        }

        $message = 'Report not belongs to admin nor has no live or recently completed campaigns.';
        $this->log->warning($message, ['report_id' => $this->report->id]);
        throw new NotAdminNorLiveCampaignsException($message);
    }

    /**
     * Send request to load data.
     *
     * @return HaapiResponse
     * @throws \App\Exceptions\BaseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    protected function load(): HaapiResponse
    {
        return $this->campaignSearch->handle(
            $this->createParam(AdminCampaignSearchParam::class, [
                'accountId'                => $this->accountId,
                'impressionCountStartTime' => $this->startDateImpressions,
                'impressionCountEndTime'   => $this->endDateImpressions,
                'campaignStartBefore'      => $this->endDate,
                'campaignEndAfter'         => $this->startDate,
            ]),
            $this->adminId
        );
    }

    /**
     * Get key of data that should be iterated.
     *
     * @return string
     */
    protected function getResponseKey(): string
    {
        return 'campaigns';
    }

    /**
     * Get account id for the request.
     *
     * @return null|string
     */
    protected function getAccountId(): ?string
    {
        if (!$this->report->user || ($this->report->user->isAdmin() && !$this->report->getAdvertiserExternalId())) {
            return null;
        }

        return $this->report->getAdvertiserExternalId() ?: $this->report->user->account_external_id;
    }
}
