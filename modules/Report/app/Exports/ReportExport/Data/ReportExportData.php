<?php

namespace Modules\Report\Exports\ReportExport\Data;

use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * Class ReportExportData
 * @package Modules\Report\Exports\ReportExport\Data
 * @property bool $isAdmin
 * @property Carbon $dateFrom
 * @property Carbon $dateTo
 * @property CampaignsDataIterator $campaigns
 * @property Carbon $createdAt
 * @property string $createdBy
 * @property string $name
 */
class ReportExportData
{
    /**
     * @var array
     */
    protected $data;

    /**
     * ReportExportData constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the value.
     *
     * @param string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return Arr::get($this->data, $key);
    }
}
