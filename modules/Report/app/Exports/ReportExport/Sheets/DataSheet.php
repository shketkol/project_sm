<?php

namespace Modules\Report\Exports\ReportExport\Sheets;

use App\Exports\Sheets\BaseSheet;
use App\Helpers\DateFormatHelper;
use Carbon\CarbonInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Iterator;
use Maatwebsite\Excel\Concerns\FromIterator;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use Modules\Creative\Models\CreativeStatus;
use Modules\Report\Exports\ReportExport\Data\ReportExportData;

class DataSheet extends BaseSheet implements FromIterator, ShouldAutoSize, WithStrictNullComparison, WithTitle
{
    /**
     * Content used for empty cells.
     */
    private const EMPTY_CELL = '-';

    /**
     * Mapping for creative status.
     */
    private const CREATIVE_STATUS_MAP = [
        'approved'         => CreativeStatus::APPROVED,
        'draft'            => CreativeStatus::DRAFT,
        'missing'          => CreativeStatus::MISSING,
        'pending_approval' => CreativeStatus::PENDING_APPROVAL,
        'processing'       => CreativeStatus::PROCESSING,
        'rejected'         => CreativeStatus::REJECTED,
    ];

    /**
     * @var string
     */
    protected $currency;

    /**
     * @var ReportExportData
     */
    protected $data;

    /**
     * @var bool
     */
    protected $isAdmin;

    /**
     * @param ReportExportData $data
     */
    public function __construct(ReportExportData $data)
    {
        $this->data = $data;

        $this->isAdmin = (bool)$this->data->isAdmin;
        $this->currency = config('currency.code');
    }

    /**
     * Get sheet title.
     *
     * @return string
     */
    public function title(): string
    {
        return __('report::labels.export.sheets.data.title');
    }

    /**
     * Get sheet data.
     *
     * @return Iterator
     */
    public function iterator(): Iterator
    {
        // Headings
        yield $this->getHeadings();

        // Campaigns
        $campaigns = $this->data->campaigns;
        foreach ($campaigns as $campaign) {
            yield $this->mapCampaign($campaign);
        }
    }

    /**
     * Format dates.
     *
     * @param string          $title
     * @param CarbonInterface $date
     *
     * @return string
     */
    protected function mapDate(string $title, CarbonInterface $date): string
    {
        return $title . ': ' . DateFormatHelper::formattedDateTime($date);
    }

    /**
     * Map campaign data to display in the sheet.
     * (e.g. dates should be formatted in the specified way).
     *
     * @param array $campaign
     *
     * @return array
     */
    protected function mapCampaign(array $campaign): array
    {
        return array_map(function ($cellValue) {
            return ($cellValue || $cellValue === 0 || $cellValue === 0.0) ? $cellValue : self::EMPTY_CELL;
        }, array_merge(
            [
                $campaign['name'],
            ],
            $this->isAdmin ? [Arr::get($campaign, 'accountName')] : [],
            [
                Arr::has($campaign, 'orderDate') ? DateFormatHelper::toMonthDayYear($campaign['orderDate']) : null,
                Arr::get($campaign, 'campaignStatus'),
                $this->mapCreativeStatus(Arr::get($campaign, 'creativeStatus')),
                isset($campaign['startDate']) ? DateFormatHelper::toMonthDayYear($campaign['startDate']) : null,
                isset($campaign['endDate']) ? DateFormatHelper::toMonthDayYear($campaign['endDate']) : null,
                Arr::get($campaign, 'impressions'),
                Arr::get($campaign, 'quantity'), // booked impressions
                !is_null($campaign['discountedUnitCost']) ?
                    Arr::get($campaign, 'discountedUnitCost') :
                    Arr::get($campaign, 'unitCost'), //CPM
                !is_null($campaign['discountedTotalCost']) ?
                    Arr::get($campaign, 'discountedTotalCost') :
                    Arr::get($campaign, 'totalCost'),
                !is_null($campaign['displayDiscountedBudget']) ?
                    Arr::get($campaign, 'displayDiscountedBudget') :
                    Arr::get($campaign, 'displayBudget'),
            ]
        ));
    }

    /**
     * Returns human readable creative status.
     *
     * @param null|string $creativeStatus
     *
     * @return string
     */
    protected function mapCreativeStatus(?string $creativeStatus): string
    {
        $internalStatus = Arr::get(self::CREATIVE_STATUS_MAP, Str::lower($creativeStatus));

        if (!$internalStatus) {
            return self::EMPTY_CELL;
        }

        return __("creative::labels.creative.statuses.{$internalStatus}");
    }

    /**
     * Get headings for the table.
     *
     * @return array
     */
    protected function getHeadings(): array
    {
        return array_merge(
            [
                __('campaign::labels.campaign_name'),
            ],
            $this->isAdmin ? [__('labels.advertiser')] : [],
            [
                __('labels.date_submitted'),
                __('campaign::labels.status'),
                __('creative::labels.status'),
                __('campaign::labels.start_date'),
                __('campaign::labels.end_date'),
                __('labels.delivered_impressions'),
                __('labels.target_impressions'),
                __('labels.discounted_cpm'),
                __('labels.total_cost'),
                __('labels.discounted_budget'),
            ]
        );
    }

    /**
     * Add styling to the sheet.
     *
     * @param Sheet $sheet
     */
    protected function styleSheet(Sheet $sheet): void
    {
        $row = '1';
        $startColumn = 'A';
        $endColumn = chr(ord($startColumn) + count($this->getHeadings()) - 1);

        $range = "{$startColumn}{$row}:{$endColumn}{$row}";

        $this->styleBold($sheet, $range)
            ->styleFill($sheet, $range)
            ->styleBorder($sheet, $range)
            ->setColumnWidth($sheet, $startColumn, 50);
    }
}
