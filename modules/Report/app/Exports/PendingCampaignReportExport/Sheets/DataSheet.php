<?php

namespace Modules\Report\Exports\PendingCampaignReportExport\Sheets;

use App\Exports\Sheets\BaseSheet;
use App\Helpers\DateFormatHelper;
use Iterator;
use Maatwebsite\Excel\Concerns\FromIterator;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use Modules\Report\Exports\PendingCampaignReportExport\Data\ReportExportData;

class DataSheet extends BaseSheet implements FromIterator, ShouldAutoSize, WithStrictNullComparison, WithTitle
{
    /**
     * @var ReportExportData
     */
    protected $data;

    /**
     * @param ReportExportData $data
     */
    public function __construct(ReportExportData $data)
    {
        $this->data = $data;
    }

    /**
     * Get sheet title.
     *
     * @return string
     */
    public function title(): string
    {
        return __('report::labels.export.sheets.data.title');
    }

    /**
     * Get sheet data.
     *
     * @return Iterator
     */
    public function iterator(): Iterator
    {
        // Headings
        yield $this->getHeadings();

        // Advertisers
        foreach ($this->data->campaigns as $campaign) {
            yield $this->mapCampaigns($campaign);
        }
    }

    /**
     * @param $campaign
     *
     * @return array
     */
    protected function mapCampaigns($campaign): array
    {
        return [
            $campaign->company_name,
            $campaign->name,
            $campaign->updated_at,
            __("campaign::labels.steps.$campaign->step_stopped"),
            !empty($campaign->date_start) ? DateFormatHelper::toMonthDayYear($campaign->date_start) : '',
            !empty($campaign->date_end) ? DateFormatHelper::toMonthDayYear($campaign->date_end) : '',
            $campaign->has_ad,
            $campaign->budget,
            $campaign->impressions,
            $campaign->has_purchase,
        ];
    }

    /**
     * Get headings for the table.
     *
     * @return array
     */
    protected function getHeadings(): array
    {
        return [
            __('labels.advertiser'),
            __('labels.campaign_name'),
            __('labels.last_edit_date'),
            __('labels.campaign_flow_stage'),
            __('labels.start_date'),
            __('labels.end_date'),
            __('labels.has_ad'),
            __('labels.budget'),
            __('labels.target_impressions'),
            __('labels.has_prior_purchase'),
        ];
    }

    /**
     * Add styling to the sheet.
     *
     * @param Sheet $sheet
     */
    protected function styleSheet(Sheet $sheet): void
    {
        $range = 'A1:J1';

        $this->styleBold($sheet, $range)
            ->styleFill($sheet, $range)
            ->styleBorder($sheet, $range);
    }
}
