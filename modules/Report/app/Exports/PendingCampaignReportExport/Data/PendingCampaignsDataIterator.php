<?php

namespace Modules\Report\Exports\PendingCampaignReportExport\Data;

use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Report\Models\Report;

class PendingCampaignsDataIterator
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var Report|null
     */
    protected $report;

    /**
     * Set report property.
     *
     * @return array
     */
    public function setReport(Report $report): array
    {
        $this->report = $report;
        $this->load();

        return $this->data;
    }

    /**
     * Load campaigns from local DB.
     *
     * @return void
     * @throws \App\Exceptions\BaseException
     */
    protected function load(): void
    {
        $statuses = implode(',', [CampaignStatus::ID_COMPLETED, CampaignStatus::ID_LIVE]);
        $excludedAdvertisers = config('advertiser.test_advertisers_external_ids');

        DB::table('campaigns AS c')->select(
            'u.id AS user_id',
            'u.external_id AS user_ext_id',
            'u.company_name',
            'c.id AS campaign_id',
            'c.name',
            'c.updated_at',
            'cs.name AS step_stopped',
            'c.date_start',
            'c.date_end',
            DB::raw('IF(ccr.creative_id IS NOT NULL, \'Y\', \'N\') AS has_ad'),
            'c.budget',
            'c.impressions',
            DB::raw('IF(cp.has_purchase IS NOT NULL, \'Y\', \'N\') AS has_purchase')
        )
            ->join('users AS u', 'c.user_id', '=', 'u.id')
            ->join('campaign_steps AS cs', 'c.step_id', '=', 'cs.id')
            ->leftJoin('campaign_creative AS ccr', 'c.id', '=', 'ccr.campaign_id')
            ->leftJoin(
                DB::raw("(SELECT user_id, 1 as has_purchase FROM campaigns WHERE status_id IN ($statuses)
                GROUP BY user_id) AS cp"),
                function ($join) {
                    $join->on('u.id', '=', 'cp.user_id');
                }
            )
            ->where('c.status_id', '=', CampaignStatus::ID_DRAFT)
            ->where('c.updated_at', '>=', $this->report->getDateFrom())
            ->where('c.updated_at', '<=', $this->report->getDateTo())
            ->whereNotIn('u.external_id', $excludedAdvertisers)
            ->orderBy('user_id')
            ->chunk(100, function ($campaigns) {
                foreach ($campaigns as $campaign) {
                    array_push($this->data, $campaign);
                }
            });
    }
}
