<?php

namespace Modules\Report\Exports\ReportMetricsExport\Sheets;

use App\Exports\Sheets\BaseSheet;
use App\Helpers\DateFormatHelper;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Modules\Campaign\Models\Campaign;
use Modules\User\Models\User;

class OverviewSheet extends BaseSheet implements FromView, ShouldAutoSize, WithTitle
{
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * @var User
     */
    private $user;

    /**
     * @param Campaign $campaign
     * @param User     $user
     */
    public function __construct(Campaign $campaign, User $user)
    {
        $this->campaign = $campaign;
        $this->user = $user;
    }

    /**
     * Get sheet title.
     *
     * @return string
     */
    public function title(): string
    {
        return __('report::report_metrics.sheets.overview.title');
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('report::reports.metrics.overview', [
            'campaign'      => $this->campaign,
            'periodStart'   => DateFormatHelper::formattedDateTime(
                DateFormatHelper::convertToTimezone($this->campaign->date_start->startOfDay())
            ),
            'periodEnd'     => DateFormatHelper::formattedDateTime($this->getEndDate()),
            'dateGenerated' => DateFormatHelper::formattedDateTime(DateFormatHelper::now()),
            'user'          => $this->user,
        ]);
    }

    /**
     * The Report End Date should be the date of the generation or the campaign end date, whichever ends first.
     * Must be converted both "now" date and "campaign end date" to the same timezone before comparison.
     *
     * @return Carbon
     */
    private function getEndDate(): Carbon
    {
        $endDate = DateFormatHelper::convertToTimezone($this->campaign->date_end->endOfDay());
        $now = DateFormatHelper::now();

        if ($now->lt($endDate)) {
            return $now;
        }

        return $endDate;
    }
}
