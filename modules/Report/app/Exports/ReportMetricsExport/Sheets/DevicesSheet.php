<?php

namespace Modules\Report\Exports\ReportMetricsExport\Sheets;

use App\Exports\Sheets\BaseSheet;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Modules\Campaign\Models\Campaign;

class DevicesSheet extends BaseSheet implements FromView, ShouldAutoSize, WithTitle
{
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * @var array
     */
    private $metrics;

    /**
     * @param Campaign $campaign
     * @param array    $metrics
     */
    public function __construct(Campaign $campaign, array $metrics)
    {
        $this->campaign = $campaign;
        $this->metrics = $metrics;
    }

    /**
     * Get sheet title.
     *
     * @return string
     */
    public function title(): string
    {
        return __('report::report_metrics.sheets.devices.title');
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('report::reports.metrics.devices', [
            'campaign'    => $this->campaign,
            'impressions' => $this->metrics,
        ]);
    }
}
