<?php

namespace Modules\Report\Exports\ReportMetricsExport\Sheets;

use App\Exports\Sheets\BaseSheet;
use App\Helpers\DateFormatHelper;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Models\CreativeStatus;

class ReportSheet extends BaseSheet implements FromView, ShouldAutoSize, WithTitle
{
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * @var array
     */
    private $statistics;

    /**
     * @param Campaign $campaign
     * @param array    $statistics
     */
    public function __construct(Campaign $campaign, array $statistics)
    {
        $this->campaign = $campaign;
        $this->statistics = $statistics;
    }

    /**
     * Get sheet title.
     *
     * @return string
     */
    public function title(): string
    {
        return __('report::report_metrics.sheets.report.title');
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('report::reports.metrics.report', [
            'campaign'             => $this->campaign,
            'submitDate'           => DateFormatHelper::toMonthDayYear($this->campaign->submitted_at, true),
            'startDate'            => DateFormatHelper::toMonthDayYear($this->campaign->date_start->startOfDay()),
            'endDate'              => DateFormatHelper::toMonthDayYear($this->campaign->date_end->endOfDay()),
            'creativeStatus'       => $this->getCreativeStatus(),
            'deliveredImpressions' => Arr::get($this->statistics, 'deliveredImpressions'),
            'totalCost'            => number_format(Arr::get($this->statistics, 'cost'), 2),
        ]);
    }

    /**
     * @return string
     */
    private function getCreativeStatus(): string
    {
        $creative = $this->campaign->getActiveCreative();

        if (is_null($creative)) {
            return __('creative::labels.creative.statuses.' . CreativeStatus::MISSING);
        }

        return __("creative::labels.creative.statuses.{$creative->status->name}");
    }
}
