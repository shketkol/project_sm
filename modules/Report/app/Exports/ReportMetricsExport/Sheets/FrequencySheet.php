<?php

namespace Modules\Report\Exports\ReportMetricsExport\Sheets;

use App\Exports\Sheets\BaseSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Modules\Campaign\Models\Campaign;

class FrequencySheet extends BaseSheet implements FromView, ShouldAutoSize, WithTitle
{
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get sheet title.
     *
     * @return string
     */
    public function title(): string
    {
        return __('report::report_metrics.sheets.frequency.title');
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('report::reports.metrics.frequency', [
            'campaign' => $this->campaign,
        ]);
    }
}
