<?php

namespace Modules\Report\Exports\ReportMetricsExport;

use App\Exports\Sheets\BaseSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Modules\Report\Exports\Traits\RemoveFormulaCharacters;

class ReportMetricsExport implements WithMultipleSheets, WithCustomValueBinder
{
    use Exportable, RemoveFormulaCharacters;

    /**
     * @var BaseSheet[]
     */
    private $sheets;

    /**
     * @param BaseSheet[] $sheets
     */
    public function __construct(BaseSheet ...$sheets)
    {
        $this->sheets = $sheets;
    }

    /**
     * Get sheets for the file.
     *
     * @return BaseSheet[]
     */
    public function sheets(): array
    {
        return $this->sheets;
    }
}
