<?php

namespace Modules\Report\Exports\Traits;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

trait RemoveFormulaCharacters
{
    /**
     * Cell that begins with any of the following characters counts as a excel formula
     *
     * @var string[]
     * @see https://owasp.org/www-community/attacks/CSV_Injection
     */
    private $formulaCharacters = ['@', '=', '-', '+'];

    /**
     * @param Cell  $cell
     * @param mixed $value
     *
     * @return bool
     */
    public function bindValue(Cell $cell, $value): bool
    {
        // remove characters that might be a formula start
        $value = ltrim($value, implode('', $this->formulaCharacters));

        // pass the value to default value binder
        return $this->getValueBinder()->bindValue($cell, $value);
    }

    /**
     * @return DefaultValueBinder
     */
    private function getValueBinder(): DefaultValueBinder
    {
        return app(DefaultValueBinder::class);
    }
}
