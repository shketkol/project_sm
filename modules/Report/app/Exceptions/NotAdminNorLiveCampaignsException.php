<?php

namespace Modules\Report\Exceptions;

use App\Exceptions\BaseException;

class NotAdminNorLiveCampaignsException extends BaseException
{
    protected $logLevel = BaseException::LOG_LEVEL_WARNING;
}
