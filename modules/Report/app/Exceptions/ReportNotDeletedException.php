<?php

namespace Modules\Report\Exceptions;

use App\Exceptions\ModelNotDeletedException;

class ReportNotDeletedException extends ModelNotDeletedException
{
    //
}
