<?php

namespace Modules\Report\Exceptions;

use App\Exceptions\BaseException;

class ReportNotGeneratedException extends BaseException
{
    //
}
