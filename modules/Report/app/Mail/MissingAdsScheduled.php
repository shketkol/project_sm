<?php

namespace Modules\Report\Mail;

use App\Mail\Mail;

class MissingAdsScheduled extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('report::emails.missing_ads_scheduled.subject', [
                'name' => $this->getPayloadValue('reportName'),
            ]))
            ->view('report::emails.missing-ads-scheduled')
            ->with($this->payload);
    }
}
