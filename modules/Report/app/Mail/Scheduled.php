<?php

namespace Modules\Report\Mail;

use App\Mail\Mail;
use App\Models\QueuePriority;

class Scheduled extends Mail
{
    /**
     * Mail constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload = [])
    {
        parent::__construct($payload);
        $this->onQueue(QueuePriority::low());
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('report::emails.scheduled.subject', [
                'name' => $this->getPayloadValue('reportName'),
            ]))
            ->view('report::emails.scheduled')
            ->with($this->payload);
    }
}
