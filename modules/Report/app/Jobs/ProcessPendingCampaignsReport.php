<?php

namespace Modules\Report\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use Modules\Report\Actions\GeneratePendingCampaignsReportAction;
use Modules\Report\Exceptions\ReportNotGeneratedException;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use SM\SMException;

/**
 * @package Modules\Report\Jobs
 */
class ProcessPendingCampaignsReport extends Job implements ShouldQueue
{
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 600;

    /**
     * @var Report
     */
    protected $report;

    /**
     * Create a new job instance.
     *
     * @param Report $report
     * @return void
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @param GeneratePendingCampaignsReportAction $generateReportAction
     *
     * @return void
     * @throws CanNotApplyStatusException
     * @throws SMException
     * @throws \App\Exceptions\BaseException
     * @throws ReportNotGeneratedException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function handle(GeneratePendingCampaignsReportAction $generateReportAction): void
    {
        try {
            $this->report->applyInternalStatus(ReportStatus::SUBMITTED);

            $generateReportAction->handle($this->report);
        } catch (Throwable $exception) {
            $this->report->applyInternalStatus(ReportStatus::FAILED);

            throw ReportNotGeneratedException::createFrom($exception);
        }
    }

    /**
     * @return void
     * @throws CanNotApplyStatusException
     * @throws SMException
     */
    public function failed(): void
    {
        if ($this->report) {
            $this->report->applyInternalStatus(ReportStatus::FAILED);
        }
    }
}
