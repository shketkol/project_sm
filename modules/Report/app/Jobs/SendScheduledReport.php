<?php

namespace Modules\Report\Jobs;

use App\Exceptions\InvalidArgumentException;
use App\Jobs\Job;
use App\Models\QueuePriority;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Notifications\Dispatcher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Report\Mail\Scheduled;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportEmail;
use Modules\Report\Models\ReportType;
use Modules\Report\Notifications\MissingAdsScheduledCompleted;
use Modules\Report\Notifications\ScheduledCompleted;

class SendScheduledReport extends Job implements ShouldQueue
{
    protected const NOTIFICATIONS = [
        ReportType::ID_SCHEDULED             => ScheduledCompleted::class,
        ReportType::ID_MISSING_ADS_SCHEDULED => MissingAdsScheduledCompleted::class,
    ];

    /**
     * @var Report
     */
    protected $report;

    /**
     * @var string
     */
    protected $notificationClass;

    /**
     * Create a new job instance.
     *
     * @param Report $report
     *
     * @return void
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
        $this->onQueue(QueuePriority::low());
    }

    /**
     * Execute the job.
     *
     * @param Logger $log
     * @param Mailer $mailer
     *
     * @return void
     * @throws \App\Exceptions\BaseException
     */
    public function handle(Logger $log, Mailer $mailer): void
    {
        $this->setNotificationClass($this->report);
        $this->sendEmails($log, $mailer);
        $this->sendOnSiteNotifications($log);
    }

    /**
     * @param Logger $log
     * @param Mailer $mailer
     *
     * @return void
     */
    protected function sendEmails(Logger $log, Mailer $mailer): void
    {
        if (!$this->report->isEmailable()) {
            return;
        }

        /** @var Scheduled $mail */
        $mail = (new $this->notificationClass($this->report))->toMail($this->report->user);

        $this->report->emails->each(function (ReportEmail $reportEmail) use ($mail, $mailer) {
            $mail->resetRecipients();

            $mailer->to($reportEmail->email)->send($mail);
        });

        $log->info('Scheduled report email has been sent.', [
            'report_id'   => $this->report->id,
            'report_type' => $this->report->type->name,
        ]);
    }

    /**
     * Send on-site notifications.
     *
     * @param Logger $log
     */
    protected function sendOnSiteNotifications(Logger $log): void
    {
        if (!$this->report->isNotifiable()) {
            return;
        }

        if (!$this->report->user->isAdvertiser()) {
            $log->info('On-site notification implemented only for advertisers. Skip current user.', [
                'user_id'   => $this->report->user->id,
                'report_id' => $this->report->id,
            ]);

            return;
        }

        /** @var Dispatcher $notification */
        $notification = app(Dispatcher::class);
        $notification->send($this->report->user, new $this->notificationClass($this->report));

        $log->info('Scheduled report on-site notification has been sent.', [
            'user_id'   => $this->report->user->id,
            'report_id' => $this->report->id,
        ]);
    }

    /**
     * @param Report $report
     *
     * @return void
     * @throws InvalidArgumentException
     */
    protected function setNotificationClass(Report $report): void
    {
        $this->notificationClass = Arr::get(self::NOTIFICATIONS, $report->type->id);
        if (is_null($this->notificationClass)) {
            throw new InvalidArgumentException(sprintf(
                'Wrong report type was given - "%s"',
                $report->type->name
            ));
        }
    }
}
