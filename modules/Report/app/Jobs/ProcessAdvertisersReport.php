<?php

namespace Modules\Report\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use Modules\Report\Actions\GenerateAdvertisersReportAction;
use Modules\Report\Exceptions\ReportNotGeneratedException;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use SM\SMException;
use Throwable;

/**
 * @package Modules\Report\Jobs
 */
class ProcessAdvertisersReport extends Job implements ShouldQueue
{
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 600;

    /**
     * @var Report
     */
    protected $report;

    /**
     * Create a new job instance.
     *
     * @param Report $report
     * @return void
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @param GenerateAdvertisersReportAction $generateReportAction
     * @throws \App\Exceptions\BaseException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function handle(GenerateAdvertisersReportAction $generateReportAction)
    {
        try {
            $this->report->applyInternalStatus(ReportStatus::SUBMITTED);

            $generateReportAction->handle($this->report);
        } catch (Throwable $exception) {
            $this->report->applyInternalStatus(ReportStatus::FAILED);

            throw ReportNotGeneratedException::createFrom($exception);
        }
    }

    /**
     * @throws CanNotApplyStatusException
     * @throws SMException
     */
    public function failed(): void
    {
        if ($this->report) {
            $this->report->applyInternalStatus(ReportStatus::FAILED);
        }
    }
}
