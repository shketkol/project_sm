<?php

namespace Modules\Report\Jobs;

use App\Jobs\Job;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use Modules\Report\Actions\GenerateMissingAdsReportAction;
use Modules\Report\Exceptions\ReportNotGeneratedException;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use SM\SMException;

class ProcessMissingAdsReport extends Job
{
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 600;

    /**
     * @var Report
     */
    protected $report;

    /**
     * Create a new job instance.
     *
     * @param Report $report
     * @return void
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @param GenerateMissingAdsReportAction $generateReportAction
     *
     * @return void
     * @throws CanNotApplyStatusException
     * @throws SMException
     * @throws \App\Exceptions\BaseException
     * @throws ReportNotGeneratedException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function handle(GenerateMissingAdsReportAction $generateReportAction): void
    {
        try {
            $this->report->applyInternalStatus(ReportStatus::SUBMITTED);
            $generateReportAction->handle($this->report);
        } catch (\Throwable $exception) {
            $this->fail(ReportNotGeneratedException::createFrom($exception));
        }
    }

    /**
     * @return void
     * @throws CanNotApplyStatusException
     * @throws SMException
     */
    public function failed(): void
    {
        if ($this->report) {
            $this->report->applyInternalStatus(ReportStatus::FAILED);
        }
    }
}
