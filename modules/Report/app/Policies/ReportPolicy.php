<?php

namespace Modules\Report\Policies;

use App\Policies\Policy;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use Modules\User\Models\User;

class ReportPolicy extends Policy
{
    /**
     * Model class.
     *
     * @var string
     */
    protected $model = Report::class;

    /**
     * Permissions.
     */
    public const PERMISSION_LIST_REPORT = 'list_report';
    public const PERMISSION_CREATE_PENDING_CAMPAIGNS_REPORT = 'create_pending_campaigns_report';
    public const PERMISSION_CREATE_ADVERTISERS_REPORT = 'create_advertisers_report';
    public const PERMISSION_CREATE_DOWNLOAD_REPORT = 'create_download_report';
    public const PERMISSION_CREATE_SCHEDULED_REPORT = 'create_scheduled_report';
    public const PERMISSION_CREATE_MISSING_ADS_REPORT = 'create_missing_ads_report';

    public const PERMISSION_SHOW_PENDING_CAMPAIGNS_REPORT = 'show_pending_campaigns_report';
    public const PERMISSION_SHOW_ADVERTISERS_REPORT = 'show_advertisers_report';
    public const PERMISSION_SHOW_DOWNLOAD_REPORT = 'show_download_report';
    public const PERMISSION_SHOW_SCHEDULED_REPORT = 'show_scheduled_report';
    public const PERMISSION_SHOW_MISSING_ADS_REPORT = 'show_missing_ads_report';

    public const PERMISSION_UPDATE_ADVERTISERS_REPORT = 'update_advertisers_report';
    public const PERMISSION_UPDATE_PENDING_CAMPAIGNS_REPORT = 'update_pending_campaigns_report';
    public const PERMISSION_UPDATE_DOWNLOAD_REPORT = 'update_download_report';
    public const PERMISSION_UPDATE_SCHEDULED_REPORT = 'update_scheduled_report';
    public const PERMISSION_UPDATE_MISSING_ADS_REPORT = 'update_missing_ads_report';

    public const PERMISSION_PAUSE_REPORT = 'pause_report';
    public const PERMISSION_RESUME_REPORT = 'resume_report';
    public const PERMISSION_DELETE_REPORT = 'delete_report';

    /**
     * Returns true if user can view list of all reports.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_REPORT);
    }

    /**
     * Returns true if user can create Scheduled reports.
     *
     * @param User $user
     *
     * @return bool
     */
    public function createScheduled(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CREATE_SCHEDULED_REPORT);
    }

    /**
     * Returns true if user can create Download reports.
     *
     * @param User $user
     *
     * @return bool
     */
    public function createDownload(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CREATE_DOWNLOAD_REPORT);
    }

    /**
     * Returns true if user can create Advertisers reports.
     *
     * @param User $user
     *
     * @return bool
     */
    public function createAdvertisers(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CREATE_ADVERTISERS_REPORT);
    }

    /**
     * Returns true if user can create Pending Campaigns reports.
     *
     * @param User $user
     *
     * @return bool
     */
    public function createPendingCampaigns(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CREATE_PENDING_CAMPAIGNS_REPORT);
    }

    /**
     * Returns true if user can create Missing Ads reports.
     *
     * @param User $user
     *
     * @return bool
     */
    public function createMissingAds(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CREATE_MISSING_ADS_REPORT);
    }

    /**
     * True if user can download the report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function download(User $user, Report $report): bool
    {
        // Only owner is allowed to download the report
        if (!$this->isOwner($user, $report)) {
            return false;
        }

        // User should always be able to download report of downloadable types
        if ($report->isDownloadable()) {
            return true;
        }

        // User should be able to download scheduled reports only if they are already generated
        if ($report->isSchedulable() && $report->path) {
            return true;
        }

        return false;
    }

    /**
     * Returns true if user can view single Schedule report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function showScheduled(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_SHOW_SCHEDULED_REPORT)) {
            return false;
        }

        if (!$report->isScheduled()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can view Download report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function showDownload(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_SHOW_DOWNLOAD_REPORT)) {
            return false;
        }

        if (!$report->isDownload()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can view Advertisers report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function showAdvertisers(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_SHOW_ADVERTISERS_REPORT)) {
            return false;
        }

        if (!$report->isAdvertisers()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can view Pending Campaigns report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function showPendingCampaigns(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_SHOW_PENDING_CAMPAIGNS_REPORT)) {
            return false;
        }

        if (!$report->isPendingCampaigns()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can show Missing Ads reports.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function showMissingAds(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_SHOW_MISSING_ADS_REPORT)) {
            return false;
        }

        if (!$report->isMissingAds()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update single Schedule report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function updateScheduled(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_SCHEDULED_REPORT)) {
            return false;
        }

        if (!$report->isScheduled()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update Download report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function updateDownload(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_DOWNLOAD_REPORT)) {
            return false;
        }

        if (!$report->isDownload()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update Advertisers report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function updateAdvertisers(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_ADVERTISERS_REPORT)) {
            return false;
        }

        if (!$report->isAdvertisers()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update Pending Campaigns report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function updatePendingCampaigns(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_PENDING_CAMPAIGNS_REPORT)) {
            return false;
        }

        if (!$report->isPendingCampaigns()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update Missing Ads reports.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function updateMissingAds(User $user, Report $report): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_MISSING_ADS_REPORT)) {
            return false;
        }

        if (!$report->isMissingAds()) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can pause the report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     * @throws \SM\SMException
     */
    public function pause(User $user, Report $report): bool
    {
        if ($report->isDownloadable()) {
            return false;
        }

        if (!$user->hasPermissionTo(self::PERMISSION_PAUSE_REPORT)) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return $report->canApply(ReportStatus::PAUSED);
    }

    /**
     * Returns true if user can resume the report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     * @throws \SM\SMException
     */
    public function resume(User $user, Report $report): bool
    {
        if ($report->isDownloadable()) {
            return false;
        }

        if (!$report->isPaused()) {
            return false;
        }

        if (!$user->hasPermissionTo(self::PERMISSION_RESUME_REPORT)) {
            return false;
        }

        if (!$this->isOwner($user, $report)) {
            return false;
        }

        return $report->canApply(ReportStatus::DRAFT);
    }

    /**
     * Returns true if user can delete the report.
     *
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    public function delete(User $user, Report $report): bool
    {
        // User has no permission to delete the report
        if (!$user->hasPermissionTo(self::PERMISSION_DELETE_REPORT)) {
            return false;
        }

        return $this->isOwner($user, $report);
    }

    /**
     * @param User   $user
     * @param Report $report
     *
     * @return bool
     */
    private function isOwner(User $user, Report $report): bool
    {
        return $report->user_id === $user->id;
    }
}
