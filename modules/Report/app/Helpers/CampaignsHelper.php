<?php

namespace Modules\Report\Helpers;

use App\Exceptions\InvalidArgumentException;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Modules\Campaign\DataTable\Repositories\Criteria\ActiveCampaignsCriteria;
use Modules\Campaign\Repositories\Criteria\RecentlyCompletedCampaignsCriteria;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportDeliveryFrequency;
use Modules\Report\Repositories\Criteria\AddUsersCriteria;
use Modules\Report\Repositories\Criteria\ReportIdCriteria;
use Modules\Report\Repositories\ReportRepository;
use Modules\User\DataTable\Repositories\Criteria\AddCampaignsCriteria;

class CampaignsHelper
{
    /**
     * @var ReportRepository
     */
    private $reportRepository;

    /**
     * @param ReportRepository $reportRepository
     */
    public function __construct(ReportRepository $reportRepository)
    {
        $this->reportRepository = $reportRepository;
    }

    /**
     * @param Report $report
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function hasLiveCampaigns(Report $report): bool
    {
        return $this->reportRepository
            ->reset()
            ->pushCriteria(new AddUsersCriteria())
            ->pushCriteria(new AddCampaignsCriteria())
            ->pushCriteria(new ActiveCampaignsCriteria())
            ->pushCriteria(new ReportIdCriteria($report->id))
            ->exists();
    }

    /**
     * @param Report $report
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws InvalidArgumentException
     */
    public function hasRecentlyCompletedCampaigns(Report $report): bool
    {
        $query = $this->reportRepository
            ->reset()
            ->pushCriteria(new AddUsersCriteria())
            ->pushCriteria(new AddCampaignsCriteria())
            ->pushCriteria(new ReportIdCriteria($report->id));

        $recentlyCompletedCriteria = $this->getRecentlyCompletedCriteriaByReport($report);
        $query->pushCriteria($recentlyCompletedCriteria);

        return $query->exists();
    }

    /**
     * @param Report $report
     *
     * @return RecentlyCompletedCampaignsCriteria
     * @throws InvalidArgumentException
     */
    private function getRecentlyCompletedCriteriaByReport(Report $report): RecentlyCompletedCampaignsCriteria
    {
        $map = [
            ReportDeliveryFrequency::ID_DAILY   => function (): RecentlyCompletedCampaignsCriteria {
                $retryTime = config('report.stop.retry.times.' . ReportDeliveryFrequency::ID_DAILY);
                return new RecentlyCompletedCampaignsCriteria($retryTime, 'DAY');
            },
            ReportDeliveryFrequency::ID_WEEKLY  => function (): RecentlyCompletedCampaignsCriteria {
                $retryTime = config('report.stop.retry.times.' . ReportDeliveryFrequency::ID_WEEKLY);
                return new RecentlyCompletedCampaignsCriteria($retryTime, 'WEEK');
            },
            ReportDeliveryFrequency::ID_MONTHLY => function (): RecentlyCompletedCampaignsCriteria {
                $retryTime = config('report.stop.retry.times.' . ReportDeliveryFrequency::ID_MONTHLY);
                return new RecentlyCompletedCampaignsCriteria($retryTime, 'MONTH');
            },
        ];

        $criteria = Arr::get($map, $report->delivery_frequency_id);

        if (is_null($criteria) || is_null($report->delivery_frequency_id)) {
            throw new InvalidArgumentException(sprintf(
                'Report delivery_frequency_id not found: "%s".',
                $report->delivery_frequency_id
            ));
        }

        return $criteria();
    }

    /**
     * If report has only completed campaigns we need to modify dates that would be send to HAAPI
     * to get campaigns that has its statistics updated
     *
     * @param Report $report
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public function getDatesForRecentlyCompletedCampaigns(Report $report): array
    {
        $map = [
            ReportDeliveryFrequency::ID_DAILY   => function (Report $report): array {
                $retryTime = config('report.stop.retry.times.' . ReportDeliveryFrequency::ID_DAILY);
                $startDate = Carbon::yesterday()->startOfDay()->subDays($retryTime);
                $endDate = $report->getDateTo();

                return [$startDate, $endDate];
            },
            ReportDeliveryFrequency::ID_WEEKLY  => function (Report $report): array {
                $retryTime = config('report.stop.retry.times.' . ReportDeliveryFrequency::ID_WEEKLY);
                $startDate = Carbon::yesterday()->startOfDay()->subWeeks($retryTime);
                $endDate = $report->getDateTo();

                return [$startDate, $endDate];
            },
            ReportDeliveryFrequency::ID_MONTHLY => function (Report $report): array {
                $retryTime = config('report.stop.retry.times.' . ReportDeliveryFrequency::ID_MONTHLY);
                $startDate = Carbon::now()->startOfMonth()->startOfDay()->subMonths($retryTime);
                $endDate = $startDate->clone()->endOfMonth()->endOfDay();

                return [$startDate, $endDate];
            },
        ];

        $getStartDateCallable = Arr::get($map, $report->delivery_frequency_id);

        if (is_null($getStartDateCallable)) {
            throw new InvalidArgumentException(sprintf(
                'Report delivery_frequency_id not found: "%s".',
                $report->delivery_frequency_id
            ));
        }

        return $getStartDateCallable($report);
    }
}
