<?php

namespace Modules\Report\Helpers;

use App\Helpers\DateFormatHelper;
use App\Helpers\FilenameHelper;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Excel;
use Modules\Report\Models\Report;

trait GenerateReportFilename
{
    /**
     * Get filename with the correct file format.
     *
     * @param Report $report
     *
     * @return string
     * @throws \App\Exceptions\BaseException
     */
    public function createFilename(Report $report): string
    {
        $name = $this->createName($report);

        return $this->addExtension($name);
    }

    /**
     * @param Report $report
     *
     * @return string
     * @throws \App\Exceptions\BaseException
     */
    private function createName(Report $report): string
    {
        if ($report->isDownload()) {
            return $this->createFromToDateReportFilename($report->name, $report->getDateFrom(), $report->getDateTo());
        }

        return $this->createReportFilename($report->name);
    }

    /**
     * Get url encoded filename with the correct file format.
     *
     * @param Report $report
     *
     * @return string
     * @throws \App\Exceptions\BaseException
     */
    public function createEncodedFilename(Report $report): string
    {
        $name = $this->createName($report);
        $name = $this->encodeName($name);

        return $this->addExtension($name);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function addExtension(string $name): string
    {
        return $name . '.' . Str::lower(Excel::XLSX);
    }

    /**
     * @param string $reportName
     * @param Carbon $from
     * @param Carbon $to
     *
     * @return string
     */
    private function createFromToDateReportFilename(string $reportName, Carbon $from, Carbon $to): string
    {
        $name = strtr('{prefix} - {report_name} - {date_start} to {date_end}', [
            '{prefix}'      => __('labels.hulu_ad_manager'),
            '{report_name}' => $reportName,
            '{date_start}'  => $from->format(config('report.format.date')),
            '{date_end}'    => $to->format(config('report.format.date')),
        ]);

        if (!FilenameHelper::isNameTooLong($this->addExtension($name))) {
            return $name;
        }

        $limitedReportName = $this->getLimitedName($name, $reportName);

        return $this->createFromToDateReportFilename($limitedReportName, $from, $to);
    }

    /**
     * @param string $reportName
     *
     * @return string
     */
    private function createReportFilename(string $reportName): string
    {
        $name = strtr('{prefix} - {report_name}', [
            '{prefix}'      => __('labels.hulu_ad_manager'),
            '{report_name}' => $reportName,
        ]);

        if (!FilenameHelper::isNameTooLong($this->addExtension($name))) {
            return $name;
        }

        $limitedReportName = $this->getLimitedName($name, $reportName);

        return $this->createReportFilename($limitedReportName);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function encodeName(string $name): string
    {
        return urlencode($name);
    }

    /**
     * @param string $campaignName
     *
     * @return string
     */
    public function createReportMetricsFilename(string $campaignName): string
    {
        $campaignName = preg_replace('/[\/\\\\]/', '_', $campaignName);
        $name = strtr('{prefix} - {campaign_name} - {generation_date}', [
            '{prefix}'          => __('labels.hulu_ad_manager'),
            '{campaign_name}'   => $campaignName,
            '{generation_date}' => DateFormatHelper::now()->format(config('report.format.date')),
        ]);

        if (!FilenameHelper::isNameTooLong($this->addExtension($name))) {
            return $this->addExtension($name);
        }

        $limitedCampaignName = $this->getLimitedName($name, $campaignName);

        return $this->createReportMetricsFilename($limitedCampaignName);
    }

    /**
     * @param string $downloadFileName
     * @param string $name
     *
     * @return string
     */
    private function getLimitedName(string $downloadFileName, string $name): string
    {
        $excess = FilenameHelper::getNameAbsoluteExcess($this->addExtension($downloadFileName));
        return FilenameHelper::limit($name, $excess);
    }
}
