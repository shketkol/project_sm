<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Report\Http\Resources\ScheduleReportResource;
use Modules\Report\Models\Report;

class ShowScheduleReportController extends Controller
{
    /**
     * @param Report $report
     * @return ScheduleReportResource
     */
    public function __invoke(Report $report): ScheduleReportResource
    {
        return new ScheduleReportResource($report);
    }
}
