<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Report\Actions\StoreScheduleReportAction;
use Modules\Report\Http\Requests\StoreScheduleReportRequest;

class StoreScheduleReportController extends Controller
{
    /**
     * @param StoreScheduleReportRequest $request
     * @param StoreScheduleReportAction  $action
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(StoreScheduleReportRequest $request, StoreScheduleReportAction $action): Response
    {
        $report = $action->handle($request->toArray());
        return response([
            'message' => __('report::messages.report_was_successfully_created'),
            'id'      => $report->id,
        ]);
    }
}
