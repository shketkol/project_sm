<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Report\Http\Resources\DeliveryFrequencyResource;
use Modules\Report\Models\ReportDays;
use Modules\Report\Models\ReportDeliveryFrequency;

class IndexDeliveryFrequencyController extends Controller
{
    /**
     * Index delivery frequency.
     *
     * @param ReportDeliveryFrequency $model
     * @return AnonymousResourceCollection
     */
    public function __invoke(ReportDeliveryFrequency $model): AnonymousResourceCollection
    {
        return DeliveryFrequencyResource::collection($model->all())
            ->additional(['reportDays' => ReportDays::get()]);
    }
}
