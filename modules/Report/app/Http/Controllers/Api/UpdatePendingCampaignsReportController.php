<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Report\Actions\UpdatePendingCampaignsReportAction;
use Modules\Report\Http\Requests\StorePendingCampaignsReportRequest;
use Modules\Report\Models\Report;

class UpdatePendingCampaignsReportController extends Controller
{
    /**
     * @param UpdatePendingCampaignsReportAction $action
     * @param StorePendingCampaignsReportRequest $request
     * @param Report $report
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(
        UpdatePendingCampaignsReportAction $action,
        StorePendingCampaignsReportRequest $request,
        Report $report
    ): Response {
        $action->handle($report, $request->toArray());
        return response(['message' => __('report::messages.report_was_successfully_updated')]);
    }
}
