<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Report\Http\Resources\DownloadReportResource;
use Modules\Report\Models\Report;

class ShowDownloadReportController extends Controller
{
    /**
     * @param Report $report
     * @return DownloadReportResource
     */
    public function __invoke(Report $report): DownloadReportResource
    {
        return new DownloadReportResource($report);
    }
}
