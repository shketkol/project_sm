<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Report\Actions\UpdateAdvertisersReportAction;
use Modules\Report\Http\Requests\StoreAdvertisersReportRequest;
use Modules\Report\Models\Report;

class UpdateAdvertisersReportController extends Controller
{
    /**
     * @param UpdateAdvertisersReportAction $action
     * @param StoreAdvertisersReportRequest $request
     * @param Report $report
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(
        UpdateAdvertisersReportAction $action,
        StoreAdvertisersReportRequest $request,
        Report $report
    ): Response {
        $action->handle($report, $request->toArray());
        return response(['message' => __('report::messages.report_was_successfully_updated')]);
    }
}
