<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Report\Actions\StoreAdvertisersReportAction;
use Modules\Report\Http\Requests\StoreAdvertisersReportRequest;

class StoreAdvertisersReportController extends Controller
{
    /**
     * @param StoreAdvertisersReportAction $action
     * @param StoreAdvertisersReportRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(StoreAdvertisersReportAction $action, StoreAdvertisersReportRequest $request): Response
    {
        $action->handle($request->toArray());
        return response(['message' => __('report::messages.report_was_successfully_created')]);
    }
}
