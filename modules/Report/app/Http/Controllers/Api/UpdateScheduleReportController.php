<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Report\Actions\UpdateScheduleReportAction;
use Modules\Report\Http\Requests\StoreScheduleReportRequest;
use Modules\Report\Models\Report;

class UpdateScheduleReportController extends Controller
{
    /**
     * @param StoreScheduleReportRequest $request
     * @param UpdateScheduleReportAction $action
     * @param Report $report
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(
        StoreScheduleReportRequest $request,
        UpdateScheduleReportAction $action,
        Report $report
    ): Response {
        $action->handle($report, $request->toArray());
        return response(['message' => __('report::messages.report_was_successfully_updated')]);
    }
}
