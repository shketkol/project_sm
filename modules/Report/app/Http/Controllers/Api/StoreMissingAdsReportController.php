<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Report\Actions\StoreMissingAdsReportAction;
use Modules\Report\Actions\StoreMissingAdsScheduledReportAction;
use Modules\Report\Http\Requests\StoreMissingAdsReportRequest;

class StoreMissingAdsReportController extends Controller
{
    public function __invoke(
        StoreMissingAdsReportRequest $request,
        StoreMissingAdsScheduledReportAction $storeScheduledAction,
        StoreMissingAdsReportAction $storeDownloadableAction
    ): Response {
        if ($request->isScheduledReport()) {
            $storeScheduledAction->handle($request->getDataForScheduledReport());
        } else {
            $storeDownloadableAction->handle($request->getDataForDownloadReport());
        }
        return response(['message' => __('report::messages.report_was_successfully_created')]);
    }
}
