<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Modules\Report\Actions\StorePendingCampaignsReportAction;
use Modules\Report\Http\Requests\StorePendingCampaignsReportRequest;

class StorePendingCampaignsReportController extends Controller
{
    /**
     * @param StorePendingCampaignsReportAction $action
     * @param StorePendingCampaignsReportRequest $request
     *
     * @return ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(
        StorePendingCampaignsReportAction $action,
        StorePendingCampaignsReportRequest $request
    ): Response {
        $action->handle($request->toArray());
        return response(['message' => __('report::messages.report_was_successfully_created')]);
    }
}
