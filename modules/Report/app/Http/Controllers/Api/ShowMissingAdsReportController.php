<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Report\Http\Resources\MissingAdsReportResource;
use Modules\Report\Models\Report;

class ShowMissingAdsReportController extends Controller
{
    /**
     * @param Report $report
     * @return MissingAdsReportResource
     */
    public function __invoke(Report $report): MissingAdsReportResource
    {
        return new MissingAdsReportResource($report);
    }
}
