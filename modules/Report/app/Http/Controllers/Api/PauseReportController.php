<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Report\Http\Resources\ReportResource;
use Modules\Report\Actions\PauseReportAction;
use Modules\Report\Models\Report;

class PauseReportController extends Controller
{
    /**
     * Pause the report.
     *
     * @param Report $report
     * @param PauseReportAction $action
     * @return ReportResource
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function __invoke(Report $report, PauseReportAction $action): ReportResource
    {
        $action->handle($report);

        return new ReportResource($report);
    }
}
