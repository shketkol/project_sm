<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Report\Http\Resources\AdvertisersReportResource;
use Modules\Report\Models\Report;

class ShowAdvertisersReportController extends Controller
{
    /**
     * @param Report $report
     * @return AdvertisersReportResource
     */
    public function __invoke(Report $report): AdvertisersReportResource
    {
        return new AdvertisersReportResource($report);
    }
}
