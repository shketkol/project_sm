<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Report\Actions\UpdateDownloadReportAction;
use Modules\Report\Http\Requests\StoreDownloadReportRequest;
use Modules\Report\Models\Report;

class UpdateDownloadReportController extends Controller
{
    /**
     * @param UpdateDownloadReportAction $action
     * @param StoreDownloadReportRequest $request
     * @param Report $report
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(
        UpdateDownloadReportAction $action,
        StoreDownloadReportRequest $request,
        Report $report
    ): Response {
        $action->handle($report, $request->toArray());
        return response(['message' => __('report::messages.report_was_successfully_updated')]);
    }
}
