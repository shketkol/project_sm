<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Report\Actions\UpdateMissingAdsDownloadReportAction;
use Modules\Report\Actions\UpdateMissingAdsScheduledReportAction;
use Modules\Report\Http\Requests\StoreMissingAdsReportRequest;
use Modules\Report\Models\Report;

class UpdateMissingAdsReportController extends Controller
{
    /**
     * @param UpdateMissingAdsScheduledReportAction $updateScheduledAction
     * @param UpdateMissingAdsDownloadReportAction  $updateDownloadableAction
     * @param StoreMissingAdsReportRequest          $request
     * @param Report                                $report
     *
     * @return Response
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function __invoke(
        UpdateMissingAdsScheduledReportAction $updateScheduledAction,
        UpdateMissingAdsDownloadReportAction $updateDownloadableAction,
        StoreMissingAdsReportRequest $request,
        Report $report
    ): Response {
        if ($request->isScheduledReport()) {
            $updateScheduledAction->handle($report, $request->getDataForScheduledReport());
        } else {
            $updateDownloadableAction->handle($report, $request->getDataForDownloadReport());
        }
        return response(['message' => __('report::messages.report_was_successfully_updated')]);
    }
}
