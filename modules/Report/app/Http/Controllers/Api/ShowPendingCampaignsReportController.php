<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Report\Http\Resources\PendingCampaignsReportResource;
use Modules\Report\Models\Report;

class ShowPendingCampaignsReportController extends Controller
{
    /**
     * @param Report $report
     * @return PendingCampaignsReportResource
     */
    public function __invoke(Report $report): PendingCampaignsReportResource
    {
        return new PendingCampaignsReportResource($report);
    }
}
