<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Report\Actions\StoreDownloadReportAction;
use Modules\Report\Http\Requests\StoreDownloadReportRequest;

class StoreDownloadReportController extends Controller
{
    /**
     * @param StoreDownloadReportAction  $action
     * @param StoreDownloadReportRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function __invoke(StoreDownloadReportAction $action, StoreDownloadReportRequest $request): Response
    {
        $action->handle($request->toArray());
        return response(['message' => __('report::messages.downloaded_report_created')]);
    }
}
