<?php

namespace Modules\Report\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Report\Http\Resources\ReportResource;
use Modules\Report\Actions\ResumeReportAction;
use Modules\Report\Models\Report;

class ResumeReportController extends Controller
{
    /**
     * Resume the report.
     *
     * @param Report $report
     * @param ResumeReportAction $action
     * @return ReportResource
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function __invoke(Report $report, ResumeReportAction $action): ReportResource
    {
        $action->handle($report);

        return new ReportResource($report);
    }
}
