<?php

namespace Modules\Report\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Report\Models\Report;

/**
 * @mixin Report
 */
class PendingCampaignsReportResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'   => $this->resource->id,
            'name' => $this->resource->name,
            'dateRange' => [
                'start' => $this->resource->date_start,
                'end'   => $this->resource->date_end,
            ],
        ];
    }
}
