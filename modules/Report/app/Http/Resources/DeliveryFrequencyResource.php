<?php

namespace Modules\Report\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Report\Models\ReportDeliveryFrequency;

/**
 * @mixin ReportDeliveryFrequency
 */
class DeliveryFrequencyResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->translated_name,
        ];
    }
}
