<?php

namespace Modules\Report\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportType;

/**
 * @mixin Report
 */
class MissingAdsReportResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'isScheduled' => $this->resource->type_id === ReportType::ID_MISSING_ADS_SCHEDULED,
            'id'          => $this->resource->id,
            'name'        => $this->resource->name,
            'frequency'   => $this->resource->delivery_frequency_id,
            'day'         => $this->resource->execution_day,
            'emails'      => $this->resource->emails()->get('email')->pluck('email'),
            'dateRange'   => [
                'start' => $this->resource->date_start,
                'end'   => $this->resource->date_end,
            ],
        ];
    }
}
