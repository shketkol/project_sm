<?php

namespace Modules\Report\Http\Resources;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Modules\Advertiser\Http\Resources\AdvertiserSelectResource;
use Modules\Report\Models\Report;

/**
 * @mixin Report
 */
class DownloadReportResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $resource = [
            'id'        => $this->resource->id,
            'name'      => $this->resource->name,
            'dateRange' => [
                'start' => $this->resource->date_start,
                'end'   => $this->resource->date_end,
            ],
        ];

        /** @var Guard $auth */
        $auth = app(Guard::class);

        if (!$auth->guest() && $auth->user()->isAdmin()) {
            Arr::set(
                $resource,
                'advertiser',
                $this->resource->advertiser ? new AdvertiserSelectResource($this->resource->advertiser) : null
            );
        }

        return $resource;
    }
}
