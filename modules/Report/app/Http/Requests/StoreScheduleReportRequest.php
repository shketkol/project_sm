<?php

namespace Modules\Report\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Modules\Report\Models\ReportType;
use Modules\User\Models\User;

class StoreScheduleReportRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     * @param Guard           $auth
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules, Guard $auth): array
    {
        $executionDayRules = $validationRules->only(
            'report.execution_day',
            ['integer', 'nullable', 'included', 'max']
        );

        array_push(
            $executionDayRules,
            Rule::requiredIf($this->frequency === config('report.show_days_frequency_id'))
        );

        $rules = [
            'name'      => $validationRules->only(
                'report.name',
                ['required', 'string', 'max', 'regex']
            ),
            'frequency' => $validationRules->only(
                'report.delivery_frequency',
                ['required', 'integer', 'exists', 'max']
            ),
            'day'       => $executionDayRules,
            'emails'    => $validationRules->only(
                'report.emails',
                ['required', 'array']
            ),
            'emails.*'  => $validationRules->only(
                'report.email',
                ['required', 'email', 'max']
            ),
        ];

        if (!$auth->guest() && $auth->user()->isAdmin()) {
            Arr::set($rules, 'advertiser.value', $validationRules->only(
                'report.account',
                ['nullable', 'integer', 'max']
            ));
        }

        return $rules;
    }

    /**
     * Prepare the data for validation.
     * Limit scheduled reports recipients for advertiser to only their email.
     *
     * @param Guard $auth
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        /** @var Guard $auth */
        $auth = app(Guard::class);

        /** @var User|null $user */
        $user = $auth->user();

        if (!$auth->guest() && $user->isAdvertiser()) {
            $all = $this->all();
            Arr::set($all, 'emails', [$user->email]);
            $this->replace($all);
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name'                  => $this->name,
            'delivery_frequency_id' => $this->frequency,
            'execution_day'         => $this->day,
            'user_id'               => $this->user()->getKey(),
            'type_id'               => ReportType::ID_SCHEDULED,
            'emails'                => $this->emails,
            'advertiser_id' => $this->user()->isAdmin()
                ? Arr::get($this->advertiser, 'value')
                : null,
        ];
    }
}
