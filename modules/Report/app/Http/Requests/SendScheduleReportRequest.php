<?php

namespace Modules\Report\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

/**
 * @property array $request
 */
class SendScheduleReportRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'request.payload.report_id' => $validationRules->only(
                'report.id',
                ['required', 'integer', 'max']
            ),
        ];
    }
}
