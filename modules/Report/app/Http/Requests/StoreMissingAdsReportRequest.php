<?php

namespace Modules\Report\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Carbon\Carbon;
use Modules\Report\Models\ReportType;
use Illuminate\Validation\Rule;

class StoreMissingAdsReportRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        // Common rules (for Downloadable and Scheduled reports).
        $rules = [
            'name' => $validationRules->only(
                'report.name',
                ['required', 'string', 'max', 'regex']
            ),
        ];

        // Rules for scheduled reports.
        if ($this->isScheduledReport()) {
            $executionDayRules = $validationRules->only(
                'report.execution_day',
                ['integer', 'nullable', 'included', 'max']
            );
            array_push(
                $executionDayRules,
                Rule::requiredIf($this->frequency === config('report.show_days_frequency_id'))
            );

            $rules = array_merge($rules, [
                'day'       => $executionDayRules,
                'frequency' => $validationRules->only(
                    'report.missing_ads_delivery_frequency',
                    ['required', 'integer', 'exists', 'included']
                ),
                'emails'    => $validationRules->only(
                    'report.emails',
                    ['required', 'array']
                ),
                'emails.*'  => $validationRules->only(
                    'report.email',
                    ['required', 'email', 'max']
                ),
            ]);
        } else {
            $rules = array_merge($rules, [
                'dateRange.start' => $validationRules->only(
                    'report.date',
                    ['required', 'date']
                ),
                'dateRange.end'   => $validationRules->only(
                    'report.date',
                    ['required', 'date']
                ),
            ]);
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function getDataForScheduledReport(): array
    {
        return [
            'name'                  => $this->name,
            'delivery_frequency_id' => $this->frequency,
            'execution_day'         => $this->day,
            'type_id'               => ReportType::ID_MISSING_ADS_SCHEDULED,
            'user_id'               => $this->user()->getKey(),
            'emails'                => $this->emails,
            'date_start'            => null,
            'date_end'              => null,
        ];
    }

    /**
     * @return array
     */
    public function getDataForDownloadReport(): array
    {
        return [
            'name'                  => $this->name,
            'type_id'               => ReportType::ID_MISSING_ADS_DOWNLOAD,
            'user_id'               => $this->user()->getKey(),
            'delivery_frequency_id' => null,
            'execution_day'         => null,
            'emails'                => null,
            'date_start'            => Carbon::createFromDate($this->dateRange['start']),
            'date_end'              => Carbon::createFromDate($this->dateRange['end']),
        ];
    }

    /**
     * @return bool
     */
    public function isScheduledReport(): bool
    {
        return (bool) $this->input('isScheduled', false);
    }
}
