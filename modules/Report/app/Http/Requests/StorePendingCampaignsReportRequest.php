<?php

namespace Modules\Report\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Carbon\Carbon;
use Modules\Report\Models\ReportType;

class StorePendingCampaignsReportRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        $dateRule = $validationRules->only(
            'report.date',
            ['required', 'date']
        );

        return [
            'name' => $validationRules->only(
                'report.name',
                ['required', 'string', 'max', 'regex']
            ),
            'dateRange.start' => $dateRule,
            'dateRange.end'   => $dateRule,
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name'    => $this->name,
            'type_id' => ReportType::ID_PENDING_CAMPAIGNS,
            'user_id' => $this->user()->getKey(),
            'date_start'    => Carbon::createFromDate($this->dateRange['start']),
            'date_end'      => Carbon::createFromDate($this->dateRange['end']),
        ];
    }
}
