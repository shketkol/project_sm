<?php

namespace Modules\Report\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Modules\Report\Models\ReportType;

class StoreAdvertisersReportRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'name'             => $validationRules->only(
                'report.name',
                ['required', 'string', 'max', 'regex']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name'          => $this->name,
            'type_id'       => ReportType::ID_ADVERTISERS,
            'user_id'       => $this->user()->getKey(),
        ];
    }
}
