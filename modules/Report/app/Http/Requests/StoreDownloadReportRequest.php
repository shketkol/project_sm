<?php

namespace Modules\Report\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Arr;
use Modules\Report\Models\ReportType;

class StoreDownloadReportRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     * @param Guard           $auth
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules, Guard $auth): array
    {
        $dateRule = $validationRules->only(
            'report.date',
            ['required', 'date']
        );

        $rules = [
            'name'            => $validationRules->only(
                'report.name',
                ['required', 'string', 'max', 'regex']
            ),
            'dateRange.start' => $dateRule,
            'dateRange.end'   => $dateRule,
        ];

        if (!$auth->guest() && $auth->user()->isAdmin()) {
            Arr::set($rules, 'advertiser.value', $validationRules->only(
                'report.account',
                ['nullable', 'integer', 'max']
            ));
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name'          => $this->name,
            'date_start'    => Carbon::createFromDate($this->dateRange['start']),
            'date_end'      => Carbon::createFromDate($this->dateRange['end']),
            'type_id'       => ReportType::ID_DOWNLOAD,
            'user_id'       => $this->user()->getKey(),
            'advertiser_id' => $this->user()->isAdmin()
                ? Arr::get($this->advertiser, 'value')
                : null,
        ];
    }
}
