<?php

namespace Modules\Report;

use App\Providers\ModuleServiceProvider;
use Modules\Report\Commands\DeleteOldReportsCommand;
use Modules\Report\DataTable\Repositories\ReportsDataTableRepository;
use Modules\Report\DataTable\Repositories\Contracts\ReportsDataTableRepository as ReportsDataTableRepositoryContract;
use Modules\Report\Repositories\Contracts\ReportRepository as ReportRepositoryContract;
use Modules\Report\Commands\ReportMakeCommand;
use Modules\Report\Commands\SendScheduledReportCommand;
use Modules\Report\Policies\ReportPolicy;
use Modules\Report\Repositories\ReportRepository;
use Modules\Report\States\CompletedState;
use Modules\Report\States\SubmittedState;

class ReportServiceProvider extends ModuleServiceProvider
{
    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'report' => ReportPolicy::class,
    ];

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        ReportMakeCommand::class,
        SendScheduledReportCommand::class,
        DeleteOldReportsCommand::class,
    ];

    /**
     * @var array
     */
    public $bindings = [
        'state-machine.report.states.completed'   => CompletedState::class,
        'state-machine.report.states.submitted'   => SubmittedState::class,
        ReportRepositoryContract::class           => ReportRepository::class,
        ReportsDataTableRepositoryContract::class => ReportsDataTableRepository::class,
    ];

    /**
     * Register any user services.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();

        $this->loadRoutes();
        $this->loadConfigs(['report', 'state-machine']);
        $this->commands($this->commands);
    }

    /**
     * Get module prefix
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'report';
    }
}
