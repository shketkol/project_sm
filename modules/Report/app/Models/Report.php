<?php

namespace Modules\Report\Models;

use App\Helpers\DateFormatHelper;
use App\Traits\State;
use Carbon\Carbon;
use App\Traits\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Report\Helpers\AWSReport;
use Modules\Report\Models\Traits\BelongsToDeliveryFrequency;
use Modules\Report\Models\Traits\BelongsToStatus;
use Modules\Report\Models\Traits\BelongsToType;
use Modules\Report\Models\Traits\HasEmails;
use Modules\User\Models\Traits\BelongsToAdvertiser;
use Modules\User\Models\Traits\BelongsToUser;
use Modules\User\Models\User;

/**
 * @property int         $id
 * @property string      $name
 * @property string      $path
 * @property Carbon      $date_start
 * @property Carbon      $date_end
 * @property Carbon      $created_at
 * @property Carbon      $updated_at
 * @property int         $type_id
 * @property int         $delivery_frequency_id
 * @property int         $user_id
 * @property int         $execution_day
 * @property int         $advertiser_id
 * @property Carbon      $generated_at
 * @property string|null $url
 * @property int         $status_id
 * @property boolean     $update_required
 * @see Report::getUrlAttribute()
 * @method static \Modules\Report\Database\Factories\ReportFactory factory()
 */
class Report extends Model
{
    use BelongsToDeliveryFrequency,
        BelongsToStatus,
        BelongsToType,
        BelongsToUser,
        BelongsToAdvertiser,
        HasEmails,
        State,
        AWSReport,
        HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        'date_end',
        'date_start',
        'delivery_frequency_id',
        'execution_day',
        'generated_at',
        'name',
        'path',
        'status_id',
        'type_id',
        'user_id',
        'advertiser_id',
        'update_required',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date_start',
        'date_end',
        'generated_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'update_required' => 'boolean',
    ];

    /**
     * Campaign state's flow.
     *
     * @var string
     */
    protected $flow = 'report';

    /**
     * Get start date for the report.
     *
     * @return Carbon
     * @throws \App\Exceptions\BaseException
     */
    public function getDateFrom(): Carbon
    {
        if ($this->deliveryFrequency) {
            $date = $this->deliveryFrequency->getDateFrom();
        } else {
            $date = $this->date_start->startOfDay();
        }

        return DateFormatHelper::convertToTimezone($date);
    }

    /**
     * Get end date for the report.
     *
     * @return Carbon
     * @throws \App\Exceptions\BaseException
     */
    public function getDateTo(): Carbon
    {
        if ($this->deliveryFrequency) {
            $date = $this->deliveryFrequency->getDateTo();
        } else {
            $date = $this->date_end->endOfDay();
        }

        return DateFormatHelper::convertToTimezone($date);
    }

    /**
     * Get url for the report file.
     *
     * @return null|string
     */
    public function getUrlAttribute(): ?string
    {
        return $this->path ? $this->generateDownloadUrl($this) : null;
    }

    /**
     * @return User|null
     */
    public function getAdvertiserExternalId(): ?string
    {
        return $this->advertiser ? $this->advertiser->getExternalId() : null;
    }
}
