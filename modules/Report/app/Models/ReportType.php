<?php

namespace Modules\Report\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property string $name
 */
class ReportType extends Model
{
    /**
     * Default types.
     */
    public const SCHEDULED = 'scheduled';
    public const DOWNLOAD = 'download';
    public const ADVERTISERS = 'advertisers';
    public const PENDING_CAMPAIGNS = 'pending_campaigns';
    public const MISSING_ADS_SCHEDULED = 'missing_ads_scheduled';
    public const MISSING_ADS_DOWNLOAD = 'missing_ads_download';

    public const ID_SCHEDULED = 1;
    public const ID_DOWNLOAD = 2;
    public const ID_ADVERTISERS = 3;
    public const ID_PENDING_CAMPAIGNS = 4;
    public const ID_MISSING_ADS_SCHEDULED = 5;
    public const ID_MISSING_ADS_DOWNLOAD = 6;

    /**
     * Mapped types
     */
    public const TYPES = [
        self::ID_SCHEDULED             => self::SCHEDULED,
        self::ID_DOWNLOAD              => self::DOWNLOAD,
        self::ID_ADVERTISERS           => self::ADVERTISERS,
        self::ID_PENDING_CAMPAIGNS     => self::PENDING_CAMPAIGNS,
        self::ID_MISSING_ADS_SCHEDULED => self::MISSING_ADS_SCHEDULED,
        self::ID_MISSING_ADS_DOWNLOAD  => self::MISSING_ADS_DOWNLOAD,
    ];

    /**
     * Downloadable type ids.
     */
    public const DOWNLOADABLE_TYPE_IDS = [
        self::ID_DOWNLOAD,
        self::ID_ADVERTISERS,
        self::ID_PENDING_CAMPAIGNS,
        self::ID_MISSING_ADS_DOWNLOAD,
    ];

    /**
     * Schedulable type ids.
     */
    public const SCHEDULABLE_TYPE_IDS = [
        self::ID_SCHEDULED,
        self::ID_MISSING_ADS_SCHEDULED,
    ];

    /**
     * Emailable type ids.
     */
    public const EMAILABLE_TYPE_IDS = [
        self::ID_SCHEDULED,
        self::ID_MISSING_ADS_SCHEDULED,
    ];

    /**
     * Notifiable type ids.
     */
    public const NOTIFIABLE_TYPE_IDS = [
        self::ID_SCHEDULED,
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param string $type
     *
     * @return int
     */
    public static function getTypeId(string $type): int
    {
        $typeId = array_search($type, self::TYPES);
        if ($typeId === false) {
            throw new InvalidArgumentException(sprintf('Unsupported report type ("%s") was given.', $type));
        }

        return $typeId;
    }
}
