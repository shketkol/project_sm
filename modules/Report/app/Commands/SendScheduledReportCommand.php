<?php

namespace Modules\Report\Commands;

use App\Jobs\Job;
use App\Models\QueuePriority;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Report\Jobs\ProcessMissingAdsReport;
use Modules\Report\Jobs\ProcessReport;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportDeliveryFrequency;
use Modules\Report\Models\ReportStatus;
use Modules\Report\Models\ReportType;
use Modules\Report\Repositories\Contracts\ReportRepository;
use Symfony\Component\Console\Exception\InvalidArgumentException;

class SendScheduledReportCommand extends Command
{
    protected const REPORT_PROCESSORS = [
        ReportType::SCHEDULED             => ProcessReport::class,
        ReportType::MISSING_ADS_SCHEDULED => ProcessMissingAdsReport::class,
    ];

    /**
     * Map for available delivery frequency options.
     *
     * @var array
     */
    protected $optionsMap = [
        ReportDeliveryFrequency::DAILY   => ReportDeliveryFrequency::ID_DAILY,
        ReportDeliveryFrequency::WEEKLY  => ReportDeliveryFrequency::ID_WEEKLY,
        ReportDeliveryFrequency::MONTHLY => ReportDeliveryFrequency::ID_MONTHLY,
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'platform:report:scheduled:send
        {frequency : Supported values "daily",  "weekly", "monthly"}
        {--type= : Supported values "scheduled", "missing_ads_scheduled"}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate XLSX files for scheduled reports and send emails.';

    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ReportRepository
     */
    protected $reportRepository;

    /**
     * @param Dispatcher       $dispatcher
     * @param Logger           $log
     * @param ReportRepository $reportRepository
     */
    public function __construct(Dispatcher $dispatcher, Logger $log, ReportRepository $reportRepository)
    {
        parent::__construct();

        $this->dispatcher = $dispatcher;
        $this->log = $log;
        $this->reportRepository = $reportRepository;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->log->info('Running send scheduled report command.');

        $frequency = $this->getFrequencyId();
        $reportType = $this->getReportType();

        $reportTypes = is_null($reportType)
            ? array_keys(self::REPORT_PROCESSORS)
            : [$reportType];
        foreach ($reportTypes as $type) {
            $this->processReportsOfType($type, $frequency);
        }
    }

    /**
     * @param string $reportType
     * @param int    $frequency
     *
     * @return void
     * @throws \Modules\Report\Exceptions\ReportNotGeneratedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function processReportsOfType(string $reportType, int $frequency)
    {
        $this->log->info("Processing reports of \"{$reportType}\" type.");

        $reports = $this->getReports($reportType, $frequency);

        $reports->each(function (Report $report) {
            $this->log->info('Report will be sent.', ['report_id' => $report->id]);
            $this->processReport($report);
        });
    }

    /**
     * @return int
     */
    protected function getFrequencyId(): int
    {
        $frequency = $this->argument('frequency');
        $id = Arr::get($this->optionsMap, $frequency);

        if (is_null($id)) {
            throw new InvalidArgumentException(sprintf(
                'Wrong argument was given. Supported values are: %s.',
                implode(', ', array_keys($this->optionsMap))
            ));
        }

        $this->log->info('Selected frequency.', [
            'frequency'    => $frequency,
            'frequency_id' => $id,
        ]);

        return $id;
    }

    /**
     * @return string
     */
    protected function getReportType(): ?string
    {
        $type = $this->option('type');

        if (!is_null($type) && !array_key_exists($type, self::REPORT_PROCESSORS)) {
            throw new InvalidArgumentException(sprintf(
                'Wrong type ("%s") was given. Supported values are: %s.',
                $type,
                implode(', ', array_keys(self::REPORT_PROCESSORS))
            ));
        }

        $this->log->info('Selected report type.', ['report_type' => $type]);

        return $type;
    }

    /**
     * Get all reports that should be generated and sent.
     *
     * @param string $reportType
     * @param int    $frequencyId
     *
     * @return Collection
     * @throws \Modules\Report\Exceptions\ReportNotGeneratedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function getReports(string $reportType, int $frequencyId): Collection
    {
        $typeId = ReportType::getTypeId($reportType);

        $reports = $this->reportRepository
            ->byStatus([ReportStatus::ID_DRAFT, ReportStatus::ID_FAILED])
            ->byType($typeId)
            ->byDeliveryFrequencies([$frequencyId]);

        if ($reportType === ReportType::ID_SCHEDULED) {
            $reports->byActiveOrRecentlyCompletedCampaignsOrAdmins($frequencyId);
        }

        $reports = $reports->all();
        $this->reportRepository->reset();

        $this->log->info('Amount of reports to be sent.', ['amount' => $reports->count()]);

        return $reports;
    }

    /**
     * @param Report $report
     *
     * @return void
     */
    protected function processReport(Report $report): void
    {
        $processor = self::REPORT_PROCESSORS[$report->type->name];

        /** @var Job $job */
        $job = new $processor($report);

        $this->dispatcher->dispatch($job->onQueue(QueuePriority::low()));
    }
}
