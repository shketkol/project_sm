<?php

namespace Modules\Report\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Modules\Report\Models\Report;
use Modules\User\Models\User;

class ReportMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:report {user_id} {count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate test reports for user.';

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * ReportMakeCommand constructor.
     *
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        parent::__construct();
        $this->databaseManager = $databaseManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        $user = User::findOrFail($this->argument('user_id'));
        $count = $this->argument('count');

        $this->info(sprintf('Creating %d report(s) for User#%d', $count, $user->getKey()));
        $this->databaseManager->beginTransaction();

        try {
            for ($i = 1; $i <= $count; $i++) {
                $this->createReport($user);
            }

            $this->info('Done.');
        } catch (\Throwable $exception) {
            $this->warn(sprintf('Campaign creating failed. Reason: "%s".', $exception->getMessage()));
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * @param User $user
     *
     * @return Report
     */
    private function createReport(User $user): Report
    {
        $this->info('Creating report.');

        return Report::factory()->create([
            'user_id' => $user->id,
        ]);
    }
}
