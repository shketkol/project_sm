<?php

namespace Modules\Report\Repositories\Criteria;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Modules\Report\Models\ReportDeliveryFrequency;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class ExecutionDayReportCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    protected $ids;

    /**
     * ExecutionDayReportCriteria constructor.
     * @param array $ids
     */
    public function __construct(array $ids)
    {
        $this->ids = $ids;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Prettus\Repository\Contracts\RepositoryInterface $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function ($query) {
            $query->where([
                ['execution_day', '=', Carbon::now()->dayOfWeek + 1],
                ['delivery_frequency_id', '=', ReportDeliveryFrequency::ID_WEEKLY],
            ])->orWhereIn('delivery_frequency_id', $this->ids);
        });
    }
}
