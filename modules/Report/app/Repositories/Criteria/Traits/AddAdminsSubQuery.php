<?php

namespace Modules\Report\Repositories\Criteria\Traits;

use Illuminate\Database\Eloquent\Builder;
use Modules\User\Models\Role;
use Modules\User\Repositories\Criteria\RolesCriteria;
use Prettus\Repository\Contracts\RepositoryInterface;

trait AddAdminsSubQuery
{
    /**
     * @param Builder             $query
     * @param RepositoryInterface $repository
     *
     * @return Builder
     */
    private function createAdminsSubQuery(Builder $query, RepositoryInterface $repository): Builder
    {
        return $query->orWhere(function (Builder $query) use ($repository): Builder {
            return (new RolesCriteria(Role::ID_ADMIN, Role::ID_ADMIN_READ_ONLY))->apply($query, $repository);
        });
    }
}
