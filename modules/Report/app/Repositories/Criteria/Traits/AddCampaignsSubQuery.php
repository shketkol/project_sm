<?php

namespace Modules\Report\Repositories\Criteria\Traits;

use Illuminate\Database\Eloquent\Builder;
use Modules\Campaign\DataTable\Repositories\Criteria\ActiveCampaignsCriteria;
use Prettus\Repository\Contracts\RepositoryInterface;

trait AddCampaignsSubQuery
{
    /**
     * @param Builder             $query
     * @param RepositoryInterface $repository
     *
     * @return Builder
     */
    private function createCampaignsSubQuery(Builder $query, RepositoryInterface $repository): Builder
    {
        return $query->where(function (Builder $query) use ($repository): Builder {
            $query = (new ActiveCampaignsCriteria())->apply($query, $repository);
            $query = $this->createRecentlyCompletedSubQuery($query, $repository);

            return $query;
        });
    }
}
