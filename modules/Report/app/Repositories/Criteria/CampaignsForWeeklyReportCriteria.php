<?php

namespace Modules\Report\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Repositories\Criteria\RecentlyCompletedCampaignsCriteria;
use Modules\Report\Models\ReportDeliveryFrequency;
use Modules\Report\Repositories\Criteria\Traits\AddAdminsSubQuery;
use Modules\Report\Repositories\Criteria\Traits\AddCampaignsSubQuery;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class CampaignsForWeeklyReportCriteria implements CriteriaInterface
{
    use AddCampaignsSubQuery, AddAdminsSubQuery;

    /**
     * @param Model|Builder       $model
     * @param RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where(function (Builder $query) use ($repository): Builder {
            $query = $this->createCampaignsSubQuery($query, $repository);
            $query = $this->createAdminsSubQuery($query, $repository);

            return $query;
        });
    }

    /**
     * @param Builder             $query
     * @param RepositoryInterface $repository
     *
     * @return Builder
     */
    private function createRecentlyCompletedSubQuery(Builder $query, RepositoryInterface $repository): Builder
    {
        return $query->orWhere(function (Builder $query) use ($repository): Builder {
            $times = config('report.stop.retry.times.' . ReportDeliveryFrequency::ID_WEEKLY);
            return (new RecentlyCompletedCampaignsCriteria($times, 'WEEK'))->apply($query, $repository);
        });
    }
}
