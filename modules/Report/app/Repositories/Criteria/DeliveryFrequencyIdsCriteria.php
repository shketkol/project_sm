<?php

namespace Modules\Report\Repositories\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class DeliveryFrequencyIdsCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    protected $ids;

    /**
     * DeliveryFrequencyIdsCriteria constructor.
     * @param array $ids
     */
    public function __construct(array $ids)
    {
        $this->ids = $ids;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Prettus\Repository\Contracts\RepositoryInterface $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('delivery_frequency_id', $this->ids);
    }
}
