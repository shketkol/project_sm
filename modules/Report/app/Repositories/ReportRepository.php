<?php

namespace Modules\Report\Repositories;

use App\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Modules\Report\Exceptions\ReportNotGeneratedException;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportDeliveryFrequency;
use Modules\Report\Repositories\Contracts\ReportRepository as ReportRepositoryInterface;
use Modules\Report\Repositories\Criteria\AddUsersCriteria;
use Modules\Report\Repositories\Criteria\DeliveryFrequencyIdsCriteria;
use Modules\Report\Repositories\Criteria\ExecutionDayReportCriteria;
use Modules\Report\Repositories\Criteria\HasFileCriteria;
use Modules\Report\Repositories\Criteria\OlderThanCriteria;
use Modules\Report\Repositories\Criteria\StatusIdsCriteria;
use Modules\Report\Repositories\Criteria\TypeIdCriteria;
use Modules\Report\Repositories\Criteria\UserIdsCriteria;
use Modules\Targeting\Repositories\Criteria\DistinctCriteria;
use Modules\User\DataTable\Repositories\Criteria\AddCampaignsCriteria;
use Modules\User\DataTable\Repositories\Criteria\AddRolesCriteria;

class ReportRepository extends Repository implements ReportRepositoryInterface
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Report::class;
    }

    /**
     * Filter reports by status (few may be passed).
     *
     * @param array $ids
     *
     * @return ReportRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byStatus(array $ids): ReportRepositoryInterface
    {
        return $this->pushCriteria(new StatusIdsCriteria($ids));
    }

    /**
     * Filter reports by type id.
     *
     * @param int $typeId
     *
     * @return ReportRepositoryInterface
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byType(int $typeId): ReportRepositoryInterface
    {
        return $this->pushCriteria(new TypeIdCriteria($typeId));
    }

    /**
     * Filter reports by delivery frequency (few may be passed).
     *
     * @param array $ids
     *
     * @return ReportRepositoryInterface
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byDeliveryFrequencies(array $ids): ReportRepositoryInterface
    {
        if (in_array(ReportDeliveryFrequency::ID_WEEKLY, $ids)) {
            Arr::forget($ids, array_search(ReportDeliveryFrequency::ID_WEEKLY, $ids));
            return $this->pushCriteria(new ExecutionDayReportCriteria($ids));
        }

        return $this->pushCriteria(new DeliveryFrequencyIdsCriteria($ids));
    }

    /**
     * Filter reports by user ids.
     *
     * @param array $userIds
     *
     * @return ReportRepositoryInterface
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byUser(array $userIds): ReportRepositoryInterface
    {
        return $this->pushCriteria(new UserIdsCriteria($userIds));
    }

    /**
     * @param Carbon $date
     *
     * @return ReportRepositoryInterface
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function olderThan(Carbon $date): ReportRepositoryInterface
    {
        return $this->pushCriteria(new OlderThanCriteria($date));
    }

    /**
     * @return ReportRepositoryInterface
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function hasFile(): ReportRepositoryInterface
    {
        return $this->pushCriteria(new HasFileCriteria());
    }

    /**
     * @param int $frequencyId
     *
     * @return ReportRepositoryInterface
     * @throws ReportNotGeneratedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byActiveOrRecentlyCompletedCampaignsOrAdmins(int $frequencyId): ReportRepositoryInterface
    {
        $frequencyCriteria = config('report.stop.retry.criterion.' . $frequencyId);

        if (is_null($frequencyCriteria)) {
            throw new ReportNotGeneratedException('Report Frequency Criteria not found.');
        }

        $query = $this->pushCriteria(new DistinctCriteria('reports.*'))
            ->pushCriteria(new AddUsersCriteria())
            ->pushCriteria(new AddRolesCriteria())
            ->pushCriteria(new AddCampaignsCriteria());

        return $query->pushCriteria(new $frequencyCriteria);
    }
}
