<?php

namespace Modules\Report\DataTable;

use App\DataTable\DataTable;
use Illuminate\Database\Eloquent\Builder;
use Modules\Campaign\DataTable\Cards\ActiveCampaignsCard;
use Modules\Report\DataTable\Repositories\Contracts\ReportsDataTableRepository;
use Modules\Report\DataTable\Repositories\Criteria\AddAdvertiserCriteria;
use Modules\Report\DataTable\Repositories\Criteria\AddReportTypeCriteria;
use Modules\Report\DataTable\Repositories\Criteria\ExcludeAdvertisersCriteria;
use Modules\Report\DataTable\Repositories\Criteria\FrequencyCriteria;
use Modules\Report\DataTable\Repositories\Criteria\StatusCriteria;
use Modules\Report\DataTable\Repositories\Criteria\UserCriteria;
use Modules\Report\DataTable\Repositories\Filters\ReportRunDateFilter;
use Modules\Report\DataTable\Repositories\Filters\ReportStatusFilter;
use Modules\Report\DataTable\Transformers\ReportsTransformer;

class ReportDataTable extends DataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'reports';

    /**
     * Transformer class
     *
     * @var string
     */
    protected $transformer = ReportsTransformer::class;

    /**
     * Repository
     *
     * @var string
     */
    protected $repository = ReportsDataTableRepository::class;

    /**
     * Available cards
     *
     * @var array
     */
    protected $cards = [
        1 => ActiveCampaignsCard::class,
    ];

    /**
     * Filters
     *
     * @var array
     */
    protected $filters = [
        ReportStatusFilter::class,
        ReportRunDateFilter::class,
    ];

    /**
     * Add aliases according model relation.
     * Required for search
     *
     * @var array
     */
    protected $relationAliases = [
        'advertisers'   => 'users.company_name',
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        /** @var ReportsDataTableRepository $repository */
        $repository = $this->getRepository();

        return $this->applySortCriteria($repository)
            ->pushCriteria(new ExcludeAdvertisersCriteria())
            ->pushCriteria(new UserCriteria($this->getUser()))
            ->pushCriteria(new AddReportTypeCriteria())
            ->pushCriteria(new FrequencyCriteria())
            ->pushCriteria(new StatusCriteria())
            ->pushCriteria(new AddAdvertiserCriteria())
            ->reports();
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('report.list');
    }
}
