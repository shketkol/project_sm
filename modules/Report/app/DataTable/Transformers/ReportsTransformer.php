<?php

namespace Modules\Report\DataTable\Transformers;

use App\DataTable\Transformers\DataTableTransformer;
use Illuminate\Database\Eloquent\Model;
use Modules\Report\Http\Resources\ReportStatesResource;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportDays;
use Modules\Report\Models\ReportType;

class ReportsTransformer extends DataTableTransformer
{
    /**
     * Mapping fields.
     *
     * @var array
     */
    protected $mapMap = [
        'id'              => [
            'name'    => 'id',
            'default' => '-',
        ],
        'type'            => [
            'name'    => 'type',
            'default' => '-',
        ],
        'type_id'            => [
            'name'    => 'type_id',
            'default' => '-',
        ],
        'name'            => [
            'name'    => 'name',
            'default' => '-',
        ],
        'advertisers'     => [
            'name'    => 'advertisers',
            'default' => 'All advertisers',
        ],
        'generated_at'    => [
            'name'    => 'generated_at',
            'default' => '-',
        ],
        'frequency'       => [
            'name'    => 'frequency',
            'default' => '-',
        ],
        'report_status'   => [
            'name'    => 'report_status',
            'default' => '-',
        ],
        'status_id'   => [
            'name'    => 'status_id',
            'default' => '-',
        ],
        'update_required' => [
            'name'    => 'update_required',
            'default' => '-',
        ],
    ];

    /**
     * Do transform.
     *
     * @param \Illuminate\Database\Eloquent\Model|\Modules\Report\Models\Report $model
     *
     * @return array
     */
    public function transform(Model $model): array
    {
        $mapped = parent::transform($model);

        // Add url
        $mapped['url'] = $model->url;

        // Add states
        $mapped['states'] = new ReportStatesResource($model);

        if ($model->isSchedulable()) {
            $mapped['frequency'] = $this->formFrequencyLabel($model, $mapped['frequency']);
        }

        return $mapped;
    }

    /**
     * @param Model|Report  $model
     * @param string $value
     *
     * @return string
     */
    protected function formFrequencyLabel(Model $model, string $value): string
    {
        $result = trans('report::labels.frequency.' . $value);
        if ($model->delivery_frequency_id === config('report.show_days_frequency_id')) {
            $dayName = ReportDays::translateDayById($model->execution_day);
            $result = "$dayName ($result)";
        }

        return $result;
    }
}
