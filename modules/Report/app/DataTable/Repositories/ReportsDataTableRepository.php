<?php

namespace Modules\Report\DataTable\Repositories;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Report\Models\Report;
use Modules\Report\DataTable\Repositories\Contracts\ReportsDataTableRepository
    as ReportsDataTableRepositoryInterface;

class ReportsDataTableRepository extends Repository implements ReportsDataTableRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Report::class;
    }

    /**
     * Get user reports
     *
     * @return Builder
     */
    public function reports(): Builder
    {
        $this->applyCriteria();

        return $this->model->addSelect('reports.*');
    }
}
