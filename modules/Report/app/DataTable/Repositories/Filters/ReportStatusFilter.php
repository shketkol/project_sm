<?php

namespace Modules\Report\DataTable\Repositories\Filters;

use App\DataTable\Filters\DataTableFilter;
use Illuminate\Database\Eloquent\Builder;
use Modules\Report\Models\ReportStatus;
use Yajra\DataTables\DataTableAbstract;

class ReportStatusFilter extends DataTableFilter
{
    /**
     * Filter name
     *
     * @var string
     */
    public static $name = 'report-status';

    /**
     * @param DataTableAbstract $dataTable
     */
    public function filter(DataTableAbstract $dataTable): void
    {
        $this->makeFilter($dataTable, 'report_status', function (Builder $query, $values) {
            $query->orWhereIn('report_statuses.id', $values);
        });
    }

    /**
     * Get filter options
     *
     * @return array
     */
    public function options(): array
    {
        return ReportStatus::all()
            ->pluck('name', 'id')
            ->mapWithKeys(function (string $value, int $key) {
                return [$key => __("report::labels.statuses.{$value}")];
            })
            ->toArray();
    }
}
