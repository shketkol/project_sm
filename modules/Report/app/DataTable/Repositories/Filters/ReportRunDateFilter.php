<?php

namespace Modules\Report\DataTable\Repositories\Filters;

use App\DataTable\Filters\DataTableFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTableAbstract;

class ReportRunDateFilter extends DataTableFilter
{
    /**
     * Filter name
     *
     * @var string
     */
    public static $name = 'run_date';

    /**
     * Column name.
     */
    private const COLUMN = 'generated_at';

    /**
     * @param DataTableAbstract $dataTable
     */
    public function filter(DataTableAbstract $dataTable): void
    {
        $this->makeFilter($dataTable, self::COLUMN, function (Builder $query, $values) {
            if (empty($values) || count($values) !== 2) {
                return;
            }

            return $query->whereBetween(self::COLUMN, [
                Carbon::parse($values[0]),
                Carbon::parse($values[1])
            ]);
        });
    }
}
