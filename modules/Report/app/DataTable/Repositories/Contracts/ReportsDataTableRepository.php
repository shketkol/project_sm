<?php

namespace Modules\Report\DataTable\Repositories\Contracts;

use App\Repositories\Contracts\Repository;
use Illuminate\Database\Eloquent\Builder;

interface ReportsDataTableRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string;

    /**
     * Get user reports
     *
     * @return Builder
     */
    public function reports(): Builder;
}
