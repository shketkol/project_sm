<?php

namespace Modules\Report\DataTable\Repositories\Criteria;

use Modules\Report\Models\ReportType;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ExcludeAdvertisersCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Builder             $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('reports.type_id', [ReportType::ID_DOWNLOAD, ReportType::ID_SCHEDULED]);
    }
}
