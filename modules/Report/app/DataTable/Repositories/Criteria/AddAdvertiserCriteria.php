<?php

namespace Modules\Report\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddAdvertiserCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model
            ->addSelect('users.company_name as advertisers')
            ->leftJoin('users', 'reports.advertiser_id', '=', 'users.id');
    }
}
