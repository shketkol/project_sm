<?php

namespace Modules\Report\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class FrequencyCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model
            ->addSelect('report_delivery_frequencies.name as frequency')
            ->leftJoin(
                'report_delivery_frequencies',
                'reports.delivery_frequency_id',
                '=',
                'report_delivery_frequencies.id'
            );
    }
}
