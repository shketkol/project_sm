<?php

namespace Modules\Report\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Modules\Report\Models\ReportStatus;
use Modules\Report\Models\ReportType;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class StatusCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->selectRaw(
            'IF (report_statuses.id = ? AND report_types.id IN (?,?,?), ?, report_statuses.name) AS report_status',
            [
                ReportStatus::ID_DRAFT,
                ReportType::ID_DOWNLOAD,
                ReportType::ID_ADVERTISERS,
                ReportType::ID_MISSING_ADS_DOWNLOAD,
                ReportStatus::SUBMITTED
            ]
        )->leftJoin(
            'report_statuses',
            'reports.status_id',
            '=',
            'report_statuses.id'
        );
    }
}
