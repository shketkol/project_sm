<?php

namespace Modules\Report\DataTable;

use Illuminate\Database\Eloquent\Builder;
use Modules\Campaign\DataTable\Cards\TotalActiveCampaignsCard;
use Modules\Report\DataTable\Repositories\Contracts\ReportsDataTableRepository;
use Modules\Report\DataTable\Repositories\Criteria\AddAdvertiserCriteria;
use Modules\Report\DataTable\Repositories\Criteria\AddReportTypeCriteria;
use Modules\Report\DataTable\Repositories\Criteria\FrequencyCriteria;
use Modules\Report\DataTable\Repositories\Criteria\StatusCriteria;
use Modules\Report\DataTable\Repositories\Criteria\UserCriteria;

class AdminReportDataTable extends ReportDataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'admin-reports';

    /**
     * Available cards
     *
     * @var array
     */
    protected $cards = [
        1 => TotalActiveCampaignsCard::class,
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        /** @var ReportsDataTableRepository $repository */
        $repository = $this->getRepository();

        return $this->applySortCriteria($repository)
            ->pushCriteria(new UserCriteria($this->getUser()))
            ->pushCriteria(new AddReportTypeCriteria())
            ->pushCriteria(new FrequencyCriteria())
            ->pushCriteria(new StatusCriteria())
            ->pushCriteria(new AddAdvertiserCriteria())
            ->reports();
    }
}
