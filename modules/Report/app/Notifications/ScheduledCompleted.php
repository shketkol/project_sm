<?php

namespace Modules\Report\Notifications;

use App\Helpers\DateFormatHelper;
use App\Helpers\HtmlHelper;
use App\Models\QueuePriority;
use App\Notifications\Notification;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Support\Arr;
use Modules\Notification\Models\NotificationCategory;
use Modules\Report\Helpers\GenerateReportFilename;
use Modules\Report\Mail\Scheduled as Mail;
use Modules\Report\Models\Report;
use Modules\User\Models\User;

class ScheduledCompleted extends Notification
{
    use GenerateReportFilename;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * @var Report
     */
    protected $report;

    /**
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
        $this->onQueue(QueuePriority::low());
    }

    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(User $notifiable): array
    {
        return ['database'];
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_REPORT;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        /** @var HtmlHelper $html */
        $html = app(HtmlHelper::class);

        return __('report::notifications.advertiser.scheduled', [
            'report_name'       => e(Arr::get($data, 'reportName')),
            'reporting_section' => $html->createAnchorElement(
                Arr::get($data, 'reportListing', route('reports.index')),
                ['title' => __('report::labels.reporting_section')]
            ),
        ]);
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param User $notifiable
     *
     * @return Mailable
     * @throws \App\Exceptions\BaseException
     */
    public function toMail(User $notifiable): Mailable
    {
        $mail = parent::toMail($notifiable);
        $mail->attachFromStorage($this->report->path, $this->createFilename($this->report));

        return $mail;
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     * @throws \App\Exceptions\BaseException
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'reportName'      => $this->report->name,
            'reportListing'   => route('reports.index'),
            'dateFrom'        => DateFormatHelper::formattedDateTime($this->report->getDateFrom()),
            'dateTo'          => DateFormatHelper::formattedDateTime($this->report->getDateTo()),
            'campaignListing' => route('campaigns.index'),
            'isAdmin'         => $this->report->user->isAdmin(),
            'timezone'        => config('report.timezone'),
            'titleIcon'       => 'schedule',
            'title'           => __('report::emails.scheduled.title'),
        ];
    }
}
