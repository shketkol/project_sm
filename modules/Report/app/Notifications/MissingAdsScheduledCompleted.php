<?php

namespace Modules\Report\Notifications;

use App\Helpers\DateFormatHelper;
use App\Models\QueuePriority;
use App\Notifications\Notification;
use Carbon\Carbon;
use Illuminate\Contracts\Mail\Mailable;
use Modules\Notification\Models\EmailIcon;
use Modules\Report\Helpers\GenerateReportFilename;
use Modules\Report\Mail\MissingAdsScheduled as Mail;
use Modules\Report\Models\Report;
use Modules\User\Models\User;

class MissingAdsScheduledCompleted extends Notification
{
    use GenerateReportFilename;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * @var Report
     */
    protected $report;

    /**
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
        $this->onQueue(QueuePriority::low());
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param User $notifiable
     *
     * @return Mailable
     * @throws \App\Exceptions\BaseException
     */
    public function toMail(User $notifiable): Mailable
    {
        $mail = parent::toMail($notifiable);
        $mail->attachFromStorage($this->report->path, $this->createFilename($this->report));

        return $mail;
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'      => $notifiable->first_name,
            'reportName'     => $this->report->name,
            'generationDate' => DateFormatHelper::formatted(Carbon::now()),
            'timezone'       => config('report.timezone'),
            'titleIcon'      => EmailIcon::TYPE_SCHEDULE,
            'title'          => __('report::emails.missing_ads_scheduled.title'),
        ];
    }
}
