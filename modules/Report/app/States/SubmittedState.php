<?php

namespace Modules\Report\States;

use Modules\Report\Actions\DestroyReportFileAction;
use Modules\Report\Models\Report;

class SubmittedState
{
    /**
     * @var DestroyReportFileAction
     */
    protected $destroyReportFileAction;

    /**
     * SubmittedState constructor.
     * @param DestroyReportFileAction $destroyReportFileAction
     */
    public function __construct(DestroyReportFileAction $destroyReportFileAction)
    {
        $this->destroyReportFileAction = $destroyReportFileAction;
    }

    /**
     * @param Report $report
     */
    public function after(Report $report): void
    {
        // When we start to generate new report, old report file should be deleted
        if ($report->isScheduled()) {
            $this->destroyReportFileAction->handle($report);
        }
    }
}
