<?php

namespace Modules\Report\States;

use Modules\Report\Jobs\SendScheduledReport;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;

class CompletedState
{
    /**
     * @param Report $report
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function after(Report $report): void
    {
        if ($report->isSchedulable()) {
            // Scheduled reports should be automatically returned into DRAFT status.
            $report->load('status');
            $report->applyInternalStatus(ReportStatus::DRAFT);

            // Send email
            SendScheduledReport::dispatch($report);
        }
    }
}
