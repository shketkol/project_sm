<?php

namespace Modules\Report\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Report\Exceptions\ReportNotCreatedException;
use Modules\Report\Jobs\ProcessPendingCampaignsReport;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use Modules\Report\Repositories\Contracts\ReportRepository;

class StorePendingCampaignsReportAction
{
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ReportRepository
     */
    protected $repository;

    /**
     * @param ReportRepository $repository
     * @param DatabaseManager $databaseManager
     * @param Logger $log
     */
    public function __construct(
        ReportRepository $repository,
        DatabaseManager $databaseManager,
        Logger $log
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return Report
     * @throws \App\Exceptions\BaseException
     */
    public function handle(array $data = []): Report
    {
        $this->log->info('Storing new pending campaigns report.', ['data' => $data]);

        $this->databaseManager->beginTransaction();

        try {
            $report = $this->storeReport($data);
            ProcessPendingCampaignsReport::dispatch($report);
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();

            throw ReportNotCreatedException::createFrom($throwable);
        }

        $this->databaseManager->commit();

        return $report;
    }

    /**
     * @param array $data
     * @return Report
     */
    protected function storeReport(array $data): Report
    {
        $data['status_id'] = ReportStatus::ID_SUBMITTED;
        $report = $this->repository->create($data);
        return $report;
    }
}
