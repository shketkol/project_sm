<?php

namespace Modules\Report\Actions;

use Illuminate\Log\Logger;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;

class PauseReportAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * PauseReportAction constructor.
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * Pause the report.
     *
     * @param Report $report
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function handle(Report $report): void
    {
        $this->log->info('Pause the report.', ['report_id' => $report->id]);

        $report->applyInternalStatus(ReportStatus::PAUSED);
        $report->load('status');

        $this->log->info('Report was successfully paused.', ['report_id' => $report->id]);
    }
}
