<?php

namespace Modules\Report\Actions;

use Illuminate\Support\Arr;
use Modules\Report\Models\Report;

class StoreMissingAdsScheduledReportAction extends StoreReportAction
{
    /**
     * @param array $data
     * @return Report
     */
    protected function storeReport(array $data): Report
    {
        $report = parent::storeReport($data);
        $this->saveReportEmails($report, Arr::get($data, 'emails'));
        return $report;
    }
}
