<?php

namespace Modules\Report\Actions;

use Illuminate\Support\Arr;
use Modules\Report\Actions\Traits\ReportEmails;
use Modules\Report\Models\Report;

class UpdateScheduleReportAction extends UpdateReportAction
{
    /**
     * @param Report $report
     * @param array $data
     * @return Report
     */
    protected function updateReport(Report $report, array $data): Report
    {
        $report = parent::updateReport($report, $data);
        $this->saveReportEmails($report, Arr::get($data, 'emails'));
        return $report;
    }
}
