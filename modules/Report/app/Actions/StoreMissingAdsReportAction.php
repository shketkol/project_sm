<?php

namespace Modules\Report\Actions;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Report\Jobs\ProcessMissingAdsReport;
use Modules\Report\Models\Report;

class StoreMissingAdsReportAction extends StoreReportAction
{
    /**
     * @param Report $report
     *
     * @return ShouldQueue|null
     */
    protected function getProcessingJob(Report $report): ?ShouldQueue
    {
        return new ProcessMissingAdsReport($report);
    }
}
