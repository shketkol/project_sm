<?php

namespace Modules\Report\Actions;

use App\Exceptions\BaseException;
use Illuminate\Log\Logger;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use Modules\Report\Exceptions\ReportNotGeneratedException;
use Modules\Report\Exports\PendingCampaignReportExport\PendingCampaignReportExport;
use Modules\Report\Models\Report;
use PhpOffice\PhpSpreadsheet\Exception;

class GeneratePendingCampaignsReportAction
{
    /**
     * @var GetReportPendingCampaignsDataAction
     */
    protected $getCampaignDataAction;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var UploadReportAction
     */
    protected $uploadReportAction;

    /**
     * @var UpdateReportPathAction
     */
    protected $updateReportPathAction;

    /**
     * @param GetReportPendingCampaignsDataAction $getCampaignDataAction
     * @param Logger $log
     * @param UploadReportAction $uploadReportAction
     * @param UpdateReportPathAction $updateReportPathAction
     */
    public function __construct(
        GetReportPendingCampaignsDataAction $getCampaignDataAction,
        Logger $log,
        UploadReportAction $uploadReportAction,
        UpdateReportPathAction $updateReportPathAction
    ) {
        $this->getCampaignDataAction = $getCampaignDataAction;
        $this->log = $log;
        $this->uploadReportAction = $uploadReportAction;
        $this->updateReportPathAction = $updateReportPathAction;
    }

    /**
     * Generate excel file for the report.
     *
     * @param Report $report
     *
     * @return void
     * @throws BaseException
     * @throws CanNotApplyStatusException
     * @throws ReportNotGeneratedException
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \SM\SMException
     */
    public function handle(Report $report): void
    {
        $this->log->info('Report generation has been started.', ['report_id' => $report->id]);

        // Get report from local DB
        $data = $this->getCampaignDataAction->handle($report);

        // Generate XLSX file
        $export = new PendingCampaignReportExport($data);
        unset($data);

        // Upload XLSX file to S3
        $path = $this->uploadReportAction->handle($report, $export);

        // Save S3 link in DB and update report status
        $this->updateReportPathAction->handle($report, $path);

        $this->log->info('Report has been successfully generated.', ['report_id' => $report->id]);
    }
}
