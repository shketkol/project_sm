<?php

namespace Modules\Report\Actions;

use Modules\Report\Models\Report;
use Modules\Report\Repositories\Criteria\ReportIdCriteria;
use Modules\Report\Repositories\ReportRepository;

class IsActiveScheduledReportAction
{
    /**
     * @var ReportRepository
     */
    private $reportRepository;

    /**
     * @param ReportRepository $reportRepository
     */
    public function __construct(ReportRepository $reportRepository)
    {
        $this->reportRepository = $reportRepository;
    }

    /**
     * @param Report $report
     *
     * @return bool
     * @throws \Modules\Report\Exceptions\ReportNotGeneratedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Report $report): bool
    {
        return $this->reportRepository
            ->pushCriteria(new ReportIdCriteria($report->id))
            ->byActiveOrRecentlyCompletedCampaignsOrAdmins($report->delivery_frequency_id)
            ->exists();
    }
}
