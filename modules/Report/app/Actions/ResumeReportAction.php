<?php

namespace Modules\Report\Actions;

use Illuminate\Log\Logger;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;

class ResumeReportAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * PauseReportAction constructor.
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * Pause the report.
     *
     * @param Report $report
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function handle(Report $report): void
    {
        $this->log->info('Resume the report.', ['report_id' => $report->id]);

        $report->applyInternalStatus(ReportStatus::DRAFT);
        $report->load('status');

        $this->log->info('Report was successfully resumed.', ['report_id' => $report->id]);
    }
}
