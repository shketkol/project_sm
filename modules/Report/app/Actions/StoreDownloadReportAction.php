<?php

namespace Modules\Report\Actions;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Report\Jobs\ProcessReport;
use Modules\Report\Models\Report;

class StoreDownloadReportAction extends StoreReportAction
{
    /**
     * @param Report $report
     *
     * @return ShouldQueue|null
     */
    protected function getProcessingJob(Report $report): ?ShouldQueue
    {
        return new ProcessReport($report);
    }
}
