<?php

namespace Modules\Report\Actions;

use Carbon\Carbon;
use Modules\Report\Exports\MissingAdsReportExport\Data\MissingAdsDataProvider;
use Modules\Report\Exports\MissingAdsReportExport\Data\ReportExportData;
use Modules\Report\Models\Report;

class GetReportMissingAdsDataAction
{
    /**
     * @var MissingAdsDataProvider
     */
    protected $campaignsDataProvider;

    /**
     * @param MissingAdsDataProvider $campaignsDataProvider
     */
    public function __construct(MissingAdsDataProvider $campaignsDataProvider)
    {
        $this->campaignsDataProvider = $campaignsDataProvider;
    }

    /**
     * @param Report $report
     * @return ReportExportData
     * @throws \App\Exceptions\BaseException
     */
    public function handle(Report $report): ReportExportData
    {
        return new ReportExportData([
            'name'      => $report->name,
            'createdAt' => Carbon::now(config('date.default_timezone_full_code')), //America/New_York
            'createdBy' => $report->user->isAdmin() ? $report->user->email : $report->user->company_name,
            'campaigns' => $this->campaignsDataProvider->setReport($report),
        ]);
    }
}
