<?php

namespace Modules\Report\Actions;

use Illuminate\Log\Logger;
use Modules\Report\Exports\ReportExport\ReportExport;
use Modules\Report\Jobs\SendScheduledReport;
use Modules\Report\Models\Report;

class GenerateReportAction
{
    /**
     * @var GetReportDataAction
     */
    protected $getReportDataAction;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var UploadReportAction
     */
    protected $uploadReportAction;

    /**
     * @var UpdateReportPathAction
     */
    protected $updateReportPathAction;

    /**
     * @param GetReportDataAction    $getReportDataAction
     * @param Logger                 $log
     * @param UploadReportAction     $uploadReportAction
     * @param UpdateReportPathAction $updateReportPathAction
     */
    public function __construct(
        GetReportDataAction $getReportDataAction,
        Logger $log,
        UploadReportAction $uploadReportAction,
        UpdateReportPathAction $updateReportPathAction
    ) {
        $this->getReportDataAction = $getReportDataAction;
        $this->log = $log;
        $this->uploadReportAction = $uploadReportAction;
        $this->updateReportPathAction = $updateReportPathAction;
    }

    /**
     * @param Report $report
     * @return void
     * @throws \App\Exceptions\BaseException
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Modules\Report\Exceptions\NotAdminNorLiveCampaignsException
     * @throws \Modules\Report\Exceptions\ReportNotGeneratedException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \SM\SMException
     */
    public function handle(Report $report): void
    {
        $this->log->info('Report generation has been started.', ['report_id' => $report->id]);

        // Get report from HAAPI
        $data = $this->getReportDataAction->handle($report);

        // Generate XLSX file
        $export = new ReportExport($data);
        unset($data);

        // Upload XLSX file to S3
        $path = $this->uploadReportAction->handle($report, $export);

        // Save S3 link in DB and update report status
        $this->updateReportPathAction->handle($report, $path);

        $this->log->info('Report has been successfully generated.', ['report_id' => $report->id]);
    }
}
