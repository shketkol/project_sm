<?php

namespace Modules\Report\Actions;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Report\Jobs\ProcessMissingAdsReport;
use Modules\Report\Models\Report;

class UpdateMissingAdsDownloadReportAction extends UpdateReportAction
{
    /**
     * @param Report $report
     * @return ShouldQueue|null
     */
    protected function getProcessingJob(Report $report): ?ShouldQueue
    {
        return new ProcessMissingAdsReport($report);
    }

    /**
     * @param Report $report
     * @param array  $data
     * @return Report
     */
    protected function updateReport(Report $report, array $data): Report
    {
        $report = parent::updateReport($report, $data);
        $this->saveReportEmails($report, []);

        return $report;
    }
}
