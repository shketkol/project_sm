<?php

namespace Modules\Report\Actions;

use Carbon\Carbon;
use Modules\Report\Exports\PendingCampaignReportExport\Data\PendingCampaignsDataIterator;
use Modules\Report\Exports\PendingCampaignReportExport\Data\ReportExportData;
use Modules\Report\Models\Report;

class GetReportPendingCampaignsDataAction
{
    /**
     * @var PendingCampaignsDataIterator
     */
    protected $campaignsDataIterator;

    /**
     * @param PendingCampaignsDataIterator $campaignsDataIterator
     */
    public function __construct(PendingCampaignsDataIterator $campaignsDataIterator)
    {
        $this->campaignsDataIterator = $campaignsDataIterator;
    }

    /**
     * Get data from HAAPI.
     *
     * @param Report $report
     * @return ReportExportData
     * @throws \App\Exceptions\BaseException
     */
    public function handle(Report $report): ReportExportData
    {
        return new ReportExportData([
            'name'      => $report->name,
            'isAdmin'   => $report->user->isAdmin(),
            'createdAt' => Carbon::now(config('date.default_timezone_full_code')), //America/New_York
            'createdBy' => $report->user->isAdmin() ? $report->user->email : $report->user->company_name,
            'campaigns' => $this->campaignsDataIterator->setReport($report),
        ]);
    }
}
