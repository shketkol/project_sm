<?php

namespace Modules\Report\Actions;

use Illuminate\Log\Logger;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Exporter;
use Modules\Report\Exceptions\ReportNotGeneratedException;
use Modules\Report\Helpers\GenerateReportFilename;
use Modules\Report\Models\Report;

class UploadReportAction
{
    use GenerateReportFilename;

    /**
     * @var Exporter
     */
    protected $exporter;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var GetReportUploadPathAction
     */
    protected $pathAction;

    /**
     * UploadReportAction constructor.
     *
     * @param Exporter                  $exporter
     * @param Logger                    $log
     * @param GetReportUploadPathAction $pathAction
     */
    public function __construct(Exporter $exporter, Logger $log, GetReportUploadPathAction $pathAction)
    {
        $this->exporter = $exporter;
        $this->log = $log;
        $this->pathAction = $pathAction;
    }

    /**
     * Generates and uploads file on S3.
     *
     * @param Report             $report
     * @param WithMultipleSheets $export
     *
     * @return string
     * @throws ReportNotGeneratedException
     * @throws \App\Exceptions\BaseException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function handle(Report $report, WithMultipleSheets $export): string
    {
        $this->log->info('Uploading report to S3.', ['report_id' => $report->id]);
        $path = $this->getPath($report);
        $this->log->info('Path for report file has been generated.', ['report_id' => $report->id]);

        if (!$this->exporter->store($export, $path)) {
            $this->log->error('Report was not stored on S3.', ['report_id' => $report->id]);

            throw new ReportNotGeneratedException('Report file was not stored on disk.');
        }

        $this->log->info('Report was successfully uploaded to S3.', ['report_id' => $report->id]);

        return $path;
    }

    /**
     * Get full path for the report file.
     *
     * @param Report $report
     *
     * @return string
     * @throws \App\Exceptions\BaseException
     */
    protected function getPath(Report $report): string
    {
        return $this->pathAction->handle($report->user) . '/' . $this->createEncodedFilename($report);
    }
}
