<?php

namespace Modules\Report\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Report\Exceptions\ReportNotUpdatedException;
use Modules\Report\Jobs\ProcessPendingCampaignsReport;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use Modules\Report\Repositories\Contracts\ReportRepository;

class UpdatePendingCampaignsReportAction
{
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ReportRepository
     */
    protected $repository;

    /**
     * @param ReportRepository $repository
     * @param DatabaseManager $databaseManager
     * @param Logger $log
     */
    public function __construct(
        ReportRepository $repository,
        DatabaseManager $databaseManager,
        Logger $log
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param Report $report
     * @param array $data
     *
     * @return Report
     * @throws \App\Exceptions\BaseException
     */
    public function handle(Report $report, array $data = []): Report
    {
        $this->log->info('Updating pending campaigns report.', ['data' => $data, 'report_id' => $report->getKey()]);

        $this->databaseManager->beginTransaction();

        try {
            $report = $this->updateReport($report, $data);
            ProcessPendingCampaignsReport::dispatch($report);
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();
            throw ReportNotUpdatedException::createFrom($throwable);
        }

        $this->databaseManager->commit();

        return $report;
    }

    /**
     * @param Report $report
     * @param array $data
     *
     * @return Report
     */
    protected function updateReport(Report $report, array $data): Report
    {
        $data['status_id'] = ReportStatus::ID_DRAFT;
        $data['update_required'] = false;
        $report = $this->repository->update($data, $report->getKey());
        return $report;
    }
}
