<?php

namespace Modules\Report\Actions;

use Carbon\Carbon;
use Modules\Report\Exports\ReportExport\Data\CampaignsDataIterator;
use Modules\Report\Exports\ReportExport\Data\ReportExportData;
use Modules\Report\Models\Report;

class GetReportDataAction
{
    /**
     * @var CampaignsDataIterator
     */
    protected $campaignsDataIterator;

    /**
     * @param CampaignsDataIterator $campaignsDataIterator
     */
    public function __construct(CampaignsDataIterator $campaignsDataIterator)
    {
        $this->campaignsDataIterator = $campaignsDataIterator;
    }

    /**
     * Get data from HAAPI.
     *
     * @param Report $report
     *
     * @return ReportExportData
     * @throws \App\Exceptions\BaseException
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \Modules\Report\Exceptions\NotAdminNorLiveCampaignsException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Report $report): ReportExportData
    {
        return new ReportExportData([
            'name'      => $report->name,
            'isAdmin'   => $report->user->isAdmin(),
            'createdAt' => Carbon::now(config('date.default_timezone_full_code')), //America/New_York
            'createdBy' => $report->user->isAdmin() ? $report->user->email : $report->user->company_name,
            'dateFrom'  => $report->getDateFrom(),
            'dateTo'    => $report->getDateTo(),
            'campaigns' => $this->campaignsDataIterator->setReport($report),
        ]);
    }
}
