<?php

namespace Modules\Report\Actions\Metrics;

use Illuminate\Log\Logger;
use Modules\Campaign\Models\Campaign;
use Modules\Report\Exports\ReportMetricsExport\Sheets\FrequencySheet;

class GenerateFrequencySheetAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * @param Campaign $campaign
     *
     * @return FrequencySheet
     */
    public function handle(Campaign $campaign): FrequencySheet
    {
        $this->log->info('Report Metrics Frequency Sheet generation has been started.', [
            'campaign_id' => $campaign->id,
        ]);

        $sheet = new FrequencySheet($campaign);

        $this->log->info('Report Metrics Frequency Sheet generation has been finished.', [
            'campaign_id' => $campaign->id,
        ]);

        return $sheet;
    }
}
