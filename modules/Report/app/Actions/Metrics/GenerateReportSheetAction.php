<?php

namespace Modules\Report\Actions\Metrics;

use Illuminate\Log\Logger;
use Modules\Campaign\Actions\GetCampaignStatisticsAction;
use Modules\Campaign\Models\Campaign;
use Modules\Report\Exports\ReportMetricsExport\Sheets\ReportSheet;

class GenerateReportSheetAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var GetCampaignStatisticsAction
     */
    private $statisticsAction;

    /**
     * @param Logger                      $log
     * @param GetCampaignStatisticsAction $statisticsAction
     */
    public function __construct(Logger $log, GetCampaignStatisticsAction $statisticsAction)
    {
        $this->log = $log;
        $this->statisticsAction = $statisticsAction;
    }

    /**
     * @param Campaign $campaign
     *
     * @return ReportSheet
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(Campaign $campaign): ReportSheet
    {
        $this->log->info('Report Metrics Report Sheet generation has been started.', [
            'campaign_id' => $campaign->id,
        ]);

        $statistics = $this->statisticsAction->handle($campaign);
        $sheet = new ReportSheet($campaign, $statistics);

        $this->log->info('Report Metrics Report Sheet generation has been finished.', [
            'campaign_id' => $campaign->id,
        ]);

        return $sheet;
    }
}
