<?php

namespace Modules\Report\Actions\Metrics\Traits;

use App\Helpers\Math\CalculationHelper;
use Illuminate\Support\Arr;

trait FormatMetricPercentageTrait
{
    protected $metricKey = 'totalImpressionsByType';

    /**
     * @param array $metrics
     * @return array
     */
    protected function formatMetricPercentage(array $metrics): array
    {
        $metricsToFormat = Arr::get($metrics, $this->metricKey);

        if (!$metricsToFormat) {
            return $metrics;
        }

        return CalculationHelper::largestReminderPercentage($metricsToFormat, ['multiplier' => 100, 'precision' => 2]);
    }
}
