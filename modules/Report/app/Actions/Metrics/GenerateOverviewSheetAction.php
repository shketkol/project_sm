<?php

namespace Modules\Report\Actions\Metrics;

use Illuminate\Log\Logger;
use Modules\Campaign\Models\Campaign;
use Modules\Report\Exports\ReportMetricsExport\Sheets\OverviewSheet;
use Modules\User\Models\User;

class GenerateOverviewSheetAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * @param Campaign $campaign
     * @param User     $user
     *
     * @return OverviewSheet
     */
    public function handle(Campaign $campaign, User $user): OverviewSheet
    {
        $this->log->info('Report Metrics Overview Sheet generation has been started.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        $sheet = new OverviewSheet($campaign, $user);

        $this->log->info('Report Metrics Overview Sheet generation has been finished.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        return $sheet;
    }
}
