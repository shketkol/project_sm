<?php

namespace Modules\Report\Actions\Metrics;

use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\GetCampaignImpressionsMetricsAction;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsMetricsParams;
use Modules\Report\Actions\Metrics\Traits\FormatMetricPercentageTrait;
use Modules\Report\Exports\ReportMetricsExport\Sheets\GenresSheet;
use Modules\User\Models\User;

class GenerateGenresSheetAction
{
    use FormatMetricPercentageTrait;

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var GetCampaignImpressionsMetricsAction
     */
    private $metricsAction;

    /**
     * @param Logger                              $log
     * @param GetCampaignImpressionsMetricsAction $metricsAction
     */
    public function __construct(Logger $log, GetCampaignImpressionsMetricsAction $metricsAction)
    {
        $this->log = $log;
        $this->metricsAction = $metricsAction;
        $this->metricKey = 'lineItemImpressionsByType.0.impressions';
    }

    /**
     * @param Campaign $campaign
     *
     * @param User     $user
     *
     * @return GenresSheet
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(Campaign $campaign, User $user): GenresSheet
    {
        $this->log->info('Report Metrics Genres Sheet generation has been started.', [
            'campaign_id' => $campaign->id,
        ]);

        $metrics = $this->metricsAction->handle($campaign, $user, ImpressionsMetricsParams::TYPE_GENRE);
        $metrics = $this->formatMetricPercentage($metrics);

        $sheet = new GenresSheet($campaign, $metrics);

        $this->log->info('Report Metrics Genres Sheet generation has been finished.', [
            'campaign_id' => $campaign->id,
        ]);

        return $sheet;
    }
}
