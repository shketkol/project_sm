<?php

namespace Modules\Report\Actions\Metrics;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Log\Logger;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Report\Exceptions\ReportNotGeneratedException;
use Modules\Report\Exports\ReportMetricsExport\ReportMetricsExport;
use Modules\User\Models\User;

class GenerateReportMetricsAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var GenerateOverviewSheetAction
     */
    private $overviewSheetAction;

    /**
     * @var GenerateReportSheetAction
     */
    private $reportSheetAction;

    /**
     * @var GenerateGenresSheetAction
     */
    private $genresSheetAction;

    /**
     * @var GenerateDevicesSheetAction
     */
    private $devicesSheetAction;

    /**
     * @var GenerateFrequencySheetAction
     */
    private $frequencySheetAction;

    /**
     * @param Logger                       $log
     * @param GenerateOverviewSheetAction  $overviewSheetAction
     * @param GenerateReportSheetAction    $reportSheetAction
     * @param GenerateGenresSheetAction    $genresSheetAction
     * @param GenerateDevicesSheetAction   $devicesSheetAction
     * @param GenerateFrequencySheetAction $frequencySheetAction
     */
    public function __construct(
        Logger $log,
        GenerateOverviewSheetAction $overviewSheetAction,
        GenerateReportSheetAction $reportSheetAction,
        GenerateGenresSheetAction $genresSheetAction,
        GenerateDevicesSheetAction $devicesSheetAction,
        GenerateFrequencySheetAction $frequencySheetAction
    ) {
        $this->log = $log;
        $this->overviewSheetAction = $overviewSheetAction;
        $this->reportSheetAction = $reportSheetAction;
        $this->genresSheetAction = $genresSheetAction;
        $this->devicesSheetAction = $devicesSheetAction;
        $this->frequencySheetAction = $frequencySheetAction;
    }

    /**
     * @param Campaign             $campaign
     * @param User|Authenticatable $user
     *
     * @return string
     * @throws ReportNotGeneratedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(Campaign $campaign, User $user)
    {
        $this->log->info('Report Metrics generation has been started.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        $this->isReportAvailable($campaign);

        $sheets = [];
        $sheets[] = $this->overviewSheetAction->handle($campaign, $user);
        $sheets[] = $this->reportSheetAction->handle($campaign);
        $sheets[] = $this->genresSheetAction->handle($campaign, $user);
        $sheets[] = $this->devicesSheetAction->handle($campaign, $user);
//        $sheets[] = $this->frequencySheetAction->handle($campaign);

        // Generate XLSX file
        $report = new ReportMetricsExport(...$sheets);

        $this->log->info('Report Metrics generation has been finished.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        return $report;
    }

    /**
     * @param Campaign $campaign
     *
     * @throws ReportNotGeneratedException
     */
    private function isReportAvailable(Campaign $campaign): void
    {
        $allowed = [
            CampaignStatus::ID_READY,
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_PAUSED,
            CampaignStatus::ID_COMPLETED,
            CampaignStatus::ID_CANCELED,
            CampaignStatus::ID_PROCESSING,
            CampaignStatus::ID_SUSPENDED,
        ];

        if ($campaign->inState($allowed) && $campaign->external_id) {
            return;
        }

        throw new ReportNotGeneratedException(sprintf(
            'Report can not be generated for campaign with id "%d" and with status "%s".',
            $campaign->id,
            $campaign->status->name
        ), Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
