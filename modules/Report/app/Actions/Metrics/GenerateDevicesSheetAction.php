<?php

namespace Modules\Report\Actions\Metrics;

use Illuminate\Log\Logger;
use Modules\Campaign\Actions\GetCampaignImpressionsMetricsAction;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsMetricsParams;
use Modules\Report\Actions\Metrics\Traits\FormatMetricPercentageTrait;
use Modules\Report\Exports\ReportMetricsExport\Sheets\DevicesSheet;
use Modules\User\Models\User;

class GenerateDevicesSheetAction
{
    use FormatMetricPercentageTrait;

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var GetCampaignImpressionsMetricsAction
     */
    private $metricsAction;

    /**
     * @param Logger                              $log
     * @param GetCampaignImpressionsMetricsAction $metricsAction
     */
    public function __construct(Logger $log, GetCampaignImpressionsMetricsAction $metricsAction)
    {
        $this->log = $log;
        $this->metricsAction = $metricsAction;
    }

    /**
     * @param Campaign $campaign
     *
     * @param User     $user
     *
     * @return DevicesSheet
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(Campaign $campaign, User $user): DevicesSheet
    {
        $this->log->info('Report Metrics Devices Sheet generation has been started.', [
            'campaign_id' => $campaign->id,
        ]);

        $metrics = $this->metricsAction->handle($campaign, $user, ImpressionsMetricsParams::TYPE_DEVICE);
        $metrics = $this->formatMetricPercentage($metrics);

        $sheet = new DevicesSheet($campaign, $metrics);

        $this->log->info('Report Metrics Devices Sheet generation has been finished.', [
            'campaign_id' => $campaign->id,
        ]);

        return $sheet;
    }
}
