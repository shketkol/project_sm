<?php

namespace Modules\Report\Actions;

use Carbon\Carbon;
use Modules\Report\Exports\AdvertiserReportExport\Data\AdvertisersDataIterator;
use Modules\Report\Exports\AdvertiserReportExport\Data\ReportExportData;
use Modules\Report\Models\Report;

class GetAdvertiserReportDataAction
{
    /**
     * @var AdvertisersDataIterator
     */
    protected $advertisersDataIterator;

    /**
     * GetReportDataAction constructor.
     * @param AdvertisersDataIterator $advertisersDataIterator
     */
    public function __construct(AdvertisersDataIterator $advertisersDataIterator)
    {
        $this->advertisersDataIterator = $advertisersDataIterator;
    }

    /**
     * Get data from HAAPI.
     *
     * @param Report $report
     * @return ReportExportData
     * @throws \App\Exceptions\BaseException
     */
    public function handle(Report $report): ReportExportData
    {
        return new ReportExportData([
            'name'      => $report->name,
            'isAdmin'   => $report->user->isAdmin(),
            'createdAt' => Carbon::now(config('date.default_timezone_full_code')), //America/New_York
            'createdBy' => $report->user->isAdmin() ? $report->user->email : $report->user->company_name,
            'advertisers' => $this->advertisersDataIterator->setReport($report),
        ]);
    }
}
