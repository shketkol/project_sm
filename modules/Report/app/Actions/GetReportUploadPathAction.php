<?php

namespace Modules\Report\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Str;
use Modules\User\Models\User;

class GetReportUploadPathAction
{
    /**
     * @param Authenticatable|User $user
     *
     * @return string
     */
    public function handle(Authenticatable $user): string
    {
        return sprintf('%s/reports/%s', $user->getUserFolder(), Str::uuid()->toString());
    }
}
