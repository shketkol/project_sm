<?php

namespace Modules\Report\Actions;

use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Report\Actions\Traits\ReportEmails;
use Modules\Report\Exceptions\ReportNotCreatedException;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportEmail;
use Modules\Report\Models\ReportStatus;
use Modules\Report\Repositories\Contracts\ReportRepository;

class StoreReportAction
{
    use ReportEmails;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ReportRepository
     */
    protected $repository;

    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * StoreReportAction constructor.
     *
     * @param DatabaseManager  $databaseManager
     * @param Logger           $log
     * @param ReportRepository $repository
     * @param Dispatcher       $dispatcher
     */
    public function __construct(
        DatabaseManager $databaseManager,
        Logger $log,
        ReportRepository $repository,
        Dispatcher $dispatcher
    ) {
        $this->databaseManager = $databaseManager;
        $this->log             = $log;
        $this->repository      = $repository;
        $this->dispatcher      = $dispatcher;
    }

    /**
     * @param array $data
     *
     * @return Report
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function handle(array $data = []): Report
    {
        $this->log->info('Storing new report.', ['data' => $data]);

        $this->databaseManager->beginTransaction();

        try {
            $report = $this->storeReport($data);

            // Try to process the report if it should be processed.
            $processingJob = $this->getProcessingJob($report);
            if ($processingJob) {
                $this->dispatcher->dispatch($processingJob);
            }
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();

            throw ReportNotCreatedException::createFrom($throwable);
        }

        $this->databaseManager->commit();

        return $report;
    }

    /**
     * @param Report $report
     *
     * @return ShouldQueue|null
     */
    protected function getProcessingJob(Report $report): ?ShouldQueue
    {
        return null;
    }

    /**
     * @param array $data
     *
     * @return Report
     */
    protected function storeReport(array $data): Report
    {
        $data['status_id'] = ReportStatus::ID_DRAFT;
        $report            = $this->repository->create($data);

        return $report;
    }

    /**
     * @param Report $report
     * @param array  $emails
     * @return void
     */
    protected function saveReportEmails(Report $report, array $emails): void
    {
        $emailsModelArray = array_map(function ($email) {
            return new ReportEmail(['email' => $email]);
        }, $emails);
        $report->emails()->delete();
        $report->emails()->saveMany($emailsModelArray);
    }
}
