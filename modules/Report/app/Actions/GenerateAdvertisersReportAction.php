<?php

namespace Modules\Report\Actions;

use Illuminate\Log\Logger;
use Modules\Report\Exports\AdvertiserReportExport\AdvertiserReportExport;
use Modules\Report\Models\Report;

class GenerateAdvertisersReportAction
{
    /**
     * @var GetAdvertiserReportDataAction
     */
    protected $getReportDataAction;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var UploadReportAction
     */
    protected $uploadReportAction;

    /**
     * @var UpdateReportPathAction
     */
    protected $updateReportPathAction;

    /**
     * @param GetAdvertiserReportDataAction $getReportDataAction
     * @param Logger                        $log
     * @param UploadReportAction            $uploadReportAction
     * @param UpdateReportPathAction        $updateReportPathAction
     */
    public function __construct(
        GetAdvertiserReportDataAction $getReportDataAction,
        Logger $log,
        UploadReportAction $uploadReportAction,
        UpdateReportPathAction $updateReportPathAction
    ) {
        $this->getReportDataAction = $getReportDataAction;
        $this->log = $log;
        $this->uploadReportAction = $uploadReportAction;
        $this->updateReportPathAction = $updateReportPathAction;
    }

    /**
     * Generate excel file for the report.
     *
     * @param Report $report
     *
     * @throws \App\Exceptions\BaseException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Modules\Report\Exceptions\ReportNotGeneratedException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \SM\SMException
     */
    public function handle(Report $report)
    {
        $this->log->info('Report generation has been started.', ['report_id' => $report->id]);

        // Get report from HAAPI
        $data = $this->getReportDataAction->handle($report);

        // Generate XLSX file
        $export = new AdvertiserReportExport($data);
        unset($data);

        // Upload XLSX file to S3
        $path = $this->uploadReportAction->handle($report, $export);

        // Save S3 link in DB and update report status
        $this->updateReportPathAction->handle($report, $path);

        $this->log->info('Report has been successfully generated.', ['report_id' => $report->id]);
    }
}
