<?php

Route::middleware(['auth:admin'])->group(function () {
    // Index
    Route::get('/', 'AdvertiserIndexController')->name('index')->middleware('can:advertiser.list');

    // Show
    Route::group(['prefix' => '/{advertiser}', 'middleware' => 'can:advertiser.view,advertiser'], function () {
        Route::get('/{tab?}', 'AdvertiserShowController')->name('show');
        Route::get('/campaigns/{campaignId?}', 'AdvertiserShowController');
    });
});
