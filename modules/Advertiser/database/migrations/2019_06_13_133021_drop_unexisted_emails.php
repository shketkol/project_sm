<?php

use Illuminate\Database\Migrations\Migration;
use Modules\User\Models\User;

class DropUnexistedEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        User::where('email', 'full-rights-admin-platform@hulu.com')->delete();
        User::where('email', 'read-only-admin-platform@hulu.com')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        //
    }
}
