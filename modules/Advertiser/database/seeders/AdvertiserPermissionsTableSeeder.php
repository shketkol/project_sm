<?php

namespace Modules\Advertiser\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Advertiser\Policies\AdvertiserPolicy;
use Modules\User\Models\Role;

class AdvertiserPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [
            AdvertiserPolicy::PERMISSION_LIST,
            AdvertiserPolicy::PERMISSION_VIEW,
            AdvertiserPolicy::PERMISSION_ACTIVATE,
            AdvertiserPolicy::PERMISSION_DEACTIVATE,
            AdvertiserPolicy::PERMISSION_UPDATE_ACCOUNT_TYPE,
        ],
        Role::ID_ADMIN_READ_ONLY => [
            AdvertiserPolicy::PERMISSION_LIST,
            AdvertiserPolicy::PERMISSION_VIEW,
        ],
        Role::ID_ADVERTISER      => [],
    ];
}
