@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('advertiser::emails.advertiser.activated.body.your_account_for', [
            'publisher_company_full_name' => config('general.company_full_name'),
            'company_name' => $companyName,
        ]) }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('advertiser::emails.advertiser.activated.body.you_can_now_book_campaigns') }}
    </p>

    @include('common.email.part.button', [
        'link' => $bookNewCampaign,
        'text' => __('advertiser::emails.advertiser.activated.body.book_new_campaign'),
    ])
@stop
