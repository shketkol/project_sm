@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <show-page :id="{{ $id }}"></show-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/resitrevda.js') }}"></script>
@endpush

