<?php

return [
    'advertiser' => [
        'activated'   => [
            'subject' => ':publisher_company_full_name account for :company_name has been reactivated',
            'title'   => 'Account Reactivated',
            'body'    => [
                'your_account_for'             => 'Good news! Your :publisher_company_full_name account for :company_name is active again.',
                'you_can_now_book_campaigns'   => 'To submit a new campaign, click the link below:',
                'book_new_campaign'            => 'Submit new campaign',
            ],
        ],
        'deactivated' => [
            'subject' => 'Your :publisher_company_full_name account for :company_name has been deactivated',
            'title'   => 'Account Deactivated',
            'body'    => [
                'your_publisher_account_suspended'            => 'Your :publisher_company_full_name account for :company_name has been deactivated.',
            ],
        ],
    ],
];
