<?php

return [
    'account_created'                => 'Account Created',
    'advertisers'                    => 'Advertisers',
    'search_advertisers_placeholder' => 'Search Advertisers',
    'booked_spend'                   => 'Spent To Date',
    'campaigns'                      => 'Campaigns',
    'no_advertisers'                 => 'There Are No Advertisers',
    'number_of_campaigns'            => 'Number of Campaigns',
    'user_status'                    => 'Status',
    'company_name'                   => 'Name',
    'last_login'                     => 'Last Login',
    'status'                         => [
        'approved'    => 'Approved',
        'deactivated' => 'Deactivated',
    ],
    'advertiser_status'              => 'Advertiser Status',
    'advertiser_details'             => 'Advertiser Details',
    'download_report'                => 'Export',
    'not_right'                      => 'Something\'s not right!',
    'advertiser_sfdc_link'           => 'Advertiser SFDC Link',
    'brand_sfdc_link'                => 'Brand SFDC Link',
    'contact_sfdc_link'              => 'Contact SFDC Link',
    'enable_special_ads_category'    => 'Enable Special Ads Category',
];
