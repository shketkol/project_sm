<?php

return [
    'advertiser' => [
        'deactivated' => 'Your account for :company_name has been suspended and you will no longer be able to submit campaigns. If you have any questions, please use the Contact Us form.',
        'activated'   => 'Your account for :company_name has been reactivated and you can now submit campaigns.',
    ],
];
