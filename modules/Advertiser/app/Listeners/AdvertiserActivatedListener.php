<?php

namespace Modules\Advertiser\Listeners;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Advertiser\Events\AdvertiserActivated;
use Modules\User\Models\User;
use Modules\Advertiser\Notifications\Advertiser\AdvertiserActivated as AdvertiserActivatedNotification;
use Psr\Log\LoggerInterface;

class AdvertiserActivatedListener
{
    /**
     * @var Dispatcher
     */
    private $notification;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * AdvertiserActivatedListener constructor.
     *
     * @param Dispatcher      $notification
     * @param LoggerInterface $log
     */
    public function __construct(Dispatcher $notification, LoggerInterface $log)
    {
        $this->notification = $notification;
        $this->log = $log;
    }

    /**
     * Handle the event.
     *
     * @param AdvertiserActivated $event
     */
    public function handle(AdvertiserActivated $event): void
    {
        $this->sendAdvertiserNotification($event->advertiser, $event->admin);
    }

    /**
     * @param User $advertiser
     * @param User $admin
     */
    private function sendAdvertiserNotification(User $advertiser, User $admin): void
    {
        $this->notification->send($advertiser, new AdvertiserActivatedNotification($advertiser));

        $this->log->info('Notification "AdvertiserActivated" sent for advertiser.', [
            'advertiser' => $advertiser->getKey(),
            'admin'      => $admin->getKey(),
        ]);
    }
}
