<?php

namespace Modules\Advertiser\DataTable\Repositories;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Advertiser\DataTable\Repositories\Contracts\AdvertiserDataTableRepository
    as AdvertiserDataTableRepositoryContract;
use Modules\User\Models\User;

class AdvertiserDataTableRepository extends Repository implements AdvertiserDataTableRepositoryContract
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Get user creatives
     *
     * @return Builder
     */
    public function users(): Builder
    {
        $this->applyCriteria();

        return $this->model->addSelect([
            'users.id',
            'users.company_name',
            'users.status_id',
            'users.created_at',
            'users.last_login'
        ]);
    }
}
