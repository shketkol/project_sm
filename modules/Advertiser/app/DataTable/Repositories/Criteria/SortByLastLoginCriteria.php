<?php

namespace Modules\Advertiser\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SortByLastLoginCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $direction;

    /**
     * SortByLastLoginCriteria constructor.
     * @param string $direction
     */
    public function __construct(string $direction)
    {
        $this->direction = $direction;
    }

    /**
     * Apply criteria in query repository
     *
     * @param Model|Builder       $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->orderBy('user_statuses.name', $this->direction)->orderByDesc('users.last_login');
    }
}
