<?php

namespace Modules\Advertiser\Http\Requests;

use App\Http\Requests\SearchRequest;

class SearchAdvertiserRequest extends SearchRequest
{
    /**
     * @var array
     */
    protected $validationRulesArray = ['nullable', 'string', 'max'];

    /**
     * Don`t override default search value
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        return;
    }
}
