<?php

namespace Modules\Advertiser\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Advertiser\Actions\GetAdvertiserAction;
use Modules\Advertiser\Http\Resources\AdvertiserResource;
use Modules\User\Models\User;

class ShowAdvertiserController extends Controller
{
    /**
     * Show advertiser details info.
     *
     * @param GetAdvertiserAction $action
     * @param User                $advertiser
     *
     * @return AdvertiserResource
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __invoke(GetAdvertiserAction $action, User $advertiser)
    {
        return $action->handle($advertiser);
    }
}
