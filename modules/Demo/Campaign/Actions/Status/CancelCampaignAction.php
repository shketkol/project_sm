<?php

namespace Modules\Demo\Campaign\Actions\Status;

use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\User\Models\User;
use \Modules\Campaign\Actions\Status\CancelCampaignAction as OriginCancelCampaignAction;

class CancelCampaignAction extends OriginCancelCampaignAction
{
    /**
     * Cancel campaign.
     *
     * @param Campaign $campaign
     * @param User $user
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     * @throws \Modules\Campaign\Exceptions\SameStatusException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function handle(Campaign $campaign, User $user): Campaign
    {
        $this->log->info('Campaign cancel started.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        $this->canApply($campaign, $user, CampaignStatus::CANCELED);
        $campaign->applyInternalStatus(CampaignStatus::CANCELED);

        $this->log->info('Campaign cancel finished.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        return $campaign;
    }
}
