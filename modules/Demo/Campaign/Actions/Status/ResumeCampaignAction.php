<?php

namespace Modules\Demo\Campaign\Actions\Status;

use Modules\Campaign\Actions\Status\ResumeCampaignAction as OriginResumeCampaignAction;
use Modules\Campaign\Models\Campaign;
use Modules\Demo\Helpers\CampaignHelper;
use Modules\User\Models\User;

class ResumeCampaignAction extends OriginResumeCampaignAction
{
    /**
     * @inheritdoc
     */
    public function handle(Campaign $campaign, User $user): Campaign
    {
        $this->log->info('Campaign resume started.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        /** @var CampaignHelper $helper */
        $helper = app(CampaignHelper::class);
        $helper->resume($campaign, $user);

        $this->log->info('Campaign resumed.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        $this->sendNotification($campaign, $user);

        return $campaign;
    }
}
