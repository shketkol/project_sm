<?php

namespace Modules\Demo\Support\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class ContactController extends Controller
{
    /**
     * Send contact us message.
     *
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        return response()->json([
            'data' => [
                'message' => trans('support::messages.message_sent'),
            ],
        ]);
    }
}
