<?php

namespace Modules\Demo\Http\Middleware;

use Illuminate\Http\Request;
use Modules\Demo\Exceptions\UsersLimitExceedException;
use Modules\Demo\Helpers\AuthHelper;

class UsersLimit
{
    /**
     * @param Request  $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws UsersLimitExceedException
     */
    public function handle(Request $request, \Closure $next)
    {
        if (AuthHelper::isLoginAllowed()) {
            return $next($request);
        }

        throw UsersLimitExceedException::create('User limit exceed. Please try again later.');
    }
}
