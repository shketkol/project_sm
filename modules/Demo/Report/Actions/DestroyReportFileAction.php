<?php

namespace Modules\Demo\Report\Actions;

use Modules\Report\Actions\DestroyReportFileAction as BaseDestroyReportFileAction;
use Modules\Report\Models\Report;

class DestroyReportFileAction extends BaseDestroyReportFileAction
{
    /**
     * Do not delete pre-seeded report files from the storage
     *
     * @inheritdoc
     */
    public function handle(Report $report): void
    {
        $list = array_column(config('demo.storage.reports.records', []), 'path');

        if (in_array($report->path, $list)) {
            return;
        }

        parent::handle($report);
    }
}
