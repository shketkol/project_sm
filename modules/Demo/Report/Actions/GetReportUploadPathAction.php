<?php

namespace Modules\Demo\Report\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Str;
use Modules\Report\Actions\GetReportUploadPathAction as BaseGetReportUploadPathAction;

class GetReportUploadPathAction extends BaseGetReportUploadPathAction
{
    /**
     * @inheritdoc
     */
    public function handle(Authenticatable $user): string
    {
        return sprintf('%s/reports/%s', $this->getDemoUserFolder(), Str::uuid()->toString());
    }

    /**
     * @return string
     */
    private function getDemoUserFolder(): string
    {
        return config('demo.storage.reports.folder');
    }
}
