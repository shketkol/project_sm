<?php

namespace Modules\Demo\View;

use App\View\ViewServiceProvider;
use Modules\Demo\View\Engines\DemoCompilerEngine;

class DemoViewServiceProvider extends ViewServiceProvider
{
    public function register(): void
    {
        $resolver = $this->app['view']->getEngineResolver();
        $resolver->register('blade', function(){
            return new DemoCompilerEngine($this->app['blade.compiler']);
        });
    }
}
