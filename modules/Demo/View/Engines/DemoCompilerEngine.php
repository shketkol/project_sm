<?php

namespace Modules\Demo\View\Engines;

use App\View\Engines\CompilerEngine;
use App\View\Exceptions\HaapiViewException;
use Exception;
use Illuminate\Support\Facades\Auth;
use Modules\Demo\Exceptions\InvalidTokenException;
use Modules\Haapi\Exceptions\HaapiException;
use Throwable;

class DemoCompilerEngine extends CompilerEngine
{
    /**
     * Handle a view exception.
     *
     * @param Throwable $e
     * @param int $obLevel
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws Throwable
     * @throws Exception
     */
    protected function handleViewException(Throwable $e, $obLevel)
    {
        if ($e instanceof InvalidTokenException) {
            Auth::logout();
            throw $e;
        }

        parent::handleViewException($e, $obLevel);
    }
}
