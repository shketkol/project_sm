<?php

namespace Modules\Demo\Commands;

use Illuminate\Console\Command;
use Modules\Campaign\DataTable\Repositories\Criteria\AddCreativesCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\SelectCampaignsCriteria;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Campaign\Repositories\Criteria\AddStatusCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\ApprovedCreativeCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\CreativePendingReviewCriteria;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Repositories\CreativeRepository;
use Modules\Demo\Campaign\Repositories\Criteria\DemoCampaignCriteria;
use Modules\Demo\Creative\Repositories\Criteria\DemoCreativeCriteria;
use Modules\Demo\Commands\Traits\ApplyStatus;

class StatusNotificationCallbackCommand extends Command
{
    use ApplyStatus;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:notification:status_update {--campaign} {--creative}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Emulate status notification callback from HAAPI.';

    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * @var CreativeRepository
     */
    private $creativeRepository;

    /**
     * @param CampaignRepository $campaignRepository
     * @param CreativeRepository $creativeRepository
     */
    public function __construct(CampaignRepository $campaignRepository, CreativeRepository $creativeRepository)
    {
        parent::__construct();
        $this->campaignRepository = $campaignRepository;
        $this->creativeRepository = $creativeRepository;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        if ($this->option('creative')) {
            $this->creativesChangeStatus();
        }

        if ($this->option('campaign')) {
            $this->campaignsChangeStatus();
        }
    }

    /**
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function campaignsChangeStatus(): void
    {
        $campaigns = $this->campaignRepository
            ->pushCriteria(new SelectCampaignsCriteria())
            ->pushCriteria(new AddCreativesCriteria())
            ->pushCriteria(new DemoCreativeCriteria())
            ->pushCriteria(new DemoCampaignCriteria())
            ->pushCriteria(new AddStatusCriteria([CampaignStatus::ID_PENDING_APPROVAL]))
            ->pushCriteria(new ApprovedCreativeCriteria())
            ->get();

        $this->applyStatusReady($campaigns);
    }

    /**
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function creativesChangeStatus(): void
    {
        $creatives = $this->creativeRepository
            ->pushCriteria(new DemoCreativeCriteria())
            ->pushCriteria(new CreativePendingReviewCriteria())
            ->get();

        foreach ($creatives as $creative) {
            $creative->applyInternalStatus(CreativeStatus::APPROVED);
        }

        $campaigns = $this->campaignRepository
            ->pushCriteria(new SelectCampaignsCriteria())
            ->pushCriteria(new AddCreativesCriteria())
            ->pushCriteria(new DemoCreativeCriteria())
            ->pushCriteria(new DemoCampaignCriteria())
            ->pushCriteria(new AddStatusCriteria([CampaignStatus::ID_PENDING_APPROVAL]))
            ->findWhereIn('creatives.id', $creatives->pluck('id')->toArray());

        $this->applyStatusReady($campaigns);
    }
}
