<?php

namespace Modules\Demo\Commands;

use Illuminate\Console\Command;
use Modules\Demo\User\Repositories\Criteria\DemoUserCriteria;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;
use Modules\User\Repositories\UserRepository;

class AccountUpdateCallbackCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:callbacks:account_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Emulate update accounts callback from HAAPI.';

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        /** @var User[] $users */
        $users = $this->repository
            ->pushCriteria(new DemoUserCriteria())
            ->findWhere([
                ['lock_update', '=', 1],
            ]);

        foreach ($users as $user) {
            $user->applyInternalStatus(UserStatus::ACTIVE);
            $user->unlockUpdate();
        }
    }
}
