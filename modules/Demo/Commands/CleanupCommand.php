<?php

namespace Modules\Demo\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Demo\Actions\CleanupAction;
use Modules\Demo\User\Repositories\Criteria\DemoUserCriteria;
use Modules\User\Repositories\UserRepository;
use Psr\Log\LoggerInterface;

class CleanupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup demo advertisers and all their data.';

    /**
     * @var CleanupAction
     */
    private $cleanupAction;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CleanupAction   $cleanupAction
     * @param UserRepository  $userRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        CleanupAction $cleanupAction,
        UserRepository $userRepository,
        LoggerInterface $logger
    ) {
        parent::__construct();
        $this->cleanupAction = $cleanupAction;
        $this->userRepository = $userRepository;
        $this->logger = $logger;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Throwable
     */
    public function handle(): void
    {
        $date = Carbon::now()->subMinutes(config('demo.users.ttl'));
        $users = $this->userRepository->pushCriteria(new DemoUserCriteria())
            ->findWhere([
                ['created_at', '<', $date],
            ])->all();

        if (count($users) === 0) {
            return;
        }

        $this->logger->info('Started deleting demo users.', [
            'count' => count($users),
        ]);

        foreach ($users as $user) {
            $this->cleanupAction->handle($user);
        }

        $this->logger->info('Finished deleting demo users.');
    }
}
