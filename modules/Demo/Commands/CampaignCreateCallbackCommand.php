<?php

namespace Modules\Demo\Commands;

use Illuminate\Console\Command;
use Modules\Campaign\DataTable\Repositories\Criteria\AddUsersCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\SelectCampaignsCriteria;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Campaign\Repositories\Criteria\AddStatusCriteria;
use Modules\Demo\Commands\Traits\ApplyStatus;
use Modules\Demo\User\Repositories\Criteria\DemoUserCriteria;

class CampaignCreateCallbackCommand extends Command
{
    use ApplyStatus;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:callbacks:campaign_create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Emulate create campaign callback from HAAPI.';

    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @param CampaignRepository $repository
     */
    public function __construct(CampaignRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        $campaigns = $this->repository
            ->pushCriteria(new SelectCampaignsCriteria())
            ->pushCriteria(new AddUsersCriteria())
            ->pushCriteria(new DemoUserCriteria())
            ->pushCriteria(new AddStatusCriteria([CampaignStatus::ID_PROCESSING]))
            ->findWhere([
                ['campaigns.order_id', '=', null],
                ['campaigns.external_id', '=', null],
            ]);

        $this->applyCampaignsStatus($campaigns, true);
    }
}
