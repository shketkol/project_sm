<?php

namespace Modules\Demo\Commands\Traits;

use Faker\Generator;
use Illuminate\Database\Eloquent\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Creative\Models\CreativeStatus;
use Modules\Demo\Helpers\ExternalIdHelper;

trait ApplyStatus
{
    /**
     * @param Collection|Campaign[] $campaigns
     * @param bool                  $setOrderId
     *
     * @return void
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    private function applyCampaignsStatus(Collection $campaigns, bool $setOrderId = true): void
    {
        $faker = app(Generator::class);

        foreach ($campaigns as $campaign) {
            $creative = $campaign->getActiveCreative();

            if ($creative && $creative->status_id === CreativeStatus::ID_PROCESSING) {
                $creative->applyInternalStatus(CreativeStatus::PENDING_APPROVAL);
            }

            if ($setOrderId) {
                $campaign->order_id = $faker->unique()->numberBetween(1, 999999);
                $campaign->external_id = ExternalIdHelper::createCampaignExternalId();
            }

            if ($creative && $setOrderId) {
                $creative->external_id = ExternalIdHelper::createCreativeExternalId();
                $creative->save();
            }

            $campaign->applyInternalStatus(CampaignStatus::PENDING_APPROVAL);
        }
    }

    /**
     * @param Collection $campaigns
     *
     * @return void
     */
    private function applyStatusReady(Collection $campaigns): void
    {
        foreach ($campaigns as $campaign) {
            $campaign->applyInternalStatus(CampaignStatus::READY);
        }
    }
}
