<?php

namespace Modules\Demo\Commands;

use Illuminate\Console\Command;
use Modules\Campaign\DataTable\Repositories\Criteria\AddCreativesCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\SelectCampaignsCriteria;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Campaign\Repositories\Criteria\AddStatusCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\ApprovedCreativeCriteria;
use Modules\Demo\Campaign\Repositories\Criteria\DemoCampaignCriteria;
use Modules\Demo\Creative\Repositories\Criteria\DemoCreativeCriteria;
use Modules\Demo\Commands\Traits\ApplyStatus;

class CreativeUpdateCallbackCommand extends Command
{
    use ApplyStatus;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:callbacks:creative_update {--ready}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Emulate update creative callback from HAAPI.';

    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @param CampaignRepository $repository
     */
    public function __construct(CampaignRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        if ($this->option('ready')) {
            $this->updateStatusToReady();
            return;
        }

        $campaigns = $this->repository
            ->pushCriteria(new DemoCampaignCriteria())
            ->pushCriteria(new AddStatusCriteria([CampaignStatus::ID_PROCESSING]))
            ->findWhere([
                ['status_changed_by', '=', null],
            ]);

        $this->applyCampaignsStatus($campaigns);
    }

    /**
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function updateStatusToReady(): void
    {
        $campaigns = $this->repository
            ->pushCriteria(new SelectCampaignsCriteria())
            ->pushCriteria(new AddCreativesCriteria())
            ->pushCriteria(new DemoCreativeCriteria())
            ->pushCriteria(new DemoCampaignCriteria())
            ->pushCriteria(new ApprovedCreativeCriteria())
            ->pushCriteria(new AddStatusCriteria([CampaignStatus::ID_PROCESSING]))
            ->get();

        $this->applyStatusReady($campaigns);
    }
}
