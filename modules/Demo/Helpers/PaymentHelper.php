<?php

namespace Modules\Demo\Helpers;

use Modules\Payment\Models\PaymentMethod;
use Modules\User\Models\User;

class PaymentHelper
{
    /**
     * @param User $advertiser
     *
     * @return PaymentMethod
     */
    public function firstOrCreate(User $advertiser): PaymentMethod
    {
        /** @var PaymentMethod|null $method */
        $method = $advertiser->paymentMethods()->first();

        if (!is_null($method)) {
            return $method;
        }

        return PaymentMethod::factory()->create([
            'user_id'     => $advertiser->id,
            'external_id' => ExternalIdHelper::createPaymentMethodExternalId(),
        ]);
    }
}
