<?php

namespace Modules\Demo\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Modules\Campaign\Actions\Traits\ValidateStatusEvaluation;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\Traits\CreateCreative;
use Modules\Demo\Commands\Traits\ApplyStatus;
use Modules\User\Models\User;

class CampaignHelper
{
    use CreateCampaign,
        CreateCreative,
        ValidateStatusEvaluation,
        ApplyStatus;

    /**
     * @var PaymentHelper
     */
    private $paymentHelper;

    /**
     * @var TargetingHelper
     */
    private $targetingHelper;

    /**
     * @var NotificationHelper
     */
    private $notificationHelper;

    /**
     * @param PaymentHelper      $paymentHelper
     * @param TargetingHelper    $targetingHelper
     * @param NotificationHelper $notificationHelper
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        TargetingHelper $targetingHelper,
        NotificationHelper $notificationHelper
    ) {
        $this->paymentHelper = $paymentHelper;
        $this->targetingHelper = $targetingHelper;
        $this->notificationHelper = $notificationHelper;
    }

    /**
     * Create campaign based on given index.
     * For example if we need to create 12 campaigns
     * Method would create given amount in $map campaigns in that order 12 times
     *
     * @param User $advertiser
     * @param int  $index
     *
     * @return Campaign
     * @throws \Exception
     */
    public function create(User $advertiser, int $index): Campaign
    {
        $campaign = $this->createCampaign($advertiser, $index);
        $this->targetingHelper->createPredefinedTargetings($campaign, $index);

        $creative = $this->createCreative($advertiser, $index);
        $campaign->creatives()->attach($creative->id);

        return $campaign;
    }

    /**
     * @param User $advertiser
     * @param int  $index
     *
     * @return Campaign
     */
    private function createCampaign(User $advertiser, int $index): Campaign
    {
        $map = [
            'createTestReadyCampaign', /** @see createTestReadyCampaign */
            'createTestLiveCampaign', /** @see createTestLiveCampaign */
            'createTestCompletedCampaign', /** @see createTestCompletedCampaign */
            'createTestPausedCampaign', /** @see createTestPausedCampaign */
            'createTestCanceledCampaign',/** @see createTestCanceledCampaign */
        ];

        return call_user_func([$this, Arr::get($map, $index % count($map))], [
            'user_id'           => $advertiser->id,
            'name'              => $this->getCampaignName($index),
            'step_id'           => CampaignStep::ID_REVIEW,
            'payment_method_id' => $this->paymentHelper->firstOrCreate($advertiser)->id,
        ]);
    }

    /**
     * @param int $index
     *
     * @return string
     */
    private function getCampaignName(int $index): string
    {
        $map = [
            'Male Prospecting (25-34 yrs)',
            'Coming soon - Parents & Coffee Drinkers (Hop Bottom, PA)',
            'Flash sale - targeted zip code 25356',
            'Female Prospecting - Computer Devices',
            'Young Professionals - Science Fiction (targeted zips: 10226 & 25261)',
        ];

        return Arr::get($map, $index % count($map));
    }

    /**
     * @param User $advertiser
     * @param int  $index
     *
     * @return Creative
     */
    private function createCreative(User $advertiser, int $index): Creative
    {
        $records = config('demo.storage.creatives.records', []);
        $record = Arr::get($records, $index % count($records));

        Arr::set($record, 'user_id', $advertiser->id);
        Arr::set($record, 'external_id', ExternalIdHelper::createCreativeExternalId());

        return $this->createTestCreative($record);
    }

    /**
     * Set Campaign status based on previous status
     * If Demo Advertiser would like to PAUSE and then RESUME campaign
     * Application needs to know to which status campaign should be changed (LIVE or READY)
     * To know this application should store previous status somewhere (cache in our case)
     *
     * @param Campaign $campaign
     * @param User     $user
     *
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Campaign\Exceptions\SameStatusException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function resume(Campaign $campaign, User $user): void
    {
        $status = $this->getStoredStatus($campaign);
        $this->canApply($campaign, $user, $status);
        $campaign->applyInternalStatus($status);
    }

    /**
     * @param Campaign $campaign
     *
     * @return string
     */
    private function getCampaignStatusCacheKey(Campaign $campaign): string
    {
        return sprintf('campaign-status-before-pause-%s', $campaign->external_id);
    }

    /**
     * Get previous status for campaign
     * Used for determine if campaign should go READY or LIVE
     *
     * @param Campaign $campaign
     *
     * @return string
     */
    public function getStoredStatus(Campaign $campaign): string
    {
        return Cache::get($this->getCampaignStatusCacheKey($campaign), CampaignStatus::READY);
    }

    /**
     * @param Campaign $campaign
     */
    public function storeStatus(Campaign $campaign): void
    {
        Cache::add(
            $this->getCampaignStatusCacheKey($campaign),
            $campaign->status->name,
            config('demo.campaigns.ttl')
        );
    }
}
