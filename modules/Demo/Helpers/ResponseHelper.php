<?php

namespace Modules\Demo\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Route;
use Modules\Demo\Auth\Http\Services\ValidateCognitoJwtAction;
use Modules\Demo\Exceptions\RequestNotSupportedException;
use Modules\Demo\Helpers\Response\Factory\Haapi\HaapiResponseTrait;
use Modules\Demo\Helpers\Response\Factory\ResponseFactory;
use Psr\Http\Message\ResponseInterface;

class ResponseHelper
{
    use HaapiResponseTrait;

    /**
     * Routes that need to ignore and send without mocking.
     *
     * @var string[]
     */
    private $ignoreRouteNames = [
        'admin-sign-in',
    ];

    /**
     * HAAPI request types that should be ignored to login admin through HAAPI
     *
     * @var string[]
     */
    private $ignoreAdminRequests = [
        'client/authorize',
        'user/get',
        'user/signout',
    ];

    /**
     * Targeting values requests to update targetings stored in DB
     *
     * @var string[]
     */
    private $targetingRequestTypes = [
        'target/type/list',
        'target/value/list',
    ];

    /**
     * @param Request $request
     *
     * @return ResponseInterface
     * @throws RequestNotSupportedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createResponse(Request $request): ResponseInterface
    {
        if ($this->requestShouldNotBeMocked($request)) {
            return $this->sendOriginalRequest($request);
        }

        $this->log()->debug('Request would be mocked.', [
            'uri'      => $this->getRequestUri($request),
            'contents' => $this->getRequestBody($request),
        ]);

        return (new ResponseFactory())->createResponse($request);
    }

    /**
     * @param Request $request
     *
     * @return ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function sendOriginalRequest(Request $request): ResponseInterface
    {
        return (new Client())->send($request);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function requestShouldNotBeMocked(Request $request): bool
    {
        return $this->isIgnoredRoute($request)
            || $this->isAdminCognito($request)
            || $this->isRecaptcha($request)
            || $this->isTargetingUpdateRequest($request)
            || $this->isRequiredAdminRequest($request);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isRequiredAdminRequest(Request $request): bool
    {
        $isAdmin = $this->isAdmin();
        if (!$isAdmin) {
            return false;
        }

        $body = $this->getRequestBody($request);
        if (!$body) {
            return false;
        }

        return $this->isIgnoredType($body);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isTargetingUpdateRequest(Request $request): bool
    {
        $body = $this->getRequestBody($request);
        if (!$body) {
            return false;
        }

        $type = $this->getHaapiType($body);

        return in_array($type, $this->targetingRequestTypes);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isRecaptcha(Request $request): bool
    {
        return $this->getRequestUri($request) === config('recaptcha.verification_url');
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isAdminCognito(Request $request): bool
    {
        return $this->getRequestUri($request) === ValidateCognitoJwtAction::formJwksUri();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isIgnoredRoute(Request $request): bool
    {
        if (!in_array(Route::currentRouteName(), $this->ignoreRouteNames, true)) {
            return false;
        }

        $body = $this->getRequestBody($request);
        if (!$body) {
            return false;
        }

        return $this->isIgnoredType($body);
    }

    /**
     * @param array $body
     *
     * @return bool
     */
    private function isIgnoredType(array $body): bool
    {
        return in_array($this->getHaapiType($body), $this->ignoreAdminRequests, true);
    }

    /**
     * @return string
     */
    public static function getResponsesPath(): string
    {
        return base_path('modules/Demo/resources/fixtures/responses');
    }
}
