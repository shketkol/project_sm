<?php

namespace Modules\Demo\Helpers;

use Illuminate\Support\Arr;
use Modules\Demo\Exceptions\InvalidTokenException;
use Modules\User\Repositories\UserRepository;

class AuthHelper
{
    /**
     * @return bool
     */
    public static function isLoginAllowed(): bool
    {
        /** @var UserRepository $repository */
        $repository = app(UserRepository::class);
        $demoIdentifier = config('demo.uuid.advertiser.starts_with');

        $demoUsersCount = $repository->findWhere([
            ['external_id', 'like', $demoIdentifier . '%'],
        ])->count();

        return $demoUsersCount < config('demo.users.limit');
    }

    /**
     * Email and password stored in jwt-like format:
     * base64(email).base64(password)
     * For example "dGVzdEBnbWFpbC5jb20=.bXlTZWNyZXRQYXNzd29yZA==" equals "test@gmail.com" and "mySecretPassword"
     *
     * @param string $token
     *
     * @return array
     * @throws InvalidTokenException
     */
    public static function getCredentialsFromToken(string $token): array
    {
        [$userBase64, $passwordBase64] = explode('.', $token);
        $userEmail = base64_decode($userBase64, true);
        $userPassword = base64_decode($passwordBase64, true);

        if (!$userEmail || !$userPassword) {
            throw InvalidTokenException::create();
        }

        return ['email' => $userEmail, 'password' => $userPassword];
    }

    /**
     * @param string $token
     *
     * @return string
     * @throws InvalidTokenException
     */
    public static function getEmailFromToken(string $token): string
    {
        return self::getCredentialsFromToken($token)['email'];
    }

    /**
     * @param string $token
     *
     * @return string
     * @throws InvalidTokenException
     */
    public static function getNameFromToken(string $token): string
    {
        $email = self::getCredentialsFromToken($token)['email'];

        if (self::isAdminEmail($email)) {
            return self::getAdminNameByEmail($email);
        }

        return self::getAdvertiserNameByEmail($email);
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    private static function isAdminEmail(string $email): bool
    {
        return $email === config('daapi.admin.email');
    }

    /**
     * @param string $email
     *
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    private static function getAdminNameByEmail(string $email): string
    {
        return 'Technical Admin';
    }

    /**
     * @param string $email
     *
     * @return string
     */
    private static function getAdvertiserNameByEmail(string $email): string
    {
        $credentials = Arr::first(Arr::where(
            config('demo.users.credentials'),
            function ($value) use ($email) {
                return $email === $value['email'];
            }
        ));

        return $credentials['name'];
    }

    /**
     * Create Demo Admin Credentials Token using email
     * Use zeroes instead of valid password
     *
     * @param string $email
     *
     * @return string
     */
    public static function createAdminCredentialsToken(string $email): string
    {
        return base64_encode($email) . '.' . base64_encode('000000000000');
    }
}
