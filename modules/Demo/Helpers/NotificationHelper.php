<?php

namespace Modules\Demo\Helpers;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Notifications\Advertiser\Completed;
use Modules\Campaign\Notifications\Advertiser\Started;
use Modules\Creative\Models\Creative;
use Modules\Creative\Notifications\Advertiser\Approved;
use Modules\Payment\Notifications\Advertiser\TransactionIsMade;

class NotificationHelper
{
    /**
     * @var Dispatcher
     */
    private $notification;

    /**
     * @param Dispatcher $notification
     */
    public function __construct(Dispatcher $notification)
    {
        $this->notification = $notification;
    }

    /**
     * @param Creative $creative
     *
     * @return void
     */
    public function createApprovedCreativeNotification(Creative $creative): void
    {
        $this->notification->send($creative->user, new Approved($creative));
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     */
    public function createCompletedCampaignNotification(Campaign $campaign): void
    {
        $this->notification->send($campaign->user, new Completed($campaign));
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     */
    public function createStartedCampaignNotification(Campaign $campaign): void
    {
        $this->notification->send($campaign->user, new Started($campaign));
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     */
    public function createTransactionIsMadeNotification(Campaign $campaign): void
    {
        $this->notification->send(
            $campaign->user,
            new TransactionIsMade(
                $campaign,
                $this->getInvoiceData()
            )
        );
    }

    /**
     * @return array
     */
    private function getInvoiceData(): array
    {
        return [
            'amount'            => 1000,
            'paymentMethodName' => trans('payment::labels.ending_in', [
                'brand'     => trans("payment::labels.visa"),
                'ending_id' => 1111,
            ]),
        ];
    }
}
