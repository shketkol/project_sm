<?php

namespace Modules\Demo\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\Demo\Exceptions\CredentialsNotFoundException;
use Modules\User\Models\AccountType;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;

class UserHelper
{
    use CreateUser;

    /**
     * @var CampaignHelper
     */
    private $campaignHelper;

    /**
     * @var ReportHelper
     */
    private $reportHelper;

    /**
     * @var NotificationHelper
     */
    private $notificationHelper;

    /**
     * @param CampaignHelper     $campaignHelper
     * @param ReportHelper       $reportHelper
     * @param NotificationHelper $notificationHelper
     */
    public function __construct(
        CampaignHelper $campaignHelper,
        ReportHelper $reportHelper,
        NotificationHelper $notificationHelper
    ) {
        $this->campaignHelper = $campaignHelper;
        $this->reportHelper = $reportHelper;
        $this->notificationHelper = $notificationHelper;
    }

    /**
     * @param User $user
     *
     * @return bool
     * @throws CredentialsNotFoundException
     */
    public function shouldBeSeeded(User $user): bool
    {
        return Arr::get($this->findUserCredentials($user->email), 'need_for_seed', false);
    }

    /**
     * @param User $advertiser
     *
     * @throws \Exception
     */
    public function seedAdvertiser(User $advertiser): void
    {
        for ($i = 0; $i < config('demo.campaigns.number_to_create'); $i++) {
            $campaign = $this->campaignHelper->create($advertiser, $i);
            $this->notificationHelper->createApprovedCreativeNotification($campaign->getActiveCreative());
        }

        for ($i = 0; $i < config('demo.reports.number_to_create'); $i++) {
            $this->reportHelper->create($advertiser, $i);
        }
    }

    /**
     * Seed admin with test advertisers
     *
     * @param User $admin
     *
     * @throws \Exception
     */
    public function seedAdmin(User $admin): void
    {
        for ($i = 0; $i < config('demo.advertisers.number_to_create'); $i++) {
            $advertiser = $this->createTestAdvertiser([
                'company_name'        => sprintf('Demo Company #%d', $i + 1),
                'account_type_id'     => AccountType::ID_COMMON,
                'external_id'         => ExternalIdHelper::createAdvertiserForAdminExternalId($admin),
                'account_external_id' => ExternalIdHelper::createAdvertiserForAdminExternalId($admin),
            ]);

            $this->seedAdvertiser($advertiser);
        }
    }

    /**
     * @param string $email
     *
     * @return array
     * @throws CredentialsNotFoundException
     */
    public function findUserCredentials(string $email): array
    {
        foreach (config('demo.users.credentials') as $data) {
            if (Arr::get($data, 'email') === $email) {
                return $data;
            }
        }

        throw CredentialsNotFoundException::create(sprintf('Credentials not found for email "%s".', $email));
    }

    /**
     * @param User $advertiser
     *
     * @return bool
     */
    public function isDemoAdvertiser(User $advertiser): bool
    {
        return Str::startsWith($advertiser->getExternalId(), [
            config('demo.uuid.advertiser.starts_with'),
            config('demo.uuid.admin.starts_with'),
        ]);
    }
}
