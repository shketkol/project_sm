<?php

namespace Modules\Demo\Helpers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\DeviceGroup;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Zipcode;

class TargetingHelper
{
    /**
     * Create campaign targeting based on given index.
     *
     * @param Campaign $campaign
     * @param int      $index
     */
    public function createPredefinedTargetings(Campaign $campaign, int $index): void
    {
        $map = [
            function (Campaign $campaign): void {
                $this->fillFirstCampaign($campaign);
            },
            function (Campaign $campaign): void {
                $this->fillSecondCampaign($campaign);
            },
            function (Campaign $campaign): void {
                $this->fillThirdCampaign($campaign);
            },
            function (Campaign $campaign): void {
                $this->fillForthCampaign($campaign);
            },
            function (Campaign $campaign): void {
                $this->fillFifthCampaign($campaign);
            },
        ];

        $callable = Arr::get($map, $index % count($map));
        $callable($campaign);
    }

    /**
     * Male 25-34
     *
     * @param Campaign $campaign
     */
    private function fillFirstCampaign(Campaign $campaign): void
    {
        // Male
        $genderId = Gender::where('name', 'males')->first()->id;
        $this->addGender($campaign, $genderId);

        // 25-34
        $ageGroupId = AgeGroup::where('name', '25-34')
            ->where('gender_id', $genderId)
            ->first()
            ->id;
        $this->addAgeGroups($campaign, $ageGroupId);

        $this->addAllDevices($campaign);
    }

    /**
     * Parents, Coffee Drinkers, Hop Bottom, PA
     *
     * @param Campaign $campaign
     *
     * @throws \Exception
     */
    private function fillSecondCampaign(Campaign $campaign): void
    {
        // Parents, Coffee Drinkers
        $audiencesIds = Audience::where(function (Builder $query): void {
            $query->where('name', 'like', '%Parents%')
                ->orWhere('name', 'Coffee Drinkers');
        })
            ->where('visible', true)
            ->get()
            ->pluck('id')
            ->toArray();

        $this->addAudiences($campaign, ...$audiencesIds);

        // Hop Bottom, PA
        $locationId = Location::where('name', 'Hop Bottom, PA')
            ->first()
            ->id;
        $this->addLocations($campaign, $locationId);

        $this->addAllDevices($campaign);
        $this->addAllGenders($campaign);
    }

    /**
     * zip 25356
     *
     * @param Campaign $campaign
     */
    private function fillThirdCampaign(Campaign $campaign): void
    {
        $id = Zipcode::where('name', '25356')->first()->id;
        $this->addZipcodes($campaign, $id);

        $this->addAllDevices($campaign);
        $this->addAllGenders($campaign);
    }

    /**
     * Female, Computer Devices
     *
     * @param Campaign $campaign
     */
    private function fillForthCampaign(Campaign $campaign): void
    {
        // Female
        $genderId = Gender::where('name', 'females')->first()->id;
        $this->addGender($campaign, $genderId);

        // Computer Devices
        $deviceId = DeviceGroup::where('name', 'computer')->first()->id;
        $this->addDeviceGroups($campaign, $deviceId);

        // Age Groups (must be added)
        $ageGroupIds = AgeGroup::where('gender_id', $genderId)
            ->get()
            ->pluck('id')
            ->toArray();
        $this->addAgeGroups($campaign, ...$ageGroupIds);
    }

    /**
     * Science Fiction, zip 10226, zip 25261
     *
     * @param Campaign $campaign
     */
    private function fillFifthCampaign(Campaign $campaign): void
    {
        // Science Fiction
        $genreId = Genre::where('name', 'Science Fiction')->first()->id;
        $audienceId = Audience::where('name', 'Young Professionals')->first()->id;

        $this->addGenres($campaign, $genreId);
        $this->addAudiences($campaign, $audienceId);

        // zip 10226, zip 25261
        $zipcodesIds = Zipcode::where('name', '10226')
            ->orWhere('name', '25261')
            ->get()
            ->pluck('id')
            ->toArray();
        $this->addZipcodes($campaign, ...$zipcodesIds);
        $this->addAllDevices($campaign);

        // Male
        $genderId = Gender::where('name', 'all')->first()->id;
        $this->addGender($campaign, $genderId);

        // Age Groups (must be added)
        $ageGroupIds = AgeGroup::where('gender_id', $genderId)
            ->whereIn('name', ['18-24', '25-34'])
            ->get()
            ->pluck('id')
            ->toArray();

        $this->addAgeGroups($campaign, ...$ageGroupIds);
    }

    /**
     * @param Campaign $campaign
     */
    private function addAllDevices(Campaign $campaign): void
    {
        $deviceIds = DeviceGroup::all()->pluck('id')->toArray();
        $this->addDeviceGroups($campaign, ...$deviceIds);
    }

    /**
     * @param Campaign $campaign
     */
    private function addAllGenders(Campaign $campaign): void
    {
        $genderId = Gender::where('name', 'all')->first()->id;
        $this->addGender($campaign, $genderId);

        $ageGroupIds = AgeGroup::where('gender_id', $genderId)
            ->get()
            ->pluck('id')
            ->toArray();
        $this->addAgeGroups($campaign, ...$ageGroupIds);
    }

    /**
     * @param Campaign $campaign
     * @param int      ...$ids
     */
    private function addDeviceGroups(Campaign $campaign, int ...$ids): void
    {
        $campaign->deviceGroups()->sync($ids);
    }

    /**
     * @param Campaign $campaign
     * @param int      ...$ids
     */
    private function addGenres(Campaign $campaign, int ...$ids): void
    {
        $data = [];
        for ($i = 0; $i < count($ids); $i++) {
            $data[] = [
                'genre_id' => $ids[$i],
                'excluded' => false,
            ];
        }

        $campaign->genres()->sync($data);
    }

    /**
     * @param Campaign $campaign
     * @param int      ...$ids
     */
    private function addZipcodes(Campaign $campaign, int ...$ids): void
    {
        $data = [];
        for ($i = 0; $i < count($ids); $i++) {
            $data[] = [
                'zipcode_id' => $ids[$i],
                'excluded'   => false,
            ];
        }

        $campaign->zipcodes()->sync($data);
    }

    /**
     * @param Campaign $campaign
     * @param int      ...$ids
     *
     * @throws \Exception
     */
    private function addLocations(Campaign $campaign, int ...$ids): void
    {
        $data = [];
        for ($i = 0; $i < count($ids); $i++) {
            $data[] = [
                'location_id' => $ids[$i],
                'excluded'    => false,
            ];
        }

        $campaign->locations()->sync($data);
    }

    /**
     * @param Campaign $campaign
     * @param int      ...$ids
     *
     * @throws \Exception
     */
    private function addAudiences(Campaign $campaign, int ...$ids): void
    {
        $campaign->audiences()->sync($ids);
    }

    /**
     * @param Campaign $campaign
     * @param int      ...$ids
     */
    private function addAgeGroups(Campaign $campaign, int ...$ids): void
    {
        $campaign->targetingAgeGroups()->sync($ids);
    }

    /**
     * @param Campaign $campaign
     * @param int      $id
     */
    private function addGender(Campaign $campaign, int $id): void
    {
        $campaign->targetingGenders()->sync([$id]);
    }
}
