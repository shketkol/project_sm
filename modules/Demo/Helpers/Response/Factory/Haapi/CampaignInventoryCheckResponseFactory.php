<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Modules\Demo\Exceptions\ResponseNotFoundException;
use Modules\Haapi\Actions\Campaign\CampaignInventoryCheck;
use Psr\Http\Message\ResponseInterface;

class CampaignInventoryCheckResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);

        if ($this->checkTargets(Arr::get($body, 'haapi.request.payload.targets'))) {
            $body = $this->setResponse($body);
        }

        if ($this->isUnderMinPerDay($body)) {
            $body = $this->setUnderMinPerDayResponse($body);
        }

        return $this->responseOk($body);
    }

    /**
     * @param array $body
     *
     * @return array
     */
    private function setUnderMinPerDayResponse(array $body): array
    {
        Arr::set(
            $body,
            'haapi.response.payload.minDailyImpressions',
            config('demo.targetings.inventory_check.min_daily_impressions')
        );

        Arr::set(
            $body,
            'haapi.response.payload.statusCode',
            CampaignInventoryCheck::HAAPI_TYPE_DANGER_SCHEDULE
        );

        return $body;
    }

    /**
     * @param array $body
     *
     * @return bool
     */
    private function isUnderMinPerDay(array $body): bool
    {
        $startDate = Carbon::createFromFormat(
            config('date.format.haapi'),
            Arr::get($body, 'haapi.request.payload.startDate')
        );
        $endDate = Carbon::createFromFormat(
            config('date.format.haapi'),
            Arr::get($body, 'haapi.request.payload.endDate')
        );

        $days = $endDate->diffInDays($startDate);

        // If campaign starts 25.08.2020 and ends 26.08.2020
        // It would be 1 day diff in Carbon
        // But Campaign would work both days so we need to +1 day to make it true result
        $days += 1;

        $impressions = Arr::get($body, 'haapi.request.payload.quantity');
        $impressionsPerDay = $impressions / $days;

        $minDailyImpressions = config('demo.targetings.inventory_check.min_daily_impressions');

        return $minDailyImpressions > $impressionsPerDay;
    }

    /**
     * @param array $body
     *
     * @return array
     */
    private function setResponse(array $body): array
    {
        $data = [
            'quantityType'         => 'IMPRESSIONS',
            'quantity'             => 284090,
            'costModel'            => 'CPM',
            'unitCost'             => 35.2,
            'availableBudget'      => 0.528,
            'availableImpressions' => 15,
            'systemFloorBudget'    => 500,
            'statusCode'           => CampaignInventoryCheck::HAAPI_TYPE_DANGER_BUDGET,
        ];
        Arr::set($body, 'haapi.response.payload', $data);

        return $body;
    }

    /**
     * @param array $targets
     *
     * @return bool
     */
    private function checkTargets(array $targets): bool
    {
        foreach ($targets as $target) {
            if (Arr::get($target, 'typeId') === config('demo.targetings.inventory_check.type_id_for_fail')) {
                $filtered = Arr::where(Arr::get($target, 'values'), function ($value) {
                    return Arr::get($value, 'value') === config('demo.targetings.inventory_check.value_for_fail');
                });

                if (!empty($filtered)) {
                    return true;
                }
            }
        }

        return false;
    }
}
