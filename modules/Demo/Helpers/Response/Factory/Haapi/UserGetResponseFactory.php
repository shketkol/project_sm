<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Modules\Demo\Helpers\AuthHelper;
use Psr\Http\Message\ResponseInterface;

class UserGetResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws \Modules\Demo\Exceptions\InvalidTokenException
     * @throws \Modules\Demo\Exceptions\ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);
        $body = $this->setExternalId($body);

        $token = $this->getToken($data);
        $email = AuthHelper::getEmailFromToken($token);
        $name = AuthHelper::getNameFromToken($token);

        $body = $this->setEmail($body, $email);
        $body = $this->setFirstName($body, $name);

        return $this->responseOk($body);
    }
}
