<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;

class CampaignDraftSaveResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws \Modules\Demo\Exceptions\ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);
        $body = $this->setOriginalExternalId($body);

        return $this->responseOk($body);
    }

    /**
     * @param array $body
     *
     * @return array
     */
    private function setOriginalExternalId(array $body): array
    {
        $originalExternalId = Arr::get($body, 'haapi.request.payload.campaign.externalId');

        if (!is_null($originalExternalId)) {
            Arr::set($body, 'haapi.response.payload.campaign.id', $originalExternalId);
        }

        return $body;
    }
}
