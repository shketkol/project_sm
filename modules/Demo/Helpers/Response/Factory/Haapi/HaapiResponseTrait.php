<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use App\Helpers\Json;
use App\Services\ValidationRulesService\ValidationRules;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Demo\Exceptions\InvalidTokenException;
use Modules\Demo\Exceptions\ResponseNotFoundException;
use Modules\Demo\Helpers\AuthHelper;
use Modules\Demo\Helpers\ExternalIdHelper;
use Modules\Demo\Helpers\ResponseHelper;
use Modules\Demo\Helpers\UserHelper;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

trait HaapiResponseTrait
{
    /**
     * @param string $path
     * @param array  $content
     *
     * @return array
     */
    private function setOriginalPayload(string $path, array $content): array
    {
        $body = file_get_contents($path);
        $data = Json::decode($body);

        $payload = Arr::get($content, 'haapi.request.payload');
        Arr::set($data, 'haapi.request.payload', $payload);

        return $data;
    }

    /**
     * @param array $data
     *
     * @return string
     * @throws ResponseNotFoundException
     */
    private function getResponseFilePath(array $data): string
    {
        $type = $this->getHaapiType($data);
        $path = $this->getHaapiPath($type);

        if (!file_exists($path)) {
            throw ResponseNotFoundException::create(sprintf('Response file not found at path: "%s".', $path));
        }

        return $path;
    }

    /**
     * @param string $type
     *
     * @return string
     */
    private function getHaapiPath(string $type): string
    {
        return sprintf(
            '%s/haapi/%s.json',
            ResponseHelper::getResponsesPath(),
            $type
        );
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function getHaapiType(array $data): string
    {
        return Arr::get($data, 'haapi.request.type');
    }

    /**
     * @param array $content
     *
     * @return array
     */
    private function setExternalId(array $content): array
    {
        $externalId = ExternalIdHelper::createAdvertiserExternalId();

        Arr::set($content, 'haapi.response.payload.id', $externalId);
        Arr::set($content, 'haapi.response.payload.account.id', $externalId);

        return $content;
    }

    /**
     * @param array $content
     *
     * @return array
     */
    private function setExternalIdInCampaignResponse(array $content): array
    {
        $externalId = ExternalIdHelper::createAdvertiserExternalId();
        Arr::set($content, 'haapi.response.payload.campaign.advertiserId', $externalId);

        return $content;
    }

    /**
     * @param array  $content
     * @param string $email
     *
     * @return array
     */
    private function setEmail(array $content, string $email): array
    {
        Arr::set($content, 'haapi.response.payload.email', $email);

        return $content;
    }

    /**
     * @param array  $content
     * @param string $name
     *
     * @return array
     */
    private function setFirstName(array $content, string $name): array
    {
        Arr::set($content, 'haapi.response.payload.firstName', $name);

        return $content;
    }

    /**
     * @param array $body
     * @param array $headers
     *
     * @return GuzzleResponse
     */
    private function responseOk(array $body, array $headers = []): GuzzleResponse
    {
        return new GuzzleResponse(Response::HTTP_OK, $headers, Json::encode($body));
    }

    /**
     * @param array $content
     *
     * @return array
     *
     * Set this exact token as here
     * @see \Modules\Demo\Helpers\AuthHelper::getCredentialsFromToken()
     * To be able to identify user on next requests (user/get)
     */
    private function setCredentialsToken(array $content): array
    {
        // advertiser
        $token = Arr::get($content, 'haapi.request.payload.accessToken');

        // tech admin
        if (is_null($token)) {
            $token = AuthHelper::createAdminCredentialsToken(Arr::get($content, 'haapi.request.payload.username'));
        }

        Arr::set($content, 'haapi.response.payload.sessionToken', $token);

        return $content;
    }

    /**
     * @param array $data
     *
     * @return array
     * @throws \Modules\Demo\Exceptions\CredentialsNotFoundException
     * @throws \Modules\Demo\Exceptions\InvalidTokenException
     */
    private function findCredentials(array $data): array
    {
        $token = $this->parseToken($data);

        if ($this->isAdvertiserCredentials($token)) {
            return $this->findAdvertiserCredentials($token);
        }

        return [];
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    private function isAdminCredentials(string $token): bool
    {
        /** @var ValidationRules $rules */
        $rules = app(ValidationRules::class);
        $validator = Validator::make(['uuid' => $token], $rules->only('uuid', ['regex']));

        return !$validator->fails();
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    private function isAdvertiserCredentials(string $token): bool
    {
        return substr_count($token, '.') === 1;
    }

    /**
     * @param string $token
     *
     * @return array
     * @throws \Modules\Demo\Exceptions\CredentialsNotFoundException
     * @throws \Modules\Demo\Exceptions\InvalidTokenException
     */
    private function findAdvertiserCredentials(string $token): array
    {
        $email = AuthHelper::getEmailFromToken($token);

        /** @var UserHelper $helper */
        $helper = app(UserHelper::class);

        return $helper->findUserCredentials($email);
    }

    /**
     * @param array $data
     *
     * @return bool
     * @throws \Modules\Demo\Exceptions\CredentialsNotFoundException
     * @throws \Modules\Demo\Exceptions\InvalidTokenException
     */
    private function isSeededAdvertiser(array $data): bool
    {
        return Arr::get($this->findCredentials($data), 'need_for_seed', false);
    }

    /**
     * @param array $data
     *
     * @return bool
     * @throws \Modules\Demo\Exceptions\CredentialsNotFoundException
     * @throws \Modules\Demo\Exceptions\InvalidTokenException
     */
    private function isCleanAdvertiser(array $data): bool
    {
        return !$this->isSeededAdvertiser($data);
    }

    /**
     * @param array $data
     *
     * @return string
     * @throws InvalidTokenException
     */
    private function getToken(array $data): string
    {
        $token = $this->parseToken($data);
        if (is_null($token)) {
            $this->log()->warning('Token not found in request.', ['request' => $data]);
            throw InvalidTokenException::create();
        }

        return $token;
    }

    private function parseToken(array $data): string
    {
        return Arr::last(explode(' ', Arr::get($data, 'headers.Authorization.0')));
    }

    /**
     * @return LoggerInterface
     */
    private function log(): LoggerInterface
    {
        return app(LoggerInterface::class);
    }

    /**
     * @return bool
     */
    private function isAdmin(): bool
    {
        /** @var User|null $user */
        $user = Auth::guard('admin')->user();
        if (is_null($user)) {
            return false;
        }

        return $user->isAdmin();
    }

    /**
     * @param Request $request
     *
     * @return array|false
     */
    private function getRequestBody(Request $request)
    {
        $contents = $request->getBody()->__toString();
        if (!Json::isJson($contents)) {
            return false;
        }

        return Json::decode($contents);
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    private function getRequestUri(Request $request): string
    {
        return $request->getUri()->__toString();
    }
}
