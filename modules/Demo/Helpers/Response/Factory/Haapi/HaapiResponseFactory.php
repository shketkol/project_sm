<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;

class HaapiResponseFactory
{
    /**
     * @var string[]
     */
    private $responseMap = [
        'campaign/inventorycheck'      => CampaignInventoryCheckResponseFactory::class,
        'campaign/impressions/metrics' => CampaignImpressionsMetricsResponseFactory::class,
        'campaign/impressions/detail'  => CampaignImpressionsDetailResponseFactory::class,
        'campaign/draft_save'          => CampaignDraftSaveResponseFactory::class,
        'campaign/get'                 => CampaignGetResponseFactory::class,
        'campaign/price'               => CampaignPriceResponseFactory::class,
        'campaign/pause'               => CampaignPauseResponseFactory::class,
        'orders/get'                   => OrdersGetResponseFactory::class,
        'user/get'                     => UserGetResponseFactory::class,
        'client/authorize'             => ClientAuthorizeResponseFactory::class,
        'report/accountImpressions'    => ReportAccountImpressionsResponseFactory::class,
        'default'                      => DefaultResponseFactory::class,
    ];

    /**
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function createFromContent(array $data): ResponseInterface
    {
        return $this->getResponse($data);
    }

    /**
     * @param array $data
     *
     * @return ResponseInterface
     */
    private function getResponse(array $data): ResponseInterface
    {
        $type = $this->getHaapiType($data);
        $builder = $this->chooseResponseBuilder($type);
        return $builder->createFromContent($data);
    }

    /**
     * @param string $type
     *
     * @return ResponseFactoryInterface
     */
    private function chooseResponseBuilder(string $type): ResponseFactoryInterface
    {
        $class = Arr::get($this->responseMap, $type, $this->responseMap['default']);
        return app($class);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function getHaapiType(array $data): string
    {
        return Arr::get($data, 'haapi.request.type');
    }
}
