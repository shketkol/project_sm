<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Illuminate\Support\Arr;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Demo\Helpers\CampaignHelper;
use Psr\Http\Message\ResponseInterface;

class CampaignPauseResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @var CampaignHelper
     */
    private $campaignHelper;

    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @param CampaignRepository $repository
     * @param CampaignHelper     $campaignHelper
     */
    public function __construct(CampaignRepository $repository, CampaignHelper $campaignHelper)
    {
        $this->campaignHelper = $campaignHelper;
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws \Modules\Campaign\Exceptions\CampaignNotFoundException
     * @throws \Modules\Demo\Exceptions\ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);

        $externalId = Arr::get($body, 'haapi.request.payload.campaignId');
        $campaign = $this->repository->findCampaignByExternalId($externalId);
        $this->campaignHelper->storeStatus($campaign);

        return $this->responseOk($body);
    }
}
