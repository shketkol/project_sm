<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Modules\Demo\Exceptions\ResponseNotFoundException;
use Psr\Http\Message\ResponseInterface;

class DefaultResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);

        return $this->responseOk($body);
    }
}
