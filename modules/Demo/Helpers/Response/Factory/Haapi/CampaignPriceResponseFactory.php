<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Illuminate\Support\Arr;
use Modules\Targeting\Models\Type;
use Modules\Targeting\Repositories\DeviceRepository;
use Psr\Http\Message\ResponseInterface;

class CampaignPriceResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @var DeviceRepository
     */
    private $deviceRepository;

    /**
     * @param DeviceRepository $deviceRepository
     */
    public function __construct(DeviceRepository $deviceRepository)
    {
        $this->deviceRepository = $deviceRepository;
    }

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws \Modules\Demo\Exceptions\ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);

        $targetingsValues = Arr::get($body, 'haapi.request.payload.targets');
        $cpm = Arr::get($body, 'haapi.response.payload.unitCost');

        $modifiers = [];
        $modifiers['hasLocations'] = $this->hasLocations($targetingsValues);
        $modifiers['hasAudiences'] = $this->hasAudiences($targetingsValues);
        $modifiers['hasDevices'] = $this->hasDevices($targetingsValues);

        $cpm = $this->calculateCpm($cpm, $modifiers);

        Arr::set($body, 'haapi.response.payload.unitCost', $cpm);

        return $this->responseOk($body);
    }

    /**
     * @param float $cpm
     * @param array $modifiers
     *
     * @return float
     */
    private function calculateCpm(float $cpm, array $modifiers): float
    {
        if (Arr::get($modifiers, 'hasLocations')) {
            $cpm = $cpm + $cpm * config('demo.targetings.cpm.extra_rate.locations');
        }

        if (Arr::get($modifiers, 'hasDevices')) {
            $cpm = $cpm + $cpm * config('demo.targetings.cpm.extra_rate.devices');
        }

        if (Arr::get($modifiers, 'hasAudiences')) {
            $cpm += config('demo.targetings.cpm.extra_rate.audiences');
        }

        return $cpm;
    }

    /**
     * @param array $targetingsValues
     *
     * @return bool
     */
    private function hasLocations(array $targetingsValues): bool
    {
        $locationTypes = [
            Type::HAAPI_ZIPCODE_NAME,
            Type::HAAPI_DMA_NAME,
            Type::HAAPI_CITY_NAME,
            Type::HAAPI_STATE_NAME,
        ];

        $locationTypesExternalIds = Arr::pluck(
            Type::whereIn('name', $locationTypes)->get('external_id')->toArray(),
            'external_id'
        );

        foreach ($targetingsValues as $type) {
            if (in_array($type['typeId'], $locationTypesExternalIds)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $targetingsValues
     *
     * @return bool
     */
    private function hasAudiences(array $targetingsValues): bool
    {
        $audiencesTypes = [
            Type::HAAPI_AUDIENCE_NAME,
        ];

        $audiencesTypesExternalIds = Arr::pluck(
            Type::whereIn('name', $audiencesTypes)->get('external_id')->toArray(),
            'external_id'
        );

        foreach ($targetingsValues as $type) {
            if (in_array($type['typeId'], $audiencesTypesExternalIds)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $targetingsValues
     *
     * @return bool
     */
    private function hasDevices(array $targetingsValues): bool
    {
        $devicesType = [
            Type::HAAPI_DEVICE_NAME,
        ];

        $devicesTypesExternalIds = Arr::pluck(
            Type::whereIn('name', $devicesType)->get('external_id')->toArray(),
            'external_id'
        );

        $countAllDevices = $this->deviceRepository->count();

        foreach ($targetingsValues as $type) {
            if (in_array($type['typeId'], $devicesTypesExternalIds) && $countAllDevices !== count($type['values'])) {
                return true;
            }
        }

        return false;
    }
}
