<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Psr\Http\Message\ResponseInterface;

interface ResponseFactoryInterface
{
    /**
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function createFromContent(array $data): ResponseInterface;
}
