<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Psr\Http\Message\ResponseInterface;

class OrdersGetResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws \Modules\Demo\Exceptions\CredentialsNotFoundException
     * @throws \Modules\Demo\Exceptions\InvalidTokenException
     * @throws \Modules\Demo\Exceptions\ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);

        if (!Auth::user()->isAdmin() && $this->isCleanAdvertiser($data)) {
            Arr::set($body, 'haapi.response.payload.orders', []);
        }

        return $this->responseOk($body);
    }
}
