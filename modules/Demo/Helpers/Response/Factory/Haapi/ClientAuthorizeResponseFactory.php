<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Psr\Http\Message\ResponseInterface;

class ClientAuthorizeResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws \Modules\Demo\Exceptions\ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);
        $body = $this->setCredentialsToken($body);

        return $this->responseOk($body);
    }
}
