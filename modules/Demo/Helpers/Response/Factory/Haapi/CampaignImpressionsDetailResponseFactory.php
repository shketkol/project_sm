<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\Campaign\Actions\Traits\GetGenericStructure;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Demo\Helpers\UserHelper;
use Psr\Http\Message\ResponseInterface;

class CampaignImpressionsDetailResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait,
        GetGenericStructure;

    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @var UserHelper
     */
    private $userHelper;

    /**
     * @param CampaignRepository $repository
     * @param UserHelper         $userHelper
     */
    public function __construct(CampaignRepository $repository, UserHelper $userHelper)
    {
        $this->repository = $repository;
        $this->userHelper = $userHelper;
    }

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws \Modules\Campaign\Exceptions\CampaignNotFoundException
     * @throws \Modules\Demo\Exceptions\CredentialsNotFoundException
     * @throws \Modules\Demo\Exceptions\InvalidTokenException
     * @throws \Modules\Demo\Exceptions\ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);
        $body = $this->setRealData($body);

        // request from demo admin
        $token = $this->parseToken($data);

        if ($this->isAdminCredentials($token)) {
            $body = $this->handleAdminRequest($body);
            return $this->responseOk($body);
        }

        // request from demo advertiser
        if ($this->isCleanAdvertiser($body)) {
            $body = $this->handleCleanAdvertiserRequest($body);
        }

        return $this->responseOk($body);
    }

    /**
     * @param array $body
     *
     * @return array
     * @throws \Modules\Campaign\Exceptions\CampaignNotFoundException
     */
    private function setRealData(array $body): array
    {
        $campaign = $this->getCampaignByExternalId($body);
        Arr::set(
            $body,
            'haapi.response.payload.campaigns.0.impressions',
            ceil($campaign->impressions * 0.9)
        );
        Arr::set(
            $body,
            'haapi.response.payload.campaigns.0.cost',
            ceil($campaign->budget * 0.9)
        );

        return $body;
    }

    /**
     * @param array $body
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotFoundException
     */
    private function getCampaignByExternalId(array $body): Campaign
    {
        $externalId = Arr::get($body, 'haapi.request.payload.campaignId.0');
        return $this->repository->findCampaignByExternalId($externalId);
    }

    /**
     * @param array $body
     *
     * @return array
     */
    private function handleCleanAdvertiserRequest(array $body): array
    {
        Arr::set($body, 'haapi.response.payload.campaigns', [$this->getGenericStructure()]);
        return $body;
    }

    /**
     * @param array $body
     *
     * @return array
     * @throws \Modules\Campaign\Exceptions\CampaignNotFoundException
     * @throws \Modules\Demo\Exceptions\CredentialsNotFoundException
     */
    private function handleAdminRequest(array $body): array
    {
       $campaign = $this->getCampaignByExternalId($body);

        // admin viewing campaign for demo advertiser that seeded for admin (always seeded)
        if (Str::startsWith($campaign->user->getExternalId(), config('demo.uuid.admin.starts_with'))) {
            return $body;
        }

        // admin viewing campaign for demo advertiser that logged in by himself
        if (Str::startsWith($campaign->user->getExternalId(), config('demo.uuid.advertiser.starts_with'))) {
            if ($this->userHelper->shouldBeSeeded($campaign->user)) {
                return $body;
            }

            $body = $this->handleCleanAdvertiserRequest($body);
        }

        return $body;
    }
}
