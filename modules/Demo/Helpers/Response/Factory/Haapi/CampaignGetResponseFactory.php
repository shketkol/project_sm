<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Illuminate\Support\Arr;
use Modules\Campaign\Repositories\CampaignRepository;
use Psr\Http\Message\ResponseInterface;

class CampaignGetResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @param CampaignRepository $repository
     */
    public function __construct(CampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws \Modules\Campaign\Exceptions\CampaignNotFoundException
     * @throws \Modules\Demo\Exceptions\ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);
        $body = $this->setExternalIdInCampaignResponse($body);
        $body = $this->setTargets($body);
        $body = $this->setCreative($body);

        return $this->responseOk($body);
    }

    /**
     * @param array $body
     *
     * @return array
     * @throws \Modules\Campaign\Exceptions\CampaignNotFoundException
     */
    private function setCreative(array $body): array
    {
        $externalId = Arr::get($body, 'haapi.request.payload.campaignId');
        $campaign = $this->repository->findCampaignByExternalId($externalId);
        $creative = $campaign->getActiveCreative();

        if (is_null($creative)) {
            Arr::set($body, 'haapi.response.payload.campaign.lineItems.0.creative', []);
            return $body;
        }

        Arr::set($body, 'haapi.response.payload.campaign.lineItems.0.creative.id', $creative->external_id);
        Arr::set($body, 'haapi.response.payload.campaign.lineItems.0.creative.fileName', $creative->name);
        Arr::set($body, 'haapi.response.payload.campaign.lineItems.0.creative.creativeUrl', $creative->preview_url);
        Arr::set(
            $body,
            'haapi.response.payload.campaign.lineItems.0.creative.accountId',
            $campaign->user->getExternalId()
        );

        return $body;
    }

    /**
     * @param array $body
     *
     * @return array
     */
    private function setTargets(array $body): array
    {
        Arr::set(
            $body,
            'haapi.response.payload.targets',
            Arr::get($body, 'haapi.request.payload.targets')
        );

        return $body;
    }
}
