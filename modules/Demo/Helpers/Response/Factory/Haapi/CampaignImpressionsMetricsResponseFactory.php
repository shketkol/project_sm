<?php

namespace Modules\Demo\Helpers\Response\Factory\Haapi;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsMetricsParams;
use Psr\Http\Message\ResponseInterface;

class CampaignImpressionsMetricsResponseFactory implements ResponseFactoryInterface
{
    use HaapiResponseTrait;

    /**
     * @param array $data
     *
     * @return ResponseInterface
     * @throws \Modules\Demo\Exceptions\ResponseNotFoundException
     */
    public function createFromContent(array $data): ResponseInterface
    {
        $path = $this->getResponseFilePath($data);
        $body = $this->setOriginalPayload($path, $data);

        if (Arr::get($body, 'haapi.request.payload.type') === ImpressionsMetricsParams::TYPE_DEVICE) {
            $body = $this->setResponse($body);
        }

        return $this->responseOk($body);
    }

    /**
     * @param array $body
     *
     * @return array
     */
    private function setResponse(array $body): array
    {
        $data = [
            'totalImpressionsByType'    => [
                [
                    'name'        => 'Living Room',
                    'impressions' => 30584,
                    'percentage'  => 0.074922783000001,
                ],
                [
                    'name'        => 'Mobile',
                    'impressions' => 2070,
                    'percentage'  => 0.1002500368,
                ],
                [
                    'name'        => 'Computer',
                    'impressions' => 3,
                    'percentage'  => 0.824827180467001,
                ],
            ],
            'lineItemImpressionsByType' => [
                'lineItemId'  => 'demo0000-line-item-0000-000000000000',
                'status'      => 'LIVE',
                'placementId' => '0000000',
                'flightId'    => '1111111',
                'impressions' => [
                    [
                        'name'        => 'Living Room',
                        'impressions' => 30584,
                        'percentage'  => 0.074922783000001,
                    ],
                    [
                        'name'        => 'Mobile',
                        'impressions' => 2070,
                        'percentage'  => 0.1002500368,
                    ],
                    [
                        'name'        => 'Computer',
                        'impressions' => 3,
                        'percentage'  => 0.824827180467001,
                    ],
                ],
            ],
        ];
        Arr::set($body, 'haapi.response.payload', $data);

        return $body;
    }
}
