<?php

namespace Modules\Demo\Helpers\Response\Factory;

use App\Helpers\Json;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Arr;
use Modules\Demo\Exceptions\RequestNotSupportedException;
use Modules\Demo\Helpers\Response\Factory\Haapi\HaapiResponseFactory;
use Psr\Http\Message\ResponseInterface;

class ResponseFactory
{
    /**
     * @param Request $request
     *
     * @return ResponseInterface
     * @throws RequestNotSupportedException
     */
    public function createResponse(Request $request): ResponseInterface
    {
        $content = $request->getBody()->__toString();
        $headers = $request->getHeaders();

        return $this->getResponse($content, $headers);
    }

    /**
     * @param string $content
     *
     * @return ResponseInterface
     * @throws RequestNotSupportedException
     */
    private function getResponse(string $content, array $headers): ResponseInterface
    {
        $data = Json::decode($content);
        $data['headers'] = $headers;

        if ($this->isHaapi($data)) {
            return (new HaapiResponseFactory())->createFromContent($data);
        }

        throw RequestNotSupportedException::create(sprintf(
            'Request is not supported: "%s".',
            print_r($content, true)
        ));
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    private function isHaapi(array $data): bool
    {
        return Arr::has($data, 'haapi');
    }
}
