<?php

namespace Modules\Demo\Helpers;

use Illuminate\Support\Str;
use Modules\User\Models\User;

class ExternalIdHelper
{
    /**
     * Demo module have external id for application in format "demo0000-adve-rtis-er00-000000000000"
     *
     * To create unique uuid from which we can determine user external id
     * We take first part of the demo external id "demo0000-adve-rtis-er00-"
     * And create random string as second part "Sr5FCiX3WSR8"
     *
     * Result external id and account external id is "demo0000-adve-rtis-er00-Sr5FCiX3WSR8"
     *
     * And in this case we got enough unique value to work with while demo user session is alive
     *
     * @return string
     */
    public static function createAdvertiserExternalId(): string
    {
        return config('demo.uuid.advertiser.starts_with') . Str::random(12);
    }

    /**
     * @param User $admin
     *
     * @return string
     *
     * Almost same as
     * @see self::createAdvertiserExternalId()
     * but application needs to separate demo advertisers that logged in by themself
     * and advertisers that was generated for logged in admin.
     *
     * After admin would logout application need to delete those advertisers
     */
    public static function createAdvertiserForAdminExternalId(User $admin): string
    {
        return config('demo.uuid.admin.starts_with') . str_pad($admin->id, 12, 0);
    }

    /**
     * @return string
     */
    public static function createCampaignExternalId(): string
    {
        return config('demo.uuid.campaign.starts_with') . Str::random(12);
    }

    /**
     * @return string
     */
    public static function createCreativeExternalId(): string
    {
        return config('demo.uuid.creative.starts_with') . Str::random(12);
    }

    /**
     * @return string
     */
    public static function createPaymentMethodExternalId(): string
    {
        return config('demo.uuid.payment_method.starts_with') . Str::random(12);
    }
}
