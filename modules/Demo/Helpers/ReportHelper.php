<?php

namespace Modules\Demo\Helpers;

use Illuminate\Support\Arr;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportEmail;
use Modules\Report\Models\Traits\CreateReport;
use Modules\User\Models\User;

class ReportHelper
{
    use CreateReport;

    /**
     * @param User $advertiser
     * @param int  $index
     */
    public function create(User $advertiser, int $index): void
    {
        $report = $this->createReport($advertiser, $index);

        if ($report->isScheduled()) {
            $report->emails()->save(new ReportEmail(['email' => $advertiser->email]));
        }
    }

    /**
     * @param User $advertiser
     * @param int  $index
     *
     * @return Report
     */
    private function createReport(User $advertiser, int $index): Report
    {
        $records = config('demo.storage.reports.records', []);
        $record = Arr::get($records, $index % count($records));

        Arr::set($record, 'name', Arr::get($record, 'name') . ' #' . ++$index);
        Arr::set($record, 'user_id', $advertiser->id);

        return $this->createTestReport($record);
    }
}
