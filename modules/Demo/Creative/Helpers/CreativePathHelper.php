<?php

namespace Modules\Demo\Creative\Helpers;

use Illuminate\Support\Str;
use Modules\Creative\Helpers\CreativePathHelper as BaseCreativePathHelper;
use Modules\User\Models\User;

class CreativePathHelper extends BaseCreativePathHelper
{
    /**
     * @inheritdoc
     */
    public function getCreativePath(User $user): string
    {
        return sprintf('%s/creatives/%s', $this->getDemoUserFolder(), Str::uuid()->toString());
    }

    /**
     * @inheritdoc
     */
    public function getCreativePreviewPath(User $user, string $extension): string
    {
        return sprintf('%s/images/%s.%s', $this->getDemoUserFolder(), Str::uuid()->toString(), $extension);
    }

    /**
     * @return string
     */
    private function getDemoUserFolder(): string
    {
        return config('demo.storage.creatives.folder');
    }
}
