<?php

namespace Modules\Demo\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Actions\DeleteCreativeAction;
use Modules\Creative\Models\Creative;
use Modules\Creative\Repositories\CreativeRepository;
use Modules\Demo\Helpers\UserHelper;
use Modules\Notification\Models\Alert;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class CleanupAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var DeleteCreativeAction
     */
    private $deleteCreativeAction;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var CreativeRepository
     */
    private $creativeRepository;

    /**
     * @var UserHelper
     */
    private $userHelper;

    /**
     * @param LoggerInterface      $log
     * @param DatabaseManager      $databaseManager
     * @param DeleteCreativeAction $deleteCreativeAction
     * @param CreativeRepository   $creativeRepository
     * @param UserHelper           $userHelper
     */
    public function __construct(
        LoggerInterface $log,
        DatabaseManager $databaseManager,
        DeleteCreativeAction $deleteCreativeAction,
        CreativeRepository $creativeRepository,
        UserHelper $userHelper
    ) {
        $this->log = $log;
        $this->deleteCreativeAction = $deleteCreativeAction;
        $this->databaseManager = $databaseManager;
        $this->creativeRepository = $creativeRepository;
        $this->userHelper = $userHelper;
    }

    /**
     * @param User $user
     *
     * @throws \Throwable
     */
    public function handle(User $user): void
    {
        if (!$this->canBeDeleted($user)) {
            return;
        }

        $this->databaseManager->beginTransaction();

        try {
            // we have to delete creative files manually
            $this->deleteCreativeFiles($user);

            // and some records from DB also manually since they are not connected with foreign keys
            $this->deleteNotifications($user);
            $this->deleteAlerts($user);
            $this->deleteRoles($user);

            // everything other should be deleted with DB
            $user->delete();
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * @param User $advertiser
     */
    private function deleteRoles(User $advertiser): void
    {
        $this->log->info('Started deletion advertiser roles.', [
            'user_id'     => $advertiser->id,
            'external_id' => $advertiser->getExternalId(),
        ]);

        $count = DB::table('model_has_roles')
            ->where(['model_type' => get_class($advertiser)])
            ->where(['model_id' => $advertiser->id])
            ->delete();

        $this->log->info('Finished deletion advertiser roles.', [
            'user_id'     => $advertiser->id,
            'external_id' => $advertiser->getExternalId(),
            'count'       => $count,
        ]);
    }

    /**
     * @param User $advertiser
     */
    private function deleteAlerts(User $advertiser): void
    {
        $this->log->info('Started deletion advertiser alerts.', [
            'user_id'     => $advertiser->id,
            'external_id' => $advertiser->getExternalId(),
        ]);

        $ids = $advertiser->campaigns()->get('id')->toArray();
        $count = Alert::where(['item_type' => Campaign::class])
            ->whereIn('item_id', $ids)
            ->delete();

        $this->log->info('Finished deletion advertiser alerts.', [
            'user_id'     => $advertiser->id,
            'external_id' => $advertiser->getExternalId(),
            'count'       => $count,
        ]);
    }

    /**
     * @param User $advertiser
     */
    private function deleteNotifications(User $advertiser): void
    {
        $this->log->info('Started deletion advertiser notifications.', [
            'user_id'     => $advertiser->id,
            'external_id' => $advertiser->getExternalId(),
        ]);

        $count = DB::table('notifications')
            ->where(['notifiable_type' => get_class($advertiser)])
            ->where(['notifiable_id' => $advertiser->id])
            ->delete();

        $this->log->info('Finished deletion advertiser notifications.', [
            'user_id'     => $advertiser->id,
            'external_id' => $advertiser->getExternalId(),
            'count'       => $count,
        ]);
    }

    /**
     * Do not delete Advertiser if external id does not match with demo one
     * This is possible at least when Advertiser logged in in non-demo environment
     * And demo environment enabled while Advertiser is still logged in
     *
     * @param User $user
     *
     * @return bool
     */
    private function canBeDeleted(User $user): bool
    {
        if ($this->userHelper->isDemoAdvertiser($user)) {
            return true;
        }

        $this->log->warning('Prevented deletion of non-demo Advertiser.', [
            'external_id' => $user->getExternalId(),
            'user_id'     => $user->id,
        ]);

        return false;
    }

    /**
     * Delete manually uploaded creative files, seeded ones MUST left unchanged
     *
     * @param User $advertiser
     *
     * @throws \Modules\Creative\Exceptions\CreativeNotDeletedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function deleteCreativeFiles(User $advertiser): void
    {
        $creatives = $this->creativeRepository->byUser($advertiser)->get();
        $seededCreatives = $this->getSeededCreatives();
        $keys = array_column($seededCreatives, 'original_key');

        $this->log->info('Started deletion advertiser creatives.', [
            'user_id'              => $advertiser->id,
            'external_id'          => $advertiser->getExternalId(),
            'advertiser_creatives' => $creatives->count(),
            'seeded_creatives'     => count($seededCreatives),
        ]);

        $creatives->each(function (Creative $creative) use ($keys): void {
            if (in_array($creative->original_key, $keys, true)) {
                return;
            }

            $this->deleteCreativeAction->handle($creative);
        });

        $this->log->info('Finished deletion advertiser creatives.', [
            'user_id'     => $advertiser->id,
            'external_id' => $advertiser->getExternalId(),
        ]);
    }

    /**
     * @return array
     */
    private function getSeededCreatives(): array
    {
        return config('demo.storage.creatives.records', []);
    }
}
