<?php

namespace Modules\Demo\Payment\Policies;

use Modules\User\Models\User;
use \Modules\Payment\Policies\OrderPolicy as OriginOrderPolicy;

class OrderPolicy extends OriginOrderPolicy
{
    /**
     * Returns true if user can view an order.
     *
     * @param User $user
     * @param string $orderId
     * @return bool
     * @throws \Modules\Payment\Exceptions\OrderNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function view(User $user, string $orderId): bool
    {
        // TODO Need to modify after campaign would be seeded
        return true;
    }
}
