<?php

namespace Modules\Demo\Exceptions;

use App\Exceptions\BaseException;
use App\Services\LoggerService\LoggerService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Psr\Log\LoggerInterface;

class DemoUserNotCreatedException extends BaseException
{
    /**
     * @inheritdoc
     */
    public const STATUS_CODE = Response::HTTP_INTERNAL_SERVER_ERROR;

    /**
     * @param string|null     $message
     * @param \Throwable|null $previous
     *
     * @return self
     */
    public static function create(string $message = 'User not created.', \Throwable $previous = null): self
    {
        if ($previous) {
            /** @var LoggerInterface|LoggerService $logger */
            $logger = app(LoggerInterface::class);
            $logger->logException($previous, true);
        }

        return new self($message, self::STATUS_CODE, $previous);
    }

    /**
     * Report the exception.
     *
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json([
            'message' => $this->message,
        ], self::STATUS_CODE);
    }
}
