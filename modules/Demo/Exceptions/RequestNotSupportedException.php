<?php

namespace Modules\Demo\Exceptions;

use App\Exceptions\BaseException;

class RequestNotSupportedException extends BaseException
{
    /**
     * @param string $message
     *
     * @return self
     */
    public static function create(string $message): self
    {
        return new self($message);
    }
}
