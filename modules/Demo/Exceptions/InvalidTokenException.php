<?php

namespace Modules\Demo\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class InvalidTokenException extends BaseException
{
    /**
     * @inheritdoc
     */
    public const STATUS_CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * @param string|null $message
     *
     * @return self
     */
    public static function create(string $message = 'Invalid token.'): self
    {
        return new self($message);
    }

    /**
     * Report the exception.
     *
     * @return \Illuminate\Http\RedirectResponse|JsonResponse
     */
    public function render()
    {
        if (!\request()->is('api/*')) {
            return redirect()->route('login.form');
        }

        return response()->json([
            'message' => $this->message,
        ], self::STATUS_CODE);
    }
}
