<?php

namespace Modules\Demo;

use App\Providers\ModuleServiceProvider;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Event;
use Modules\Auth\Events\UserLogin;
use Modules\Auth\Http\Controllers\Api\AdminLogoutController;
use Modules\Auth\Http\Controllers\Api\LoginController;
use Modules\Auth\Http\Controllers\Api\LogoutController;
use Modules\Auth\Http\Controllers\AdminAttemptLoginController;
use Modules\Auth\Http\Requests\RegisterUserRequest;
use Modules\Auth\Services\AuthService\Actions\ValidateCognitoJwtAction;
use Modules\Demo\Auth\Http\Services\ValidateCognitoJwtAction as DemoValidateCognitoJwtAction;
use Modules\Broadcast\Commands\StartBroadcastCommand;
use Modules\Campaign\Actions\Status\ResumeCampaignAction;
use Modules\Creative\Helpers\CreativePathHelper;
use Modules\Campaign\Actions\Status\CancelCampaignAction;
use Modules\Demo\Broadcast\Commands\DemoStartBroadcastCommand;
use Modules\Demo\Campaign\Actions\Status\CancelCampaignAction as DemoCancelCampaignAction;
use Modules\Demo\Campaign\Actions\Status\ResumeCampaignAction as DemoResumeCampaignAction;
use Modules\Demo\Auth\Http\Controllers\Api\AdminLogoutController as DemoAdminLogoutController;
use Modules\Demo\Auth\Http\Controllers\Api\LoginController as DemoLoginController;
use Modules\Demo\Auth\Http\Controllers\Api\LogoutController as DemoLogoutController;
use Modules\Demo\Auth\Http\Controllers\AdminAttemptLoginController as DemoAdminAttemptLoginController;
use Modules\Demo\Auth\Http\Requests\RegisterUserRequest as DemoRegisterUserRequest;
use Modules\Demo\Commands\CleanupCommand;
use Modules\Demo\Creative\Helpers\CreativePathHelper as DemoCreativePathHelper;
use Modules\Demo\Commands\AccountUpdateCallbackCommand;
use Modules\Demo\Commands\CampaignCreateCallbackCommand;
use Modules\Demo\Commands\CreativeUpdateCallbackCommand;
use Modules\Demo\Commands\StatusNotificationCallbackCommand;
use Modules\Demo\Helpers\ResponseHelper;
use Modules\Demo\Invitation\Http\Controllers\Api\ValidateInviteCodeController as DemoValidateInviteCodeController;
use Modules\Demo\Invitation\Repositories\InvitationRepository as DemoInvitationRepository;
use Modules\Demo\Listeners\UserLoginListener;
use Modules\Demo\Report\Actions\DestroyReportFileAction as DemoDestroyReportFileAction;
use Modules\Demo\Report\Actions\GetReportUploadPathAction as DemoGetReportUploadPathAction;
use Modules\Invitation\Http\Controllers\Api\ValidateInviteCodeController;
use Modules\Invitation\Repositories\InvitationRepository;
use Modules\Payment\Http\Controllers\Api\DownloadOrderPdfController;
use Modules\Demo\Payment\Http\Controllers\Api\DownloadOrderPdfController as DemoDownloadOrderPdfController;
use Modules\Payment\Http\Controllers\Api\ShowOrderController;
use Modules\Demo\Payment\Http\Controllers\Api\ShowOrderController as DemoShowOrderController;
use Modules\Payment\Policies\OrderPolicy;
use Modules\Demo\Payment\Policies\OrderPolicy as DemoOrderPolicy;
use Modules\Report\Actions\DestroyReportFileAction;
use Modules\Report\Actions\GetReportUploadPathAction;
use Modules\Support\Http\Controllers\Api\ContactController;
use Modules\Demo\Support\Http\Controllers\Api\ContactController as DemoContactController;
use Psr\Http\Message\ResponseInterface;

class DemoServiceProvider extends ModuleServiceProvider
{
    /**
     * @var int
     */
    private const MAX_MOCKED_REQUESTS = 100;

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        CampaignCreateCallbackCommand::class,
        CreativeUpdateCallbackCommand::class,
        AccountUpdateCallbackCommand::class,
        StatusNotificationCallbackCommand::class,
        CleanupCommand::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function register(): void
    {
        parent::register();
        $this->mergeConfigFrom(__DIR__ . '/config/demo.php', 'demo');

        $this->rebindClasses();
        $this->setConfig();
        $this->addEventListeners();

        $this->commands($this->commands);
    }

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'demo';
    }

    /**
     * Set config for module.
     *
     * @return void
     */
    private function setConfig(): void
    {
        config(['mail.driver' => 'log']);
    }

    /**
     * Rebind classes to be able to mock and change functional
     */
    private function rebindClasses(): void
    {
        $this->rebindInvitationModule();
        $this->rebindAuthModule();
        $this->rebindPaymentModule();
        $this->rebindCampaignModule();
        $this->rebindSupportModule();
        $this->rebindCreativeModule();
        $this->rebindReportModule();
        $this->rebindBroadcastModule();

        $this->rebindGuzzleClient();
    }

    /**
     * Rebind Campaign Module
     */
    private function rebindCampaignModule(): void
    {
        $this->app->bind(ResumeCampaignAction::class, function () {
            return $this->app->make(DemoResumeCampaignAction::class);
        });

        $this->app->bind(CancelCampaignAction::class, function () {
            return $this->app->make(DemoCancelCampaignAction::class);
        });
    }

    /**
     * Rebind Payment Module
     */
    private function rebindPaymentModule(): void
    {
        $this->app->bind(ShowOrderController::class, function () {
            return $this->app->make(DemoShowOrderController::class);
        });

        $this->app->bind(DownloadOrderPdfController::class, function () {
            return $this->app->make(DemoDownloadOrderPdfController::class);
        });

        $this->app->bind(OrderPolicy::class, function () {
            return $this->app->make(DemoOrderPolicy::class);
        });
    }

    /**
     * Rebind Invitation Module
     */
    private function rebindInvitationModule(): void
    {
        $this->app->bind(ValidateInviteCodeController::class, function () {
            return $this->app->make(DemoValidateInviteCodeController::class);
        });

        $this->app->bind(InvitationRepository::class, function () {
            return $this->app->make(DemoInvitationRepository::class);
        });
    }

    /**
     * Rebind Auth Module
     */
    private function rebindAuthModule(): void
    {
        $this->app->bind(RegisterUserRequest::class, function () {
            return $this->app->make(DemoRegisterUserRequest::class);
        });

        $this->app->bind(ValidateCognitoJwtAction::class, function () {
            return $this->app->make(DemoValidateCognitoJwtAction::class);
        });

        $this->app->bind(LoginController::class, function () {
            return $this->app->make(DemoLoginController::class);
        });

        $this->app->bind(LogoutController::class, function () {
            return $this->app->make(DemoLogoutController::class);
        });

        $this->app->bind(AdminLogoutController::class, function () {
            return $this->app->make(DemoAdminLogoutController::class);
        });

        $this->app->bind(AdminAttemptLoginController::class, function () {
            return $this->app->make(DemoAdminAttemptLoginController::class);
        });
    }

    /**
     * Rebind Support Module
     */
    private function rebindSupportModule(): void
    {
        $this->app->bind(ContactController::class, function () {
            return $this->app->make(DemoContactController::class);
        });
    }

    /**
     * Rebind Creative Module
     */
    private function rebindCreativeModule(): void
    {
        $this->app->bind(CreativePathHelper::class, function () {
            return $this->app->make(DemoCreativePathHelper::class);
        });
    }

    /**
     * Rebind Report Module
     */
    private function rebindReportModule(): void
    {
        $this->app->bind(GetReportUploadPathAction::class, function () {
            return $this->app->make(DemoGetReportUploadPathAction::class);
        });

        $this->app->bind(DestroyReportFileAction::class, function () {
            return $this->app->make(DemoDestroyReportFileAction::class);
        });
    }

    /**
     * Rebind Broadcast Module
     */
    private function rebindBroadcastModule(): void
    {
        $this->app->bind(StartBroadcastCommand::class, function () {
            return $this->app->make(DemoStartBroadcastCommand::class);
        });
    }

    /**
     * Rebind Guzzle Client
     */
    private function rebindGuzzleClient(): void
    {
        $callable = function (Request $request): ResponseInterface {
            return (new ResponseHelper())->createResponse($request);
        };

        // MockHandler requires array of callables to work with
        // In demo env application need to mock a lot of different requests
        // That's why using a queue of predefined responses are not the best idea
        // Application uses ResponseHelper to determine what response to return
        // And since MockHandler requires an array to work with (not an object with interfaces that mimic array)
        // We set queue with enough amount of callables
        $callables = array_map(function () use ($callable): callable {
            return $callable;
        }, range(1, self::MAX_MOCKED_REQUESTS));

        $handler = new MockHandler($callables);
        $client = new Client(['handler' => $handler]);
        $this->app->instance(Client::class, $client);
    }

    /**
     * Demo event listeners
     */
    private function addEventListeners(): void
    {
        Event::listen(UserLogin::class, function (UserLogin $event): void {
            /** @var UserLoginListener $listener */
            $listener = app(UserLoginListener::class);
            $listener->handle($event);
        });
    }
}
