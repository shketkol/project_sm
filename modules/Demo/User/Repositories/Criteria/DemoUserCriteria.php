<?php

namespace Modules\Demo\User\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class DemoUserCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where(function (Builder $query) {
            $query->where('users.account_external_id', 'like', config('demo.uuid.advertiser.starts_with') . '%')
                ->orWhere('users.account_external_id', 'like', config('demo.uuid.admin.starts_with') . '%');
        });
    }
}
