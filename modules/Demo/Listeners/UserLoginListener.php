<?php

namespace Modules\Demo\Listeners;

use Illuminate\Database\DatabaseManager;
use Modules\Auth\Events\UserLogin;
use Modules\Demo\Exceptions\DemoUserNotCreatedException;
use Modules\Demo\Helpers\PaymentHelper;
use Modules\Demo\Helpers\UserHelper;
use Modules\User\Models\User;

class UserLoginListener
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var UserHelper
     */
    private $userHelper;

    /**
     * @var PaymentHelper
     */
    private $paymentHelper;

    /**
     * @param DatabaseManager $databaseManager
     * @param UserHelper      $userHelper
     * @param PaymentHelper   $paymentHelper
     */
    public function __construct(DatabaseManager $databaseManager, UserHelper $userHelper, PaymentHelper $paymentHelper)
    {
        $this->databaseManager = $databaseManager;
        $this->userHelper = $userHelper;
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * @param UserLogin $event
     *
     * @throws DemoUserNotCreatedException
     */
    public function handle(UserLogin $event): void
    {
        if ($event->user->isAdvertiser()) {
            $this->handleAdvertiser($event->user);
            return;
        }

        if ($event->user->isAdmin()) {
            $this->handleAdmin($event->user);
        }
    }

    /**
     * @param User $user
     *
     * @throws DemoUserNotCreatedException
     * @throws \Exception
     */
    private function handleAdvertiser(User $user): void
    {
        $this->databaseManager->beginTransaction();

        try {
            $this->paymentHelper->firstOrCreate($user);

            if ($this->userHelper->shouldBeSeeded($user)) {
                $this->userHelper->seedAdvertiser($user);
            }
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw DemoUserNotCreatedException::create('Error occurred while creating Advertiser.', $exception);
        }

        $this->databaseManager->commit();
    }

    /**
     * @param User $user
     *
     * @throws DemoUserNotCreatedException
     * @throws \Exception
     */
    private function handleAdmin(User $user): void
    {
        $this->databaseManager->beginTransaction();

        try {
            $this->userHelper->seedAdmin($user);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw DemoUserNotCreatedException::create('Error occurred while creating Admin.', $exception);
        }

        $this->databaseManager->commit();
    }
}
