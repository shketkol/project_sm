<?php

use Modules\Creative\Models\CreativeStatus;
use Modules\Report\Models\ReportType;

$demoStorageFolder = env('DEMO_STORAGE_CREATIVES_FOLDER', 'demo_advertisers');
$ttl = 1440; // default time to live is 1440 minutes (1 day)

// phpcs:ignoreFile
 return [
    'users'       => [
        'limit'       => env('DEMO_USERS_LIMIT', 20),
        'ttl'         => env('DEMO_USERS_TTL', $ttl),
        'credentials' => [
            [
                'email'         => env('DEMO_USER_SEEDED_EMAIL', 'June.Osborne@hulu.com'),
                'password'      => env('DEMO_USER_SEEDED_PASSWORD', 'Dq7q^zf@A)3-kA;b'),
                'name'          => env('DEMO_USER_SEEDED_NAME', 'June'),
                'need_for_seed' => true,
            ],
            [
                'email'         => env('DEMO_USER_CLEAN_EMAIL', 'Josh.Futterman@hulu.com'),
                'password'      => env('DEMO_USER_CLEAN_PASSWORD', 'hF2Qu?JPP3t"M4wg'),
                'name'          => env('DEMO_USER_CLEAN_NAME', 'Josh'),
                'need_for_seed' => false,
            ],
        ],
    ],
    'uuid'        => [
        'advertiser'     => [
            // used for advertiser himself
            'starts_with' => 'demo0000-adve-rtis-er00-',
        ],
        'admin'          => [
            // used for admin to seed advertisers
            // can't use advertiser's format since it used to control amount of logged in demo advertisers
            'starts_with' => 'demo0000-admi-n000-0000-',
        ],
        'campaign'       => [
            'starts_with' => 'demo0000-camp-aign-0000-',
        ],
        'creative'       => [
            'starts_with' => 'demo0000-crea-tive-0000-',
        ],
        'payment_method' => [
            'starts_with' => 'demo0000-paym-ent0-0000-',
        ],
    ],
    'campaigns'   => [
        'number_to_create' => env('DEMO_CAMPAIGNS_NUMBER_TO_CREATE', 5),
        'ttl'              => env('DEMO_CAMPAIGNS_TTL', $ttl),
    ],
    'advertisers' => [
        'number_to_create' => env('DEMO_ADVERTISERS_NUMBER_TO_CREATE', 5),
    ],
    'reports'     => [
        'number_to_create' => env('DEMO_REPORTS_NUMBER_TO_CREATE', 5),
    ],
    'targetings'  => [
        'inventory_check' => [
            // Default type target 'krux' (Audiences)
            'type_id_for_fail' => env('DEMO_TARGETINGS_INVENTORY_CHECK_TYPE_ID_FOR_FAIL', '285f9b5c-066a-11ea-8714-0a3b51d4007e'),

            // Default value 'All Autos' (Behavior.In-Market.All Autos)
            'value_for_fail'   => env('DEMO_TARGETINGS_INVENTORY_CHECK_VALUE_FOR_FAIL', '2675cea4-7074-11ea-ba9d-0a3b51d4007e'),

            'min_daily_impressions' => env('DEMO_TARGETINGS_INVENTORY_MIN_DAILY_IMPRESSIONS', 1000),
        ],
        'cpm'             => [
            'extra_rate' => [
                'locations' => env('DEMO_TARGETINGS_CPM_EXTRA_RATE_LOCATIONS', 0.1),
                'devices'   => env('DEMO_TARGETINGS_CPM_EXTRA_RATE_DEVICES', 0.1),
                'audiences' => env('DEMO_TARGETINGS_CPM_EXTRA_RATE_AUDIENCES', 2.5),
            ],
        ],
    ],

    // if you need to make any changes here make sure you have it updated in storage also
    // https://s3.console.aws.amazon.com/s3/buckets/da-hulu-demo-usercontent/demo_advertisers/?region=us-east-1&tab=overview
    'storage'     => [
        'reports'   => [
            'folder'  => $demoStorageFolder,
            'records' => [
                [
                    'name'    => 'Demo Downloadable Report',
                    'path'    => "{$demoStorageFolder}/reports/Hulu+Ad+Manager+-+Demo+Report.xlsx",
                    'type_id' => ReportType::ID_DOWNLOAD,
                ],
                [
                    'name'    => 'Demo Scheduled Report',
                    'path'    => "{$demoStorageFolder}/reports/Hulu+Ad+Manager+-+Demo+Report.xlsx",
                    'type_id' => ReportType::ID_SCHEDULED,
                ],
            ],
        ],
        'creatives' => [
            'folder'  => $demoStorageFolder,
            'records' => [
                [
                    'name'         => 'MissingLink_Promo_17s_103119_MASTER_higher_bitrate_edit',
                    'extension'    => 'mov',
                    'duration'     => 17.03,
                    'width'        => 1920,
                    'height'       => 1080,
                    'filesize'     => 362927604,
                    'status_id'    => CreativeStatus::ID_APPROVED,
                    'original_key' => "{$demoStorageFolder}/creatives/1f91ba62-2001-442b-b8d4-c562b0a84983.mp4",
                    'preview_url'  => "https://da-hulu-demo-usercontent.s3.amazonaws.com/{$demoStorageFolder}/creatives/1f91ba62-2001-442b-b8d4-c562b0a84983.mp4",
                    'poster_key'   => "{$demoStorageFolder}/images/c7988d05-8fbf-4aad-95bf-7514167da1f3.jpg",
                    'hash'         => '415952cf07706a3d19035c0d5e78f47a43de79c4d28e1194194c95da7ae41691',
                    'is_pro_res'   => 0,
                ],
                [
                    'name'         => 'PlusOne_ILoveWeddings_16x9_MASTER',
                    'extension'    => 'mov',
                    'duration'     => 15.849,
                    'width'        => 1920,
                    'height'       => 1080,
                    'filesize'     => 282063956,
                    'status_id'    => CreativeStatus::ID_APPROVED,
                    'original_key' => "{$demoStorageFolder}/creatives/3e127f4e-86fd-40e6-9b23-a437673541e3.mp4",
                    'preview_url'  => "https://da-hulu-demo-usercontent.s3.amazonaws.com/{$demoStorageFolder}/creatives/3e127f4e-86fd-40e6-9b23-a437673541e3.mp4",
                    'poster_key'   => "{$demoStorageFolder}/images/0aede703-e075-4646-a2d6-bb0797eca4ee.jpg",
                    'hash'         => '50134aa66c88413622110c6d728f1bc7ff5359804eb1e2b1fca5ea4cf34770ed',
                    'is_pro_res'   => 0,
                ],
                [
                    'name'         => 'FightingWithMyFamily_House_30s_16x9_012920_MASTER_higher_bitrate_edit',
                    'extension'    => 'mov',
                    'duration'     => 30.03,
                    'width'        => 1920,
                    'height'       => 1080,
                    'filesize'     => 410724356,
                    'status_id'    => CreativeStatus::ID_APPROVED,
                    'original_key' => "{$demoStorageFolder}/creatives/347bc4bf-2340-4f0e-a496-67bd3bfde776.mp4",
                    'preview_url'  => "https://da-hulu-demo-usercontent.s3.amazonaws.com/{$demoStorageFolder}/creatives/347bc4bf-2340-4f0e-a496-67bd3bfde776.mp4",
                    'poster_key'   => "{$demoStorageFolder}/images/9d68c33b-bdb9-473b-99f3-bfbe5e306f6c.jpg",
                    'hash'         => '761b15b6c79f5da8f5e336a1d8c6fdbc4e0959098f3046e329f35e0657479190',
                    'is_pro_res'   => 0,
                ],
                [
                    'name'         => '2020_Q1_NewMoviesPromo_Refresh_30s_v4_020720_BS_',
                    'extension'    => 'mp4',
                    'duration'     => 30.197,
                    'width'        => 1920,
                    'height'       => 1080,
                    'filesize'     => 69110681,
                    'status_id'    => CreativeStatus::ID_APPROVED,
                    'original_key' => "{$demoStorageFolder}/creatives/9ea4036c-287c-4380-abd0-90ec9144755a.mp4",
                    'preview_url'  => "https://da-hulu-demo-usercontent.s3.amazonaws.com/{$demoStorageFolder}/creatives/9ea4036c-287c-4380-abd0-90ec9144755a.mp4",
                    'poster_key'   => "{$demoStorageFolder}/images/caf27206-e438-4475-96fb-b54f32bfdd11.jpg",
                    'hash'         => '798415a69d39c295964f148ae6777447e85e47144000adb056e49c9566a4f498',
                    'is_pro_res'   => 0,
                ],
                [
                    'name'         => 'UtopiaFalls_S1_TrailerCutdown_30s_16x9_House_021320_MASTER_higher_bitrate_edit',
                    'extension'    => 'mov',
                    'duration'     => 30.03,
                    'width'        => 1920,
                    'height'       => 1080,
                    'filesize'     => 271384052,
                    'status_id'    => CreativeStatus::ID_APPROVED,
                    'original_key' => "{$demoStorageFolder}/creatives/abd22492-6331-4ff4-b713-ad529f4a8b0c.mp4",
                    'preview_url'  => "https://da-hulu-demo-usercontent.s3.amazonaws.com/{$demoStorageFolder}/creatives/abd22492-6331-4ff4-b713-ad529f4a8b0c.mp4",
                    'poster_key'   => "{$demoStorageFolder}/images/bd0315d1-9ac0-4975-adf3-d231ffb18417.jpg",
                    'hash'         => '9da131bab4c94cf64574b612b1c1f87de6e1c14023793d38590df3f747589d0c',
                    'is_pro_res'   => 0,
                ],
            ],
        ],
    ],
];
