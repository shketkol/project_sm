<?php

namespace Modules\Demo\Invitation\Repositories;

use Modules\Invitation\Repositories\InvitationRepository as BaseInvitationRepository;

class InvitationRepository extends BaseInvitationRepository
{
    /**
     * @param string $code
     *
     * @return void
     */
    public function applyProcessingStatus(string $code): void
    {
        // ignore invite code for demo env
    }
}
