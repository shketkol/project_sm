<?php

namespace Modules\Demo\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Faker\Generator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ValidateInviteCodeController extends Controller
{
    /**
     * @param Request   $request
     * @param Generator $faker
     *
     * @return Response
     */
    public function __invoke(Request $request, Generator $faker): Response
    {
        return response(['email' => $faker->safeEmail]);
    }
}
