<?php

namespace Modules\Demo\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Modules\Auth\Traits\AdminAuthHelper;

class AdminAttemptLoginController extends Controller
{
    use AdminAuthHelper,
        AuthenticatesUsers {
        logout as public doLogout;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show(Request $request)
    {
        $this->doLogout($request);

        return redirect($this->getAdminLoginRedirectUrl());
    }
}
