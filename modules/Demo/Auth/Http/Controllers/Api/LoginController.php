<?php

namespace Modules\Demo\Auth\Http\Controllers\Api;

use App\Services\ValidationRulesService\ValidationRules;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Modules\Auth\Http\Controllers\Api\LoginController as BaseLoginController;
use Modules\Demo\Helpers\AuthHelper;
use Modules\Demo\Http\Middleware\UsersLimit;
use Psr\Log\LoggerInterface;

class LoginController extends BaseLoginController
{
    /**
     * @inheritdoc
     */
    public function __construct(
        LoggerInterface $log,
        Dispatcher $dispatcher,
        SessionManager $session,
        ValidationRules $validationRules
    ) {
        $this->middleware(UsersLimit::class)->only('login');

        parent::__construct($log, $dispatcher, $session, $validationRules);
    }

    /**
     * Application works with Amazon Cognito on default systems.
     * To change as less as possible and use predefined credentials
     *
     * @see AuthHelper::getCredentialsFromToken()
     *
     * This method would check if credentials matched with any in config and then proceed default procedure
     * @throws \Modules\Demo\Exceptions\InvalidTokenException
     *
     * @inheritdoc
     */
    protected function attemptLogin(Request $request): bool
    {
        $token = $request->get('token');
        if (!$token) {
            $this->log->debug('Token is missing.');
            return false;
        }

        ['email' => $userEmail, 'password' => $userPassword] = AuthHelper::getCredentialsFromToken($token);

        foreach (config('demo.users.credentials') as ['email' => $email, 'password' => $password]) {
            if ($userEmail === $email && $userPassword === $password) {
                $this->log->debug('Credentials matched with config.');
                return parent::attemptLogin($request);
            }
        }

        $this->log->debug('Credentials did not match with config.');

        return false;
    }
}
