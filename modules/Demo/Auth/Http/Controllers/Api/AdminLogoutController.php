<?php

namespace Modules\Demo\Auth\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use Modules\Auth\Http\Controllers\Api\AdminLogoutController as BaseAdminLogoutController;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Demo\Helpers\ExternalIdHelper;
use Modules\User\Models\User;
use Modules\User\Repositories\UserRepository;
use Psr\Log\LoggerInterface;

class AdminLogoutController extends BaseAdminLogoutController
{
    /**
     * @inheritdoc
     */
    public function __invoke(AuthService $authService, LoggerInterface $log)
    {
        $admin = Auth::guard('admin')->user();
        $response = parent::__invoke($authService, $log);

        if (!is_null($admin)) {
            $this->deleteAdmin($admin);
        }

        return $response;
    }

    /**
     * @param User $admin
     *
     * @return void
     * @throws \Exception
     */
    private function deleteAdmin(User $admin): void
    {
        // delete admin advertisers
        $externalId = ExternalIdHelper::createAdvertiserForAdminExternalId($admin);

        /** @var UserRepository $repository */
        $repository = app(UserRepository::class);
        $repository->deleteByExternalId([$externalId]);

        // delete admin itself
        $admin->delete();
    }
}
