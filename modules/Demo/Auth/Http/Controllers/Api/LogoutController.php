<?php

namespace Modules\Demo\Auth\Http\Controllers\Api;

use App\Exceptions\Contracts\UnauthorizedException;
use App\Helpers\EnvironmentHelper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Http\Controllers\Api\LogoutController as BaseLogoutController;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Demo\Actions\CleanupAction;
use Modules\User\Models\User;

class LogoutController extends BaseLogoutController
{
    /**
     * @param Request     $request
     * @param AuthService $authService
     *
     * @return JsonResponse
     * @throws UnauthorizedException
     */
    public function __invoke(Request $request, AuthService $authService): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        if (is_null($user)) {
            return $this->redirect();
        }

        $this->doLogout($request);

        return $this->redirect();
    }

    /**
     * @inheritdoc
     * @throws \Throwable
     * @see logout
     */
    public function doLogout(Request $request)
    {
        /** @var User $user */
        $user = $this->guard()->user();
        $response = parent::logout($request);

        /** @var CleanupAction $action */
        $action = app(CleanupAction::class);
        $action->handle($user);

        return $response;
    }

    /**
     * @return JsonResponse
     */
    private function redirect(): JsonResponse
    {
        return response()->json([
            'redirect' => EnvironmentHelper::isMockedLandingEnv() ? route('landing') : config('auth.redirect.url'),
        ]);
    }
}
