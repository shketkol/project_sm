<?php

namespace Modules\Demo\Auth\Http\Services;

use \Modules\Auth\Services\AuthService\Actions\ValidateCognitoJwtAction as ValidateCognitoJwtActionOrigin;

class ValidateCognitoJwtAction extends ValidateCognitoJwtActionOrigin
{
    /**
     * @param string $jwt
     *
     * @return bool
     */
    public function handle(string $jwt): bool
    {
        // For admin in demo mode we need to actually refresh token. demo-adver will have non-valid jwt.
        if ($this->isValidJwtStructure($jwt)) {
            return parent::handle($jwt);
        }

        return true;
    }
}
