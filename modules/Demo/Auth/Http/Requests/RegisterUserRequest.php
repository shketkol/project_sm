<?php

namespace Modules\Demo\Auth\Http\Requests;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Support\Arr;
use Modules\Auth\Http\Requests\RegisterUserRequest as BaseRegisterUserRequest;

class RegisterUserRequest extends BaseRegisterUserRequest
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        $rules = parent::rules($validationRules);

        Arr::set($rules, 'email', $validationRules->only(
            'user.email',
            ['required', 'string', 'email', 'max', 'valid_email']
        ));

        Arr::forget($rules, 'invite_code');

        return $rules;
    }
}
