<?php

namespace Modules\Demo\Broadcast\Commands;

use Illuminate\Support\Collection;
use Modules\Broadcast\Commands\StartBroadcastCommand;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastStatus;

class DemoStartBroadcastCommand extends StartBroadcastCommand
{
    /**
     * @param Collection|Broadcast[] $broadcasts
     *
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function dispatchBroadcasts(Collection $broadcasts): void
    {
        $broadcasts->each(function (Broadcast $broadcast): void {
            $broadcast->applyInternalStatus(BroadcastStatus::PROCESSING);
            $broadcast->applyInternalStatus(BroadcastStatus::SENT);

            $this->log->info('[DEMO Broadcast command] Broadcast dispatched.', [
                'broadcast_id' => $broadcast->id,
            ]);
        });
    }
}
