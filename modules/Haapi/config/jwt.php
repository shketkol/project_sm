<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Enable JWT
    |--------------------------------------------------------------------------
    |
    | If value is set to true, pre shared key will be generated JWT
    |
    */

    'enable' => env('JWT_ENABLE', true),

    /*
    |--------------------------------------------------------------------------
    | Algorithm Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of signing algorithm being used,
    | such as HMAC SHA256 or RSA.
    |
    */

    'alg' => 'HS256',

    /*
    |--------------------------------------------------------------------------
    | Media Type Name
    |--------------------------------------------------------------------------
    |
    |  The application can use this value to disambiguate among the different
    |  kinds of objects that might be present.
    |
    */

    'typ' => 'JWT',

    /*
    |--------------------------------------------------------------------------
    | Claim "Issuer"
    |--------------------------------------------------------------------------
    |
    |  This is a unique string identifier that is used to identify an external
    |  third party. With the issuer value, Hulu can confidently verify the
    |  identity of the API caller.
    |
    */

    'iss' => 'danads',

    /*
    |--------------------------------------------------------------------------
    | Expiration Time
    |--------------------------------------------------------------------------
    |
    |  Time interval (in seconds) which after jwt token will expire, it should
    |  be less than presharedKey expiration time.
    |
    */

    'expire' => env('JWT_EXPIRE', 55 * 60),

    /*
    |--------------------------------------------------------------------------
    | Expiration Time
    |--------------------------------------------------------------------------
    |
    |  Time interval which after jwt token will expire, it should be lesser
    |  than presharedKey expiration time.
    |
    */

    'shared_secret' => env('JWT_SHARED_SECRET', 'SAMLE_SECRET_STING')

];
