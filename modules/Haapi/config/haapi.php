<?php

return [
    'prefix'                     => env('HAAPI_DEFAULT_PREFIX', ''),
    'base_url'                   => env('HAAPI_BASE_URL', 'https://selfservice.adservices.huluqa.com/api'),
    'admin_token_cache_lifetime' => env('HAAPI_ADMIN_TOKEN_CACHE_LIFETIME', 300), // 5min
    'system'                     => env('HAAPI_SYSTEM', 'danads-api'),
    'haapi_version'              => env('HAAPI_VERSION', '1'),
    'shared_key'                 => env('HULU_SHARED_KEY', 'f2e64f90-624b-4db5-aecd-cf23b3772822'),
];
