<?php

/**
 * Stubs for HAAPI
 * If service is down, we should be able use system for testing purpose
 * All values should be FALSE by default
 */
return [
  'callback' => [
      'stub' => env('STUB_CALLBACK', false),
      'host' => env('STUB_HOST', 'http://{YOU_ID}.ngrok.io'),
  ],
  'login' => env('STUB_LOGIN', false),
  'creative' => [
      'upload' => env('STUB_CREATIVE_UPLOAD', false),
      'update' => env('STUB_CREATIVE_UPDATE', false),
  ],
];
