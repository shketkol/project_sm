<?php

namespace Modules\Haapi\Actions;

use App\Helpers\SessionIdentifierHelper;
use Illuminate\Support\Arr;

/**
 * @package Modules\Haapi\Actions
 */
abstract class BaseCallbackAction extends BaseAction
{
    /**
     * @var SessionIdentifierHelper
     */
    private $identifierHelper;

    /**
     * @param SessionIdentifierHelper $identifierHelper
     */
    public function __construct(SessionIdentifierHelper $identifierHelper)
    {
        parent::__construct();
        $this->identifierHelper = $identifierHelper;
    }

    /**
     * @param array $payload
     *
     * @return array
     */
    protected function getPayloadWithSessionIdentifier(array $payload): array
    {
        Arr::set(
            $payload,
            static::IDENTIFIER_KEY,
            $this->identifierHelper->getIdentifier()
        );

        return $payload;
    }
}
