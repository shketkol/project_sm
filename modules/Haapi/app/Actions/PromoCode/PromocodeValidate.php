<?php

namespace Modules\Haapi\Actions\PromoCode;

use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\DataTransferObjects\Promocode\PromocodeValidateData;
use Modules\Haapi\DataTransferObjects\Promocode\PromocodeValidateParams;
use Modules\Haapi\Services\Contracts\RequestService;

class PromocodeValidate extends BaseAction
{
    /**
     * Endpoint type
     */
    private const REQUEST_TYPE = 'promocode/validate';

    /**
     * HAAPI Decision about promocode validity.
     */
    public const PROMOCODE_APPROVED = 'Approved';


    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param PromocodeValidateParams $params
     * @param int                     $userId
     *
     * @return PromocodeValidateData
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(PromocodeValidateParams $params, int $userId): PromocodeValidateData
    {
        $response = $this->requestService->request(
            self::REQUEST_TYPE,
            $params->all(),
            $userId
        );

        $this->checkPromocodeValid($response->getPayload());

        return new PromocodeValidateData($response->getPayload());
    }

    /**
     * @param array $payload
     *
     * @return bool
     * @throws ValidationException
     */
    private function checkPromocodeValid(array $payload): bool
    {
        if (Arr::get($payload, 'decision') !== self::PROMOCODE_APPROVED) {
            throw ValidationException::withMessages([
                'promocode' => Arr::get($payload, 'message', 'Promocode can not be found or expired.')
            ]);
        }

        return true;
    }
}
