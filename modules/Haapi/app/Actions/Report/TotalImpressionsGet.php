<?php

namespace Modules\Haapi\Actions\Report;

use Illuminate\Support\Arr;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Services\Contracts\RequestService;

class TotalImpressionsGet extends BaseAction
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'report/accountImpressions';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CreativeGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param string $accountId
     * @param int    $userId
     *
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function handle(string $accountId, int $userId): int
    {
        $response = $this->requestService->request(self::REQUEST_TYPE, compact(['accountId']), $userId);
        return Arr::get($response->getPayload(), 'impressions');
    }
}
