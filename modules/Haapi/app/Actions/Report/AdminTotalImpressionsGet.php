<?php

namespace Modules\Haapi\Actions\Report;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Services\Contracts\RequestService;

class AdminTotalImpressionsGet extends BaseAction
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'report/totalImpressions';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CreativeGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param string $accountId
     * @param int $userId
     * @return int
     */
    public function handle(string $accountId, int $userId): int
    {
        sleep(2);
        return 1000000;
    }
}
