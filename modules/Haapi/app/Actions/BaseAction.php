<?php

namespace Modules\Haapi\Actions;

/**
 * Class BaseAction
 *
 * @package Modules\Haapi\Actions
 */
abstract class BaseAction
{
    /**
     * BaseAction constructor.
     */
    public function __construct()
    {
    }
}
