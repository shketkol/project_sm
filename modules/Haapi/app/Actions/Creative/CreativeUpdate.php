<?php

namespace Modules\Haapi\Actions\Creative;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Creative\Contracts\CreativeUpdate as CreativeUpdateInterface;
use Modules\Haapi\DataTransferObjects\Creative\CreativeUpdateParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

/**
 * Class CreativeUpdate
 *
 * @package Modules\Haapi\Actions\Creative
 */
class CreativeUpdate extends BaseAction implements CreativeUpdateInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'creative/update';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CreativeUpdate constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Update creative
     *
     * @param CreativeUpdateParam $creativeUpdateParam
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(CreativeUpdateParam $creativeUpdateParam, int $userId): HaapiResponse
    {
        if (config('haapi.stubs.creative.update')) {
            return app(HaapiResponse::class);
        }

        return $this->requestService->request(
            self::REQUEST_TYPE,
            $creativeUpdateParam->all(),
            $userId
        );
    }
}
