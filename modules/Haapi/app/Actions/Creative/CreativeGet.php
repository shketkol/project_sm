<?php

namespace Modules\Haapi\Actions\Creative;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\CampaignData;
use Modules\Daapi\DataTransferObjects\Types\CreativeData;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Creative\Contracts\CreativeGet as CreativeGetInterface;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class CreativeGet
 *
 * @package Modules\Haapi\Actions\Creative
 */
class CreativeGet extends BaseAction implements CreativeGetInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'creative/get';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CreativeGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param string $creativeId
     * @param int    $userId
     *
     * @return DataTransferObject
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(string $creativeId, int $userId): DataTransferObject
    {
        $response = $this->requestService->request(
            self::REQUEST_TYPE,
            compact(['creativeId']),
            $userId
        );

        return new CreativeData(Arr::get($response->getPayload(), 'creative', []));
    }
}
