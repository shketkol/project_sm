<?php

namespace Modules\Haapi\Actions\Creative;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Creative\Contracts\CreativeSearch as CreativeSearchInterface;
use Modules\Haapi\DataTransferObjects\Creative\CreativeSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

/**
 * Class CreativeSearch
 *
 * @package Modules\Haapi\Actions\Creative
 */
class CreativeSearch extends BaseAction implements CreativeSearchInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'creative/search';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CreativeSearch constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Search creative
     *
     * @param CreativeSearchParam $creativeSearchParam
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(CreativeSearchParam $creativeSearchParam, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $creativeSearchParam->all(),
            $userId
        );
    }
}
