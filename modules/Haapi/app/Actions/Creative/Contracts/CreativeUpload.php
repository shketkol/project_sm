<?php

namespace Modules\Haapi\Actions\Creative\Contracts;

use Modules\Haapi\DataTransferObjects\Creative\CreativeUploadParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

/**
 * Interface CreativeUpload
 *
 * @package Modules\Haapi\Actions\Creative\Contracts
 */
interface CreativeUpload
{
    /**
     * Upload creative
     *
     * @param CreativeUploadParam $creativeUploadParam
     * @param int $userId
     * @return HaapiResponse
     */
    public function handle(CreativeUploadParam $creativeUploadParam, int $userId): HaapiResponse;
}
