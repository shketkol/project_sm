<?php

namespace Modules\Haapi\Actions\Creative\Contracts;

use Modules\Haapi\DataTransferObjects\Creative\CreativeSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

/**
 * Interface CreativeSearch
 *
 * @package Modules\Haapi\Actions\Creative\Contracts
 */
interface CreativeSearch
{
    /**
     * Search creative
     *
     * @param CreativeSearchParam $creativeSearchParam
     * @param int $userId
     * @return HaapiResponse
     */
    public function handle(CreativeSearchParam $creativeSearchParam, int $userId): HaapiResponse;
}
