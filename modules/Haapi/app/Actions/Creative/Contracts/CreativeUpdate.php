<?php

namespace Modules\Haapi\Actions\Creative\Contracts;

use Modules\Haapi\DataTransferObjects\Creative\CreativeUpdateParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

/**
 * Interface CreativeUpdate
 *
 * @package Modules\Haapi\Actions\Creative\Contracts
 */
interface CreativeUpdate
{
    /**
     * Update creative
     *
     * @param CreativeUpdateParam $creativeUpdateParam
     * @param int $userId
     * @return HaapiResponse
     */
    public function handle(CreativeUpdateParam $creativeUpdateParam, int $userId): HaapiResponse;
}
