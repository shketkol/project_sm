<?php

namespace Modules\Haapi\Actions\Creative\Contracts;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Interface CreativeGet
 *
 * @package Modules\Haapi\Actions\Creative\Contracts
 */
interface CreativeGet
{
    /**
     * @param string $creativeId
     * @param int $userId
     *
     * @return DataTransferObject
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(string $creativeId, int $userId): DataTransferObject;
}
