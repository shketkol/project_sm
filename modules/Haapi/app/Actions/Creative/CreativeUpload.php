<?php

namespace Modules\Haapi\Actions\Creative;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Creative\Contracts\CreativeUpload as CreativeUploadInterface;
use Modules\Haapi\DataTransferObjects\Creative\CreativeUploadParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

/**
 * Class CreativeUpload
 *
 * @package Modules\Haapi\Actions\Creative
 */
class CreativeUpload extends BaseAction implements CreativeUploadInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'creative/update';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CreativeUpload constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Upload creative
     *
     * @param CreativeUploadParam $creativeUploadParam
     * @param int                 $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(CreativeUploadParam $creativeUploadParam, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $creativeUploadParam->all(),
            $userId
        );
    }
}
