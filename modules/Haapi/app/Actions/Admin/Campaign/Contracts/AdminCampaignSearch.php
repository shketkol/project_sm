<?php

namespace Modules\Haapi\Actions\Admin\Campaign\Contracts;

use Modules\Haapi\DataTransferObjects\Campaign\AdminCampaignSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface AdminCampaignSearch
{
    /**
     * @param AdminCampaignSearchParam $adminCampaignSearchParam
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(AdminCampaignSearchParam $adminCampaignSearchParam, int $userId): HaapiResponse;
}
