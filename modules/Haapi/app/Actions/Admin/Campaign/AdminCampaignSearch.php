<?php

namespace Modules\Haapi\Actions\Admin\Campaign;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Admin\Campaign\Contracts\AdminCampaignSearch as AdminCampaignSearchInterface;
use Modules\Haapi\DataTransferObjects\Campaign\AdminCampaignSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class AdminCampaignSearch extends BaseAction implements AdminCampaignSearchInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'campaign/search';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * AdminCampaignSearch constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param AdminCampaignSearchParam $adminCampaignSearchParam
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(AdminCampaignSearchParam $adminCampaignSearchParam, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $adminCampaignSearchParam->all(),
            $userId
        );
    }
}
