<?php

namespace Modules\Haapi\Actions\Admin\Creative;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Admin\Creative\Contracts\AdminCreativeSearch as AdminCreativeSearchInterface;
use Modules\Haapi\DataTransferObjects\Creative\AdminCreativeSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class AdminCreativeSearch extends BaseAction implements AdminCreativeSearchInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'creative/search';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * AdminCreativeSearch constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param AdminCreativeSearchParam $adminCreativeSearchParam
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(AdminCreativeSearchParam $adminCreativeSearchParam, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $adminCreativeSearchParam->all(),
            $userId
        );
    }
}
