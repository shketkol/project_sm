<?php

namespace Modules\Haapi\Actions\Admin\Creative\Contracts;

use Modules\Haapi\DataTransferObjects\Creative\AdminCreativeSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface AdminCreativeSearch
{
    /**
     * @param AdminCreativeSearchParam $adminCreativeSearchParam
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(AdminCreativeSearchParam $adminCreativeSearchParam, int $userId): HaapiResponse;
}
