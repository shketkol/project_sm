<?php

namespace Modules\Haapi\Actions\Admin\User;

use Modules\Daapi\DataTransferObjects\Types\UserDataPlain;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Services\Contracts\RequestService;

/**
 * user/get endpoint usage for admin users.
 *
 * @package Modules\Haapi\Actions\User
 */
class SelfDataGet extends BaseAction
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'user/get';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * UserGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Get user
     *
     * @param int $userId
     *
     * @return UserDataPlain
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(int $userId): UserDataPlain
    {
        $response = $this->requestService->request(self::REQUEST_TYPE, [], $userId);

        return new UserDataPlain($response->getPayload());
    }

    /**
     * @param string $token
     * @return UserDataPlain
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handleByToken(string $token): UserDataPlain
    {
        return new UserDataPlain(
            $this->requestService
                ->requestWithSessionToken(self::REQUEST_TYPE, $token)
                ->getPayload()
        );
    }
}
