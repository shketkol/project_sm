<?php

namespace Modules\Haapi\Actions\Admin\User;

use Illuminate\Contracts\Auth\Guard;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Haapi\Actions\Admin\User\Contracts\AdminUserGet as AdminUserGetInterface;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Traits\CreateUserData;
use Modules\Haapi\Events\HaapiAccountRetrieved;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\User\Models\User;

class AdminUserGet extends BaseAction implements AdminUserGetInterface
{
    use CreateUserData;

    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'user/get_by_accountId';

    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @param Guard          $guard
     * @param RequestService $requestService
     */
    public function __construct(Guard $guard, RequestService $requestService)
    {
        parent::__construct();

        $this->guard = $guard;
        $this->requestService = $requestService;
    }

    /**
     * Get user details for admin pages.
     *
     * @param User     $user
     * @param int|null $userId
     *
     * @return UserData
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     */
    public function handle(User $user, ?int $userId = null): UserData
    {
        $response = $this->requestService->request(
            self::REQUEST_TYPE,
            ['accountId' => $user->account_external_id],
            $userId ?: $this->guard->id()
        );

        event(new HaapiAccountRetrieved($response));

        return $this->getUserData($response, $user->id);
    }
}
