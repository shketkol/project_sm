<?php

namespace Modules\Haapi\Actions\Admin\User;

use Illuminate\Support\Arr;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Services\Contracts\RequestService;

/**
 * @package Modules\Haapi\Actions\User
 */
class AdminClientAuthorize extends BaseAction
{
    /**
     * Endpoint type
     */
    private const REQUEST_TYPE = 'client/authorize';

    /**
     * Request service
     *
     * @var RequestService
     */
    private $requestService;

    /**
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Sign in an admin
     *
     * @param string $token
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(string $token): array
    {
        $response = $this->requestService->request(
            self::REQUEST_TYPE,
            [
                'clientId'    => config('cognito.admin.login_params.client_id'),
                'code'        => $token,
                'redirectUri' => config('cognito.admin.login_params.redirect_uri'),
            ]
        );

        return $this->getTokensFromPayload($response->getPayload());
    }

    /**
     * @param array $payload
     *
     * @return array
     */
    private function getTokensFromPayload(array $payload): array
    {
        return [
            'token'        => Arr::get($payload, 'accessToken', ''),
            'refreshToken' => Arr::get($payload, 'refreshToken', ''),
        ];
    }
}
