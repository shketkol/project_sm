<?php

namespace Modules\Haapi\Actions\Admin\User\Contracts;

use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\User\Models\User;

interface AdminUserGet
{
    /**
     * Get user details for admin pages.
     *
     * @param User     $user
     * @param int|null $userId
     *
     * @return UserData
     */
    public function handle(User $user, ?int $userId = null): UserData;
}
