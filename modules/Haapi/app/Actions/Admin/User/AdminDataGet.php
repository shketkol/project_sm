<?php

namespace Modules\Haapi\Actions\Admin\User;

use Modules\Daapi\DataTransferObjects\Types\UserDataPlain;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Services\Contracts\RequestService;

/**
 * user/get endpoint to fetch admin data by admin.
 *
 * @package Modules\Haapi\Actions\User
 */
class AdminDataGet extends BaseAction
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'user/get';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * UserGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Get admin
     *
     * @param int $userId
     * @param string $externalId
     * @return UserDataPlain
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(int $userId, string $externalId): UserDataPlain
    {
        $response = $this->requestService->request(self::REQUEST_TYPE, [
            'userId' => $externalId
        ], $userId);

        return new UserDataPlain($response->getPayload());
    }
}
