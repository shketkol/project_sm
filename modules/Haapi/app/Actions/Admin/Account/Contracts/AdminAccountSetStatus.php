<?php

namespace Modules\Haapi\Actions\Admin\Account\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface AdminAccountSetStatus
{
    /**
     * @param string $accountId
     * @param bool   $isActive
     * @param int    $userId
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(string $accountId, bool $isActive, int $userId): HaapiResponse;
}
