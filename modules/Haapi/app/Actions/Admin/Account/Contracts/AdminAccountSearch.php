<?php

namespace Modules\Haapi\Actions\Admin\Account\Contracts;

use Modules\Haapi\DataTransferObjects\Account\AdminAccountSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface AdminAccountSearch
{
    /**
     * @param AdminAccountSearchParam $adminAccountSearchParam
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(AdminAccountSearchParam $adminAccountSearchParam, int $userId): HaapiResponse;
}
