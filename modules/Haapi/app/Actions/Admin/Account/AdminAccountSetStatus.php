<?php

namespace Modules\Haapi\Actions\Admin\Account;

use Modules\Haapi\Actions\Admin\Account\Contracts\AdminAccountSetStatus as AdminAccountSetStatusInterface;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class AdminAccountSetStatus extends BaseAction implements AdminAccountSetStatusInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'account/set_status';

    /**
     * Status Type Active
     */
    const ACTIVE = 'ACTIVE';

    /**
     * Status Type Inactive
     */
    const INACTIVE = 'INACTIVE';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * AdminAccountSetStatus constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param string $accountId
     * @param bool   $isActive
     * @param int    $userId
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(string $accountId, bool $isActive, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            [
                'accountId' => $accountId,
                'status' => $this->getStatus($isActive)
            ],
            $userId
        );
    }

    /**
     * @param bool $isActive
     *
     * @return string
     */
    private function getStatus(bool $isActive): string
    {
        return $isActive ? self::ACTIVE : self::INACTIVE;
    }
}
