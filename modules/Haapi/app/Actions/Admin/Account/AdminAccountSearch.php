<?php

namespace Modules\Haapi\Actions\Admin\Account;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Admin\Account\Contracts\AdminAccountSearch as AdminAccountSearchInterface;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\Haapi\DataTransferObjects\Account\AdminAccountSearchParam;

class AdminAccountSearch extends BaseAction implements AdminAccountSearchInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'account/search';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * AdminAccountSearch constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param AdminAccountSearchParam $adminAccountSearchParam
     * @param int                     $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(AdminAccountSearchParam $adminAccountSearchParam, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $adminAccountSearchParam->all(),
            $userId
        );
    }
}
