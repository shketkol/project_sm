<?php

namespace Modules\Haapi\Actions\Campaign;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Campaign\Contracts\TargetingValuesList as TargetingValuesListInterface;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class TargetingValuesList extends BaseAction implements TargetingValuesListInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'target/value/list';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * TargetingValuesList constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param string $targetTypeId
     * @param int    $limit
     * @param int    $start
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(string $targetTypeId, int $limit = 1, int $start = 0): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            compact(['targetTypeId', 'limit', 'start'])
        );
    }
}
