<?php

namespace Modules\Haapi\Actions\Campaign;

use App\Helpers\SessionIdentifierHelper;
use Modules\Haapi\Actions\BaseCallbackAction;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignCreate as CampaignCreateInterface;
use Modules\Haapi\DataTransferObjects\Campaign\CampaignParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class CampaignCreate extends BaseCallbackAction implements CampaignCreateInterface
{
    /**
     * Endpoint type
     */
    private const REQUEST_TYPE = 'campaign/create';

    /**
     * Session identifier key
     */
    protected const IDENTIFIER_KEY = 'campaign.sessionIdentifier';

    /**
     * @var RequestService
     */
    private $requestService;


    /**
     * @param SessionIdentifierHelper $identifierHelper
     * @param RequestService          $requestService
     */
    public function __construct(SessionIdentifierHelper $identifierHelper, RequestService $requestService)
    {
        parent::__construct($identifierHelper);
        $this->requestService = $requestService;
    }

    /**
     * @param CampaignParams $campaignParams
     * @param int            $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(CampaignParams $campaignParams, int $userId): HaapiResponse
    {
        $payload = $this->getPayloadWithSessionIdentifier($campaignParams->all());

        return $this->requestService->request(
            self::REQUEST_TYPE,
            $payload,
            $userId
        );
    }
}
