<?php

namespace Modules\Haapi\Actions\Campaign;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignPause as CampaignPauseInterface;

class CampaignPause extends BaseAction implements CampaignPauseInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'campaign/pause';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CampaignPause constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param string $campaignId
     * @param int    $userId
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(string $campaignId, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            compact(['campaignId']),
            $userId
        );
    }
}
