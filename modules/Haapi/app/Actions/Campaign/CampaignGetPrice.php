<?php

namespace Modules\Haapi\Actions\Campaign;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\DataTransferObjects\Campaign\CampaignPriceParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignGetPrice as CampaignGetPriceInterface;

class CampaignGetPrice extends BaseAction implements CampaignGetPriceInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'campaign/price';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CampaignInventoryCheck constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param CampaignPriceParams $campaignPriceParams
     * @param int                 $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(CampaignPriceParams $campaignPriceParams, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $campaignPriceParams->all(),
            $userId
        );
    }
}
