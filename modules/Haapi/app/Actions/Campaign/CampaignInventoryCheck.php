<?php

namespace Modules\Haapi\Actions\Campaign;

use Illuminate\Support\Arr;
use Modules\Campaign\Models\InventoryCheckStatus;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\DataTransferObjects\Campaign\InventoryCheckParams;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignInventoryCheck as CampaignInventoryCheckInterface;

class CampaignInventoryCheck extends BaseAction implements CampaignInventoryCheckInterface
{
    /** Types */
    public const HAAPI_TYPE_SUCCESS = InventoryCheckStatus::AVAILABLE;
    public const HAAPI_TYPE_YIELDEX_DOWN = InventoryCheckStatus::YX_CONNECTION_FAILED;
    public const HAAPI_TYPE_WARNING = InventoryCheckStatus::UNAVAILABLE;
    public const HAAPI_TYPE_DANGER_BUDGET = InventoryCheckStatus::AVAILABILITY_UNDER_MIN_SPEND;
    public const HAAPI_TYPE_DANGER_SCHEDULE = InventoryCheckStatus::REQUEST_UNDER_MIN_PER_DAY;

    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'campaign/inventorycheck';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @var HaapiResponse
     */
    private $response;

    /**
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param InventoryCheckParams $inventoryCheckParams
     * @param int                  $userId
     *
     * @return HaapiResponse
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(InventoryCheckParams $inventoryCheckParams, int $userId): HaapiResponse
    {
        $this->response = $this->requestService->request(
            self::REQUEST_TYPE,
            $inventoryCheckParams->all(),
            $userId
        );

        return $this->response;
    }

    /**
     * @return string
     * @throws HaapiException
     */
    public function getType(): string
    {
        $statusCode = $this->response->get('statusCode');

        $type = Arr::where(config('inventory-check.statuses'), function ($item) use ($statusCode) {
            return in_array($statusCode, $item);
        });

        if (empty($type)) {
            throw InternalErrorException::create("Not valid HAAPI inventory check status: {$statusCode}");
        }

        return Arr::first(array_keys($type));
    }
}
