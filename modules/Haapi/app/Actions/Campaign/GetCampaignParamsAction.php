<?php

namespace Modules\Haapi\Actions\Campaign;

use Modules\Campaign\Models\Campaign;
use Modules\Creative\Models\Creative;
use Modules\Haapi\DataTransferObjects\Campaign\LineItemParams;
use Modules\Targeting\Actions\GetCampaignTargetingsAction;
use Modules\Targeting\Exceptions\TypeNotFoundException;

class GetCampaignParamsAction
{
    /**
     * @var GetCampaignTargetingsAction
     */
    protected $getTargetingsAction;

    /**
     * @param GetCampaignTargetingsAction $getTargetingsAction
     */
    public function __construct(GetCampaignTargetingsAction $getTargetingsAction)
    {
        $this->getTargetingsAction = $getTargetingsAction;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     */
    public function handle(Campaign $campaign): array
    {
        return [
            'sourceId'      => $campaign->id,
            'externalId'    => $campaign->external_id,
            'name'          => $campaign->name,
            'advertiserId'  => $campaign->user->account_external_id,
            'displayBudget' => $campaign->budget,
            'lineItems'     => $this->getLineItemParams($campaign),
            'promoCodeName' => optional($campaign->promocode)->code,
        ];
    }

    /**
     * @param Campaign $campaign
     *
     * @return LineItemParams[]
     * @throws TypeNotFoundException
     */
    protected function getLineItemParams(Campaign $campaign): array
    {
        $params = [
            'sourceId'  => $campaign->id,
            'startDate' => optional($campaign->startDateWithTimezone)->format(config('date.format.haapi')),
            'endDate'   => optional($campaign->endDateWithTimezone)->format(config('date.format.haapi')),
            'unitCost'  => $campaign->cpm,
            'quantity'  => $campaign->impressions,
            'targets'   => $this->getTargetingsAction->handle($campaign),
            'creative'  => $this->getCreativeData(
                $campaign->getActiveCreative(),
                $campaign->user->account_external_id
            ),
        ];

        if ($campaign->promocode) {
            $params['discountedUnitCost'] = $campaign->discounted_cpm;
        }

        return [new LineItemParams($params)];
    }

    /**
     * @param Creative|null $creative
     * @param string        $accountId
     *
     * @return array|null
     */
    private function getCreativeData(?Creative $creative, string $accountId): ?array
    {
        if (!$creative) {
            return null;
        }

        if ($creative->external_id) {
            return [
                'creativeId' => $creative->external_id,
            ];
        }

        return [
            'creativeCreation' => [
                'fileName'      => $creative->name . '.' . $creative->extension,
                'sourceUrl'     => $creative->source_url,
                'contentLength' => (int)$creative->filesize,
                'accountId'     => $accountId,
            ],
        ];
    }
}
