<?php

namespace Modules\Haapi\Actions\Campaign;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\Haapi\Actions\Campaign\Contracts\ReportAccountImpressions as ReportAccountImpressionsInterface;

class ReportAccountImpressions extends BaseAction implements ReportAccountImpressionsInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'report/accountImpressions';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * ReportAccountImpressions constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param string $accountId
     * @param int    $userId
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse|int
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(string $accountId, int $userId)
    {
        $haapiResponse = $this->requestService->request(
            self::REQUEST_TYPE,
            compact(['accountId']),
            $userId
        );
        $impressions = $haapiResponse->get('impressions');

        return isset($impressions) ? $impressions : $haapiResponse;
    }
}
