<?php

namespace Modules\Haapi\Actions\Campaign;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsMetricsParams;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Haapi\Services\Contracts\RequestService;

class CampaignImpressionsMetrics extends BaseAction
{
    /**
     * Endpoint type
     */
    private const REQUEST_TYPE = 'campaign/impressions/metrics';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CampaignImpressionsDetail constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param ImpressionsMetricsParams $params
     * @param int                      $userId
     *
     * @return array
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    public function handle(ImpressionsMetricsParams $params, int $userId): array
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $params->all(),
            $userId
        )->getPayload();
    }
}
