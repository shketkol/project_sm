<?php

namespace Modules\Haapi\Actions\Campaign;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\Haapi\Actions\Campaign\Contracts\TargetingTypeList as TargetingTypeListInterface;

class TargetingTypeList extends BaseAction implements TargetingTypeListInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'target/type/list';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * TargetingTypeList constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param int $limit
     * @param int $start
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(int $limit = 1, int $start = 0): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            compact(['limit', 'start'])
        );
    }
}
