<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface CampaignResume
{
    /**
     * @param string $campaignId
     * @param int    $userId
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(string $campaignId, int $userId): HaapiResponse;
}
