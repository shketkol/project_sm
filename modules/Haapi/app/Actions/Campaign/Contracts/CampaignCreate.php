<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

use Modules\Haapi\DataTransferObjects\Campaign\CampaignParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface CampaignCreate
{
    /**
     * @param CampaignParams $campaignParams
     * @param int            $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(CampaignParams $campaignParams, int $userId): HaapiResponse;
}
