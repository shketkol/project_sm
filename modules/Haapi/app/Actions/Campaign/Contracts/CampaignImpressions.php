<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;

interface CampaignImpressions
{
    /**
     * @param array $campaignIds
     * @param int   $userId
     *
     * @return int
     * @throws GuzzleException
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    public function handle(array $campaignIds, int $userId): int;
}
