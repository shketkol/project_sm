<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface TargetingValuesList
{
    /**
     * @param string $targetTypeId
     * @param int    $limit
     * @param int    $start
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(string $targetTypeId, int $limit, int $start): HaapiResponse;
}
