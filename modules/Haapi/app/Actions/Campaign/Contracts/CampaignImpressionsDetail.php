<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsDetailParams;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface CampaignImpressionsDetail
{
    /**
     * @param ImpressionsDetailParams $detailParams
     * @param int                     $userId
     *
     * @return HaapiResponse
     * @throws GuzzleException
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    public function handle(ImpressionsDetailParams $detailParams, int $userId): array;
}
