<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface TargetingTypeList
{
    /**
     * @param int $limit
     * @param int $start
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(int $limit, int $start): HaapiResponse;
}
