<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

use Modules\Haapi\DataTransferObjects\Campaign\CampaignPriceParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface CampaignGetPrice
{
    /**
     * @param CampaignPriceParams $campaignPriceParams
     * @param int                 $userId
     *
     * @return HaapiResponse
     */
    public function handle(CampaignPriceParams $campaignPriceParams, int $userId): HaapiResponse;
}
