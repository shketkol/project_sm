<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

use Modules\Daapi\DataTransferObjects\Types\CampaignData;

interface CampaignGet
{
    /**
     * @param string $campaignId
     * @param int $userId
     *
     * @return CampaignData
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(string $campaignId, int $userId): CampaignData;
}
