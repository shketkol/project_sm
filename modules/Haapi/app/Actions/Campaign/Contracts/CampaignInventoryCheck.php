<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

use Modules\Haapi\DataTransferObjects\Campaign\InventoryCheckParams;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface CampaignInventoryCheck
{
    /**
     * @param InventoryCheckParams $inventoryCheckParams
     * @param int                  $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(InventoryCheckParams $inventoryCheckParams, int $userId): HaapiResponse;

    /**
     * @return string
     * @throws HaapiException
     * @throws InternalErrorException
     */
    public function getType(): string;
}
