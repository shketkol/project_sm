<?php

namespace Modules\Haapi\Actions\Campaign\Contracts;

interface ReportAccountImpressions
{
    /**
     * @param string $accountId
     * @param int    $userId
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse|int
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(string $accountId, int $userId);
}
