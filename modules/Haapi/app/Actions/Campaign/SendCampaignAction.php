<?php

namespace Modules\Haapi\Actions\Campaign;

use Modules\Campaign\Models\Campaign;
use Modules\Haapi\DataTransferObjects\Campaign\CampaignParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

class SendCampaignAction
{
    /**
     * @var CampaignCreate
     */
    protected $campaignCreateAction;

    /**
     * @var GetCampaignParamsAction
     */
    protected $campaignParamsAction;

    /**
     * @param CampaignCreate          $campaignCreateAction
     * @param GetCampaignParamsAction $campaignParamsAction
     */
    public function __construct(CampaignCreate $campaignCreateAction, GetCampaignParamsAction $campaignParamsAction)
    {
        $this->campaignCreateAction = $campaignCreateAction;
        $this->campaignParamsAction = $campaignParamsAction;
    }

    /**
     * @param Campaign $campaign
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function handle(Campaign $campaign): ?HaapiResponse
    {
        $params = new CampaignParams($this->campaignParamsAction->handle($campaign));
        return $this->campaignCreateAction->handle($params, $campaign->user_id);
    }
}
