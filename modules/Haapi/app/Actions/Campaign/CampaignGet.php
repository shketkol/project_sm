<?php

namespace Modules\Haapi\Actions\Campaign;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\CampaignData;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignGet as CampaignGetInterface;

class CampaignGet extends BaseAction implements CampaignGetInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'campaign/get';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CampaignGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param string $campaignId
     * @param int    $userId
     *
     * @return CampaignData
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(string $campaignId, int $userId): CampaignData
    {
        $response = $this->requestService->request(
            self::REQUEST_TYPE,
            compact(['campaignId']),
            $userId
        );

        return new CampaignData(Arr::get($response->getPayload(), 'campaign', []));
    }
}
