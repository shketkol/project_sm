<?php

namespace Modules\Haapi\Actions\Campaign;

use Modules\Campaign\Models\Campaign;
use Modules\Haapi\DataTransferObjects\Campaign\UpdateLineItemParams;
use Modules\Targeting\Actions\GetCampaignTargetingsAction;
use Modules\Targeting\Exceptions\TypeNotFoundException;

class GetCampaignUpdateParamsAction
{
    /**
     * @var GetCampaignTargetingsAction
     */
    private $getTargetingsAction;

    /**
     * @param GetCampaignTargetingsAction $getTargetingsAction
     */
    public function __construct(GetCampaignTargetingsAction $getTargetingsAction)
    {
        $this->getTargetingsAction = $getTargetingsAction;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     */
    public function handle(Campaign $campaign): array
    {
        return [
            'id'            => $campaign->external_id,
            'name'          => $campaign->name,
            'displayBudget' => $campaign->budget,
            'lineItems'     => $this->getLineItemParams($campaign),
        ];
    }

    /**
     * @param Campaign $campaign
     *
     * @return UpdateLineItemParams[]
     * @throws TypeNotFoundException
     */
    protected function getLineItemParams(Campaign $campaign): array
    {
        $params = [
            'id'        => $campaign->external_id,
            'startDate' => optional($campaign->startDateWithTimezone)->format(config('date.format.haapi')),
            'endDate'   => optional($campaign->endDateWithTimezone)->format(config('date.format.haapi')),
            'unitCost'  => $campaign->cpm,
            'quantity'  => $campaign->impressions,
            'targets'   => $this->getTargetingsAction->handle($campaign),
        ];

        return [new UpdateLineItemParams($params)];
    }
}
