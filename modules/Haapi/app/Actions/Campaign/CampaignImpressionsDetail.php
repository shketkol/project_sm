<?php

namespace Modules\Haapi\Actions\Campaign;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignImpressionsDetail as CampaignImpressionsDetailInterface;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsDetailParams;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class CampaignImpressionsDetail extends BaseAction implements CampaignImpressionsDetailInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'campaign/impressions/detail';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CampaignImpressionsDetail constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param ImpressionsDetailParams $detailParams
     * @param int $userId
     *
     * @return HaapiResponse
     * @throws GuzzleException
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    public function handle(ImpressionsDetailParams $detailParams, int $userId): array
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $detailParams->all(),
            $userId
        )->getPayload();
    }
}
