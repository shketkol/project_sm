<?php

namespace Modules\Haapi\Actions\Campaign;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignFrequencyDetail as CampaignFrequencyDetailInterface;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Haapi\Services\Contracts\RequestService;

class CampaignFrequencyDetail extends BaseAction implements CampaignFrequencyDetailInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'campaign/frequency/metrics';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param string $campaignId
     * @param int    $userId
     *
     * @return array
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    public function handle(string $campaignId, int $userId): array
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            compact(['campaignId']),
            $userId
        )->getPayload();
    }
}
