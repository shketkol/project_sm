<?php

namespace Modules\Haapi\Actions\Campaign;

use App\Exceptions\InvalidArgumentException;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\GetCampaignPriceAction;
use Modules\Campaign\Exceptions\CancelCampaignActivityRequestException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Haapi\DataTransferObjects\Campaign\LineItemParams;
use Modules\Haapi\Mappers\Campaign\ActionMapper;
use Modules\Targeting\Actions\GetCampaignTargetingsAction;
use Modules\Targeting\Exceptions\TypeNotFoundException;
use Psr\Log\LoggerInterface;

class GetDraftActivityCampaignParamsAction extends GetCampaignParamsAction
{
    /**
     * @var GetCampaignPriceAction
     */
    private $getCampaignPriceAction;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param GetCampaignTargetingsAction $getTargetingsAction
     * @param GetCampaignPriceAction      $getCampaignPriceAction
     * @param LoggerInterface             $log
     */
    public function __construct(
        GetCampaignTargetingsAction $getTargetingsAction,
        GetCampaignPriceAction $getCampaignPriceAction,
        LoggerInterface $log
    ) {
        parent::__construct($getTargetingsAction);
        $this->getCampaignPriceAction = $getCampaignPriceAction;
        $this->log = $log;
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return array
     * @throws TypeNotFoundException
     * @throws CancelCampaignActivityRequestException
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function handle(Campaign $campaign, array $data = []): array
    {
        $params = parent::handle($campaign);
        $params = $this->setLatestAction($campaign, $data, $params);

        Arr::set($params, 'pageLastVisited', Arr::get($data, 'last_page'));

        return $params;
    }

    /**
     * Action name equal to null would be possible when
     * advertiser open already filled campaign on review page and goes to payment page
     *
     * Set either creative or budget step action since it's hulu required
     *
     * @param Campaign $campaign
     * @param array    $data
     * @param array    $params
     *
     * @return array
     * @throws \App\Exceptions\InvalidArgumentException
     */
    private function setLatestAction(Campaign $campaign, array $data, array $params): array
    {
        $action = Arr::get($data, 'action_name');
        if (!is_null($action)) {
            Arr::set($params, 'latestAction', ActionMapper::map($action));
            return $params;
        }

        // set latest filled campaign step as latest action
        $this->log->info('[Campaign][Draft Activity] Wizard action is missing.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        if ($this->campaignHasCreative($campaign)) {
            return $this->setLatestActionAsCreativeAttached($campaign, $params);
        }

        if ($this->campaignHasBudget($campaign)) {
            return $this->setLatestActionAsBudget($campaign, $params);
        }

        throw new InvalidArgumentException(sprintf(
            'Could not find latest action for Campaign #%d since it has no creative and no budget.',
            $campaign->id
        ));
    }

    /**
     * @param Campaign $campaign
     * @param array    $params
     *
     * @return array
     * @throws InvalidArgumentException
     */
    private function setLatestActionAsCreativeAttached(Campaign $campaign, array $params): array
    {
        Arr::set($params, 'latestAction', ActionMapper::map(ActionMapper::CREATIVE_ATTACHED));

        $this->log->info('[Campaign][Draft Activity] Set creative attached as latest action.', [
            'campaign_id' => $campaign->id,
            'params'      => $params,
        ]);

        return $params;
    }

    /**
     * @param Campaign $campaign
     * @param array    $params
     *
     * @return array
     * @throws InvalidArgumentException
     */
    private function setLatestActionAsBudget(Campaign $campaign, array $params): array
    {
        Arr::set($params, 'latestAction', ActionMapper::map(CampaignStep::BUDGET));

        $this->log->info('[Campaign][Draft Activity] Set budget as latest action.', [
            'campaign_id' => $campaign->id,
            'params'      => $params,
        ]);

        return $params;
    }

    /**
     * @param Campaign $campaign
     *
     * @return LineItemParams[]
     * @throws CancelCampaignActivityRequestException
     * @throws TypeNotFoundException
     */
    protected function getLineItemParams(Campaign $campaign): array
    {
        if ($this->shouldCancelRequest($campaign)) {
            throw CancelCampaignActivityRequestException::create($campaign);
        }

        if ($this->shouldSendEmptyLineItems($campaign)) {
            return [];
        }

        $params = parent::getLineItemParams($campaign);
        $params = $this->setUnitCost($campaign, $params);

        return $params;
    }

    /**
     * @param Campaign         $campaign
     * @param LineItemParams[] $params
     *
     * @return array
     * @throws TypeNotFoundException
     */
    private function setUnitCost(Campaign $campaign, array $params): array
    {
        if (!Arr::has($params, '0')) {
            return $params;
        }

        // fetch cpm if campaign has no cpm saved but line items exists
        if ($params[0]->unitCost === 0.0) {
            $params[0]->unitCost = Arr::get($this->getCampaignPriceAction->handle($campaign), 'unitCost');
        }

        return $params;
    }

    /**
     * if campaign has no dates and has NO targetings and NO creative -> send empty line items
     *
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function shouldSendEmptyLineItems(Campaign $campaign): bool
    {
        return !$this->campaignHasDates($campaign)
            && !$this->campaignHasAnyTargetings($campaign)
            && !$this->campaignHasCreative($campaign);
    }

    /**
     * If campaign has no dates but has targetings or creative -> cancel activity request
     *
     * This is possible when:
     * 1. Advertiser fills dates in campaign
     * 2. Then fills next steps and then returns to the dates step and remove dates
     * 3. Then advertiser could move through wizard in the top bar navigation without any validation errors
     *
     * Or
     *
     * 1. Advertiser fills dates in campaign
     * 2. Exits wizard, goes to details page and uploads/attach creative
     * 3. Then clicks edit campaign and wizard would be open on creative step
     *
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function shouldCancelRequest(Campaign $campaign): bool
    {
        return !$this->campaignHasDates($campaign)
            && ($this->campaignHasAnyTargetings($campaign) || $this->campaignHasCreative($campaign));
    }

    /**
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function campaignHasDates(Campaign $campaign): bool
    {
        return $campaign->date_start && $campaign->date_end;
    }

    /**
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function campaignHasAnyTargetings(Campaign $campaign): bool
    {
        if ($campaign->targetingGenders()->exists()) {
            return true;
        }

        if ($campaign->targetingAgeGroups()->exists()) {
            return true;
        }

        if ($campaign->audiences()->exists()) {
            return true;
        }

        if ($campaign->locations()->exists()) {
            return true;
        }

        if ($campaign->genres()->exists()) {
            return true;
        }

        if ($campaign->deviceGroups()->exists()) {
            return true;
        }

        return false;
    }

    /**
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function campaignHasCreative(Campaign $campaign): bool
    {
        return !is_null($campaign->getActiveCreative());
    }

    /**
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function campaignHasBudget(Campaign $campaign): bool
    {
        return $campaign->budget !== 0.0;
    }
}
