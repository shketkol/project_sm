<?php

namespace Modules\Haapi\Actions\Campaign;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignImpressions as CampaignImpressionsInterface;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Haapi\Services\Contracts\RequestService;
use Illuminate\Cache\Repository;

class CampaignImpressions extends BaseAction implements CampaignImpressionsInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'campaign/impressions';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * CampaignImpressions constructor.
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Get impressions for the campaigns.
     *
     * @param array $campaignIds
     * @param int $userId
     * @return int
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws GuzzleException
     */
    public function handle(array $campaignIds, int $userId): int
    {
        $response = $this->requestService->request(
            self::REQUEST_TYPE,
            ['campaignId' => $campaignIds],
            $userId
        );

        return $response->get('impressions');
    }
}
