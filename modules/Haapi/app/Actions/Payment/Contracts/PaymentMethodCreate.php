<?php

namespace Modules\Haapi\Actions\Payment\Contracts;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\DataTransferObjects\Payment\PaymentMethodCreateParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface PaymentMethodCreate
{
    /**
     * @param PaymentMethodCreateParams $orderGetParams
     * @param int            $userId
     *
     * @return HaapiResponse
     * @throws GuzzleException
     */
    public function handle(PaymentMethodCreateParams $orderGetParams, int $userId): HaapiResponse;
}
