<?php

namespace Modules\Haapi\Actions\Payment\Contracts;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface OrderGet
{
    /**
     * @param OrderGetParams $orderGetParams
     * @param int            $userId
     *
     * @return HaapiResponse
     * @throws GuzzleException
     */
    public function handle(OrderGetParams $orderGetParams, int $userId): HaapiResponse;
}
