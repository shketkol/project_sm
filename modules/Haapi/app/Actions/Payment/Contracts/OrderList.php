<?php

namespace Modules\Haapi\Actions\Payment\Contracts;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\DataTransferObjects\Payment\OrderListParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface OrderList
{
    /**
     * @param OrderListParams $orderListParams
     * @param int             $userId
     *
     * @return HaapiResponse
     * @throws GuzzleException
     */
    public function handle(OrderListParams $orderListParams, int $userId): HaapiResponse;
}
