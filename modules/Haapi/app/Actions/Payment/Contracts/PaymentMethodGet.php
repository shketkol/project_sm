<?php

namespace Modules\Haapi\Actions\Payment\Contracts;

use GuzzleHttp\Exception\GuzzleException;

interface PaymentMethodGet
{
    /**
     * @param string $paymentMethodId
     * @param int    $userId
     *
     * @return array
     * @throws GuzzleException
     */
    public function handle(string $paymentMethodId, int $userId): array;
}
