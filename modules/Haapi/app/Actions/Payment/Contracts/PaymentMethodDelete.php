<?php

namespace Modules\Haapi\Actions\Payment\Contracts;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\DataTransferObjects\Payment\PaymentMethodDeleteParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

interface PaymentMethodDelete
{
    /**
     * @param PaymentMethodDeleteParams $deleteParams
     * @param int                       $userId
     *
     * @return HaapiResponse
     * @throws GuzzleException
     */
    public function handle(PaymentMethodDeleteParams $deleteParams, int $userId): HaapiResponse;
}
