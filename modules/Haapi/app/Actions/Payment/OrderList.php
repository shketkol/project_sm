<?php

namespace Modules\Haapi\Actions\Payment;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Payment\Contracts\OrderList as OrderListInterface;
use Modules\Haapi\DataTransferObjects\Payment\OrderListParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class OrderList extends BaseAction implements OrderListInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'orders/get';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * OrderList constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param OrderListParams $orderListParams
     * @param int             $userId
     *
     * @return HaapiResponse
     * @throws GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(OrderListParams $orderListParams, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $orderListParams->all(),
            $userId
        );
    }
}
