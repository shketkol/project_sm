<?php

namespace Modules\Haapi\Actions\Payment;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetBillingDateData;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetBillingDateParams;
use Modules\Haapi\Services\Contracts\RequestService;

class OrderGetBillingDate extends BaseAction
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'order/get_billing_date';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param OrderGetBillingDateParams $orderGetBillingDateParams
     * @param int                       $userId
     *
     * @return OrderGetBillingDateData
     * @throws GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(OrderGetBillingDateParams $orderGetBillingDateParams, int $userId): OrderGetBillingDateData
    {
        $response =  $this->requestService->request(
            self::REQUEST_TYPE,
            $orderGetBillingDateParams->all(),
            $userId
        );

        return new OrderGetBillingDateData($response->getPayload());
    }
}
