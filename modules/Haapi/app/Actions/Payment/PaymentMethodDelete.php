<?php

namespace Modules\Haapi\Actions\Payment;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodDelete as PaymentMethodDeleteInterface;
use Modules\Haapi\DataTransferObjects\Payment\PaymentMethodDeleteParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class PaymentMethodDelete extends BaseAction implements PaymentMethodDeleteInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'payment_method/delete';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * OrderGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param PaymentMethodDeleteParams $deleteParams
     * @param int $userId
     * @return HaapiResponse
     * @throws GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(PaymentMethodDeleteParams $deleteParams, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $deleteParams->all(),
            $userId
        );
    }
}
