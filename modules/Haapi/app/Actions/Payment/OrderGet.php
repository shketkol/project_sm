<?php

namespace Modules\Haapi\Actions\Payment;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Payment\Contracts\OrderGet as OrderGetInterface;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class OrderGet extends BaseAction implements OrderGetInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'order/get';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * OrderGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param OrderGetParams $orderGetParams
     * @param int            $userId
     *
     * @return HaapiResponse
     * @throws GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(OrderGetParams $orderGetParams, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $orderGetParams->all(),
            $userId
        );
    }
}
