<?php

namespace Modules\Haapi\Actions\Payment;

use Illuminate\Cache\Repository;
use Illuminate\Support\Arr;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodGet as PaymentMethodGetInterface;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;

class PaymentMethodGet extends BaseAction implements PaymentMethodGetInterface
{

    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'payment_method/get';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @var Repository
     */
    private $cache;

    /**
     * OrderGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService, Repository $cache)
    {
        parent::__construct();
        $this->requestService = $requestService;
        $this->cache = $cache;
    }

    /**
     * @param string $paymentMethodId
     * @param int    $userId
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(string $paymentMethodId, int $userId): array
    {
        return Arr::get($this->fetch($paymentMethodId, $userId), 'card');
    }

    /**
     * @param string $paymentMethodId
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function fetch(string $paymentMethodId, int $userId): array
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            ['paymentMethodId' => $paymentMethodId],
            $userId
        )->getPayload();
    }
}
