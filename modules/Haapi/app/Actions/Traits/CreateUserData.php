<?php

namespace Modules\Haapi\Actions\Traits;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Daapi\DataTransferObjects\Types\UserDataWithResetToken;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

trait CreateUserData
{
    /**
     * @param HaapiResponse $response
     * @param int           $userId
     *
     * @return UserData
     */
    protected function getUserData(HaapiResponse $response, int $userId): UserData
    {
        $payload = $response->getPayload();
        $accountIdKey = Arr::has($payload, 'account.externalId') ? 'externalId' : 'id';
        Arr::set($payload, 'externalId', Arr::get($payload, 'id'));
        Arr::set($payload, 'account.externalId', Arr::get($payload, "account.$accountIdKey"));

        return new UserData(array_merge(
            $payload,
            ['id' => $userId]
        ));
    }

    /**
     * @param HaapiResponse $response
     *
     * @return UserDataWithResetToken
     */
    protected function getUserWithPasswordToken(HaapiResponse $response): UserDataWithResetToken
    {
        return new UserDataWithResetToken($response->getPayload());
    }
}
