<?php

namespace Modules\Haapi\Actions\Account;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\User\Models\User;

class UpdateSpecialAdsCategory extends BaseAction
{
    /**
     * Endpoint type
     */
    private const REQUEST_TYPE = 'account/update_special_ads_category';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * @param User $advertiser
     * @param int  $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(User $advertiser, int $userId): HaapiResponse
    {
        return $this->requestService->request(self::REQUEST_TYPE, [
            'accountId'          => $advertiser->getExternalId(),
            'specialAdsCategory' => $advertiser->isSpecialAds(),
        ], $userId);
    }
}
