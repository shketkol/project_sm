<?php

namespace Modules\Haapi\Actions\Account;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Actions\Account\Contracts\AccountRetryPayment as AccountRetryPaymentInterface;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\User\Repositories\Contracts\UserRepository;

/**
 * Class AccountRetryPayment
 *
 * @package Modules\Haapi\Actions\Account
 */
class AccountRetryPayment extends BaseAction implements AccountRetryPaymentInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'account/retry_payment';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService, UserRepository $repository)
    {
        parent::__construct();
        $this->requestService = $requestService;
        $this->repository = $repository;
    }

    /**
     * Retry payment for the specific user.
     *
     * @param int $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\UserNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(int $userId): HaapiResponse
    {
        $user = $this->repository->getById($userId);

        return $this->requestService->request(
            self::REQUEST_TYPE,
            ['accountId' => $user->account_external_id],
            $userId
        );
    }
}
