<?php

namespace Modules\Haapi\Actions\Account;

use App\Helpers\SessionIdentifierHelper;
use Modules\Haapi\Actions\BaseCallbackAction;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\User\DataTransferObjects\UserCompanyUpdateData;
use Modules\Haapi\Actions\Account\Contracts\AccountUpdateProfile as AccountUpdateProfileInterface;

/**
 * @package Modules\Haapi\Actions\Account
 */
class AccountUpdateProfile extends BaseCallbackAction implements AccountUpdateProfileInterface
{
    /**
     * Endpoint type
     */
    private const REQUEST_TYPE = 'account/update';

    /**
     * Session identifier key
     */
    protected const IDENTIFIER_KEY = 'sessionIdentifier';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @param SessionIdentifierHelper $identifierHelper
     * @param RequestService          $requestService
     */
    public function __construct(SessionIdentifierHelper $identifierHelper, RequestService $requestService)
    {
        parent::__construct($identifierHelper);
        $this->requestService = $requestService;
    }

    /**
     * Update account profile
     *
     * @param UserCompanyUpdateData $addressData
     * @param int                   $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(UserCompanyUpdateData $addressData, int $userId): HaapiResponse
    {
        $payload = $this->getPayloadWithSessionIdentifier($addressData->toArray());
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $payload,
            $userId
        );
    }
}
