<?php

namespace Modules\Haapi\Actions\Account;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Mappers\Account\UserMapper;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\User\DataTransferObjects\UserData;
use Modules\Haapi\Actions\Account\Contracts\AccountSignUp as AccountSignUpInterface;

/**
 * Class AccountSignUp
 *
 * @package Modules\Haapi\Actions\Account
 */
class AccountSignUp extends BaseAction implements AccountSignUpInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'account/signup';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * AccountSignUp constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Sign up an account
     *
     * @param UserData $user
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(UserData $user): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            UserMapper::map($user)
        );
    }
}
