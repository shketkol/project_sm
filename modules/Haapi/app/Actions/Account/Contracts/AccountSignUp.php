<?php

namespace Modules\Haapi\Actions\Account\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\User\DataTransferObjects\UserData;

/**
 * Interface AccountSignUp
 *
 * @package Modules\Haapi\Actions\Account\Contracts
 */
interface AccountSignUp
{
    /**
     * Sign up an account
     *
     * @param UserData $user
     *
     * @return HaapiResponse
     */
    public function handle(UserData $user): HaapiResponse;
}
