<?php

namespace Modules\Haapi\Actions\Account\Contracts;

use Modules\Daapi\DataTransferObjects\Types\AccountData;

/**
 * Interface AccountGet
 *
 * @package Modules\Haapi\Actions\Account\Contracts
 */
interface AccountGet
{
    /**
     * Get an account with specific account id
     *
     * @param string $accountId
     * @param int    $userId
     *
     * @return AccountData
     */
    public function handle(string $accountId, int $userId): AccountData;
}
