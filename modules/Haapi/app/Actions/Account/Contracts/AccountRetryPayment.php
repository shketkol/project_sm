<?php

namespace Modules\Haapi\Actions\Account\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;

/**
 * Interface AccountRetryPayment
 *
 * @package Modules\Haapi\Actions\Account\Contracts
 */
interface AccountRetryPayment
{
    /**
     * Retry payment for the specific user.
     *
     * @param int $userId
     *
     * @return HaapiResponse
     */
    public function handle(int $userId): HaapiResponse;
}
