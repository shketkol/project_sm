<?php

namespace Modules\Haapi\Actions\Account\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\User\DataTransferObjects\UserCompanyUpdateData;

/**
 * Interface AccountUpdateProfile
 *
 * @package Modules\Haapi\Actions\Account\Contracts
 */
interface AccountUpdateProfile
{
    /**
     * Update account profile
     *
     * @param UserCompanyUpdateData $addressData
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(UserCompanyUpdateData $addressData, int $userId): HaapiResponse;
}
