<?php

namespace Modules\Haapi\Actions\Account;

use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Account\Contracts\AccountGet as AccountGetInterface;
use Modules\Haapi\Services\Contracts\RequestService;

/**
 * Class AccountGet
 *
 * @package Modules\Haapi\Actions\Account
 */
class AccountGet extends BaseAction implements AccountGetInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'account/get';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * AccountGet constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Get an account with specific account id
     *
     * @param string $accountId
     * @param int    $userId
     *
     * @return AccountData
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(string $accountId, int $userId): AccountData
    {
        $response = $this->requestService->request(
            self::REQUEST_TYPE,
            ['accountId' => $accountId],
            $userId
        );

        return new AccountData($response->getPayload());
    }
}
