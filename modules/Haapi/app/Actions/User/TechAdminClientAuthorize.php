<?php

namespace Modules\Haapi\Actions\User;

use Illuminate\Support\Arr;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Services\Contracts\RequestService;
use Psr\Log\LoggerInterface;

class TechAdminClientAuthorize extends BaseAction
{
    /**
     * Endpoint type
     */
    private const REQUEST_TYPE = 'client/authorize';

    /**
     * Request service
     *
     * @var RequestService
     */
    private $requestService;

    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param RequestService  $requestService
     * @param AuthService     $authService
     * @param LoggerInterface $log
     */
    public function __construct(RequestService $requestService, AuthService $authService, LoggerInterface $log)
    {
        $this->requestService = $requestService;
        $this->authService    = $authService;
        $this->log            = $log;
    }


    /**
     * Sign in tech-admin user.
     *
     * @param string $email
     * @param string $password
     * @param int    $userId
     * @param null|string $token
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(
        string $email,
        string $password,
        int $userId,
        ?string $token = ''
    ): string {
        if ($token) {
            $this->authService->setTokens($userId, ['token' => $token]);

            return $token;
        }

        try {
            $response = $this->requestService->request(
                self::REQUEST_TYPE,
                [
                    'clientId' => config('cognito.admin.login_params.client_id'),
                    'username' => $email,
                    'password' => $password,
                ]
            );
        } catch (\Throwable $exception) {
            $this->log->error('Fail admin client authorize');
        }

        $tokens = $response->getPayload();
        $sessionToken = Arr::get($tokens, 'accessToken', '');
        $this->authService->setTokens($userId, ['token' => $sessionToken]);

        return $sessionToken;
    }
}
