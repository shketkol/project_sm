<?php

namespace Modules\Haapi\Actions\User\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\User\DataTransferObjects\UserUpdateData;

/**
 * Interface UserUpdate
 *
 * @package Modules\Haapi\Actions\User\Contracts
 */
interface UserUpdate
{
    /**
     * Update a user
     *
     * @param UserUpdateData $userData
     *
     * @param int $userId
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(UserUpdateData $userData, int $userId): HaapiResponse;
}
