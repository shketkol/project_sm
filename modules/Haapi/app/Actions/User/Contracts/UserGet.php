<?php

namespace Modules\Haapi\Actions\User\Contracts;

use Modules\Daapi\DataTransferObjects\Types\UserData;

/**
 * Interface UserGet
 *
 * @package Modules\Haapi\Actions\User\Contracts
 */
interface UserGet
{
    /**
     * Get user
     *
     * @param int $userId
     *
     * @return UserData
     */
    public function handle(int $userId): UserData;
}
