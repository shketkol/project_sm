<?php

namespace Modules\Haapi\Actions\User;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\User\Contracts\UserUpdate as UserUpdateInterface;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\User\DataTransferObjects\UserUpdateData;

/**
 * Class UserUpdate
 *
 * @package Modules\Haapi\Actions\User
 */
class UserUpdate extends BaseAction implements UserUpdateInterface
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'user/update';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * UserUpdate constructor.
     *
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Update a user
     *
     * @param UserUpdateData $userData
     *
     * @param int            $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(UserUpdateData $userData, int $userId): HaapiResponse
    {
        return $this->requestService->request(
            self::REQUEST_TYPE,
            $userData->toArray(),
            $userId
        );
    }
}
