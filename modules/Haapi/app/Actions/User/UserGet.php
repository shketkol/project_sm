<?php

namespace Modules\Haapi\Actions\User;

use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\Actions\Traits\CreateUserData;
use Modules\Haapi\Actions\User\Contracts\UserGet as UserGetInterface;
use Modules\Haapi\Events\HaapiAccountRetrieved;
use Modules\Haapi\Services\Contracts\RequestService;

class UserGet extends BaseAction implements UserGetInterface
{
    use CreateUserData;

    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'user/get';

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @param RequestService $requestService
     */
    public function __construct(RequestService $requestService)
    {
        parent::__construct();
        $this->requestService = $requestService;
    }

    /**
     * Get user
     *
     * @param int $userId
     *
     * @return UserData
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     */
    public function handle(int $userId): UserData
    {
        $response = $this->requestService->request(self::REQUEST_TYPE, [], $userId);

        event(new HaapiAccountRetrieved($response));

        return $this->getUserData($response, $userId);
    }
}
