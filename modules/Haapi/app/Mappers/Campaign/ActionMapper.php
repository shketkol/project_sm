<?php

namespace Modules\Haapi\Mappers\Campaign;

use App\Exceptions\InvalidArgumentException;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\CampaignStep;

class ActionMapper
{
    /**
     * Campaign Wizard Actions
     */
    public const CREATIVE_ATTACHED = 'creative.attached';
    public const CREATIVE_APPROVED = 'creative.approved';
    public const CAMPAIGN_DELETED  = 'campaign.deleted';
    public const CREATIVE_REJECTED = 'creative.rejected';
    public const CREATIVE_DETACHED = 'creative.detached';

    /**
     * Campaign details page that used as last visited page when advertiser edits campaign
     */
    public const CAMPAIGN_DETAILS = 'campaign.details';

    /**
     * @var string[]
     */
    private const MAP = [
        self::CREATIVE_ATTACHED => 'AD_ATTACHED',
        self::CREATIVE_APPROVED => 'AD_APPROVED',
        self::CAMPAIGN_DELETED  => 'DRAFT_DELETED',
        self::CREATIVE_REJECTED => 'AD_REJECTED',
        self::CREATIVE_DETACHED => 'AD_DETACHED',
        CampaignStep::SCHEDULE  => 'DATES_SELECTED',
        CampaignStep::DEVICES   => 'TARGET_PLATFORMS_SELECTED',
        CampaignStep::NAME      => 'NAMED',
        CampaignStep::LOCATIONS => 'TARGET_LOCATIONS_SELECTED',
        CampaignStep::GENRES    => 'TARGET_GENRES_SELECTED',
        CampaignStep::BUDGET    => 'BUDGET_SET',
        CampaignStep::AUDIENCES => 'TARGET_AUDIENCES_SELECTED',
        CampaignStep::AGES      => 'TARGET_AGE_DEMO_SELECTED',
    ];

    /**
     * @param string $wizardAction
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public static function map(string $wizardAction): string
    {
        $haapiAction = Arr::get(self::MAP, $wizardAction);

        if (!is_null($haapiAction)) {
            return $haapiAction;
        }

        throw new InvalidArgumentException(sprintf(
            'Provided Campaign Wizard Action "%s" could not be mapped to HAAPI action.',
            $wizardAction
        ));
    }

    /**
     * @return string[]
     */
    public static function getWizardActions(): array
    {
        return array_keys(self::MAP);
    }
}
