<?php

namespace Modules\Haapi\Mappers\Account;

use Modules\User\DataTransferObjects\UserData;

class UserMapper
{
    /**
     * @param UserData $user
     *
     * @return array
     */
    public static function map(UserData $user): array
    {
        return [
            'email'             => $user->email,
            'firstName'         => $user->first_name,
            'lastName'          => $user->last_name,
            'companyName'       => $user->company->name,
            'companyAddress'    => UserCompanyMapper::map($user->company),
            'phoneNumber'       => $user->phone_number,
            'identificationKey' => $user->identification_key,
        ];
    }
}
