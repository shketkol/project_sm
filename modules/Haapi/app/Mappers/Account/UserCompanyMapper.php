<?php

namespace Modules\Haapi\Mappers\Account;

use Modules\User\DataTransferObjects\UserCompanyData;

/**
 * Class UserCompanyMapper
 *
 * @package Modules\Haapi\Mappers\Account
 */
class UserCompanyMapper
{
    /**
     * @param UserCompanyData $userCompany
     * @return array
     */
    public static function map(UserCompanyData $userCompany): array
    {
        return [
            'line1'   => $userCompany->address_line1,
            'line2'   => $userCompany->address_line2,
            'state'   => $userCompany->state,
            'city'    => $userCompany->city,
            'country' => $userCompany->country,
            'zipcode' => $userCompany->zipcode,
        ];
    }
}
