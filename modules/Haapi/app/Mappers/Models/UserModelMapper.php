<?php

namespace Modules\Haapi\Mappers\Models;

use Illuminate\Support\Str;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\User\Models\User;

class UserModelMapper
{
    /**
     * @param UserData $user
     *
     * @return array
     */
    public static function map(UserData $user): array
    {
        return [
            'external_id'         => $user->externalId,
            'status_id'           => User::getStatusIdByTransactionFlow($user->company->accountStatus, 'user'),
            'account_external_id' => $user->company->externalId,
            'company_name'        => $user->company->companyName,
        ];
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public static function findRole(string $name): string
    {
        return config('daapi.roles_mapping.' . Str::lower($name));
    }
}
