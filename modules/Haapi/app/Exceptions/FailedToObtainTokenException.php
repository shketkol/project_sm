<?php

namespace Modules\Haapi\Exceptions;

use App\Exceptions\BaseException;
use Exception;
use Illuminate\Http\Response;

class FailedToObtainTokenException extends BaseException
{
    /**
     * Default Status code used for improved logging.
     * This exception being logged as WARNING.
     * See BaseExceptions and LoggerService for details.
     */
    public const STATUS_CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response()->json([
            'message' => trans('auth::messages.password.failed_token'),
        ], $this->getCode());
    }
}
