<?php

namespace Modules\Haapi\Exceptions;

use Illuminate\Http\Response;

class ObjectNotFoundException extends HaapiException
{
    /**
     * Default Status code used for improved logging.
     * This exception being logged as WARNING.
     * See BaseExceptions and LoggerService for details.
     */
    public const STATUS_CODE = Response::HTTP_NOT_FOUND;

    /**
     * @var string
     */
    protected static $defaultMessage = 'The requested object could not be found.';

    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response()->json([
            'message' => static::$defaultMessage,
        ], $this->getCode());
    }
}
