<?php

namespace Modules\Haapi\Exceptions;

use App\Exceptions\Contracts\UnauthorizedException as UnauthorizedExceptionContract;
use Illuminate\Http\Response;

class UnauthorizedException extends HaapiException implements UnauthorizedExceptionContract
{
    /**
     * Default Status code used for improved logging.
     * This exception being logged as WARNING.
     * See BaseExceptions and LoggerService for details.
     */
    public const STATUS_CODE = Response::HTTP_UNAUTHORIZED;

    /**
     * @var string
     */
    protected static $defaultMessage = 'Cannot authorize user for this request.';

    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response()->json([
            'message' => static::$defaultMessage,
        ], Response::HTTP_UNAVAILABLE_FOR_LEGAL_REASONS); // 401 code triggers Basic Auth logout
    }
}
