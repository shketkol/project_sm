<?php

namespace Modules\Haapi\Exceptions;

use Illuminate\Http\Response;

class ForbiddenException extends HaapiException
{
    /**
     * Default Status code used for improved logging.
     * This exception being logged as WARNING.
     * See BaseExceptions and LoggerService for details.
     */
    public const STATUS_CODE = Response::HTTP_FORBIDDEN;

    /**
     * @var string
     */
    protected static $defaultMessage = 'Forbidden operation.';

    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response()->json([
            'message' => static::$defaultMessage,
        ], $this->getCode());
    }
}
