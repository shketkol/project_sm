<?php

namespace Modules\Haapi\Exceptions;

use Illuminate\Http\Response;

class InvalidRequestException extends HaapiException
{
    /**
     * Default Status code used for improved logging.
     * This exception being logged as WARNING.
     * See BaseExceptions and LoggerService for details.
     */
    public const STATUS_CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * @var string
     */
    protected static $defaultMessage = 'Cannot complete request due to validation issues.';

    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response()->json([
            'message' => $this->message,
        ], $this->getCode());
    }
}
