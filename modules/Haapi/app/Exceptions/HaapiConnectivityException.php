<?php

namespace Modules\Haapi\Exceptions;

use Illuminate\Http\Response;

class HaapiConnectivityException extends HaapiException
{
    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response()->json([
            'message' => trans('auth::messages.connectivity'),
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
