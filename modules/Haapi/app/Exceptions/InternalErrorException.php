<?php

namespace Modules\Haapi\Exceptions;

use Illuminate\Http\Response;

class InternalErrorException extends HaapiException
{
    /**
     * Default Status code used for improved logging.
     * This exception being logged as ERROR.
     * See BaseExceptions and LoggerService for details.
     */
    public const STATUS_CODE = Response::HTTP_INTERNAL_SERVER_ERROR;

    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response()->json([
            'message' => static::$defaultMessage,
        ], Response::HTTP_CONFLICT);
    }
}
