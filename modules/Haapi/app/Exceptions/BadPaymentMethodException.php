<?php

namespace Modules\Haapi\Exceptions;

use Illuminate\Http\Response;

class BadPaymentMethodException extends HaapiException
{
    /**
     * @var string
     */
    protected static $defaultMessage = 'Bad payment';

    /**
     * @var string
     */
    protected const TYPE_ERROR = 'application';

    /**
     * Render the exception.
     *
     * @return Response
     */
    public function render()
    {
        $errorMessage = in_array($this->getCode(), [
            Response::HTTP_UNPROCESSABLE_ENTITY,
            Response::HTTP_INTERNAL_SERVER_ERROR
        ])
            ? $this->getMessage()
            : trans('payment::messages.cannot_be_obtained');

        return response()->json([
            'message'    => $errorMessage,
            'error_type' => self::TYPE_ERROR
        ], $this->getCode());
    }
}
