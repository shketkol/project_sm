<?php

namespace Modules\Haapi\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\Response;

abstract class HaapiException extends BaseException
{
    /**
     * @var string
     */
    protected static $defaultMessage = 'Something went wrong. Please, contact administrator.';

    /**
     * Report the exception.
     *
     * @return Response
     */
    abstract public function render();

    /**
     * @param string $message
     *
     * @param int $code
     * @return self
     */
    public static function create(string $message = '', int $code = 0): self
    {
        return new static($message ?: static::$defaultMessage, $code);
    }
}
