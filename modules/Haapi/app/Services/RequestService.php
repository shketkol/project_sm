<?php

namespace Modules\Haapi\Services;

use App\Helpers\EnvironmentHelper;
use Illuminate\Routing\UrlGenerator;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Haapi\Helpers\HaapiCheckResponse;
use Modules\Haapi\HttpClient\Builders\Contracts\RequestBuilder;
use Modules\Haapi\HttpClient\Builders\Contracts\ResponseBuilder;
use Modules\Haapi\HttpClient\HaapiClient;
use Modules\Haapi\HttpClient\Requests\HaapiRequest;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService as RequestServiceInterface;
use Psr\Log\LoggerInterface;

/**
 * Class RequestService
 *
 * @package Modules\Haapi\Services
 */
class RequestService implements RequestServiceInterface
{
    /**
     * @var HaapiClient
     */
    private $requestClient;

    /**
     * @var AuthService
     */
    protected $authService;

    /**
     * @var RequestBuilder
     */
    private $requestBuilder;

    /***
     * @var ResponseBuilder
     */
    private $responseBuilder;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * RequestService constructor.
     *
     * @param AuthService    $authService
     * @param HaapiClient     $requestClient
     * @param RequestBuilder  $requestBuilder
     * @param ResponseBuilder $responseBuilder
     * @param UrlGenerator    $urlGenerator
     * @param LoggerInterface $log
     */
    public function __construct(
        AuthService $authService,
        HaapiClient $requestClient,
        RequestBuilder $requestBuilder,
        ResponseBuilder $responseBuilder,
        UrlGenerator $urlGenerator,
        LoggerInterface $log
    ) {
        $this->authService = $authService;
        $this->requestClient = $requestClient;
        $this->requestBuilder = $requestBuilder;
        $this->responseBuilder = $responseBuilder;
        $this->urlGenerator = $urlGenerator;
        $this->log = $log;
    }

    /**
     * @param string   $type
     * @param array    $data
     * @param int|null $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Throwable
     */
    public function request(string $type, array $data = [], int $userId = null): HaapiResponse
    {
        if (!EnvironmentHelper::isLocalEnv()) {
            $this->setCallback($type);
        }

        try {
            $haapiResponse = $this->requestAttempt($type, $data, $userId);
        } catch (\Throwable $e) {
            // If request responded with unauthorized response then refresh token and make another attempt.
            $shouldRefresh = $userId && HaapiCheckResponse::isUnauthorizedException($e);
            if (!$shouldRefresh) {
                throw $e;
            }

            if (!$this->authService->tryRefreshToken($userId)) {
                throw $e;
            }
            $haapiResponse = $this->requestAttempt($type, $data, $userId);
        }

        return $haapiResponse;
    }

    /**
     * @param string   $type
     * @param array    $data
     * @param int|null $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    private function requestAttempt(string $type, array $data = [], ?int $userId = null): HaapiResponse
    {
        $this->setToken($userId);
        $response = $this->makeResponse($type, $this->makeRequest($type, $data));
        HaapiCheckResponse::isSuccess($response, false);
        return $response;
    }

    /**
     * @param string $type
     * @param string $token
     * @param array $data
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function requestWithSessionToken(string $type, string $token, array $data = []): HaapiResponse
    {
        if (!EnvironmentHelper::isLocalEnv()) {
            $this->setCallback($type);
        }
        $this->requestBuilder->setToken($token);

        $haapiResponse = $this->makeResponse($type, $this->makeRequest($type, $data));

        HaapiCheckResponse::isSuccess($haapiResponse, false);

        return $haapiResponse;
    }

    /**
     * @param string $type
     */
    private function setCallback(string $type): void
    {
        $routeName = config('daapi.callbacks.' . $type);

        if (!$routeName) {
            $this->log->info('There is no callback endpoint for this config key: daapi.callbacks.' . $type);
        } else {
            $this->requestBuilder->setCallBackUrl($this->urlGenerator->signedRoute($routeName));
        }
    }

    /**
     * @param int|null $userId
     *
     * @return void
     */
    private function setToken(?int $userId): void
    {
        $token = $this->authService->getToken($userId);

        if ($token) {
            $this->requestBuilder->setToken($token);
        }
    }

    /**
     * @param array  $data
     * @param string $type
     */
    private function logRequest(array $data, string $type): void
    {
        $this->log->debug('[HAAPI] Sending request to HULU... Data:', [
            'data' => $data,
            'type' => $type,
        ]);
    }

    /**
     * @param HaapiResponse $response
     * @param string        $type
     */
    private function logResponse(HaapiResponse $response, string $type): void
    {
        $message = '[HAAPI] Done. Response received with status code ';
        $message .= "{$response->getStatusCode()} and message {$response->getStatusMsg()}";

        $context = ['type' => $type];
        if ($response->get('message')) {
            $context['reason'] = $response->get('message');
        }

        $this->log->debug($message, $context);
    }

    /**
     * @param string $type
     * @param array  $data
     *
     * @return HaapiRequest
     */
    private function makeRequest(string $type, array $data): HaapiRequest
    {
        $haapiRequest = $this->requestBuilder->make($type, $data);
        $this->logRequest($data, $type);

        return $haapiRequest;
    }

    /**
     * @param string       $type
     * @param HaapiRequest $haapiRequest
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     */
    private function makeResponse(string $type, HaapiRequest $haapiRequest): HaapiResponse
    {
        $response = $this->responseBuilder->make(
            $this->requestClient->makeRequest($haapiRequest)
        );
        $this->logResponse($response, $type);

        return $response;
    }
}
