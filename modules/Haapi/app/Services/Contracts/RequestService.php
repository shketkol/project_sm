<?php

namespace Modules\Haapi\Services\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;

/**
 * Interface RequestService
 *
 * @package Modules\Haapi\Services\Contracts
 */
interface RequestService
{
    /**
     * @param string   $type
     * @param array    $data
     * @param int|null $userId
     *
     * @return HaapiResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function request(string $type, array $data = [], int $userId = null): HaapiResponse;

    /**
     * @param string $type
     * @param string $token
     * @param array $data
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function requestWithSessionToken(string $type, string $token, array $data = []): HaapiResponse;
}
