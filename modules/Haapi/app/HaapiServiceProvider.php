<?php

namespace Modules\Haapi;

use App\Providers\ModuleServiceProvider;
use Modules\Haapi\Actions\Account\AccountGet;
use Modules\Haapi\Actions\Account\AccountRetryPayment;
use Modules\Haapi\Actions\Account\AccountUpdateProfile;
use Modules\Haapi\Actions\Admin\Account\AdminAccountSetStatus;
use Modules\Haapi\Actions\Admin\Account\AdminAccountSearch;
use Modules\Haapi\Actions\Admin\Account\Contracts\AdminAccountSearch as AdminAccountSearchInterface;
use Modules\Haapi\Actions\Admin\Campaign\AdminCampaignSearch;
use Modules\Haapi\Actions\Admin\Creative\AdminCreativeSearch;
use Modules\Haapi\Actions\Admin\Campaign\Contracts\AdminCampaignSearch as AdminCampaignSearchInterface;
use Modules\Haapi\Actions\Admin\Creative\Contracts\AdminCreativeSearch as AdminCreativeSearchInterface;
use Modules\Haapi\Actions\Campaign\CampaignImpressions;
use Modules\Haapi\Actions\Campaign\CampaignImpressionsDetail;
use Modules\Haapi\Actions\Creative\Contracts\CreativeGet as CreativeGetInterface;
use Modules\Haapi\Actions\Campaign\CampaignGetPrice;
use Modules\Haapi\Actions\Campaign\ReportAccountImpressions;
use Modules\Haapi\Actions\Creative\Contracts\CreativeSearch as CreativeSearchInterface;
use Modules\Haapi\Actions\Creative\Contracts\CreativeUpdate as CreativeUpdateInterface;
use Modules\Haapi\Actions\Creative\Contracts\CreativeUpload as CreativeUploadInterface;
use Modules\Haapi\Actions\Creative\CreativeGet;
use Modules\Haapi\Actions\Creative\CreativeSearch;
use Modules\Haapi\Actions\Creative\CreativeUpdate;
use Modules\Haapi\Actions\Creative\CreativeUpload;
use Modules\Haapi\Actions\Campaign\CampaignCreate;
use Modules\Haapi\Actions\Campaign\CampaignCancel;
use Modules\Haapi\Actions\Campaign\CampaignGet;
use Modules\Haapi\Actions\Campaign\CampaignPause;
use Modules\Haapi\Actions\Campaign\CampaignResume;
use Modules\Haapi\Actions\Campaign\CampaignInventoryCheck;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignGet as CampaignGetInterface;
use Modules\Haapi\Actions\Campaign\TargetingTypeList;
use Modules\Haapi\Actions\Campaign\TargetingValuesList;
use Modules\Haapi\Actions\Payment\OrderGet;
use Modules\Haapi\Actions\Payment\OrderList;
use Modules\Haapi\Actions\Payment\PaymentMethodCreate;
use Modules\Haapi\Actions\Payment\PaymentMethodDelete;
use Modules\Haapi\Actions\Payment\PaymentMethodGet;
use Modules\Haapi\Actions\User\UserGet;
use Modules\Haapi\Actions\Admin\User\AdminUserGet;
use Modules\Haapi\Actions\User\UserUpdate;
use Modules\Haapi\HttpClient\Builders\Contracts\RequestBuilder;
use Modules\Haapi\HttpClient\Builders\Contracts\ResponseBuilder;
use Modules\Haapi\HttpClient\Builders\HaapiRequestBuilder;
use Modules\Haapi\HttpClient\Builders\HaapiResponseBuilder;
use Modules\Haapi\Services\RequestService;
use Modules\Haapi\Services\Contracts\RequestService as RequestServiceInterface;
use Modules\Haapi\Actions\Account\Contracts\AccountSignUp as AccountSignUpInterface;
use Modules\Haapi\Actions\User\Contracts\UserGet as UserGetInterface;
use Modules\Haapi\Actions\Account\Contracts\AccountGet as AccountGetInterface;
use Modules\Haapi\Actions\Account\Contracts\AccountRetryPayment as AccountRetryPaymentInterface;
use Modules\Haapi\Actions\User\Contracts\UserUpdate as UserUpdateInterface;
use Modules\Haapi\Actions\Account\AccountSignUp;
use Modules\Haapi\Actions\Campaign\Contracts\TargetingValuesList as TargetingValuesListInterface;
use Modules\Haapi\Actions\Campaign\Contracts\TargetingTypeList as TargetingTypeListInterface;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignCancel as CampaignCancelInterface;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignPause as CampaignPauseInterface;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignResume as CampaignResumeInterface;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignInventoryCheck as CampaignInventoryCheckInterface;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignGetPrice as CampaignGetPriceInterface;
use Modules\Haapi\Actions\Admin\User\Contracts\AdminUserGet as AdminUserGetInterface;
use Modules\Haapi\Actions\Admin\Account\Contracts\AdminAccountSetStatus as AdminAccountSetStatusInterface;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignCreate as CampaignCreateInterface;
use Modules\Haapi\Actions\Account\Contracts\AccountUpdateProfile as AccountUpdateProfileInterface;
use Modules\Haapi\Actions\Payment\Contracts\OrderList as OrderListInterface;
use Modules\Haapi\Actions\Payment\Contracts\OrderGet as OrderGetInterface;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodCreate as PaymentMethodCreateInterface;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodGet as PaymentMethodGetInterface;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodDelete as PaymentMethodDeleteInterface;
use Modules\Haapi\Actions\Campaign\Contracts\ReportAccountImpressions as ReportAccountImpressionsInterface;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignImpressions as CampaignImpressionsInterface;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignImpressionsDetail as CampaignImpressionsDetailInterface;

/**
 * Class HaapiServiceProvider
 *
 * @package Modules\Haapi
 */
class HaapiServiceProvider extends ModuleServiceProvider
{
    /**
     * @var array
     */
    public $bindings = [
        RequestBuilder::class => HaapiRequestBuilder::class,
        ResponseBuilder::class => HaapiResponseBuilder::class,
        RequestServiceInterface::class => RequestService::class,
        AccountSignUpInterface::class => AccountSignUp::class,
        AccountGetInterface::class => AccountGet::class,
        AccountRetryPaymentInterface::class => AccountRetryPayment::class,
        UserGetInterface::class => UserGet::class,
        AccountUpdateProfileInterface::class => AccountUpdateProfile::class,
        UserUpdateInterface::class => UserUpdate::class,
        CreativeSearchInterface::class => CreativeSearch::class,
        CreativeUpdateInterface::class => CreativeUpdate::class,
        CreativeUploadInterface::class => CreativeUpload::class,
        CreativeGetInterface::class => CreativeGet::class,

        /**
         * Campaign actions
         */
        TargetingValuesListInterface::class => TargetingValuesList::class,
        TargetingTypeListInterface::class => TargetingTypeList::class,
        CampaignCreateInterface::class => CampaignCreate::class,
        CampaignCancelInterface::class => CampaignCancel::class,
        CampaignPauseInterface::class => CampaignPause::class,
        CampaignResumeInterface::class => CampaignResume::class,
        CampaignInventoryCheckInterface::class => CampaignInventoryCheck::class,
        CampaignGetPriceInterface::class => CampaignGetPrice::class,
        CampaignGetInterface::class => CampaignGet::class,
        ReportAccountImpressionsInterface::class => ReportAccountImpressions::class,
        CampaignImpressionsInterface::class => CampaignImpressions::class,
        CampaignImpressionsDetailInterface::class => CampaignImpressionsDetail::class,

        /**
         * Admin Actions
         */
        AdminUserGetInterface::class => AdminUserGet::class,
        AdminAccountSetStatusInterface::class => AdminAccountSetStatus::class,
        AdminAccountSearchInterface::class => AdminAccountSearch::class,
        AdminCampaignSearchInterface::class => AdminCampaignSearch::class,
        AdminCreativeSearchInterface::class => AdminCreativeSearch::class,

        /**
         * Payment Actions
         */
        OrderListInterface::class => OrderList::class,
        OrderGetInterface::class => OrderGet::class,
        PaymentMethodCreateInterface::class => PaymentMethodCreate::class,
        PaymentMethodDeleteInterface::class => PaymentMethodDelete::class,
        PaymentMethodGetInterface::class => PaymentMethodGet::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->publishes(
            [__DIR__ . '/../tests' => base_path('tests/haapi')],
            'tests'
        );
    }

    /**
     * Register method
     */
    public function register()
    {
        $this->loadConfigs([
            'haapi' => 'haapi',
            'haapi.stubs' => 'stubs',
            'haapi.jwt' => 'jwt'
        ]);
    }

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'haapi';
    }
}
