<?php

namespace Modules\Haapi\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

class HaapiAccountRetrieved
{
    use Dispatchable, SerializesModels;

    /**
     * @var HaapiResponse
     */
    public $response;

    /**
     * @param HaapiResponse $response
     */
    public function __construct(HaapiResponse $response)
    {
        $this->response = $response;
    }
}
