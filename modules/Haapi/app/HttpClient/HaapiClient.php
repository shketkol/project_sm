<?php

namespace Modules\Haapi\HttpClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Arr;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Helpers\HaapiConfig;
use Modules\Haapi\HttpClient\Requests\HaapiRequest;
use Psr\Log\LoggerInterface;

class HaapiClient
{
    /**
     * Guzzle client.
     *
     * @var Client
     */
    protected $client;

    /**
     * @var HaapiConfig
     */
    protected $config;

    /**
     * Response content.
     *
     * @var array|Response
     */
    protected $responseContent;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * HaapiClient constructor.
     *
     * @param Client          $client
     * @param HaapiConfig     $config
     * @param LoggerInterface $logger
     */
    public function __construct(Client $client, HaapiConfig $config, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * Set the last exception.
     *
     * @param Response $response Guzzle response
     *
     * @return void
     */
    public function setResponseContent(Response $response): void
    {
        $this->responseContent = $response;
    }

    /**
     * Obtain a response content.
     *
     * @param bool $json json flag
     *
     * @return array
     * @var    mixed
     */
    public function obtainResponseContent($json = true): array
    {
        if ($this->responseContent) {
            $content = $this->responseContent->getBody()->getContents();
            if (strlen($content) > 1) {
                if ($json) {
                    return is_array(json_decode($content, 1)) ? json_decode($content, 1) : [];
                } else {
                    return $content;
                }
            }
        }

        return [];
    }

    /**
     * @param HaapiRequest $haapiRequest
     *
     * @return array|mixed
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws HaapiConnectivityException
     */
    public function makeRequest(HaapiRequest $haapiRequest)
    {
        $defaultHeaders = [
            'headers'        => [
                'Content-Type' => 'application/json',
            ],
            'json'           => $haapiRequest->toArray(),
            'decode_content' => false,
            'http_errors'    => $this->config->getShowErrorsFlag(),
        ];
        $fullUrl = $this->config->getBaseUrl();
        $headers = array_replace_recursive($defaultHeaders, $haapiRequest->getHeaders());

        try {
            $this->logger->debug('[API.REQUEST] Type: \'' . Arr::get($headers, 'json.haapi.request.type') . '\'', [
                'type'    => Arr::get($headers, 'json.haapi.request.type'),
                'headers' => Arr::get($headers, 'headers'),
                'json'    => Arr::get($headers, 'json'),
                'payload' => $haapiRequest->getPayload(),
            ]);

            $response = $this->client->request(
                'POST',
                $fullUrl,
                $headers,
                $haapiRequest->getPayload()
            );
        } catch (ConnectException $exception) {
            throw new HaapiConnectivityException();
        }

        $this->setResponseContent($response);
        $responseContent = $this->obtainResponseContent();

        $this->logger->debug('[API.RESPONSE] Type: \'' . Arr::get($responseContent, 'haapi.request.type') . '\'', [
            'type'             => Arr::get($responseContent, 'haapi.request.type'),
            'http_status'      => $response->getStatusCode(),
            'api_status'       => Arr::get($responseContent, 'haapi.response.statusCode'),
            'response_content' => $responseContent,
        ]);

        return $responseContent;
    }
}
