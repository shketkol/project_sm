<?php

namespace Modules\Haapi\HttpClient\Requests;

/**
 * Class HaapiRequest
 *
 * @package Modules\Haapi\HttpClient\Requests
 */
class HaapiRequest
{

    /**
     * Id for a request
     *
     * @var string
     */
    protected $id;

    /**
     * System of calling side
     *
     * @var string
     */
    protected $system;

    /**
     * Haapi version
     *
     * @var string
     */
    protected $haapiVersion;

    /**
     * Type of a request
     *
     * @var string
     */
    protected $type;

    /**
     * Payload of a request
     *
     * @var array
     */
    protected $payload;

    /**
     * @var string
     */
    protected $preSharedKey;

    /**
     * Token for a authenticated types
     *
     * @var string
     */
    protected $token;

    /**
     * Body of a request
     *
     * @var array
     */
    protected $body;

    /**
     * Callback url
     *
     * @var string
     */
    protected $callbackUrl;

    /**
     * Headers for a request
     *
     * @var array
     */
    protected $headers = [];

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSystem(): string
    {
        return $this->system;
    }

    /**
     * @param string $system
     */
    public function setSystem(string $system): void
    {
        $this->system = $system;
    }

    /**
     * @return string
     */
    public function getHaapiVersion(): string
    {
        return $this->haapiVersion;
    }

    /**
     * @param string $haapiVersion
     */
    public function setHaapiVersion(string $haapiVersion): void
    {
        $this->haapiVersion = $haapiVersion;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPayload()
    {
        return !empty($this->payload) ? $this->payload : new \stdClass();
    }

    /**
     * @param array $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getPreSharedKey(): string
    {
        return $this->preSharedKey;
    }

    /**
     * @param string $preSharedKey
     */
    public function setPreSharedKey(string $preSharedKey): void
    {
        $this->preSharedKey = $preSharedKey;
    }

    /**
     * @return string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @param array $body
     */
    public function setBody(array $body): void
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getCallbackUrl(): ?string
    {
        return $this->callbackUrl;
    }

    /**
     * @param string $callbackUrl
     */
    public function setCallbackUrl(string $callbackUrl): void
    {
        $this->callbackUrl = $callbackUrl;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers ?: [];
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @param string $header
     * @param mixed  $value
     *
     * @return void
     */
    public function setHeader(string $header, $value): void
    {
        if ($header) {
            $this->headers['headers'][$header] = $value;
        }
    }

    /**
     * @param string $token
     *
     * @return void
     */
    public function setBearer(string $token): void
    {
        $this->setHeader('Authorization', "Bearer {$token}");
    }

    /**
     * Convert body of the request to json
     *
     * @return false|string
     */
    public function toJson(): string
    {
        return json_encode($this->body);
    }

    /**
     * Convert body of the request to array
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->body;
    }
}
