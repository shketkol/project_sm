<?php

namespace Modules\Haapi\HttpClient\Responses;

use Illuminate\Support\Arr;

/**
 * Class HaapiResponse
 *
 * @package Modules\Haapi\HttpClient\Responses
 */
class HaapiResponse
{
    /**
     * id
     *
     * @var string
     */
    private $id;

    /**
     * Body
     *
     * @var array
     */
    private $body;

    /**
     * System
     *
     * @var string
     */
    private $system;

    /**
     * @var string
     */
    private $duration;

    /**
     * @var string
     */
    private $received;

    /**
     * Payload
     *
     * @var array
     */
    private $payload;

    /**
     * Status Code
     *
     * @var string
     */
    private $statusCode;

    /**
     * Status Message
     *
     * @var string
     */
    private $statusMsg;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSystem(): string
    {
        return $this->system;
    }

    /**
     * @param string $system
     */
    public function setSystem(string $system): void
    {
        $this->system = $system;
    }

    /**
     * @param array $body
     */
    public function setBody(array $body): void
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getDuration(): string
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration(string $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getReceived(): string
    {
        return $this->received;
    }

    /**
     * @param string $received
     */
    public function setReceived(string $received): void
    {
        $this->received = $received;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    /**
     * @param string $statusCode
     */
    public function setStatusCode(string $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return string
     */
    public function getStatusMsg(): string
    {
        return $this->statusMsg;
    }

    /**
     * @param string $statusMsg
     */
    public function setStatusMsg(string $statusMsg): void
    {
        $this->statusMsg = $statusMsg;
    }

    public function getReason(): string
    {
        return Arr::get($this->payload, 'reason', '');
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->body;
    }

    /**
     * Convert HaapiResponse to array
     *
     * @param  string $key
     * @return mixed
     */
    public function get(string $key)
    {
        return Arr::get($this->payload, $key);
    }

    /**
     * @param string $key
     * @param        $value
     */
    public function set(string $key, $value)
    {
        Arr::set($this->payload, $key, $value);
    }
}
