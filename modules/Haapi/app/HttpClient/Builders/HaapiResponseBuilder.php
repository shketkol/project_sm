<?php

namespace Modules\Haapi\HttpClient\Builders;

use Illuminate\Support\Arr;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Haapi\Helpers\HaapiLogger;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\HttpClient\Builders\Contracts\ResponseBuilder;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

/**
 * Class HaapiResponseBuilder
 *
 * @package Modules\Haapi\HttpClient\Builders
 */
class HaapiResponseBuilder implements ResponseBuilder
{
    /**
     * HaapiResponse
     *
     * @var HaapiResponse
     */
    private $haapiResponse;

    /**
     * HaapiLogger
     *
     * @var HaapiLogger
     */
    private $logger;

    /**
     * HaapiResponseBuilder constructor.
     *
     * @param HaapiLogger $logger
     */
    public function __construct(HaapiLogger $logger)
    {
        $this->haapiResponse = new HaapiResponse();
        $this->logger = $logger;
    }

    /**
     * Build Response
     *
     * @param array $data
     *
     * @return HaapiResponse
     * @throws HaapiConnectivityException
     * @throws UnauthorizedException
     */
    public function make(array $data): HaapiResponse
    {
        $body = Arr::get($data, 'haapi.response');

        if (!$body) {
            throw new HaapiConnectivityException(json_encode([
                'message' => 'Cannot get response body',
                'response' => $data,
            ], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        }

        $statusCode = Arr::get($body, 'statusCode');

        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $this->haapiResponse->setBody($body);
        $this->haapiResponse->setPayload(Arr::get($body, 'payload'));
        $this->haapiResponse->setId($responseId);
        $this->haapiResponse->setStatusCode($statusCode);
        $this->haapiResponse->setStatusMsg($statusMsg);
        $this->haapiResponse->setDuration(Arr::get($body, 'duration'));
        $this->haapiResponse->setReceived(Arr::get($body, 'received'));

        $this->logger->log($this->haapiResponse);

        return $this->haapiResponse;
    }
}
