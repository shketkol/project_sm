<?php

namespace Modules\Haapi\HttpClient\Builders;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Modules\Haapi\Helpers\PresharedKey;
use Modules\Haapi\HttpClient\Builders\Contracts\RequestBuilder;
use Modules\Haapi\HttpClient\Requests\HaapiRequest;
use Modules\Haapi\Helpers\HaapiConfig;
use Ramsey\Uuid\Uuid;

/**
 * @package Modules\Haapi\HttpClient\Builders
 */
class HaapiRequestBuilder implements RequestBuilder
{
    /**
     * @var HaapiRequest
     */
    private $haapiRequest;

    /**
     * @var HaapiConfig
     */
    protected $config;

    /**
     * @param HaapiConfig $config
     */
    public function __construct(HaapiConfig $config)
    {
        $this->haapiRequest = new HaapiRequest();
        $this->config = $config;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->haapiRequest->setToken($token);
    }

    /**
     * @param string $url
     */
    public function setCallBackUrl(string $url)
    {
        /**
         * Adding possibility to test callbacks with ngrok
         * see: https://dashboard.ngrok.com/get-started
         */
        if (config('haapi.stubs.callback.stub')) {
            $url = str_ireplace(config('app.url'), config('haapi.stubs.callback.host'), $url);
        }
        $this->haapiRequest->setCallbackUrl($url);
    }

    /**
     * Generate request body
     *
     * @return array
     */
    private function getBody(): array
    {
        return [
            'haapi' => [
                'request' => [
                    'id'            => $this->haapiRequest->getId(),
                    'system'        => $this->haapiRequest->getSystem(),
                    'haapiVersion'  => $this->haapiRequest->getHaapiVersion(),
                    'preSharedKey'  => $this->haapiRequest->getPreSharedKey(),
                    'type'          => $this->haapiRequest->getType(),
                    'payload'       => $this->haapiRequest->getPayload()
                ]
            ]
        ];
    }

    /**
     * Set params for haapi request
     *
     * @param string $type
     * @param array  $payload
     *
     * @throws \Exception
     */
    private function setParams(string $type, array $payload): void
    {
        $preSharedKey = config('haapi.jwt.enable') ? $this->getSharedKey() : $this->config->getSharedKey();

        $this->haapiRequest->setId(Uuid::uuid4()->toString());
        $this->haapiRequest->setSystem($this->config->getSystem());
        $this->haapiRequest->setHaapiVersion($this->config->getHaapiVersion());
        $this->haapiRequest->setPreSharedKey($preSharedKey);
        $this->haapiRequest->setType($type);
        $this->haapiRequest->setPayload($payload);
    }

    /**
     * Build Request
     *
     * @param  string $type
     * @param  array  $payload
     * @return HaapiRequest
     *
     * @throws \Exception
     */
    public function make(string $type, array $payload) : HaapiRequest
    {
        $this->setParams($type, $payload);
        $body = $this->getBody();

        $token = $this->haapiRequest->getToken();
        if ($token) {
            $this->haapiRequest->setBearer($token);
        }

        if ($this->haapiRequest->getCallbackUrl()) {
            Arr::set(
                $body,
                'haapi.request.callbackUrl',
                $this->haapiRequest->getCallbackUrl()
            );
        }

        $this->haapiRequest->setBody($body);

        return $this->haapiRequest;
    }

    /**
     * @return string
     */
    private function getSharedKey(): string
    {
        $cachedJwt = Cache::get('jwt-token');

        if (is_null($cachedJwt)) {
            $jwt = PresharedKey::generate();
            Cache::add('jwt-token', $jwt, config('haapi.jwt.expire'));

            return $jwt;
        }

        return $cachedJwt;
    }
}
