<?php

namespace Modules\Haapi\HttpClient\Builders\Contracts;

use Modules\Haapi\HttpClient\Requests\HaapiRequest;

/**
 * Interface RequestBuilder
 *
 * @package Modules\Haapi\HttpClient\Builders\Contracts
 */
interface RequestBuilder
{
    /**
     * Set token
     *
     * @param  string $token
     * @return mixed
     */
    public function setToken(string $token);

    /**
     * Set callback url for Action
     *
     * @param  string $url
     * @return mixed
     */
    public function setCallBackUrl(string $url);

    /**
     * Build Request
     *
     * @param  string $type
     * @param  array  $payload
     * @return HaapiRequest
     */
    public function make(string $type, array $payload) : HaapiRequest;
}
