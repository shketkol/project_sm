<?php

namespace Modules\Haapi\HttpClient\Builders\Contracts;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;

/**
 * Interface ResponseBuilder
 *
 * @package Modules\Haapi\HttpClient\Builders\Contracts
 */
interface ResponseBuilder
{
    /**
     * Build Response
     *
     * @param  array $data
     * @return HaapiResponse
     */
    public function make(array $data): HaapiResponse;
}
