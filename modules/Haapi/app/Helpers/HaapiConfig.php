<?php

namespace Modules\Haapi\Helpers;

use Modules\Daapi\Helpers\DaapiConfig;

/**
 * @package Modules\Haapi\Helpers
 */
class HaapiConfig implements DaapiConfig
{
    /**
     * Haapi configs
     *
     * @var \Illuminate\Config\Repository|mixed
     */
    public $config;

    /**
     * HaapiConfig constructor.
     *
     * @param string $configKey
     */
    public function __construct(string $configKey = 'haapi')
    {
        /**
         * Set all config
         */
        $this->config = config($configKey);
    }

    /**
     * @param  $name
     * @param  $arguments
     * @return mixed|null
     */
    public function __call(string $name, array $arguments)
    {
        if (!in_array(substr($name, 0, 3), ['get'])) {
            return null;
        }

        $config = $this->config;
        $configKey = $this->toSnakeCase(substr($name, 3));

        if (!isset($config[$configKey])) {
            return null;
        }

        return !empty($arguments) ?
            data_get($config[$configKey], $arguments[0]) :
            $config[$configKey];
    }

    /**
     * CamelCase to snake_case
     *
     * @param $name
     *
     * @return string
     */
    private function toSnakeCase(string $name): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $name));
    }
}
