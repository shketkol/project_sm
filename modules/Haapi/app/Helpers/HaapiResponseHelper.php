<?php

namespace Modules\Haapi\Helpers;

class HaapiResponseHelper
{
    /**
     * @param string|bool $value
     *
     * @return bool
     */
    public static function valueToBoolean($value): bool
    {
        if (gettype($value) === 'boolean') {
            return $value;
        }
        return strtolower($value) === 'true';
    }
}
