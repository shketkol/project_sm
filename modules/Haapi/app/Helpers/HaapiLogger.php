<?php

namespace Modules\Haapi\Helpers;

use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Psr\Log\LoggerInterface;

/**
 * Class HaapiLogger
 *
 * @package Modules\Haapi\Helpers
 */
class HaapiLogger
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * HaapiLogger constructor.
     *
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * @param HaapiResponse $response
     */
    public function log(HaapiResponse $response)
    {
        $this->log->info($this->text($response), [
            'response' => [
                'id' => $response->getId(),
            ],
            'code'     => $response->getStatusCode(),
        ]);
    }

    /**
     * @param HaapiResponse $response
     *
     * @return string
     */
    private function text(HaapiResponse $response): string
    {
        $message = $response->getStatusMsg() . '.';

        if ($response->getStatusCode() === '0') {
            $message .= $response->get('message') ? (' ' . $response->get('message') . '.') : '';
        } else {
            $message .= $response->get('reason') ? (' ' . $response->get('reason') . '.') : '';
        }

        return $message;
    }
}
