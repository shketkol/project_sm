<?php

namespace Modules\Haapi\Helpers;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\ObjectNotFoundException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

class HaapiCheckResponse
{
    public const STATUS_CODE_OK                    = '0';
    public const STATUS_CODE_CONFLICT              = '409';
    public const STATUS_CODE_INVALID_REQUEST       = '400';
    public const STATUS_CODE_UNAUTHORIZED          = '401';
    public const STATUS_CODE_FORBIDDEN             = '403';
    public const STATUS_CODE_NOT_FOUND             = '404';
    public const STATUS_CODE_INTERNAL_ERROR        = '500';

    /**
     * Check if HaapiResponse succeed
     *
     * @param HaapiResponse $response
     * @param bool          $hideReason
     *
     * @return bool
     * @throws ConflictException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws ForbiddenException
     */
    public static function isSuccess(HaapiResponse $response, bool $hideReason = true): bool
    {
        if ($response->getStatusCode() === self::STATUS_CODE_OK) {
            return true;
        }

        $exception = self::getException($response, $hideReason);
        Log::logException($exception);

        throw $exception;
    }

    /**
     * @param Exception $exception
     *
     * @return bool
     */
    public static function isUnauthorizedException(\Throwable $exception): bool
    {
        return $exception instanceof UnauthorizedException;
    }

    /**
     * @param HaapiResponse $response
     * @param bool          $hideReason
     *
     * @return ConflictException|InvalidRequestException|UnauthorizedException|ForbiddenException|InternalErrorException
     */
    private static function getException(HaapiResponse $response, bool $hideReason): HaapiException
    {
        $status = $response->getStatusCode();
        $list = [
            self::STATUS_CODE_CONFLICT        => ConflictException::class,
            self::STATUS_CODE_INVALID_REQUEST => InvalidRequestException::class,
            self::STATUS_CODE_NOT_FOUND       => ObjectNotFoundException::class,
            self::STATUS_CODE_UNAUTHORIZED    => UnauthorizedException::class,
            self::STATUS_CODE_FORBIDDEN       => ForbiddenException::class,
        ];

        /** @var HaapiException $exception */
        $exception = Arr::get($list, $status, InternalErrorException::class);

        return $exception::create(($hideReason ? '' : ($response->get('reason') ?: $response->get('message'))) ?: '');
    }
}
