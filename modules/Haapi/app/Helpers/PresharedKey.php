<?php

namespace Modules\Haapi\Helpers;

use Firebase\JWT\JWT;

class PresharedKey
{
    /**
     * @return string
     */
    public static function generate(): string
    {
        return JWT::encode(self::getPayload(), config('haapi.jwt.shared_secret'));
    }

    /**
     * @return array
     */
    private static function getPayload(): array
    {
        return [
            'iss' => config('haapi.jwt.iss'),
            'iat' => time()
        ];
    }
}
