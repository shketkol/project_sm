<?php

namespace Modules\Haapi\Iterators;

use App\Exceptions\BaseException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Iterator;
use InvalidArgumentException;

abstract class PaginationIterator implements Iterator
{
    /**
     * Pagination limit for the page.
     */
    const LIMIT = 100;

    /**
     * Index of element inside the page.
     *
     * @var int
     */
    protected $position = 0;

    /**
     * Index of the page.
     *
     * @var int
     */
    protected $page = 0;

    /**
     * Loaded data.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Has value if there is a next page.
     *
     * @var null
     */
    protected $next = null;

    /**
     * Send request to load data.
     *
     * @return HaapiResponse
     */
    abstract protected function load(): HaapiResponse;

    /**
     * Get key of data that should be iterated.
     *
     * @return string
     */
    abstract protected function getResponseKey(): string;

    /**
     * Load next page of data.
     */
    protected function loadNext(): void
    {
        $response = $this->load();

        $this->data = $response->get($this->getResponseKey());
        $this->nextPage($response->get('next'));
    }

    /**
     * Create DTO param.
     *
     * @param string $className
     * @param array $params
     * @return mixed
     * @throws BaseException
     */
    protected function createParam(string $className, array $params = [])
    {
        if (!class_exists($className)) {
            throw BaseException::createFrom(new InvalidArgumentException("Class {$className} does not exist."));
        }

        return new $className(array_merge($params, [
            'start'     => $this->getStart(),
            'limit'     => $this->getLimit(),
        ]));
    }

    /**
     * Get pagination start value for the request.
     *
     * @return int
     */
    protected function getStart(): int
    {
        return $this->page * $this->getLimit();
    }

    /**
     * Get pagination limit value for the request.
     *
     * @return int
     */
    protected function getLimit(): int
    {
        return static::LIMIT;
    }

    /**
     * Set next page.
     *
     * @param int|null $next
     */
    protected function nextPage(?int $next): void
    {
        $this->next = $next;
        $this->position = 0;
        ++$this->page;
    }

    /**
     * Return the current element
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->data[$this->position];
    }

    /**
     * Move forward to next element
     * @return void Any returned value is ignored.
     */
    public function next(): void
    {
        ++$this->position;

        if (!isset($this->data[$this->position]) && $this->next) {
            $this->loadNext();
        }
    }

    /**
     * Return the key of the current element
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->getStart() + $this->position;
    }

    /**
     * Checks if current position is valid
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid(): bool
    {
        return isset($this->data[$this->position]);
    }

    /**
     * Rewind the Iterator to the first element
     * @return void Any returned value is ignored.
     */
    public function rewind(): void
    {
        $this->position = 0;
        $this->page = 0;
        $this->next = $this->getLimit();

        $this->loadNext();
    }
}
