<?php

namespace Modules\Haapi\DataTransferObjects\Creative;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Spatie\DataTransferObject\DataTransferObject;

class CreativeUploadParam extends DataTransferObject
{
    /**
     * This field shall be the UUID of the campaign to which creative needs to be associated, Required
     *
     * @var string
     */
    public $campaignId;

    /**
     * This field shall be the UUID of the advertiser, Required
     *
     * @var string
     */
    public $accountId;

    /**
     * This field shall be the name of the creative file that is being uploaded, Required
     *
     * @var string
     */
    public $fileName;

    /**
     * This field shall be the location URL of the creative file uploaded, Required
     *
     * @var string
     */
    public $sourceUrl;

    /**
     * This field shall be the size(in Bytes) of the creative that was uploaded by the owner, Required
     *
     * @var int
     */
    public $contentLength;

    /**
     * @return array
     */
    public function all(): array
    {
        $data = parent::all();

        return [
            'campaignId' => $data['campaignId'],
            'creative' => [
                'creativeCreation' => Arr::only($data, ['fileName', 'sourceUrl', 'contentLength', 'accountId'])
            ]
        ];
    }
}
