<?php

namespace Modules\Haapi\DataTransferObjects\Creative;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class AdminCreativeSearchParam extends DataTransferObject
{
    /**
     * Start param, default 0, Optional
     *
     * @var int|null
     */
    public $start;

    /**
     * Limit param, default 10, Optional
     *
     * @var int|null
     */
    public $limit;

    /**
     * Account id, Required
     *
     * @var string
     */
    public $accountId;

    /**
     * This field shall contain the text to be matched with Creative Name.
     * This is a case-insensitive match, Optional
     *
     * @var string|null
     */
    public $searchText;

    /**
     * When provided, only creatives with this status shall be included in the response. Optional
     * Allowed Values: SUBMITTED IN_REVIEW APPROVED REJECTED
     *
     * @var string|null
     */
    public $creativeStatusFilter;

    /**
     * When provided, the results shall include only those creatives that were uploaded on the given date, Optional
     * Format: MM/dd/YYYY
     *
     * @var string|null
     */
    public $uploadDateFilter;

    /**
     * When provided, the result will be sorted by this field. Optional
     * Allowed Values: uploaded_date
     *
     * @var string|null
     */
    public $sortFieldName;

    /**
     * When provided, the results returned in the response will follow a sort order of the given type. Optional
     * Default value is DESCENDING
     *
     * @var string|null
     */
    public $sortOrder;

    /**
     * AdminCreativeSearchParam constructor.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        parent::__construct(
            Arr::only(
                $parameters,
                [
                    'start',
                    'limit',
                    'accountId',
                    'searchText',
                    'creativeStatusFilter',
                    'uploadDateFilter',
                    'sortFieldName',
                    'sortOrder'
                ]
            )
        );
    }
}
