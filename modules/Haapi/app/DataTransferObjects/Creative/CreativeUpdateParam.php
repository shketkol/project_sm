<?php

namespace Modules\Haapi\DataTransferObjects\Creative;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class CreativeUpdateParam extends DataTransferObject
{
    /**
     * This field shall be the UUID of the campaign to which creative needs to be associated, Required
     *
     * @var string
     */
    public $campaignId;

    /**
     * This field shall be the UUID of an existing creative selected from the creative gallery, Required
     *
     * @var null|string
     */
    public $creativeId;

    /**
     * @return array
     */
    public function all(): array
    {
        $data = parent::all();

        return [
            'campaignId' => Arr::get($data, 'campaignId'),
            'creative' => [
                'creativeId' => Arr::get($data, 'creativeId')
            ]
        ];
    }
}
