<?php

namespace Modules\Haapi\DataTransferObjects\Promocode;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class PromocodeValidateData extends DataTransferObject
{
    /**
     * @var float
     */
    public $budget;

    /**
     * @var float
     */
    public $displayBudget;

    /**
     * @var float
     */
    public $discountedBudget;

    /**
     * @var float
     */
    public $discountedUnitCost;

    /**
     * @var string
     */
    public $decision;

    /**
     * @var string
     */
    public $discountType;

    /**
     * @var float
     */
    public $unitCost;

    /**
     * @var string
     */
    public $message;

    /**
     * @var float
     */
    public $discountValue;

    /**
     * @var float
     */
    public $displayDiscountedBudget;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        parent::__construct([
            'budget'                  => Arr::get($parameters, 'budget'),
            'displayBudget'           => Arr::get($parameters, 'displayBudget'),
            'discountedBudget'        => Arr::get($parameters, 'discountedBudget'),
            'displayDiscountedBudget' => Arr::get($parameters, 'displayDiscountedBudget'),
            'discountedUnitCost'      => Arr::get($parameters, 'discountedUnitCost'),
            'decision'                => Arr::get($parameters, 'decision'),
            'discountType'            => Arr::get($parameters, 'discountType'),
            'unitCost'                => Arr::get($parameters, 'unitCost'),
            'message'                 => Arr::get($parameters, 'message'),
            'discountValue'           => Arr::get($parameters, 'discountValue'),
        ]);
    }
}
