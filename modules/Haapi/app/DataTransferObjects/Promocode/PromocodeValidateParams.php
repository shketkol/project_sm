<?php

namespace Modules\Haapi\DataTransferObjects\Promocode;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Campaign\Traits\CampaignParamsTrait;
use Spatie\DataTransferObject\DataTransferObject;

class PromocodeValidateParams extends DataTransferObject
{
    use CampaignParamsTrait;

    /**
     * @var string
     */
    public $accountId;

    /**
     * @var string
     */
    public $startDate;

    /**
     * @var string
     */
    public $endDate;

    /**
     * @var int
     */
    public $quantity;

    /**
     * @var float
     */
    public $total;

    /**
     * @var float
     */
    public $unitCost;

    /**
     * @var string
     */
    public $promoCodeName;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Campaign\TargetParams[]
     */
    public $targets;

    /**
     * @var string
     */
    public $productId;

    /**
     * @var float
     */
    public $displayBudget;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        parent::__construct([
            'accountId'     => Arr::get($parameters, 'accountId'),
            'startDate'     => Arr::get($parameters, 'startDate'),
            'endDate'       => Arr::get($parameters, 'endDate'),
            'quantity'      => Arr::get($parameters, 'quantity'),
            'total'         => Arr::get($parameters, 'total'),
            'unitCost'      => Arr::get($parameters, 'unitCost'),
            'promoCodeName' => Arr::get($parameters, 'promoCodeName'),
            'targets'       => $this->getTargets(Arr::get($parameters, 'targets', [])),
            'productId'     => config('daapi.campaign.productId'),
            'displayBudget' => Arr::get($parameters, 'displayBudget'),
        ]);
    }

    /**
     * Get all instances as an array
     *
     * @return array
     */
    public function all(): array
    {
        return [
            'accountId'     => $this->accountId,
            'startDate'     => $this->startDate,
            'endDate'       => $this->endDate,
            'targets'       => $this->targetsToArray(),
            'productId'     => $this->productId,
            'quantity'      => $this->quantity,
            'total'         => $this->total,
            'unitCost'      => $this->unitCost,
            'promoCodeName' => $this->promoCodeName,
            'displayBudget' => $this->displayBudget,
        ];
    }
}
