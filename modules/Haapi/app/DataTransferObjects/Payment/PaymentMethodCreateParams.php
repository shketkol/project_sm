<?php

namespace Modules\Haapi\DataTransferObjects\Payment;

use Spatie\DataTransferObject\DataTransferObject;

class PaymentMethodCreateParams extends DataTransferObject
{
    /**
     * Payment method namespace
     *
     * @var string
     */
    public $namespace;

    /**
     * User account haapi id
     *
     * @var string
     */
    public $accountId;

    /**
     * Payment method token
     *
     * @var string
     */
    public $paymentToken;
}
