<?php

namespace Modules\Haapi\DataTransferObjects\Payment;

use Spatie\DataTransferObject\DataTransferObject;

class PaymentMethodDeleteParams extends DataTransferObject
{
    /**
     * Payment method id
     *
     * @var string
     */
    public $paymentMethodId;
}
