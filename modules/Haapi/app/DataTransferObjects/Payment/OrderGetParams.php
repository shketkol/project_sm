<?php

namespace Modules\Haapi\DataTransferObjects\Payment;

use Spatie\DataTransferObject\DataTransferObject;

class OrderGetParams extends DataTransferObject
{
    /**
     * The advertiser account UUID in HAAPI, Required
     *
     * @var string
     */
    public $accountId;

    /**
     * The ​ Operative Sales Order ID associated with the campaign, Required
     *
     * @var string
     */
    public $orderId;
}
