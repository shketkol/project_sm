<?php

namespace Modules\Haapi\DataTransferObjects\Payment;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class OrderGetBillingDateParams extends DataTransferObject
{
    /**
     * The campaign order start date, Required
     *
     * @var string
     */
    public $orderStartDate;

    public function __construct(array $parameters = [])
    {
        $dateFormatted = Carbon::parse(
            Arr::get($parameters, 'orderStartDate')
        )->format(config('date.format.daapi'));

        parent::__construct([
            'orderStartDate' => $dateFormatted,
        ]);
    }
}
