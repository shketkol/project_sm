<?php

namespace Modules\Haapi\DataTransferObjects\Payment;

use Spatie\DataTransferObject\DataTransferObject;

class OrderListParams extends DataTransferObject
{
    /**
     * The advertiser account UUID, Required
     *
     * @var string
     */
    public $accountId;

    /**
     * This is the 0-based start index of the search results for the current request, Optional
     *
     * @var int|null
     */
    public $start;

    /**
     * This is the maximum number of records per page to be returned in the response.
     *
     * @var int|null
     */
    public $limit;

    /**
     * The search text to be matched with the name of the campaign. This is not a case-sensitive match.
     *
     * @var string|null
     */
    public $searchText;

    /**
     * To include only orders with a specific status in the response, Optional
     * provide the appropriate value: PENDING, CURRENT, FAILED, SETTLED
     *
     * @var string|null
     */
    public $orderStatusFilter;

    /**
     * By default, orders are listed by "lastActivityDate​" in the order defined by the "sortOrder​" value
     * Enums: "NAME", "TYPE", "ORDER ID", "AMOUNT", "LAST ACTIVITY"
     *
     * @var string|null
     */
    public $sortFieldName;

    /**
     * By default, orders are listed in DESCENDING order of the selected "sortFieldName" ​values, Optional.
     * To change the sorting order, provide the “ASCENDING”​ value for this field.
     *
     * @var string|null
     */
    public $sortOrder;
}
