<?php

namespace Modules\Haapi\DataTransferObjects\Payment;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class OrderGetBillingDateData extends DataTransferObject
{
    /**
     * The name of the billing period, for example, "25-Jan-2020".
     *
     * @var string
     */
    public $billingPeriodName;

    /**
     * The date when the advertiser will be billed during the specified billing period.
     *
     * @var string
     */
    public $billingDate;

    public function __construct(array $data)
    {
        parent::__construct([
            'billingPeriodName' => Arr::get($data, 'nextBillingDate.billingPeriodName'),
            'billingDate'       => Arr::get($data, 'nextBillingDate.billingDate'),
        ]);
    }
}
