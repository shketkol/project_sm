<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Spatie\DataTransferObject\DataTransferObject;

class TargetParams extends DataTransferObject
{
    /**
     * @var string
     */
    public $typeId;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Campaign\TargetValueParams[]
     */
    public $values;

    /**
     * Get all instances as an array
     *
     * @return array
     */
    public function all(): array
    {
        $valueParams = array_map(function ($value) {
            return $value->all();
        }, $this->values);

        return [
            'typeId' => $this->typeId,
            'values' => $valueParams
        ];
    }
}
