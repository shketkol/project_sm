<?php

namespace Modules\Haapi\DataTransferObjects\Campaign\Traits;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Campaign\LineItemParams;
use Modules\Haapi\DataTransferObjects\Campaign\TargetParams;
use Modules\Haapi\DataTransferObjects\Campaign\TargetValueParams;
use Modules\Haapi\DataTransferObjects\Campaign\UpdateLineItemParams;

/**
 * @property LineItemParams[]|UpdateLineItemParams[] $lineItems
 * @property TargetParams[]                          $targets
 */
trait CampaignParamsTrait
{
    /**
     * @return array[]
     */
    private function lineItemsToArray(): array
    {
        return array_map(function ($lineItem): array {
            return $lineItem->all();
        }, $this->lineItems);
    }

    /**
     * @param array[] $targets
     *
     * @return TargetParams[]
     */
    private function getTargets(array $targets): array
    {
        return array_map(function (array $target): TargetParams {
            return new TargetParams([
                'typeId' => Arr::get($target, 'typeId'),
                'values' => $this->targetValues(Arr::get($target, 'values')),
            ]);
        }, $targets);
    }

    /**
     * @param array[] $targetValues
     *
     * @return TargetValueParams[]
     */
    private function targetValues(array $targetValues): array
    {
        return array_map(
            function (array $targetValue): TargetValueParams {
                return new TargetValueParams([
                    'value'     => Arr::get($targetValue, 'value'),
                    'exclusion' => Arr::get($targetValue, 'exclusion'),
                ]);
            },
            $targetValues
        );
    }

    /**
     * @return array
     */
    private function targetsToArray(): array
    {
        return array_map(function (TargetParams $target) {
            return $target->all();
        }, $this->targets);
    }
}
