<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class ImpressionsDetailParams extends DataTransferObject
{

    /**
     * Daily rollup
     */
    public const ROLL_UP_DAILY = 'DAILY';

    /**
     * Monthly rollup
     */
    public const ROLL_UP_MONTHLY = 'MONTHLY';

    /**
     * Lifetime rollup
     */
    public const ROLL_UP_LIFETIME = 'LIFETIME';

    /**
     * UUID, required
     *
     * @var string
     */
    public $advertiserId;

    /**
     * Campaign Ids, required
     *
     * @var array
     */
    public $campaignIds;

    /**
     * Required,
     * The frequency for the retrieved impression data
     * breakdown within the specified reporting period.
     * The supported values include:
     * "DAILY"
     * "MONTHLY"
     * "LIFETIME" (to date)
     *
     * @var string
     */
    public $rollUpType;

    /**
     * Optional,
     * The start date of the reporting period for
     * which impression details are to be retrieved.
     *
     * @var string|null
     */
    public $startDate;

    /**
     * Optional,
     * The end dates of the reporting period for
     * which impression details are to be retrieved.
     *
     * @var string|null
     */
    public $endDate;

    /**
     * @return array
     */
    public function all(): array
    {
        $all = parent::all();
        Arr::set($all, 'campaignId', Arr::get($all, 'campaignIds'));
        unset($all['campaignIds']);

        return $all;
    }
}
