<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Spatie\DataTransferObject\DataTransferObject;

class FrequencyMetricsParams extends DataTransferObject
{
    /**
     * Campaign Id, required
     *
     * @var string
     */
    public $campaignId;
}
