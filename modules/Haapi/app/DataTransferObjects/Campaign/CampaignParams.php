<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Modules\Haapi\DataTransferObjects\Campaign\Traits\CampaignParamsTrait;
use Spatie\DataTransferObject\DataTransferObject;

class CampaignParams extends DataTransferObject
{
    use CampaignParamsTrait;

    /**
     * Campaign id
     *
     * @var int
     */
    public $sourceId;

    /**
     * Campaign external id
     *
     * @var string|null
     */
    public $externalId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $advertiserId;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Campaign\LineItemParams[]
     */
    public $lineItems;

    /**
     * @var string|null
     */
    public $promoCodeName;

    /**
     * @var float
     */
    public $displayBudget;

    /**
     * Get as an array
     *
     * @return array
     */
    public function all(): array
    {
        return [
            'campaign' => [
                'sourceId'      => $this->sourceId,
                'externalId'    => $this->externalId,
                'name'          => $this->name,
                'advertiserId'  => $this->advertiserId,
                'displayBudget' => $this->displayBudget,
                'lineItems'     => $this->lineItemsToArray(),
                'promoCodeName' => $this->promoCodeName,
            ],
        ];
    }
}
