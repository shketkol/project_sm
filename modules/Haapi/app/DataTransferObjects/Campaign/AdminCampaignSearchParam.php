<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class AdminCampaignSearchParam extends DataTransferObject
{
    /**
     * Start param, default 0, Optional
     *
     * @var int|null
     */
    public $start;

    /**
     * Limit param, default 10, Optional
     *
     * @var int|null
     */
    public $limit;

    /**
     * Account id, Optional
     *
     * @var string|null
     */
    public $accountId;

    /**
     * The start date of the period for which campaign impression details are to be retrieved.
     * String, Optional (format: yyyy-MM-dd)
     *
     * @var string|null
     */
    public $impressionCountStartTime;

    /**
     * The end date of the period for which campaign impression details are to be retrieved.
     * String, Optional (format: yyyy-MM-dd)
     *
     * @var string|null
     */
    public $impressionCountEndTime;

    /**
     * Use this filter to return only those campaigns that have at least one line item that starts after
     * the specified date and time.
     * String, Optional (format: yyyy-MM-ddThh:mm:ss.SSSZ)
     *
     * @var string|null
     */
    public $campaignStartBefore;

    /**
     * Use this filter to return only those campaigns that have at least one line item that ends after
     * the specified date and time.
     * String, Optional (format: yyyy-MM-ddThh:mm:ss.SSSZ)
     *
     * @var string|null
     */
    public $campaignEndAfter;
}
