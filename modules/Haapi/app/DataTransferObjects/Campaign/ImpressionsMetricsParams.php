<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Spatie\DataTransferObject\DataTransferObject;

class ImpressionsMetricsParams extends DataTransferObject
{
    /**
     * Metrics type Genre
     */
    public const TYPE_GENRE = 'GENRE';

    /**
     * Metrics type Device
     */
    public const TYPE_DEVICE = 'DEVICE';

    /**
     * Metrics type DMA
     */
    public const TYPE_DMA = 'DMA';

    /**
     * Campaign Id, required
     *
     * @var string
     */
    public $campaignId;

    /**
     * Required,
     * The type of metric used. The supported values include:
     * "GENRE"
     * "DEVICE"
     * "DMA"
     *
     * @var string
     */
    public $type;
}
