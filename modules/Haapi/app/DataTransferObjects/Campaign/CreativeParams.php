<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\CreativeCreationData;
use Spatie\DataTransferObject\DataTransferObject;

class CreativeParams extends DataTransferObject
{
    /**
     * Creative UUID, optional
     *
     * @var string|null
     */
    public $creativeId;

    /**
     * Creative creation, optional
     *
     * @var \Modules\Daapi\DataTransferObjects\Types\CreativeCreationData|null
     */
    public $creativeCreation;

    /**
     * CreativeParams constructor.
     *
     * @param array $payloads
     */
    public function __construct(array $payloads)
    {
        $creativeCreation = Arr::get($payloads, 'creativeCreation');
        parent::__construct(
            [
                'creativeId' => Arr::get($payloads, 'creativeId'),
                'creativeCreation' => $creativeCreation ? new CreativeCreationData($creativeCreation) : null
            ]
        );
    }

    public function all(): array
    {
        return [
            'creativeId' => $this->creativeId,
            'creativeCreation' => $this->creativeCreation ? $this->creativeCreation->all() : $this->creativeCreation
        ];
    }
}
