<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Campaign\Traits\CampaignParamsTrait;
use Spatie\DataTransferObject\DataTransferObject;

class LineItemParams extends DataTransferObject
{
    use CampaignParamsTrait;

    /**
     * Campaign id
     *
     * @var int
     */
    public $sourceId;

    /**
     * Start date, required
     *
     * @var string
     */
    public $startDate;

    /**
     * End data, required
     *
     * @var string
     */
    public $endDate;

    /**
     * Product id, required
     *
     * @var string
     */
    public $productId;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Campaign\TargetParams[]
     */
    public $targets;

    /**
     * Unit cost
     *
     * @var float
     */
    public $unitCost;

    /**
     * Unit cost
     *
     * @var float|null
     */
    public $discountedUnitCost;

    /**
     * Cost model
     *
     * @var string
     */
    public $costModel;

    /**
     * Quantity
     *
     * @var int
     */
    public $quantity;

    /**
     * Quantity type
     *
     * @var string
     */
    public $quantityType;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Campaign\CreativeParams|null;
     */
    public $creative;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $creative = Arr::get($parameters, 'creative');
        parent::__construct([
            'sourceId'           => Arr::get($parameters, 'sourceId'),
            'startDate'          => Arr::get($parameters, 'startDate'),
            'endDate'            => Arr::get($parameters, 'endDate'),
            'productId'          => config('daapi.campaign.productId'),
            'targets'            => $this->getTargets(Arr::get($parameters, 'targets', [])),
            'unitCost'           => Arr::get($parameters, 'unitCost'),
            'discountedUnitCost' => Arr::get($parameters, 'discountedUnitCost'),
            'costModel'          => config('daapi.campaign.costModel'),
            'quantity'           => Arr::get($parameters, 'quantity'),
            'quantityType'       => config('daapi.campaign.quantityType'),
            'creative'           => $creative ? $this->getCreativeParams($creative) : $creative,
        ]);
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return [
            'sourceId'           => $this->sourceId,
            'startDate'          => $this->startDate,
            'endDate'            => $this->endDate,
            'productId'          => $this->productId,
            'targets'            => $this->targetsToArray(),
            'unitCost'           => $this->unitCost,
            'discountedUnitCost' => $this->discountedUnitCost,
            'costModel'          => $this->costModel,
            'quantity'           => $this->quantity,
            'quantityType'       => $this->quantityType,
            'creative'           => $this->creative ? $this->creative->all() : $this->creative,
        ];
    }

    /**
     * @param array|null $creative
     *
     * @return CreativeParams
     */
    private function getCreativeParams(?array $creative): ?CreativeParams
    {
        if (!$creative) {
            return null;
        }

        return new CreativeParams(
            Arr::only($creative, ['creativeId', 'creativeCreation'])
        );
    }
}
