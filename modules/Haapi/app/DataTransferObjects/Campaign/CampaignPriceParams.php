<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Campaign\Traits\CampaignParamsTrait;
use Spatie\DataTransferObject\DataTransferObject;

class CampaignPriceParams extends DataTransferObject
{
    use CampaignParamsTrait;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Campaign\TargetParams[]
     */
    public $targets;

    /**
     * @var string
     */
    public $productId;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        parent::__construct([
            'targets'   => $this->getTargets(Arr::get($parameters, 'targets', [])),
            'productId' => Arr::get($parameters, 'productId'),
        ]);
    }

    /**
     * Get all instances as an array
     *
     * @return array
     */
    public function all(): array
    {
        return [
            'targets'   => $this->targetsToArray(),
            'productId' => $this->productId,
        ];
    }
}
