<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Campaign\Traits\CampaignParamsTrait;
use Spatie\DataTransferObject\DataTransferObject;

class InventoryCheckParams extends DataTransferObject
{
    use CampaignParamsTrait;

    /**
     * @var string
     */
    public $campaignSourceId;

    /**
     * @var string
     */
    public $startDate;

    /**
     * @var string
     */
    public $endDate;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Campaign\TargetParams[]
     */
    public $targets;

    /**
     * @var string
     */
    public $productId;

    /**
     * @var int|null
     */
    public $quantity;

    /**
     * @var string
     */
    public $quantityType;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        parent::__construct([
            'campaignSourceId' => (string)Arr::get($parameters, 'campaignSourceId'),
            'startDate'        => Arr::get($parameters, 'startDate', ''),
            'endDate'          => Arr::get($parameters, 'endDate', ''),
            'targets'          => $this->getTargets(Arr::get($parameters, 'targets', [])),
            'productId'        => Arr::get($parameters, 'productId'),
            'quantity'         => Arr::get($parameters, 'quantity'),
            'quantityType'     => Arr::get($parameters, 'quantityType'),
        ]);
    }

    /**
     * Get all instances as an array
     *
     * @return array
     */
    public function all(): array
    {
        return [
            'campaignSourceId' => $this->campaignSourceId,
            'startDate'        => $this->startDate,
            'endDate'          => $this->endDate,
            'targets'          => $this->targetsToArray(),
            'productId'        => $this->productId,
            'quantity'         => $this->quantity,
            'quantityType'     => $this->quantityType,
        ];
    }
}
