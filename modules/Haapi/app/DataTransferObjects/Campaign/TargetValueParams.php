<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Spatie\DataTransferObject\DataTransferObject;

class TargetValueParams extends DataTransferObject
{
    /**
     * @var string
     */
    public $value;

    /**
     * @var int|null
     */
    public $exclusion;
}
