<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Campaign\Traits\CampaignParamsTrait;
use Spatie\DataTransferObject\DataTransferObject;

class UpdateLineItemParams extends DataTransferObject
{
    use CampaignParamsTrait;

    /**
     * @var string
     */
    public $id;

    /**
     * Start date, required
     *
     * @var string
     */
    public $startDate;

    /**
     * End data, required
     *
     * @var string
     */
    public $endDate;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Campaign\TargetParams[]
     */
    public $targets;

    /**
     * Unit cost
     *
     * @var float
     */
    public $unitCost;

    /**
     * Quantity
     *
     * @var int
     */
    public $quantity;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        parent::__construct([
            'id'        => Arr::get($parameters, 'id'),
            'startDate' => Arr::get($parameters, 'startDate'),
            'endDate'   => Arr::get($parameters, 'endDate'),
            'targets'   => $this->getTargets(Arr::get($parameters, 'targets', [])),
            'unitCost'  => Arr::get($parameters, 'unitCost'),
            'quantity'  => Arr::get($parameters, 'quantity'),
        ]);
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return [
            'id'        => $this->id,
            'startDate' => $this->startDate,
            'endDate'   => $this->endDate,
            'targets'   => $this->targetsToArray(),
            'unitCost'  => $this->unitCost,
            'quantity'  => $this->quantity,
        ];
    }
}
