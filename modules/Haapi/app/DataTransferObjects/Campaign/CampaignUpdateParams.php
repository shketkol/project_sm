<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Modules\Haapi\DataTransferObjects\Campaign\Traits\CampaignParamsTrait;
use Spatie\DataTransferObject\DataTransferObject;

class CampaignUpdateParams extends DataTransferObject
{
    use CampaignParamsTrait;

    /**
     * Campaign external id
     *
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var float
     */
    public $displayBudget;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Campaign\UpdateLineItemParams[]
     */
    public $lineItems;

    /**
     * Get as an array
     *
     * @return array
     */
    public function all(): array
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'displayBudget' => $this->displayBudget,
            'lineItems'     => $this->lineItemsToArray(),
        ];
    }
}
