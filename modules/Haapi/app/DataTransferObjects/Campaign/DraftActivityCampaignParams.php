<?php

namespace Modules\Haapi\DataTransferObjects\Campaign;

use Illuminate\Support\Arr;

class DraftActivityCampaignParams extends CampaignParams
{
    /**
     * Campaign step mapped to HAAPI
     *
     * @var string
     */
    public $latestAction;

    /**
     * Last visited page by advertiser in campaign wizard
     *
     * @var string
     */
    public $pageLastVisited;

    /**
     * Get as an array
     *
     * @return array
     */
    public function all(): array
    {
        $params = parent::all();

        Arr::set($params, 'campaign.latestAction', $this->latestAction);
        Arr::set($params, 'campaign.pageLastVisited', $this->pageLastVisited);

        return $params;
    }
}
