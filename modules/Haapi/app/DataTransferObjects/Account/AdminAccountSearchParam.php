<?php

namespace Modules\Haapi\DataTransferObjects\Account;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class AdminAccountSearchParam extends DataTransferObject
{
    /**
     * Start param, default 0, Optional
     *
     * @var int|null
     */
    public $start;

    /**
     * Limit param, default 10, Optional
     *
     * @var int|null
     */
    public $limit;

    /**
     * This field shall contain the text to be matched with either Advertiser Name or Advertiser email.
     * This is a case-insensitive match, Optional
     *
     * @var string|null
     */
    public $searchText;

    /**
     * When provided, only Advertisers with this account status shall be included in the response, Optional
     *
     * @var string|null
     */
    public $accountStatusFilter;

    /**
     * When provided, the results returned shall be sorted based on the value of this field in the advertiser.
     * Default value for this parameter is account creation date. Optional
     * Allowed values are “Name”, “Email”, “Spend”, “Campaign Count”, “Status”
     *
     * @var string|null
     */
    public $sortFieldName;

    /**
     * When provided, the results returned in the response will follow a sort order of the given type. Optional
     * Default value is DESCENDING
     *
     * @var string|null
     */
    public $sortOrder;
}
