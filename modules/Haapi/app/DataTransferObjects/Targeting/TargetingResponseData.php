<?php

namespace Modules\Haapi\DataTransferObjects\Targeting;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Response with targeting iteration from HAAPI.
 */
class TargetingResponseData extends DataTransferObject
{
    /**
     * @var int|null
     */
    public $next;

    /**
     * @var \Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection
     */
    public $targetValues;

    /**
     * @var string
     */
    public $externalId;

    /**
     * @var int
     */
    public $limit;

    /**
     * @var int
     */
    public $start;

    /**
     * @param array $data
     *
     * @return self
     */
    public static function create(array $data): self
    {
        return new self([
            'next'         => Arr::get($data, 'next'),
            'targetValues' => TargetingValuesCollection::create(Arr::get($data, 'targetValues', [])),
            'externalId'   => Arr::get($data, 'externalId'),
            'limit'        => Arr::get($data, 'limit'),
            'start'        => Arr::get($data, 'start'),
        ]);
    }
}
