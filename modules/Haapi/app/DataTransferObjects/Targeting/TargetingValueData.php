<?php

namespace Modules\Haapi\DataTransferObjects\Targeting;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Import values from HAAPI.
 */
class TargetingValueData extends DataTransferObject
{
    /**
     * @var string
     */
    public $external_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var null|array
     */
    public $hierarchy;

    /**
     * @var null|bool
     */
    public $visible;

    /**
     * @param array $data
     *
     * @return self
     */
    public static function create(array $data): self
    {
        return new self([
            'external_id' => Arr::get($data, 'id'),
            'name'        => Arr::get($data, 'name'),
            'hierarchy'   => Arr::get($data, 'hierarchy', []),
            'visible'     => Arr::get($data, 'visible'),
        ]);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();

        if (!$this->hierarchy) {
            Arr::forget($data, 'hierarchy');
        }

        if (is_null($this->visible)) {
            Arr::forget($data, 'visible');
        }

        return $data;
    }
}
