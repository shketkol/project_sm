<?php

namespace Modules\Haapi\DataTransferObjects\Targeting;

use Spatie\DataTransferObject\DataTransferObjectCollection;

class TargetingValuesCollection extends DataTransferObjectCollection
{
    /**
     * @return TargetingValueData
     */
    public function current(): TargetingValueData
    {
        return parent::current();
    }

    /**
     * @param array $data
     *
     * @return self
     */
    public static function create(array $data): self
    {
        $collection = array_map(function (array $item): TargetingValueData {
            return TargetingValueData::create($item);
        }, $data);

        return new self($collection);
    }
}
