<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone_number');
            $table->dropColumn('email_verified_at');
            $table->dropColumn('password');

            $table->string('external_id')->after('id')->nullable();
            $table->string('company_external_id')->after('external_id')->nullable();
            $table->string('company_name')->after('company_external_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('external_id');
            $table->dropColumn('company_external_id');
            $table->dropColumn('company_name');

            $table->string('phone_number')->after('last_name');
            $table->string('password')->nullable();
            $table->timestamp('email_verified_at')->nullable();
        });
    }
}
