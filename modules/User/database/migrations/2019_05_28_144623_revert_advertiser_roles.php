<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Modules\User\Models\Role;
use Modules\User\Models\User;

class RevertAdvertiserRoles extends Migration
{
    /**
     * At first advertiser role was '2'.
     * When we separated admins into 2 roles, advertiser role becomes '3'.
     * So all advertisers created before separation automatically becomes read-only admins.
     * This migration fixes it.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('model_has_roles')
            ->where('model_type', User::class)
            ->where('role_id', Role::ID_ADMIN_READ_ONLY)
            ->update(['role_id' => Role::ID_ADVERTISER]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        //
    }
}
