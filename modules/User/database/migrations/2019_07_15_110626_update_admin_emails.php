<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * NOTE: Currently N/A
 */
class UpdateAdminEmails extends Migration
{
    /**
     * Old and new admin emails.
     *
     * @var array
     */
    protected $admins = [
        'danadstest@gmail.com' => 'dev.team@danads.se',
        'danadstest2@gmail.com' => 'mikhail.chupyr@danads.se',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->update();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->update(true);
    }

    /**
     * Update admins' emails in database.
     *
     * @param bool|null $reversed
     */
    protected function update(?bool $reversed = false)
    {
        foreach ($this->admins as $old => $new) {
            if ($reversed) {
                list($old, $new) = [$new, $old];
            }

            DB::table('users')->where('email', $old)->update(['email' => $new]);
        }
    }
}
