<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableForRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('approved_at')->nullable();
            $table->string('password')->nullable()->change();

            $table->dropForeign(['status_id']);
            $table->foreign('status_id')
                  ->references('id')
                  ->on('user_statuses')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('approved_at');
            $table->dropForeign(['status_id']);
            $table->dropColumn(['status_id']);

            $table->string('password')->change();
        });
    }
}
