<?php

namespace Modules\User\Database\Factories;

use App\Factories\Factory;
use Carbon\Carbon;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;
use Modules\User\Models\AccountType;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'status_id'           => UserStatus::ID_ACTIVE,
            'last_login'          => Carbon::now(),
            'company_name'        => $this->faker->word,
            'external_id'         => $this->faker->uuid,
            'account_external_id' => $this->faker->uuid,
            'account_type_id'     => AccountType::ID_COMMON,
        ];
    }
}
