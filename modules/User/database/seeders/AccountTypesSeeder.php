<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\User\Models\AccountType;
use Modules\User\Models\User;

class AccountTypesSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run(): void
    {
        $this->seedAccountTypes();
        $this->setDefaultAccountTypeToUsers();
    }

    /**
     * Run the database seeds to add account types.
     *
     * @return void
     */
    private function seedAccountTypes()
    {
        $types = [
            [
                'id'                  => AccountType::ID_COMMON,
                'name'                => AccountType::TYPE_COMMON,
                'targeting_ages'      => true,
                'targeting_audiences' => true,
                'targeting_locations' => true,
                'targeting_zips'      => true,
                'targeting_genres'    => true,
            ],
            [
                'id'                  => AccountType::ID_SPECIAL_ADS,
                'name'                => AccountType::TYPE_SPECIAL_ADS,
                'targeting_ages'      => false,
                'targeting_audiences' => false,
                'targeting_locations' => true,
                'targeting_zips'      => false,
                'targeting_genres'    => false,
            ],
        ];

        foreach ($types as $type) {
            AccountType::updateOrCreate(
                ['id' => $type['id']],
                $type
            );
        }
    }

    /**
     * Set default account type to users without account type.
     *
     * @return void
     */
    private function setDefaultAccountTypeToUsers()
    {
        DB::table((new User())->getTable())
            ->whereNull('account_type_id')
            ->update(['account_type_id' => AccountType::ID_COMMON]);
    }
}
