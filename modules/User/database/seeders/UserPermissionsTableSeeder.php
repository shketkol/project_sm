<?php

namespace Modules\User\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\User\Models\Role;
use Modules\User\Policies\UserPolicy;

class UserPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [],
        Role::ID_ADMIN_READ_ONLY => [],
        Role::ID_ADVERTISER      => [
            UserPolicy::PERMISSION_UPDATE_PROFILE,
        ],
    ];
}
