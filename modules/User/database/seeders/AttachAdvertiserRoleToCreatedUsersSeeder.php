<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\User\Actions\AssignRolesAction;
use Modules\User\Models\Role;
use Modules\User\Models\User;

class AttachAdvertiserRoleToCreatedUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        /** @var AssignRolesAction $assignRoleAction */
        $assignRoleAction = app(AssignRolesAction::class);

        User::all()->each(function (User $user) use ($assignRoleAction): void {
            if (!$user->roles()->exists()) {
                $assignRoleAction->handle($user, [Role::ID_ADVERTISER]);
            }
        });
    }
}
