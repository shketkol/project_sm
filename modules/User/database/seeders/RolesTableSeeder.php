<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\User\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $roles = [
            [
                'id'   => Role::ID_ADMIN,
                'name' => Role::ADMIN,
            ],
            [
                'id'   => Role::ID_ADMIN_READ_ONLY,
                'name' => Role::ADMIN_READ_ONLY,
            ],
            [
                'id'   => Role::ID_ADVERTISER,
                'name' => Role::ADVERTISER,
            ],
            [
                'id'   => Role::ID_TECHNICAL_ADMIN,
                'name' => Role::TECHNICAL_ADMIN,
            ],
        ];

        foreach ($roles as $role) {
            Role::updateOrCreate(
                ['id' => Arr::get($role, 'id')],
                $role
            );
        }
    }
}
