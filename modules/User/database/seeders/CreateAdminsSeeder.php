<?php

namespace Modules\User\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\User\Exceptions\AdminNotFoundException;
use Modules\User\Models\Role;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;
use Modules\User\Repositories\UserRepository;
use Psr\Log\LoggerInterface;

class CreateAdminsSeeder extends Seeder
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param UserRepository  $repository
     * @param LoggerInterface $log
     */
    public function __construct(UserRepository $repository, LoggerInterface $log)
    {
        $this->repository = $repository;
        $this->log = $log;
    }

    /**
     * There must always be a Technical Admin.
     *
     * @return void
     * @throws \Throwable
     */
    public function run(): void
    {
        $this->log->info('Running seeder to create Technical Admin.');

        try {
            $this->repository->getTechnicalAdmin();
            $this->log->info('Technical Admin already exists. Skip creation.');
        } catch (AdminNotFoundException $exception) {
            $this->log->info('Technical Admin not exists. Creating.');
            $this->dropOldTechnicalAdmins();
            $this->create();
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * External id required only if admin wants to get info about user or another admin.
     * Since we don't need to fetch data about technical admin by anyone except himself,
     * We prune this field to not confuse with wrong value.
     */
    private function dropOldTechnicalAdmins(): void
    {
        $this->log->info('Drop old Technical Admins.');

        $count = 0;
        $count += User::where('external_id', '01fc93fb-98ff-4557-a63c-8fde0551e0b9')->delete();
        $count += User::where('external_id', 'bf094aa7-1af8-4f74-bf11-723d3ab38542')->delete();

        $this->log->info(sprintf('Deleted %d Technical Admin(s).', $count));
    }

    /**
     * @throws \Exception
     */
    private function create(): void
    {
        $data = [
            'user'  => [
                'status_id'    => UserStatus::ID_ACTIVE,
                'company_name' => '[ENV] Admin',
            ],
            'roles' => [Role::ID_ADMIN_READ_ONLY, Role::ID_TECHNICAL_ADMIN],
        ];

        $this->log->info('Started creating Technical Admin.', ['data' => $data]);

        Arr::set($data, 'user.approved_at', Carbon::now());

        /** @var User $admin */
        $admin = $this->repository->create(Arr::get($data, 'user'));

        if (is_null($admin)) {
            $this->log->warning('Technical Admin not created.', ['data' => $data]);
            return;
        }

        $admin->syncRoles(Arr::get($data, 'roles'));

        $this->log->info('Technical Admin created.', [
            'user_id' => $admin->id,
            'data'    => $data,
        ]);
    }
}
