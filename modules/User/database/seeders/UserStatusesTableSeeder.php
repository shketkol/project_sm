<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\User\Models\UserStatus;

class UserStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds to add standard user statuses.
     *
     * @return void
     */
    public function run(): void
    {
        $statuses = [
            [
                'id'   => UserStatus::ID_CREATE_IN_PROGRESS,
                'name' => UserStatus::CREATE_IN_PROGRESS,
            ],
            [
                'id'   => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'name' => UserStatus::PENDING_MANUAL_REVIEW,
            ],
            [
                'id'   => UserStatus::ID_ACTIVE,
                'name' => UserStatus::ACTIVE,
            ],
            [
                'id'   => UserStatus::ID_INACTIVE,
                'name' => UserStatus::INACTIVE,
            ],
            [
                'id'   => UserStatus::ID_INACTIVE_UNPAID,
                'name' => UserStatus::INACTIVE_UNPAID,
            ],
            [
                'id'   => UserStatus::ID_INCOMPLETE,
                'name' => UserStatus::INCOMPLETE,
            ],
        ];

        foreach ($statuses as $status) {
            UserStatus::updateOrCreate(
                ['id' => Arr::get($status, 'id')],
                $status
            );
        }
    }
}
