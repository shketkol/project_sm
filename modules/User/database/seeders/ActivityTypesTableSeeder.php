<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\User\Models\Activity\ActivityType;

class ActivityTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $records = [
            [
                'id'   => ActivityType::ID_LOGIN,
                'name' => ActivityType::LOGIN,
            ],
            [
                'id'   => ActivityType::ID_LOGOUT,
                'name' => ActivityType::LOGOUT,
            ],
        ];

        foreach ($records as $record) {
            ActivityType::updateOrCreate(
                ['id' => Arr::get($record, 'id')],
                $record
            );
        }
    }
}
