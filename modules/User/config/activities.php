<?php

return [
    'database' => [
        'store_months' => env('ACTIVITIES_DATABASE_STORE_MONTHS', 1),
    ],
];
