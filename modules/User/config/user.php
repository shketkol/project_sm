<?php

return [

    'accessors'                      => [
        'cache_lifetime' => 1800 //30min
    ],
    /*
    |--------------------------------------------------------------------------
    | Interval of checking possibility of updating business info status
    |--------------------------------------------------------------------------
    |
    | This option defines interval between checks
    |
    */
    'check_update_business_interval' => env('CHECK_UPDATE_BUSINESS_INTERVAL', 5000) , // In milliseconds
];
