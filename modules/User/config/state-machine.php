<?php

use \Modules\User\Models\UserStatus;
use \Modules\User\Models\User;

return [
    /**
     * User workflow graph
     */
    'user' => [
        // class of your domain object
        'class'         => User::class,

        // name of the graph (default is "default")
        'graph'         => 'user',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status_id',

        // list of all possible states
        'states'        => [
            [
                'name' => UserStatus::ID_CREATE_IN_PROGRESS,
            ],
            [
                'name' => UserStatus::ID_PENDING_MANUAL_REVIEW,
            ],
            [
                'name' => UserStatus::ID_ACTIVE,
            ],
            [
                'name' => UserStatus::ID_INACTIVE,
            ],
            [
                'name' => UserStatus::ID_INACTIVE_UNPAID,
            ],
        ],

        // list of all possible transitions
        'transitions'   => [
            UserStatus::CREATE_IN_PROGRESS    => [
                'from' => [
                    UserStatus::ID_PENDING_MANUAL_REVIEW,
                    UserStatus::ID_ACTIVE,
                    UserStatus::ID_INACTIVE,
                    UserStatus::ID_INACTIVE_UNPAID,
                ],
                'to'   => UserStatus::ID_CREATE_IN_PROGRESS,
            ],
            UserStatus::PENDING_MANUAL_REVIEW => [
                'from' => [
                    UserStatus::ID_CREATE_IN_PROGRESS,
                    UserStatus::ID_ACTIVE,
                    UserStatus::ID_INACTIVE,
                    UserStatus::ID_INACTIVE_UNPAID,
                ],
                'to'   => UserStatus::ID_PENDING_MANUAL_REVIEW,
            ],
            UserStatus::ACTIVE                => [
                'from' => [
                    UserStatus::ID_CREATE_IN_PROGRESS,
                    UserStatus::ID_PENDING_MANUAL_REVIEW,
                    UserStatus::ID_INACTIVE,
                    UserStatus::ID_INACTIVE_UNPAID,
                ],
                'to'   => UserStatus::ID_ACTIVE,
            ],
            UserStatus::INACTIVE              => [
                'from' => [
                    UserStatus::ID_CREATE_IN_PROGRESS,
                    UserStatus::ID_PENDING_MANUAL_REVIEW,
                    UserStatus::ID_ACTIVE,
                    UserStatus::ID_INACTIVE,
                    UserStatus::ID_INACTIVE_UNPAID,
                ],
                'to'   => UserStatus::ID_INACTIVE,
            ],
            UserStatus::INACTIVE_UNPAID       => [
                'from' => [
                    UserStatus::ID_CREATE_IN_PROGRESS,
                    UserStatus::ID_PENDING_MANUAL_REVIEW,
                    UserStatus::ID_ACTIVE,
                    UserStatus::ID_INACTIVE,
                    UserStatus::ID_INACTIVE_UNPAID,
                ],
                'to'   => UserStatus::ID_INACTIVE_UNPAID,
            ],
        ],

        // list of all callbacks
        'callbacks'     => [
            // will be called when testing a transition
            'guard'  => [],

            // will be called before applying a transition
            'before' => [],

            // will be called after applying a transition
            'after'  => [
                'after_active' => [
                    // call the callback on a specific transition
                    'on'   => UserStatus::ACTIVE,
                    // will call the method of this class
                    'do'   => ['state-machine.user.states.active', 'after'],
                    // arguments for the callback
                    'args' => ['object'],
                ],
            ],
        ],
    ],
];
