@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <onboarding-page></onboarding-page>
    </div>
@endsection

@push('scripts')
    @include('common.includes.worldpay')
    <script src="{{ mix('js/user.js') }}"></script>
@endpush
