@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <profile-page></profile-page>
    </div>
@endsection

@push('scripts')
    @include('common.includes.worldpay')
    <script src="{{ mix('js/user.js') }}"></script>
@endpush
