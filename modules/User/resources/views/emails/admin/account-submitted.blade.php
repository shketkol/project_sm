@extends('common.email.layout-admin')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">{{ __('emails.hi', ['name' => $first_name]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">
        {{ __('user::emails.admin.account_submitted.body.business_name', [
            'company_name' => $company_name,
        ]) }}<br>
        {{ __('user::emails.admin.account_submitted.body.status', [
            'status_name' => $status_name,
        ]) }}<br>
        {{ __('user::emails.admin.account_submitted.body.advertiser_email', [
            'email' => $email,
        ]) }}<br>
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">
    {!! __('user::emails.admin.account_submitted.body.details', [
            'link' => view(
                    'common.email.part.link',
                    [
                        'link' => $review_account,
                        'text' => __('user::emails.admin.account_submitted.body.advertiser_details')
                    ]
                )->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>
@stop
