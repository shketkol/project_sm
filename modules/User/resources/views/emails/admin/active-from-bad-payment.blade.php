@extends('common.email.layout-admin')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">
        {{ __('user::emails.admin.active_from_bad_payment.body.company_had_been_reactivated', [
            'company_name' => $companyName,
        ]) }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('user::emails.admin.active_from_bad_payment.body.for_more_information_about_account', [
            'advertiser_details' => view('common.email.part.link', [
                'link' => $reviewAccount,
                'text' => __('advertiser::labels.advertiser_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>
@stop
