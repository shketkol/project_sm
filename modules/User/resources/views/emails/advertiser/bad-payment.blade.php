@extends('common.email.layout')

@section('body')
    @include('common.email.part.p', [
        'text' => __('emails.hi', ['name' => $firstName]),
    ])

    @include('common.email.part.p', [
        'text' => __('emails.business', ['business' => $companyName]),
    ])

    @include('common.email.part.p', [
        'text' => __('user::emails.advertiser.bad_payment.body.unfortunately_there_has_been'),
    ])

    @include('common.email.part.p', [
        'text' => __('user::emails.advertiser.bad_payment.body.to_resume_your_campaigns'),
    ])

    @include('common.email.part.button', [
        'link' => $accountSettings,
        'text' => __('user::emails.advertiser.bad_payment_repeats.update_payment_now'),
    ])

    @include('common.email.part.p', [
        'text' => __('user::emails.advertiser.bad_payment.body.for_more_information_about_the_payment', [
            'campaign_details' => view('common.email.part.link', [
                'link' => $profile,
                'text' => __('campaign::labels.campaign_details'),
            ])->render(function ($content) {
                return trim($content);
            }),
        ]),
        'renderHtml' => true,
    ])

    @include('common.email.part.if-you-have-any-questions')
@stop
