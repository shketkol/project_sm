@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('user::emails.advertiser.pending_account_is_activated.body.your_account_for', [
            'publisher_company_full_name' => $publisherCompanyFullName,
            'company_name'                => $companyName,
        ]) }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('user::emails.advertiser.pending_account_is_activated.body.you_can_now_submit_or_resume') }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('user::emails.advertiser.pending_account_is_activated.body.click_link') }}
    </p>

    @include('common.email.part.button', [
        'link' => $bookNewCampaign,
        'text' => __('actions.submit_new_campaign'),
    ])
@stop
