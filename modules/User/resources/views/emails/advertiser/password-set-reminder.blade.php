@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('user::emails.advertiser.password_set_reminder.body.there_is_a_reminder', [
        'publisher_company_full_name' => $publisherCompanyFullName
    ]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('user::emails.advertiser.password_set_reminder.body.to_complete_the_signup') }}</p>

    @include('common.email.part.button', [
        'link' => $setPassword,
        'text' => __('actions.set_password'),
    ])

    @include('common.email.part.if-you-have-any-questions')
@stop
