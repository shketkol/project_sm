@extends('common.email.layout')

@section('body')
    <p  style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('emails.hi', ['name' => $firstName]) }}
    </p>

    <p  style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('user::emails.advertiser.approved.body.thank_you_for_signing_up', [
            'publisher_company_full_name' => $publisherCompanyFullName,
        ]) }}
    </p>

    <p  style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('user::emails.advertiser.approved.body.to_log_in_use_the_temporary_password') }}
    </p>

    <p  style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('user::emails.advertiser.approved.body.to_log_in', [
            'link' => view('common.email.part.link', [
                'link' => $loginLink,
                'text' => $publisherCompanyFullName,
            ])
        ]) !!}
    </p>

    <p  style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('user::emails.advertiser.approved.body.for_help', [
            'faqs' => view('common.email.part.link', [
                'link' => $faqs,
                'text' => __('faq.labels.user_guide_and_faq'),
            ])
        ]) !!}
    </p>

    @include('common.email.part.if-you-have-any-questions')
@stop
