@extends('common.email.layout')

@section('body')
    @include('common.email.part.p', [
        'text' => $firstName . ',',
    ])
    @include('common.email.part.p', [
        'text' => __('emails.business', ['business' => $companyName]),
    ])
    @include('common.email.part.p', [
        'text' => __('user::emails.advertiser.bad_payment_repeats.repeat_1.body.we_noticed'),
    ])

    @include('user::emails.advertiser.bad-payment-repeats.instructions')

    @include('common.email.part.if-you-have-any-questions')
@stop

@include('user::emails.advertiser.bad-payment-repeats.signature-thanks')
