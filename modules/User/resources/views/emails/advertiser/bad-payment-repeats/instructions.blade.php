@include('common.email.part.p', [
   'text' => __('user::emails.advertiser.bad_payment_repeats.instructions.steps_intro'),
])

@include('common.email.part.list', [
   'listItems' => [
       __('user::emails.advertiser.bad_payment_repeats.instructions.step_1', [
            'account_settings' => view('common.email.part.link', [
                'link' => $accountSettings,
                'text' => __('user::emails.advertiser.bad_payment_repeats.instructions.account_settings'),
            ])->render(function ($content) {
                    return trim($content);
                }),
       ]) . PHP_EOL . view('common.email.part.image', [
           'url' => url('/images/email/bad-payment-account.png'),
           'width' => 150,
       ])->render(function ($content) {
           return trim($content);
       }),
       __('user::emails.advertiser.bad_payment_repeats.instructions.step_2') . PHP_EOL .
       view('common.email.part.image', [
           'url' => url('/images/email/bad-payment-update-card.png'),
           'width' => 250,
       ])->render(function ($content) {
           return trim($content);
       }),
       __('user::emails.advertiser.bad_payment_repeats.instructions.step_3'),
       __('user::emails.advertiser.bad_payment_repeats.instructions.step_4') . PHP_EOL .
       view('common.email.part.image', [
           'url' => url('/images/email/bad-payment-card-info.png'),
           'width' => 250,
       ])->render(function ($content) {
           return trim($content);
       }),
       __('user::emails.advertiser.bad_payment_repeats.instructions.step_5')
    ],
   'numeric'=> true,
   'renderHtml' => true,
])

@include('common.email.part.p', [
   'text' => __('user::emails.advertiser.bad_payment.body.for_more_information_about_the_payment', [
       'campaign_details' => view('common.email.part.link', [
       'link' => $profile,
       'text' => __('campaign::labels.campaign_details'),
      ])->render(function ($content) {
          return trim($content);
      })
   ]),
   'renderHtml' => true,
])

@include('common.email.part.button', [
    'link' => $accountSettings,
    'text' => __('user::emails.advertiser.bad_payment_repeats.update_payment_now'),
])
