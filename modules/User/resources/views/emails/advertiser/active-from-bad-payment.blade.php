@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('user::emails.advertiser.active_from_bad_payment.body.thank_you_for_updating_your_payment') }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('user::emails.advertiser.active_from_bad_payment.body.for_more_information_about_the_campaign', [
            'campaign_details' => view('common.email.part.link', [
                'link' => $campaignListing,
                'text' => __('campaign::labels.campaigns_page'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    @include('common.email.part.if-you-have-any-questions')
@stop
