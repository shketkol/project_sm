@extends('common.email.layout')

@section('body')

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('emails.hi', ['name' => $firstName]) }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('user::emails.advertiser.rejected.body.thank_you_for_signing_up', [
            'publisher_company_full_name' => $publisherCompanyFullName,
        ]) }}
    </p>

    <p style="{{\App\Helpers\HtmlHelper::getMailTagCommonStyles('p')}}">
        {!! __('user::emails.advertiser.rejected.body.to_verify_your_account', [
            'contact_us' => view('common.email.part.link', [
                'link' => route('users.profile.contact'),
                'text' => __('support::actions.contact_us'),
            ])->render(function ($content) {
                return trim($content);
            }),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('user::emails.advertiser.rejected.body.in_the_meantime') }}
    </p>

@include('common.email.part.link', [
    'link' => $loginLink,
    'text' => __('labels.login_page_short_name'),
])

@stop
