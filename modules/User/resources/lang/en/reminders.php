<?php

return [

    'special_ads' => [
        'title' => 'Special Ads Category',
        'message' => 'Your business has been identified as belonging to the Special Ads category, which means that your targeting options will be limited to comply with the <a target="_blank" href=":url">Hulu Ad Manager Guidelines for Special Ads Categories.</a>',
        'cta_title' => 'Got it',
    ],
];
