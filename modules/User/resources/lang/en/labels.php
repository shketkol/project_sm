<?php

use Modules\User\Models\UserStatus;

return [
    'statuses' => [
        UserStatus::ACTIVE                => 'Active',
        UserStatus::CREATE_IN_PROGRESS    => 'In Progress',
        UserStatus::INACTIVE              => 'Inactive',
        UserStatus::INCOMPLETE            => 'Incomplete',
        UserStatus::PENDING_MANUAL_REVIEW => 'Pending',
        UserStatus::INACTIVE_UNPAID       => 'Bad Payment',
    ],
];
