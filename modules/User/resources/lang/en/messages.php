<?php

use Modules\User\Models\AccountType;

return [
    'not_found'    => 'Double-check your email address and/or password and try again.',
    'approve'      => [
        'failed'  => 'Error happened during approve User ID #:user_id and message ":message".',
        'success' => 'User ID #:user_id successfully approved.',
    ],
    'reject'       => [
        'failed'  => 'Error happened during reject User ID #:user_id and message ":message".',
        'success' => 'User ID #:user_id rejected.',
    ],
    'profile'      => [
        'update_personal'           => 'You\'ve updated your personal information.',
        'update_business'           => 'You\'ve updated your business information.',
        'update_business_review'    => 'You\'ve updated your business information.',
        'update_password'           => 'You\'ve updated your password.',
        'update_password_forbidden' => 'Double-check your password and try again.',
        'failed'                    => 'Something went wrong during the update. Please contact the administrator',
        'change_password'           => 'Change Password',
        'pending_info'              => '<b>ACCOUNT PENDING</b> <br><br> We’re verifying your business. If your account status is not updated within two business days, please use the <b>Contact Us</b> form to contact customer service and verify your business.',
    ],
    'account_type' => [
        AccountType::ID_COMMON      => 'Common',
        AccountType::ID_SPECIAL_ADS => 'Special Ads',
    ],
];
