<?php

return [
    'advertiser' => [
        'bad_payment'             => 'We could not process your payment. All your active campaigns have been paused. To resume your campaigns, update the payment information in your :profile as soon as possible.',
        'active_from_bad_payment' => 'Your account payment method has been updated. Any relevant suspended campaigns are now up and running again.',
        'not_valid' => [
            'title'   => 'Account is not validated',
            'message_p1' => 'You can\'t add a payment method, as your account has not yet been verified.',
            'message_p2' => 'For details & to verify your account, please use the <b>Contact Us</b> form.',
        ]
    ],
];
