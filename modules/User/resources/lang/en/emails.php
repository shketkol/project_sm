<?php

return [
    'admin'      => [
        'account_submitted'       => [
            'subject' => ':publisher_company_full_name Notification: New advertiser account reviewed',
            'body'    => [
                'business_name'      => 'We’ve completed our review of a new advertiser, :company_name',
                'status'             => 'Status: :status_name',
                'advertiser_email'   => 'Advertiser Email: :email',
                'details'            => 'For more information about the account, see :link.',
                'advertiser_details' => 'Advertiser details',
            ],
        ],
        'bad_payment'             => [
            'subject' => ':publisher_company_full_name Notification: :company_name Account suspended',
            'body'    => [
                'an_account_has_been_suspended'      => 'The :company_name account has been suspended due to payment issues.',
                'for_more_information_about_payment' => 'For more information about the current payment status of this account, see :campaign_details.',
            ],
        ],
        'active_from_bad_payment' => [
            'subject' => ':publisher_company_full_name Notification: :company_name Account reactivated',
            'body'    => [
                'company_had_been_reactivated'       => 'The :company_name account has resolved its payment issues and is active again.',
                'for_more_information_about_account' => 'For more information about the account, see :advertiser_details. ',
            ],
        ],
    ],
    'advertiser' => [
        'approved'                     => [
            'subject' => 'Your :publisher_company_full_name account was approved!',
            'title'   => 'Account Approved',
            'body'    => [
                'thank_you_for_signing_up'             => 'Thank you for signing up for :publisher_company_full_name! Your account has been approved, and you can now create and submit campaigns.',
                'to_log_in_use_the_temporary_password' => 'To log in, use the temporary password sent in a separate email. Or, if you’ve already set your own password, use that.',
                'to_log_in'                            => 'Log in at: :link.',
                'for_help'                             => 'For help, check out our :faqs.',
            ],
        ],
        'rejected'                     => [
            'subject' => 'Your :publisher_company_full_name account needs additional review',
            'title'   => 'Account Needs Additional Review',
            'body'    => [
                'thank_you_for_signing_up' => 'Thank you for signing up for :publisher_company_full_name! It appears that your account requires additional review before you can submit a campaign.',
                'to_verify_your_account'   => 'While we are verifying your business information you will see the <b>Account Pending</b> status displayed in Hulu Ad Manager. If your account status is not updated within two business days, please log in and use the <b>:contact_us</b> form at the bottom-right to contact customer service and verify your account.',
                'in_the_meantime'          => 'In the meantime, you can create campaign drafts. Unfortunately, you won’t be able to submit any campaigns until your account is verified.',
            ],
        ],
        'pending_account_is_activated' => [
            'subject' => ':company_name :publisher_company_full_name account is approved',
            'title'   => 'Account Approved',
            'body'    => [
                'your_account_for'             => 'Good news! Your :publisher_company_full_name account for :company_name has been approved.',
                'you_can_now_submit_or_resume' => 'You can now submit any campaigns you may have already created.',
                'click_link'                   => 'To submit a new campaign, click the link below:',
            ],
        ],
        'bad_payment'                  => [
            'subject' => ':publisher_company_full_name billing issue',
            'title'   => 'Billing Issue',
            'body'    => [
                'unfortunately_there_has_been'           => 'Unfortunately, we couldn’t process your payment and had to pause your campaigns.',
                'to_resume_your_campaigns'               => 'To resume your campaigns, please update the payment information in your account as soon as possible.',
                'for_more_information_about_the_payment' => 'For more information about the current payment status of your campaign, see :campaign_details.',
            ],
        ],
        'bad_payment_repeats'          => [
            'repeat_1'              => [
                'subject' => 'Second reminder: :publisher_company_full_name billing issue',
                'title'   => 'Your Campaign Is Suspended',
                'body'    => [
                    'we_noticed' => 'We noticed that you have not updated your payment method. Your campaigns will remain suspended and your Ads will not be able to run until you update your payment details.',
                ],
            ],
            'repeat_2'              => [
                'subject' => 'Attention needed: :publisher_company_full_name billing issue',
                'title'   => 'Your Campaign Is Suspended',
                'body'    => [
                    'we_noticed' => 'We have contacted you twice to let you know that your campaign is suspended because we do not have a current payment method on file. You will not be able to run your Ads or resume your suspended campaigns until you update your payment information.',
                ],
            ],
            'repeat_3'              => [
                'subject' => 'Final reminder: :publisher_company_full_name billing issue',
                'title'   => 'Account Suspended for Non-Payment',
                'body'    => [
                    'we_noticed' => 'We have contacted you several times to let you know that your campaign is suspended because we do not have a current payment method on file. You will not be able to run your Ads or resume your suspended campaigns until you update your payment information, and Hulu may take steps to collect past due amounts.',
                ],
            ],
            'instructions'          => [
                'steps_intro'      => 'To update your payment information:',
                'step_1'           => 'On the navigation bar, hover the mouse over your user name and select <b>Account</b> from the menu or click :account_settings.',
                'step_2'           => 'In the <b>Your Payment Information</b> section on the left, click <b>Update</b>.',
                'step_3'           => 'In the displayed form, click <b>Replace Card</b> at the bottom.',
                'step_4'           => 'Enter your new credit card information.',
                'step_5'           => 'Click <b>Save Changes</b>.',
                'account_settings' => 'Account Settings',
            ],
            'update_payment_now'    => 'Update Payment Now',
            'update_payment_method' => 'Update Your Payment Method',
            'thank_you_short'       => 'Thank you,',
            'thank_you_long'        => 'Thank you for your prompt attention to this matter.',
        ],
        'active_from_bad_payment'      => [
            'subject' => ':publisher_company_full_name billing issue resolved',
            'title'   => 'Billing Issue Resolved',
            'body'    => [
                'thank_you_for_updating_your_payment'     => 'Thank you for updating your payment information. Any relevant suspended campaigns are now up and running again.',
                'for_more_information_about_the_campaign' => 'For more information about your campaigns, see :campaign_details.',
            ],
        ],
        'password_set_reminder'        => [
            'subject' => ':publisher_company_full_name password set reminder',
            'title'   => ':publisher_company_full_name password set reminder',
            'body'    => [
                'there_is_a_reminder'    => 'Thank you for signing up for:publisher_company_full_name! We’re looking forward to helping you connect with consumers in a viewer-first ad experience.',
                'to_complete_the_signup' => 'To complete your account setup, click the link below and create your account password. (For the best experience, we recommend Chrome or Safari on a desktop).',
            ],
        ],
    ],
];
