<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Onboarding Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used only for onboarding page.
    |
    */

    'get_started' => 'Get Started',
    'subtitles'   => [
        'step_1' => 'Create Campaigns with Video Ads',
        'step_2' => 'Engage with your Audience',
        'step_3' => 'Track Performance of Video Ads',
    ],
    'text'        => 'A documentary that profiles the life and work of artist Shepard Fairey, following his roots in 
                      punk rock and skateboarding.',
    'welcome'     => 'Welcome to Hulu Advertising',
];
