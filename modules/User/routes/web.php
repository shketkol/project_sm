<?php

Route::middleware(['auth:web,admin'])->group(function () {
    // Profile
    Route::get('profile/{tab?}', 'ShowProfileController')->name('profile')->middleware('can:user.updateProfile');
    Route::get('profile/settings/contact', 'ShowProfileController')->name('profile.contact')
        ->middleware('can:user.updateProfile');
    Route::get('profile/transactions/{orderId}', 'ShowProfileController')
        ->name('profile.transactions')
        ->middleware('can:user.updateProfile');

    // Onboarding
    Route::get('onboarding/{steps?}/{step?}', 'ShowOnboardingController')->name('onboarding.show');
    Route::post('onboarding', 'CompleteOnboardingController')->name('onboarding.complete');
});
