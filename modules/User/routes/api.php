<?php

Route::middleware(['auth:web,admin'])->group(function () {
    Route::get('/profile', 'ShowProfileController')->name('profile');
    Route::patch('/update', 'UpdateUserController')->name('update');
    Route::patch('/company/update', 'UpdateCompanyController')->name('company.update');
    Route::get('/check-lock-update', 'CheckLockUpdateController')->name('check-lock-update');
});
