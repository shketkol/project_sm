<?php

namespace Modules\User\Mail\Advertiser;

use App\Mail\Mail;

class PasswordSetReminder extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('user::emails.advertiser.password_set_reminder.subject', [
                'publisher_company_full_name' => config('general.company_full_name')
            ]))
            ->view('user::emails.advertiser.password-set-reminder')
            ->with($this->payload);
    }
}
