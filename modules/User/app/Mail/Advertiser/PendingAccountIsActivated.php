<?php

namespace Modules\User\Mail\Advertiser;

use App\Mail\Mail;

class PendingAccountIsActivated extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('user::emails.advertiser.pending_account_is_activated.subject', [
                'company_name'                => $this->getPayloadValue('companyName'),
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('user::emails.advertiser.pending-account-is-activated')
            ->with($this->payload);
    }
}
