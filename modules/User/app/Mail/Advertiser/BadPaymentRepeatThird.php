<?php

namespace Modules\User\Mail\Advertiser;

use App\Mail\Mail;

class BadPaymentRepeatThird extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        \Log::info($this->payload);
        return $this
            ->subject(__('user::emails.advertiser.bad_payment_repeats.repeat_3.subject', [
                'publisher_company_full_name' => config('general.company_full_name')
            ]))
            ->view('user::emails.advertiser.bad-payment-repeats.repeat-3')
            ->with($this->payload);
    }
}
