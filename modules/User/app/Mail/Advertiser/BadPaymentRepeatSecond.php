<?php

namespace Modules\User\Mail\Advertiser;

use App\Mail\Mail;

class BadPaymentRepeatSecond extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        \Log::info($this->payload);
        return $this
            ->subject(__('user::emails.advertiser.bad_payment_repeats.repeat_2.subject', [
                'publisher_company_full_name' => config('general.company_full_name')
            ]))
            ->view('user::emails.advertiser.bad-payment-repeats.repeat-2')
            ->with($this->payload);
    }
}
