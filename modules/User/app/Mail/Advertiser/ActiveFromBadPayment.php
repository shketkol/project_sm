<?php

namespace Modules\User\Mail\Advertiser;

use App\Mail\Mail;

class ActiveFromBadPayment extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('user::emails.advertiser.active_from_bad_payment.subject', [
                'publisher_company_full_name' => config('general.company_full_name')
            ]))
            ->view('user::emails.advertiser.active-from-bad-payment')
            ->with($this->payload);
    }
}
