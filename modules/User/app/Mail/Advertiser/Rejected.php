<?php

namespace Modules\User\Mail\Advertiser;

use App\Mail\Mail;

class Rejected extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('user::emails.advertiser.rejected.subject', [
                'publisher_company_full_name' => config('general.company_full_name')
            ]))
            ->view('user::emails.advertiser.rejected')
            ->with($this->payload);
    }
}
