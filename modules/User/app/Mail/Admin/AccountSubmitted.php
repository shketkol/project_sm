<?php

namespace Modules\User\Mail\Admin;

use App\Mail\Mail;

class AccountSubmitted extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('user::emails.admin.account_submitted.subject', [
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('user::emails.admin.account-submitted')
            ->with($this->payload);
    }
}
