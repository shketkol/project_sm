<?php

namespace Modules\User\Mail\Admin;

use App\Mail\Mail;
use Illuminate\Support\Arr;

class ActiveFromBadPayment extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('user::emails.admin.active_from_bad_payment.subject', [
                'company_name' => Arr::get($this->payload, 'companyName'),
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('user::emails.admin.active-from-bad-payment')
            ->with($this->payload);
    }
}
