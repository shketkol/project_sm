<?php

namespace Modules\User\Actions;

use Modules\User\Models\User;

class AssignRolesAction
{
    /**
     * Create meta data for the user.
     *
     * @param User $user
     * @param array $roles
     *
     * @return mixed
     */
    public function handle(User $user, array $roles): void
    {
        $user->assignRole($roles);
    }
}
