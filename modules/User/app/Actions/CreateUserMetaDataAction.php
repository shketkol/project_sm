<?php

namespace Modules\User\Actions;

use Modules\User\Models\User;
use Modules\User\Models\UserMetaData;
use Psr\Log\LoggerInterface;

class CreateUserMetaDataAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * Create meta data for the user.
     *
     * @param User $user
     *
     * @return UserMetaData
     */
    public function handle(User $user): UserMetaData
    {
        /** @var UserMetaData $meta */
        $meta = $user->metaData()->create();

        $this->log->info('User MetaData created.', [
            'user' => $user->getKey(),
            'meta' => $meta->getKey(),
        ]);

        return $meta;
    }
}
