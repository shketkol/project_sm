<?php

namespace Modules\User\Actions\Payment;

use Modules\Daapi\Listeners\Notifications\Traits\GetFailedCampaigns;
use Modules\Notification\Actions\DestroyReminderAction;
use Modules\Notification\Actions\StoreReminderAction;
use Modules\Notification\Models\ReminderType;
use Modules\User\Models\User;

class SetRetryPaymentAction
{
    use GetFailedCampaigns;

    /**
     * @var StoreReminderAction
     */
    private $storeReminderAction;

    /**
     * @var DestroyReminderAction
     */
    protected $destroyReminderAction;

    /**
     * SetRetryPaymentAction constructor.
     *
     * @param StoreReminderAction   $storeReminderAction
     * @param DestroyReminderAction $destroyReminderAction
     */
    public function __construct(StoreReminderAction $storeReminderAction, DestroyReminderAction $destroyReminderAction)
    {
        $this->storeReminderAction   = $storeReminderAction;
        $this->destroyReminderAction = $destroyReminderAction;
    }

    /**
     * @param User  $user
     * @param bool  $canRetryPayment
     * @param array $ordersData
     *
     * @return void
     */
    public function handle(User $user, bool $canRetryPayment, array $ordersData): void
    {
        $this->setUserRetryPayment($user, $canRetryPayment);
        $campaigns = $this->getFailedCampaigns($ordersData);
        $reminderData = $this->prepareData($campaigns);
        $this->destroyReminderAction->handle($user, ReminderType::ID_RETRY_PAYMENT_PROCESSING);
        $this->storeReminderAction->handle(
            $user,
            ReminderType::ID_RETRY_PAYMENT,
            $reminderData
        );
    }

    /**
     * @param User $user
     * @param bool $canRetryPayment
     *
     * @return void
     */
    private function setUserRetryPayment(User $user, bool $canRetryPayment): void
    {
        $user->retry_payment = $canRetryPayment;
        $user->retry_payment_processing = false;
        $user->save();
    }

    /**
     * @param $campaigns
     *
     * @return array
     */
    private function prepareData($campaigns): array
    {
        $campaigns = $campaigns->map(function ($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
            ];
        });

        return [
            'campaigns' => $campaigns->toArray(),
        ];
    }
}
