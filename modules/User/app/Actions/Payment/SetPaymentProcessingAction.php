<?php

namespace Modules\User\Actions\Payment;

use Modules\Notification\Actions\DestroyReminderAction;
use Modules\Notification\Actions\StoreReminderAction;
use Modules\Notification\Models\ReminderType;
use Modules\User\Models\User;

class SetPaymentProcessingAction
{
    /**
     * @var StoreReminderAction
     */
    private $storeReminderAction;

    /**
     * @var DestroyReminderAction
     */
    protected $destroyReminderAction;

    /**
     * @param StoreReminderAction   $storeReminderAction
     * @param DestroyReminderAction $destroyReminderAction
     */
    public function __construct(StoreReminderAction $storeReminderAction, DestroyReminderAction $destroyReminderAction)
    {
        $this->storeReminderAction   = $storeReminderAction;
        $this->destroyReminderAction = $destroyReminderAction;
    }

    /**
     * @param User  $user
     *
     * @return void
     */
    public function handle(User $user): void
    {
        $this->setUserRetryPaymentProcessing($user);
        $this->destroyReminderAction->handle($user, ReminderType::ID_RETRY_PAYMENT);
        $this->storeReminderAction->handle($user, ReminderType::ID_RETRY_PAYMENT_PROCESSING);
    }

    /**
     * @param User $user
     *
     * @return void
     */
    private function setUserRetryPaymentProcessing(User $user): void
    {
        $user->retry_payment = false;
        $user->retry_payment_processing = true;
        $user->save();
    }
}
