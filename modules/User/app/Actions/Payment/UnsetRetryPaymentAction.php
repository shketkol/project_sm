<?php

namespace Modules\User\Actions\Payment;

use Modules\Notification\Actions\DestroyReminderAction;
use Modules\Notification\Models\ReminderType;
use Modules\User\Models\User;

class UnsetRetryPaymentAction
{
    /**
     * @var DestroyReminderAction
     */
    private $destroyReminderAction;

    /**
     * UnsetRetryPaymentAction constructor.
     *
     * @param DestroyReminderAction $destroyReminderAction
     */
    public function __construct(DestroyReminderAction $destroyReminderAction)
    {
        $this->destroyReminderAction = $destroyReminderAction;
    }

    /**
     * @param User  $user
     *
     * @return void
     */
    public function handle(User $user): void
    {
        $this->unsetUserRetryPayment($user);
        $this->destroyReminderAction->handle($user, ReminderType::ID_RETRY_PAYMENT_PROCESSING);
        $this->destroyReminderAction->handle($user, ReminderType::ID_RETRY_PAYMENT);
    }

    /**
     * @param User $user
     *
     * @return void
     */
    private function unsetUserRetryPayment(User $user): void
    {
        $user->retry_payment = false;
        $user->retry_payment_processing = false;
        $user->save();
    }
}
