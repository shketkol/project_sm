<?php

namespace Modules\User\Actions\Notifications;

use Carbon\Carbon;
use Illuminate\Log\Logger;
use Modules\Daapi\Actions\Notification\UpdateStatusBaseAction;
use Modules\User\Exceptions\UserNotFoundException;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;
use Modules\User\Repositories\UserRepository;

class UpdateUserStatusAction extends UpdateStatusBaseAction
{
    /**
     * @param Logger         $log
     * @param UserRepository $repository
     */
    public function __construct(Logger $log, UserRepository $repository)
    {
        parent::__construct($log, $repository);
    }

    /**
     * @param string     $externalId
     * @param string     $status
     * @param array|null $additionalData
     *
     * @throws UserNotFoundException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function handle(string $externalId, string $status, ?array $additionalData = null): void
    {
        /** @var \Modules\User\Models\User $user */
        $user = $this->getEntity($externalId, 'account_external_id');

        if (!$user) {
            throw UserNotFoundException::create($externalId, 'change status');
        }

        $oldStatus = $user->status->name;

        $this->log->info('Updating User status.', [
            'user_id'     => $user->id,
            'from_status' => $oldStatus,
            'to_status'   => $status,
        ]);

        $this->applyState($status);

        $this->updateInactiveUnpaidAt($user, $user->findTransition($status));

        $this->log->info('User status was successfully updated.', [
            'user_id'     => $user->id,
            'from_status' => $oldStatus,
            'to_status'   => $status,
        ]);
    }

    /**
     * @param User   $user
     * @param string $status
     */
    private function updateInactiveUnpaidAt(User $user, string $status)
    {
        if ($status === UserStatus::INACTIVE_UNPAID) {
            $user->inactive_unpaid_at = Carbon::now();
            $user->save();
        }
    }
}
