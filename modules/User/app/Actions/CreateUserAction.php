<?php

namespace Modules\User\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Log;
use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Modules\Daapi\DataTransferObjects\Types\UserDataPlain;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use Modules\Invitation\Repositories\InvitationRepository;
use Modules\Notification\Actions\StoreReminderAction;
use Modules\Notification\Models\ReminderType;
use Modules\User\Exceptions\UserNotUpdatedException;
use Modules\User\Models\AccountType;
use Modules\User\Models\Role;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;
use Modules\User\Repositories\Contracts\UserRepository;
use Psr\Log\LoggerInterface;
use SM\SMException;
use Modules\Invitation\Models\Invitation;

class CreateUserAction
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var InvitationRepository
     */
    private $invitationRepository;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var CreateUserMetaDataAction
     */
    private $createUserMetaDataAction;

    /**
     * @var UserDataPlain
     */
    private $userData;

    /**
     * @var AccountData
     */
    private $accountData;

    /**
     * @var bool
     */
    private $hasUserData;

    /**
     * @var User|null
     */
    private $user;

    /**
     * @var string
     */
    private $accountId;

    /**
     * @var Invitation
     */
    private $invitation;

    /**
     * @var StoreReminderAction
     */
    private $storeReminderAction;

    /**
     * CreateUserAction constructor.
     * @param CreateUserMetaDataAction $createUserMetaDataAction
     * @param DatabaseManager $databaseManager
     * @param LoggerInterface $log
     * @param UserRepository $userRepository
     * @param InvitationRepository $invitationRepository
     * @param StoreReminderAction $storeReminderAction
     */
    public function __construct(
        CreateUserMetaDataAction $createUserMetaDataAction,
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        UserRepository $userRepository,
        InvitationRepository $invitationRepository,
        StoreReminderAction $storeReminderAction
    ) {
        $this->createUserMetaDataAction = $createUserMetaDataAction;
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->userRepository = $userRepository;
        $this->invitationRepository = $invitationRepository;
        $this->storeReminderAction = $storeReminderAction;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param string $accountId
     * @param null $userData
     * @param null $accountData
     * @return User|null
     * @throws \Exception
     */
    public function handle(
        string $accountId,
        $userData = null,
        $accountData = null
    ): ?User {
        $this->user = $this->userRepository->findWhere(['account_external_id' => $accountId])->first();

        $this->hasUserData = $userData && $accountData;
        $this->accountData = $accountData;
        $this->userData = $userData;
        $this->accountId = $accountId;

        $this->databaseManager->beginTransaction();
        try {
            if (!$this->user) {
                $this->createUser();

                // Complete invite code
                $this->applyCompletedStatus();
            } else {
                $this->updateUser();
            }
        } catch (\Throwable $exception) {
            Log::logException($exception);
            $this->databaseManager->rollBack();

            throw UserNotUpdatedException::create('Failed to store user data.');
        }

        $this->databaseManager->commit();

        return $this->user;
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function applyCompletedStatus(): void
    {
        $this->invitationRepository->updateStatusByEmail($this->userData->email, $this->user);
    }

    /**
     * @return int
     * @throws \Exception
     */
    private function getAccountTypeId(): int
    {
        $invite = $this->invitationRepository->byEmail($this->userData->email);
        if ($invite->exists()) {
            return $invite->first()->account_type_category;
        }

        return AccountType::ID_COMMON;
    }

    /**
     * @return User
     * @throws \Exception
     */
    private function createUser(): User
    {
        if (!$this->hasUserData) {
            return $this->initCreateUser();
        }
        $this->user = $this->userRepository->create([
            'external_id'         => $this->userData->externalId,
            'company_name'        => $this->accountData->companyName,
            'account_external_id' => $this->accountData->externalId,
            'account_type_id'     => $this->getAccountTypeId(),
            'status_id'           => User::getStatusIdByTransactionFlow($this->accountData->accountStatus, 'user'),
        ]);

        $this->createUserMetaDataAction->handle($this->user);
        $this->user->assignRole([[Role::ID_ADVERTISER]]);

        if ($this->getAccountTypeId() === AccountType::ID_SPECIAL_ADS) {
            $this->storeReminderAction->handle($this->user, ReminderType::ID_SPECIAL_ADS);
        }

        return $this->user;
    }

    /**
     * @return User
     * @throws CanNotApplyStatusException
     * @throws SMException
     */
    private function updateUser(): User
    {
        if (!$this->hasUserData) {
            return $this->briefUpdateUser();
        }
        $this->user->account_external_id = $this->accountData->externalId;
        $this->user->external_id = $this->userData->externalId;
        $this->user->company_name = $this->accountData->companyName;

        $this->user->applyHaapiStatus($this->accountData->accountStatus);

        $this->log->info('User has been updated.', ['user_id' => $this->user->id]);

        return $this->user;
    }

    /**
     * @return User
     */
    private function briefUpdateUser(): User
    {
        $this->userRepository->update(
            [
                'account_external_id' => $this->accountId,
            ],
            $this->user->getKey()
        );

        $this->log->info('User has been updated with brief data.', ['user_id' => $this->user->id]);

        return $this->user;
    }

    /**
     * @return User
     */
    private function initCreateUser(): User
    {
        $this->user = $this->userRepository->create([
            'account_external_id' => $this->accountId,
            'status_id'           => UserStatus::ID_PENDING_MANUAL_REVIEW,
        ]);

        $this->createUserMetaDataAction->handle($this->user);
        $this->user->assignRole([[Role::ID_ADVERTISER]]);

        $this->log->info('User with brief dataset has been created.', ['user_id' => $this->user->id]);

        return $this->user;
    }
}
