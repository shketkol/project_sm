<?php

namespace Modules\User\Actions;

use Modules\User\Models\User;
use Modules\User\Models\UserMetaData;
use Psr\Log\LoggerInterface;

class CompleteOnboardingAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * Create meta data for the user.
     *
     * @param User $user
     *
     * @return UserMetaData
     */
    public function handle(User $user): UserMetaData
    {
        $this->log->info('Onboarding update for User started.', ['user' => $user->getKey()]);

        /** @var UserMetaData $metaData */
        $metaData = $user->metaData()->updateOrCreate([], ['onboarding_completed' => true]);

        $this->log->info('Onboarding update for User finished.', ['user' => $user->getKey()]);

        return $metaData;
    }
}
