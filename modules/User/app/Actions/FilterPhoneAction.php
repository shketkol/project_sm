<?php

namespace Modules\User\Actions;

class FilterPhoneAction
{
    /**
     * Filter phone number so only numbers would left.
     *
     * @param string $phone
     *
     * @return string
     */
    public function handle(string $phone): string
    {
        return $phone;
    }
}
