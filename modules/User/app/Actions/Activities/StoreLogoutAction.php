<?php

namespace Modules\User\Actions\Activities;

use Modules\User\Models\Activity\ActivityType;
use Modules\User\Repositories\ActivityRepository;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class StoreLogoutAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var ActivityRepository
     */
    private $repository;

    /**
     * @param LoggerInterface    $log
     * @param ActivityRepository $repository
     */
    public function __construct(LoggerInterface $log, ActivityRepository $repository)
    {
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param User     $user
     * @param int|null $activityId
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(User $user, ?int $activityId = null): void
    {
        if (!$user->isAdvertiser()) {
            return;
        }

        $this->log->info('[Activity] Started storing advertiser logout.', ['user_id' => $user->id]);

        $activityId = $activityId ?? session()->get(ActivityType::LOGIN);
        $this->repository->storeLogout($user, $activityId);

        $this->log->info('[Activity] Finished storing advertiser logout.', ['user_id' => $user->id]);
    }
}
