<?php

namespace Modules\User\DataTransferObjects;

use Modules\User\Models\UserStatus;
use Spatie\DataTransferObject\DataTransferObject;

class BriefUserData extends DataTransferObject
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;

    /**
     * @var string
     */
    public $company_name;

    /**
     * @var int|null
     */
    public $status_id;

    public function __construct(UserData $fullData)
    {
        parent::__construct([
            'email'        => $fullData->email,
            'first_name'   => $fullData->first_name,
            'last_name'    => $fullData->last_name,
            'company_name' => $fullData->company->name,
            'status_id'    => UserStatus::ID_CREATE_IN_PROGRESS,
        ]);
    }
}
