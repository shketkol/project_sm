<?php

namespace Modules\User\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class AdminUserData extends DataTransferObject
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;
}
