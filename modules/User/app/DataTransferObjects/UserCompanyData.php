<?php

namespace Modules\User\DataTransferObjects;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class UserCompanyData extends DataTransferObject
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $address_line1;

    /**
     * @var string
     */
    public $address_line2;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $state;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $zipcode;

    /**
     * @param array $userData
     *
     * @return UserCompanyData
     */
    public static function fromPayload(array $userData)
    {
        return new self([
            'name'          => Arr::get($userData, 'companyName'),
            'address_line1' => Arr::get($userData, 'companyAddress.line1'),
            'address_line2' => Arr::get($userData, 'companyAddress.line2', '') ?? '',
            'city'          => Arr::get($userData, 'companyAddress.city'),
            'state'         => Arr::get($userData, 'companyAddress.state'),
            'country'       => Arr::get($userData, 'companyAddress.country'),
            'zipcode'       => Arr::get($userData, 'companyAddress.zipcode'),
        ]);
    }

    /**
     * Static constructor.
     *
     * @param Request $request
     *
     * @return UserCompanyData
     */
    public static function fromRequest(Request $request): self
    {
        return new self([
            'name'          => $request->json('company_name'),
            'address_line1' => $request->json('company_address.line1'),
            'address_line2' => $request->json('company_address.line2', '') ?? '', // see ConvertEmptyStringsToNull
            'city'          => $request->json('company_address.city'),
            'state'         => $request->json('company_address.state'),
            'country'       => $request->json('company_address.country', 'US'),
            'zipcode'       => $request->json('company_address.zipcode'),
        ]);
    }
}
