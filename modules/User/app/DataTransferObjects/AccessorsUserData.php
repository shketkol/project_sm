<?php

namespace Modules\User\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class AccessorsUserData extends DataTransferObject
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string|null
     */
    public $phone;
}
