<?php

namespace Modules\User\DataTransferObjects;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Modules\User\Models\UserStatus;
use Spatie\DataTransferObject\DataTransferObject;

class UserData extends DataTransferObject
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;

    /**
     * @var string
     */
    public $phone_number;

    /**
     * @var int|null
     */
    public $status_id;

    /**
     * @var \Modules\User\DataTransferObjects\UserCompanyData
     */
    public $company;

    /**
     * @var string|null
     */
    public $identification_key;

    /**
     * UserData constructor.
     *
     * @param array $userData
     *
     * @return UserData
     */
    public static function fromPayload(array $userData)
    {
        return new self([
            'email'        => Arr::get($userData, 'email'),
            'first_name'   => Arr::get($userData, 'firstName'),
            'last_name'    => Arr::get($userData, 'lastName'),
            'phone_number' => Arr::get($userData, 'phoneNumber'),
            'status_id'    => UserStatus::ID_CREATE_IN_PROGRESS,
            'company'      => UserCompanyData::fromPayload($userData),
        ]);
    }

    /**
     * Static constructor.
     *
     * @param Request     $request
     * @param string|null $identificationKey
     *
     * @return UserData
     */
    public static function fromRequest(Request $request, ?string $identificationKey): self
    {
        return new self([
            'email'              => $request->get('email'),
            'first_name'         => $request->get('first_name'),
            'last_name'          => $request->get('last_name'),
            'phone_number'       => $request->get('phone_number'),
            'status_id'          => UserStatus::ID_CREATE_IN_PROGRESS,
            'company'            => UserCompanyData::fromRequest($request),
            'identification_key' => $identificationKey,
        ]);
    }
}
