<?php

namespace Modules\User\DataTransferObjects;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class UserUpdateData extends DataTransferObject
{

    /**
     * @var int
     */
    public $sourceId;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @param       $userId
     * @param array $payload
     */
    public function __construct($userId, array $payload)
    {
        parent::__construct([
            'sourceId' => $userId,
            'firstName'  => Arr::get($payload, 'first_name', ''),
            'lastName' => Arr::get($payload, 'last_name', ''),
        ]);
    }

    public static function fromPayload(array $payload)
    {
        return new self(
            Arr::get($payload, 'sourceId'),
            Arr::only($payload, ['firstName', 'lastName'])
        );
    }
}
