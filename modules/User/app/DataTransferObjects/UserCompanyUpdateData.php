<?php

namespace Modules\User\DataTransferObjects;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\CompanyAddressData;
use Spatie\DataTransferObject\DataTransferObject;

class UserCompanyUpdateData extends DataTransferObject
{
    /**
     * @var string
     */
    public $accountId;

    /**
     * @var \Modules\Daapi\DataTransferObjects\Types\CompanyAddressData
     */
    public $companyAddress;

    /**
     * @var string
     */
    public $phoneNumber;

    /**
     * @param string $accountId
     * @param array  $payload
     */
    public function __construct(string $accountId, array $payload)
    {
        parent::__construct([
            'accountId'      => $accountId,
            'companyAddress' => new CompanyAddressData($payload),
            'phoneNumber'    => Arr::get($payload, 'phone_number', ''),
        ]);
    }
}
