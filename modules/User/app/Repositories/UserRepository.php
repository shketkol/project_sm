<?php

namespace Modules\User\Repositories;

use App\Repositories\Repository;
use Modules\User\Exceptions\AdminNotFoundException;
use Modules\User\Exceptions\UserNotFoundException;
use Modules\User\Models\Role;
use Modules\User\Models\User;
use Modules\User\Repositories\Contracts\UserRepository as UserRepositoryInterface;
use Modules\User\Repositories\Criteria\AccountExternalIdCriteria;
use Modules\User\Repositories\Criteria\CompanyIdCriteria;
use Modules\User\Repositories\Criteria\EmailCriteria;
use Modules\User\Repositories\Criteria\ExternalIdCriteria;
use Modules\User\Repositories\Criteria\HasExternalId;
use Modules\User\Repositories\Criteria\RoleCriteria;
use Modules\User\Repositories\Criteria\StatusCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;

class UserRepository extends Repository implements UserRepositoryInterface
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return User::class;
    }

    /**
     * @param int $id
     *
     * @return User
     * @throws UserNotFoundException
     */
    public function getById(int $id)
    {
        $user = User::find($id);
        if (!$user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * @param string $email
     *
     * @return User
     * @throws UserNotFoundException
     * @throws RepositoryException
     */
    public function getByEmail(string $email)
    {
        $user = $this->pushCriteria(new EmailCriteria($email))->first();
        if (!$user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * @param string $companyExternalId
     *
     * @return User
     * @throws UserNotFoundException
     * @throws RepositoryException
     */
    public function getByCompanyId(string $companyExternalId)
    {
        $user = $this->pushCriteria(new CompanyIdCriteria($companyExternalId))->first();
        if (!$user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * @param string $externalId
     *
     * @return User
     * @throws RepositoryException
     * @throws UserNotFoundException
     */
    public function getByExternalId(string $externalId)
    {
        $user = $this->pushCriteria(new ExternalIdCriteria($externalId))->first();
        if (!$user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * @param string $accountExternalId
     *
     * @return User
     * @throws RepositoryException
     * @throws UserNotFoundException
     */
    public function getByAccountExternalId(string $accountExternalId): User
    {
        $user = $this->pushCriteria(new AccountExternalIdCriteria($accountExternalId))->first();
        if (!$user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * Add role criteria.
     *
     * @param int $roleId
     *
     * @return UserRepositoryInterface
     * @throws RepositoryException
     */
    public function roleCriteria(int $roleId): UserRepositoryInterface
    {
        return $this->pushCriteria(new RoleCriteria($roleId));
    }

    /**
     * @return User
     * @throws AdminNotFoundException
     * @throws RepositoryException
     */
    public function getAdmin(): User
    {
        $admin = $this->reset()->roleCriteria(Role::ID_ADMIN)->first();

        if ($admin) {
            return $admin;
        }

        // if admin user never logged in (so never created in DB) on this environment use technical admin
        return $this->getTechnicalAdmin();
    }

    /**
     * @return User
     * @throws AdminNotFoundException
     * @throws RepositoryException
     */
    public function getTechnicalAdmin(): User
    {
        $admin = $this->reset()->roleCriteria(Role::ID_TECHNICAL_ADMIN)->first();

        if (is_null($admin)) {
            throw new AdminNotFoundException();
        }

        return $admin;
    }

    /**
     * Update a entity in repository by id
     *
     * @param array $attributes
     * @param       $id
     *
     * @return mixed
     * @throws ValidatorException
     */
    public function update(array $attributes, $id)
    {
        parent::update($attributes, $id);
        $user = self::find($id);
        return $user->refreshAttributes();
    }

    /**
     * Has external_id params.
     *
     * @return UserRepositoryInterface
     * @throws RepositoryException
     */
    public function onlyHasExternalId(): UserRepositoryInterface
    {
        return $this->pushCriteria(new HasExternalId());
    }

    /**
     * @param array $statusIds
     *
     * @return UserRepository
     * @throws RepositoryException
     */
    public function byStatus(array $statusIds): self
    {
        return $this->pushCriteria(new StatusCriteria($statusIds));
    }
}
