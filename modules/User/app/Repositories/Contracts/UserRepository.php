<?php

namespace Modules\User\Repositories\Contracts;

use App\Repositories\Contracts\Repository;
use Modules\User\Exceptions\UserNotFoundException;
use Modules\User\Models\User;
use Prettus\Repository\Exceptions\RepositoryException;

interface UserRepository extends Repository
{
    /**
     * @param int $id
     *
     * @return User
     * @throws UserNotFoundException
     * @throws RepositoryException
     */
    public function getById(int $id);

    /**
     * @param string $email
     *
     * @return User
     * @throws UserNotFoundException
     * @throws RepositoryException
     */
    public function getByEmail(string $email);

    /**
     * @param string $companyExternalId
     *
     * @return User
     * @throws UserNotFoundException
     * @throws RepositoryException
     */
    public function getByCompanyId(string $companyExternalId);

    /**
     * @param string $externalId
     *
     * @return User
     * @throws RepositoryException
     * @throws UserNotFoundException
     */
    public function getByExternalId(string $externalId);

    /**
     * @param string $accountExternalId
     *
     * @return User
     * @throws RepositoryException
     * @throws UserNotFoundException
     */
    public function getByAccountExternalId(string $accountExternalId): User;

    /**
     * Add role criteria.
     *
     * @param int $roleId
     *
     * @return UserRepository
     * @throws RepositoryException
     */
    public function roleCriteria(int $roleId): self;

    /**
     * Get admin user.
     *
     * @return User|null
     * @throws RepositoryException
     */
    public function getAdmin(): ?User;

    /**
     * Get technical admin user.
     *
     * @return User|null
     * @throws RepositoryException
     */
    public function getTechnicalAdmin(): ?User;
}
