<?php

namespace Modules\User\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class StatusCriteria implements CriteriaInterface
{
    /**
     * @var int[]
     */
    private $statusIds;

    /**
     * @param array $statusIds
     */
    public function __construct(array $statusIds)
    {
        $this->statusIds = $statusIds;
    }

    /**
     * Apply criteria in query repository
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->whereIn('status_id', $this->statusIds);
    }
}
