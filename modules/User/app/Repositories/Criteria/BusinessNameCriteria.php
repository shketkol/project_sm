<?php

namespace Modules\User\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class BusinessNameCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private $needle;

    /**
     * BusinessNameCriteria constructor.
     * @param string $needle
     */
    public function __construct(string $needle)
    {
        $this->needle = mb_strtolower($needle);
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->whereRaw("LOWER(`company_name`) LIKE ?", ["%$this->needle%"]);
    }
}
