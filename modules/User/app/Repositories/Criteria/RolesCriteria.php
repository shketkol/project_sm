<?php

namespace Modules\User\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class RolesCriteria implements CriteriaInterface
{
    /**
     * @var int[]
     */
    private $roles;

    /**
     * @param int ...$roles
     */
    public function __construct(int ...$roles)
    {
        $this->roles = $roles;
    }

    /**
     * @param Model|Builder       $model
     * @param RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->whereIn('model_has_roles.role_id', $this->roles);
    }
}
