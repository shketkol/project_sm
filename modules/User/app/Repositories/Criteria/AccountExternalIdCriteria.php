<?php

namespace Modules\User\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AccountExternalIdCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private $accountExternalId;

    /**
     * @param string $accountExternalId
     */
    public function __construct(string $accountExternalId)
    {
        $this->accountExternalId = $accountExternalId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where('account_external_id', $this->accountExternalId);
    }
}
