<?php

namespace Modules\User\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ExternalIdCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private $externalId;

    /**
     * @param string $externalId
     */
    public function __construct(string $externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where('external_id', $this->externalId);
    }
}
