<?php

namespace Modules\User\Repositories;

use App\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Modules\User\Models\Activity\Activity;
use Modules\User\Models\Activity\ActivityType;
use Modules\User\Models\User;
use Modules\User\Repositories\Criteria\HasActivityCriteria;
use Modules\User\Repositories\Criteria\UniqueUserCriteria;

class ActivityRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * @return string
     */
    public function model(): string
    {
        return Activity::class;
    }

    /**
     * This method does not store info about user device.
     * It means that it could be several login records and all of them could be active.
     *
     * @param User $user
     *
     * @return Activity
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function storeLogin(User $user): Activity
    {
        return $this->create([
            'user_id' => $user->id,
            'type_id' => ActivityType::ID_LOGIN,
        ]);
    }

    /**
     * @param User     $user
     * @param int|null $parentId
     *
     * @return Activity
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function storeLogout(User $user, ?int $parentId = null): Activity
    {
        return $this->create([
            'user_id'   => $user->id,
            'parent_id' => $parentId,
            'type_id'   => ActivityType::ID_LOGOUT,
        ]);
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     *
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function countActiveAdvertisers(Carbon $start, Carbon $end): int
    {
        return $this->getActiveAdvertisers($start, $end)->count();
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     *
     * @return Collection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getActiveAdvertisers(Carbon $start, Carbon $end): Collection
    {
        return $this
            ->pushCriteria(new HasActivityCriteria($start, $end))
            ->pushCriteria(new UniqueUserCriteria())
            ->all();
    }
}
