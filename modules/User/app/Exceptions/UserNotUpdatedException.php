<?php

namespace Modules\User\Exceptions;

use App\Exceptions\ModelNotUpdatedException;

class UserNotUpdatedException extends ModelNotUpdatedException
{
    /**
     * @param string $message
     *
     * @return UserNotUpdatedException
     */
    public static function create(string $message): self
    {
        return new self($message);
    }
}
