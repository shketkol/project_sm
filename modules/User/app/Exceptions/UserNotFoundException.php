<?php

namespace Modules\User\Exceptions;

use App\Exceptions\ModelNotFoundException;
use Illuminate\Http\Response;

class UserNotFoundException extends ModelNotFoundException
{
    /**
     * Default Status code used for improved logging.
     * This exception being logged as WARNING.
     * See BaseExceptions and LoggerService for details.
     */
    public const STATUS_CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response()->json([
            'message' => __('user::messages.not_found'),
            'errors'  => [
                'email' => [__('user::messages.not_found')],
            ],
        ], $this->getCode());
    }

    /**
     * @param string|null $externalId
     * @param string|null $cause
     *
     * @return self
     */
    public static function create(?string $externalId = null, ?string $cause = null): self
    {
        $message = 'User was not found';

        if (is_null($externalId)) {
            $message .= ', external_id is missing.';
        } else {
            $message .= sprintf(' by external_id: "%s".', $externalId);
        }

        if ($cause) {
            $message .= sprintf(' (Cause: "%s").', $cause);
        }

        return new self($message, Response::HTTP_NOT_FOUND);
    }
}
