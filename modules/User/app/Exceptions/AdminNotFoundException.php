<?php

namespace Modules\User\Exceptions;

use App\Exceptions\ModelNotFoundException;
use Illuminate\Http\Response;

class AdminNotFoundException extends ModelNotFoundException
{
    /**
     * Default Status code used for improved logging.
     * This exception being logged as WARNING.
     * See BaseExceptions and LoggerService for details.
     */
    public const STATUS_CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return redirect()->route('login-failed');
    }
}
