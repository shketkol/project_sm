<?php

namespace Modules\User\Exceptions;

use App\Exceptions\ModelNotUpdatedException;

class PasswordNotUpdatedException extends ModelNotUpdatedException
{
    //
}
