<?php

namespace Modules\User\Policies;

use App\Policies\Policy;
use Modules\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy extends Policy
{
    use HandlesAuthorization;

    /**
     * Permissions.
     */
    public const PERMISSION_UPDATE_PROFILE = 'update_profile';

    /**
     * Model class.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Returns true if user can update profile.
     *
     * @param User $user
     *
     * @return bool
     */
    public function updateProfile(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_UPDATE_PROFILE);
    }
}
