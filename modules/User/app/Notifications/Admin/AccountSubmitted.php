<?php

namespace Modules\User\Notifications\Admin;

use App\Notifications\Traits\AdminNotification;
use Modules\User\Mail\Admin\AccountSubmitted as Mail;
use Modules\User\Models\User;
use Modules\User\Notifications\UserNotification;

class AccountSubmitted extends UserNotification
{
    use AdminNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'first_name'     => $notifiable->first_name,
            'company_name'   => $this->user->company_name,
            'status_name'    => $this->user->status->translated_name,
            'email'          => $this->user->email,
            'review_account' => route('advertisers.show', ['advertiser' => $this->user->id]),
        ];
    }
}
