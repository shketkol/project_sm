<?php

namespace Modules\User\Notifications\Admin;

use App\Notifications\Traits\AdminNotification;
use Modules\User\Mail\Admin\BadPayment as Mail;
use Modules\User\Models\User;
use Modules\User\Notifications\UserNotification;

class BadPayment extends UserNotification
{
    use AdminNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'      => $notifiable->first_name,
            'companyName'    => $this->user->company_name,
            'reviewAccount' => route(
                'advertisers.show',
                [
                    'advertiser' => $this->user->id,
                    'tab' => 'transactions'
                ]
            ),
        ];
    }
}
