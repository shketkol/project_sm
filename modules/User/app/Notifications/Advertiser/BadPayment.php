<?php

namespace Modules\User\Notifications\Advertiser;

use App\Helpers\HtmlHelper;
use App\Notifications\Traits\AdvertiserNotification;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Notification\Models\NotificationCategory;
use Modules\User\Mail\Advertiser\BadPayment as Mail;
use Modules\User\Models\User;
use Modules\User\Notifications\UserNotification;

class BadPayment extends UserNotification
{
    use AdvertiserNotification;

    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * @param Campaign $campaign
     * @param User $user
     * @param array|null $payload
     */
    public function __construct(Campaign $campaign, User $user, ?array $payload = null)
    {
        $this->campaign = $campaign;

        parent::__construct($user, $payload);
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'companyName'     => $notifiable->company_name,
            'accountSettings' => route('users.profile'),
            'profile'         => route(
                'users.profile.transactions',
                ['orderId' => $this->campaign->order_id]
            ),
            'titleIcon' => 'pause',
            'title'     => __('user::emails.advertiser.bad_payment.title'),
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        /** @var HtmlHelper $html */
        $html = app(HtmlHelper::class);

        return __('user::notifications.advertiser.bad_payment', [
            'company_name' => e(Arr::get($data, 'companyName')),
            'profile'      => $html->createAnchorElement(Arr::get($data, 'profile'), [
                'title' => __('labels.profile'),
            ]),
        ]);
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_TRANSACTION;
    }
}
