<?php

namespace Modules\User\Notifications\Advertiser;

use Modules\Campaign\Models\Campaign;
use Modules\Notification\Models\NotificationCategory;
use Modules\User\Models\User;
use Modules\User\Notifications\UserNotification;

abstract class BadPaymentRepeat extends UserNotification
{
    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * @param Campaign $campaign
     * @param User $user
     * @param array|null $payload
     */
    public function __construct(Campaign $campaign, User $user, ?array $payload = null)
    {
        $this->campaign = $campaign;

        parent::__construct($user, $payload);
    }

    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(User $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'companyName'     => $notifiable->company_name,
            'profile'         => route(
                'users.profile.transactions',
                ['orderId' => $this->campaign->order_id]
            ),
            'accountSettings' => route('users.profile'),
            'titleIcon'       => 'pause',
            'title'           => __('user::emails.advertiser.bad_payment.title'),
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public static function getContent(array $data): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_TRANSACTION;
    }
}
