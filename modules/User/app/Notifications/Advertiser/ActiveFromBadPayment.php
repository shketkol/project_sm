<?php

namespace Modules\User\Notifications\Advertiser;

use App\Notifications\Traits\AdvertiserNotification;
use Modules\Notification\Models\NotificationCategory;
use Modules\User\Mail\Advertiser\ActiveFromBadPayment as Mail;
use Modules\User\Models\User;
use Modules\User\Notifications\UserNotification;

class ActiveFromBadPayment extends UserNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'campaignListing' => route('campaigns.index'),
            'titleIcon'       => 'marked',
            'title'           => __('user::emails.advertiser.active_from_bad_payment.title'),
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        return e(__('user::notifications.advertiser.active_from_bad_payment'));
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_TRANSACTION;
    }
}
