<?php

namespace Modules\User\Notifications\Advertiser;

use Modules\User\Mail\Advertiser\BadPaymentRepeatSecond as Mail;
use Modules\User\Models\User;

class BadPaymentRepeatSecond extends BadPaymentRepeat
{
    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return array_merge(parent::getPayload($notifiable), [
            'title' => __('user::emails.advertiser.bad_payment_repeats.repeat_2.title'),
        ]);
    }
}
