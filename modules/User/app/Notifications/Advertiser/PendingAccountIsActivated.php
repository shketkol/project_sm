<?php

namespace Modules\User\Notifications\Advertiser;

use App\Notifications\Traits\AdvertiserNotification;
use Modules\User\Mail\Advertiser\PendingAccountIsActivated as Mail;
use Modules\User\Models\User;
use Modules\User\Notifications\UserNotification;

class PendingAccountIsActivated extends UserNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(User $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'companyName'     => $this->user->company_name,
            'bookNewCampaign' => route('campaigns.create'),
            'titleIcon'       => 'notice',
            'title'           => __('user::emails.advertiser.pending_account_is_activated.title'),
        ];
    }
}
