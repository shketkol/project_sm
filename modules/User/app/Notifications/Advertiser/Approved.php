<?php

namespace Modules\User\Notifications\Advertiser;

use App\Notifications\Traits\AdvertiserNotification;
use Modules\User\Mail\Advertiser\Approved as Mail;
use Modules\User\Models\User;
use Modules\User\Notifications\UserNotification;

class Approved extends UserNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(User $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'   => $notifiable->first_name,
            'faqs'        => config('general.links.faq.authorized'),
            'loginLink'   => route('login.form'),
            'titleIcon'   => 'thumbs_up',
            'title'       => __('user::emails.advertiser.approved.title'),
        ];
    }
}
