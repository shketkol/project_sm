<?php

namespace Modules\User\Notifications;

use App\Notifications\Notification;
use Modules\Notification\Models\NotificationCategory;
use Modules\User\Models\User;

abstract class UserNotification extends Notification
{
    /**
     * @var array|null
     */
    protected $payload;

    /**
     * @var User
     */
    protected $user;

    /**
     * UserNotification constructor.
     *
     * @param User       $user
     * @param array|null $payload
     */
    public function __construct(User $user, ?array $payload = null)
    {
        $this->payload = $payload;
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_ACCOUNT;
    }
}
