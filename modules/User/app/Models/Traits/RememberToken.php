<?php

namespace Modules\User\Models\Traits;

/**
 * Overrides remember token methods to disable Remember Me functionality
 *
 * @package Modules\User\Models\Traits
 */
trait RememberToken
{
    /**
     * Override to disable remember me functionality
     *
     * @return null
     */
    public function getRememberToken()
    {
        return null; // not supported
    }

    /**
     * Override to disable remember me functionality
     *
     * @param string $value
     */
    public function setRememberToken($value)
    {
        // not supported
    }

    /**
     * Override to disable remember me functionality
     *
     * @return string|null
     */
    public function getRememberTokenName()
    {
        return null; // not supported
    }

    /**
     * Override to ignore remember token
     *
     * @param $key
     * @param $value
     */
    public function setAttribute($key, $value)
    {
        if (!$key == $this->getRememberTokenName()) {
            parent::setAttribute($key, $value);
        }
    }
}
