<?php

namespace Modules\User\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\User\Models\User;

trait HasUser
{
    /**
     * Relation to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'user_id');
    }
}
