<?php

namespace Modules\User\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Zipcode;
use Modules\User\Models\User;
use Modules\User\Models\AccountType;

/**
 * @mixin User
 * @property AccountType $accountType
 */
trait BelongsToAccountType
{
    /**
     * Get user's account-type.
     *
     * @return BelongsTo
     */
    public function accountType(): BelongsTo
    {
        return $this->belongsTo(AccountType::class);
    }

    /**
     * @return array
     */
    public function getTargetingPermissions(): array
    {
        return [
            Gender::TYPE_NAME   => Gender::PERMISSION_NAME,
            AgeGroup::TYPE_NAME => AgeGroup::PERMISSION_NAME,
            Audience::TYPE_NAME => Audience::PERMISSION_NAME,
            Location::TYPE_NAME  => Location::PERMISSION_NAME,
            Zipcode::TYPE_NAME  => Zipcode::PERMISSION_NAME,
            Genre::TYPE_NAME    => Genre::PERMISSION_NAME,
        ];
    }

    /**
     * @param string $targetingType
     *
     * @return bool
     */
    public function canAccessTargeting(string $targetingType): bool
    {
        $permission = Arr::get($this->getTargetingPermissions(), $targetingType);
    
        if (!$permission) {
            return false;
        }

        return (bool) optional($this->accountType)->$permission ?? false;
    }

    /**
     * @return bool
     */
    public function isSpecialAds(): bool
    {
        return $this->account_type_id === AccountType::ID_SPECIAL_ADS;
    }
}
