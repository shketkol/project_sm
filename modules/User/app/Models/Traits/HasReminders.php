<?php

namespace Modules\User\Models\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Notification\Models\Reminder;

/**
 * @property Reminder[]|Collection $reminders
 */
trait HasReminders
{
    /**
     * @return HasMany
     */
    public function reminders(): HasMany
    {
        return $this->hasMany(Reminder::class, 'user_id');
    }
}
