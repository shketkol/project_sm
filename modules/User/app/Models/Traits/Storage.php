<?php

namespace Modules\User\Models\Traits;

trait Storage
{
    /**
     * @return string
     */
    public function getUserFolder(): string
    {
        return hash('sha256', config('app.key') . $this->getExternalId());
    }
}
