<?php

namespace Modules\User\Models\Traits;

use Modules\User\Actions\AssignRolesAction;
use Modules\User\Models\Role;
use Modules\User\Models\User;

trait CreateUser
{
    /**
     * Create test user using factory.
     *
     * @param array       $attributes
     * @param string|null $state
     *
     * @return User
     */
    private function createTestUser(array $attributes = [], string $state = null): User
    {
        $user = User::factory()->create($attributes);

        if ($state) {
            /** @var AssignRolesAction $assignRoleAction */
            $assignRoleAction = app(AssignRolesAction::class);
            $assignRoleAction->handle($user, [$state]);
        }

        return $user;
    }

    /**
     * Create test advertiser using factory.
     *
     * @param array $attributes
     *
     * @return User
     */
    private function createTestAdvertiser(array $attributes = []): User
    {
        return $this->createTestUser($attributes, Role::ADVERTISER);
    }

    /**
     * Create test read-only admin using factory.
     *
     * @param array $attributes
     *
     * @return User
     */
    private function createTestReadOnlyAdmin(array $attributes = []): User
    {
        return $this->createTestUser($attributes, Role::ADMIN_READ_ONLY);
    }

    /**
     * Create test full rights admin using factory.
     *
     * @param array $attributes
     *
     * @return User
     */
    private function createTestAdmin(array $attributes = []): User
    {
        return $this->createTestUser($attributes, Role::ADMIN);
    }

    /**
     * Create test technical admin using factory.
     *
     * @param array $attributes
     *
     * @return User
     */
    private function createTechnicalAdmin(array $attributes = []): User
    {
        return $this->createTestUser($attributes, Role::TECHNICAL_ADMIN);
    }
}
