<?php

namespace Modules\User\Models\Traits;

use Modules\User\Models\Role;

trait UserRoles
{
    /**
     * @return bool
     */
    public function isAdvertiser(): bool
    {
        return $this->hasRole(Role::ID_ADVERTISER);
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->hasRole(Role::ID_ADMIN) || $this->hasRole(Role::ADMIN_READ_ONLY);
    }

    /**
     * @return bool
     */
    public function isSuperAdmin(): bool
    {
        return $this->hasRole(Role::ID_ADMIN);
    }
}
