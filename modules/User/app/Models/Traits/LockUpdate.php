<?php

namespace Modules\User\Models\Traits;

use Modules\User\Models\User;

/**
 * @mixin User
 */
trait LockUpdate
{
    /**
     * Lock business info update
     *
     * @return User
     */
    public function lockUpdate(): User
    {
        $this->update(['lock_update' => true]);
        return $this->refresh();
    }

    /**
     * Unlock business info update
     *
     * @return User
     */
    public function unlockUpdate(): User
    {
        $this->update(['lock_update' => false]);
        return $this->refresh();
    }
}
