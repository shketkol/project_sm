<?php

namespace Modules\User\Models\Traits;

use Modules\User\Models\User;

/**
 * @mixin User
 */
trait RetryPayment
{
    /**
     * @return bool
     */
    public function canRetryPayment(): bool
    {
        return $this->retry_payment && $this->isInactiveUnpaid();
    }

    /**
     * @return bool
     */
    public function maySetPaymentProcessing(): bool
    {
        return !$this->retry_payment && $this->isInactiveUnpaid();
    }

    /**
     * @return bool
     */
    public function needPaymentUpdate(): bool
    {
        return !$this->retry_payment_processing && $this->isInactiveUnpaid();
    }

    /**
     * @return bool
     */
    public function isRetryPaymentProcessing(): bool
    {
        return $this->retry_payment_processing && $this->isInactiveUnpaid();
    }
}
