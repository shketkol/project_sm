<?php

namespace Modules\User\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\User\Models\UserMetaData;

/**
 * Trait HasUserMetaData
 *
 * @package Modules\User\Models\Traits
 * @property UserMetaData $metaData
 */
trait HasUserMetaData
{
    /**
     * Relation to meta data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function metaData(): HasOne
    {
        return $this->hasOne(UserMetaData::class, 'user_id')->withDefault([
            'onboarding_completed' => false,
        ]);
    }
}
