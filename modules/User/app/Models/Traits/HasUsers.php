<?php

namespace Modules\User\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\User\Models\User;

/**
 * @property User[]|\Illuminate\Support\Collection $users
 */
trait HasUsers
{
    /**
     * Get the users for the status.
     *
     * @return HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'status_id');
    }
}
