<?php

namespace Modules\User\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;

/**
 * @mixin User
 * @property UserStatus $status
 */
trait BelongsToUserStatus
{
    /**
     * Get status for the user.
     *
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this
            ->belongsTo(UserStatus::class)
            ->withDefault([
                'id' => UserStatus::ID_CREATE_IN_PROGRESS,
            ]);
    }

    /**
     * Check if user creation is in progress.
     *
     * @return bool
     */
    public function isCreateInProgress(): bool
    {
        return $this->status_id === UserStatus::ID_CREATE_IN_PROGRESS;
    }

    /**
     * Check if user is activated.
     *
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status_id === UserStatus::ID_ACTIVE;
    }

    /**
     * Returns true if used is pending manual review.
     *
     * @return bool
     */
    public function isPendingManualReview(): bool
    {
        return $this->status_id === UserStatus::ID_PENDING_MANUAL_REVIEW;
    }

    /**
     * @return bool
     */
    public function isInactive(): bool
    {
        return $this->status_id === UserStatus::ID_INACTIVE;
    }

    /**
     * Returns true if used is inactive unpaid.
     *
     * @return bool
     */
    public function isInactiveUnpaid(): bool
    {
        return $this->status_id === UserStatus::ID_INACTIVE_UNPAID;
    }

    /**
     * @return bool
     */
    public function isDeactivated(): bool
    {
        return !$this->isActive();
    }

    /**
     * @return bool
     */
    public function isIncomplete(): bool
    {
        return !$this->last_login && $this->status_id === UserStatus::ID_ACTIVE;
    }

    /**
     * Return fake incomplete status.
     *
     * @return string
     */
    public function getStatusAttribute(): UserStatus
    {
        $status = $this->status()->first();

        if ($this->isIncomplete()) {
            $status = new UserStatus(['id' => UserStatus::ID_INCOMPLETE, 'name' => UserStatus::INCOMPLETE]);
        }

        return $status;
    }
}
