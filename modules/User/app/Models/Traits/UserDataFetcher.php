<?php

namespace Modules\User\Models\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Auth;
use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Admin\User\AdminDataGet;
use Modules\Haapi\Actions\Admin\User\AdminUserGet;
use Modules\Haapi\Actions\Admin\User\SelfDataGet;
use Modules\Haapi\Actions\User\UserGet;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\User\DataTransferObjects\AccessorsUserData;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

/**
 * @mixin User
 */
trait UserDataFetcher
{
    use ActAsAdmin;

    /**
     * @return LoggerInterface
     */
    private function log(): LoggerInterface
    {
        return app(LoggerInterface::class);
    }

    /**
     * @return AccessorsUserData
     *
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function fetchData(): AccessorsUserData
    {
        $currentUser = Auth::guard('web')->user();
        if ($currentUser) {
            return $this->handleCurrentUser($currentUser);
        }

        $currentAdmin = Auth::guard('admin')->user();
        if ($currentAdmin) {
            return $this->handleCurrentAdmin($currentAdmin);
        }

        // No user is logged in, fetch by technical admin
        return $this->fetchByAdmin($this, $this->authAsAdmin());
    }

    /**
     * @param User $currentUser
     *
     * @return AccessorsUserData
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function handleCurrentUser(User $currentUser): AccessorsUserData
    {
        if ($currentUser->id !== $this->id) {
            return $this->fetchByAdmin($this, $this->authAsAdmin());
        }

        return $this->fetchUserSelfData();
    }

    /**
     * @param User $currentAdmin
     *
     * @return AccessorsUserData
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    private function handleCurrentAdmin(User $currentAdmin): AccessorsUserData
    {
        if ($currentAdmin->id === $this->id) {
            return $this->fetchAdminSelfData();
        }

        return $this->fetchByAdmin($this, $currentAdmin->id);
    }

    /**
     * Fetch user self data
     *
     * @return AccessorsUserData
     *
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    private function fetchUserSelfData(): AccessorsUserData
    {
        $this->log()->info('Fetch user data for user by himself.', [
            'user_id' => $this->id,
        ]);

        /** @var UserGet $userGetService */
        $userGetService = app(UserGet::class);
        $data = $userGetService->handle($this->id);

        return new AccessorsUserData([
            'email'     => $data->email,
            'firstName' => $data->firstName,
            'lastName'  => $data->lastName,
            'phone'     => $data->company->phoneNumber,
        ]);
    }

    /**
     * Fetch admin self data
     *
     * @return AccessorsUserData
     *
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    private function fetchAdminSelfData(): AccessorsUserData
    {
        $this->log()->info('Fetch admin data for admin by himself.', [
            'admin_id' => $this->id,
        ]);

        /** @var SelfDataGet $adminSelfGet */
        $adminSelfGet = app(SelfDataGet::class);
        $data = $adminSelfGet->handle($this->id);

        return new AccessorsUserData([
            'email'     => $data->email,
            'firstName' => $data->firstName,
            'lastName'  => $data->lastName,
        ]);
    }

    /**
     * Fetch data by admin.
     *
     * @param User $user
     * @param int  $adminId
     *
     * @return AccessorsUserData
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    private function fetchByAdmin(User $user, int $adminId): AccessorsUserData
    {
        if (!$user->isAdmin()) {
            return $this->fetchUser($user, $adminId);
        }

        if ($user->id === $adminId) {
            return $this->fetchAdminSelfData();
        }

        return $this->fetchAdmin($user, $adminId);
    }

    /**
     * Fetch user data by admin.
     *
     * @param User $user
     * @param int  $adminId
     *
     * @return AccessorsUserData
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    private function fetchUser(User $user, int $adminId): AccessorsUserData
    {
        $this->log()->info('Fetch user data for user by admin.', [
            'user_id'  => $user->id,
            'admin_id' => $adminId,
        ]);

        /** @var AdminUserGet $userGetService */
        $userGetService = app(AdminUserGet::class);
        $data = $userGetService->handle($user, $adminId);

        return new AccessorsUserData([
            'email'     => $data->email,
            'firstName' => $data->firstName,
            'lastName'  => $data->lastName,
        ]);
    }

    /**
     * Fetch admin data by admin
     * Operation like send notification based on haapi notification for admin
     *
     * @param User $admin
     * @param int  $adminId
     *
     * @return AccessorsUserData
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     */
    private function fetchAdmin(User $admin, int $adminId): AccessorsUserData
    {
        $this->log()->info('Fetch admin data for admin by another admin.', [
            'user_id'  => $admin->id,
            'admin_id' => $adminId,
        ]);

        /** @var AdminDataGet $adminGetService */
        $adminGetService = app(AdminDataGet::class);
        $data = $adminGetService->handle($adminId, $admin->external_id);

        return new AccessorsUserData([
            'email'     => $data->email,
            'firstName' => $data->firstName,
            'lastName'  => $data->lastName,
        ]);
    }
}
