<?php

namespace Modules\User\Models\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\User\Models\User;

/**
 * @mixin User
 * @property-read string $email
 * @property-read string $first_name
 * @property-read string $last_name
 * @property-read string $full_name
 * @property-read string $phone
 */
trait HaapiAccessors
{
    use UserDataFetcher;

    /**
     * @return string
     *
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getFirstNameAttribute()
    {
        return $this->getHaapiAttribute(self::FIRST_NAME_KEY);
    }

    /**
     * @return string
     *
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getLastNameAttribute()
    {
        return $this->getHaapiAttribute(self::LAST_NAME_KEY);
    }

    /**
     * Get user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        // admin
        if (!empty($this->account_name)) {
            return $this->account_name;
        }

        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * @return string
     *
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getEmailAttribute()
    {
        return $this->getHaapiAttribute(self::EMAIL_KEY);
    }

    /**
     * @return string
     *
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getPhoneAttribute()
    {
        return $this->getHaapiAttribute(self::PHONE_KEY);
    }

    /**
     * @param string $key
     *
     * @return string
     */
    private function getCacheKey(string $key): string
    {
        return "{$key}_{$this->id}";
    }

    /**
     * @param string $key
     *
     * @return mixed
     *
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getHaapiAttribute(string $key)
    {
        $cacheKey = $this->getCacheKey($key);
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $this->refreshAttributes();

        return Cache::get($cacheKey);
    }

    /**
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function refreshAttributes()
    {
        $lifetime = config('user.accessors.cache_lifetime', 1800); //30min
        $userData = $this->fetchData();

        Cache::put($this->getCacheKey(self::FIRST_NAME_KEY), $userData->firstName, $lifetime);
        Cache::put($this->getCacheKey(self::LAST_NAME_KEY), $userData->lastName, $lifetime);
        Cache::put($this->getCacheKey(self::EMAIL_KEY), $userData->email, $lifetime);
        Cache::put($this->getCacheKey(self::PHONE_KEY), $userData->phone, $lifetime);
    }
}
