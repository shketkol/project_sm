<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int     $id
 * @property string  $name
 * @property boolean $targeting_ages
 * @property boolean $targeting_audiences
 * @property boolean $targeting_locations
 * @property boolean $targeting_genres
 */
class AccountType extends Model
{
    /**
     * Account types IDs.
     */
    public const ID_COMMON      = 1;
    public const ID_SPECIAL_ADS = 2;

    /**
     * Account types.
     */
    public const TYPE_COMMON      = 'common';
    public const TYPE_SPECIAL_ADS = 'special-ads';

    /**
     * Permissions that are dependent on account type.
     */
    public const PERMISSION_TARGETING_AGES = 'targeting_ages';
    public const PERMISSION_TARGETING_AUDIENCES = 'targeting_audiences';
    public const PERMISSION_TARGETING_LOCATIONS = 'targeting_locations';
    public const PERMISSION_TARGETING_GENRES = 'targeting_genres';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        self::PERMISSION_TARGETING_AGES,
        self::PERMISSION_TARGETING_AUDIENCES,
        self::PERMISSION_TARGETING_LOCATIONS,
        self::PERMISSION_TARGETING_GENRES
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
