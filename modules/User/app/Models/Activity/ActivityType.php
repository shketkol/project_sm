<?php

namespace Modules\User\Models\Activity;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class ActivityType extends Model
{
    public const ID_LOGIN = 1;
    public const ID_LOGOUT = 2;

    public const LOGIN = 'login';
    public const LOGOUT = 'logout';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
