<?php

namespace Modules\User\Models\Activity;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @mixin Model
 * @property int            $id
 * @property int            $user_id
 * @property int            $type_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Activity extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type_id',
        'parent_id',
    ];
}
