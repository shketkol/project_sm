<?php

namespace Modules\User\Models;

use Spatie\Permission\Models\Role as Model;

class Role extends Model
{
    /**
     * Default roles.
     */
    public const ID_ADMIN           = 1;
    public const ID_ADMIN_READ_ONLY = 2;
    public const ID_ADVERTISER      = 3;
    public const ID_TECHNICAL_ADMIN = 4;

    public const ADMIN           = 'admin';
    public const ADMIN_READ_ONLY = 'admin-read-only';
    public const ADVERTISER      = 'advertiser';
    public const TECHNICAL_ADMIN = 'technical-admin';

    /**
     * @var array
     */
    protected $fillable = ['name', 'guard_name'];
}
