<?php

namespace Modules\User\Models;

use App\Traits\HasFactory;
use App\Traits\State;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Creative\Models\Traits\HasManyCreatives;
use Modules\Notification\Models\Traits\MorphAlerts;
use Modules\Payment\Models\Traits\HasPaymentMethods;
use Modules\User\Models\Traits\BelongsToAccountType;
use Modules\User\Models\Traits\HaapiAccessors;
use Modules\Report\Models\Traits\HasUserReports;
use Modules\User\Models\Traits\HasCampaigns;
use Modules\User\Models\Traits\HasNotifications;
use Modules\User\Models\Traits\HasReminders;
use Modules\User\Models\Traits\LockUpdate;
use Modules\User\Models\Traits\RememberToken;
use Modules\User\Models\Traits\RetryPayment;
use Modules\User\Models\Traits\Storage;
use Modules\User\Models\Traits\UserRoles;
use Modules\User\Models\Traits\BelongsToUserStatus;
use Modules\User\Models\Traits\HasUserMetaData;
use Spatie\Permission\Traits\HasRoles;

/**
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @mixin \Illuminate\Database\Eloquent\Model
 * @property int               $id
 * @property string            $external_id
 * @property string            $account_external_id
 * @property string            $company_name
 * @property int|null          $account_type_id
 * @property string            $account_name Same as full name but stored in DB and only admins
 * @property int|null          $status_id
 * @property boolean           $lock_update
 * @property boolean           $retry_payment
 * @property boolean           $retry_payment_processing
 * @property \Carbon\Carbon    $approved_at
 * @property \Carbon\Carbon    $created_at
 * @property \Carbon\Carbon    $rejected_at
 * @property \Carbon\Carbon    $last_login
 * @property \Carbon\Carbon    $notified_at
 * @property \Carbon\Carbon    $inactive_unpaid_at
 * @property Role[]|Collection $roles
 */
class User extends Authenticatable
{
    use BelongsToUserStatus,
        BelongsToAccountType,
        HasCampaigns,
        HasManyCreatives,
        HasPaymentMethods,
        HasRoles,
        HasUserMetaData,
        HasUserReports,
        HasNotifications,
        MorphAlerts,
        State,
        HaapiAccessors,
        RememberToken,
        UserRoles,
        Storage,
        LockUpdate,
        HasReminders,
        RetryPayment,
        HasFactory;

    /*** Keys to retrieve rows stored in HAAPI */
    private const EMAIL_KEY      = 'email';
    private const FIRST_NAME_KEY = 'first_name';
    private const LAST_NAME_KEY  = 'last_name';
    private const PHONE_KEY      = 'phone';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'status_id',
        'external_id',
        'last_login',
        'account_external_id',
        'notified_at',
        'inactive_unpaid_at',
        'lock_update',
        'retry_payment',
        'retry_payment_processing',
        'account_type_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'inactive_unpaid_at' => 'datetime',
    ];

    /**
     * @var string
     */
    protected $guard_name = 'web';

    /**
     * User state's flow.
     *
     * @var string
     */
    protected $flow = 'user';

    /**
     * HAAPI external id.
     *
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->account_external_id;
    }
}
