<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\Traits\HasUsers;

/**
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @property integer $id
 * @property string  $name
 */
class UserStatus extends Model
{
    use HasUsers;

    /**
     * Standard statuses.
     */
    public const ID_CREATE_IN_PROGRESS    = 1;
    public const ID_PENDING_MANUAL_REVIEW = 2;
    public const ID_ACTIVE                = 3;
    public const ID_INACTIVE              = 4;
    public const ID_INACTIVE_UNPAID       = 5;
    public const ID_INCOMPLETE            = 6;

    /**
     * HAAPI is processing the account approval. This is typically a short-term status.
     */
    public const CREATE_IN_PROGRESS = 'create_in_progress';

    /**
     * These are the Exception cases. We don’t know enough about you to let you buy.
     *
     * The account approval by HAAPI has encountered an error during the creation process and
     * requires manual approval by an Admin, for example, when duplicate accounts are found or
     * automatic validation of the advertiser as a real business fails.
     */
    public const PENDING_MANUAL_REVIEW = 'pending_manual_review';

    /**
     * A user has become Active in HAAPI, but has not logged in yet.
     *
     * This was intended to be an Admin-page status, to allow success managers to identify users
     * who have not logged in yet "we notice you haven’t logged in yet!"
     *
     * This status not implemented.
     */
    public const INCOMPLETE = 'incomplete';

    /**
     * The advertiser has been onboarded and can create campaigns.
     */
    public const ACTIVE = 'active';

    /**
     * The advertiser has been manually deactivated by the Admin either through the UI
     * or HAAPI and can no longer create campaigns.
     */
    public const INACTIVE = 'inactive';

    /**
     * The advertiser has been automatically deactivated through HAAPI due to a failed payment.
     * To activate the account, the advertiser must resolve the payment issue.
     */
    public const INACTIVE_UNPAID = 'inactive_unpaid';


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get translated status name.
     *
     * @return string
     */
    public function getTranslatedNameAttribute(): string
    {
        return __("user::labels.statuses.{$this->name}");
    }
}
