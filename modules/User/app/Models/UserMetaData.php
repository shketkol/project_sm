<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\Traits\BelongsToUser;

/**
 * Class UserMetaData
 *
 * @package Modules\User\Models
 * @property bool $onboarded
 * @see UserMetaData::getOnboardedAttribute()
 */
class UserMetaData extends Model
{
    use BelongsToUser;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'onboarding_completed',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Returns true if user is already onboarded.
     *
     * @return bool
     */
    public function getOnboardedAttribute(): bool
    {
        return $this->onboarding_completed;
    }
}
