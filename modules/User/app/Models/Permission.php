<?php

namespace Modules\User\Models;

use Spatie\Permission\Models\Permission as Model;

class Permission extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'guard_name'];
}
