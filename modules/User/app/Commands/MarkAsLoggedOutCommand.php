<?php

namespace Modules\User\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Modules\User\Actions\Activities\StoreLogoutAction;
use Modules\User\Repositories\Criteria\LoginAfterCriteria;
use Modules\User\Repositories\Criteria\LoginBeforeCriteria;
use Modules\User\Repositories\Criteria\LogoutAfterCriteria;
use Modules\User\Repositories\ActivityRepository;
use Modules\Report\Repositories\Criteria\UserIdsCriteria;
use Modules\User\Models\User;
use Modules\User\Repositories\UserRepository;
use Psr\Log\LoggerInterface;

class MarkAsLoggedOutCommand extends Command
{
    public const CRON_PERIOD_MINUTES = 60;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activities:mark-as-logged-out';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark advertisers as logged out. Runs each 60 minutes. '
    . 'Take all login records in between 1 hour that were 24h ago. Add for each expired login record logout record.';

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var StoreLogoutAction
     */
    private $storeLogoutAction;

    /**
     * @param LoggerInterface    $log
     * @param ActivityRepository $activityRepository
     * @param UserRepository     $userRepository
     * @param StoreLogoutAction  $storeLogoutAction
     */
    public function __construct(
        LoggerInterface $log,
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        StoreLogoutAction $storeLogoutAction
    ) {
        parent::__construct();
        $this->log = $log;
        $this->activityRepository = $activityRepository;
        $this->userRepository = $userRepository;
        $this->storeLogoutAction = $storeLogoutAction;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();
        $loginRows = $this->getLoginRows($dateFrom, $dateTo);
        $logoutRows = $this->getLogoutRows($dateFrom, $loginRows);
        $notLoggedOutRows = $this->getNotLoggedOutRows($loginRows, $logoutRows);
        $this->markAsLoggedOut($notLoggedOutRows);
    }

    /**
     * @return Carbon
     */
    private function getDateFrom(): Carbon
    {
        $minutes = config('session.lifetime');

        // take session lifetime (24h) + cron period (1h) so it would not overlap
        return Carbon::now()->subMinutes($minutes + self::CRON_PERIOD_MINUTES);
    }

    /**
     * @return Carbon
     */
    private function getDateTo(): Carbon
    {
        return Carbon::now()->subMinutes(config('session.lifetime'));
    }

    /**
     * @param Carbon $dateFrom
     * @param Carbon $dateTo
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getLoginRows(Carbon $dateFrom, Carbon $dateTo): array
    {
        $rows = $this->activityRepository
            ->pushCriteria(new LoginAfterCriteria($dateFrom))
            ->pushCriteria(new LoginBeforeCriteria($dateTo))
            ->all()
            ->toArray();

        $this->activityRepository->reset();

        $this->log->info('[Activity] User login rows.', [
            'from_date' => $dateFrom,
            'to_date'   => $dateTo,
            'rows'      => $rows,
        ]);

        return $rows;
    }

    /**
     * @param Carbon $dateFrom
     * @param array  $loginRows
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getLogoutRows(Carbon $dateFrom, array $loginRows): array
    {
        $loggedInUsers = collect($loginRows)->pluck('user_id')->unique()->toArray();
        $rows = $this->activityRepository
            ->pushCriteria(new LogoutAfterCriteria($dateFrom))
            ->pushCriteria(new UserIdsCriteria($loggedInUsers))
            ->all()
            ->toArray();

        $this->activityRepository->reset();

        $this->log->info('[Activity] Users logout rows.', [
            'from_date' => $dateFrom,
            'rows'      => $rows,
        ]);

        return $rows;
    }

    /**
     * Get user login rows but without corresponding logout row
     *
     * @param array $loginRows
     * @param array $logoutRows
     *
     * @return array
     */
    private function getNotLoggedOutRows(array $loginRows, array $logoutRows): array
    {
        $parentIds = $this->findLoginRowsThatAlreadyLoggedOut($logoutRows);
        $loginRows = $this->removeLoginRowsThatAlreadyLoggedOut($loginRows, $parentIds);

        $this->trackOldLogoutRows($logoutRows, $parentIds);

        $this->log->info('[Activity] Users rows with active session that did not logged out.', [
            'login_rows' => $loginRows,
        ]);

        return $loginRows;
    }

    /**
     * Added to be able to check if old functional would stop after 24h
     *
     * @param array $logoutRows
     * @param array $parentIds
     *
     * @deprecated
     */
    private function trackOldLogoutRows(array $logoutRows, array $parentIds): void
    {
        // remove logout rows that have parent_id
        foreach ($logoutRows as $key => $logoutRow) {
            if (in_array(Arr::get($logoutRow, 'parent_id'), $parentIds)) {
                unset($logoutRows[$key]);
            }
        }

        // old logouts should stop 24h after deployment
        if (!empty($logoutRows)) {
            $this->log->info('[Activity] User logout rows without parent_id.', [
                'logout_rows' => $logoutRows,
            ]);
        }
    }

    /**
     * @param array $loginRows
     * @param array $parentIds
     *
     * @return array
     */
    private function removeLoginRowsThatAlreadyLoggedOut(array $loginRows, array $parentIds): array
    {
        foreach ($loginRows as $key => $loginRow) {
            if (in_array(Arr::get($loginRow, 'id'), $parentIds)) {
                unset($loginRows[$key]);
            }
        }

        return $loginRows;
    }

    /**
     * @param array $logoutRows
     *
     * @return array
     */
    private function findLoginRowsThatAlreadyLoggedOut(array $logoutRows): array
    {
        return collect($logoutRows)
            ->pluck('parent_id')
            ->unique()
            ->filter(function (?int $value): bool {
                return $value != null;
            })
            ->toArray();
    }

    /**
     * @param array $loginRows
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function markAsLoggedOut(array $loginRows): void
    {
        $userIds = collect($loginRows)->pluck('user_id')->unique()->toArray();
        $users = $this->userRepository->findWhereIn('id', $userIds)->keyBy('id');

        foreach ($loginRows as $loginRow) {
            /** @var User $user */
            $user = Arr::get($users, Arr::get($loginRow, 'user_id'));
            $this->storeLogoutAction->handle($user, Arr::get($loginRow, 'id'));
        }
    }
}
