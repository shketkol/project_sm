<?php

namespace Modules\User\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Modules\User\Repositories\ActivityRepository;
use Psr\Log\LoggerInterface;

class CleanupActivitiesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activities:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup activities table records older then 1 month.';

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @param LoggerInterface    $log
     * @param ActivityRepository $activityRepository
     * @param DatabaseManager    $databaseManager
     */
    public function __construct(
        LoggerInterface $log,
        ActivityRepository $activityRepository,
        DatabaseManager $databaseManager
    ) {
        parent::__construct();
        $this->log = $log;
        $this->activityRepository = $activityRepository;
        $this->databaseManager = $databaseManager;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->log->info('[Activity] Started deleting old records.');

        try {
            $date = $this->getDate();
            $count = $this->delete($date);
        } catch (\Throwable $exception) {
            $this->log->warning('[Activity] Failed deleting old records.', [
                'reason' => $exception->getMessage(),
            ]);
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->log->info('[Activity] Finished deleting old records.', [
            'from_date' => $date,
            'count'     => $count,
        ]);

        $this->databaseManager->commit();
    }

    /**
     * Get records older than config value
     *
     * @return Carbon
     */
    private function getDate(): Carbon
    {
        return Carbon::now()->startOfDay()->subMonths(config('activities.database.store_months'));
    }

    /**
     * @param Carbon $date
     *
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function delete(Carbon $date): int
    {
        $count = $this->activityRepository->deleteWhere([['created_at', '<', $date]]);
        $this->activityRepository->reset();

        return $count;
    }
}
