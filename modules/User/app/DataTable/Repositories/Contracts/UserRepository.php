<?php

namespace Modules\User\DataTable\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 *
 * @package Modules\User\DataTable\Repositories\Contracts
 */
interface UserRepository extends RepositoryInterface
{

}
