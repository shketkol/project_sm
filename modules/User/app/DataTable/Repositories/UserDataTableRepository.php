<?php

namespace Modules\User\DataTable\Repositories;

use App\Repositories\Repository;
use Modules\User\DataTable\Repositories\Contracts\UserRepository as UserRepositoryContract;
use Modules\User\Models\User;

class UserDataTableRepository extends Repository implements UserRepositoryContract
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
}
