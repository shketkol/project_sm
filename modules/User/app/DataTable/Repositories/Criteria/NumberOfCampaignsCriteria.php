<?php

namespace Modules\User\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignStatus;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class NumberOfCampaignsCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param Model|Builder       $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        $condition = sprintf('IF(campaigns.status_id != %d, campaigns.id, NULL)', CampaignStatus::ID_DRAFT);

        return $model
            ->addSelect(DB::raw("COUNT({$condition}) as `number_of_campaigns`"))
            ->groupBy([
                'users.id',
                'user_status',
                'company_name',
            ]);
    }
}
