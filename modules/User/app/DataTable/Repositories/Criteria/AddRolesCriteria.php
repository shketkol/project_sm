<?php

namespace Modules\User\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddRolesCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private $modelClass;

    /**
     * @param Model|null $model
     */
    public function __construct(Model $model = null)
    {
        if (is_null($model)) {
            $model = new User();
        }

        $this->modelClass = get_class($model);
    }

    /**
     * Apply criteria in query repository
     *
     * @param Model|Builder       $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model
            ->leftJoin('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
            ->where('model_has_roles.model_type', '=', $this->modelClass);
    }
}
