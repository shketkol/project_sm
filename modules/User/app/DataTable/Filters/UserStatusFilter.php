<?php

namespace Modules\User\DataTable\Filters;

use App\DataTable\Filters\DataTableFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Modules\User\Models\UserStatus;
use Yajra\DataTables\DataTableAbstract;

class UserStatusFilter extends DataTableFilter
{
    /**
     * Filter name
     *
     * @var string
     */
    public static $name = 'user-status';

    /**
     * @param DataTableAbstract $dataTable
     */
    public function filter(DataTableAbstract $dataTable): void
    {
        $this->makeFilter($dataTable, 'user_status', function (Builder $query, $values) {
            if (in_array(UserStatus::ID_INCOMPLETE, $values)) {
                Arr::forget($values, array_search(UserStatus::ID_INCOMPLETE, $values));
                $query->orWhere(function ($query) {
                    $query->where(['user_statuses.id' => UserStatus::ID_ACTIVE, 'last_login' => null]);
                });
            }

            if (in_array(UserStatus::ID_ACTIVE, $values)) {
                Arr::forget($values, array_search(UserStatus::ID_ACTIVE, $values));
                $query->orWhere(function ($query) {
                    $query->where(['user_statuses.id' => UserStatus::ID_ACTIVE])->whereNotNull('last_login');
                });
            }

            $query->orWhereIn('user_statuses.id', $values);
        });
    }

    /**
     * Get filter options
     *
     * @return array
     */
    public function options(): array
    {
        return UserStatus::all()
            ->pluck('name', 'id')
            ->mapWithKeys(function (string $value, int $key) {
                return [$key => trans("user::labels.statuses.{$value}")];
            })
            ->toArray();
    }
}
