<?php

namespace Modules\User;

use App\Providers\ModuleServiceProvider;
use Modules\User\Commands\CleanupActivitiesCommand;
use Modules\User\Commands\MarkAsLoggedOutCommand;
use Modules\User\DataTable\Repositories\Contracts\UserRepository as DataTableUserRepositoryInterface;
use Modules\User\DataTable\Repositories\UserDataTableRepository as DataTableUserRepository;
use Modules\User\Http\View\Composers\EmailComposer;
use Modules\User\Http\View\Composers\UserConfigComposer;
use Modules\User\Policies\UserPolicy;
use Modules\User\Repositories\Contracts\UserRepository as UserRepositoryInterface;
use Modules\User\Repositories\UserRepository;
use Modules\User\States\ActiveState;
use ReflectionException;

class UserServiceProvider extends ModuleServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [
        'state-machine.user.states.active'          => ActiveState::class,
        DataTableUserRepositoryInterface::class     => DataTableUserRepository::class,
        UserRepositoryInterface::class              => UserRepository::class,
    ];

    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'user' => UserPolicy::class,
    ];

    /**
     * List of all view composers.
     *
     * @var array
     */
    protected $composers = [
        EmailComposer::class,
        UserConfigComposer::class,
    ];

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        MarkAsLoggedOutCommand::class,
        CleanupActivitiesCommand::class,
    ];

    /**
     * Register any user services.
     *
     * @return void
     * @throws ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadRoutes();
        $this->loadConfigs(['state-machine', 'permission', 'user', 'activities']);
        $this->loadComposers();
        $this->commands($this->commands);
    }

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'user';
    }
}
