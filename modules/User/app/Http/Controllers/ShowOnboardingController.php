<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class ShowOnboardingController
 * @package Modules\User\Http\Controllers
 */
class ShowOnboardingController extends Controller
{
    /**
     * Display profile page.
     *
     * @return View
     */
    public function __invoke(): View
    {
        return view('user::onboarding');
    }
}
