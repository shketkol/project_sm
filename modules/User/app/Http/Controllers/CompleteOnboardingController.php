<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Modules\User\Actions\CompleteOnboardingAction;

/**
 * Class CompleteOnboardingController
 *
 * @package Modules\User\Http\Controllers
 */
class CompleteOnboardingController extends Controller
{
    /**
     * Complete onboarding for the authorized user.
     *
     * @param Guard $guard
     * @param CompleteOnboardingAction $completeOnboardingAction
     * @return JsonResponse
     */
    public function __invoke(Guard $guard, CompleteOnboardingAction $completeOnboardingAction): JsonResponse
    {
        $completeOnboardingAction->handle($guard->user());

        return response()->json(['redirect' => route('campaigns.index')]);
    }
}
