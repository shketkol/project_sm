<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class ShowProfileController
 * @package Modules\User\Http\Controllers
 */
class ShowProfileController extends Controller
{
    /**
     * Display profile page.
     *
     * @return View
     */
    public function __invoke(): View
    {
        return view('user::profile');
    }
}
