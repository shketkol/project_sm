<?php

namespace Modules\User\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Haapi\Actions\User\UserUpdate;
use Modules\User\DataTransferObjects\UserUpdateData;
use Modules\User\Http\Requests\UpdateUserRequest;
use Modules\User\Repositories\UserRepository;

/**
 * Class UpdateUserController
 *
 * @package Modules\User\Http\Controllers\Api
 */
class UpdateUserController extends Controller
{
    /**
     * @param UpdateUserRequest $request
     * @param UserRepository    $repository
     * @param UserUpdate        $action
     *
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws UnauthorizedException
     */
    public function __invoke(UpdateUserRequest $request, UserRepository $repository, UserUpdate $action): Response
    {
        $data = new UserUpdateData(Auth::id(), $request->toData());

        try {
            $action->handle($data, Auth::id());
            $repository->update($request->toData(), Auth::id());
        } catch (UnauthorizedException $exception) {
            throw $exception;
        } catch (HaapiException $exception) {
            return response(
                ['message' => trans('user::messages.profile.failed')],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return response(['message' => trans('user::messages.profile.update_personal')]);
    }
}
