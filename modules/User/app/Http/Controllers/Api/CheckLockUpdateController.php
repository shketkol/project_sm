<?php

namespace Modules\User\Http\Controllers\Api;

use Illuminate\Contracts\Auth\Authenticatable;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Haapi\Actions\User\Contracts\UserGet;
use Modules\User\Http\Resources\UserResource;

/**
 * Class CheckLockUpdateController
 *
 * @package Modules\User\Http\Controllers\Api
 */
class CheckLockUpdateController extends Controller
{
    /**
     * @param Authenticatable $user
     * @param UserGet         $userGetAction
     *
     * @return JsonResponse
     */
    public function __invoke(
        Authenticatable $user,
        UserGet $userGetAction
    ): JsonResponse {
        return response()->json([
            'data' => $user->lock_update
                ? null
                : new UserResource($userGetAction->handle($user->id)),
        ]);
    }
}
