<?php

namespace Modules\User\Http\Controllers\Api;

use Illuminate\Contracts\Auth\Authenticatable;
use App\Http\Controllers\Controller;
use Modules\Haapi\Actions\User\UserGet;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\User\Http\Resources\UserResource;

/**
 * Class ShowProfileController
 *
 * @package Modules\User\Http\Controllers\Api
 */
class ShowProfileController extends Controller
{
    /**
     * Show profile information.
     *
     * @param Authenticatable $user
     * @param UserGet $userGetAction
     * @return UserResource
     * @throws HaapiException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     */
    public function __invoke(Authenticatable $user, UserGet $userGetAction): UserResource
    {
        return new UserResource($userGetAction->handle($user->id));
    }
}
