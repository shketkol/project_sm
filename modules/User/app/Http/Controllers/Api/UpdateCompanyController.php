<?php

namespace Modules\User\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Haapi\Actions\User\Contracts\UserGet;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\User\DataTransferObjects\UserCompanyUpdateData;
use Modules\Haapi\Actions\Account\AccountUpdateProfile;
use Modules\User\Http\Requests\UpdateCompanyRequest;
use Modules\User\Http\Resources\UserResource;
use Modules\User\Models\User;

/**
 * Class UpdateCompanyController
 *
 * @package Modules\User\Http\Controllers\Api
 */
class UpdateCompanyController extends Controller
{
    /**
     * @param UpdateCompanyRequest $request
     * @param AccountUpdateProfile $action
     *
     * @return Response|UserResource
     * @throws HaapiException
     * @throws UnauthorizedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     */
    public function __invoke(
        UpdateCompanyRequest $request,
        AccountUpdateProfile $action,
        UserGet $userGetAction
    ) {
        /** @var User $user */
        $user = Auth::user();

        $data = new UserCompanyUpdateData($user->account_external_id, $request->toUpdateData());

        try {
            $action->handle($data, $user->id);
            $user->lockUpdate();
        } catch (UnauthorizedException $exception) {
            throw $exception;
        } catch (HaapiException $exception) {
            return response(['message' => trans('user::messages.profile.failed')], Response::HTTP_BAD_REQUEST);
        }

        return (new UserResource($userGetAction->handle($user->id)))->additional([
            'data' => [
                'message' => trans('user::messages.profile.update_business_review'),
            ]
        ]);
    }
}
