<?php

namespace Modules\User\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

/**
 * Class UpdatePasswordRequest
 * Validate update password request and prepare data to store in db
 *
 * @package Modules\User\Http\Requests
 */
class UpdatePasswordRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'password'     => $validationRules->only(
                'user.password',
                ['required', 'string', 'max']
            ),
            'new_password' => $validationRules->only(
                'user.password',
                ['required', 'string', 'min', 'max', 'confirmed', 'regex']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        return ['password' => $this->new_password];
    }
}
