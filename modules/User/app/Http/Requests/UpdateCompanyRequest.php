<?php

namespace Modules\User\Http\Requests;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'addressLine1' => $validationRules->only(
                'user.address.line1',
                ['required', 'string', 'max']
            ),
            'addressLine2' => $validationRules->only(
                'user.address.line2',
                ['string', 'max'],
                ['nullable']
            ),
            'phone'   => $validationRules->only(
                'user.phone',
                ['required', 'string', 'max', 'regex']
            ),
            'city'    => $validationRules->only(
                'user.address.city',
                ['required', 'string', 'max']
            ),
            'state'   => $validationRules->only(
                'user.address.state',
                ['required', 'string', 'max', 'exists']
            ),
            'zip'     => $validationRules->only(
                'user.address.zip',
                ['required', 'string', 'max', 'regex', 'exists', 'has_state']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        return [
            'address_line1' => $this->addressLine1,
            'address_line2' => $this->addressLine2,
            'city'          => $this->city,
            'state'         => $this->state,
            'zipcode'       => $this->zip,
        ];
    }

    public function toUpdateData(): array
    {
        return [
            'line1'        => $this->addressLine1,
            'line2'        => $this->addressLine2,
            'city'         => $this->city,
            'state'        => $this->state,
            'zipcode'      => $this->zip,
            'phone_number' => $this->phone,
        ];
    }
}
