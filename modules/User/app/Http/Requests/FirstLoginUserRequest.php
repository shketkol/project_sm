<?php

namespace Modules\User\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class FirstLoginUserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'token' => $validationRules->only(
                'user.token',
                ['required', 'string', 'max']
            ),
        ];
    }
}
