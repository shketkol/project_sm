<?php

namespace Modules\User\Http\Requests;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

/**
 * Class UpdateUserRequest
 *
 * @package Modules\User\Http\Requests
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'firstName'     => $validationRules->only(
                'user.first_name',
                ['required', 'string', 'max']
            ),
            'lastName' => $validationRules->only(
                'user.last_name',
                ['required', 'string', 'max']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        $data['first_name'] = $this->firstName;
        $data['last_name'] = $this->lastName;
        return $data;
    }
}
