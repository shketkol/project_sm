<?php

namespace Modules\User\Http\View\Composers;

use App\Helpers\SessionIdentifierHelper;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\View\View;
use Modules\Payment\Models\PaymentMethod;
use Modules\User\Models\User;

class UserConfigComposer
{
    /**
     * Variable name used in views.
     */
    const VAR_NAME = 'userConfig';

    /**
     * View composer will be attached to these views.
     *
     * @var array
     */
    public static $views = [
        'layouts.footer.user',
    ];

    /**
     * @var User
     */
    protected $user;

    /**
     * @var SessionIdentifierHelper
     */
    protected $userSessionIdentifier;

    /**
     * UserConfigComposer constructor.
     *
     * @param Guard                   $auth
     * @param SessionIdentifierHelper $userSessionIdentifier
     */
    public function __construct(Guard $auth, SessionIdentifierHelper $userSessionIdentifier)
    {
        $this->user                  = $auth->user();
        $this->userSessionIdentifier = $userSessionIdentifier;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     * @throws AuthenticationException|\Throwable
     */
    public function compose(View $view)
    {
        if (!$this->user) {
            $view->with(self::VAR_NAME, []);

            return;
        }

        $view->with(self::VAR_NAME, [
            'firstName'    => $this->user->first_name,
            'lastName'     => $this->user->last_name,
            'fullName'     => $this->getFullName(),
            'permissions'  => $this->getPermissions(),
            'currency'     => $this->getCurrency(),
            'states'       => $this->getStates(),
            'company_name' => $this->user->company_name,
            'email'        => $this->user->email,
            'isAdmin'      => $this->user->isAdmin(),
            'identifier'   => $this->userSessionIdentifier->getIdentifier(),
        ]);
    }

    /**
     * Get user's full name.
     *
     * @return string
     */
    protected function getFullName(): string
    {
        return $this->user->first_name . ' ' . $this->user->last_name;
    }

    /**
     * Get user's permissions.
     *
     * @return array
     * @throws \Exception
     */
    protected function getPermissions(): array
    {
        return $this->user->getAllPermissions()->pluck('name')->toArray();
    }

    /**
     * Get user currency.
     *
     * @return array
     */
    protected function getCurrency(): array
    {
        return config('currency');
    }

    /**
     * Get all states data for the user.
     *
     * @return array
     */
    protected function getStates(): array
    {
        /*** @var PaymentMethod $paymentMethod */
        $isInactiveUnpaidPending = false;
        $paymentMethod           = $this->user->paymentMethods->last();

        if ($paymentMethod && $this->user->inactive_unpaid_at) {
            $isInactiveUnpaidPending = $this->user->inactive_unpaid_at->lt($paymentMethod->updated_at);
        }

        return [
            'is_deactivated'                     => $this->user->isDeactivated(),
            'is_pending'                         => $this->user->isPendingManualReview(),
            'is_inactive'                        => $this->user->isInactive(),
            'is_inactive_unpaid'                 => $this->user->isInactiveUnpaid(),
            'is_retry_payment_processing'        => $this->user->isRetryPaymentProcessing(),
            'is_special_ads'                     => $this->user->isSpecialAds(),
            'is_inactive_unpaid_pending'         => $isInactiveUnpaidPending,
            'creative_draft_allowed'             => $this->user->can('creative.placeDraft'),
            'advertisers_report_allowed'         => $this->user->can('report.createAdvertisers'),
            'pending_campaigns_report_allowed'   => $this->user->can('report.createPendingCampaigns'),
            'missing_ads_report_allowed'         => $this->user->can('report.createMissingAds'),
            'access_age_targetings_allowed'      => $this->user->can('campaign.accessAgeTargetings'),
            'access_audience_targetings_allowed' => $this->user->can('campaign.accessAudienceTargetings'),
            'access_zip_targetings_allowed'      => $this->user->can('campaign.accessZipTargetings'),
            'access_genre_targetings_allowed'    => $this->user->can('campaign.accessGenreTargetings'),
        ];
    }
}
