<?php

namespace Modules\User\Http\View\Composers;

use Illuminate\View\View;

class EmailComposer
{
    /**
     * View composer will be attached to these views.
     *
     * @var array
     */
    public static $views = [
        'advertiser::emails.*',
        'auth::emails.*',
        'campaign::emails.*',
        'common.email.*',
        'creative::emails.*',
        'user::emails.*',
        'payment::emails.*',
    ];

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     * @throws \Exception
     */
    public function compose(View $view)
    {
        $view->with([
            'publisherCompanyName'     => config('general.company_name'),
            'publisherCompanyFullName' => config('general.company_full_name'),
            'supportEmail'             => config('general.company_support_email'),
            'publisherBusinessAddress' => config('general.company_business_address'),
            'publisherInstagramLink'   => config('general.links.instagram'),
            'publisherFacebookLink'    => config('general.links.facebook'),
            'publisherTwitterLink'     => config('general.links.twitter'),
            'publisherHelpLink'        => config('general.links.help'),
        ]);
    }
}
