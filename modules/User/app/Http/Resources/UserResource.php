<?php

namespace Modules\User\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\User\Models\User;

/**
 * @mixin \Modules\User\Models\User
 */
class UserResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $user = User::find($this->id);

        return [
            'id'           => $this->id,
            'first_name'   => $this->firstName,
            'last_name'    => $this->lastName,
            'email'        => $this->email,
            'phone_number' => $this->company->phoneNumber,
            'company'      => [
                'company_name'  => $this->company->companyName,
                'address_line1' => $this->company->companyAddress->line1,
                'address_line2' => $this->company->companyAddress->line2,
                'city'          => $this->company->companyAddress->city,
                'state'         => $this->company->companyAddress->state,
                'zipcode'       => $this->company->companyAddress->zipcode,
            ],
            'permissions' => [
                'update_business' => !$user->lock_update,
            ],
        ];
    }
}
