<?php

namespace Modules\User\Listeners;

use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Modules\Daapi\Events\Account\AccountUpdated;
use Modules\User\Models\User;
use Modules\User\Repositories\Contracts\UserRepository;
use Psr\Log\LoggerInterface;

class CompanyUpdateApprovedListener
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @param LoggerInterface $log
     * @param UserRepository  $repository
     */
    public function __construct(LoggerInterface $log, UserRepository $repository)
    {
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * Handle the event.
     *
     * @param AccountUpdated $event
     *
     * @return void
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Modules\User\Exceptions\UserNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \SM\SMException
     */
    public function handle(AccountUpdated $event): void
    {
        $companyData = $event->getAccount();
        $user = $this->repository->getByCompanyId($companyData->externalId);

        $this->approved($user, $companyData);
    }

    /**
     * @param User        $user
     * @param AccountData $companyData
     *
     * @return void
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    private function approved(User $user, AccountData $companyData)
    {
        $this->applyApprovedUpdate($user, $companyData);
    }

    /**
     * @param User $user
     * @param AccountData $companyData
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    private function applyApprovedUpdate(User $user, AccountData $companyData)
    {
        $user->applyHaapiStatus($companyData->accountStatus);
        $user->unlockUpdate();

        $this->log->info('Approved user\'s company data update has been applied.', ['user_id' => $user->id]);
    }
}
