<?php

namespace Modules\User\Listeners;

use Carbon\Carbon;
use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Daapi\Events\User\UserApproved;
use Modules\User\Actions\CreateUserAction;
use Modules\User\Actions\CreateUserMetaDataAction;
use Modules\User\Exceptions\UserNotUpdatedException;
use Modules\User\Models\User;
use Modules\User\Notifications\Admin\AccountSubmitted;
use Modules\User\Notifications\Advertiser\Rejected;
use Modules\User\Repositories\Contracts\UserRepository;
use Psr\Log\LoggerInterface;
use Modules\User\Notifications\Advertiser\Approved;

class UserApprovedListener
{
    /**
     * @var Dispatcher
     */
    private $notification;

    /**
     * @var CreateUserMetaDataAction
     */
    private $createUserMetaDataAction;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var CreateUserAction
     */
    private $createUserAction;

    /**
     * UserApprovedListener constructor.
     *
     * @param Dispatcher               $notification
     * @param LoggerInterface          $log
     * @param UserRepository           $repository
     * @param CreateUserMetaDataAction $createUserMetaDataAction
     * @param CreateUserAction         $createUserAction
     */
    public function __construct(
        Dispatcher $notification,
        LoggerInterface $log,
        UserRepository $repository,
        CreateUserMetaDataAction $createUserMetaDataAction,
        CreateUserAction $createUserAction
    ) {
        $this->log = $log;
        $this->notification = $notification;
        $this->repository = $repository;
        $this->createUserMetaDataAction = $createUserMetaDataAction;
        $this->createUserAction = $createUserAction;
    }

    /**
     * Handle the event.
     *
     * @param UserApproved $event
     *
     * @throws UserNotUpdatedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(UserApproved $event): void
    {
        $companyData = $event->getAccount();
        $userData = $event->getUser();

        try {
            $user = $this->createUserAction->handle($companyData->externalId, $userData, $companyData);
            $user->update([
                'approved_at' => Carbon::now(),
            ]);
        } catch (\Exception $exception) {
            $this->log->warning('Callback\'s data cannot be stored to user.', [
                'user_id'     => $user->id,
                'external_id' => $user->external_id,
                'status_to'   => $user->status,
            ]);

            throw UserNotUpdatedException::create('User data store failed.');
        }

        $user->refresh();
        $this->sendNotification($user);
    }

    /**
     * Send email notifications to advertiser and admin.
     *
     * @param User $user
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function sendNotification(User $user)
    {
        // Send notification to admin
        $this->notification->send(
            $this->repository->getAdmin(),
            new AccountSubmitted($user)
        );

        // User is pending manual review
        if ($user->isPendingManualReview()) {
            $this->notification->send(
                $user,
                new Rejected($user)
            );
            return;
        }

        // User is approved
        if ($user->isActive()) {
            $this->notification->send(
                $user,
                new Approved($user)
            );
        }
    }
}
