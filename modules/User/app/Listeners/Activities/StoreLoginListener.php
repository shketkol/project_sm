<?php

namespace Modules\User\Listeners\Activities;

use Modules\Auth\Events\UserLogin;
use Modules\User\Models\Activity\ActivityType;
use Modules\User\Repositories\ActivityRepository;
use Psr\Log\LoggerInterface;

class StoreLoginListener
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var ActivityRepository
     */
    private $repository;

    /**
     * @param LoggerInterface    $log
     * @param ActivityRepository $repository
     */
    public function __construct(LoggerInterface $log, ActivityRepository $repository)
    {
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * Handle the event.
     *
     * @param UserLogin $event
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(UserLogin $event): void
    {
        if (!$event->user->isAdvertiser()) {
            return;
        }

        $this->log->info('[Activity] Started storing advertiser login.', ['user_id' => $event->user->id]);

        $activity = $this->repository->storeLogin($event->user);

        $this->log->info('[Activity] Finished storing advertiser login.', [
            'user_id'     => $event->user->id,
            'activity_id' => $activity->id,
        ]);

        $this->log->info('[Activity] Started storing advertiser activity_id in session.', [
            'user_id'     => $event->user->id,
            'activity_id' => $activity->id,
        ]);

        session([ActivityType::LOGIN => $activity->id]);

        $this->log->info('[Activity] Finished storing advertiser activity_id in session.', [
            'user_id'     => $event->user->id,
            'activity_id' => $activity->id,
        ]);
    }
}
