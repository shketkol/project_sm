<?php

namespace Modules\User\Listeners\Activities;

use Illuminate\Auth\Events\Logout;
use Modules\User\Actions\Activities\StoreLogoutAction;

class StoreLogoutListener
{
    /**
     * @var StoreLogoutAction
     */
    private $action;

    /**
     * @param StoreLogoutAction $action
     */
    public function __construct(StoreLogoutAction $action)
    {
        $this->action = $action;
    }

    /**
     * Handle the event.
     *
     * @param Logout $event
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(Logout $event): void
    {
        if (is_null($event->user)) {
            return;
        }

        $this->action->handle($event->user);
    }
}
