<?php

namespace Modules\User\Listeners;

use Carbon\Carbon;
use Modules\Auth\Events\UserLogin;
use Modules\Haapi\Actions\User\Contracts\UserGet;
use Modules\User\Actions\Notifications\UpdateUserStatusAction;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class UserLoginListener
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UpdateUserStatusAction
     */
    private $updateUserStatusAction;

    /**
     * @var UserGet
     */
    private $advertiserGetAction;

    /**
     * @param LoggerInterface        $log
     * @param UserGet             $advertiserGetAction
     * @param UpdateUserStatusAction $updateUserStatusAction
     */
    public function __construct(
        LoggerInterface $log,
        UserGet $advertiserGetAction,
        UpdateUserStatusAction $updateUserStatusAction
    ) {
        $this->log = $log;
        $this->advertiserGetAction = $advertiserGetAction;
        $this->updateUserStatusAction = $updateUserStatusAction;
    }

    /**
     * Handle the event.
     *
     * @param UserLogin $event
     *
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Modules\User\Exceptions\UserNotFoundException
     * @throws \SM\SMException
     */
    public function handle(UserLogin $event): void
    {
        $this->updateLastLogin($event->user);

        if (!config('haapi.stubs.login')) {
            $this->updateUserStatus($event->user);
        }
    }

    /**
     * @param User $user
     */
    protected function updateLastLogin(User $user): void
    {
        $user->last_login = Carbon::now();
        $user->save();
    }

    /**
     * Since HAAPI can change account status without notifying platform we would check user status after each login.
     *
     * @param User $user
     *
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Modules\User\Exceptions\UserNotFoundException
     * @throws \SM\SMException
     */
    private function updateUserStatus(User $user): void
    {
        $this->log->info('Auto-update user status after login started.', ['user_id' => $user->getKey()]);

        if (is_null($user->getExternalId())) {
            $this->log->info('Auto-update user status after login skipped. User does not have account id.', [
                'user_id' => $user->getKey(),
            ]);
            return;
        }

        $advertiser = $this->advertiserGetAction->handle($user->getKey());
        $this->updateUserStatusAction->handle($user->getExternalId(), $advertiser->company->accountStatus, []);

        $this->log->info('Auto-update user status after login finished.', ['user_id' => $user->getKey()]);
    }
}
