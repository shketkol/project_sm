<?php

namespace Modules\User\States;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Notification\Actions\DestroyAlertsAction;
use Modules\Notification\Models\AlertType;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;
use Modules\User\Notifications\Admin\ActiveFromBadPayment as AdminNotification;
use Modules\User\Notifications\Advertiser\ActiveFromBadPayment as AdvertiserNotification;
use Modules\User\Notifications\Advertiser\PendingAccountIsActivated;
use Modules\User\Repositories\UserRepository;
use Psr\Log\LoggerInterface;

class ActiveState extends UserState
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var DestroyAlertsAction
     */
    private $destroyAlertsAction;

    /**
     * @param Dispatcher $notification
     * @param LoggerInterface $log
     * @param UserRepository $userRepository
     * @param DestroyAlertsAction $destroyAlertsAction
     */
    public function __construct(
        Dispatcher $notification,
        LoggerInterface $log,
        UserRepository $userRepository,
        DestroyAlertsAction $destroyAlertsAction
    ) {
        parent::__construct($notification, $log);
        $this->userRepository = $userRepository;
        $this->destroyAlertsAction = $destroyAlertsAction;
    }

    /**
     * @param User $user
     *
     * @return void
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function after(User $user): void
    {
        if ($user->getOriginal('status_id') === UserStatus::ID_INACTIVE_UNPAID) {
            $this->activeFromBadPayment($user);
        }
        if ($user->getOriginal('status_id') === UserStatus::ID_PENDING_MANUAL_REVIEW) {
            $this->activeFromPending($user);
        }
    }

    /**
     * @param User $user
     *
     * @return void
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function activeFromBadPayment(User $user): void
    {
        $user->inactive_unpaid_at = null;
        $user->save();

        // Send notification to admin
        $this->notification->send(
            $this->userRepository->getAdmin(),
            new AdminNotification($user)
        );

        // Send notification to advertiser
        $this->notification->send(
            $user,
            new AdvertiserNotification($user)
        );

        $this->destroyAlertsAction->handle($user, [AlertType::ID_FAILED_PAYMENT]);

        $this->log->info('Active from bad payment notification was sent.', ['user' => $user->getKey()]);
    }

    /**
     * Send "Active from pending manual review" notification.
     *
     * @param User $user
     *
     * @return void
     */
    protected function activeFromPending(User $user): void
    {
        $this->notification->send(
            $user,
            new PendingAccountIsActivated($user)
        );

        $this->log->info('Active from pending manual review notification was sent.', ['user' => $user->getKey()]);
    }
}
