<?php

namespace Modules\User\States;

use Illuminate\Contracts\Notifications\Dispatcher;
use Psr\Log\LoggerInterface;

class UserState
{
    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @param Dispatcher      $notification
     * @param LoggerInterface $log
     */
    public function __construct(Dispatcher $notification, LoggerInterface $log)
    {
        $this->notification = $notification;
        $this->log = $log;
    }
}
