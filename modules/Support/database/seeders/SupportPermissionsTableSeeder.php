<?php

namespace Modules\Support\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Support\Policies\SupportPolicy;
use Modules\User\Models\Role;

class SupportPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [],
        Role::ID_ADMIN_READ_ONLY => [],
        Role::ID_ADVERTISER      => [
            SupportPolicy::PERMISSION_CONTACT_US,
        ],
    ];
}
