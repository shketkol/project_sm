<?php

namespace Modules\Support\Mail;

use App\Mail\Mail;

class Contact extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('support::emails.contact.subject', [
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('support::emails.contact')
            ->with($this->payload);
    }
}
