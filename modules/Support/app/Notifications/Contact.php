<?php

namespace Modules\Support\Notifications;

use App\Notifications\Notification;
use Illuminate\Support\Arr;
use Modules\Support\Mail\Contact as ContactMail;
use Modules\User\Models\User;

class Contact extends Notification
{
    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = ContactMail::class;

    /**
     * @var array
     */
    protected $payload;

    /**
     * @param array $payload
     */
    public function __construct(array $payload = [])
    {
        $this->payload = $payload;
    }

    /**
     * Get the notification's channels.
     *
     * @return array
     */
    public function via(): array
    {
        return ['mail'];
    }

    /**
     * Get notification payload.
     *
     * @param User $admin
     * @return array
     */
    protected function getPayload(User $admin): array
    {
        $user = Arr::get($this->payload, 'user');

        return [
            'publisherCompanyFullName' => config('general.company_full_name'),
            'company'                  => $user->company_name,
            'email'                    => $user->email,
            'firstName'                => $admin->first_name,
            'name'                     => $user->first_name,
            'subject'                  => Arr::get($this->payload, 'subject'),
            'text'                     => Arr::get($this->payload, 'message'),
            'title'                    => __('support::emails.contact.title'),
        ];
    }
}
