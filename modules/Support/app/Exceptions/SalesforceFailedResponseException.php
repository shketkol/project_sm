<?php

namespace Modules\Support\Exceptions;

use App\Exceptions\BaseException;

class SalesforceFailedResponseException extends BaseException
{
    /**
     * @param int $code
     *
     * @return self
     */
    public static function create(int $code): self
    {
        $message = sprintf('Got non-OK status code: "%d"', $code);

        return new self($message, $code);
    }
}
