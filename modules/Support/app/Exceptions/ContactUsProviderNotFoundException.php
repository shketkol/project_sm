<?php

namespace Modules\Support\Exceptions;

use App\Exceptions\ModelNotFoundException;
use Illuminate\Http\Response;

class ContactUsProviderNotFoundException extends ModelNotFoundException
{
    /**
     * @param string $name
     *
     * @return self
     */
    public static function create(string $name): self
    {
        $message = sprintf('Contact Us provider "%s" not found.', $name);

        return new self($message, Response::HTTP_NOT_FOUND);
    }
}
