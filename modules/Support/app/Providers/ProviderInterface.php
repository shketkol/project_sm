<?php

namespace Modules\Support\Providers;

interface ProviderInterface
{
    /**
     * Send contact us message.
     *
     * @param array $data
     */
    public function handle(array $data): void;
}
