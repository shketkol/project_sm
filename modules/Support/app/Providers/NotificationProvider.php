<?php

namespace Modules\Support\Providers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Support\Notifications\Contact;
use Modules\User\Repositories\Contracts\UserRepository;

class NotificationProvider implements ProviderInterface
{
    /**
     * @var Guard
     */
    protected $auth;

    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ContactAction constructor.
     *
     * @param Dispatcher     $notification
     * @param Guard          $auth
     * @param UserRepository $userRepository
     */
    public function __construct(Dispatcher $notification, Guard $auth, UserRepository $userRepository)
    {
        $this->auth = $auth;
        $this->notification = $notification;
        $this->userRepository = $userRepository;
    }

    /**
     * Send contact us email.
     *
     * @param array $data
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(array $data): void
    {
        $this->notification->send(
            $this->userRepository->getAdmin(),
            new Contact(array_merge($data, ['user' => $this->auth->user()]))
        );
    }
}
