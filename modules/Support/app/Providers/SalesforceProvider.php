<?php

namespace Modules\Support\Providers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Arr;
use Modules\Support\Jobs\ProcessSalesforceRequest;
use Psr\Log\LoggerInterface;

class SalesforceProvider implements ProviderInterface
{
    /**
     * @var Guard
     */
    private $auth;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param Guard           $auth
     * @param LoggerInterface $log
     */
    public function __construct(Guard $auth, LoggerInterface $log)
    {
        $this->auth = $auth;
        $this->log = $log;
    }

    /**
     * Send contact us to Salesforce.
     *
     * @param array $data
     */
    public function handle(array $data): void
    {
        $subjectField = config('contact-us.providers.salesforce.customer_issue.type');
        $params = [
            $subjectField => Arr::get($data, 'subject'),
            'subject'     => Arr::get($data, 'subject'),
            'description' => Arr::get($data, 'message'),
        ];
        $userParams = $this->prepareUserParams();
        $salesforceParams = $this->prepareSalesforceParams();

        $this->sendRequest(array_merge($params, $userParams, $salesforceParams));
    }

    /**
     * @return array
     */
    private function prepareUserParams(): array
    {
        /** @var \Modules\User\Models\User $user */
        $user = $this->auth->user();

        $this->log->info('[Salesforce] User used contact us form.', [
            'user_id' => $user->id,
        ]);

        return [
            'email' => $user->email,
            'name'  => $user->full_name,
            'phone' => $user->phone,
        ];
    }

    /**
     * @return array
     */
    private function prepareSalesforceParams(): array
    {
        $params = [
            'orgid'      => config('contact-us.providers.salesforce.orgid'),
            'retURL'     => config('contact-us.providers.salesforce.retURL'),
            'recordType' => config('contact-us.providers.salesforce.customer_issue.recordType'),
            'external'   => config('contact-us.providers.salesforce.external'),
        ];

        if (config('contact-us.providers.salesforce.debug.enabled')) {
            Arr::set($params, 'debug', config('contact-us.providers.salesforce.debug.enabled'));
            Arr::set($params, 'debugEmail', config('contact-us.providers.salesforce.debug.email'));
        }

        return $params;
    }

    /**
     * @param array $params
     */
    private function sendRequest(array $params): void
    {
        $this->log->info('[Salesforce] Dispatch ProcessSalesforceRequest job.');

        ProcessSalesforceRequest::dispatch($params, $this->auth->user());
    }
}
