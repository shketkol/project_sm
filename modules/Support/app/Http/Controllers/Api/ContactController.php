<?php

namespace Modules\Support\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Support\Actions\ContactAction;
use Modules\Support\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    /**
     * Send contact us message.
     *
     * @param ContactAction  $action
     * @param ContactRequest $request
     *
     * @return JsonResponse
     */
    public function __invoke(ContactAction $action, ContactRequest $request): JsonResponse
    {
        $action->handle($request->only(['subject', 'message']));

        return response()->json([
            'data' => [
                'message' => trans('support::messages.message_sent'),
            ],
        ]);
    }
}
