<?php

namespace Modules\Support\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

/**
 * Class ContactRequest
 * @package Modules\Support\Http\Requests
 */
class ContactRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'subject' => $validationRules->only('support.contact.subject', ['required', 'string', 'max']),
            'message' => $validationRules->only('support.contact.message', ['required', 'string', 'max']),
        ];
    }
}
