<?php

namespace Modules\Support\Jobs;

use App\Jobs\Job;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Response;
use Modules\Support\Exceptions\SalesforceFailedResponseException;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class ProcessSalesforceRequest extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * @var array
     */
    private $payload;

    /**
     * @var User
     */
    private $user;

    /**
     * @param array $payload
     * @param User  $user
     */
    public function __construct(array $payload, User $user)
    {
        $this->payload = $payload;
        $this->user = $user;
    }

    /**
     * Send request to Salesforce
     */
    public function handle(): void
    {
        $this->getLogger()->info('[Salesforce] Started sending request.', [
            'job_id'  => $this->job->getJobId(),
            'user_id' => $this->user->id,
        ]);

        try {
            $this->sendRequest();
        } catch (\Throwable $exception) {
            $this->handleFail($exception);
            return;
        }

        $this->getLogger()->info('[Salesforce] Finished sending request.', [
            'job_id'  => $this->job->getJobId(),
            'user_id' => $this->user->id,
        ]);
    }

    /**
     * @param \Throwable $exception
     */
    private function handleFail(\Throwable $exception): void
    {
        $this->getLogger()->warning('[Salesforce] Failed sending request.', [
            'reason'  => $exception->getMessage(),
            'job_id'  => $this->job->getJobId(),
            'user_id' => $this->user->id,
        ]);

        $this->getLogger()->info('[Salesforce] Delete job and mark as failed.', [
            'job_id'  => $this->job->getJobId(),
            'user_id' => $this->user->id,
        ]);
        $this->fail($exception);
    }

    /**
     * @throws SalesforceFailedResponseException
     */
    private function sendRequest(): void
    {
        $response = $this->getClient()->send($this->makeRequest());

        $this->getLogger()->debug('[Salesforce] Response data.', [
            'response' => [
                'headers' => $response->getHeaders(),
                'body'    => $response->getBody()->getContents(),
            ],
            'job_id'   => $this->job->getJobId(),
            'user_id'  => $this->user->id,
        ]);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw SalesforceFailedResponseException::create($response->getStatusCode());
        }
    }

    /**
     * @return Request
     */
    private function makeRequest(): Request
    {
        $request = new Request(
            \Illuminate\Http\Request::METHOD_POST,
            config('contact-us.providers.salesforce.url'),
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($this->payload, null, '&')
        );

        $this->getLogger()->debug('[Salesforce] Request data.', [
            'request' => [
                'headers' => $request->getHeaders(),
                'payload' => $this->payload,
                'body'    => $request->getBody()->getContents(),
            ],
            'job_id'  => $this->job->getJobId(),
            'user_id' => $this->user->id,
        ]);

        return $request;
    }

    /**
     * @return Client
     */
    private function getClient(): Client
    {
        return app(Client::class);
    }

    /**
     * @return LoggerInterface
     */
    private function getLogger(): LoggerInterface
    {
        return app('log');
    }
}
