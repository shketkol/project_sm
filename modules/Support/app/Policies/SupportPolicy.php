<?php

namespace Modules\Support\Policies;

use App\Policies\Policy;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\User\Models\User;

class SupportPolicy extends Policy
{
    use HandlesAuthorization;

    /**
     * Permissions.
     */
    public const PERMISSION_CONTACT_US = 'contact_us';

    /**
     * Returns true if user send emails via contact us form.
     *
     * @param User $user
     *
     * @return bool
     */
    public function contactUs(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CONTACT_US);
    }
}
