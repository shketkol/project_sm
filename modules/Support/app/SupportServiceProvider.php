<?php

namespace Modules\Support;

use App\Providers\ModuleServiceProvider;
use Modules\Support\Policies\SupportPolicy;

class SupportServiceProvider extends ModuleServiceProvider
{
    /**
     * @var bool
     */
    protected $pluralRoutePrefix = false;

    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'support' => SupportPolicy::class,
    ];

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'support';
    }

    /**
     * Register any user services.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();

        $this->loadRoutes();
        $this->loadConfigs(['contact-us']);
    }
}
