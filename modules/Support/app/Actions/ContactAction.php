<?php

namespace Modules\Support\Actions;

use Modules\Support\Exceptions\ContactUsProviderNotFoundException;
use Modules\Support\Providers\ProviderInterface;

class ContactAction
{
    /**
     * @var ProviderInterface
     */
    private $provider;

    /**
     * @param string|null $provider
     *
     * @throws ContactUsProviderNotFoundException
     */
    public function __construct(string $provider = null)
    {
        $this->provider = $this->makeProvider($provider);
    }

    /**
     * @param string|null $provider
     *
     * @return ProviderInterface
     * @throws ContactUsProviderNotFoundException
     */
    protected function makeProvider(string $provider = null): ProviderInterface
    {
        $provider = $provider ?? config('contact-us.default');
        $class = config("contact-us.providers.$provider.class");

        if (is_null($class) || !class_exists($class)) {
            throw ContactUsProviderNotFoundException::create($class);
        }

        return app($class);
    }

    /**
     * Send contact us message.
     *
     * @param array $data
     */
    public function handle(array $data = []): void
    {
        $this->provider->handle($data);
    }
}
