@extends('common.email.layout-admin')

@section('body')
<p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
    {{ __('emails.hi', ['name' => $firstName]) }}
</p>

<p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
    {{ __('support::emails.contact.body.part-1', [
        'publisher_company_full_name' => $publisherCompanyFullName
]) }}
</p>

<p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
    <b>{{ __('support::labels.subject') }}: </b>{{ $subject ?? '-' }}
</p>

<p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
    <b>{{ __('labels.name') }}: </b>{{ $name ?? '-' }}
</p>

<p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
    <b>{{ __('labels.business_name') }}: </b>{{ $company ?? '-' }}
</p>

<p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
    <b>{{ __('labels.email') }}: </b>@include('common.email.part.mailto',[
        'email' => $email
    ])
</p>

<p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
    <pre style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px; white-space: normal;"><b>{{ __('labels.message') }}: </b>{{ $text ?? '-' }}</pre>
</p>
@stop
