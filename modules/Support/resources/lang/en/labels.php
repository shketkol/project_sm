<?php

return [
    'enter_message_here'    => 'Enter your message.',
    'message'               => 'Message',
    'subject'               => 'Subject',
    'subjects'              => [
        'account_setup'        => 'Account Setup',
        'campaigns'            => 'Campaigns',
        'ads'                  => 'Ads',
        'billing_and_payments' => 'Billing & Payments',
        'other'                => 'General Questions / Other',
    ],
    'we_are_here_for_you'   => 'If you have a question or would like to share feedback, fill out the form below, and we\'ll get back to you shortly.',
];
