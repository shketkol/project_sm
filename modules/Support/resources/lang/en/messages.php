<?php

return [
    'message_sent' => 'One of our agents should be in contact within approximately 1-2 business days.',
];
