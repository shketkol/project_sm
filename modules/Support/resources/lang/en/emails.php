<?php

return [
    'contact' => [
        'subject' => ':publisher_company_full_name Notification: New message',
        'body'    => [
            'part-1' => 'An advertiser has sent you the following message through the :publisher_company_full_name:',
        ],
    ],
];
