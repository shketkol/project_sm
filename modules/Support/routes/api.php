<?php

Route::group([
    'middleware' => 'auth:web,admin',
], function () {
    Route::post('/contact', 'ContactController')->name('contact')->middleware('can:support.contactUs');
});
