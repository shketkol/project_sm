<?php
// @codingStandardsIgnoreFile

use Modules\Support\Providers\NotificationProvider;
use Modules\Support\Providers\SalesforceProvider;

return [
    /*
    |--------------------------------------------------------------------------
    | Contact Us
    |--------------------------------------------------------------------------
    |
    | These configs are used on contact us page.
    |
    */
    'default'   => env('CONTACT_US_PROVIDER', 'notification'),

    /*
    |--------------------------------------------------------------------------
    | Contact Us Provider
    |--------------------------------------------------------------------------
    |
    | Here you may configure the contact us providers for your application.
    |
    | Available Drivers: "notification", "salesforce"
    |
    */
    'providers' => [
        'notification' => [
            'class' => NotificationProvider::class,
        ],

        'salesforce' => [
            'class'          => SalesforceProvider::class,
            'url'            => env('SALESFORCE_CONTACT_US_URL', 'https://hulusales--FullSB.cs42.my.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8'),
            'orgid'          => env('SALESFORCE_CONTACT_US_ORG_ID', '00D560000002hvX'),
            'retURL'         => env('SALESFORCE_CONTACT_US_RET_URL', 'https://www.hulu.com'),
            'external'       => env('SALESFORCE_CONTACT_US_EXTERNAL', '1'),
            'customer_issue' => [
                'type'       => env('SALESFORCE_CONTACT_US_CUSTOMER_ISSUE_TYPE', '00N19000004DsAP'),
                'recordType' => env('SALESFORCE_CONTACT_US_CUSTOMER_ISSUE_RECORD_TYPE', '01256000000UgaF'),
            ],
            'debug'          => [
                'enabled' => env('SALESFORCE_CONTACT_US_DEBUG_ENABLED', env('APP_DEBUG', false)),
                'email'   => env('SALESFORCE_CONTACT_US_DEBUG_EMAIL', 'danadstest@gmail.com'),
            ],
        ],
    ],
];
