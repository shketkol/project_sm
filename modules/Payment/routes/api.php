<?php

Route::middleware(['auth:web,admin'])->group(function () {
    Route::group([
        'prefix' => 'payment-method',
        'as'     => 'payment-method.',
        'where'  => [
            'paymentMethod' => '[0-9]+',
        ],
    ], function () {

        Route::get('/', 'IndexPaymentMethodController')->name('index')->middleware('can:payment.list');
        Route::post('/store', 'StorePaymentMethodController')->name('store')->middleware('can:payment.create');
        Route::patch('/replace', 'ReplacePaymentMethodController')
            ->name('replace')
            ->middleware('can:payment.replace');

        Route::group([
            'prefix' => '{paymentMethod}',
            'where'  => [
                'paymentMethod' => '[0-9]+',
            ],
        ], function () {
            Route::get('/', 'ShowPaymentMethodController')
                ->name('show')
                ->middleware('can:payment.view,paymentMethod');
            Route::delete('/', 'DeletePaymentMethodController')
                ->name('destroy')
                ->middleware('can:payment.delete,paymentMethod');
            Route::post('/retry', 'RetryPaymentController')
                 ->name('retry')
                 ->middleware('can:payment.retry,paymentMethod');
        });
    });

    Route::group([
        'prefix' => 'order',
        'as'     => 'order.',
        'where'  => [
            'order' => '[0-9]+',
        ],
    ], function () {
        Route::group([
            'prefix' => '{orderId}',
            'where'  => [
                'order' => '[0-9]+',
            ],
        ], function () {
            Route::get('/', 'ShowOrderController')
                ->name('show')
                ->middleware('can:order.view,orderId');

            Route::get('/payment-pdf', 'DownloadOrderPdfController')
                ->name('pdf.download')
                ->middleware('can:order.view,orderId');
        });
    });
});
