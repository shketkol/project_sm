<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="css/pdf.css" rel="stylesheet" media="all">
    <style>
        @font-face {
          font-family: "Graphik";
          src: url(fonts/Graphik-Medium.eot?5a14bf45d59aa8ac9bfc03e395eb01bd);
          src: url(fonts/Graphik-Medium.eot?5a14bf45d59aa8ac9bfc03e395eb01bd) format("embedded-opentype"), url(fonts/Graphik-Medium.woff2?81459c895a84fe937c8a22c8868089b0) format("woff2"), url(fonts/Graphik-Medium.woff?e3611bfce40d242a59fd3b53b3c26588) format("woff"), url(fonts/Graphik-Medium.ttf?9a2a58264a160a90dd1737ef0292de0d) format("truetype"), url(fonts/Graphik-Medium.svg?240267bef12dfce18f4e00449eb3f796) format("svg");
          font-weight: 500;
          font-style: normal;
        }

        .budget-code {
          font-family: helvetica, sans-serif!important;
        }
    </style>
</head>
<body>
<table class="pdf-table">
    <tr class="pdf-table__header">
        <td colspan="5">
            <img src="{{ public_path('/images/Logo-black-beta-dark.svg') }}" alt="HULU Advertising">
        </td>
    </tr>
</table>
@yield('content')
</body>
</html>
