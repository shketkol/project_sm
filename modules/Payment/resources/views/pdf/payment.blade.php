@extends('payment::pdf.layouts.main')
@section('content')
    @include('payment::pdf.includes.payment.details')
    @include('payment::pdf.includes.payment.creative')
    @include('payment::pdf.includes.payment.transactions')
@endsection
