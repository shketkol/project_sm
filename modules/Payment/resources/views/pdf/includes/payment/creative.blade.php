<table class="pdf-table">
<tr>
    <td colspan="5" class="pdf-table__row">
        <table>
            <tr class="pdf-table__sub-row">
                <td colspan="4" class="title title-sm">
                    {{ trans('payment::labels.video_commercial') }}
                </td>
                <td class="title title-sm text-right">
                    {{ trans('payment::labels.total_budget') }}
                </td>
            </tr>
            <tr class="pdf-table__sub-row">
                <td class="col-2" style="width: 125px;">{{ trans('campaign::labels.date_range') }}:</td>
                <td colspan="3" class="title title-sm">
                    {{ \App\Helpers\DateFormatHelper::toMonthDayYear($data['campaign']['startDate']) }} -
                    {{ \App\Helpers\DateFormatHelper::toMonthDayYear($data['campaign']['endDate']) }}
                </td>
                <td class="text-right title title-sm">
                    @if($data['displayDiscountedBudget'] !== null && $data['promocode'])
                        <div class="discounts__container">
                            <div class="discounts">
                              <div class="discounts__body">
                                <p>{{ \App\Helpers\NumberFormatHelper::floatCurrency($data['budget']) }}</p>
                                <div>
                                  <div class="budget-tooltip">
                                    <span class="budget-code">
                                      Code Applied: <br><br>
                                      {{ $data['promocode'] }}
                                    </span>
                                  </div>
                                  -{{ \App\Helpers\NumberFormatHelper::floatCurrency($data['budget'] - $data['displayDiscountedBudget']) }}
                                </div>
                              </div>
                              <div class="discounts__summary">
                                <span class="text-body-sm">
                                    {{ \App\Helpers\NumberFormatHelper::floatCurrency($data['displayDiscountedBudget']) }}
                                </span>
                              </div>
                            </div>
                        </div>
                    @else
                        {{ \App\Helpers\NumberFormatHelper::floatCurrency($data['budget']) }}
                    @endif
                </td>
            </tr>
            <tr class="pdf-table__sub-row">
                <td>{{ trans('campaign::labels.target_impressions') }}:</td>
                <td class="title title-sm">{{ \App\Helpers\NumberFormatHelper::standart($data['campaign']['bookedImpressions']) }}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="pdf-table__sub-row">
                <td>{{ trans('payment::labels.unit_cost') }}:</td>
                <td class="title title-sm">{{ \App\Helpers\NumberFormatHelper::floatCurrency($data['campaign']['unitCost']) }}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="pdf-table__sub-row">
                <td>{{ trans('payment::labels.cost_method') }}:</td>
                <td class="title title-sm">{{ $data['campaign']['costMethod'] }}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </td>
</tr>
</table>
