<table class="pdf-table">
<tr>
    <td colspan="5" class="pdf-table__row">
        <table>
            <tr class="pdf-table__sub-row">
                <td colspan="4" class="title title-sm">
                    {{ trans('campaign::labels.campaign_details') }}
                </td>
                <td width="110px">
                    <div class="btn cell-btn btn--{{ strtolower($data['orderStatus']) }}">
                        {{ trans('payment::labels.statuses.' . $data['orderStatus']) }}
                    </div>
                </td>
            </tr>
            <tr class="pdf-table__sub-row">
                <td style="width: 125px;">{{ trans('labels.advertiser') }}:</td>
                <td colspan="3" class="title title-sm">{{ $data['campaign']['advertiserName'] }}</td>
            </tr>
            <tr class="pdf-table__sub-row">
                <td>{{ trans('campaign::labels.campaign_title') }}:</td>
                <td colspan="3" class="title title-sm">{{ $data['campaign']['campaignName'] }}</td>
            </tr>
            <tr class="pdf-table__sub-row">
                <td>{{ trans('labels.submit_date') }}:</td>
                <td colspan="3" class="title title-sm">{{ \App\Helpers\DateFormatHelper::formatted($data['campaign']['orderDate']) }}</td>
            </tr>
        </table>
    </td>
</tr>
</table>
