@if(!$data['transactions'] || !count($data['transactions']))
<table class="pdf-table">
    <tr>
        <td colspan="5" class="pdf-table__row">
            <table>
                <tr class="pdf-table__sub-row">
                    <td colspan="5" class="title title-sm">
                        {{ trans('payment::labels.transaction_history') }}
                    </td>
                </tr>
                <tr class="pdf-table__sub-row">
                    <th class="text-muted text-left">{{ trans('payment::labels.billing_period') }}</th>
                    <th class="text-muted text-left">{{ trans('payment::labels.transaction_id') }}</th>
                    <th class="text-muted text-left">{{ trans('payment::labels.transaction_date') }}</th>
                    <th class="text-muted text-left">{{ trans('payment::labels.status') }}</th>
                    <th class="text-muted text-right">{{ trans('payment::labels.payment_amount') }}</th>
                </tr>
                <tr class="pdf-table__sub-row pdf-table__sub-row--empty">
                    <td colspan="5" class="text-center">
                        {{ trans('payment::messages.no_activity_yet') }}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
@else
<div class="page-break">
<table class="pdf-table">
    <tr>
        <td colspan="5" class="pdf-table__row">
            <table>
                <tr class="pdf-table__sub-row">
                    <td colspan="5" class="title title-sm">
                        {{ trans('payment::labels.transaction_history') }}
                    </td>
                </tr>
                <tr class="pdf-table__sub-row">
                    <th class="text-muted text-left">{{ trans('payment::labels.billing_period') }}</th>
                    <th class="text-muted text-left">{{ trans('payment::labels.transaction_id') }}</th>
                    <th class="text-muted text-left">{{ trans('payment::labels.transaction_date') }}</th>
                    <th class="text-muted text-left">{{ trans('payment::labels.status') }}</th>
                    <th class="text-muted text-right">{{ trans('payment::labels.payment_amount') }}</th>
                    <th class="text-muted text-right">{{ trans('payment::labels.payment_method') }}</th>
                </tr>
                @foreach($data['transactions'] as $transaction)
                    <tr class="pdf-table__sub-row transactions-row">
                        <td class="transactions-col transactions-col-1">
                            {{ \App\Helpers\DateFormatHelper::toMonthDayYear($transaction->billingPeriodStart) }} -
                            {{ \App\Helpers\DateFormatHelper::toMonthDayYear($transaction->billingPeriodEnd) }}
                        </td>
                        <td class="transactions-col transactions-col-2">{{ $transaction->transactionId }}</td>
                        <td class="transactions-col transactions-col-3">{{ \Carbon\Carbon::parse($transaction->transactionDate)->format('m/d/Y') }}</td>
                        <td class="transactions-col transactions-col-4 {{\Modules\Payment\Models\TransactionStatus::
                            checkFailedByHaapiTransition($transaction->status) ? ' is-failed' : ''
                            }}">
                            {{ trans('payment::labels.transaction.statuses.' . strtolower($transaction->status)) }}
                        </td>
                        <td class="transactions-col transactions-col-5 text-right{{\Modules\Payment\Models\TransactionStatus::
                            checkFailedByHaapiTransition($transaction->status) ? ' is-failed' : ''
                            }}">
                            {{ \App\Helpers\NumberFormatHelper::floatCurrency($transaction->amount) }}
                        </td>
                        <td class="text-right">
                            <span class="card-cell">
                                @if($transaction->card->brand)
                                    <img src="{{ public_path('/images/cards/' . config("cards.icons.png.{$transaction->card->brand}")) }}" alt="payment_method">
                                @endif
                                @if($transaction->card->lastFour)
                                    <span class="card-number">{{ trans('payment::labels.only_ending_in', ['ending_id' => $transaction->card->lastFour]) }}</span>
                                @endif
                            </span>
                        </td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
</table>
</div>
@endif
<table class="pdf-table">
<tr>
    <td colspan="5" class="pdf-table__row pdf-table__row--bottom">
        <table>
            <tr class="pdf-table__sub-row">
                <td></td>
                <td></td>
                <td></td>
                <td class="text-right valign-center">{{ trans('payment::labels.paid_amount') }}:</td>
                <td class="title title-xl text-right valign-center col-2">
                    {{ \App\Helpers\NumberFormatHelper::floatCurrency($data['paidAmount']) }}
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
@if($data['dueAmount'])
<table class="pdf-table">
    <tr>
        <td colspan="3"></td>
        <td class="text-right valign-center"> {{ trans('payment::labels.amount_due') }}:</td>
        <td class="title title-xl text-right valign-center col-2 is-failed">
            {{ \App\Helpers\NumberFormatHelper::floatCurrency($data['dueAmount']) }}
        </td>
    </tr>
</table>
@endif
