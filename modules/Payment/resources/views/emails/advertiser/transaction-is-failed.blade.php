@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">{{ __('emails.hi', ['name' => $firstName]) }}</p>
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">
        {{ __('payment::emails.advertiser.transaction_is_failed.body.a_transaction_has_been_failed') }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">
        {!! __('payment::emails.advertiser.transaction_is_failed.body.campaign_name', [
            'campaign_name' => e($campaignName)
        ]) !!}
    </p>
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">
        {!! __('payment::emails.advertiser.transaction_is_failed.body.amount', [
            'amount' => e('$666')
        ]) !!}
    </p>
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; mso-line-height-rule: exactly;line-height:150%; margin:15px 0;">
        {!! __('payment::emails.advertiser.transaction_is_failed.body.you_can_find_more_details_here', [
            'order_details' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => 'Transactions - Order Summary',
            ]),
        ]) !!}
    </p>
@stop
