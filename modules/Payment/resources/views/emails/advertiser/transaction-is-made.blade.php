@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('payment::emails.advertiser.transaction_is_made.body.we_have_processed_a_transaction', [
            'campaign_name' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => $campaignName,
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('payment::emails.advertiser.transaction_is_made.body.for_transaction_details', [
            'campaign_details' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => 'Transactions - Campaign Details',
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    @include('common.email.part.if-you-have-any-questions')
@stop
