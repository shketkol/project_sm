<?php

return [
    'advertiser' => [
        'transaction_is_made'   => [
            'subject' => 'Campaign transaction processed',
            'title'   => 'Campaign Transaction Processed',
            'body'    => [
                'we_have_processed_a_transaction' => 'We’ve processed a transaction for your :campaign_name campaign.',
                'for_transaction_details'         => 'For transaction details, see :campaign_details.',
            ],
        ],
        'transaction_is_failed' => [
            'subject' => 'A Transaction Has Been Failed',
            'body'    => [
                'a_transaction_has_been_failed'  => 'A Transaction Has Been Failed:',
                'campaign_name'                  => 'Campaign Name: :campaign_name',
                'amount'                         => 'Amount: :amount',
                'you_can_find_more_details_here' => 'You can find more details here: :order_details.',
            ],
        ],
    ],
];
