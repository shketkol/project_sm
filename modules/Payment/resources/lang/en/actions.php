<?php

return [
    'add_new'            => 'Add New',
    'cancel'             => 'Cancel',
    'credit_card'        => 'Credit Card',
    'save_changes'       => 'Save Changes',
    'delete_card'        => 'Delete Card',
    'replace_card'       => 'Replace Card',
    'retry_payment'      => 'Retry Payment',
    'update_credit_card' => 'Update credit card.',
];
