<?php

use Modules\Payment\Models\OrderStatus;
use Modules\Payment\Models\TransactionStatus;

return [
    'statuses'                         => [
        OrderStatus::CURRENT => 'Current',
        OrderStatus::PENDING => 'Pending',
        OrderStatus::SETTLED => 'Settled',
        OrderStatus::FAILED  => 'Failed',
    ],
    'transaction'                      => [
        'statuses' => [
            TransactionStatus::INITIATED       => 'Initiated',
            TransactionStatus::SYSTEM_FAILED   => 'System Error',
            TransactionStatus::CUSTOMER_FAILED => 'Failed',
            TransactionStatus::SUCCESS         => 'Success',
        ],
    ],
    'add_credit_card'                  => 'Add Credit Card',
    'address'                          => 'Address',
    'address_placeholder'              => 'Enter the billing address.',
    'american_express'                 => 'American Express',
    'card_number'                      => 'Card Number',
    'card_number_placeholder'          => 'Enter the card number.',
    'cardholder_name'                  => 'Cardholder Name',
    'cardholder_name_placeholder'      => 'Enter the cardholder name.',
    'city'                             => 'City',
    'city_placeholder'                 => 'Enter the city.',
    'credit_card'                      => 'Credit Card',
    'responsibility_p1'                => 'It is your responsibility to comply with all applicable laws and industry regulations or standards.',
    'responsibility_p2'                => 'We will attempt to deliver against your budget across the life of your campaign, but cannot guarantee delivery. We will not, however, exceed your budget limit. For more details, including details on billing, please review our <a href=":terms_of_use_link" target="_blank">Ad Manager Terms</a>.',
    'diners'                           => 'Diners Club',
    'discover'                         => 'Discover',
    'ending_in'                        => ':brand ending in :ending_id',
    'only_ending_in'                   => 'ending in :ending_id',
    'expiration_date'                  => 'Expiration',
    'expiration_date_placeholder'      => 'Enter the expiration date (MM/YY).',
    'how_would_you_to_pay_info'        => 'You will not be billed today, you will be charged at the end of each billing period following your campaign’s start date.',
    'jcb'                              => 'JCB',
    'last_activity'                    => 'Last Activity',
    'make_default_payment_method'      => 'Make default payment method',
    'others'                           => 'MasterCard',
    'month'                            => 'Month',
    'payment_info'                     => 'Payment Information',
    'payment_method'                   => 'Payment Method',
    'payment_methods'                  => 'Payment methods',
    'transaction_status'               => 'Transaction Status',
    'save_for_future_use'              => 'Save for future use',
    'security_code'                    => 'Security Code',
    'security_code_placeholder'        => 'Enter the security code. ',
    'state_placeholder'                => 'Enter your state...',
    'subtotal'                         => 'SubTotal',
    'transaction_history'              => 'Transaction History',
    'unionpay'                         => 'UnionPay',
    'update_payment_info'              => 'Update payment information',
    'visa'                             => 'Visa',
    'we_accept_all_major_credit_cards' => 'We accept all major credit cards.',
    'we_accept_credit_cards'           => 'We accept all major credit cards.',
    'year'                             => 'Year',
    'your_payment_information'         => 'Your Payment Information',
    'zip_code'                         => 'ZIP Code',
    'zip_code_placeholder'             => 'Enter your ZIP code.',
    'order_confirmation'               => 'Order Confirmation',
    'order_summary'                    => 'Summary',
    'download_pdf'                     => 'Download PDF',
    'campaign_details'                 => 'Campaign details',
    'video_commercial'                 => 'Video Commercial',
    'total_budget'                     => 'Total budget',
    'unit_cost'                        => 'Unit Cost',
    'cost_method'                      => 'Cost Method',
    'billing_period'                   => 'Billing Period',
    'transaction_id'                   => 'Transaction ID',
    'transaction_date'                 => 'Transaction Date',
    'status'                           => 'Status',
    'payment_amount'                   => 'Payment Amount',
    'paid_amount'                      => 'Paid Amount',
    'amount_due'                       => 'Amount Due',
];
