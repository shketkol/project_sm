<?php

return [
    'advertiser' => [
        'transaction_is_made'                            => 'A transaction has been processed for the :campaign_name campaign and your payment method “:payment_method” has been charged.',
        'transaction_is_failed'                          => 'A Transaction Has Been Failed for :campaign_name.',
        'card_cannot_be_deleted'                         => 'Credit Card cannot be deleted',
        'card_cannot_be_deleted_due_to_active_campaigns' => 'You cannot delete your credit card because one of your campaigns is currently running or is ready to go live. You can, however, replace it with a new one.',
    ],
];
