<?php

return [
    'cannot_be_obtained'             => 'Payment Method cannot be obtained',
    'card_was_declined'              => [
        'title'   => 'This credit card was declined.',
        'message' => 'Please update the card information, or replace the card and retry payment. If the problem persists, contact your card issuer and try again.',
    ],
    'delete_card'                    => 'Delete Credit Card',
    'delete_card_warning'            => 'Do you want to delete this credit card?',
    'delete_card_notice'             => 'To avoid interruptions with your campaigns, make sure to add another credit card.',
    'payment_method_created'         => 'You\'ve updated your payment information.',
    'payment_method_declined'        => [
        'title'   => 'This payment method was declined.',
        'message' => 'Please update the payment information or replace the card and retry payment. If the problem persists, contact your card issuer and try again.',
    ],
    'payment_method_deleted'         => 'You\'ve successfully deleted your payment information.',
    'payment_method_replaced'        => 'You\'ve replaced the selected payment method.',
    'payment_processed_successfully' => 'No further action is needed.',
    'no_activity_yet'                => 'There are no transactions yet',
    'update_your_credit'             => 'Something\'s not right. Please review and update your credit information and try again.',
    'the_same_card_error'            => 'Sorry, but you are not able to add the same card',
    'charge_info'                    => [
        'current' => 'Your credit card will be charged on',
        'pending' => 'Your credit card will be charged on',
        'settled' => 'Credit card was charged on',
        'failed'  => 'A charge was attempted on',
    ],
    'validation'                     => [
        'cvv'        => 'Enter a valid security code.',
        'expiration' => 'Enter a valid expiration date.',
        'number'     => 'Enter a valid card number.',
        'address'    => [
            'required' => 'Enter the billing address.',
        ],
        'zip'        => [
            'required' => 'Enter a 5-digit ZIP code.',
            'max'      => 'Enter a 5-digit ZIP code.',
            'regex'    => 'Enter a 5-digit ZIP code.',
        ],
    ],
    'fail_store_card'                => 'We couldn’t add your payment method. Please use the Contact Us form to resolve the issue.'
];
