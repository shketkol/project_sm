<?php

return [
    'retry_payment'            => [
        'title'                      => 'Payment Method Declined',
        'message_single_campaign'    => 'We could not launch your "<a href=":href">:campaign</a>" campaign because the payment method on file was declined. Please resolve the issue as instructed in your Account Settings.',
        'message_multiple_campaigns' => 'We could not launch your campaigns because the payment method on file was declined. Please resolve the issue as instructed in your Account Settings.',
        'cta_title'                  => 'Account Settings',
    ],
    'retry_payment_processing' => [
        'title'   => 'Still Processing Your Payment',
        'message' => 'Please allow us some time to process your payment and update your account status. Please check back later.',
    ],
];
