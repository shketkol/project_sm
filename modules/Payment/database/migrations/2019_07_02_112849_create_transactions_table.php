<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('external_id')->nullable();
            $table->bigInteger('campaign_payment_id')->unsigned();
            $table->float('amount');
            $table->date('transaction_date');
            $table->date('start_period');
            $table->date('end_period');
            $table->bigInteger('status_id')->unsigned();
            $table->timestamps();
            $table->foreign('status_id')->references('id')->on('transaction_statuses')->onDelete('cascade');
            $table->foreign('campaign_payment_id')
                ->references('id')
                ->on('campaign_payments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign(['status_id', 'campaign_payment_id']);
        });
        Schema::dropIfExists('transactions');
    }
}
