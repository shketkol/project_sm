<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCompanyIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('payment_methods', 'company_id')) {
            Schema::table('payment_methods', function (Blueprint $table) {
                $table->dropForeign(['company_id']);
                $table->renameColumn('company_id', 'user_id');
                $table->foreign('user_id')->references('id')->on('users')->onDeleteCascade();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('payment_methods', 'user_id')) {
            Schema::table('payment_methods', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->renameColumn('user_id', 'company_id');
                $table->foreign('company_id')->references('id')->on('user_companies')->onDeleteCascade();
            });
        }
    }
}
