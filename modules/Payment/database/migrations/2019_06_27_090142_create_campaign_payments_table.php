<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('payment_method_id')->unsigned()->nullable();
            $table->bigInteger('campaign_id')->unsigned();
            $table->bigInteger('status_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('campaign_payments', function (Blueprint $table) {
            $table->foreign('status_id')
                ->references('id')->on('campaign_payment_statuses')->onDelete('cascade');
            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')->onDelete('set null');
            $table->foreign('campaign_id')
                ->references('id')->on('campaigns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_payments');
    }
}
