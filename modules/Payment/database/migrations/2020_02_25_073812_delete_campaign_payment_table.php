<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteCampaignPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('campaign_payments');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('campaign_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('payment_method_id')->unsigned()->nullable();
            $table->bigInteger('campaign_id')->unsigned();
            $table->bigInteger('status_id')->unsigned();
            $table->dateTime('last_activity')->nullable();
            $table->uuid('external_id')->nullable();
            $table->timestamps();

            $table->foreign('status_id')
                ->references('id')->on('campaign_payment_statuses')->onDelete('cascade');
            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')->onDelete('set null');
            $table->foreign('campaign_id')
                ->references('id')->on('campaigns')->onDelete('cascade');
        });
    }
}
