<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentMethodsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_methods', function (Blueprint $table) {
            $table->string('brand');
            $table->integer('exp_month');
            $table->bigInteger('user_id')->unsigned();
            $table->integer('exp_year');
            $table->string('last4');
            $table->string('token');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_methods', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::table('payment_methods', function (Blueprint $table) {
            $table->dropColumn(['brand', 'exp_month', 'company_id', 'exp_year', 'last4', 'token']);
        });
    }
}
