<?php

namespace Modules\Payment\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Payment\Policies\OrderPolicy;
use Modules\User\Models\Role;

class OrderPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADVERTISER      => [
           OrderPolicy::PERMISSION_VIEW_CAMPAIGN_ORDER,
           OrderPolicy::PERMISSION_LIST_CAMPAIGN_ORDER
        ],
        Role::ID_ADMIN => [
            OrderPolicy::PERMISSION_VIEW_CAMPAIGN_ORDER,
            OrderPolicy::PERMISSION_LIST_CAMPAIGN_ORDER
        ],
        Role::ID_ADMIN_READ_ONLY => [
            OrderPolicy::PERMISSION_VIEW_CAMPAIGN_ORDER,
            OrderPolicy::PERMISSION_LIST_CAMPAIGN_ORDER
        ],
    ];
}
