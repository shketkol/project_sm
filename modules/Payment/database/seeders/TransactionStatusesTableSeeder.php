<?php

namespace Modules\Payment\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Payment\Models\TransactionStatus;

class TransactionStatusesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $statuses = [
            [
                'id'   => TransactionStatus::ID_SUCCESS,
                'name' => TransactionStatus::SUCCESS,
            ],
            [
                'id'   => TransactionStatus::ID_CUSTOMER_FAILED,
                'name' => TransactionStatus::CUSTOMER_FAILED,
            ],
            [
                'id'   => TransactionStatus::ID_SYSTEM_FAILED,
                'name' => TransactionStatus::SYSTEM_FAILED,
            ],
            [
                'id'   => TransactionStatus::ID_INITIATED,
                'name' => TransactionStatus::INITIATED,
            ],
        ];

        foreach ($statuses as $status) {
            TransactionStatus::updateOrCreate(
                ['id' => Arr::get($status, 'id')],
                $status
            );
        }
    }
}
