<?php

namespace Modules\Payment\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Payment\Policies\PaymentPolicy;
use Modules\User\Models\Role;

class PaymentPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADVERTISER      => [
            PaymentPolicy::PERMISSION_LIST_PAYMENT_METHOD,
            PaymentPolicy::PERMISSION_VIEW_PAYMENT_METHOD,
            PaymentPolicy::PERMISSION_CREATE_PAYMENT_METHOD,
            PaymentPolicy::PERMISSION_DELETE_PAYMENT_METHOD,
            PaymentPolicy::PERMISSION_REPLACE_PAYMENT_METHOD,
            PaymentPolicy::PERMISSION_RETRY_PAYMENT,
        ],
    ];
}
