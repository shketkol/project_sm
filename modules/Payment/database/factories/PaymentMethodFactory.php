<?php

namespace Modules\Payment\Database\Factories;

use App\Factories\Factory;
use Modules\Payment\Models\PaymentMethod;

class PaymentMethodFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PaymentMethod::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'external_id' => $this->faker->uuid(),
        ];
    }
}
