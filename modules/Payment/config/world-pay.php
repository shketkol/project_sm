<?php

$wpUrl = env('WP_BASE_URL', 'https://try.access.worldpay.com');

return [
    'username'     => env('WP_USERNAME', 'o25jfcblap364yt7'),
    'password'     => env('WP_PASSWORD', '2s00xw0m09uvq961'),
    'entity'       => env('WP_ENTITY', 'DANADS'),
    'src'          => env('WP_SRC', $wpUrl . '/access-checkout/v1/checkout.js'),
    'currency'     => 'USD',
    'country_code' => 'US',
    'requests'     => [
        'card_on_file'            => [
            'url'              => $wpUrl . '/verifiedTokens/cardOnFile',
            'type'             => 'POST',
            'native_type'      => 'card/checkout',
            'namespace_prefix' => 'ACCOUNT_ID_',
            'headers'          => [
                'type'   => 'application/vnd.worldpay.verified-tokens-v1.hal+json',
                'accept' => 'application/vnd.worldpay.verified-tokens-v1.hal+json',
            ],
        ],
        'get_token'               => [
            'url'     => $wpUrl . '/tokens/',
            'type'    => 'GET',
            'headers' => [
                'type' => 'application/vnd.worldpay.verified-tokens-v1.hal+json',
            ],
        ],
        'validation'              => [
            'type'    => 'GET',
            'headers' => [
                'type'   => 'application/vnd.worldpay.verifications.accounts-v3+json',
                'accept' => 'application/vnd.worldpay.verifications.accounts-v3+json',
            ],
        ],
        'resolve_token_conflicts' => [
            'type'    => 'PUT',
            'headers' => [
                'type' => 'application/vnd.worldpay.verified-tokens-v1.hal+json',
            ],
        ],
    ],
    'front_end'    => [
        'checkout_id'     => env('WP_CHECKOUT_ID', '56767217-f686-4efe-be4e-2e703d821ca3'),
        'selectors'       => [
            'card'   => 'card-number',
            'cvv'    => 'card-cvv',
            'expiry' => 'card-expiry',
        ],
        'styles'          => [
            'input'              => [
                'font-size'   => '14px',
                'color'       => '#2c313a',
                'font-weight' => 400,
            ],
            'input::placeholder' => [
                'font-weight' => 400,
                'color'       => '#586174',
            ],
            'input::-moz-placeholder' => [
                'font-weight' => 400,
                'color'       => '#586174'
            ],
        ],
        'externalClasses' => [
            'valid'   => 'is-valid',
            'invalid' => 'is-invalid',
            'onfocus' => 'is-onfocus',
        ],
    ],
];
