<?php

use Modules\Payment\Models\Transaction;
use Modules\Payment\Models\TransactionStatus;

return [
    /**
     * Transaction workflow graph (USE ONLY FOR EXTERNAL STATUS MAPPING)
     */
    'transaction' => [
        // class of your domain object
        'class'         => Transaction::class,

        // name of the graph (default is "default")
        'graph'         => 'transaction',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status_id',

        // list of all possible states
        'states'        => [
            [
                'name' => TransactionStatus::ID_SUCCESS,
            ],
            [
                'name' => TransactionStatus::ID_SYSTEM_FAILED,
            ],
            [
                'name' => TransactionStatus::ID_CUSTOMER_FAILED,
            ],
            [
                'name' => TransactionStatus::ID_INITIATED,
            ],
        ],

        // list of all possible transitions
        'transitions'   => [
            TransactionStatus::SUCCESS => [
                'from' => [TransactionStatus::ID_SUCCESS],
                'to'   => TransactionStatus::ID_SUCCESS,
            ],
            TransactionStatus::SYSTEM_FAILED => [
                'from' => [TransactionStatus::ID_SYSTEM_FAILED],
                'to'   => TransactionStatus::ID_SYSTEM_FAILED,
            ],
            TransactionStatus::CUSTOMER_FAILED => [
                'from' => [TransactionStatus::ID_CUSTOMER_FAILED],
                'to'   => TransactionStatus::ID_CUSTOMER_FAILED,
            ],
            TransactionStatus::INITIATED => [
                'from' => [TransactionStatus::ID_INITIATED],
                'to'   => TransactionStatus::ID_INITIATED,
            ],
        ],
    ],
];
