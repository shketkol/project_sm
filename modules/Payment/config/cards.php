<?php

return [
    'icons' => [
        'svg' => [
            'visa'             => 'Visa.svg',
            'others'           => 'Mastercard.svg',
            'mastercard'       => 'Mastercard.svg',
            'discover'         => 'Discover.svg',
            'american_express' => 'American-Express.svg',
        ],
        'png' => [
            'visa'             => 'Visa.png',
            'others'           => 'Mastercard.png',
            'mastercard'       => 'Mastercard.png',
            'discover'         => 'Discover.png',
            'american_express' => 'American-Express.png',
        ],
    ],
];
