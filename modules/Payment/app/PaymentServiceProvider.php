<?php

namespace Modules\Payment;

use App\Providers\ModuleServiceProvider;
use Modules\Payment\Commands\TransactionDropCommand;
use Modules\Payment\Policies\OrderPolicy;
use Modules\Payment\Policies\PaymentPolicy;

class PaymentServiceProvider extends ModuleServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [];

    /**
     * Get module prefix
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'payment';
    }

    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'payment' => PaymentPolicy::class,
        'order'   => OrderPolicy::class,
    ];

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        TransactionDropCommand::class,
    ];

    /**
     * Boot provider.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadRoutes();
        $this->loadConfigs(['world-pay', 'state-machine', 'cards']);
        $this->commands($this->commands);
    }
}
