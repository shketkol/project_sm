<?php

namespace Modules\Payment\Policies;

use App\Policies\Policy;
use App\Policies\Traits\Checks;
use Modules\Campaign\Exceptions\CampaignNotFoundException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Payment\Exceptions\OrderNotFoundException;
use Modules\User\Models\User;

class OrderPolicy extends Policy
{
    use Checks;

    public const PERMISSION_LIST_CAMPAIGN_ORDER = 'list_campaign_order';
    public const PERMISSION_VIEW_CAMPAIGN_ORDER = 'view_campaign_order';

    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @param CampaignRepository $campaignRepository
     * @return void
     */
    public function __construct(CampaignRepository $campaignRepository)
    {
        $this->repository = $campaignRepository;
    }

    /**
     * Returns true if user can view list of all orders.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_CAMPAIGN_ORDER);
    }

    /**
     * Returns true if user can view an order.
     *
     * @param User $user
     * @param string $orderId
     * @return bool
     * @throws \Modules\Payment\Exceptions\OrderNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function view(User $user, string $orderId): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_VIEW_CAMPAIGN_ORDER)) {
            return false;
        }

        try {
            $campaign = $this->repository->findCampaignByOrderId($orderId);

            if (!$this->isOwner($user, $campaign) && !$this->isAdmin($user)) {
                return false;
            }
        } catch (CampaignNotFoundException $exception) {
            throw OrderNotFoundException::create($orderId);
        }

        return true;
    }

    /**
     * @param User $user
     * @param Campaign $campaign
     * @return bool
     */
    private function isOwner(User $user, Campaign $campaign): bool
    {
        return $campaign->user_id === $user->id;
    }
}
