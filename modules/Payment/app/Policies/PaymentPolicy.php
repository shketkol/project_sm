<?php

namespace Modules\Payment\Policies;

use App\Policies\Policy;
use Illuminate\Database\Eloquent\Collection;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Payment\Models\PaymentMethod;
use Modules\User\Models\User;

class PaymentPolicy extends Policy
{
    /**
     * Model class.
     *
     * @var string
     */
    protected $model = PaymentMethod::class;

    /**
     * Permissions.
     */
    public const PERMISSION_CREATE_PAYMENT_METHOD  = 'create_payment_method';
    public const PERMISSION_DELETE_PAYMENT_METHOD  = 'delete_payment_method';
    public const PERMISSION_REPLACE_PAYMENT_METHOD = 'replace_payment_method';
    public const PERMISSION_LIST_PAYMENT_METHOD    = 'list_payment_method';
    public const PERMISSION_VIEW_PAYMENT_METHOD    = 'view_payment_method';
    public const PERMISSION_RETRY_PAYMENT          = 'retry_payment';

    /**
     * Returns true if user can view list of all payment methods.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_PAYMENT_METHOD);
    }

    /**
     * Returns true if user can create a new payment method.
     *
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CREATE_PAYMENT_METHOD) &&
            !($user->isPendingManualReview() || $user->isCreateInProgress()) &&
            !$user->paymentMethods()->count();
    }

    /**
     * Returns true if user can view a payment method.
     *
     * @param User $user
     * @param PaymentMethod $paymentMethod
     * @return bool
     */
    public function view(User $user, PaymentMethod $paymentMethod): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_VIEW_PAYMENT_METHOD)) {
            return false;
        }

        if (!$this->isOwner($user, $paymentMethod)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can delete payment method.
     *
     * @param User $user
     * @param PaymentMethod $paymentMethod
     * @return bool
     */
    public function delete(User $user, PaymentMethod $paymentMethod): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_DELETE_PAYMENT_METHOD)) {
            return false;
        }

        if (!$this->isOwner($user, $paymentMethod)) {
            return false;
        }

        if ($this->hasOngoingCampaigns($user)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can replace payment method.
     *
     * @param User $user
     * @return bool
     */
    public function replace(User $user): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_REPLACE_PAYMENT_METHOD)) {
            return false;
        }

        if (!$user->paymentMethods->first()) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can send retry-payment request.
     *
     * @param User $user
     * @return bool
     */
    public function retry(User $user, PaymentMethod $paymentMethod): bool
    {
        if (!$this->isOwner($user, $paymentMethod)) {
            return false;
        }

        if (!$user->hasPermissionTo(self::PERMISSION_RETRY_PAYMENT)) {
            return false;
        }

        if (!$user->canRetryPayment()) {
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @param PaymentMethod $paymentMethod
     * @return bool
     */
    private function isOwner(User $user, PaymentMethod $paymentMethod): bool
    {
        return $paymentMethod->user_id === $user->id;
    }

    /**
     * Check if user has ongoing campaigns.
     *
     * @param User $user
     *
     * @return bool
     */
    private function hasOngoingCampaigns(User $user): bool
    {
        /** @var CampaignRepository $campaignRepository */
        $campaignRepository = app(CampaignRepository::class);

        /** @var Collection $ongoingCampaigns */
        $ongoingCampaigns = $campaignRepository->findOngoingCampaignsByUser($user);
        return $ongoingCampaigns->isNotEmpty();
    }
}
