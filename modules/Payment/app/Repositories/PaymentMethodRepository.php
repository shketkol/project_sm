<?php

namespace Modules\Payment\Repositories;

use App\Repositories\Repository;
use Modules\Payment\Models\PaymentMethod;

/**
 * Class PaymentMethodRepository
 *
 * @package Modules\Payment\Repositories
 */
class PaymentMethodRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PaymentMethod::class;
    }
}
