<?php

namespace Modules\Payment\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Modules\Payment\Models\TransactionStatus;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class TransactionProcessedCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private $transactionId;

    /**
     * @param string $transactionId
     */
    public function __construct(string $transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model
            ->select('*')
            ->where('external_id', "=", $this->transactionId)
            ->whereNotIn('status_id', TransactionStatus::FAILED_STATUS_IDS);
    }
}
