<?php

namespace Modules\Payment\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class OrderIdCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private $orderId;

    public function __construct(string $orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model
            ->select('campaign_payments.*')
            ->leftJoin('campaigns', 'campaign_payments.campaign_id', '=', 'campaigns.id')
            ->where('campaigns.order_id', $this->orderId);
    }
}
