<?php

namespace Modules\Payment\Repositories;

use App\Repositories\Repository;
use Modules\Payment\Models\Transaction;
use Modules\Payment\Repositories\Criteria\TransactionProcessedCriteria;

/**
 * Class TransactionRepository
 *
 * @package Modules\Payment\Repositories
 */
class TransactionRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Transaction::class;
    }

    /**
     * @param integer $lastTransactionId
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function isTransactionProcessed(int $lastTransactionId): bool
    {
        $sentNotificationCount = $this->pushCriteria(new TransactionProcessedCriteria($lastTransactionId));

        $isProcessed = $sentNotificationCount->count() > 0;

        $this->reset();

        return $isProcessed;
    }
}
