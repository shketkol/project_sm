<?php

namespace Modules\Payment\Models;

use Illuminate\Support\Str;

class OrderStatus
{
    /**
     * Campaign payment before first transactions
     */
    public const PENDING = 'pending';

    /**
     * At least one transaction have completed successfully
     */
    public const CURRENT = 'current';

    /**
     * Last transaction failed
     */
    public const FAILED = 'failed';

    /**
     * Campaign is complete. Last transaction finished successfully
     */
    public const SETTLED = 'settled';

    /**
     * Order SUCCESS states ids
     */
    public const SUCCESS_STATUS_NAMES = [
        self::CURRENT,
        self::SETTLED
    ];

    /**
     * Payment statuses (initially used for transactions history data table filter)
     */
    public const STATUSES = [
        self::PENDING => self::PENDING,
        self::CURRENT => self::CURRENT,
        self::FAILED  => self::FAILED,
        self::SETTLED => self::SETTLED,
    ];

    /**
     * @param string|null $status
     * @return bool
     */
    public static function checkSuccessByHaapiTransition(?string $status): bool
    {
        $statusName = config('daapi.status_mapping.campaign_payment.' . Str::lower($status));
        return $status && in_array($statusName, self::SUCCESS_STATUS_NAMES);
    }

    /**
     * Get lowercased HAAPI order status.
     *
     * @param $status
     *
     * @return false|string
     */
    public static function getHaapiStatusName(string $status)
    {
        return array_search($status, config('daapi.status_mapping.campaign_payment'));
    }
}
