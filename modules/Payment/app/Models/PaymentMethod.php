<?php

namespace Modules\Payment\Models;

use Carbon\Carbon;
use App\Traits\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Payment\Models\Traits\HasCampaigns;
use Modules\User\Models\Traits\BelongsToUser;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string  $uuid
 * @property string  $external_id
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 */
class PaymentMethod extends Model
{
    use BelongsToUser,
        HasCampaigns,
        HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'external_id',
    ];

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->external_id;
    }
}
