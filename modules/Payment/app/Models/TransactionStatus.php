<?php

namespace Modules\Payment\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Payment\Models\Traits\HasTransactions;

/**
 * @property integer $id
 * @property string $name
 */
class TransactionStatus extends Model
{
    use HasTransactions;

    /**
     * A request for payment for this billing period has been processed successfully.
     */
    public const SUCCESS = 'success';

    /**
     * A request for payment for this billing period has failed because of a system error.
     */
    public const SYSTEM_FAILED = 'system_failed';

    /**
     * A request for payment for this billing period has not been processed
     * because of an issue with the advertiser’s payment method
     */
    public const CUSTOMER_FAILED = 'customer_failed';

    /**
     * A request for payment for this billing period has been sent. The campaign is active.
     */
    public const INITIATED = 'initiated';

    public const ID_SUCCESS         = 1;
    public const ID_SYSTEM_FAILED   = 2;
    public const ID_CUSTOMER_FAILED = 3;
    public const ID_INITIATED       = 4;

    /**
     * Transaction FAILED states ids
     */
    public const FAILED_STATUS_IDS = [
        TransactionStatus::ID_CUSTOMER_FAILED,
        TransactionStatus::ID_SYSTEM_FAILED
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @param string|null $status
     * @return bool
     */
    public static function checkFailedByHaapiTransition(?string $status): bool
    {
        $statusId = Transaction::getStatusIdByTransactionFlow($status, 'transaction');
        return $status && in_array($statusId, self::FAILED_STATUS_IDS);
    }
}
