<?php

namespace Modules\Payment\Models;

use App\Traits\State;
use Illuminate\Database\Eloquent\Model;
use Modules\Payment\Models\Traits\BelongsToTransactionStatus;

/**
 * @property integer $external_id
 * @property integer $submitted
 * @property integer $status_id
 */
class Transaction extends Model
{
    use BelongsToTransactionStatus, State;

    /**
     * @var array
     */
    protected $fillable = [
        'external_id',
        'status_id',
    ];

    /**
     * Campaign state's flow.
     *
     * @var string
     */
    protected $flow = 'transaction';
}
