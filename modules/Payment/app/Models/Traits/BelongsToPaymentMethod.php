<?php

namespace Modules\Payment\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Payment\Models\PaymentMethod;

/**
 * @property PaymentMethod $paymentMethod
 */
trait BelongsToPaymentMethod
{
    /**
     * @return BelongsTo
     */
    public function paymentMethod(): BelongsTo
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id');
    }
}
