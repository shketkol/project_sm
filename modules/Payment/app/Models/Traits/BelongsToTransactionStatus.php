<?php

namespace Modules\Payment\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Payment\Models\TransactionStatus;

/**
 * Trait BelongsToTransactionStatus
 *
 * @package Modules\Campaign\Models\Traits
 */
trait BelongsToTransactionStatus
{
    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(TransactionStatus::class, 'status_id');
    }
}
