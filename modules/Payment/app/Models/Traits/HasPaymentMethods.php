<?php

namespace Modules\Payment\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Payment\Models\PaymentMethod;

/**
 * @property PaymentMethod[]|\Illuminate\Database\Eloquent\Collection $paymentMethods
 */
trait HasPaymentMethods
{
    /**
     * @return HasMany
     */
    public function paymentMethods(): HasMany
    {
        return $this->hasMany(PaymentMethod::class, 'user_id');
    }
}
