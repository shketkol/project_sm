<?php

namespace Modules\Payment\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Campaign\Models\Campaign;

trait HasCampaigns
{
    /**
     * @return HasMany
     */
    public function campaigns(): HasMany
    {
        return $this->hasMany(Campaign::class, 'payment_method_id');
    }
}
