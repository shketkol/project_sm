<?php

namespace Modules\Payment\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Payment\Models\Transaction;

/**
 * @property Transaction[] $transaction
 */
trait HasTransactions
{
    /**
     * @return HasMany
     */
    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class, 'status_id');
    }
}
