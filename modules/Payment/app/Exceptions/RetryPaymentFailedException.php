<?php

namespace Modules\Payment\Exceptions;

use App\Exceptions\BaseException;

class RetryPaymentFailedException extends BaseException
{
    //
}
