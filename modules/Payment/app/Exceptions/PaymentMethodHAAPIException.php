<?php

namespace Modules\Payment\Exceptions;

use App\Exceptions\ModelNotCreatedException;

class PaymentMethodHAAPIException extends ModelNotCreatedException
{
    //
}
