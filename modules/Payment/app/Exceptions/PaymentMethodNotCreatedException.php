<?php

namespace Modules\Payment\Exceptions;

use App\Exceptions\ModelNotCreatedException;

class PaymentMethodNotCreatedException extends ModelNotCreatedException
{
    //
}
