<?php

namespace Modules\Payment\Exceptions;

use App\Exceptions\ModelNotUpdatedException;

class PaymentMethodNotDeletedException extends ModelNotUpdatedException
{
    //
}
