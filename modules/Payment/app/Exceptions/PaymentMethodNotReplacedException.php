<?php

namespace Modules\Payment\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class PaymentMethodNotReplacedException extends BaseException
{
    /**
     * Default Status code used for improved logging.
     * This exception being logged as WARNING.
     * See BaseExceptions and LoggerService for details.
     */
    public const STATUS_CODE = 422;

    /**
     * Report the exception.
     *
     * @return JsonResponse
     */
    public function render()
    {
        return response()->json([
            'message' => $this->getMessage(),
            'errors' => $this->collectErrors(),
        ], $this->getCode() ?: static::STATUS_CODE);
    }

    private function collectErrors(): array
    {
        $previous = $this->getPrevious();

        if (!$previous || !$previous instanceof ValidationException) {
            return [];
        }

        return $previous->errors();
    }
}
