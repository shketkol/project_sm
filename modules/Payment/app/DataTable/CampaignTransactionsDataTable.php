<?php

namespace Modules\Payment\DataTable;

use App\DataTable\ExternalDataTable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository;
use Modules\Campaign\DataTable\Transformers\CampaignsTransformer;
use Modules\Haapi\Actions\Payment\OrderList;
use Modules\Haapi\DataTransferObjects\Payment\OrderListParams;
use Modules\Payment\DataTable\Filter\OrderStatusFilter;

class CampaignTransactionsDataTable extends ExternalDataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'campaign-transactions';

    /**
     * @var array
     */
    public $columnMapper = [
        'updatedOn'     => 'last_activity',
        'paymentStatus' => 'payment_status',
        'campaignName'  => 'name',
        'orderId'       => 'order_id',
    ];

    /**
     * Transformer class
     *
     * @var string
     */
    protected $transformer = CampaignsTransformer::class;

    /**
     * Repository
     *
     * @var string
     */
    protected $repository = CampaignsDataTableRepository::class;

    /**
     * @var string
     */
    protected $externalAction = OrderList::class;

    /**
     * @var string
     */
    protected $payloadKey = 'orders';

    /**
     * Available cards
     *
     * @var array
     */
    protected $cards = [];

    /**
     * Add aliases according model relation.
     * Required for search
     *
     * @var array
     */
    protected $relationAliases = [
        'payment_status' => 'campaign_payment_statuses.name',
        'last_activity'  => 'campaign_payments.last_activity',
    ];

    /**
     * Filters
     *
     * @var array
     */
    protected $filters = [
        OrderStatusFilter::class,
    ];

    /**
     * @return OrderListParams
     */
    protected function prepareParams()
    {
        $filters = $this->getFiltersValue();
        return new OrderListParams([
            'accountId'         => $this->getAdvertiser()->getExternalId(),
            'start'             => (int)$this->request->start,
            'limit'             => (int)$this->request->length,
            'searchText'        => Arr::get($this->request->search, 'value'),
            'sortOrder'         => $this->getExternalSortDirection(),
            'sortFieldName'     => $this->getExternalSortColumn(),
            'orderStatusFilter' => $filters ? Str::upper(Arr::get($filters, 'payment_status')) : null,
        ]);
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('order.list');
    }
}
