<?php

namespace Modules\Payment\DataTable\Filter;

use App\DataTable\Filters\DataTableFilter;
use Illuminate\Database\Eloquent\Builder;
use Modules\Payment\Models\OrderStatus;
use Yajra\DataTables\DataTableAbstract;

class OrderStatusFilter extends DataTableFilter
{
    /**
     * Filter name
     *
     * @var string
     */
    public static $name = 'campaign-payment-status';

    /**
     * @param \Yajra\DataTables\DataTableAbstract $dataTable
     */
    public function filter(DataTableAbstract $dataTable): void
    {
        $this->makeFilter($dataTable, 'campaign_payment_statuses.name', function (Builder $query, $values) {
            /** @see \Modules\Payment\DataTable\CampaignTransactionsDataTable::prepareParams() 'orderStatusFilter' */
        });
    }

    /**
     * Get filter options
     *
     * @return \Illuminate\Support\Collection
     */
    public function options()
    {
        return collect(OrderStatus::STATUSES)
            ->map(function ($item) {
                return trans("payment::labels.statuses.$item");
            });
    }
}
