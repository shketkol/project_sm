<?php

namespace Modules\Payment\Mail\Advertiser;

use App\Mail\Mail;

class TransactionIsMade extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('payment::emails.advertiser.transaction_is_made.subject'))
            ->view('payment::emails.advertiser.transaction-is-made')
            ->with($this->payload);
    }
}
