<?php

namespace Modules\Payment\Commands;

use Illuminate\Console\Command;
use Modules\User\Models\User;

/**
 * Class TransactionMakeCommand
 * @package Modules\Payment\Commands
 */
class TransactionDropCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drop:transaction {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drops all transaction for user campaigns';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        $user = User::findOrFail($this->argument('user_id'));
        $campaigns = $user->campaigns();

        foreach ($campaigns as $campaign) {
            $campaign->paymentMethod()->delete();
        }
        $this->info('Done.');
    }
}
