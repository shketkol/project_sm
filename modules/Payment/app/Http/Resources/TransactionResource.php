<?php

namespace Modules\Payment\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Payment\Models\Transaction;

/**
 * @mixin Transaction
 */
class TransactionResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'               => $this->getKey(),
            'amount'           => $this->amount,
            'transaction_date' => $this->transaction_date,
            'start_period'     => $this->start_period,
            'end_period'       => $this->end_period,
            'status'           => $this->status->name,
        ];
    }
}
