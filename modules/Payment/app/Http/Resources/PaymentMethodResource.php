<?php

namespace Modules\Payment\Http\Resources;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Payment\Models\PaymentMethod;

/**
 * @mixin PaymentMethod
 */
class PaymentMethodResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var \Modules\User\Models\User $user */
        $user = app(Guard::class)->user();

        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = Arr::get($this->resource, 'paymentMethod');

        return [
            'id'                  => $paymentMethod->id,
            'name'                => Arr::get($this->resource, 'cardHolderName'),
            'brand'               => mb_strtolower(str_replace(' ', '_', Arr::get($this->resource, 'brand'))),
            'exp_month'           => Arr::get($this->resource, 'expMonth'),
            'exp_year'            => Arr::get($this->resource, 'expYear'),
            'last4'               => Arr::get($this->resource, 'lastFour'),
            'zip'                 => Arr::get($this->resource, 'billingAddress.postalCode'),
            'address'             => Arr::get($this->resource, 'billingAddress.address1'),
            'city'                => Arr::get($this->resource, 'billingAddress.city'),
            'need_update'         => $user->needPaymentUpdate(),
            'can_retry'           => $user->canRetryPayment(),
            'is_retry_processing' => $user->isRetryPaymentProcessing(),
            'permissions'         => [
                'delete_payment' => $user->can('payment.delete', $paymentMethod),
                'retry_payment'  => $user->can('payment.retry', $paymentMethod),
            ],
        ];
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function with($request): array
    {
        return self::getAdditionalData();
    }

    /**
     * Get additional data that should be added to resource.
     *
     * @return array
     */
    public static function getAdditionalData()
    {
        /** @var \Modules\User\Models\User $user */
        $user = app(Guard::class)->user();

        /** @var CampaignRepository $campaignRepository */
        $campaignRepository = app(CampaignRepository::class);

        /** @var Collection $ongoingCampaigns */
        $ongoingCampaigns = $campaignRepository->findOngoingCampaignsByUser($user);

        return [
            'permissions' => [
                'create_payment' => $user->can('payment.create'),
                'delete_payment' => [
                    'has_ongoing_campaigns' => $ongoingCampaigns->isNotEmpty(),
                ],
            ],
        ];
    }
}
