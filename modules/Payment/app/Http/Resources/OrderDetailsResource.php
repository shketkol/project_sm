<?php

namespace Modules\Payment\Http\Resources;

use App\Http\Resources\Resource;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class OrderDetailsResource extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $paymentMethod = Arr::get($this->resource, 'paymentMethod');

        return [
            'orderId'                 => Arr::get($this->resource, 'orderId'),
            'orderStatus'             => strtolower(Arr::get($this->resource, 'orderStatus')),
            'campaign'                => Arr::get($this->resource, 'campaign'),
            'transactions'            => $this->formatInvoices(),
            'billingMessage'          => Arr::get($this->resource, 'billingMessage'),
            'dueAmount'               => Arr::get($this->resource, 'amountDue'),
            'budget'                  => Arr::get($this->resource, 'campaign.displayBudget')
                                         ?: round(Arr::get($this->resource, 'campaign.budget'), 2),
            'displayDiscountedBudget' => Arr::get($this->resource, 'campaign.promoInfo.displayDiscountedBudget'),
            'promocode'               => Arr::get($this->resource, 'campaign.promoInfo.name'),
            'paidAmount'              => Arr::get($this->resource, 'totalPaidAmount'),
        ];
    }

    /**
     * @return array
     */
    protected function formatInvoices(): array
    {
        $result = [];
        foreach (Arr::get($this->resource, 'invoices') as $invoice) {
            $paymentHistory = Arr::get($invoice, 'paymentHistory');
            $paymentHistory = array_map(function ($payment) {
                Arr::set(
                    $payment,
                    'status',
                    config('daapi.status_mapping.transaction.' . Str::lower(Arr::get($payment, 'status')))
                );
                Arr::set(
                    $payment,
                    'card.brand',
                    mb_strtolower(str_replace(' ', '_', Arr::get($payment, 'card.brand'))),
                );

                return $payment;
            }, $paymentHistory);
            $result = array_merge($result, $paymentHistory);
        }

        $result = array_values(Arr::sort($result, function ($value) {
            return $value['transactionDate'];
        }));

        return $result;
    }
}
