<?php

namespace Modules\Payment\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

/**
 * Class StorePaymentMethodRequest
 *
 * @package Modules\Campaign\Http\Requests
 */
class StorePaymentMethodRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'name' => $validationRules->only(
                'payment.name',
                ['required', 'string', 'max']
            ),
            'session' => $validationRules->only(
                'payment.session',
                ['required', 'url', 'max']
            ),
            'zip'     => $validationRules->only(
                'user.address.zip',
                ['required', 'string', 'max', 'regex']
            ),
            'address' => $validationRules->only(
                'user.address.line1',
                ['required', 'string', 'max']
            ),
            'city' => $validationRules->only(
                'user.address.city',
                ['required', 'string', 'max']
            )
        ];
    }
}
