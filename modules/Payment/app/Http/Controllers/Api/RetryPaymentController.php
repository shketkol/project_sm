<?php

namespace Modules\Payment\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Haapi\Actions\Payment\PaymentMethodGet;
use Modules\Payment\Actions\RetryPaymentAction;
use Modules\Payment\Http\Resources\PaymentMethodResource;
use Modules\Payment\Models\PaymentMethod;

/**
 * Class RetryPaymentController
 *
 * @package Modules\Payment\Http\Controllers
 */
class RetryPaymentController extends Controller
{
    public function __invoke(
        PaymentMethod $paymentMethod,
        PaymentMethodGet $paymentMethodGet,
        Authenticatable $user,
        RetryPaymentAction $retryPaymentAction
    ): PaymentMethodResource {
        $retryPaymentAction->handle($user);
        $cardInfo = $paymentMethodGet->handle(
            $paymentMethod->getExternalId(),
            $user->getKey()
        );
        $cardInfo['paymentMethod'] = $paymentMethod;
        return (new PaymentMethodResource($cardInfo));
    }
}
