<?php

namespace Modules\Payment\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Response;
use Modules\Campaign\Exceptions\CampaignNotFoundException;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Payment\Actions\DownloadPaymentPdfAction;
use Prettus\Repository\Exceptions\RepositoryException;
use Psr\SimpleCache\InvalidArgumentException;

class DownloadOrderPdfController extends Controller
{
    /**
     * @param string $orderId
     * @param DownloadPaymentPdfAction $action
     * @param CampaignRepository $campaignRepository
     * @return Response
     * @throws GuzzleException
     * @throws CampaignNotFoundException
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws UnauthorizedException
     * @throws RepositoryException
     * @throws InvalidArgumentException
     * @throws \Modules\Payment\Exceptions\OrderNotFoundException
     */
    public function __invoke(
        string $orderId,
        DownloadPaymentPdfAction $action,
        CampaignRepository $campaignRepository
    ): Response {
        $campaign = $campaignRepository->findCampaignByOrderId($orderId);
        return $action->handle($campaign);
    }
}
