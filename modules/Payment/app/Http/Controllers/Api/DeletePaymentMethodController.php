<?php

namespace Modules\Payment\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Modules\Payment\Actions\DeletePaymentMethodsAction;
use Modules\Payment\Models\PaymentMethod;

/**
 * Class DeletePaymentMethodController
 *
 * @package Modules\Payment\Http\Controllers
 */
class DeletePaymentMethodController extends Controller
{
    /**
     * Delete payment method
     *
     * @param PaymentMethod $paymentMethod
     * @param DeletePaymentMethodsAction $action
     * @param Authenticatable $user
     * @return AnonymousResourceCollection
     * @throws \Modules\Payment\Exceptions\PaymentMethodNotDeletedException
     */
    public function __invoke(
        PaymentMethod $paymentMethod,
        DeletePaymentMethodsAction $action,
        Authenticatable $user
    ): Response {
        $action->handle($paymentMethod);
        return response(['data' => ['message' => trans('payment::messages.payment_method_deleted')]]);
    }
}
