<?php

namespace Modules\Payment\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodGet;
use Modules\Payment\Http\Resources\PaymentMethodResource;
use Modules\Payment\Models\PaymentMethod;

/**
 * Class ShowPaymentMethodController
 *
 * @package Modules\Payment\Http\Controllers
 */
class ShowPaymentMethodController extends Controller
{
    /**
     * Show payment method
     *
     * @param PaymentMethod $paymentMethod
     * @param PaymentMethodGet $action
     * @param Authenticatable $user
     * @return PaymentMethodResource
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __invoke(
        PaymentMethod $paymentMethod,
        PaymentMethodGet $action,
        Authenticatable $user
    ): PaymentMethodResource {
        $cardInfo = $action->handle($paymentMethod->getExternalId(), $user->getKey());
        $cardInfo['paymentMethod'] = $paymentMethod;
        return (new PaymentMethodResource($cardInfo));
    }
}
