<?php

namespace Modules\Payment\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodGet;
use Modules\Payment\Actions\ReplacePaymentMethodsAction;
use Modules\Payment\Exceptions\PaymentMethodHAAPIException;
use Modules\Payment\Http\Resources\PaymentMethodResource;

class ReplacePaymentMethodController extends Controller
{
    /**
     * Replace payment method.
     *
     * @param PaymentMethodGet $paymentMethodGet
     * @param ReplacePaymentMethodsAction $action
     *
     * @param Authenticatable $user
     * @return JsonResponse|PaymentMethodResource
     */
    public function __invoke(
        PaymentMethodGet $paymentMethodGet,
        ReplacePaymentMethodsAction $action,
        Authenticatable $user
    ) {
        try {
            $paymentMethod             = $user->paymentMethods->first();
            $newPaymentMethod          = $action->handle($paymentMethod, $user);
            $cardInfo                  = $paymentMethodGet->handle(
                $newPaymentMethod->getExternalId(),
                $newPaymentMethod->user->getKey()
            );
            $cardInfo['paymentMethod'] = $newPaymentMethod;
            return (new PaymentMethodResource($cardInfo))->additional([
                'data' => [
                    'message' => trans('payment::messages.payment_method_replaced'),
                ],
            ]);
        } catch (\Throwable $exception) {
            return response()->json([
                'message' => trans('payment::messages.fail_store_card')
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
