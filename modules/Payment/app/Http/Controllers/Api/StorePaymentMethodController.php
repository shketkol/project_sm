<?php

namespace Modules\Payment\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodGet;
use Modules\Payment\Actions\StorePaymentMethodsAction;
use Modules\Payment\Exceptions\PaymentMethodHAAPIException;
use Modules\Payment\Http\Resources\PaymentMethodResource;

/**
 * Class StorePaymentMethodController
 *
 * @package Modules\Payment\Http\Controllers
 */
class StorePaymentMethodController extends Controller
{
    /**
     * @param StorePaymentMethodsAction $action
     * @param PaymentMethodGet $paymentMethodGet
     * @return JsonResponse|PaymentMethodResource
     * @throws \Throwable
     */
    public function __invoke(
        StorePaymentMethodsAction $action,
        PaymentMethodGet $paymentMethodGet
    ) {
        try {
            $paymentMethod = $action->handle();
            $cardInfo = $paymentMethodGet->handle($paymentMethod->getExternalId(), $paymentMethod->user->getKey());
            $cardInfo['paymentMethod'] = $paymentMethod;

            return (new PaymentMethodResource($cardInfo))->additional([
                'data' => [
                    'message' => trans('payment::messages.payment_method_created'),
                ],
            ]);
        } catch (\Throwable $exception) {
            return response()->json([
                'message' => trans('payment::messages.fail_store_card')
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
