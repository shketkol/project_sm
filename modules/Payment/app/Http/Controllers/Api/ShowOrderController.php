<?php

namespace Modules\Payment\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Arr;
use Modules\Campaign\Exceptions\CampaignNotFoundException;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Haapi\Actions\Payment\OrderGet;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetParams;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\Payment\Http\Resources\OrderDetailsResource;
use Prettus\Repository\Exceptions\RepositoryException;

class ShowOrderController extends Controller
{
    /**
     * @param string $orderId
     * @param OrderGet $orderGet
     * @param Authenticatable $user
     * @param CampaignRepository $campaignRepository
     * @return OrderDetailsResource
     * @throws CampaignNotFoundException
     * @throws ConflictException
     * @throws ForbiddenException
     * @throws GuzzleException
     * @throws HaapiConnectivityException
     * @throws HaapiException
     * @throws InternalErrorException
     * @throws InvalidRequestException
     * @throws RepositoryException
     * @throws UnauthorizedException
     */
    public function __invoke(
        string $orderId,
        OrderGet $orderGet,
        Authenticatable $user,
        CampaignRepository $campaignRepository
    ): OrderDetailsResource {
        $campaign = $campaignRepository->findCampaignByOrderId($orderId);

        $orderArray = $orderGet->handle(
            new OrderGetParams(
                [
                    'accountId' => $campaign->user->account_external_id,
                    'orderId'   => (string) $campaign->order_id,
                ]
            ),
            $user->id
        )->toArray();

        $orderArray = Arr::get($orderArray, 'payload.order');

        return new OrderDetailsResource($orderArray);
    }
}
