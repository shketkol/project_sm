<?php

namespace Modules\Payment\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodGet;
use Modules\Haapi\Exceptions\BadPaymentMethodException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Payment\Http\Resources\PaymentMethodResource;
use Modules\Payment\Models\PaymentMethod;
use Throwable;

/**
 * Class IndexPaymentMethodController
 *
 * @package Modules\Payment\Http\Controllers
 */
class IndexPaymentMethodController extends Controller
{
    /**
     * Index payment methods
     *
     * @param Authenticatable|\Modules\User\Models\User $user
     * @param PaymentMethodGet                          $action
     *
     * @return AnonymousResourceCollection
     * @throws HaapiException
     */
    public function __invoke(Authenticatable $user, PaymentMethodGet $action): AnonymousResourceCollection
    {
        try {
            $paymentMethods = $user->paymentMethods->map(function (PaymentMethod $method) use ($action, $user) {
                $cardInfo = $action->handle($method->getExternalId(), $user->getKey());
                $cardInfo['paymentMethod'] = $method;
                return $cardInfo;
            });
        } catch (HaapiException $exception) {
            throw BadPaymentMethodException::create($exception->getMessage(), $exception->getCode());
        } catch (Throwable $exception) {
            throw BadPaymentMethodException::create(
                trans('payment::messages.cannot_be_obtained'),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return PaymentMethodResource::collection($paymentMethods)
            ->additional(PaymentMethodResource::getAdditionalData());
    }
}
