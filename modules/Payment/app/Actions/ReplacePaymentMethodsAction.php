<?php

namespace Modules\Payment\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Payment\Exceptions\PaymentMethodNotReplacedException;
use Modules\Payment\Models\PaymentMethod;
use Modules\Payment\Repositories\PaymentMethodRepository;
use Modules\User\Actions\Payment\SetPaymentProcessingAction;
use Modules\User\Models\User;

class ReplacePaymentMethodsAction
{
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var StorePaymentMethodsAction
     */
    protected $storePaymentAction;

    /**
     * @var PaymentMethodRepository
     */
    protected $repository;

    /**
     * @var SetPaymentProcessingAction
     */
    protected $setPaymentProcessingAction;

    /**
     * ReplacePaymentMethodsAction constructor.
     *
     * @param DatabaseManager           $databaseManager
     * @param Logger                    $log
     * @param StorePaymentMethodsAction $storePaymentAction
     * @param PaymentMethodRepository   $repository
     */
    public function __construct(
        DatabaseManager $databaseManager,
        Logger $log,
        StorePaymentMethodsAction $storePaymentAction,
        PaymentMethodRepository $repository,
        SetPaymentProcessingAction $setPaymentProcessingAction
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->storePaymentAction = $storePaymentAction;
        $this->repository = $repository;
        $this->setPaymentProcessingAction = $setPaymentProcessingAction;
    }


    /**
     * @param      $methodToReplace
     * @param User $user
     *
     * @return PaymentMethod
     * @throws PaymentMethodNotReplacedException
     */
    public function handle($methodToReplace, User $user): PaymentMethod
    {
        try {
            $this->databaseManager->beginTransaction();
            $this->log->info('Replacing current payment method.');
            $newMethod = $this->storePaymentAction->handle();

            // Set payment-processing flag and reminder if possible.
            if ($user->maySetPaymentProcessing()) {
                $this->setPaymentProcessingAction->handle($user);
            }

            $this->log->info('Removing old payment method.', [
                'newPaymentMethodId' => $newMethod->getKey(),
                'oldPaymentMethodId' => $methodToReplace->getKey(),
            ]);
            $this->repository->delete($methodToReplace->getKey());
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();

            throw PaymentMethodNotReplacedException::createFrom($throwable);
        }

        $this->databaseManager->commit();
        $this->log->info('Replacing current payment method was finished successfully.');

        return $newMethod;
    }
}
