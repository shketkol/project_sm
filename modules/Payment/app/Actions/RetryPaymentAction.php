<?php

namespace Modules\Payment\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Haapi\Actions\Account\Contracts\AccountRetryPayment;
use Modules\Payment\Exceptions\RetryPaymentFailedException;
use Modules\User\Actions\Payment\SetPaymentProcessingAction;
use Modules\User\Models\User;
use Modules\User\Repositories\UserRepository;

class RetryPaymentAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var AccountRetryPayment
     */
    protected $accountRetryPayment;

    /**
     * @var SetPaymentProcessingAction
     */
    protected $setPaymentProcessingAction;

    /**
     * RetryPaymentAction constructor.
     *
     * @param Logger                     $log
     * @param UserRepository             $repository
     * @param DatabaseManager            $databaseManager
     * @param AccountRetryPayment        $accountRetryPayment
     * @param SetPaymentProcessingAction $setPaymentProcessingAction
     */
    public function __construct(
        Logger $log,
        UserRepository $repository,
        DatabaseManager $databaseManager,
        AccountRetryPayment $accountRetryPayment,
        SetPaymentProcessingAction $setPaymentProcessingAction
    ) {
        $this->databaseManager                 = $databaseManager;
        $this->log                             = $log;
        $this->repository                      = $repository;
        $this->accountRetryPayment             = $accountRetryPayment;
        $this->setPaymentProcessingAction = $setPaymentProcessingAction;
    }

    /**
     * @param User $user
     *
     * @return void
     * @throws RetryPaymentFailedException
     */
    public function handle(User $user): void
    {
        $this->log->info('Sending "retry payment" request.');

        $this->databaseManager->beginTransaction();
        try {
            // Update retry-payment states and set "processing" reminder.
            $this->setPaymentProcessingAction->handle($user);
            $this->accountRetryPayment->handle($user->id);
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();
            throw RetryPaymentFailedException::createFrom($throwable);
        }

        $this->log->info('"Retry payment" request was sent.');
        $this->databaseManager->commit();
    }
}
