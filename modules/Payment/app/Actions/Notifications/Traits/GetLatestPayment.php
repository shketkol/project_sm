<?php

namespace Modules\Payment\Actions\Notifications\Traits;

use Illuminate\Support\Arr;
use Modules\Payment\Exceptions\OrderNotFoundException;
use Modules\Payment\Models\Transaction;

trait GetLatestPayment
{
    /**
     * @param array $invoices
     * @param string $orderId
     * @param int|null $statusId
     * @return array
     * @throws OrderNotFoundException
     */
    private function retrieveLatestPayment(array $invoices, string $orderId, int $statusId = null): array
    {
        while ($invoices) {
            $latestInvoice = array_pop($invoices);
            $paymentHistory = Arr::get($latestInvoice, 'paymentHistory');

            if (!$paymentHistory) {
                continue;
            }

            while ($paymentHistory) {
                $latestPayment = array_pop($paymentHistory);
                if (is_null($statusId)) {
                    return $latestPayment;
                }
                $transactionStatusId = Transaction::getStatusIdByTransactionFlow(
                    Arr::get($latestPayment, 'status'),
                    'transaction'
                );
                if ($transactionStatusId === $statusId) {
                    return $latestPayment;
                }
            }
        }

        throw OrderNotFoundException::create($orderId, 'Cannot retrieve Order Data');
    }
}
