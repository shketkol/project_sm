<?php

namespace Modules\Payment\Actions\Notifications;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Campaign\Models\Campaign;
use Modules\User\Notifications\Admin\BadPayment as BadPaymentAdmin;
use Modules\User\Notifications\Advertiser\BadPayment;
use Modules\User\Repositories\UserRepository;
use Prettus\Repository\Exceptions\RepositoryException;

class FailOrderStatusAction
{
    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param Dispatcher     $notification
     * @param UserRepository $userRepository
     */
    public function __construct(Dispatcher $notification, UserRepository $userRepository)
    {
        $this->notification = $notification;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Campaign $campaign
     *
     * @throws RepositoryException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     */
    public function handle(Campaign $campaign): void
    {
        $user = $campaign->user;

        // Send notification to advertiser
        $advertiserNotification = new BadPayment($campaign, $user);
        $advertiserNotification->setBcc(config('notification.failed_payment_bcc'));
        $advertiserNotification->setCc(config('notification.failed_payment_cc'));
        $this->notification->send($user, $advertiserNotification);

        // Send notification to admin
        $this->notification->send(
            $this->userRepository->getAdmin(),
            new BadPaymentAdmin($user)
        );
    }
}
