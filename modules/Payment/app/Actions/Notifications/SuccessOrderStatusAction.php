<?php

namespace Modules\Payment\Actions\Notifications;

use Illuminate\Contracts\Notifications\Dispatcher;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Payment\PaymentMethodGet;
use Modules\Notification\Actions\DestroyAlertsAction;
use Modules\Payment\Actions\Notifications\Traits\GetLatestPayment;
use Modules\Payment\Models\TransactionStatus;
use Modules\Payment\Notifications\Advertiser\TransactionIsMade;

class SuccessOrderStatusAction
{
    use ActAsAdmin;
    use GetLatestPayment;

    /**
     * @var PaymentMethodGet
     */
    private $paymentMethodGet;

    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * @var DestroyAlertsAction
     */
    protected $destroyAlertsAction;

    /**
     * @param PaymentMethodGet    $paymentMethodGet
     * @param Dispatcher          $notification
     * @param DestroyAlertsAction $destroyAlertsAction
     */
    public function __construct(
        PaymentMethodGet $paymentMethodGet,
        Dispatcher $notification,
        DestroyAlertsAction $destroyAlertsAction
    ) {
        $this->paymentMethodGet    = $paymentMethodGet;
        $this->notification        = $notification;
        $this->destroyAlertsAction = $destroyAlertsAction;
    }

    /**
     * @param Campaign $campaign
     * @param array    $invoices
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Payment\Exceptions\OrderNotFoundException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Campaign $campaign, array $invoices): void
    {
        $user = $campaign->user;

        $this->notification->send(
            $user,
            new TransactionIsMade(
                $campaign,
                $this->fetchOrderData($campaign, $invoices)
            )
        );
    }

    /**
     * @param Campaign $campaign
     * @param array    $invoices
     *
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Payment\Exceptions\OrderNotFoundException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function fetchOrderData(Campaign $campaign, array $invoices): array
    {
        $latestPayment = $this->retrieveLatestPayment($invoices, $campaign->order_id, TransactionStatus::ID_SUCCESS);
        $amount = Arr::get($latestPayment, 'amount');

        $paymentMethodData = $this->paymentMethodGet->handle(
            $campaign->paymentMethod->getExternalId(),
            $this->authAsAdmin()
        );

        return [
            'amount'            => $amount,
            'paymentMethodName' => trans('payment::labels.ending_in', [
                'brand'     => trans("payment::labels." . str_replace(
                    ' ',
                    '_',
                    strtolower(Arr::get($paymentMethodData, 'brand'))
                )),
                'ending_id' => Arr::get($paymentMethodData, 'lastFour'),
            ]),
        ];
    }
}
