<?php

namespace Modules\Payment\Actions\Notifications;

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Exceptions\CampaignNotFoundException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Daapi\Actions\Notification\UpdateStatusBaseAction;
use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Payment\OrderGet;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetParams;
use Modules\Payment\Actions\Notifications\Traits\GetLatestPayment;
use Modules\Payment\Exceptions\PaymentNotificationException;
use Modules\Payment\Models\OrderStatus;
use Modules\Payment\Models\Transaction;
use Modules\Payment\Repositories\TransactionRepository;
use Modules\User\Models\User;
use Prettus\Repository\Eloquent\BaseRepository;

class ProcessOrderStatusAction extends UpdateStatusBaseAction
{
    use ActAsAdmin;
    use GetLatestPayment;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var BaseRepository
     */
    protected $repository;

    /**
     * @var Model|\App\Traits\State
     */
    protected $entity;

    /**
     * @var SuccessOrderStatusAction
     */
    private $successOrderStatusAction;

    /**
     * @var FailOrderStatusAction
     */
    private $failOrderStatusAction;

    /**
     * @var OrderGet
     */
    private $orderGet;

    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @param Logger $log
     * @param CampaignRepository $repository
     * @param SuccessOrderStatusAction $successOrderStatusAction
     * @param FailOrderStatusAction $failOrderStatusAction
     * @param OrderGet $orderGet
     * @param TransactionRepository $transactionRepository
     * @param DatabaseManager $databaseManager
     */
    public function __construct(
        Logger $log,
        CampaignRepository $repository,
        SuccessOrderStatusAction $successOrderStatusAction,
        FailOrderStatusAction $failOrderStatusAction,
        OrderGet $orderGet,
        TransactionRepository $transactionRepository,
        DatabaseManager $databaseManager
    ) {
        $this->successOrderStatusAction = $successOrderStatusAction;
        $this->failOrderStatusAction = $failOrderStatusAction;
        $this->orderGet = $orderGet;
        $this->transactionRepository = $transactionRepository;
        $this->databaseManager = $databaseManager;

        parent::__construct($log, $repository);
    }

    /**
     * @param string     $orderId
     * @param string     $status
     * @param array|null $additionalData
     *
     * @throws CampaignNotFoundException
     * @throws PaymentNotificationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Payment\Exceptions\OrderNotFoundException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function handle(string $orderId, string $status, ?array $additionalData = null): void
    {
        /*** @var Campaign $campaign  */
        $campaign = $this->getEntity($orderId, 'order_id');

        if (!$campaign) {
            throw CampaignNotFoundException::create($orderId, 'change order status');
        }

        $this->log->info('Send notification while change order status', [
            'order_id'  => $campaign->order_id,
            'to_status' => $status,
        ]);

        $invoices = $this->getInvoices($campaign, $campaign->user);
        $latestPayment = $this->retrieveLatestPayment($invoices, $campaign->order_id);

        $lastTransactionId = Arr::get($latestPayment, 'transactionId');

        if ($this->transactionRepository->isTransactionProcessed($lastTransactionId)) {
            return;
        }

        $this->databaseManager->beginTransaction();

        // Send notification
        try {
            $this->storeProcessedTransaction($latestPayment, $lastTransactionId);

            $this->handleNotifications($status, $campaign, $invoices);
            $this->databaseManager->commit();
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();

            $message = sprintf(
                'Fail send payment notification. Reason: "%s".',
                $throwable->getMessage()
            );

            throw PaymentNotificationException::create($message, $throwable);
        }

        $this->log->info('Notification was successfully sent.', [
            'order_id' => $campaign->order_id,
            'status'   => $status,
        ]);
    }

    /**
     * Get invoices from HAAPI
     *
     * @param Campaign $campaign
     * @param User     $user
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getInvoices(Campaign $campaign, User $user): array
    {
        $adminId = $this->authAsAdmin();

        $response = $this->orderGet->handle(
            new OrderGetParams(
                [
                    'accountId' => $user->account_external_id,
                    'orderId'   => (string) $campaign->order_id,
                ]
            ),
            $adminId
        );

        return Arr::get($response->toArray(), 'payload.order.invoices');
    }

    /**
     * @param string   $status
     * @param Campaign $campaign
     * @param array    $invoices
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Payment\Exceptions\OrderNotFoundException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function handleNotifications(string $status, Campaign $campaign, array $invoices)
    {
        if (OrderStatus::checkSuccessByHaapiTransition($status)) {
            $this->successOrderStatusAction->handle($campaign, $invoices);

            return;
        }

        $this->failOrderStatusAction->handle($campaign);
    }

    /**
     * @param string $orderId
     * @param string $field
     *
     * @return Model|null
     */
    protected function getEntity(string $orderId, string $field = 'external_id'): ?Model
    {
        return $this->entity = $this->repository->findCampaignByOrderId($orderId);
    }

    /**
     * @param array  $latestPayment
     * @param string $lastTransactionId
     */
    private function storeProcessedTransaction(array $latestPayment, string $lastTransactionId)
    {
        $transactionStatusId = Transaction::getStatusIdByTransactionFlow(
            Arr::get($latestPayment, 'status'),
            'transaction'
        );

        Transaction::firstOrCreate([
            'external_id' => $lastTransactionId,
            'status_id' => $transactionStatusId,
        ]);
    }
}
