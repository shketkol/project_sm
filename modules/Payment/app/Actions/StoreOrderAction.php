<?php

namespace Modules\Payment\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Campaign\Exceptions\CampaignNotFoundException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Repositories\CampaignRepository;

class StoreOrderAction
{
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var CampaignRepository
     */
    protected $repository;

    /**
     * @param CampaignRepository $repository
     * @param DatabaseManager $databaseManager
     * @param Logger $log
     */
    public function __construct(
        CampaignRepository $repository,
        DatabaseManager $databaseManager,
        Logger $log
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * Store new payment method.
     *
     * @param Campaign $campaign
     * @param array $data
     * @return Campaign
     * @throws CampaignNotFoundException
     * @throws \Exception
     */
    public function handle(Campaign $campaign, array $data = []): Campaign
    {
        $this->log->info('Storing new campaign payment.', ['data' => $data]);
        $this->databaseManager->beginTransaction();

        try {
            $campaign = $this->repository->update($data, $campaign->id);
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();

            throw new CampaignNotFoundException();
        }

        $this->databaseManager->commit();

        return $campaign;
    }
}
