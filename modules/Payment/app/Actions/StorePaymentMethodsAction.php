<?php

namespace Modules\Payment\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodCreate;
use Modules\Haapi\DataTransferObjects\Payment\PaymentMethodCreateParams;
use Modules\Payment\Actions\WorldPay\StoreCardOnFileAction;
use Modules\Payment\Exceptions\PaymentMethodHAAPIException;
use Modules\Payment\Exceptions\PaymentMethodNotCreatedException;
use Modules\Payment\Models\PaymentMethod;
use Modules\Payment\Repositories\PaymentMethodRepository;
use Modules\Payment\Http\Requests\StorePaymentMethodRequest;

/**
 * Class StorePaymentMethodsAction
 *
 * @package Modules\Payment\Actions
 */
class StorePaymentMethodsAction
{
    /**
     * @var Authenticatable|\Modules\User\Models\User
     */
    protected $user;

    /**
     * @var StorePaymentMethodRequest
     */
    protected $request;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var PaymentMethodRepository
     */
    protected $repository;

    /**
     * @var StoreCardOnFileAction
     */
    protected $cardOnFileAction;

    /**
     * @var PaymentMethodCreate
     */
    protected $haapiCreate;

    /**
     * StorePaymentMethodsAction constructor.
     *
     * @param PaymentMethodRepository $repository
     * @param StorePaymentMethodRequest $request
     * @param StoreCardOnFileAction $cardOnFileAction
     * @param PaymentMethodCreate $haapiCreate
     * @param DatabaseManager $databaseManager
     * @param Authenticatable $user
     * @param Logger $log
     */
    public function __construct(
        PaymentMethodRepository $repository,
        StorePaymentMethodRequest $request,
        StoreCardOnFileAction $cardOnFileAction,
        PaymentMethodCreate $haapiCreate,
        DatabaseManager $databaseManager,
        Authenticatable $user,
        Logger $log
    ) {
        $this->user = $user;
        $this->request = $request;
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
        $this->cardOnFileAction = $cardOnFileAction;
        $this->haapiCreate = $haapiCreate;
    }

    /**
     * Store new payment method.
     *
     * @param array $data
     *
     * @return PaymentMethod
     * @throws \App\Exceptions\BaseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function handle(array $data = []): PaymentMethod
    {
        $this->log->info('Storing new payment method.');

        $data = $this->prepareData();
        $token = $this->handleWorldPay($data);

        $this->log->info('Storing new payment method to haapi');
        try {
            $paymentMethodId = $this->storeToHaapi($data, $token);
            $this->log->info('Payment method was stored to haapi');

            return $this->storeToDb($paymentMethodId, $data);
        } catch (\Exception $exception) {
            $this->log->error('Fail storing new payment method to haapi', [
                'message' => $exception->getMessage()
            ]);
            throw new PaymentMethodHAAPIException();
        }
    }

    /**
     * @param string $paymentMethodId
     * @param array  $data
     *
     * @return PaymentMethod
     * @throws PaymentMethodNotCreatedException
     */
    protected function storeToDb(string $paymentMethodId, array $data): PaymentMethod
    {
        $this->databaseManager->beginTransaction();
        try {
            $paymentMethod = $this->repository->create([
                'user_id'     => $this->user->getKey(),
                'external_id' => $paymentMethodId,
                'name'        => $data['name'],
            ]);
            $this->attachToActiveCampaigns($paymentMethod);
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();
            throw new PaymentMethodNotCreatedException($throwable->getMessage(), $throwable->getCode(), $throwable);
        }

        $this->databaseManager->commit();

        return $paymentMethod;
    }

    /**
     * @param array $data
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    protected function handleWorldPay(array $data): string
    {
        $this->cardOnFileAction->handle($data);
        return $this->cardOnFileAction->getToken();
    }

    /**
     * @param PaymentMethod $paymentMethod
     */
    protected function attachToActiveCampaigns(PaymentMethod $paymentMethod): void
    {
        $this->user->campaigns()->whereNotIn('status_id', [CampaignStatus::ID_DRAFT])->get()
            ->each(function ($campaign) use ($paymentMethod) {
                $campaign->paymentMethod()->associate($paymentMethod);
                $campaign->save();
            });
    }

    /**
     * @param string $userId
     * @return string
     */
    protected function generateNamespace(string $userId): string
    {
        return config('world-pay.requests.card_on_file.namespace_prefix') . $userId;
    }

    /**
     * @param array  $params
     * @param string $token
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function storeToHaapi(array $params, string $token): string
    {
        return $this->haapiCreate->handle(new PaymentMethodCreateParams([
            'paymentToken' => $token,
            'namespace'    => Arr::get($params, 'namespace'),
            'accountId'    => Arr::get($params, 'user_external_id'),
        ]), Arr::get($params, 'user_id'))->get('paymentMethodId');
    }

    /**
     * @return array
     */
    protected function prepareData(): array
    {
        $data = $this->request->toData();
        $data['user_external_id'] = $this->user->getExternalId();
        $data['user_id'] = $this->user->getKey();
        $data['namespace'] = $this->generateNamespace($this->user->getExternalId());

        return $data;
    }
}
