<?php

namespace Modules\Payment\Actions\WorldPay;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Modules\Payment\Api\Requests\CardOnFileRequest;
use Modules\Payment\Api\Responses\WorldPayResponse;
use Modules\Payment\Api\WorldPayClient;
use Modules\Payment\Api\Requests\ResolveTokenConflictsRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreCardOnFileAction extends BaseAction
{
    /**
     * @var CardOnFileRequest
     */
    protected $request;

    /**
     * @var ResolveTokenConflictsRequest
     */
    protected $resolveRequest;

    /**
     * @var GetValidationMessageAction
     */
    protected $getValidationMessage;

    /**
     * @param Logger                       $log
     * @param CardOnFileRequest            $request
     * @param ResolveTokenConflictsRequest $resolveRequest
     * @param WorldPayClient               $client
     * @param GetValidationMessageAction   $validationMessageAction
     */
    public function __construct(
        Logger $log,
        CardOnFileRequest $request,
        ResolveTokenConflictsRequest $resolveRequest,
        WorldPayClient $client,
        GetValidationMessageAction $validationMessageAction
    ) {
        parent::__construct($log, $client);
        $this->request = $request;
        $this->resolveRequest = $resolveRequest;
        $this->getValidationMessage = $validationMessageAction;
    }

    /**
     * Store new payment method.
     *
     * @param array $data
     *
     * @return WorldPayResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function handle(array $data): WorldPayResponse
    {
        $this->request->setBody($data);
        $this->logger('Starting external payment token creation', $this->request->getPayload());

        try {
            $this->response = $this->client->request($this->request);
        } catch (\Throwable $exception) {
            if ($exception->getCode() === Response::HTTP_CONFLICT) {
                $this->resolveTokenConflict($exception);
            } elseif ($exception->getCode() === Response::HTTP_UNPROCESSABLE_ENTITY) {
                $message = $this->parseWpValidationMessage($exception);
                throw ValidationException::withMessages([$message]);
            } else {
                throw $exception;
            }
        }

        if ($this->response->getStatusCode() === Response::HTTP_PARTIAL_CONTENT) {
            $message = $this->getValidationMessage->handle($this->getValidationUrl());
            $this->logger('Invalid session state', [$message]);
            throw ValidationException::withMessages([$message]);
        }

        $this->logger('World pay payment token created', $this->response->getContent());

        return $this->response;
    }

    /**
     * We should resolve token conflict in case if we're updating card info of existing card.
     *
     * @link https://beta.developer.worldpay.com/docs/access-worldpay/tokens/querying-and-updating-tokens#updating-a-token-with-conflicts
     *
     * @param ClientException $exception
     *
     * @return void
     * @throws \Throwable
     */
    protected function resolveTokenConflict(ClientException $exception)
    {
        $this->response = new WorldPayResponse($exception->getResponse());

        $this->resolveRequest->setUrl($this->parseWpResolveUrl());
        try {
            $this->client->request($this->resolveRequest);
        } catch (\Throwable $exception) {
            $this->logger('Conflicted token cannot be resolved', [$exception->getMessage()]);
            throw $exception;
        }
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        $path = Arr::get(parse_url($this->getTokenUrl()), 'path');
        return Arr::last(explode('/', $path));
    }

    /**
     * @return string
     */
    protected function getTokenUrl(): string
    {
        return Arr::get($this->response->getContent(), '_links.tokens:token.href');
    }

    /**
     * @return string
     */
    protected function getValidationUrl(): string
    {
        return Arr::get($this->response->getContent(), '_links.verifications:verification.href');
    }

    /**
     * @param ClientException $exception
     * @return string
     */
    protected function parseWpValidationMessage(ClientException $exception): string
    {
        return Arr::get(json_decode($exception->getResponse()->getBody()->getContents(), true), 'message');
    }

    /**
     * @return string
     */
    protected function parseWpResolveUrl(): string
    {
        return Arr::get($this->response->getContent(), '_links.tokens:conflicts.href');
    }
}
