<?php

namespace Modules\Payment\Actions\WorldPay;

use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Payment\Api\Requests\GetTokenDetailsRequest;
use Modules\Payment\Api\WorldPayClient;

class GetTokenDetailsAction extends BaseAction
{
    /**
     * @var GetTokenDetailsRequest
     */
    protected $request;

    /**
     * GetTokenDetailsAction constructor.
     *
     * @param Logger $log
     * @param WorldPayClient $client
     * @param GetTokenDetailsRequest $request
     */
    public function __construct(Logger $log, WorldPayClient $client, GetTokenDetailsRequest $request)
    {
        parent::__construct($log, $client);
        $this->request = $request;
    }

    /**
     * Store new payment method.
     *
     * @param string $url
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function handle(string $url): array
    {
        $this->log->info('Starting retrieving payment token  details information');

        $this->request->setUrl($url);
        $this->response = $this->client->request($this->request);

        $this->log->info('Payment token  details information retrieved');

        return $this->toArray();
    }

    /**
     * @return array
     */
    protected function toArray(): array
    {
        $payload = $this->response->getContent();

        return [
            'token'     => Arr::get($payload, 'tokenPaymentInstrument.href'),
            'exp_month' => Arr::get($payload, 'paymentInstrument.cardExpiryDate.month'),
            'exp_year'  => Arr::get($payload, 'paymentInstrument.cardExpiryDate.year'),
            'last4'     => substr(Arr::get($payload, 'paymentInstrument.cardNumber'), -4),
        ];
    }
}
