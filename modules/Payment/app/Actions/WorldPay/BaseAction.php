<?php

namespace Modules\Payment\Actions\WorldPay;

use App\Helpers\EnvironmentHelper;
use Illuminate\Log\Logger;
use Modules\Payment\Api\Responses\WorldPayResponse;
use Modules\Payment\Api\WorldPayClient;

class BaseAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var WorldPayClient
     */
    protected $client;

    /**
     * @var WorldPayResponse
     */
    protected $response;

    /**
     * BaseAction constructor.
     * @param Logger $log
     * @param WorldPayClient $client
     */
    public function __construct(Logger $log, WorldPayClient $client)
    {
        $this->log = $log;
        $this->client = $client;
    }

    /**
     * @param string $message
     * @param array $data
     */
    protected function logger(string $message, array $data): void
    {
        if (EnvironmentHelper::isProdEnv()) {
            $this->log->info($message);
            return;
        }

        $this->log->info($message, $data);
    }
}
