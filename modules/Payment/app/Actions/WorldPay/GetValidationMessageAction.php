<?php

namespace Modules\Payment\Actions\WorldPay;

use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Payment\Api\Requests\GetValidationMessageRequest;
use Modules\Payment\Api\WorldPayClient;

class GetValidationMessageAction extends BaseAction
{
    /**
     * @var GetValidationMessageRequest
     */
    protected $request;

    /**
     * StoreCardOnFileAction constructor.
     * @param Logger $log
     * @param GetValidationMessageRequest $request
     * @param WorldPayClient $client
     */
    public function __construct(Logger $log, GetValidationMessageRequest $request, WorldPayClient $client)
    {
        parent::__construct($log, $client);
        $this->request = $request;
    }

    /**
     * Store new payment method.
     *
     * @param string $url
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function handle(string $url): string
    {
        $this->log->info('Starting retrieve validation message');

        $this->request->setUrl($url);
        $this->response = $this->client->request($this->request);

        $this->log->info('Validation Message retrieved');

        return $this->getMessage();
    }

    /**
     * @return string
     */
    protected function getMessage(): string
    {
        return Arr::get($this->response->getContent(), 'message');
    }
}
