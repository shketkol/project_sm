<?php

namespace Modules\Payment\Actions;

use Barryvdh\DomPDF\PDF;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Payment\OrderGet;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetParams;
use Modules\Payment\Http\Resources\OrderDetailsResource;

class DownloadPaymentPdfAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var PDF
     */
    protected $pdfBuilder;

    /**
     * @var Authenticatable
     */
    protected $user;

    /**
     * @var OrderGet
     */
    protected $orderGet;

    /**
     * @param Logger          $log
     * @param PDF             $pdfBuilder
     * @param Authenticatable $user
     * @param OrderGet        $orderGet
     */
    public function __construct(
        Logger $log,
        PDF $pdfBuilder,
        Authenticatable $user,
        OrderGet $orderGet
    ) {
        $this->user = $user;
        $this->pdfBuilder = $pdfBuilder;
        $this->log = $log;
        $this->orderGet = $orderGet;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Response
     *
     * @throws GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(Campaign $campaign): Response
    {
        $orderArray = $this->orderGet->handle(
            new OrderGetParams(
                [
                    'accountId' => $campaign->user->getExternalId(),
                    'orderId'   => (string)$campaign->order_id,
                ]
            ),
            $this->user->id
        )->toArray();

        $orderArray = Arr::get($orderArray, 'payload.order');

        $resource = new OrderDetailsResource($orderArray);

        $this->log->info('Generating campaign payment pdf.');

        $data = [
            'data' => $resource->toRawData(),
        ];

        $filename = str_replace(' ', '_', $campaign->name) . '.pdf';

        return $this->pdfBuilder->loadView('payment::pdf.payment', $data)
            ->download(addslashes($filename));
    }
}
