<?php

namespace Modules\Payment\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Haapi\Actions\Payment\Contracts\PaymentMethodDelete;
use Modules\Haapi\DataTransferObjects\Payment\PaymentMethodDeleteParams;
use Modules\Payment\Exceptions\PaymentMethodNotDeletedException;
use Modules\Payment\Models\PaymentMethod;
use Modules\Payment\Repositories\PaymentMethodRepository;

/**
 * Class DeletePaymentMethodsAction
 *
 * @package Modules\Payment\Actions
 */
class DeletePaymentMethodsAction
{
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var PaymentMethodRepository
     */
    protected $repository;

    /**
     * @var PaymentMethodDelete
     */
    protected $haapiDelete;

    /**
     * DeletePaymentMethodsAction constructor.
     *
     * @param PaymentMethodRepository $repository
     * @param DatabaseManager $databaseManager
     * @param PaymentMethodDelete $haapiDelete
     * @param Logger $log
     */
    public function __construct(
        PaymentMethodRepository $repository,
        DatabaseManager $databaseManager,
        PaymentMethodDelete $haapiDelete,
        Logger $log
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
        $this->haapiDelete = $haapiDelete;
    }

    /**
     * Delete payment method.
     *
     * @param PaymentMethod $paymentMethod
     *
     * @return void
     *
     * @throws \App\Exceptions\BaseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(PaymentMethod $paymentMethod): void
    {
        $this->log->info('Deleting payment method.', ['data' => $paymentMethod->getKey()]);

        $this->databaseManager->beginTransaction();

        try {
            $this->haapiDelete->handle(new PaymentMethodDeleteParams([
                'paymentMethodId' => $paymentMethod->getExternalId(),
            ]), $paymentMethod->user->getKey());
            $this->repository->delete($paymentMethod->getKey());
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();
            throw PaymentMethodNotDeletedException::createFrom($throwable);
        }

        $this->databaseManager->commit();
    }
}
