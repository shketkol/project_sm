<?php

namespace Modules\Payment\Api\Responses;

use Psr\Http\Message\ResponseInterface;

class WorldPayResponse
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var array
     */
    protected $content;

    /**
     * WorldPayResponse constructor.
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
        $this->content = $this->decodeResponse();
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->response->getStatusCode();
    }

    /**
     * @return array
     */
    protected function decodeResponse(): array
    {
        $content = $this->response->getBody()->getContents();
        $content = json_decode($content, true);

        return $this->content = $content ?: [];
    }
}
