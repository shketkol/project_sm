<?php

namespace Modules\Payment\Api\Requests;

class ResolveTokenConflictsRequest extends WorldPayRequest
{
    public function __construct()
    {
        parent::__construct();
        $this->setHeaders(config('world-pay.requests.resolve_token_conflicts.headers.type'));
        $this->setType(config('world-pay.requests.resolve_token_conflicts.type'));
        $this->setBody([]);
    }

    /**
     * @param string $type
     * @param string|null $accept
     * @return array
     */
    public function setHeaders(string $type, ?string $accept = null): array
    {
        return $this->headers = [
            'Content-Type' => $type,
        ];
    }

    /**
     * @param string $name
     * @param string $password
     * @return array
     */
    public function setAuth(string $name, string $password): array
    {
        return $this->auth = [$name, $password];
    }

    /**
     * @param string $url
     * @return string
     */
    public function setUrl(string $url): string
    {
        return $this->url = $url;
    }

    /**
     * @param string $type
     * @return string
     */
    public function setType(string $type): string
    {
        return $this->type = $type;
    }

    /**
     * @param array $params
     * @return array
     */
    public function setBody(array $params): array
    {
        return $this->body = $params;
    }
}
