<?php

namespace Modules\Payment\Api\Requests;

abstract class WorldPayRequest
{
    /**
     * @var array
     */
    protected $headers;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var array
     */
    protected $auth;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    protected $body;

    /**
     * WorldPayRequest constructor.
     */
    public function __construct()
    {
        $this->setAuth(config('world-pay.username'), config('world-pay.password'));
    }

    /**
     * @param string $type
     * @param string|null $accept
     * @return array
     */
    abstract public function setHeaders(string $type, ?string $accept): array;

    /**
     * @param string $name
     * @param string $password
     * @return array
     */
    abstract public function setAuth(string $name, string $password): array;

    /**
     * @param string $url
     * @return string
     */
    abstract public function setUrl(string $url): string;

    /**
     * @param string $type
     * @return array
     */
    abstract public function setType(string $type): string ;

    /**
     * @param array $body
     * @return array
     */
    abstract public function setBody(array $body): array;

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return [
            'auth'    => $this->getAuth(),
            'headers' => $this->getHeaders(),
            'body'    => $this->getBody(),
        ];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return array
     */
    protected function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return array
     */
    protected function getAuth(): array
    {
        return $this->auth;
    }

    /**
     * @return string
     */
    protected function getBody(): string
    {
        return json_encode($this->body);
    }
}
