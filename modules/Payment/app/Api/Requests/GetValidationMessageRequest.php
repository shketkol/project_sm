<?php

namespace Modules\Payment\Api\Requests;

class GetValidationMessageRequest extends WorldPayRequest
{
    /**
     * GetValidationMessageRequest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setHeaders(
            config('world-pay.requests.validation.headers.type'),
            config('world-pay.requests.validation.headers.accept')
        );
        $this->setType(config('world-pay.requests.validation.type'));
        $this->setBody([]);
    }

    /**
     * @param string $type
     * @param string|null $accept
     * @return array
     */
    public function setHeaders(string $type, ?string $accept): array
    {
        return $this->headers = [
            'Content-Type' => $type,
            'Accept'       => $accept,
        ];
    }

    /**
     * @param string $name
     * @param string $password
     * @return array
     */
    public function setAuth(string $name, string $password): array
    {
        return $this->auth = [$name, $password];
    }

    /**
     * @param string $url
     * @return string
     */
    public function setUrl(string $url): string
    {
        return $this->url = $url;
    }

    /**
     * @param string $type
     * @return string
     */
    public function setType(string $type): string
    {
        return $this->type = $type;
    }

    /**
     * @param array $params
     * @return array
     */
    public function setBody(array $params): array
    {
        return $this->body = $params;
    }
}
