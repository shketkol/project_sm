<?php

namespace Modules\Payment\Api\Requests;

class CardOnFileRequest extends WorldPayRequest
{
    /**
     * CardOnFileRequest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setHeaders(
            config('world-pay.requests.card_on_file.headers.type'),
            config('world-pay.requests.card_on_file.headers.accept')
        );
        $this->setUrl(config('world-pay.requests.card_on_file.url'));
        $this->setType(config('world-pay.requests.card_on_file.type'));
    }

    /**
     * @param string $type
     * @param string|null $accept
     * @return array
     */
    public function setHeaders(string $type, ?string $accept): array
    {
        return $this->headers = [
            'Content-Type' => $type,
            'Accept'       => $accept,
        ];
    }

    /**
     * @param string $name
     * @param string $password
     * @return array
     */
    public function setAuth(string $name, string $password): array
    {
        return $this->auth = [$name, $password];
    }

    /**
     * @param string $url
     * @return string
     */
    public function setUrl(string $url): string
    {
        return $this->url = $url;
    }

    /**
     * @param string $type
     * @return string
     */
    public function setType(string $type): string
    {
        return $this->type = $type;
    }

    /**
     * @param array $params
     * @return array
     */
    public function setBody(array $params): array
    {
        return $this->body = [
            'paymentInstrument'    => [
                'type'           => config('world-pay.requests.card_on_file.native_type'),
                'cardHolderName' => $params['name'],
                'sessionHref'    => $params['session'],
                'billingAddress' => [
                    "address1"    => $params['address'],
                    'countryCode' => config('world-pay.country_code'),
                    'city'        => $params['city'],
                    "postalCode"  => $params['zip'],
                ],
            ],
            "merchant"             => [
                "entity" => config('world-pay.entity'),
            ],
            "verificationCurrency" => config('world-pay.currency'),
            "namespace"            => $params['namespace'],
        ];
    }
}
