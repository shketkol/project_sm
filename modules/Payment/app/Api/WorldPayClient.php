<?php

namespace Modules\Payment\Api;

use GuzzleHttp\Client;
use Modules\Payment\Api\Responses\WorldPayResponse;
use Modules\Payment\Api\Requests\WorldPayRequest;
use Psr\Log\LoggerInterface;

class WorldPayClient
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * WorldPayClient constructor.
     * @param Client $client
     * @param LoggerInterface $log
     */
    public function __construct(Client $client, LoggerInterface $log)
    {
        $this->client = $client;
        $this->log = $log;
    }

    /**
     * @param WorldPayRequest $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function request(WorldPayRequest $request): WorldPayResponse
    {
        $response = $this->client->request($request->getType(), $request->getUrl(), $request->getPayload());
        return new WorldPayResponse($response);
    }
}
