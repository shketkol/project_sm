<?php

namespace Modules\Payment\Notifications\Advertiser;

use App\Helpers\HtmlHelper;
use App\Notifications\Traits\AdvertiserNotification;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Notification\Models\NotificationCategory;
use Modules\Payment\Mail\Advertiser\TransactionIsMade as Mail;
use Modules\Payment\Notifications\OrderNotification;
use Modules\User\Models\User;

class TransactionIsMade extends OrderNotification
{
    use AdvertiserNotification;

    /**
     * @var array
     */
    private $orderData;

    /**
     * @param Campaign $campaign
     * @param array    $orderData
     */
    public function __construct(Campaign $campaign, array $orderData)
    {
        $this->orderData = $orderData;
        parent::__construct($campaign);
    }

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'campaignName'    => $this->campaign->name,
            'campaignDetails' => $this->getDetailsUrl(),
            'paymentMethod'   => Arr::get($this->orderData, 'paymentMethodName'),
            'firstName'       => $notifiable->first_name,
            'titleIcon'       => 'cash',
            'title'           => __('payment::emails.advertiser.transaction_is_made.title'),
        ];
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_TRANSACTION;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        /** @var HtmlHelper $html */
        $html = app(HtmlHelper::class);

        return __('payment::notifications.advertiser.transaction_is_made', [
            'payment_method' => e(Arr::get($data, 'paymentMethod')),
            'campaign_name'  => $html->createAnchorElement(Arr::get($data, 'campaignDetails'), [
                'title' => Arr::get($data, 'campaignName'),
            ]),
        ]);
    }
}
