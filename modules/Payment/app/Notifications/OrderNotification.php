<?php

namespace Modules\Payment\Notifications;

use App\Notifications\Notification;
use Modules\Campaign\Models\Campaign;
use Modules\Notification\Models\NotificationCategory;

abstract class OrderNotification extends Notification
{
    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_CAMPAIGN;
    }

    /**
     * @return string
     */
    public function getDetailsUrl(): string
    {
        return route('users.profile.transactions', ['orderId' => $this->campaign->order_id]);
    }
}
