<?php

namespace Modules\Auth\Services\AuthService\Contracts;

use App\Exceptions\Contracts\UnauthorizedException;
use Modules\User\Models\User;

interface AuthService
{
    /**
     * @param int|null $userId
     *
     * @return null|string
     */
    public function getToken(?int $userId): ?string;

    /**
     * @param int|null $userId
     *
     * @return bool
     */
    public function checkToken(?int $userId): bool;

    /**
     * @param int|null $userId
     *
     * @return null|string
     */
    public function getRefreshToken(?int $userId): ?string;

    /**
     * @param int   $userId
     * @param array $tokens
     *
     * @return void
     */
    public function setTokens(int $userId, array $tokens): void;

    /**
     * @return void
     */
    public function setTokenSpoiled(): void;

    /**
     * @return bool
     */
    public function isTokenSpoiled(): bool;

    /**
     * @param int $userId
     *
     * @return void
     */
    public function flushTokens(int $userId): void;

    /**
     * @param User $user
     *
     * @return void
     * @throws UnauthorizedException
     */
    public function logout(User $user): void;
}
