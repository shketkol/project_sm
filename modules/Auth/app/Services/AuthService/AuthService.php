<?php

namespace Modules\Auth\Services\AuthService;

use Modules\Auth\Drivers\Contracts\TokenCacheDriver;
use Modules\Auth\Services\AuthService\Contracts\AuthService as AuthServiceContract;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

abstract class AuthService implements AuthServiceContract
{
    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * In case if token is missed or expired we set this flag to true.
     * @var bool
     */
    private $tokenSpoiled = false;

    /**
     * @var TokenCacheDriver
     */
    protected $tokenCacheDriver;

    public function __construct()
    {
        $this->setDependencies();
    }

    /**
     * @return void
     */
    protected function setDependencies(): void
    {
        $this->tokenCacheDriver = app(TokenCacheDriver::class);
        $this->log = app(LoggerInterface::class);
    }

    /**
     * @param int|null $userId
     *
     * @return null|string
     */
    public function getToken(?int $userId): ?string
    {
        if (!$userId) {
            return null;
        }

        return $this->tokenCacheDriver->getAccessToken($userId);
    }

    /**
     * @param int|null $userId
     *
     * @return bool
     */
    public function checkToken(?int $userId): bool
    {
        return (bool) $this->getToken($userId);
    }

    /**
     * @param int|null $userId
     *
     * @return null|string
     */
    public function getRefreshToken(?int $userId): ?string
    {
        if (!$userId) {
            return null;
        }

        return $this->tokenCacheDriver->getRefreshToken($userId);
    }

    /**
     * @param int   $userId
     * @param array $tokens
     *
     * @return void
     */
    public function setTokens(int $userId, array $tokens): void
    {
        $this->tokenCacheDriver->setTokens($userId, $tokens);
    }

    /**
     * @return void
     */
    public function setTokenSpoiled(): void
    {
        $this->tokenSpoiled = true;
    }

    /**
     * @return bool
     */
    public function isTokenSpoiled(): bool
    {
        return $this->tokenSpoiled;
    }

    /**
     * @param int $userId
     *
     * @return void
     */
    public function flushTokens(int $userId): void
    {
        $this->tokenCacheDriver->unset($userId);
    }

    /**
     * @param User $user
     *
     * @return void
     */
    abstract public function logout(User $user): void;
}
