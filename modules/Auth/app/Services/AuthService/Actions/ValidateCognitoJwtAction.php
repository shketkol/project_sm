<?php

namespace Modules\Auth\Services\AuthService\Actions;

use App\Helpers\Json;
use Firebase\JWT\JWK;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Auth\Drivers\CognitoJwkCacheDriver;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class ValidateCognitoJwtAction
{
    /**
     * Curl timeout (in seconds).
     */
    protected const REQUEST_TIMEOUT = 3;

    /**
     * Allowed algorithms for public keys.
     */
    protected const ALLOWED_ALGORITHMS = ['RS256'];

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var CognitoJwkCacheDriver
     */
    protected $cache;

    /**
     * @param LoggerInterface       $log
     * @param CognitoJwkCacheDriver $cache
     */
    public function __construct(LoggerInterface $log, CognitoJwkCacheDriver $cache)
    {
        $this->log = $log;
        $this->cache = $cache;
    }

    /**
     * @return string
     */
    public static function formJwksUri(): string
    {
        return sprintf(
            'https://cognito-idp.%s.amazonaws.com/%s/.well-known/jwks.json',
            config('cognito.region'),
            config('cognito.user_pool_id')
        );
    }

    /**
     * @param string $jwt
     *
     * @return bool
     */
    public function handle(string $jwt): bool
    {
        if (!$this->isValidJwtStructure($jwt)) {
            $this->log->info('[ValidateJwt] Invalid JWT-structure', ['jwt' => $jwt]);
            return false;
        }

        $jwks = $this->getJwks($jwt);
        if (!$jwks) {
            $this->log->info('[ValidateJwt] JWK-set cannot be obtained');
            return false;
        }

        // Get public key (as a resource).
        try {
            $jwks = JWK::parseKeySet($jwks);
        } catch (\Throwable $e) {
            $this->logTechException('Failed to parse JWKS', $e);
            return false;
        }

        // The token should be "decodable" if it has valid value.
        try {
            JWT::decode($jwt, $jwks, self::ALLOWED_ALGORITHMS);
            return true;
        } catch (\Throwable $e) {
            $this->logTechException('Failed to decode JWT', $e);
            return false;
        }
    }

    /**
     * @param string $jwt
     *
     * @return array|null
     */
    protected function getJwks(string $jwt): ?array
    {
        $kid = $this->getKid($jwt);
        if (!$kid) {
            return null;
        }

        $jwks = $this->cache->getJwk($kid);
        if ($jwks) {
            return $jwks;
        }

        $jwks = $this->fetchJwks();
        if (!$jwks) {
            return null;
        }

        $jwk = Arr::first(Arr::get($jwks, 'keys'), function ($jwk) use ($kid) {
            return Arr::get($jwk, 'kid') === $kid;
        });

        if ($jwk) {
            $jwks = ['keys' => [$jwk]];
            try {
                // Check that we are saving correct jwks.
                JWK::parseKeySet($jwks);
                $this->cache->setJwk($kid, $jwks);
                return $jwks;
            } catch (\Throwable $e) {
                $this->logTechException('Failed to parse jwks', $e);
            }
        }

        return null;
    }

    /**
     * Get jwk-set of the available keys.
     *
     * @return array|null
     */
    protected function fetchJwks(): ?array
    {
        try {
            $response = $this->getClient()->send($this->makeRequest(), $this->getRequestOptions());
            $jwks = $response->getBody()->getContents();
        } catch (\Throwable $e) {
            $this->log->error('[ValidateJwt] Failed to fetch jwks.', ['message' => $e->getMessage()]);
            return null;
        }

        if (!$this->checkResponse($response, $jwks)) {
            return null;
        }

        try {
            $jwks = Json::decode($jwks);
        } catch (\Throwable $e) {
            $this->logTechException('Failed to decode jwks', $e);
            return null;
        }

        return $jwks;
    }

    /**
     * @param resource $handle
     * @param string   $responseBody
     *
     * @return bool
     */
    protected function checkResponse(ResponseInterface $handle, string $responseBody): bool
    {
        $responseCode = $handle->getStatusCode();

        if ($responseCode !== Response::HTTP_OK) {
            $this->log->error('[ValidateJwt] Failed to get jwks due to bad response.', [
                'response_code' => $responseCode,
                'response_body' => $responseBody,
            ]);
            return false;
        }

        return true;
    }

    /**
     * @param string $jwt
     *
     * @return null|string
     */
    protected function getKid(string $jwt): ?string
    {
        $tks = explode('.', $jwt);

        try {
            $header = JWT::jsonDecode(JWT::urlsafeB64Decode($tks[0]));
            if (isset($header->kid)) {
                return $header->kid;
            }
        } catch (\Exception $e) {
            $this->logTechException('Failed to get kid', $e);
        }

        return null;
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    protected function isValidJwtStructure(string $token): bool
    {
        return substr_count($token, '.') === 2;
    }

    /**
     * @return Request
     */
    private function makeRequest(): Request
    {
        return new Request(
            \Illuminate\Http\Request::METHOD_GET,
            self::formJwksUri(),
            ['Content-Type' => 'application/json']
        );
    }

    /**
     * @return Client
     */
    private function getClient(): Client
    {
        return app(Client::class);
    }

    /**
     * @return array
     */
    private function getRequestOptions(): array
    {
        return ['timeout' => self::REQUEST_TIMEOUT, 'connect_timeout' => self::REQUEST_TIMEOUT];
    }

    /**
     * @param string     $message
     * @param \Throwable $exception
     *
     * @return void
     */
    protected function logTechException(string $message, \Throwable $exception): void
    {
        $this->log->warning($message, [
            'exception_type' => get_class($exception),
            'exception_message' => $exception->getMessage(),
        ]);
    }
}
