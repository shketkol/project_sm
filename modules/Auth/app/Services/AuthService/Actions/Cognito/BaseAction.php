<?php

namespace Modules\Auth\Services\AuthService\Actions\Cognito;

use Modules\Auth\HttpClient\CognitoClient;
use Psr\Log\LoggerInterface;

class BaseAction
{
    /**
     * @var CognitoClient
     */
    protected $client;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @param CognitoClient   $client
     * @param LoggerInterface $log
     */
    public function __construct(
        CognitoClient $client,
        LoggerInterface $log
    ) {
        $this->client = $client;
        $this->log = $log;
    }
}
