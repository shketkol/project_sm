<?php

namespace Modules\Auth\HttpClient\Traits;

use Aws\Result;
use Modules\Auth\HttpClient\CognitoClient;

/**
 * @mixin CognitoClient
 */
trait RefreshToken
{
    /**
     * Get access-token using refresh token.
     *
     * @param string $refreshToken
     *
     * @return array
     * @throws \Exception
     */
    public function refreshToken(string $refreshToken): array
    {
        $this->log->info('[COGNITO] Refresh token operation - STARTED');

        /** @var $result Result */
        $result = $this->client->initiateAuth([
            'AuthFlow'       => 'REFRESH_TOKEN_AUTH',
            'UserPoolId'     => $this->poolId,
            'ClientId'       => $this->clientId,
            'AuthParameters' => [
                'REFRESH_TOKEN' => $refreshToken,
            ],
        ]);

        $this->log->info('[COGNITO] Refresh token operation - FINISHED');

        return $result->get('AuthenticationResult');
    }
}
