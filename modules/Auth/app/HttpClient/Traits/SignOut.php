<?php

namespace Modules\Auth\HttpClient\Traits;

use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Modules\Auth\HttpClient\CognitoClient;

/**
 * @mixin CognitoClient
 */
trait SignOut
{
    /**
     * Sign out user from cognito.
     *
     * @param string $token
     *
     * @return void
     * @throws CognitoIdentityProviderException
     */
    public function signOut(string $token): void
    {
        $this->log->info('[COGNITO] Sign out user operation - STARTED');

        $this->client->globalSignOut([
            'AccessToken' => $token,
        ]);

        $this->log->info('[COGNITO] Sign out user operation - FINISHED');
    }
}
