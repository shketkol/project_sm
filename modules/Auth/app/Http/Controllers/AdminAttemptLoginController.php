<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Guards\CognitoGuard;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Auth\Traits\AdminAuthHelper;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class AdminAttemptLoginController extends Controller
{
    use AdminAuthHelper,
        AuthenticatesUsers {
        logout as public doLogout;
    }

    /**
     * @param Request     $request
     * @param AuthService $authService
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws UnauthorizedException
     */
    public function show(Request $request, AuthService $authService)
    {
        /** @var CognitoGuard $adminGuard */
        /** @var User $user */
        $guard = Auth::guard('web');
        $user = $guard->user();

        try {
            $authService->logout($user);
        } catch (UnauthorizedException $exception) {
            throw $exception;
        } catch (\Throwable $exception) {
            app(LoggerInterface::class)->logException($exception);
        }

        $this->doLogout($request);

        return redirect($this->getAdminLoginRedirectUrl());
    }
}
