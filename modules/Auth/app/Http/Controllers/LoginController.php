<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    /**
     * Show login page
     *
     * @return Response
     */
    public function __invoke(): Response
    {
        return response()
            ->view('auth::login.form')
            // Do not store any cache, otherwise there is a chance for a user to get to the login page
            // when he/she is already logged in.
            ->header('Cache-Control', 'no-store');
    }
}
