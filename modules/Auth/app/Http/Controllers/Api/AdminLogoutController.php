<?php

namespace Modules\Auth\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Auth\Guards\CognitoGuard;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Auth\Traits\AdminAuthHelper;
use Illuminate\Support\Facades\Auth;
use Psr\Log\LoggerInterface;

class AdminLogoutController extends Controller
{
    use AdminAuthHelper;

    /**
     * @param AuthService     $authService
     * @param LoggerInterface $log
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Modules\Auth\Exceptions\UnauthorizedException
     * @throws \Throwable
     */
    public function __invoke(AuthService $authService, LoggerInterface $log)
    {
        /** @var CognitoGuard $adminGuard */
        $adminGuard = Auth::guard('admin');
        $admin = $adminGuard->user();

        if (!$admin) {
            $log->info('Admin is already logged out');
            return response()->json([
                'redirect' => route('login.form'),
            ]);
        }

        try {
            $authService->logout($admin);
        } catch (\Throwable $exception) {
            // Logout user from the application anyway.
            $adminGuard->logout();
            $log->info('Admin logged out from application (with errors)', ['reason' => $exception->getMessage()]);
            throw $exception;
        }

        $adminGuard->logout();
        $log->info('Admin successfully logged out from application');

        return response()->json([
            'redirect' => $this->getAdminLogoutRedirectUrl(),
        ]);
    }
}
