<?php

namespace Modules\Auth\Http\Controllers\Api;

use App\Exceptions\Contracts\UnauthorizedException;
use App\Helpers\EnvironmentHelper;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Auth\Traits\AdminAuthHelper;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class LogoutController extends Controller
{
    use AdminAuthHelper;
    use AuthenticatesUsers {
        logout as public doLogout;
    }

    /**
     * @param Request     $request
     * @param AuthService $authService
     *
     * @return JsonResponse
     * @throws UnauthorizedException
     */
    public function __invoke(Request $request, AuthService $authService): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        if (is_null($user)) {
            return $this->redirect();
        }

        try {
            $authService->logout($user);
        } catch (UnauthorizedException $exception) {
            throw $exception;
        } catch (\Throwable $exception) {
            app(LoggerInterface::class)->logException($exception);
        }

        $this->doLogout($request);

        return $this->redirect();
    }

    /**
     * @return JsonResponse
     */
    private function redirect(): JsonResponse
    {
        return response()->json([
            'redirect' => EnvironmentHelper::isMockedLandingEnv() ? route('landing') : config('auth.redirect.url'),
        ]);
    }
}
