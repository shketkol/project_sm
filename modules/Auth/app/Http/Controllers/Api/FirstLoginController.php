<?php

namespace Modules\Auth\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Auth\Traits\AuthenticatesUsers;
use Modules\User\Http\Requests\FirstLoginUserRequest;

class FirstLoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @param FirstLoginUserRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function __invoke(FirstLoginUserRequest $request): JsonResponse
    {
        return $this->login($request);
    }
}
