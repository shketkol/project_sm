<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Modules\Auth\Traits\AdminAuthHelper;
use Modules\Auth\Traits\AuthenticatesAdmin;

class AdminLoginController extends Controller
{
    use AuthenticatesAdmin, AdminAuthHelper;

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show(): RedirectResponse
    {
        return redirect($this->getAdminLoginRedirectUrl());
    }
}
