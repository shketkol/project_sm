<?php

namespace Modules\Auth\Http\Controllers;

class AdminLogoutController
{
    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function __invoke()
    {
        return redirect(route('login.form'));
    }
}
