<?php

namespace Modules\Auth\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Auth\Providers\Contracts\AdminUserProvider as AdminUserProviderContract;
use Modules\Auth\Providers\Traits\UserProviderMethods;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Daapi\Actions\Cookie\SetTokenCookieAction;
use Modules\Daapi\DataTransferObjects\Types\UserDataPlain;
use Modules\Haapi\Actions\Admin\User\SelfDataGet;
use Modules\Haapi\Actions\Admin\User\AdminClientAuthorize;
use Modules\Haapi\Exceptions\HaapiConnectivityException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Mappers\Models\UserModelMapper;
use Modules\User\Exceptions\AdminNotFoundException;
use Modules\User\Exceptions\UserNotFoundException;
use Modules\User\Models\User;
use Modules\User\Repositories\Contracts\UserRepository;
use Psr\Log\LoggerInterface;
use Throwable;

abstract class AdminUserProvider implements AdminUserProviderContract
{
    use UserProviderMethods;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var AdminClientAuthorize
     */
    private $adminSignInAction;

    /**
     * @var SelfDataGet
     */
    private $adminGetAction;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var AuthService
     */
    protected $authService;

    /**
     * @var SetTokenCookieAction
     */
    protected $setTokenCookieAction;

    /**
     * @var string
     */
    protected $cookieToken;

    /**
     * AdminUserProvider constructor.
     */
    public function __construct()
    {
        $this->setDependencies();
    }

    protected function setDependencies(): void
    {
        $this->log                  = app(LoggerInterface::class);
        $this->databaseManager      = app(DatabaseManager::class);
        $this->adminSignInAction    = app(AdminClientAuthorize::class);
        $this->adminGetAction       = app(SelfDataGet::class);
        $this->repository           = app(UserRepository::class);
        $this->authService          = app(AuthService::class);
        $this->setTokenCookieAction = app(SetTokenCookieAction::class);
    }

    /**
     * Validate an admin against the given credentials.
     *
     * @param Authenticatable|User $user
     * @param array                $credentials
     *
     * @return bool
     * @throws HaapiConnectivityException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function validateCredentials(Authenticatable $user, array $credentials): bool
    {
        if ($user->isAdmin() && $user->isDeactivated()) {
            $this->log->warning('Admin is not allowed to login.', [
                'user_id'   => $user->id,
                'status_id' => $user->status_id,
            ]);

            return false;
        }

        try {
            if ($this->authService->checkToken($user->getKey())) {
                $this->log->info('Admin token is already stored. Skipping HAAPI login.');
                $this->setTokenCookie();
                return true;
            }

            $tokens = $this->adminSignInAction->handle(Arr::get($credentials, 'code'));
            $this->authService->setTokens($user->getKey(), $tokens);
            $this->setTokenCookie(Arr::get($tokens, 'token'));

            $this->log->info('Admin token stored successfully. ', ['user_id' => $user->id]);
        } catch (HaapiConnectivityException $exception) {
            $this->log->error('Fail admin client authorize (during validating credentials)');

            if (config('haapi.stubs.login')) {
                $this->log->warning('[HAAPI] Login has been skipped due to configuration.');

                return true;
            }
            throw $exception;
        } catch (HaapiException $exception) {
            return false;
        }

        return true;
    }

    /**
     * Retrieve an admin by the given credentials.
     *
     * @param array $credentials
     *
     * @return Authenticatable|null
     * @throws AdminNotFoundException
     * @throws Throwable
     */
    public function retrieveByCredentials(array $credentials): ?Authenticatable
    {
        $this->databaseManager->beginTransaction();

        try {
            $tokens    = $this->adminSignInAction->handle(Arr::get($credentials, 'code'));
            $adminData = $this->adminGetAction->handleByToken(Arr::get($tokens, 'token'));

            $user = $this->findAdmin($adminData);
            $this->updateName($user, $adminData);
            $this->updateRole($user, $adminData->userType);
            $user->save();
            $this->authService->setTokens($user->getKey(), $tokens);
            $this->cookieToken = Arr::get($tokens, 'token');
        } catch (Throwable $exception) {
            $this->log->error('Fail admin client authorize (during retrieving credentials)');

            $this->databaseManager->rollBack();
            $this->log->logException($exception);
            throw new AdminNotFoundException();
        }

        $this->databaseManager->commit();
        $this->log->info('Admin token stored successfully. ', ['user_id' => $user->id]);

        return $user;
    }

    /**
     * @param string|null $token
     *
     * @return void
     */
    protected function setTokenCookie(?string $token = null): void
    {
        $token = $token ?? $this->cookieToken;
        if ($token) {
            $this->setTokenCookieAction->handle($token);
        }
    }

    /**
     * @param UserDataPlain $adminData
     *
     * @return User
     * @throws Throwable
     */
    private function findAdmin(UserDataPlain $adminData): User
    {
        try {
            $user = $this->repository->getByExternalId($adminData->externalId);
            $this->log->info('Admin found in DB.', ['user_id' => $user->id]);
        } catch (UserNotFoundException $exception) {
            $user = $this->createAdmin($adminData);
        } catch (Throwable $exception) {
            throw $exception;
        }

        return $user;
    }

    /**
     * @param UserDataPlain $adminData
     *
     * @return User
     */
    private function createAdmin(UserDataPlain $adminData): User
    {
        $this->log->info('Admin not found in DB. Creating new entity...');

        $user = new User([
            'external_id' => $adminData->externalId,
            'status_id'   => User::getStatusIdByTransactionFlow($adminData->userStatus, 'user'),
        ]);

        $this->log->info('Admin stored successfully.', ['user_id' => $user->id]);

        return $user;
    }

    /**
     * @param User   $user
     * @param string $role
     */
    private function updateRole(User $user, string $role): void
    {
        $mappedRole = UserModelMapper::findRole($role);

        $this->log->info('Updating admin role.', [
            'user_id'     => $user->id,
            'user_roles'  => $user->roles->pluck('id')->toArray(),
            'role_haapi'  => $role,
            'role_mapped' => $mappedRole,
        ]);

        $user->syncRoles([$mappedRole]);
    }

    /**
     * @param User          $user
     * @param UserDataPlain $adminData
     */
    private function updateName(User $user, UserDataPlain $adminData): void
    {
        $this->log->info('Updating admin full name.', [
            'user_id' => $user->id,
        ]);

        $user->account_name = $this->extractBusinessName($adminData);
    }

    /**
     * @param UserDataPlain $adminData
     *
     * @return string
     */
    private function extractBusinessName(UserDataPlain $adminData): string
    {
        return Arr::first(explode('@', $adminData->email));
    }
}
