<?php

namespace Modules\Auth\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider as UserProviderContract;
use Modules\Auth\Providers\Traits\UserProviderMethods;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Daapi\Actions\Cookie\SetTokenCookieAction;
use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Modules\Haapi\Actions\Admin\User\SelfDataGet;
use Modules\User\Actions\CreateUserAction;
use Modules\User\Exceptions\UserNotFoundException;
use Psr\Log\LoggerInterface;
use Throwable;

abstract class UserProvider implements UserProviderContract
{
    use UserProviderMethods;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var SelfDataGet
     */
    protected $userGet;

    /**
     * @var CreateUserAction
     */
    protected $createUserAction;

    /**
     * @var SetTokenCookieAction
     */
    protected $setTokenCookieAction;

    /**
     * @var AuthService
     */
    protected $authService;

    /**
     * CommonUserProvider constructor.
     */
    public function __construct()
    {
        $this->setDependencies();
    }

    /**
     * @return void
     */
    protected function setDependencies(): void
    {
        $this->log = app(LoggerInterface::class);
        $this->userGet = app(SelfDataGet::class);
        $this->createUserAction = app(CreateUserAction::class);
        $this->setTokenCookieAction = app(SetTokenCookieAction::class);
        $this->authService = app(AuthService::class);
    }

    /**
     * @param Authenticatable $user
     * @param array           $credentials
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function validateCredentials(Authenticatable $user, array $credentials): bool
    {
        if ($user->isAdmin()) {
            $this->log->warning('User not allowed to login.', [
                'user_id'   => $user->id,
                'status_id' => $user->status_id,
            ]);
            return false;
        }

        return true;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param array $credentials
     *
     * @return Authenticatable|null
     * @throws UserNotFoundException
     */
    public function retrieveByCredentials(array $credentials)
    {
        try {
            $sessionToken = $this->getSessionToken($credentials);
            $userData = $this->userGet->handleByToken($sessionToken);
            $accountData = new AccountData($userData->company);
            $user = $this->createUserAction->handle($accountData->externalId, $userData, $accountData);
        } catch (Throwable $exception) {
            $this->log->logException($exception, true);
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * @param array $credentials
     *
     * @return string
     */
    abstract protected function getSessionToken(array $credentials): string;
}
