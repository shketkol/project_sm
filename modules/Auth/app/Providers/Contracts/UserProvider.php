<?php

namespace Modules\Auth\Providers\Contracts;

use Illuminate\Contracts\Auth\UserProvider as UserProviderContract;

interface UserProvider extends UserProviderContract
{
}
