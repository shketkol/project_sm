<?php

namespace Modules\Auth\Providers\Contracts;

use Illuminate\Contracts\Auth\UserProvider;

interface AdminUserProvider extends UserProvider
{
}
