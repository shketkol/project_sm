<?php

namespace Modules\Auth\Exceptions;

use App\Exceptions\Contracts\UnauthorizedException as UnauthorizedExceptionContract;
use App\Exceptions\BaseException;

class UnauthorizedException extends BaseException implements UnauthorizedExceptionContract
{
}
