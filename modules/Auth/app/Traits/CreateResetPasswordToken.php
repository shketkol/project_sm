<?php

namespace Modules\Auth\Traits;

use Illuminate\Contracts\Auth\CanResetPassword;

trait CreateResetPasswordToken
{
    /**
     * Create reset password token for user.
     *
     * @param \Illuminate\Contracts\Auth\CanResetPassword $user
     *
     * @return string
     */
    private function createForgotPasswordToken(CanResetPassword $user): string
    {
        /** @var \Illuminate\Auth\Passwords\PasswordBroker $broker */
        $broker = app('auth.password.broker');

        return $broker->createToken($user);
    }
}
