<?php

namespace Modules\Auth;

use App\Providers\ModuleServiceProvider;
use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Drivers\Contracts\TechTokenCacheDriver;
use Modules\Auth\Drivers\Contracts\TokenCacheDriver;
use Modules\Auth\Drivers\CognitoTokenCacheDriver;
use Modules\Auth\Drivers\TechCognitoTokenCacheDriver;
use Modules\Auth\Guards\CognitoGuard;
use Modules\Auth\HttpClient\CognitoClient;
use Modules\Auth\Providers\AdminCognitoUserProvider;
use Modules\Auth\Providers\CognitoUserProvider;
use Modules\Auth\Providers\Contracts\AdminUserProvider;
use Modules\Auth\Providers\Contracts\UserProvider;
use Modules\Auth\Services\AuthService\CognitoAuthService;
use Modules\Auth\Services\AuthService\Contracts\AuthService;

class AuthServiceProvider extends ModuleServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [
        AuthService::class => CognitoAuthService::class,
        TokenCacheDriver::class => CognitoTokenCacheDriver::class,
        TechTokenCacheDriver::class => TechCognitoTokenCacheDriver::class,
        UserProvider::class => CognitoUserProvider::class,
        AdminUserProvider::class => AdminCognitoUserProvider::class,
    ];

    /**
     * @var bool
     */
    protected $pluralRoutePrefix = false;

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadRoutes();
        $this->loadConfigs(['cognito']);
        $this->registerBindings();
        $this->registerGuards();

        Auth::provider('haapi', function ($app, array $config) {
            return $app[UserProvider::class];
        });
        Auth::provider('admin_haapi', function ($app, array $config) {
            return $app[AdminUserProvider::class];
        });
    }

    /**
     * @return void
     */
    private function registerBindings(): void
    {
        $this->app->singleton(CognitoClient::class, function (Application $app): CognitoClient {
            $config = [
                'region'      => config('cognito.region'),
                'version'     => config('cognito.version'),
                'credentials' => false,
            ];

            return new CognitoClient(
                new CognitoIdentityProviderClient($config),
                config('cognito.user_pool_web_client_id'),
                config('cognito.user_pool_id')
            );
        });
    }

    /**
     * @return void
     */
    private function registerGuards(): void
    {
        $this->app['auth']->extend('cognito', function (Application $app, string $name, array $config): CognitoGuard {
            $guard = new CognitoGuard(
                $name,
                $app['auth']->createUserProvider($config['provider']),
                $app['session.store'],
                $app['request'],
                $app[AuthService::class]
            );

            $guard->setCookieJar($this->app['cookie']);
            $guard->setDispatcher($this->app['events']);
            $guard->setRequest($this->app->refresh('request', $guard, 'setRequest'));

            return $guard;
        });
    }

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'auth';
    }
}
