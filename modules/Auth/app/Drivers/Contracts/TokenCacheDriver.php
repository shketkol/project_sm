<?php

namespace Modules\Auth\Drivers\Contracts;

interface TokenCacheDriver
{
    /**
     * Set new token.
     *
     * @param int   $key
     * @param array $tokens
     *
     * @return void
     */
    public function setTokens(int $key, array $tokens): void;

    /**
     * Remove existing tokens.
     *
     * @param mixed $key
     *
     * @return void
     */
    public function unset($key): void;

    /**
     * @param int $userId
     *
     * @return null|string
     */
    public function getAccessToken(int $userId): ?string;

    /**
     * @param int $userId
     *
     * @return null|string
     */
    public function getRefreshToken(int $userId): ?string;
}
