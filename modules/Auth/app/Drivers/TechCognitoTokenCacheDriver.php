<?php

namespace Modules\Auth\Drivers;

use Modules\Auth\Drivers\Contracts\TechTokenCacheDriver;

class TechCognitoTokenCacheDriver extends TokenCacheDriver implements TechTokenCacheDriver
{
    /**
     * @return string
     */
    protected function getKeyPrefix(): string
    {
        return 'admin_cognito_';
    }

    /**
     * @return int
     */
    protected function getTokenCacheLifeTime(): int
    {
        return (int) config('haap.haapi.admin_token_cache_lifetime');
    }
}
