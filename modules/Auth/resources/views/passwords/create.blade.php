@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <create-password-page
            token="{{ $token }}"
            :user-id="{{ $id }}"
        ></create-password-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
