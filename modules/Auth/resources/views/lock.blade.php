@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <lock-page></lock-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
