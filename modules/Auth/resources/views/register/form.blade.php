@extends('layouts.app')

@push('head')
    @include('recaptcha::scripts')
@endpush

@section('content')
    <div class="vue-app">
        <register-form-page></register-form-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
