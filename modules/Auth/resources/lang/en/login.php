<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used only for login page.
    |
    */

    'dont_have_an_account' => 'Don\'t have an account?',
    'enter_email_address'  => 'Enter your email address.',
    'enter_your_password'  => 'Enter your password.',
    'forgot_password'      => 'Forgot password?',
];
