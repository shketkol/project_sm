<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used only for register page.
    |
    */

    'create_your_account'     => 'Create your account',
    'feel_free_to_view'       => 'Please feel free to view our :terms_of_use',
    'success_message'         => 'We are currently validating and creating your account. This shouldn\'t take too long,
                                    and we will send you an email when this is complete.',
    'whats_your_address'      => 'What’s your business address?',
    'whats_your_company_name' => 'What’s the name of your business?',
    'whats_your_email'        => 'What’s your business email?',
    'whats_your_name'         => 'What’s your name?',
    'whats_your_phone'        => 'What’s your business phone number?',
    'your_invite_code'        => 'Your invite code',
    'business_name_note_p1'   => 'By creating an account, you represent that you have the authority to act on the business\' behalf.',
    'business_name_note_p2'   => 'For more details, please review the Hulu Ad Manager <a href=":link" target="_blank">Terms of Use.</a>',
];
