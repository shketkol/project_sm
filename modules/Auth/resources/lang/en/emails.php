<?php

return [
    'reset-password' => [
        'subject' => ':publisher_company_full_name New Password Request',
        'title'   => ':publisher_company_full_name New Password Request',
        'body'    => [
            'we_have_received_a_request'            => 'We’ve received a request to reset the password for your Hulu Self Service Ad Buying account.',
            'to_create_a_new_password'              => 'To create a new password, click the link below:',
            'if_you_received_this_message_in_error' => 'If you received this message in error please ignore this email.',
            'if_you_have_any_questions'             => 'If you have any questions, please contact us at (:support_email).',
        ],
    ],
];
