<?php

// Authentication Routes
Route::group(['middleware' => 'guest'], function () {
    // Login Routes
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('first-login', 'FirstLoginController')->name('first-login');

    // Registration Routes
    Route::post('register', 'RegisterController')->middleware('recaptcha')->name('register');
});

Route::middleware(['api'])->group(function () {
    Route::post('logout', 'LogoutController')->name('logout');
    Route::post('admin-logout', 'AdminLogoutController')->name('admin-logout');
});
