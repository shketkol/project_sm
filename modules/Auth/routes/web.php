<?php

// Authentication Routes
Route::group(['middleware' => 'guest'], function () {
    // Reset Password Routes
    Route::get('password/reset', 'ForgotPasswordController')->name('password.request');
});
