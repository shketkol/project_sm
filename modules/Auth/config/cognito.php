<?php

return [
    'admin'                   => [
        'okta_login_url'  => env('OKTA_LOGIN_URL', 'https://ads-stg2-haapi.auth.us-east-1.amazoncognito.com/login'),
        'okta_logout_url' => env('OKTA_LOGOUT_URL', 'https://ads-stg2-haapi.auth.us-east-1.amazoncognito.com/logout'),
        'login_params'    => [
            'response_type' => env('ADMIN_LOGIN_RESPONSE_TYPE', 'code'),
            'client_id'     => env('ADMIN_LOGIN_CLIENT_ID', '5mtv24h8t6kjjvvk3ed7ksb756'),
            'redirect_uri'  => env(
                'ADMIN_LOGIN_REDIRECT_URI',
                'https://hulu-uat.self-service.danads.com/admin-auth/sign-in'
            ),
            'scope'         => env('ADMIN_LOGIN_SCOPE', 'openid+profile+aws.cognito.signin.user.admin'),
        ],
        'logout_params'   => [
            'client_id'  => env('ADMIN_LOGIN_CLIENT_ID', '5mtv24h8t6kjjvvk3ed7ksb756'),
            'logout_uri' => env(
                'ADMIN_LOGOUT_REDIRECT_URI',
                'https://hulu-uat.self-service.danads.com/admin-auth/sign-out'
            ),
        ],
    ],
    'region'                  => env('COGNITO_REGION', 'us-east-1'),
    'version'                 => env('COGNITO_VERSION', 'latest'),
    'user_pool_id'            => env('COGNITO_USER_POOL_ID', 'us-east-1_XT0FWfu0J'),
    'user_pool_web_client_id' => env('COGNITO_USER_POOL_CLIENT_ID', '5mtv24h8t6kjjvvk3ed7ksb756'),
    'set_password_code'       => env('COGNITO_SET_PASSWORD_CODE', 'NEW_PASSWORD_REQUIRED'),
    'refresh_token_attempts'  => env('COGNITO_REFRESH_TOKEN_ATTEMPTS', 2),
    'jwk_cache_ttl'           => env('COGNITO_JWK_CACHE_TTL', 86400), // 24 hours in seconds
];
