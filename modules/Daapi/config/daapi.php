<?php

use Modules\Campaign\Models\CampaignStatus;
use Modules\Creative\Models\CreativeStatus;
use Modules\Payment\Models\OrderStatus;
use Modules\Payment\Models\TransactionStatus;
use Modules\User\Models\Role;
use Modules\User\Models\UserStatus;

return [
    'callbacks'        => [
        'account/signup'  => 'callbacks.account.create',
        'account/update'  => 'callbacks.account.update',
        'campaign/cancel' => 'callbacks.campaign.cancel',
        'campaign/create' => 'callbacks.campaign.create',
        'creative/update' => 'callbacks.creative.update',
    ],
    'oauth2_client_id' => env('OAUTH2_CLIENT_ID', 3),
    'notification'     => [
        'status' => [
            'campaigns' => \Modules\Campaign\Actions\Notifications\UpdateCampaignStatusAction::class,
            'accounts'  => \Modules\User\Actions\Notifications\UpdateUserStatusAction::class,
            'payments'  => \Modules\Payment\Actions\Notifications\ProcessOrderStatusAction::class,
            'creatives' => \Modules\Creative\Actions\Notifications\UpdateCreativeStatusAction::class,
        ],
    ],
    'campaign'         => [
        'productId'    => env('PRODUCT_ID', 'bb540ba2-b990-4c37-befd-c742bc6812d0'),
        'quantityType' => 'IMPRESSIONS',
        'costModel'    => 'CPM',
    ],
    'admin'            => [
        'email'    => env('ADMIN_EMAIL', 'dev.team@danads.se'),
        'password' => env('ADMIN_PASSWORD', 'GBeiUbDZnh0Cbi!'),
    ],
    'oauth'            => [
        'keys_path'      => env('OAUTH_KEYS_PATH', '/'),
        'default_secret' => env('OAUTH_DEFAULT_SECRET', 'JqzCyrNSw6PnYSkWQsNKGvArSkQqznldGHrYex3r'),
    ],
    'status_mapping'   => [
        'campaign'         => [
            'pending approval'   => CampaignStatus::PENDING_APPROVAL,
            'pending_approval'   => CampaignStatus::PENDING_APPROVAL,
            'ready'              => CampaignStatus::READY,
            'live'               => CampaignStatus::LIVE,
            'paused'             => CampaignStatus::PAUSED,
            'complete'           => CampaignStatus::COMPLETED,
            'completed'          => CampaignStatus::COMPLETED,
            'cancelled'          => CampaignStatus::CANCELED,
            'cancelling'         => CampaignStatus::CANCELED,
            'suspended'          => CampaignStatus::SUSPENDED,
            'create in progress' => CampaignStatus::PROCESSING,
        ],
        'user'             => [
            'create in progress'    => UserStatus::CREATE_IN_PROGRESS,
            'create_in_progress'    => UserStatus::CREATE_IN_PROGRESS,
            'pending manual review' => UserStatus::PENDING_MANUAL_REVIEW,
            'pending_manual_review' => UserStatus::PENDING_MANUAL_REVIEW,
            'active'                => UserStatus::ACTIVE,
            'inactive'              => UserStatus::INACTIVE,
            'inactive unpaid'       => UserStatus::INACTIVE_UNPAID,
            'inactive_unpaid'       => UserStatus::INACTIVE_UNPAID,
        ],
        'creative'         => [
            'submitted'        => CreativeStatus::PROCESSING,
            'processing'       => CreativeStatus::PROCESSING,
            'pending_approval' => CreativeStatus::PENDING_APPROVAL,
            'pending approval' => CreativeStatus::PENDING_APPROVAL,
            'under_review'     => CreativeStatus::PENDING_APPROVAL,
            'approved'         => CreativeStatus::APPROVED,
            'rejected'         => CreativeStatus::REJECTED,
        ],
        'campaign_payment' => [
            'pending' => OrderStatus::PENDING,
            'current' => OrderStatus::CURRENT,
            'failed'  => OrderStatus::FAILED,
            'settled' => OrderStatus::SETTLED,
        ],
        /** TODO: cleanup */
        'transaction'      => [
            'success'         => TransactionStatus::SUCCESS,
            'customer_failed' => TransactionStatus::CUSTOMER_FAILED,
            'customer failed' => TransactionStatus::CUSTOMER_FAILED,
            'failed'          => TransactionStatus::CUSTOMER_FAILED,
            'system error'    => TransactionStatus::SYSTEM_FAILED,
            'system_error'    => TransactionStatus::SYSTEM_FAILED,
            'system failed'   => TransactionStatus::SYSTEM_FAILED,
            'system_failed'   => TransactionStatus::SYSTEM_FAILED,
            'initiated'       => TransactionStatus::INITIATED,
        ],
    ],
    'roles_mapping'    => [
        'normal'          => Role::ADVERTISER,
        'advertiser'      => Role::ADVERTISER,
        'admin'           => Role::ADMIN,
        'admin read only' => Role::ADMIN_READ_ONLY,
        'admin_read_only' => Role::ADMIN_READ_ONLY,
        'admin_readonly'  => Role::ADMIN_READ_ONLY,
    ],
    'validation'       => [
        'validate_structure'     => env('VALIDATE_JSON_STRUCTURE', true),
        'validate_callbacks'     => env('VALIDATE_JSON_CALLBACKS', false),
        'validate_notifications' => env('VALIDATE_JSON_NOTIFICATIONS', false),
        'user_statuses'          => [
            'ACTIVE',
            'DEACTIVATED',
        ],
        'user_types'             => [
            'NORMAL',
            'ADMIN',
            'ADMIN_READONLY',
        ],
        'account_types'          => [
            'ADVERTISER',
            // 'AGENCY',
        ],
        'cost_models'            => [
            'CPM',
        ],
        'quantity_types'         => [
            'IMPRESSIONS',
        ],
        'payment_statuses'       => [
            'INITIATED',
            'FAILED',
            'ERROR',
            'SUCCESS',
        ],
    ],
    'maintenance'      => [
        /*
        |--------------------------------------------------------------------------
        | IP addresses or networks that allowed to access the application
        |--------------------------------------------------------------------------
        |
        | Example for .env file:
        | MAINTENANCE_ALLOW=127.0.0.1,192.168.0.0/16
        |
        */
        'allow' => env('MAINTENANCE_ALLOW')
            ? explode(',', trim(env('MAINTENANCE_ALLOW'), ','))
            : null,
    ],
    'cookies'          => [
        /*
        |--------------------------------------------------------------------------
        | AWS Cognito JWT
        |--------------------------------------------------------------------------
        |
        */
        'accessToken' => [
            /*
            |--------------------------------------------------------------------------
            | Cookie name
            |--------------------------------------------------------------------------
            |
            */
            'name'   => env('DAAPI_COOKIES_ACCESS_TOKEN_NAME', 'hamJWT'),

            /*
            |--------------------------------------------------------------------------
            | Cookie Lifetime
            |--------------------------------------------------------------------------
            |
            | Here you may specify the number of minutes that you wish the AWS JWT cookie
            | to be allowed to remain idle before it expires.
            |
            */
            'ttl'    => env('DAAPI_COOKIES_ACCESS_TOKEN_TTL', config('session.lifetime')),

            /*
            |--------------------------------------------------------------------------
            | Transfer only on https
            |--------------------------------------------------------------------------
            |
            */
            'secure' => env('DAAPI_COOKIES_ACCESS_TOKEN_SECURE', config('app.env') !== 'local'),
        ],
    ],
];
