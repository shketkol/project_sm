<?php
Route::group([
    'prefix'     => 'daapi',
    'namespace'  => 'Modules\\Daapi\\Http\\Controllers',
    'middleware' => ['json.response'],
], function () {
    Route::group(
        [
            'as'         => 'callbacks.',
            'prefix'     => 'callback',
            'namespace'  => 'Callbacks',
            'middleware' => ['logging', 'valid', 'signed'],
        ],
        function () {
            Route::group([
                'as'        => 'account.',
                'prefix'    => 'account',
                'namespace' => 'Account',
            ], function () {
                Route::post('create', [
                    'middleware' => 'validate.callback:account_create',
                    'as'         => 'create',
                    'uses'       => 'AccountController@create',
                ]);

                Route::post('update', [
                    'middleware' => 'validate.callback:account_update',
                    'as'         => 'update',
                    'uses'       => 'AccountController@update',
                ]);
            });

            Route::group(
                [
                    'as'        => 'campaign.',
                    'prefix'    => 'campaign',
                    'namespace' => 'Campaign',
                ],
                function () {
                    Route::post(
                        'cancel',
                        [
                            'middleware' => 'validate.callback:campaign_cancel',
                            'as'         => 'cancel',
                            'uses'       => 'CampaignCancelController',
                        ]
                    );

                    Route::post(
                        'create',
                        [
                            'middleware' => 'validate.callback:campaign_create',
                            'as'         => 'create',
                            'uses'       => 'CampaignCreateController',
                        ]
                    );
                }
            );

            Route::group(
                [
                    'as'        => 'creative.',
                    'prefix'    => 'creative',
                    'namespace' => 'Creative',
                ],
                function () {
                    Route::post(
                        'update',
                        [
                            'middleware' => 'validate.callback:creative_update',
                            'as'         => 'update',
                            'uses'       => 'CreativeUpdateController',
                        ]
                    );
                }
            );
        }
    );

    Route::post('login', [
        'as'         => 'login',
        'uses'       => 'LoginController',
        'middleware' => ['validate.structure'],
    ]);

    Route::group(
        [
            'as'         => 'notification.',
            'prefix'     => 'notification',
            'namespace'  => 'Notifications',
            'middleware' => ['client', 'validate.notification:status_notification'],
        ],
        function () {
            Route::post(
                'status',
                [
                    'as'   => 'status',
                    'uses' => 'StatusNotificationController',
                ]
            );
        }
    );

    Route::group(
        [
            'as'         => 'invitations.',
            'prefix'     => 'invitations',
            'namespace'  => 'Invitations',
            'middleware' => ['validate.structure', 'client', 'invitation.enabled'],
        ],
        function () {
            Route::post('send', [
                'as'   => 'send',
                'uses' => 'SendInvitationsController',
            ]);
        }
    );

    Route::group(
        [
            'as'         => 'maintenance.',
            'prefix'     => 'maintenance',
            'namespace'  => 'Maintenance',
            'middleware' => ['validate.structure', 'client'],
        ],
        function () {
            Route::post('', [
                'as'   => 'toggle',
                'uses' => 'MaintenanceController',
            ]);
        }
    );
});
