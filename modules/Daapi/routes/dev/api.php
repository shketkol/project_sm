<?php

Route::prefix('daapi/dev')->namespace("Modules\\Daapi\\Http\\Controllers")->group(function () {
    Route::group([
        'as'         => 'daapi.dev.',
        'middleware' => ['json.response', 'validate.structure', 'client'],
        'namespace'  => 'Dev',
    ], function () {
        Route::group([
            'as'        => 'reports.',
            'prefix'    => '/reports',
            'namespace' => 'Reports',
        ], function () {
            Route::group(['as' => 'schedule.', 'prefix' => '/schedule'], function () {
                Route::post('send', 'SendScheduleReportController')->name('send');
            });
            Route::group(['as' => 'missingAdsScheduled.', 'prefix' => '/missing-ads-scheduled'], function () {
                Route::post('send', 'SendMissingAdsScheduledReportController')->name('send');
            });
        });

        //Campaigns
        Route::group([
            'as' => 'campaigns.',
            'prefix' => '/campaigns',
            'namespace' => 'Campaigns',
        ], function () {
            Route::post('rollback-processing', 'RollbackProcessingController')
                ->name('rollbackProcessing');
        });

        //Notifications
        Route::group([
            'as' => 'notifications.',
            'prefix' => '/notifications',
            'namespace' => 'Notifications',
        ], function () {
            Route::post('payment-failed', 'SendPaymentFailedNotificationController')
                ->name('paymentFailed');
        });
    });
});
