<?php

namespace Modules\Daapi\Actions\Campaign;

use Illuminate\Log\Logger;
 use Modules\Campaign\Exceptions\CampaignNotFoundException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Daapi\Actions\BaseAction;
use Prettus\Repository\Exceptions\RepositoryException;

class CampaignRollbackProcessingAction extends BaseAction
{
    /**
     * @var CampaignRepository
     */
    protected $repository;

    /**
     * @param CampaignRepository $repository
     * @param Logger             $log
     */
    public function __construct(CampaignRepository $repository, Logger $log)
    {
        $this->repository = $repository;
        parent::__construct($log);
    }

    /**
     * @param int $campaignId
     *
     * @return Campaign
     * @throws CampaignNotFoundException
     * @throws RepositoryException
     */
    public function handle(int $campaignId): Campaign
    {
        $campaign = $this->repository->byId($campaignId);

        $campaign->applyInternalStatus(CampaignStatus::DRAFT);

        return $campaign;
    }
}
