<?php

namespace Modules\Daapi\Actions\Campaign;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\BaseAction;
use Modules\Daapi\Events\Campaign\CampaignCreated;
use Modules\Daapi\Http\Controllers\Traits\CallbackResponse;

class CampaignCreateAction extends BaseAction
{
    use CallbackResponse;

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function handle(Request $request): JsonResponse
    {
        $data = $request->all();
        $this->log->info('Campaign creating', [
            'campaign.sourceId'   => Arr::get($data, 'haapi.response.payload.campaign.sourceId'),
            'campaign.externalId' => Arr::get($data, 'haapi.response.payload.campaign.id'),
        ]);

        try {
            event(new CampaignCreated($request));
            $response = $this->successResponse($request);
        } catch (\Throwable $exception) {
            $this->log->warning('Error occurred while handle campaign create callback.', [
                'reason' => $exception->getMessage(),
            ]);

            $response = $this->exceptionResponse($request, $exception);
        }

        return $response;
    }
}
