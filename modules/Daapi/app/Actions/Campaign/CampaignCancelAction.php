<?php

namespace Modules\Daapi\Actions\Campaign;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\BaseAction;
use Modules\Daapi\Events\Campaign\CampaignCanceled;
use Modules\Daapi\Http\Controllers\Traits\CallbackResponse;

class CampaignCancelAction extends BaseAction
{
    use CallbackResponse;

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function handle(Request $request): JsonResponse
    {
        $data = $request->all();
        $this->log->info('Campaign cancelling', [
            'campaignId' => Arr::get($data, 'haapi.response.payload.campaignId'),
        ]);

        try {
            event(new CampaignCanceled($request));
            $response = $this->successResponse($request);
        } catch (\Throwable $exception) {
            $response = $this->exceptionResponse($request, $exception);
        }

        return $response;
    }
}
