<?php

namespace Modules\Daapi\Actions;

use Illuminate\Log\Logger;

/**
 * Class BaseAction
 *
 * @package Modules\Daapi\Actions
 */
abstract class BaseAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * BaseAction constructor.
     *
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }
}
