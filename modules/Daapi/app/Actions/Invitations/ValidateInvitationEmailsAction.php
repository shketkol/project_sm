<?php

namespace Modules\Daapi\Actions\Invitations;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class ValidateInvitationEmailsAction
{
    private const REASON_INVALID = 'INVALID';
    private const REASON_DUPLICATE = 'DUPLICATE';

    /**
     * @var ValidationRules
     */
    private $validationRules;

    /**
     * @param ValidationRules $validationRules
     */
    public function __construct(ValidationRules $validationRules)
    {
        $this->validationRules = $validationRules;
    }

    /**
     * @param array $emails
     *
     * @return array
     */
    public function handle(array $emails): array
    {
        $data = ['emails' => $emails];
        $validator = Validator::make($data, $this->getRules(), $this->getMessages());

        $emailsWithErrors = [];
        $errors = [];
        foreach ($validator->getMessageBag()->all() as $message) {
            [$key, $reason] = explode(' ', $message);
            $email = Arr::get($data, $key);

            $emailsWithErrors[] = $email;
            $errors[] = ['reason' => $reason, 'email' => $email];
        }

        return [
            'valid'  => array_values(array_diff($emails, $emailsWithErrors)),
            'errors' => $errors,
        ];
    }

    /**
     * @return array
     */
    private function getRules(): array
    {
        return [
            'emails.*' => $this->validationRules->only(
                'invitation.email',
                ['required', 'string', 'max', 'email', 'unique:invitations,email']
            ),
        ];
    }

    /**
     * @return string[]
     */
    private function getMessages(): array
    {
        return [
            'emails.*.unique' => ':attribute ' . self::REASON_DUPLICATE,
            'emails.*.email'  => ':attribute ' . self::REASON_INVALID,
        ];
    }
}
