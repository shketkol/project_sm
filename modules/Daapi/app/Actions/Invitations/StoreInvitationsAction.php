<?php

namespace Modules\Daapi\Actions\Invitations;

use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Invitation\Actions\GenerateInvitationCodeAction;
use Modules\Invitation\Actions\Traits\SendInvite;
use Modules\Invitation\Actions\Traits\StoreInvite;
use Modules\Invitation\Repositories\InvitationRepository;
use Modules\User\Models\AccountType;
use Modules\User\Models\User;

class StoreInvitationsAction
{
    use SendInvite,
        StoreInvite;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var InvitationRepository
     */
    protected $repository;

    /**
     * @var GenerateInvitationCodeAction
     */
    protected $generateCodeAction;

    /**
     * @param InvitationRepository         $repository
     * @param DatabaseManager              $databaseManager
     * @param Logger                       $log
     * @param GenerateInvitationCodeAction $generateCodeAction
     */
    public function __construct(
        InvitationRepository $repository,
        DatabaseManager $databaseManager,
        Logger $log,
        GenerateInvitationCodeAction $generateCodeAction
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
        $this->generateCodeAction = $generateCodeAction;
    }

    /**
     * @param array $data
     * @param User  $admin
     *
     * @return void
     * @throws \App\Exceptions\BaseException
     */
    public function handle(array $data, User $admin): void
    {
        $specialAdsCategory = Arr::get($data, 'special_ads_category', false);
        foreach (Arr::get($data, 'invitations', []) as $item) {
            $date = Arr::get($item, 'expiration_date');
            if (!is_null($date)) {
                $date = Carbon::parse($date)->utc();
            }

            $this->store([
                'email'                 => Arr::get($item, 'email'),
                'code'                  => Arr::get($item, 'code') ?? $this->generateCodeAction->handle(),
                'expired_at'            => $date,
                'identification_key'    => Arr::get($item, 'identification_key'),
                'account_type_category' => $specialAdsCategory === true
                    ? AccountType::ID_SPECIAL_ADS
                    : AccountType::ID_COMMON,
            ], $admin);
        }
    }
}
