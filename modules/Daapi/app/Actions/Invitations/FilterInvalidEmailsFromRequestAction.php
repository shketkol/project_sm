<?php

namespace Modules\Daapi\Actions\Invitations;

use Illuminate\Support\Arr;

class FilterInvalidEmailsFromRequestAction
{
    /**
     * @param array $request
     * @param array $validEmails
     *
     * @return array
     */
    public function handle(array $request, array $validEmails): array
    {
        $allInvitations = Arr::get($request, 'request.payload.invitations');
        $validInvitations = [];

        foreach ($allInvitations as $invitation) {
            $email = Arr::get($invitation, 'email');
            if (in_array($email, $validEmails)) {
                // ignore duplicates
                $validInvitations[$email] = $invitation;
            }
        }

        Arr::set($request, 'request.payload.invitations', array_values($validInvitations));

        return Arr::get($request, 'request.payload');
    }
}
