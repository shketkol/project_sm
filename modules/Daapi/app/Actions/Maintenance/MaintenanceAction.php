<?php

namespace Modules\Daapi\Actions\Maintenance;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Psr\Log\LoggerInterface;

class MaintenanceAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * @param array $data
     */
    public function handle(array $data): void
    {
        ((bool)Arr::get($data, 'maintenance_enabled')) ? $this->down($data) : $this->up();
    }

    /**
     * Enable Maintenance mode
     *
     * @param array $data
     */
    private function down(array $data): void
    {
        $this->log->info('Started enabling Maintenance mode.');

        Artisan::call('down', $this->getOptions($data));

        $this->log->info('Finished enabling Maintenance mode.');
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function getOptions(array $data): array
    {
        $options = [];

        $message = Arr::get($data, 'message');
        if ($message) {
            Arr::set($options, '--message', $message);
        }

        $allow = config('daapi.maintenance.allow');
        if (!empty($allow)) {
            Arr::set($options, '--allow', $allow);
        }

        $this->log->info('Options for enabling Maintenance mode.', [
            'options' => $options,
        ]);

        return $options;
    }

    /**
     * Disable Maintenance mode
     */
    private function up(): void
    {
        $this->log->info('Started disabling Maintenance mode.');

        Artisan::call('up');

        $this->log->info('Finished disabling Maintenance mode.');
    }
}
