<?php

namespace Modules\Daapi\Actions\Login;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Daapi\HttpClient\Builders\Contracts\RequestBuilder;
use Modules\Daapi\HttpClient\Builders\Contracts\ResponseBuilder;
use Modules\Daapi\HttpClient\Responses\DaapiResponse;

class LoginAction
{
    /**
     * @var RequestBuilder
     */
    private $requestBuilder;

    /**
     * @var ResponseBuilder
     */
    private $responseBuilder;

    /**
     * @var AccessTokenGenerator
     */
    private $accessTokenGenerator;

    /**
     * LoginAction constructor.
     *
     * @param RequestBuilder       $requestBuilder
     * @param ResponseBuilder      $responseBuilder
     * @param AccessTokenGenerator $accessTokenGenerator
     */
    public function __construct(
        RequestBuilder $requestBuilder,
        ResponseBuilder $responseBuilder,
        AccessTokenGenerator $accessTokenGenerator
    ) {
        $this->requestBuilder = $requestBuilder;
        $this->responseBuilder = $responseBuilder;
        $this->accessTokenGenerator = $accessTokenGenerator;
    }

    /**
     * @param Request $request
     * @return DaapiResponse
     */
    public function handle(Request $request): DaapiResponse
    {
        $daapiRequest = $this->requestBuilder->make($request->toArray());
        $accessToken = $this->accessTokenGenerator->handle($daapiRequest->get('sharedKey'));

        $payload = $accessToken ?
            ['authToken' => $accessToken] :
            ['reason' => 'Exception happened during authentication'];
        $statusCode = $accessToken ? Response::HTTP_OK : Response::HTTP_UNAUTHORIZED;

        return $this->responseBuilder->make(
            $daapiRequest,
            $statusCode,
            $payload
        );
    }
}
