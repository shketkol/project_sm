<?php

namespace Modules\Daapi\Actions\Login;

use Illuminate\Support\Arr;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Passport\TokenRepository;
use Lcobucci\JWT\Parser as JwtParser;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use Modules\Daapi\Helpers\DaapiConfig;
use Psr\Http\Message\ServerRequestInterface;
use Nyholm\Psr7\Response as Psr7Response;

class AccessTokenGenerator extends AccessTokenController
{
    /**
     * @var DaapiConfig
     */
    private $config;

    /**
     * @var ServerRequestInterface
     */
    private $serverRequest;

    /**
     * AccessTokenGenerator constructor.
     *
     * @param AuthorizationServer $server
     * @param TokenRepository $tokens
     * @param JwtParser $jwt
     * @param DaapiConfig $config
     * @param ServerRequestInterface $serverRequest
     */
    public function __construct(
        AuthorizationServer $server,
        TokenRepository $tokens,
        JwtParser $jwt,
        DaapiConfig $config,
        ServerRequestInterface $serverRequest
    ) {
        parent::__construct($server, $tokens, $jwt);
        $this->config = $config;
        $this->serverRequest = $serverRequest;
    }

    /**
     * @param ServerRequestInterface $serverRequest
     * @param string                 $preSharedKey
     *
     * @return ServerRequestInterface
     */
    private function createRequest(ServerRequestInterface $serverRequest, string $preSharedKey): ServerRequestInterface
    {
        return $serverRequest->withParsedBody([
            'grant_type' => 'client_credentials',
            'client_id' => $this->config->getOauth2ClientId(),
            'client_secret' => $preSharedKey
        ]);
    }

    /**
     * @param string $sharedKey
     *
     * @return bool|mixed
     */
    public function handle(string $sharedKey)
    {
        $serverRequest = $this->createRequest($this->serverRequest, $sharedKey);

        try {
            $response = $this->convertResponse(
                $this->server->respondToAccessTokenRequest(
                    $serverRequest,
                    new Psr7Response()
                )
            );
        } catch (OAuthServerException $e) {
            return false;
        }

        $responseArray =  json_decode($response->getContent(), true);

        return Arr::get($responseArray, 'access_token');
    }
}
