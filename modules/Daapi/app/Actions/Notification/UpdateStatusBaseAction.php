<?php

namespace Modules\Daapi\Actions\Notification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Log\Logger;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use Prettus\Repository\Eloquent\BaseRepository;

abstract class UpdateStatusBaseAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var BaseRepository
     */
    protected $repository;

    /**
     * @var Model|\App\Traits\State
     */
    protected $entity;

    /**
     * UpdateStatusBaseAction constructor.
     *
     * @param Logger         $log
     * @param BaseRepository $repository
     */
    public function __construct(Logger $log, BaseRepository $repository)
    {
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param string     $externalId
     * @param string     $status
     * @param array|null $additionalData
     */
    abstract public function handle(string $externalId, string $status, ?array $additionalData): void;

    /**
     * @param string $externalId
     * @param string $field
     *
     * @return Model|null
     */
    protected function getEntity(string $externalId, string $field = 'external_id'): ?Model
    {
        return $this->entity = $this->repository->findByField($field, $externalId)->first();
    }

    /**
     * @param string $status
     *
     * @return Model
     * @throws CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function applyState(string $status): Model
    {
        $this->log->info('Entity status update started.', [
            'entity_model' => get_class($this->entity),
            'entity_id'    => $this->entity->getKey(),
            'status_from'  => $this->entity->status->name,
            'status_to'    => $status,
        ]);

        $this->entity->applyHaapiStatus($status);

        $this->log->info('Entity status update finished.', [
            'entity_model' => get_class($this->entity),
            'entity_id'    => $this->entity->getKey(),
            'status_from'  => $this->entity->status->name,
            'status_to'    => $status,
        ]);

        return $this->entity;
    }
}
