<?php

namespace Modules\Daapi\Actions\Notification;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;
use Modules\Campaign\Models\Campaign;
use Modules\Daapi\Actions\BaseAction;
use Modules\Daapi\Exceptions\DaapiValidationErrorException;
use Modules\Notification\Models\AlertType;
use Modules\Notification\Models\FailedPaymentAlert;

class SendPaymentFailedNotificationAction extends BaseAction
{
    /**
     * @param array $data
     *
     * @return FailedPaymentAlert
     * @throws DaapiValidationErrorException
     */
    public function handle(array $data): FailedPaymentAlert
    {
        $campaignId = Arr::get($data, 'campaign_id');
        $campaign = Campaign::find($campaignId);

        $this->validate($campaign, Arr::get($data, 'account_id'));

        $alert = $campaign->alerts()->where('type_id', AlertType::ID_FAILED_PAYMENT)->first();
        $alert->event_time = Arr::get($data, 'event_time');
        $alert->save();

        Artisan::call("platform:test:alert:fail_payment {$alert->id}");

        return $alert;
    }

    /**
     * @param Campaign $campaign
     * @param string   $userExternalId
     *
     * @throws DaapiValidationErrorException
     */
    private function validate(Campaign $campaign, string $userExternalId): void
    {
        $validator = Validator::make([], []);

        if ($campaign->user->getExternalId() !== $userExternalId) {
            $validator->errors()->add('account_id', 'The advertiser doesn\'t connect to campaign');
            throw new DaapiValidationErrorException($validator);
        }

        $alert = $campaign->alerts()->where('type_id', AlertType::ID_FAILED_PAYMENT)->first();
        if (!$alert) {
            $validator->errors()->add('campaign_id', 'Campaign doesn\'t have payment failed notification');
            throw new DaapiValidationErrorException($validator);
        }
    }
}
