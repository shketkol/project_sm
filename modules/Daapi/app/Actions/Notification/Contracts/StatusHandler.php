<?php

namespace Modules\Daapi\Actions\Notification\Contracts;

use Illuminate\Http\Request;

interface StatusHandler
{
    /**
     * @param Request $request
     */
    public function handle(Request $request);
}
