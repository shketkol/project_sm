<?php

namespace Modules\Daapi\Actions\Notification;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\Notification\Contracts\StatusHandler as StatusHandlerInterface;
use Modules\Daapi\Events\Order\OrderNotificationEvent;
use Modules\Daapi\Events\StatusNotificationEvent;
use Modules\Daapi\HttpClient\Builders\DaapiRequestBuilder;
use Modules\Daapi\HttpClient\Requests\DaapiRequest;

class StatusHandler implements StatusHandlerInterface
{
    /**
     * @var DaapiRequestBuilder
     */
    private $daapiRequestBuilder;

    /**
     * StatusHandler constructor.
     *
     * @param DaapiRequestBuilder $daapiRequestBuilder
     */
    public function __construct(DaapiRequestBuilder $daapiRequestBuilder)
    {
        $this->daapiRequestBuilder = $daapiRequestBuilder;
    }

    /**
     * @param Request $request
     */
    public function handle(Request $request)
    {
        $daapiRequest = $this->daapiRequestBuilder->make($request->toArray());
        $events = $this->evaluateEvents($daapiRequest);
        foreach ($events as $event) {
            event($event);
        }
    }

    /**
     * @param DaapiRequest $daapiRequest
     *
     * @return array
     */
    private function evaluateEvents(DaapiRequest $daapiRequest): array
    {
        $events = [];

        $payload = $daapiRequest->getPayload();

        $accounts = Arr::get($payload, 'accounts');
        $orders = Arr::get($payload, 'orders');

        if ($accounts && $orders) {
            $events[] = new OrderNotificationEvent([
                'accounts' => $accounts,
                'orders' => $orders,
            ]);
            unset($payload['accounts']);
            unset($payload['orders']);
        }

        if ($payload) {
            $events[] = new StatusNotificationEvent($payload);
        }

        return $events;
    }
}
