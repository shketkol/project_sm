<?php

namespace Modules\Daapi\Actions\Notification\Responses;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Daapi\HttpClient\Builders\DaapiRequestBuilder;
use Modules\Daapi\HttpClient\Builders\DaapiResponseBuilder;

/**
 * @property int    $statusCode
 * @property string $statusMessageKey
 * @property string $statusMessage
 */
abstract class DaapiHttpResponse
{
    /**
     * @var DaapiRequestBuilder
     */
    private $daapiRequestBuilder;

    /**
     * @var DaapiResponseBuilder
     */
    private $daapiResponseBuilder;

    /**
     * @param DaapiRequestBuilder  $daapiRequestBuilder
     * @param DaapiResponseBuilder $daapiResponseBuilder
     */
    public function __construct(DaapiRequestBuilder $daapiRequestBuilder, DaapiResponseBuilder $daapiResponseBuilder)
    {
        $this->daapiRequestBuilder = $daapiRequestBuilder;
        $this->daapiResponseBuilder = $daapiResponseBuilder;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function get(Request $request): JsonResponse
    {
        $this->adjustStatusCode();

        return response()->json($this->getResponseArray($request->toArray()), $this->statusCode);
    }

    /**
     * Prevent throwing invalid HTTP code (e.g. SQL 42000 code which will lead to error)
     *
     * @return void
     */
    protected function adjustStatusCode()
    {
        if ($this->statusCode > Response::HTTP_NETWORK_AUTHENTICATION_REQUIRED ||
            $this->statusCode < Response::HTTP_CONTINUE
        ) {
            $this->statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    /**
     * @param array $requestArray
     *
     * @return array
     */
    protected function getResponseArray(array $requestArray): array
    {
        $daapiRequest = $this->daapiRequestBuilder->make($requestArray);

        return $this->daapiResponseBuilder->make($daapiRequest, $this->statusCode, [
            $this->statusMessageKey => $this->statusMessage,
        ])->toArray();
    }
}
