<?php

namespace Modules\Daapi\Actions\Notification\Responses;

use Modules\Daapi\Actions\Notification\Responses\Traits\WithoutPayload;

class StatusNotificationAcceptedResponse extends NotificationAcceptedResponse
{
    use WithoutPayload;
}
