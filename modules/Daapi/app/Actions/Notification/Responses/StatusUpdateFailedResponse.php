<?php

namespace Modules\Daapi\Actions\Notification\Responses;

class StatusUpdateFailedResponse extends DaapiHttpResponse
{
    /**
     * Response status code
     */
    public $statusCode = 500;

    /**
     * @var string
     */
    public $statusMessageKey = 'reason';

    /**
     * Response error reason
     */
    public $statusMessage = 'Failed updating status notifications.';
}
