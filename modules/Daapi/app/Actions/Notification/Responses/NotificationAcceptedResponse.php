<?php

namespace Modules\Daapi\Actions\Notification\Responses;

class NotificationAcceptedResponse extends DaapiHttpResponse
{
    /**
     * Response status code
     */
    public $statusCode = 200;

    /**
     * @var string
     */
    public $statusMessageKey = 'message';

    /**
     * Response message
     */
    public $statusMessage = 'The notification is successfully accepted';
}
