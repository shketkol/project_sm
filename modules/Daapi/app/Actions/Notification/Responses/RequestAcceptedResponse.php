<?php

namespace Modules\Daapi\Actions\Notification\Responses;

class RequestAcceptedResponse extends DaapiHttpResponse
{
    /**
     * Response status code
     */
    public $statusCode = 200;

    /**
     * @var string
     */
    public $statusMessageKey = 'message';

    /**
     * Response message
     */
    public $statusMessage = 'Request is successfully accepted';
}
