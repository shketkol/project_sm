<?php

namespace Modules\Daapi\Actions\Notification\Responses;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class ValidationResponse extends DaapiHttpResponse
{
    /**
     * Response status code
     */
    public $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * @var string
     */
    public $statusMessageKey = 'errors';

    /**
     * @param array $requestArray
     *
     * @return array
     */
    protected function getResponseArray(array $requestArray): array
    {
        $response = parent::getResponseArray($requestArray);
        Arr::set($response, 'response.payload', Arr::get($response, 'response.payload.errors'));

        return $response;
    }
}
