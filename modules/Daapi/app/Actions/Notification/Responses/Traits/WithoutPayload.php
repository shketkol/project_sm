<?php

namespace Modules\Daapi\Actions\Notification\Responses\Traits;

use Illuminate\Support\Arr;

trait WithoutPayload
{
    /**
     * @param array $requestArray
     *
     * @return array
     */
    protected function getResponseArray(array $requestArray): array
    {
        $parentResponseArray = parent::getResponseArray($requestArray);
        Arr::set($parentResponseArray, 'request.payload', []);

        return $parentResponseArray;
    }
}
