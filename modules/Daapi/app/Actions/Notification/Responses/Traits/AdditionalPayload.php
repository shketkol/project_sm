<?php

namespace Modules\Daapi\Actions\Notification\Responses\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

trait AdditionalPayload
{
    /**
     * @param Request $request
     * @param array   $additional
     *
     * @return JsonResponse
     */
    public function get(Request $request, array $additional = []): JsonResponse
    {
        $this->adjustStatusCode();
        $data = $this->getResponseArray($request->toArray());

        foreach ($additional as $key => $value) {
            Arr::set($data, $key, $value);
        }

        return response()->json($data, $this->statusCode);
    }
}
