<?php

namespace Modules\Daapi\Actions\Notification\Responses;

class UnprocessableEntityResponse extends DaapiHttpResponse
{
    /**
     * Response status code
     */
    public $statusCode = 422;

    /**
     * @var string
     */
    public $statusMessageKey = 'reason';

    /**
     * Response error reason
     */
    public $statusMessage = 'Unprocessable entity.';
}
