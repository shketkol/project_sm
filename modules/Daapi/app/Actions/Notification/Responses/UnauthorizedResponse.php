<?php

namespace Modules\Daapi\Actions\Notification\Responses;

class UnauthorizedResponse extends DaapiHttpResponse
{
    /**
     * Response status code
     */
    public $statusCode = 401;

    /**
     * @var string
     */
    public $statusMessageKey = 'reason';

    /**
     * Response error reason
     */
    public $statusMessage = 'There was a problem with authentication';
}
