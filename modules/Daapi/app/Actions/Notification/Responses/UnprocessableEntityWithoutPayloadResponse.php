<?php

namespace Modules\Daapi\Actions\Notification\Responses;

use Modules\Daapi\Actions\Notification\Responses\Traits\AdditionalPayload;
use Modules\Daapi\Actions\Notification\Responses\Traits\WithoutPayload;

class UnprocessableEntityWithoutPayloadResponse extends DaapiHttpResponse
{
    use WithoutPayload,
        AdditionalPayload;

    /**
     * Response status code
     */
    public $statusCode = 422;

    /**
     * @var string
     */
    public $statusMessageKey = 'message';

    /**
     * Response error reason
     */
    public $statusMessage = 'The given data was invalid.';
}
