<?php

namespace Modules\Daapi\Actions\Notification\Responses;

use Modules\Daapi\Actions\Notification\Responses\Traits\WithoutPayload;

class StatusUnauthorizedResponse extends UnauthorizedResponse
{
    use WithoutPayload;
}
