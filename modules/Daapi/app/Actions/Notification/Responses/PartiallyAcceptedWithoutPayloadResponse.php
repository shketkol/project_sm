<?php

namespace Modules\Daapi\Actions\Notification\Responses;

use Modules\Daapi\Actions\Notification\Responses\Traits\AdditionalPayload;
use Modules\Daapi\Actions\Notification\Responses\Traits\WithoutPayload;

class PartiallyAcceptedWithoutPayloadResponse extends DaapiHttpResponse
{
    use WithoutPayload,
        AdditionalPayload;

    /**
     * Response status code
     */
    public $statusCode = 202;

    /**
     * @var string
     */
    public $statusMessageKey = 'message';

    /**
     * Response message
     */
    public $statusMessage = 'Request is successfully accepted';
}
