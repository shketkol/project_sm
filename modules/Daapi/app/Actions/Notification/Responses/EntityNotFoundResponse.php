<?php

namespace Modules\Daapi\Actions\Notification\Responses;

class EntityNotFoundResponse extends DaapiHttpResponse
{
    /**
     * Response status code
     */
    public $statusCode = 404;

    /**
     * @var string
     */
    public $statusMessageKey = 'reason';

    /**
     * Response error reason
     */
    public $statusMessage = 'Entity not found';
}
