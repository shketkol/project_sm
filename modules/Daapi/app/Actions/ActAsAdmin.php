<?php

namespace Modules\Daapi\Actions;

use Modules\Auth\Drivers\Contracts\TechTokenCacheDriver;
use Modules\Haapi\Actions\User\TechAdminClientAuthorize;
use Modules\User\Repositories\UserRepository;

trait ActAsAdmin
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var TechTokenCacheDriver
     */
    private $adminTokenCache;

    /**
     * @var TechAdminClientAuthorize
     */
    protected $clientAuthorize;

    /**
     * Set up needed services
     */
    private function setUpServices(): void
    {
        $this->userRepository = app(UserRepository::class);
        $this->adminTokenCache = app(TechTokenCacheDriver::class);
        $this->clientAuthorize = app(TechAdminClientAuthorize::class);
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function authAsAdmin(): int
    {
        $this->setUpServices();

        $admin = $this->userRepository->getTechnicalAdmin();
        $cachedToken = $this->adminTokenCache->getAccessToken($admin->id);

        $token = $this->clientAuthorize->handle(
            config('daapi.admin.email'),
            config('daapi.admin.password'),
            $admin->id,
            $cachedToken
        );

        if ($cachedToken !== $token) {
            $this->adminTokenCache->setTokens($admin->id, ['token' => $token]);
        }

        return $admin->id;
    }
}
