<?php

namespace Modules\Daapi\Actions\Creative;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\BaseAction;
use Modules\Daapi\Events\Creative\CreativeUpdated;
use Exception;
use Modules\Daapi\Http\Controllers\Traits\CallbackResponse;

class CreativeUpdateAction extends BaseAction
{
    use CallbackResponse;

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function handle(Request $request): JsonResponse
    {
        $data = $request->all();
        $this->log->info('Creative updating', [
            'campaignId' => Arr::get($data, 'haapi.response.payload.id'),
            'accountId'  => Arr::get($data, 'haapi.response.payload.accountId'),
            'creativeId' => Arr::get($data, 'haapi.response.payload.creative.id'),
        ]);

        try {
            event(new CreativeUpdated($request));
            $response = $this->successResponse($request);
        } catch (\Throwable $exception) {
            $response = $this->exceptionResponse($request, $exception);
        }

        return $response;
    }
}
