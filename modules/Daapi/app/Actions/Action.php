<?php

namespace Modules\Daapi\Actions;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Interface Action
 *
 * @package Modules\Daapi\Actions
 */
interface Action
{
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function handle(Request $request): Response;
}
