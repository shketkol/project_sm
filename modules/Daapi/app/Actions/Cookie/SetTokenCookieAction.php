<?php

namespace Modules\Daapi\Actions\Cookie;

use Illuminate\Support\Facades\Cookie;

class SetTokenCookieAction
{
    /**
     * @param string $token
     */
    public function handle(string $token): void
    {
        // Expiration time is stored in AWS JWT and it's only 1 hour
        // And since app does not refresh the access token
        // We would use login access token longer then 1 hour
        // So stored in cookie JWT would expire in 1 hour
        $minutes = config('daapi.cookies.accessToken.ttl');
        $secure = config('daapi.cookies.accessToken.secure');
        $name = config('daapi.cookies.accessToken.name');
        $domain = '.' . parse_url(config('app.url'), PHP_URL_HOST);

        /** @see \Illuminate\Cookie\CookieJar::make() */
        Cookie::queue($name, $token, $minutes, null, $domain, $secure);
    }
}
