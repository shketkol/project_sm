<?php

namespace Modules\Daapi\Actions\Cookie;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;

class ForgetTokenCookieAction
{
    /**
     * @return void
     */
    public function handle(): void
    {
        $name = config('daapi.cookies.accessToken.name');

        if (Request::hasCookie($name)) {
            Cookie::queue(Cookie::forget($name));
        }
    }
}
