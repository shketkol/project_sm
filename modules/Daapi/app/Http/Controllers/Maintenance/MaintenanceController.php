<?php

namespace Modules\Daapi\Http\Controllers\Maintenance;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\Maintenance\MaintenanceAction;
use Modules\Daapi\Actions\Notification\Responses\RequestAcceptedResponse;
use Modules\Daapi\Http\Controllers\Controller;
use Modules\Daapi\Http\Controllers\Traits\LogRequest;
use Modules\Daapi\Http\Requests\MaintenanceRequest;

class MaintenanceController extends Controller
{
    use LogRequest;

    /**
     * @param MaintenanceRequest      $request
     * @param MaintenanceAction       $action
     * @param RequestAcceptedResponse $requestAcceptedResponse
     *
     * @return JsonResponse
     */
    public function __invoke(
        MaintenanceRequest $request,
        MaintenanceAction $action,
        RequestAcceptedResponse $requestAcceptedResponse
    ): JsonResponse {
        $this->log($request);

        $action->handle(Arr::get($request, 'request.payload'));

        return $requestAcceptedResponse->get($request);
    }
}
