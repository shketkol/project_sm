<?php

namespace Modules\Daapi\Http\Controllers\Dev\Reports;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\Notification\Responses\EntityNotFoundResponse;
use Modules\Daapi\Actions\Notification\Responses\RequestAcceptedResponse;
use Modules\Daapi\Actions\Notification\Responses\UnprocessableEntityResponse;
use Modules\Daapi\Http\Controllers\Traits\LogRequest;
use Modules\Report\Actions\IsActiveScheduledReportAction;
use Modules\Report\Http\Requests\SendScheduleReportRequest;
use Modules\Report\Jobs\ProcessReport;
use Modules\Report\Models\ReportType;
use Modules\Report\Repositories\ReportRepository;

class SendScheduleReportController
{
    use LogRequest;

    /**
     * @param SendScheduleReportRequest     $request
     * @param ReportRepository              $repository
     * @param IsActiveScheduledReportAction $isActiveScheduledReport
     * @param RequestAcceptedResponse       $requestAcceptedResponse
     * @param EntityNotFoundResponse        $entityNotFoundResponse
     * @param UnprocessableEntityResponse   $unprocessableEntityResponse
     *
     * @return JsonResponse
     * @throws \Modules\Report\Exceptions\ReportNotGeneratedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        SendScheduleReportRequest $request,
        ReportRepository $repository,
        IsActiveScheduledReportAction $isActiveScheduledReport,
        RequestAcceptedResponse $requestAcceptedResponse,
        EntityNotFoundResponse $entityNotFoundResponse,
        UnprocessableEntityResponse $unprocessableEntityResponse
    ): JsonResponse {
        $this->log($request);

        try {
            $reportId = Arr::get($request->toArray(), 'request.payload.report_id');
            $report = $repository->byType(ReportType::ID_SCHEDULED)->find($reportId);
        } catch (ModelNotFoundException $exception) {
            return $entityNotFoundResponse->get($request);
        }

        if (!$isActiveScheduledReport->handle($report)) {
            $message = 'Advertiser scheduled report with no live or recently completed campaigns would not be sent.';
            $unprocessableEntityResponse->statusMessage = $message;
            return $unprocessableEntityResponse->get($request);
        }

        ProcessReport::dispatch($report);

        return $requestAcceptedResponse->get($request);
    }
}
