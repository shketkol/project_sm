<?php

namespace Modules\Daapi\Http\Controllers\Dev\Reports;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\Notification\Responses\EntityNotFoundResponse;
use Modules\Daapi\Actions\Notification\Responses\RequestAcceptedResponse;
use Modules\Daapi\Http\Controllers\Traits\LogRequest;
use Modules\Report\Http\Requests\SendScheduleReportRequest;
use Modules\Report\Jobs\ProcessMissingAdsReport;
use Modules\Report\Models\ReportType;
use Modules\Report\Repositories\ReportRepository;

class SendMissingAdsScheduledReportController
{
    use LogRequest;

    /**
     * @param SendScheduleReportRequest     $request
     * @param ReportRepository              $repository
     * @param RequestAcceptedResponse       $requestAcceptedResponse
     * @param EntityNotFoundResponse        $entityNotFoundResponse
     *
     * @return Response
     * @throws \Modules\Report\Exceptions\ReportNotGeneratedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        SendScheduleReportRequest $request,
        ReportRepository $repository,
        RequestAcceptedResponse $requestAcceptedResponse,
        EntityNotFoundResponse $entityNotFoundResponse
    ): Response {
        $this->log($request);

        try {
            $reportId = Arr::get($request->toArray(), 'request.payload.report_id');
            $report = $repository->byType(ReportType::ID_MISSING_ADS_SCHEDULED)->find($reportId);
        } catch (ModelNotFoundException $exception) {
            return $entityNotFoundResponse->get($request);
        }

        ProcessMissingAdsReport::dispatch($report);

        return $requestAcceptedResponse->get($request);
    }
}
