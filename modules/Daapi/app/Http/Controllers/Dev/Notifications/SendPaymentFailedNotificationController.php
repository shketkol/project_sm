<?php

namespace Modules\Daapi\Http\Controllers\Dev\Notifications;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\Notification\Responses\RequestAcceptedResponse;
use Modules\Daapi\Actions\Notification\SendPaymentFailedNotificationAction;
use Modules\Daapi\Http\Controllers\Traits\LogRequest;
use Modules\Daapi\Http\Requests\SendPaymentFailedNotificationRequest;

class SendPaymentFailedNotificationController
{
    use LogRequest;

    /**
     * @param SendPaymentFailedNotificationRequest $request
     * @param SendPaymentFailedNotificationAction  $action
     * @param RequestAcceptedResponse              $requestAcceptedResponse
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Modules\Daapi\Exceptions\DaapiValidationErrorException
     */
    public function __invoke(
        SendPaymentFailedNotificationRequest $request,
        SendPaymentFailedNotificationAction $action,
        RequestAcceptedResponse $requestAcceptedResponse
    ): JsonResponse {
        $this->log($request);
        $action->handle(Arr::get($request->toArray(), 'request.payload'));

        return $requestAcceptedResponse->get($request);
    }
}
