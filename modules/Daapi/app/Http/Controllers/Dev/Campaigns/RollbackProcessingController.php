<?php

namespace Modules\Daapi\Http\Controllers\Dev\Campaigns;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\Campaign\CampaignRollbackProcessingAction;
use Modules\Daapi\Actions\Notification\Responses\RequestAcceptedResponse;
use Modules\Daapi\Http\Controllers\Traits\LogRequest;
use Modules\Daapi\Http\Requests\CampaignsRollbackProcessingRequest;

class RollbackProcessingController
{
    use LogRequest;

    /**
     * @param CampaignsRollbackProcessingRequest $request
     * @param CampaignRollbackProcessingAction $action
     * @param RequestAcceptedResponse $requestAcceptedResponse
     *
     * @return JsonResponse
     * @throws \Modules\Campaign\Exceptions\CampaignNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        CampaignsRollbackProcessingRequest $request,
        CampaignRollbackProcessingAction $action,
        RequestAcceptedResponse $requestAcceptedResponse
    ): JsonResponse {
        $this->log($request);
        $action->handle(Arr::get($request->toArray(), 'request.payload.campaign_id'));

        return $requestAcceptedResponse->get($request);
    }
}
