<?php

namespace Modules\Daapi\Http\Controllers\Callbacks\Creative;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Daapi\Actions\Creative\CreativeUpdateAction;
use Modules\Daapi\Http\Controllers\Controller;

class CreativeUpdateController extends Controller
{
    /**
     * Creative update
     *
     * @param Request              $request
     * @param CreativeUpdateAction $creativeUpdateAction
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function __invoke(Request $request, CreativeUpdateAction $creativeUpdateAction): JsonResponse
    {
        return $creativeUpdateAction->handle($request);
    }
}
