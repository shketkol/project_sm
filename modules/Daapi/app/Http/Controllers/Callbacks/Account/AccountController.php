<?php

namespace Modules\Daapi\Http\Controllers\Callbacks\Account;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\Daapi\Events\Account\AccountUpdated;
use Modules\Daapi\Events\User\UserApproved;
use Modules\Daapi\Http\Controllers\Traits\CallbackResponse;
use Modules\Haapi\Helpers\HaapiCheckResponse;

class AccountController
{
    use CallbackResponse;

    /**
     * User create
     *
     * @param Request $request
     *
     * @return JsonResponse|void
     */
    public function create(Request $request)
    {
        $approved = $request->json('haapi.response.statusCode') === HaapiCheckResponse::STATUS_CODE_OK;

        try {
            if ($approved) {
                event(new UserApproved($request));
            } else {
                Log::debug('User was not approved after sign up and no reject actions were done.', [
                    'request' => $request->json(),
                ]);
            }

            $response = $this->successResponse($request);
        } catch (\Throwable $exception) {
            $response = $this->exceptionResponse($request, $exception);
        }

        return $response;
    }

    /**
     * Account update
     *
     * @param Request $request
     *
     * @return JsonResponse|void
     */
    public function update(Request $request)
    {
        try {
            event(new AccountUpdated($request));

            $response = $this->successResponse($request);
        } catch (\Throwable $exception) {
            $response = $this->exceptionResponse($request, $exception);
        }

        return $response;
    }
}
