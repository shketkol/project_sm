<?php

namespace Modules\Daapi\Http\Controllers\Callbacks\Campaign;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Daapi\Actions\Campaign\CampaignCancelAction;
use Modules\Daapi\Http\Controllers\Controller;

class CampaignCancelController extends Controller
{
    /**
     * Campaign cancel
     *
     * @param Request              $request
     * @param CampaignCancelAction $campaignCancelAction
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function __invoke(Request $request, CampaignCancelAction $campaignCancelAction): JsonResponse
    {
        return $campaignCancelAction->handle($request);
    }
}
