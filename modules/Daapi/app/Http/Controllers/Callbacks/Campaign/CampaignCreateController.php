<?php

namespace Modules\Daapi\Http\Controllers\Callbacks\Campaign;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Daapi\Actions\Campaign\CampaignCreateAction;
use Modules\Daapi\Http\Controllers\Controller;

class CampaignCreateController extends Controller
{
    /**
     * Campaign create
     *
     * @param Request              $request
     * @param CampaignCreateAction $campaignCreateAction
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function __invoke(Request $request, CampaignCreateAction $campaignCreateAction): JsonResponse
    {
        return $campaignCreateAction->handle($request);
    }
}
