<?php

namespace Modules\Daapi\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Daapi\Actions\Login\LoginAction;
use Modules\Daapi\Http\Controllers\Traits\LogRequest;

class LoginController extends Controller
{
    use LogRequest;

    /**
     * @param Request                $request
     * @param LoginAction            $loginAction
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(
        Request $request,
        LoginAction $loginAction
    ) {
        $this->log($request);
        $daapiResponse = $loginAction->handle($request);

        return response()->json($daapiResponse->toArray(), $daapiResponse->getStatusCode());
    }
}
