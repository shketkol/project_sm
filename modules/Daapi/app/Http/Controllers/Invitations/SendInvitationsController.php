<?php

namespace Modules\Daapi\Http\Controllers\Invitations;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Modules\Daapi\Actions\Invitations\FilterInvalidEmailsFromRequestAction;
use Modules\Daapi\Actions\Invitations\StoreInvitationsAction;
use Modules\Daapi\Actions\Invitations\ValidateInvitationEmailsAction;
use Modules\Daapi\Actions\Notification\Responses\PartiallyAcceptedWithoutPayloadResponse;
use Modules\Daapi\Actions\Notification\Responses\UnprocessableEntityWithoutPayloadResponse;
use Modules\Daapi\Http\Controllers\Controller;
use Modules\Daapi\Http\Controllers\Traits\LogRequest;
use Modules\Daapi\Http\Requests\SendInvitationsRequest;
use Modules\User\Repositories\UserRepository;

class SendInvitationsController extends Controller
{
    use LogRequest;

    /**
     * @param SendInvitationsRequest                    $request
     * @param StoreInvitationsAction                    $store
     * @param ValidateInvitationEmailsAction            $validate
     * @param FilterInvalidEmailsFromRequestAction      $filterInvalidEmails
     * @param UserRepository                            $userRepository
     * @param PartiallyAcceptedWithoutPayloadResponse   $partiallyAcceptedResponse
     * @param UnprocessableEntityWithoutPayloadResponse $unprocessableEntityResponse
     *
     * @return JsonResponse
     * @throws \App\Exceptions\BaseException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        SendInvitationsRequest $request,
        StoreInvitationsAction $store,
        ValidateInvitationEmailsAction $validate,
        FilterInvalidEmailsFromRequestAction $filterInvalidEmails,
        UserRepository $userRepository,
        PartiallyAcceptedWithoutPayloadResponse $partiallyAcceptedResponse,
        UnprocessableEntityWithoutPayloadResponse $unprocessableEntityResponse
    ): JsonResponse {
        $this->log($request);

        $emails = array_column(Arr::get($request->all(), 'request.payload.invitations'), 'email');
        $result = $validate->handle($emails);

        $validEmails = Arr::get($result, 'valid');

        if (empty($validEmails)) {
            return $unprocessableEntityResponse->get($request, [
                'response.payload.invitationIgnored' => Arr::get($result, 'errors'),
            ]);
        }

        $payload = $filterInvalidEmails->handle($request->all(), $validEmails);
        $admin = $userRepository->getTechnicalAdmin();
        $store->handle($payload, $admin);

        return $partiallyAcceptedResponse->get($request, [
            'response.payload.invitationIgnored' => Arr::get($result, 'errors'),
        ]);
    }
}
