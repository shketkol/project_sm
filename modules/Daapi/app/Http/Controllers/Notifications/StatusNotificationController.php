<?php

namespace Modules\Daapi\Http\Controllers\Notifications;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Daapi\Actions\Notification\Contracts\StatusHandler;
use Modules\Daapi\Actions\Notification\Responses\StatusNotificationAcceptedResponse;
use Modules\Daapi\Http\Controllers\Traits\LogRequest;

class StatusNotificationController
{
    use LogRequest;

    /**
     * @var StatusHandler
     */
    private $statusHandler;

    /**
     * StatusNotificationController constructor.
     *
     * @param StatusHandler $statusHandler
     */
    public function __construct(StatusHandler $statusHandler)
    {
        $this->statusHandler = $statusHandler;
    }

    /**
     * @param Request                            $request
     * @param StatusNotificationAcceptedResponse $notificationAcceptedResponse
     *
     * @return JsonResponse
     */
    public function __invoke(
        Request $request,
        StatusNotificationAcceptedResponse $notificationAcceptedResponse
    ): JsonResponse {
        $this->log($request);
        $this->statusHandler->handle($request);

        return $notificationAcceptedResponse->get($request);
    }
}
