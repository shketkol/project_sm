<?php

namespace Modules\Daapi\Http\Controllers\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Modules\Daapi\HttpClient\Builders\DaapiCallbackResponseBuilder;

trait CallbackResponse
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    private function successResponse(Request $request): JsonResponse
    {
        return (new DaapiCallbackResponseBuilder())->make(
            $request,
            Response::HTTP_OK,
            ['message' => trans('daapi::messages.successfully_processed')]
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    private function unauthorizedResponse(Request $request): JsonResponse
    {
        return (new DaapiCallbackResponseBuilder())->make(
            $request,
            Response::HTTP_UNAUTHORIZED,
            ['reason' => 'Authentication of the user could not be verified for this request']
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    private function forbiddenResponse(Request $request): JsonResponse
    {
        return (new DaapiCallbackResponseBuilder())->make(
            $request,
            Response::HTTP_FORBIDDEN,
            ['reason' => 'Forbidden']
        );
    }

    /**
     * @param Request    $request
     * @param \Throwable $exception
     * @param int        $statusCode
     * @param string     $message
     *
     * @return JsonResponse
     */
    private function exceptionResponse(
        Request $request,
        \Throwable $exception,
        int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR,
        string $message = 'Internal Server Error'
    ): JsonResponse {
        Log::logException($exception);

        return (new DaapiCallbackResponseBuilder())->make($request, $statusCode, ['reason' => $message]);
    }
}
