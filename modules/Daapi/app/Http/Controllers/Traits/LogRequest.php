<?php

namespace Modules\Daapi\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Log\Logger;

trait LogRequest
{
    /**
     * @param Request $request
     */
    public function log(Request $request)
    {
        /** @var Logger $logger */
        $logger = app(Logger::class);
        $logger->debug('[NOTIFICATION.REQUEST]: Notification received', $request->all());
    }
}
