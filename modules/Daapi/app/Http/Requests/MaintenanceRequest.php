<?php

namespace Modules\Daapi\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Contracts\Validation\Validator;
use Modules\Daapi\Exceptions\DaapiValidationErrorException;

class MaintenanceRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'request.payload'                     => $validationRules->only(
                'request.payload',
                ['required', 'array']
            ),
            'request.payload.maintenance_enabled' => $validationRules->only(
                'maintenance.maintenance_enabled',
                ['required', 'boolean']
            ),
            'request.payload.message'             => $validationRules->only(
                'maintenance.message',
                ['nullable', 'string', 'max']
            ),
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     *
     * @return void
     *
     * @throws DaapiValidationErrorException
     */
    protected function failedValidation(Validator $validator): void
    {
        throw new DaapiValidationErrorException($validator);
    }
}
