<?php

namespace Modules\Daapi\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Contracts\Validation\Validator;
use Modules\Daapi\Exceptions\DaapiValidationErrorException;

class SendPaymentFailedNotificationRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'request.payload'                => $validationRules->only(
                'daapi_api.payload',
                ['required', 'array']
            ),
            'request.payload.account_id'     => $validationRules->only(
                'daapi_api.advertiser',
                ['required', 'string', 'exists', 'validate_user_status']
            ),
            'request.payload.campaign_id'    => $validationRules->only(
                'daapi_api.campaign',
                ['required', 'integer', 'min', 'exists', 'max', 'validate_campaign_status']
            ),
            'request.payload.event_time'     => $validationRules->only(
                'daapi_api.event_time',
                ['required', 'date_format']
            ),
        ];
    }

    /**
     * Handle a failed validation attempt.
     * @param Validator $validator
     *
     * @return void
     * @throws DaapiValidationErrorException
     */
    protected function failedValidation(Validator $validator): void
    {
        throw new DaapiValidationErrorException($validator);
    }
}
