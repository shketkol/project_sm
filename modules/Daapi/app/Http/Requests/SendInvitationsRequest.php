<?php

namespace Modules\Daapi\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Contracts\Validation\Validator;
use Modules\Daapi\Exceptions\DaapiValidationErrorException;

class SendInvitationsRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'request.payload'                                  => $validationRules->only(
                'invitation.payload',
                ['required', 'array']
            ),
            'request.payload.special_ads_category'             => $validationRules->only(
                'invitation.account_type_category',
                ['boolean']
            ),
            'request.payload.invitations'                      => $validationRules->only(
                'invitation.payload',
                ['required', 'array', 'indexed_array', 'min', 'max']
            ),
            'request.payload.invitations.*.email'              => $validationRules->only(
                'invitation.email',
                ['required']/** @see \Modules\Daapi\Actions\Invitations\ValidateInvitationEmailsAction::handle() */
            ),
            'request.payload.invitations.*.code'               => $validationRules->only(
                'invitation.code',
                ['nullable', 'string', 'max', 'unique:invitations,code']
            ),
            'request.payload.invitations.*.expiration_date'    => $validationRules->only(
                'invitation.expiration_date',
                ['nullable', 'haapi_date']
            ),
            'request.payload.invitations.*.identification_key' => $validationRules->only(
                'invitation.identification_key',
                ['nullable', 'string', 'max']
            ),
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     *
     * @return void
     *
     * @throws DaapiValidationErrorException
     */
    protected function failedValidation(Validator $validator): void
    {
        throw new DaapiValidationErrorException($validator);
    }
}
