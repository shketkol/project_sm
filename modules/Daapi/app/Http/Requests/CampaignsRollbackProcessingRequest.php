<?php

namespace Modules\Daapi\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Contracts\Validation\Validator;
use Modules\Daapi\Exceptions\DaapiValidationErrorException;

class CampaignsRollbackProcessingRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'request.payload.campaign_id' => $validationRules->only(
                'campaign.id',
                ['required', 'integer', 'exists', 'validate_campaign_status']
            ),
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     *
     * @return void
     *
     * @throws DaapiValidationErrorException
     */
    protected function failedValidation(Validator $validator): void
    {
        throw new DaapiValidationErrorException($validator);
    }
}
