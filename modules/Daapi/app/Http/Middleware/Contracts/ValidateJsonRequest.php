<?php

namespace Modules\Daapi\Http\Middleware\Contracts;

use App\Services\ValidationRulesService\ValidationRules;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Modules\Daapi\Exceptions\DaapiValidationErrorException;
use Modules\Daapi\Validation\RulesHelper;

abstract class ValidateJsonRequest
{
    /**
     * @var ValidationRules
     */
    protected $validationRules;

    /**
     * @var RulesHelper
     */
    protected $rulesHelper;

    /**
     * @param ValidationRules $validationRules
     * @param RulesHelper $rulesHelper
     */
    public function __construct(ValidationRules $validationRules, RulesHelper $rulesHelper)
    {
        $this->validationRules = $validationRules;
        $this->rulesHelper = $rulesHelper;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @param string $tag
     * @return Response
     */
    abstract public function handle(Request $request, Closure $next, string $tag);

    /**
     * @param string $tag
     * @return array
     */
    protected function getRules(string $tag): array
    {
        return $this->rulesHelper->getRulesByTag($tag);
    }

    /**
     * @param array $request
     * @param string $tag
     * @throws DaapiValidationErrorException
     */
    protected function validateRequest(array $request, string $tag)
    {
        $rules = $this->rulesHelper->getRulesByTag("{$tag}_body");

        $validator = Validator::make($request, $rules);

        if ($validator->fails()) {
            throw new DaapiValidationErrorException($validator);
        }
    }
}
