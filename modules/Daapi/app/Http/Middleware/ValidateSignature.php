<?php

namespace Modules\Daapi\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Modules\Daapi\Http\Controllers\Traits\CallbackResponse;

class ValidateSignature
{
    use CallbackResponse;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return Response
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->hasValidSignature()) {
            return $next($request);
        }

        Log::error('Callback call is unauthenticated');

        return $this->unauthorizedResponse($request);
    }
}
