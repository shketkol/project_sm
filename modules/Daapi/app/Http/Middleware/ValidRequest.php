<?php

namespace Modules\Daapi\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Modules\Daapi\Http\Controllers\Traits\CallbackResponse;
use Modules\Haapi\Helpers\HaapiCheckResponse;

class ValidRequest
{
    use CallbackResponse;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return Response
     */
    public function handle(Request $request, Closure $next)
    {
        if ((int)$request->json('haapi.response.statusCode') === (int)HaapiCheckResponse::STATUS_CODE_INTERNAL_ERROR) {
            Log::error('[DAAPI] Accept HAAPI INTERNAL SERVER ERROR and finish.');

            return $this->successResponse($request);
        }

        return $next($request);
    }
}
