<?php

namespace Modules\Daapi\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ForceJsonResponse
{
    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return JsonResponse
     */
    public function handle(Request $request, Closure $next): JsonResponse
    {
        // response only json
        $request->headers->set('Accept', 'application/json');

        // assume request is always json even header could be missing
        $request->headers->set('Content-Type', 'application/json');

        return $next($request);
    }
}
