<?php

namespace Modules\Daapi\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Haapi\Helpers\HaapiCheckResponse;
use Psr\Log\LoggerInterface;

class RequestLogger
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * RequestLogger constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $method = $request->method();
        $route = $request->route()->getName();
        $statusCode = $request->json('haapi.response.statusCode');

        $info = [];

        if ((int)$statusCode === (int)HaapiCheckResponse::STATUS_CODE_INTERNAL_ERROR) {
            $info[] = 'Error: HULU.ERROR.CALLBACK';
            $info[] = 'Message: ' . $request->json('haapi.response.statusMsg');
            $info[] = "Code: {$statusCode}";
        }

        $this->logger->debug('[CALLBACK.REQUEST]: Callback received.', [
            'type'    => $request->json('haapi.request.type'),
            'info'    => $info,
            'request' => $request->all(),
        ]);

        $response = $next($request);

        $this->logger->debug('[DAAPI] The callback was handled.', [
            'method' => $method,
            'route'  => $route,
        ]);

        return $response;
    }
}
