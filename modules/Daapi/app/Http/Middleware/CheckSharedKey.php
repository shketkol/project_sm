<?php

namespace Modules\Daapi\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Laravel\Passport\Exceptions\MissingScopeException;
use Laravel\Passport\Http\Middleware\CheckClientCredentials;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Modules\Daapi\Actions\Notification\Responses\StatusUnauthorizedResponse;
use Modules\Daapi\Actions\Notification\Responses\UnauthorizedResponse;
use Nyholm\Psr7\Factory\Psr17Factory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;

class CheckSharedKey extends CheckClientCredentials
{
    /**
     * @var UnauthorizedResponse
     */
    protected $unauthorizedResponse;

    /**
     * @var StatusUnauthorizedResponse
     */
    protected $statusUnauthorizedResponse;

    /**
     * CheckSharedKey constructor.
     *
     * @param ResourceServer             $server
     * @param UnauthorizedResponse       $unauthorizedResponse
     * @param StatusUnauthorizedResponse $statusUnauthorizedResponse
     */
    public function __construct(
        ResourceServer $server,
        UnauthorizedResponse $unauthorizedResponse,
        StatusUnauthorizedResponse $statusUnauthorizedResponse
    ) {
        parent::__construct($server, app(TokenRepository::class));
        $this->unauthorizedResponse = $unauthorizedResponse;
        $this->statusUnauthorizedResponse = $statusUnauthorizedResponse;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param mixed   ...$scopes
     *
     * @return mixed
     * @throws MissingScopeException
     */
    public function handle($request, Closure $next, ...$scopes)
    {
        $psr = (new PsrHttpFactory(
            new Psr17Factory,
            new Psr17Factory,
            new Psr17Factory,
            new Psr17Factory
        ))->createRequest($request);

        try {
            $psr = $this->server->validateAuthenticatedRequest($psr);
        } catch (OAuthServerException $e) {
            if ($request->entity) {
                return $this->unauthorizedResponse->get($request);
            } else {
                return $this->statusUnauthorizedResponse->get($request);
            }
        }

        $this->validate($psr, $scopes);

        return $next($request);
    }
}
