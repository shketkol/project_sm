<?php

namespace Modules\Daapi\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Modules\Daapi\Exceptions\DaapiValidationErrorException;
use Modules\Daapi\Http\Middleware\Contracts\ValidateJsonRequest;

class ValidateNotificationJsonRequest extends ValidateJsonRequest
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @param string $tag
     * @return Response
     * @throws DaapiValidationErrorException
     */
    public function handle(Request $request, Closure $next, string $tag)
    {
        if (!config('daapi.validation.validate_notifications')) {
            return $next($request);
        }

        $body = $request->toArray();
        $this->validateRequest($body, $tag);

        $payload = Arr::get($body, 'request.payload');
        $validator = Validator::make($payload, $this->getRules($tag));

        if ($validator->fails()) {
            throw new DaapiValidationErrorException($validator);
        }

        return $next($request);
    }
}
