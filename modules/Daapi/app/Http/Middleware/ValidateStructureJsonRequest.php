<?php

namespace Modules\Daapi\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Daapi\Exceptions\DaapiValidationErrorException;
use Modules\Daapi\Http\Middleware\Contracts\ValidateJsonRequest;

class ValidateStructureJsonRequest extends ValidateJsonRequest
{
    /**
     * @param Request     $request
     * @param Closure     $next
     * @param string|null $tag
     *
     * @return Response|JsonResponse
     * @throws DaapiValidationErrorException
     */
    public function handle(Request $request, Closure $next, string $tag = null)
    {
        if (!config('daapi.validation.validate_structure')) {
            return $next($request);
        }

        $body = $request->toArray();
        $this->validateRequest($body, 'structure');

        return $next($request);
    }
}
