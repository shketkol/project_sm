<?php

namespace Modules\Daapi;

use App\Helpers\EnvironmentHelper;
use App\Providers\ModuleServiceProvider;
use Laravel\Passport\Passport;
use Modules\Daapi\Actions\Notification\StatusHandler;
use Modules\Daapi\Console\Commands\AccountCreateCallbackGetCommand;
use Modules\Daapi\Console\Commands\AccountUpdateCallbackGetCommand;
use Modules\Daapi\Console\Commands\CreativeUpdateCallbackGetCommand;
use Modules\Daapi\Console\Commands\UserUpdateCallbackGetCommand;
use Modules\Daapi\HttpClient\Builders\Contracts\ResponseBuilder;
use Modules\Daapi\HttpClient\Builders\DaapiRequestBuilder;
use Modules\Daapi\HttpClient\Builders\Contracts\RequestBuilder;
use Modules\Daapi\HttpClient\Builders\DaapiResponseBuilder;
use Modules\Haapi\Helpers\HaapiConfig;
use Modules\Daapi\Actions\Notification\Contracts\StatusHandler as StatusHandlerInterface;

class DaapiServiceProvider extends ModuleServiceProvider
{
    /**
     * @var array $bindings
     */
    public $bindings = [
        RequestBuilder::class => DaapiRequestBuilder::class,
        ResponseBuilder::class => DaapiResponseBuilder::class,

        /**
         * Notification actions
         */
        StatusHandlerInterface::class => StatusHandler::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadTranslations();
        $this->mergeConfigFrom(__DIR__ . '/../config/daapi.php', 'daapi');

        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');

        if (!EnvironmentHelper::isClosedEnv()) {
            $this->loadRoutesFrom(__DIR__ . '/../routes/dev/api.php');
        }

        $this->app->bind('Modules\Daapi\Helpers\DaapiConfig', function ($app) {
            return new HaapiConfig('daapi');
        });
        $this->commands($this->commands);

        Passport::loadKeysFrom(config('daapi.oauth.keys_path'));
    }

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        AccountCreateCallbackGetCommand::class,
        AccountUpdateCallbackGetCommand::class,
        UserUpdateCallbackGetCommand::class,
        CreativeUpdateCallbackGetCommand::class,
    ];

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'daapi';
    }
}
