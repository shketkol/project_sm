<?php

namespace Modules\Daapi\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\URL;

class UserUpdateCallbackGetCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:callback:user:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate signed URL for Daapi update user callback.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        echo URL::signedRoute('callbacks.user.update') . "\r\n";
    }
}
