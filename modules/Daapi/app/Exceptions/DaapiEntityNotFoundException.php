<?php

namespace Modules\Daapi\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Daapi\Actions\Notification\Responses\EntityNotFoundResponse;

class DaapiEntityNotFoundException extends Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function render(Request $request): JsonResponse
    {
        /** @var EntityNotFoundResponse $response */
        $response = app(EntityNotFoundResponse::class);
        $response->statusMessage = $this->getMessage();
        $response->statusCode = $this->getCode();

        return $response->get($request);
    }
}
