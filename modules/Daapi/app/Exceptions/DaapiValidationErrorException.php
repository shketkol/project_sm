<?php

namespace Modules\Daapi\Exceptions;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Modules\Daapi\Actions\Notification\Responses\ValidationResponse;

class DaapiValidationErrorException extends ValidationException
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function render(Request $request): JsonResponse
    {
        /** @var ValidationResponse $response */
        $response = app(ValidationResponse::class);

        $response->statusMessage = [
            'message' => $this->message,
            'errors'  => $this->errors(),
        ];

        return $response->get($request);
    }
}
