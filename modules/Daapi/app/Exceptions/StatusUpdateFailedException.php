<?php

namespace Modules\Daapi\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Daapi\Actions\Notification\Responses\StatusUpdateFailedResponse;

class StatusUpdateFailedException extends BaseException
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function render(Request $request): JsonResponse
    {
        /** @var StatusUpdateFailedResponse $response */
        $response = app(StatusUpdateFailedResponse::class);
        $response->statusMessage = $this->getMessage();

        if ($this->getCode() !== 0) {
            $response->statusCode = $this->getCode();
        }

        return $response->get($request);
    }

    /**
     * @param string     $message
     * @param \Throwable $previous
     *
     * @return StatusUpdateFailedException
     */
    public static function create(string $message, \Throwable $previous): self
    {
        if (Str::length($previous->getMessage()) !== 0) {
            $message = $previous->getMessage();
        }

        return new static($message, $previous->getCode(), $previous);
    }
}
