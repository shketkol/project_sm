<?php

namespace Modules\Daapi\Exceptions;

use App\Exceptions\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Daapi\Actions\Notification\Responses\AdminNotFoundResponse;

class AdminNotFoundException extends ModelNotFoundException
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function render(Request $request): Response
    {
        $entityNotFoundResponse = app(AdminNotFoundResponse::class);

        return $entityNotFoundResponse->get($request);
    }
}
