<?php

namespace Modules\Daapi\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CanNotApplyStatusException extends BaseException
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function render(Request $request): Response
    {
        return response($this->message, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param Model  $model
     * @param string $transition
     *
     * @return CanNotApplyStatusException
     */
    public static function create(Model $model, string $transition): self
    {
        $exception = new self();
        $exception->code = Response::HTTP_UNPROCESSABLE_ENTITY;

        $exception->message = sprintf(
            'Can not apply transition "%s" to the entity "%s" with id: "%s". Current state is "%s".',
            $transition,
            get_class($model),
            method_exists($model, 'getExternalId') ? $model->getExternalId() : $model->getKey(),
            $model->status
        );

        return $exception;
    }
}
