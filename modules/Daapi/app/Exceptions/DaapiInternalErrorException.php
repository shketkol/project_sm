<?php

namespace Modules\Daapi\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Daapi\Actions\Notification\Responses\InternalErrorResponse;

class DaapiInternalErrorException extends BaseException
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function render(Request $request): JsonResponse
    {
        /** @var InternalErrorResponse $response */
        $response = app(InternalErrorResponse::class);

        return $response->get($request);
    }
}
