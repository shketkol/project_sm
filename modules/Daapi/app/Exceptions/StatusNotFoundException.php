<?php

namespace Modules\Daapi\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StatusNotFoundException extends BaseException
{
    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected static $messageTemplate = 'Status "%s" not found.';

    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     * @return Response|JsonResponse
     */
    public function render(Request $request)
    {
        $message = sprintf(self::$messageTemplate, $this->status);
        $code = Response::HTTP_NOT_FOUND;

        if ($request->expectsJson()) {
            return response()->json(['data' => [
                'message' => $message,
            ]], $code);
        }

        return response($message, $code);
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @param string $status
     *
     * @return StatusNotFoundException
     */
    public static function create(string $status): self
    {
        $message = sprintf(self::$messageTemplate, $status);
        $exception = new static($message, Response::HTTP_NOT_FOUND);
        $exception->setStatus($status);

        return $exception;
    }
}
