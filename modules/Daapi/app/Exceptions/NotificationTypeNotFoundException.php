<?php

namespace Modules\Daapi\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NotificationTypeNotFoundException extends BaseException
{
    /**
     * @var string
     */
    protected $notificationType;

    /**
     * @var string
     */
    protected static $messageTemplate = 'Notification type "%s" not found.';

    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     * @return Response|JsonResponse
     */
    public function render(Request $request)
    {
        $message = sprintf(self::$messageTemplate, $this->notificationType);
        $code = Response::HTTP_NOT_FOUND;

        if ($request->expectsJson()) {
            return response()->json(['data' => [
                'message' => $message,
            ]], $code);
        }

        return response($message, $code);
    }

    /**
     * @param string $notificationType
     */
    public function setNotificationType(string $notificationType): void
    {
        $this->notificationType = $notificationType;
    }

    /**
     * @param string $notificationType
     *
     * @return self
     */
    public static function create(string $notificationType): self
    {
        $message = sprintf(self::$messageTemplate, $notificationType);
        $exception = new static($message, Response::HTTP_NOT_FOUND);
        $exception->setNotificationType($notificationType);

        return $exception;
    }
}
