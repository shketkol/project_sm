<?php

namespace Modules\Daapi\DataTransferObjects;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Spatie\DataTransferObject\DataTransferObject;

class AccountUpdateData extends DataTransferObject
{
    /**
     * String(2000) required
     *
     * @var string
     */
    public $message;

    /**
     * @var \Modules\Daapi\DataTransferObjects\Types\AccountData
     */
    public $account;

    /**
     * AccountUpdateData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct([
            'message' => Arr::get($payload, 'message', ''),
            'account' => new AccountData(Arr::get($payload, 'account')),
        ]);
    }
}
