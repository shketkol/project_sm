<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use App\Helpers\Math\CalculationHelper;
use App\Helpers\DateFormatHelper;
use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class CampaignData extends DataTransferObject
{
    /**
     * Originating System Campaign ID, Optional
     *
     * @var int|string|null
     */
    public $sourceId;

    /**
     * Hulu Campaign UUID, Required
     *
     * @var string
     */
    public $id;

    /**
     * The name of self-serve campaign, Required
     *
     * @var string
     */
    public $name;

    /**
     * The UUID of self serve advertiser, Required
     * HAAPI will parse current user account based on ​accessToken​.
     * If current user account is an agency, the order will be
     * created with this ​advertiserId​ and agencyId​.
     * Otherwise, current user is advertiser
     *
     * @var string
     */
    public $advertiserId;

    /**
     * The status of a self-serve campaign required
     * Which can be the following:
     * “Pending Approval” “Pending” “Ready” “Live” “Complete”
     * “Paused” “Cancelled” "Paused For Billing Issue"​
     * if the campaign payment fails, the advertiser cannot
     * resume the campaign until the credit card issue is resolved
     *
     * @var string
     */
    public $status;

    /**
     * Array of line items
     *
     * @var \Modules\Daapi\DataTransferObjects\Types\LineItemData[]
     */
    public $lineItems;

    /**
     * The promo code applied to a self-serve campaign. Optional
     * The value will be ​null​ if there’s no promo code applied.
     *
     * @var array|null
     */
    public $promoCode;

    /**
     * UUID, Required
     *
     * @var string
     */
    public $agencyId;

    /**
     * @var string|null
     */
    public $operativeOrderId;

    /**
     * Rounded budget
     * @var double|null
     */
    public $displayBudget;

    /**
     * Rounded budget with applied promocode
     * @var double|null
     */
    public $displayDiscountedBudget;

    /**
     * CampaignData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $lineItemsData = array_map(function ($payloadItem) {
            return new LineItemData($payloadItem);
        }, Arr::get($payload, 'lineItems', []));

        parent::__construct(
            Arr::only(
                $payload,
                [
                    'sourceId',
                    'id', 'name',
                    'advertiserId',
                    'status',
                    'promoCode',
                    'agencyId',
                    'operativeOrderId',
                    'displayBudget',
                    'displayDiscountedBudget'
                ]
            ) + [
                'lineItems' => $lineItemsData
            ]
        );
    }

    /**
     * @return array
     */
    public function mapToModelStructure(): array
    {
        $lineItem = Arr::first($this->lineItems);
        return [
            'cpm' => $lineItem->unitCost,
            'impression' => $lineItem->quantity,
            'budget' => CalculationHelper::calculateBudget($lineItem->unitCost, $lineItem->quantity),
            'date_start' => DateFormatHelper::convertHaapiDate($lineItem->startDate),
            'date_end' => DateFormatHelper::convertHaapiDate($lineItem->endDate),
            'name' => $this->name,
        ];
    }
}
