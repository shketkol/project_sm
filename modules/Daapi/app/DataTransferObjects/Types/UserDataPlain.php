<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class UserDataPlain extends DataTransferObject
{
    /**
     * String(255), Required
     *
     * @var string
     */
    public $email;

    /**
     * String(40), Required
     *
     * @var string
     */
    public $firstName;

    /**
     * String(40), Required
     *
     * @var string
     */
    public $lastName;

    /**
     * UUID, Required
     *
     * @var string
     */
    public $externalId;

    /**
     * Enum, Required
     *
     * @var string
     */
    public $userStatus;

    /**
     * Enum, Required
     *
     * @var string
     */
    public $userType;

    /**
     * Array, Required
     *
     * @var null|array
     */
    public $company;

    /**
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct([
            'email'      => Arr::get($payload, 'email'),
            'firstName'  => Arr::get($payload, 'firstName'),
            'lastName'   => Arr::get($payload, 'lastName'),
            'externalId' => Arr::get($payload, 'id'),
            'userStatus' => Arr::get($payload, 'userStatus'),
            'userType'   => Arr::get($payload, 'userType', ''),
            'company'    => Arr::get($payload, 'account')
        ]);
    }
}
