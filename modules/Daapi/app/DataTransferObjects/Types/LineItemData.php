<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class LineItemData extends DataTransferObject
{
    /**
     * UUID, Required
     *
     * @var string
     */
    public $id;

    /**
     * String, Required
     *
     * @var string
     */
    public $status;

    /**
     * The start date and time of the line item, Required
     *
     * @var string
     */
    public $startDate;

    /**
     * The end date and time of the line item, Required
     *
     * @var string
     */
    public $endDate;

    /**
     * An array of targeting rules.
     * Each entry of the dictionary shall be in the format:
     * {“typeId”: “targetTypeId”, “values”: [{ “value”: “targetValue”, “exclusion”: 0}]}
     *
     * @var TargetData[]|array
     */
    public $targets;

    /**
     * Contain fields required for uploading a new asset, Optional
     *
     * @var \Modules\Daapi\DataTransferObjects\Types\CreativeData|null
     */
    public $creative;

    /**
     * The UUID of a HAAPI product String(36) Required
     *
     * @var string
     */
    public $productId;

    /**
     * The cost per billable event, for example, per impression, Required
     *
     * @var float
     */
    public $unitCost;

    /**
     * The cost model being used. The default value is “CPM”, Required
     *
     * @var string|null
     */
    public $costModel;

    /**
     * The quantity requested by self-serve order. The value shall not be smaller than 0, Required
     *
     * @var int
     */
    public $quantity;

    /**
     * The quantity type. The only value is “IMPRESSIONS”, Required
     *
     * @var string
     */
    public $quantityType;

    /**
     * UUID, Optional
     *
     * @var string|null
     */
    public $creativeId;

    /**
     * @var string|null
     */
    public $unitCostType;

    /**
     * LineItemData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $targetsData = array_map(function ($payloadItem) {
            return new TargetData($payloadItem);
        }, Arr::get($payload, 'targets'));
        $creative = Arr::get($payload, 'creative');
        $creative = is_array($creative) ? new CreativeData($creative) : null;
        Arr::set($payload, 'unitCost', (float)Arr::get($payload, 'unitCost'));

        parent::__construct(
            Arr::only(
                $payload,
                [
                    'id',
                    'status',
                    'startDate',
                    'endDate',
                    'productId',
                    'unitCost',
                    'costModel',
                    'quantity',
                    'quantityType',
                    'creativeId',
                    'unitCostType'
                ]
            ) + [
                'targets' => $targetsData,
                'creative' => $creative
            ]
        );
    }
}
