<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class CompanyAddressData extends DataTransferObject
{
    /**
     * String(255), Required
     *
     * @var string
     */
    public $line1;

    /**
     * String(255), Optional
     *
     * @var string|null
     */
    public $line2;

    /**
     * String(40), Required
     *
     * @var string
     */
    public $city;

    /**
     * String(40), Required
     *
     * @var string
     */
    public $state;

    /**
     * String(60), Required
     *
     * @var string
     */
    public $country;

    /**
     * String(20), Required
     *
     * @var string
     */
    public $zipcode;

    /**
     * CompanyAddressData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct([
            'line1'     => Arr::get($payload, 'line1', ''),
            'line2'     => Arr::get($payload, 'line2', '') ?? '',
            'city'      => Arr::get($payload, 'city', ''),
            'state'     => Arr::get($payload, 'state', ''),
            'country'   => Arr::get($payload, 'country', 'US'),
            'zipcode'   => Arr::get($payload, 'zipcode', ''),
        ]);
    }
}
