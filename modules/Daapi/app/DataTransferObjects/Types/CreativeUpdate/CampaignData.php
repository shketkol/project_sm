<?php

namespace Modules\Daapi\DataTransferObjects\Types\CreativeUpdate;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class CampaignData extends DataTransferObject
{
    /**
     * The HAAPI UUID of the campaign with which this creative is associated, Required
     *
     * @var string
     */
    public $id;

    /**
     * Information about the creative. Updated creative, Required
     *
     * @var \Modules\Daapi\DataTransferObjects\Types\CreativeUpdate\CreativeData
     */
    public $creative;

    /**
     * CampaignData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct(
            [
                'id' => Arr::get($payload, 'id'),
                'creative' => new CreativeData(Arr::get($payload, 'creative'))
            ]
        );
    }
}
