<?php

namespace Modules\Daapi\DataTransferObjects\Types\CreativeUpdate;

use Spatie\DataTransferObject\DataTransferObject;

class CreativeData extends DataTransferObject
{
    /**
     * The UUID of the creative asset in HAAPI, Required
     *
     * @var string
     */
    public $id;

    /**
     * The name of the creative asset, Required
     *
     * @var string
     */
    public $fileName;

    /**
     * The file extension of the uploaded creative asset, including the period, Required
     *
     * @var string|null
     */
    public $extension;

    /**
     * The file size of the uploaded creative, including the units, Required
     *
     * @var string|null
     */
    public $size;

    /**
     * The duration of the video commercial creative, Required
     * Format: HH:MM:ss:SSS
     *
     * @var string|null
     */
    public $duration;

    /**
     * Status, Required
     * The status of the creative: UNDER_REVIEW, APPROVED, REJECTED
     * The default status for a newly uploaded creative is UNDER_REVIEW
     *
     * @var string
     */
    public $status;

    /**
     * The date when this creative was uploaded, Required
     * Format: yyyy-mm-ddTHH:mm:ssZ
     *
     * @var null|string
     */
    public $createdAt;

    /**
     * The URL of the location where the creative is stored in HAAPI, Required
     *
     * @var string
     */
    public $creativeUrl;

    /**
     * The advertiser account UUID in HAAPI, Required
     *
     * @var string
     */
    public $accountId;

    /**
     * If the creative was rejected,
     * this field shall contain any comments
     * pertaining to the rejection of the creative, Optional
     *
     * @var string|null
     */
    public $comment;

    /**
     * If the creative was rejected,
     * this field shall provide the reason for the rejection, Optional
     *
     * @var string|null
     */
    public $reason;
}
