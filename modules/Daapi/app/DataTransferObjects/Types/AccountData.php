<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class AccountData extends DataTransferObject
{
    /**
     * UUID, Required
     *
     * @var string
     */
    public $externalId;

    /**
     * Enum, Required
     *
     * @var string
     */
    public $accountType;

    /**
     * String(255), Required
     *
     * @var string
     */
    public $companyName;

    /**
     * @var \Modules\Daapi\DataTransferObjects\Types\CompanyAddressData
     */
    public $companyAddress;

    /**
     * String(40), Required
     *
     * @var string
     */
    public $phoneNumber;

    /**
     * String(30), Required
     *
     * @var string
     */
    public $accountStatus;

    /**
     * AccountData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $idKey = Arr::has($payload, 'externalId') ? 'externalId' : 'id';
        parent::__construct([
            'externalId' => (string) Arr::get($payload, $idKey),
            'accountType' => Arr::get($payload, 'accountType'),
            'companyName' => Arr::get($payload, 'companyName'),
            'companyAddress' => new CompanyAddressData(Arr::get($payload, 'companyAddress', [])),
            'phoneNumber' => Arr::get($payload, 'phoneNumber'),
            'accountStatus' => Arr::get($payload, 'accountStatus'),
        ]);
    }
}
