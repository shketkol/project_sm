<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use Illuminate\Support\Arr;

class UserDataWithResetToken extends UserDataPlain
{
    /**
     * Reset password token, Required
     *
     * @var string
     */
    public $resetPasswordToken;

    /**
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->resetPasswordToken = Arr::get($payload, 'resetPasswordToken', '');

        parent::__construct([
            'id' => Arr::get($payload, 'id'),
            'email' => Arr::get($payload, 'email'),
            'firstName' => Arr::get($payload, 'firstName'),
            'lastName' => Arr::get($payload, 'lastName'),
            'userStatus' => Arr::get($payload, 'userStatus'),
            'userType' => Arr::get($payload, 'userType', '')
        ]);
    }
}
