<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class UserData extends DataTransferObject
{
    /**
     * ID, Required
     *
     * @var int
     */
    public $id;

    /**
     * String(255), Required
     *
     * @var string
     */
    public $email;

    /**
     * String(40), Required
     *
     * @var string
     */
    public $firstName;

    /**
     * String(40), Required
     *
     * @var string
     */
    public $lastName;

    /**
     * UUID, Required
     *
     * @var string
     */
    public $externalId;

    /**
     * Enum, Required
     *
     * @var string
     */
    public $userStatus;

    /**
     * Enum, Required
     *
     * @var string
     */
    public $userType;

    /**
     * @var \Modules\Daapi\DataTransferObjects\Types\AccountData|array
     */
    public $company;

    /**
     * String, Null
     *
     * @var string|null
     */
    public $advertiserSfdcUrl;

    /**
     * String, Null
     *
     * @var string|null
     */
    public $brandSfdcUrl;

     /**
     * String, Null
     *
     * @var string|null
     */
    public $contactSfdcUrl;

    /**
     * UserData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct([
            'id'                => Arr::get($payload, 'id'),
            'email'             => Arr::get($payload, 'email'),
            'firstName'         => Arr::get($payload, 'firstName'),
            'lastName'          => Arr::get($payload, 'lastName'),
            'externalId'        => Arr::get($payload, 'externalId'),
            'userStatus'        => Arr::get($payload, 'userStatus'),
            'userType'          => Arr::get($payload, 'userType', ''),
            'advertiserSfdcUrl' => Arr::get($payload, 'advertiserSfdcUrl'),
            'brandSfdcUrl'      => Arr::get($payload, 'brandSfdcUrl'),
            'contactSfdcUrl'    => Arr::get($payload, 'contactSfdcUrl'),
            'company'           => new AccountData(Arr::get($payload, 'account')),
        ]);
    }

    /**
     * @return array
     */
    public function mapToModelStructure(): array
    {
        return [
            'email' => $this->email,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'company_name' => $this->company->companyName,
        ];
    }
}
