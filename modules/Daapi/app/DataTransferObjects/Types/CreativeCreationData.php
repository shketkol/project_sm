<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use Spatie\DataTransferObject\DataTransferObject;

class CreativeCreationData extends DataTransferObject
{
    /**
     * The advertiser account UUID in HAAPI, Required
     *
     * @var string
     */
    public $accountId;

    /**
     * The creative asset file name, Required
     *
     * @var string
     */
    public $fileName;

    /**
     * The S3 object URL sent to HAAPI, Required
     *
     * @var string
     */
    public $sourceUrl;

    /**
     * The size of the creative file, Required
     *
     * @var int
     */
    public $contentLength;
}
