<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class TargetData extends DataTransferObject
{
    /**
     * UUID of target, Required
     *
     * @var string
     */
    public $typeId;

    /**
     * UUID, Optional
     *
     * @var array|null
     */
    public $values;

    /**
     * TargetData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct(Arr::only($payload, ['typeId', 'values']));
    }
}
