<?php

namespace Modules\Daapi\DataTransferObjects\Types;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class CreativeData extends DataTransferObject
{
    /**
     * The UUID of a HAAPI asset, which shall be stored in S3, String(36), Optional
     *
     * @var string
     */
    public $id;

    /**
     * The creative asset file name, Required
     *
     * @var string
     */
    public $fileName;

    /**
     * The S3 object URL sent to HAAPI, Required
     *
     * @var string
     */
    public $creativeUrl;

    /**
     * The size of the creative file, Optional
     *
     * @var null|int
     */
    public $contentLength;

    /**
     * Creative status
     *
     * @var string
     */
    public $status;

    /**
     * Rejection reason
     *
     * @var null|string
     */
    public $reason;

    /**
     * Rejection comment
     *
     * @var null|string
     */
    public $comment;

    /**
     * CreativeData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct(
            Arr::only(
                $payload,
                [
                    'id',
                    'fileName',
                    'creativeUrl',
                    'contentLength',
                    'reason',
                    'comment',
                    'status',
                ]
            )
        );
    }

    /**
     * @return array
     */
    public function mapToModelStructure(): array
    {
        return [
            'name'         => $this->fileName,
            'preview_url'  => $this->creativeUrl,
            'original_key' => null, // remove original S3 key, file itself must be removed separately
            'is_pro_res'   => false, // we assume that video src is converted
        ];
    }
}
