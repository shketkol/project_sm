<?php

namespace Modules\Daapi\DataTransferObjects;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Spatie\DataTransferObject\DataTransferObject;

class UserUpdateData extends DataTransferObject
{
    /**
     * String(2000) required
     *
     * @var string
     */
    public $message;

    /**
     * @var UserData
     */
    public $user;

    /**
     * UserUpdateData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct([
            'message' => Arr::get($payload, 'message'),
            'user' => new UserData(Arr::get($payload, 'user')),
        ]);
    }
}
