<?php

namespace Modules\Daapi\DataTransferObjects;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class CampaignCancelData extends DataTransferObject
{
    /**
     * Hulu Campaign UUID, Required
     *
     * @var string
     */
    public $campaignId;

    /**
     * CampaignCancelData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct(Arr::only($payload, ['campaignId']));
    }
}
