<?php

namespace Modules\Daapi\DataTransferObjects;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Modules\Daapi\DataTransferObjects\Types\UserDataPlain;
use Spatie\DataTransferObject\DataTransferObject;

class UserCreateData extends DataTransferObject
{
    /**
     * @var \Modules\Daapi\DataTransferObjects\Types\AccountData
     */
    public $account;

    /**
     * @var \Modules\Daapi\DataTransferObjects\Types\UserDataPlain
     */
    public $user;

    /**
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct([
            'account' => new AccountData(Arr::get($payload, 'account')),
            'user'    => new UserDataPlain(Arr::get($payload, 'user')),
        ]);
    }
}
