<?php

namespace Modules\Daapi\DataTransferObjects;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\CreativeUpdate\CampaignData;
use Spatie\DataTransferObject\DataTransferObject;

class CreativeUpdateData extends DataTransferObject
{
    /**
     * Campaign
     *
     * @var \Modules\Daapi\DataTransferObjects\Types\CreativeUpdate\CampaignData
     */
    public $campaign;

    /**
     * CreativeUpdateData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct([
            'campaign' => new CampaignData(Arr::get($payload, 'campaign')),
        ]);
    }
}
