<?php

namespace Modules\Daapi\DataTransferObjects;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Spatie\DataTransferObject\DataTransferObject;

class AccountCreateData extends DataTransferObject
{
    /**
     * String(2000) required
     *
     * @var string
     */
    public $message;

    /**
     * @var AccountData
     */
    public $account;

    /**
     * @var UserData
     */
    public $user;

    /**
     * AccountCreateData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct([
            'message' => Arr::get($payload, 'message'),
            'account' => new AccountData(Arr::get($payload, 'account')),
            'user' => new UserData(Arr::get($payload, 'user')),
        ]);
    }
}
