<?php

namespace Modules\Daapi\DataTransferObjects;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\Types\CampaignData;
use Spatie\DataTransferObject\DataTransferObject;

class CampaignCreateData extends DataTransferObject
{
    /**
     * Campaign object, required
     *
     * @var \Modules\Daapi\DataTransferObjects\Types\CampaignData
     */
    public $campaign;

    /**
     * CampaignCreateData constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        parent::__construct(
            [
                'campaign' => new CampaignData(Arr::get($payload, 'campaign')),
            ]
        );
    }
}
