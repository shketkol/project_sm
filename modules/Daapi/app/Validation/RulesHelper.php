<?php

namespace Modules\Daapi\Validation;

use App\Services\ValidationRulesService\ValidationRules;
use Illuminate\Support\Arr;

class RulesHelper
{
    /**
     * @var ValidationRules
     */
    private $validationRules;

    /**
     * @param ValidationRules $validationRules
     */
    public function __construct(ValidationRules $validationRules)
    {
        $this->validationRules = $validationRules;
    }

    /**
     * @param string $tag
     *
     * @return array
     */
    public function getRulesByTag(string $tag): array
    {
        $rules = $this->getAllRules();

        return Arr::get($rules, $tag, []);
    }

    /**
     * @return array
     */
    private function getAllRules(): array
    {
        return [
            //-------------STRUCTURE PAYLOAD-------------
            'structure_body'           => [
                'request'           => $this->validationRules->only(
                    'daapi.request',
                    ['required', 'array', 'min', 'max']
                ),
                'request.id'        => $this->validationRules->only(
                    'daapi.id',
                    ['required', 'uuid']
                ),
                'request.payload'   => $this->validationRules->only(
                    'daapi.payload',
                    ['required', 'array', 'min', 'max']
                ),
                'request.payload.*' => $this->validationRules->only(
                    'daapi.payload',
                    ['required']
                ),
            ],
            'status_notification_body' => [
                'request'         => $this->validationRules->only(
                    'daapi.request',
                    ['required', 'array']
                ),
                'request.payload' => $this->validationRules->only(
                    'daapi.payload',
                    ['required', 'array']
                ),
            ],
            'status_notification'      => [
                '*'         => $this->validationRules->only(
                    'daapi.notification.statuses',
                    ['allowed_attributes']
                ),
                'accounts'  => $this->validationRules->only(
                    'daapi.notification.accounts',
                    ['sometimes', 'required', 'array']
                ),
                'orders'    => $this->validationRules->only(
                    'daapi.notification.orders',
                    ['sometimes', 'required', 'array']
                ),
                'campaigns' => $this->validationRules->only(
                    'daapi.notification.campaigns',
                    ['sometimes', 'required', 'array']
                ),
                'payments'  => $this->validationRules->only(
                    'daapi.notification.payments',
                    ['sometimes', 'required', 'array']
                ),
                'creatives' => $this->validationRules->only(
                    'daapi.notification.creatives',
                    ['sometimes', 'required', 'array']
                ),
            ],
            //-------------CALLBACK BODY-------------
            'callback_body'            => [
                'haapi.request'             => $this->validationRules->only(
                    'daapi.callback.body.request',
                    ['required', 'array']
                ),
                'haapi.response'            => $this->validationRules->only(
                    'daapi.callback.body.response',
                    ['required', 'array']
                ),
                'haapi.response.payload'    => $this->validationRules->only(
                    'daapi.callback.body.payload',
                    ['required', 'array']
                ),
                'haapi.response.id'         => $this->validationRules->only(
                    'daapi.callback.body.id',
                    ['required', 'string']
                ),
                'haapi.response.statusCode' => $this->validationRules->only(
                    'daapi.callback.body.status_code',
                    ['required', 'string']
                ),
                'haapi.response.statusMsg'  => $this->validationRules->only(
                    'daapi.callback.body.status_msg',
                    ['required', 'string']
                ),
                'haapi.response.duration'   => $this->validationRules->only(
                    'daapi.callback.body.duration',
                    ['required', 'numeric']
                ),
                'haapi.response.received'   => $this->validationRules->only(
                    'daapi.callback.body.received',
                    ['required', 'string']
                ),
            ],
            //-------------ACCOUNT CREATE CALLBACK-------------
            'account_create'           => [
                'user'                           => $this->validationRules->only(
                    'daapi.callback.accounts.user',
                    ['required', 'array']
                ),
                'user.email'                     => $this->validationRules->only(
                    'daapi.callback.accounts.email',
                    ['required', 'string']
                ),
                'user.firstName'                 => $this->validationRules->only(
                    'daapi.callback.accounts.first_name',
                    ['required', 'string']
                ),
                'user.lastName'                  => $this->validationRules->only(
                    'daapi.callback.accounts.last_name',
                    ['required', 'string']
                ),
                'user.id'                        => $this->validationRules->only(
                    'daapi.callback.accounts.id',
                    ['required', 'string']
                ),
                'user.userStatus'                => $this->validationRules->only(
                    'daapi.callback.accounts.user_status',
                    ['required', 'string', 'in']
                ),
                'user.userType'                  => $this->validationRules->only(
                    'daapi.callback.accounts.user_type',
                    ['required', 'string', 'in']
                ),
                'account'                        => $this->validationRules->only(
                    'daapi.callback.accounts.account',
                    ['required', 'array']
                ),
                'account.id'                     => $this->validationRules->only(
                    'daapi.callback.accounts.account_id',
                    ['required', 'string']
                ),
                'account.accountType'            => $this->validationRules->only(
                    'daapi.callback.accounts.account_type',
                    ['required', 'string', 'in']
                ),
                'account.accountStatus'          => $this->validationRules->only(
                    'daapi.callback.accounts.account_status',
                    ['required', 'string', 'in']
                ),
                'account.companyName'            => $this->validationRules->only(
                    'daapi.callback.accounts.company_name',
                    ['required', 'string']
                ),
                'account.phoneNumber'            => $this->validationRules->only(
                    'daapi.callback.accounts.phone_number',
                    ['required', 'string']
                ),
                'account.companyAddress.line1'   => $this->validationRules->only(
                    'daapi.callback.accounts.line1',
                    ['required', 'string']
                ),
                'account.companyAddress.line2'   => $this->validationRules->only(
                    'daapi.callback.accounts.line2',
                    ['sometimes', 'string', 'nullable']
                ),
                'account.companyAddress.city'    => $this->validationRules->only(
                    'daapi.callback.accounts.city',
                    ['required', 'string']
                ),
                'account.companyAddress.state'   => $this->validationRules->only(
                    'daapi.callback.accounts.state',
                    ['required', 'string']
                ),
                'account.companyAddress.country' => $this->validationRules->only(
                    'daapi.callback.accounts.country',
                    ['required', 'string']
                ),
                'account.companyAddress.zipcode' => $this->validationRules->only(
                    'daapi.callback.accounts.zipcode',
                    ['required', 'string']
                ),
            ],
            //-------------ACCOUNT UPDATE CALLBACK-------------
            'account_update'           => [
                'message'                        => $this->validationRules->only(
                    'daapi.callback.accounts.message',
                    ['sometimes', 'string']
                ),
                'account'                        => $this->validationRules->only(
                    'daapi.callback.accounts.account',
                    ['required', 'array']
                ),
                'account.id'                     => $this->validationRules->only(
                    'daapi.callback.accounts.account_id',
                    ['required', 'string']
                ),
                'account.accountType'            => $this->validationRules->only(
                    'daapi.callback.accounts.account_type',
                    ['required', 'string', 'in']
                ),
                'account.accountStatus'          => $this->validationRules->only(
                    'daapi.callback.accounts.account_status',
                    ['required', 'string', 'in']
                ),
                'account.companyName'            => $this->validationRules->only(
                    'daapi.callback.accounts.company_name',
                    ['required', 'string']
                ),
                'account.phoneNumber'            => $this->validationRules->only(
                    'daapi.callback.accounts.phone_number',
                    ['required', 'string']
                ),
                'account.companyAddress.line1'   => $this->validationRules->only(
                    'daapi.callback.accounts.line1',
                    ['required', 'string']
                ),
                'account.companyAddress.line2'   => $this->validationRules->only(
                    'daapi.callback.accounts.line2',
                    ['sometimes', 'string', 'nullable']
                ),
                'account.companyAddress.city'    => $this->validationRules->only(
                    'daapi.callback.accounts.city',
                    ['required', 'string']
                ),
                'account.companyAddress.state'   => $this->validationRules->only(
                    'daapi.callback.accounts.state',
                    ['required', 'string']
                ),
                'account.companyAddress.country' => $this->validationRules->only(
                    'daapi.callback.accounts.country',
                    ['required', 'string']
                ),
                'account.companyAddress.zipcode' => $this->validationRules->only(
                    'daapi.callback.accounts.zipcode',
                    ['required', 'string']
                ),
            ],
            //-------------CAMPAIGN CREATE CALLBACK-------------
            'campaign_create'          => [
                'campaign'                                  => $this->validationRules->only(
                    'daapi.callback.campaigns.campaign',
                    ['required', 'array']
                ),
                'campaign.id'                               => $this->validationRules->only(
                    'daapi.callback.campaigns.id',
                    ['required', 'string']
                ),
                'campaign.operativeOrderId'                 => $this->validationRules->only(
                    'daapi.callback.campaigns.operative_order_id',
                    ['required', 'string']
                ),
                'campaign.sourceId'                         => $this->validationRules->only(
                    'daapi.callback.campaigns.source_id',
                    ['required', 'string']
                ),
                'campaign.name'                             => $this->validationRules->only(
                    'daapi.callback.campaigns.name',
                    ['required', 'string']
                ),
                'campaign.advertiserId'                     => $this->validationRules->only(
                    'daapi.callback.campaigns.advertiser_id',
                    ['required', 'string']
                ),
                'campaign.agencyId'                         => $this->validationRules->only(
                    'daapi.callback.campaigns.agency_id',
                    ['required', 'string']
                ),
                'campaign.status'                           => $this->validationRules->only(
                    'daapi.callback.campaigns.status',
                    ['required', 'string', 'in']
                ),
                'campaign.impressions'                      => $this->validationRules->only(
                    'daapi.callback.campaigns.impressions',
                    ['required', 'numeric']
                ),
                'campaign.lineItems'                        => $this->validationRules->only(
                    'daapi.callback.campaigns.line_items',
                    ['required', 'array']
                ),
                'campaign.lineItems.promoCode'              => $this->validationRules->only(
                    'daapi.callback.campaigns.line_items_promo_code',
                    ['sometimes', 'string', 'nullable']
                ),
                'campaign.lineItems.*.id'                   => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_id',
                    ['required', 'string']
                ),
                'campaign.lineItems.*.startDate'            => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_start_date',
                    ['required', 'string']
                ),
                'campaign.lineItems.*.endDate'              => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_end_date',
                    ['required', 'string']
                ),
                'campaign.lineItems.*.status'               => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_status',
                    ['required', 'string']
                ),
                'campaign.lineItems.*.unitCost'             => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_unit_cost',
                    ['required', 'numeric']
                ),
                'campaign.lineItems.*.discountedUnitCost'   => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_discounted_unit_cost',
                    ['sometimes', 'numeric', 'nullable']
                ),
                'campaign.lineItems.*.costModel'            => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_cost_model',
                    ['required', 'string', 'in']
                ),
                'campaign.lineItems.*.quantity'             => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_quantity',
                    ['required', 'numeric']
                ),
                'campaign.lineItems.*.quantityType'         => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_quantity_type',
                    ['required', 'string', 'in']
                ),
                'campaign.lineItems.*.deliveredImpressions' => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_delivered_impressions',
                    ['required', 'numeric']
                ),
                'campaign.lineItems.*.productId'            => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_product_id',
                    ['required', 'string']
                ),
                'campaign.lineItems.*.creative'             => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_creative',
                    ['sometimes', 'array', 'nullable']
                ),
                'campaign.lineItems.*.targets'              => $this->validationRules->only(
                    'daapi.callback.campaigns.line_item_targets',
                    ['sometimes', 'array']
                ),
            ],
            //-------------CAMPAIGN CANCEL CALLBACK-------------
            'campaign_cancel'          => [
                'campaignId' => $this->validationRules->only(
                    'daapi.callback.campaigns.id',
                    ['required', 'string']
                ),
            ],
            //-------------CREATIVE UPDATE CALLBACK-------------
            'creative_update'          => [
                'campaign'                      => $this->validationRules->only(
                    'daapi.callback.campaigns.campaign',
                    ['required', 'array']
                ),
                'campaign.id'                   => $this->validationRules->only(
                    'daapi.callback.campaigns.id',
                    ['required', 'string']
                ),
                'campaign.creative'             => $this->validationRules->only(
                    'daapi.callback.creatives.creative',
                    ['required', 'array']
                ),
                'campaign.creative.id'          => $this->validationRules->only(
                    'daapi.callback.creatives.id',
                    ['required', 'string']
                ),
                'campaign.creative.fileName'    => $this->validationRules->only(
                    'daapi.callback.creatives.file_name',
                    ['required', 'string']
                ),
                'campaign.creative.status'      => $this->validationRules->only(
                    'daapi.callback.creatives.status',
                    ['required', 'string', 'in']
                ),
                'campaign.creative.creativeUrl' => $this->validationRules->only(
                    'daapi.callback.creatives.creative_url',
                    ['required', 'string']
                ),
                'campaign.creative.accountId'   => $this->validationRules->only(
                    'daapi.callback.creatives.account_id',
                    ['required', 'string']
                ),
                'campaign.creative.reason'      => $this->validationRules->only(
                    'daapi.callback.creatives.reason',
                    ['sometimes', 'string', 'nullable']
                ),
                'campaign.creative.comment'     => $this->validationRules->only(
                    'daapi.callback.creatives.comment',
                    ['sometimes', 'string', 'nullable']
                ),
            ],
        ];
    }
}
