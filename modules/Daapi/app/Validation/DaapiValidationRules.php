<?php

namespace Modules\Daapi\Validation;

use App\Helpers\EnumHelper;
use Illuminate\Support\Arr;

trait DaapiValidationRules
{
    use EnumHelper;

    /**
     * Rules array used for DAAPI json validation
     *
     * @var array
     */
    protected $daapiRules = [
        'daapi' => [
            'callback'     => [
                'body'      => [
                    'request'     => [
                        'required' => true,
                        'array'    => true,
                    ],
                    'response'    => [
                        'required' => true,
                        'array'    => true,
                    ],
                    'payload'     => [
                        'required' => true,
                        'array'    => true,
                    ],
                    'id'          => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'status_code' => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'status_msg'  => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'duration'    => [
                        'required' => true,
                        'numeric'  => true,
                    ],
                    'received'    => [
                        'required' => true,
                        'string'   => true,
                    ],
                ],
                //-------------ACCOUNT CALLBACKS-------------
                'accounts'  => [
                    'message'         => [
                        'sometimes' => true,
                        'string'    => true,
                    ],
                    'user'            => [
                        'required' => true,
                        'array'    => true,
                    ],
                    'email'           => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'first_name'      => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'last_name'       => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'id'              => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'user_status'     => [
                        'required' => true,
                        'string'   => true,
                        'in'       => [
                            // To be filled
                        ],
                    ],
                    'user_type'       => [
                        'required' => true,
                        'string'   => true,
                        'in'       => [
                            // To be filled
                        ],
                    ],
                    'account'         => [
                        'required' => true,
                        'array'    => true,
                    ],
                    'account_id'      => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'account_type'    => [
                        'required' => true,
                        'string'   => true,
                        'in'       => [
                            // To be filled
                        ],
                    ],
                    'account_status'  => [
                        'required' => true,
                        'string'   => true,
                        'in'       => [
                            // To be filled
                        ],
                    ],
                    'company_name'    => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'company_address' => [
                        'required' => true,
                        'array'    => true,
                    ],
                    'line1'           => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'line2'           => [
                        'sometimes' => true,
                        'string'    => true,
                        'nullable'  => true,
                    ],
                    'city'            => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'state'           => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'country'         => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'zipcode'         => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'phone_number'    => [
                        'required' => true,
                        'string'   => true,
                    ],
                ],
                //-------------CAMPAIGN CALLBACKS-------------
                'campaigns' => [
                    'campaign'                        => [
                        'required' => true,
                        'array'    => true,
                    ],
                    'id'                              => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'operative_order_id'              => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'source_id'                       => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'name'                            => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'advertiser_id'                   => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'agency_id'                       => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'status'                          => [
                        'required' => true,
                        'string'   => true,
                        'in'       => [
                            // To be filled

                        ],
                    ],
                    'impressions'                     => [
                        'required' => true,
                        'numeric'  => true,
                    ],
                    'line_items'                      => [
                        'required' => true,
                        'array'    => true,
                    ],
                    'line_items_promo_code'           => [
                        'sometimes' => true,
                        'string'    => true,
                        'nullable'  => true,
                    ],
                    'line_item_id'                    => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'line_item_start_date'            => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'line_item_end_date'              => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'line_item_status'                => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'line_item_unit_cost'             => [
                        'required' => true,
                        'numeric'  => true,
                    ],
                    'line_item_discounted_unit_cost'  => [
                        'sometimes' => true,
                        'numeric'   => true,
                        'nullable'  => true,
                    ],
                    'line_item_cost_model'            => [
                        'required' => true,
                        'string'   => true,
                        'in'       => [
                            // To be filled
                        ],
                    ],
                    'line_item_quantity'              => [
                        'required' => true,
                        'numeric'  => true,
                    ],
                    'line_item_quantity_type'         => [
                        'required' => true,
                        'string'   => true,
                        'in'       => [
                            // To be filled
                        ],
                    ],
                    'line_item_delivered_impressions' => [
                        'required' => true,
                        'numeric'  => true,
                    ],
                    'line_item_product_id'            => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'line_item_creative'              => [
                        'sometimes' => true,
                        'array'     => true,
                        'nullable'  => true,
                    ],
                    'line_item_targets'               => [
                        'sometimes' => true,
                        'array'     => true,
                    ],
                ],
                //-------------CREATIVE CALLBACKS-------------
                'creatives' => [
                    'creative'     => [
                        'required' => true,
                        'array'    => true,
                    ],
                    'id'           => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'file_name'    => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'extension'    => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'size'         => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'duration'     => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'created_at'   => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'status'       => [
                        'required' => true,
                        'string'   => true,
                        'in'       => [
                            // To be filled
                        ],
                    ],
                    'creative_url' => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'account_id'   => [
                        'required' => true,
                        'string'   => true,
                    ],
                    'reason'       => [
                        'sometimes' => true,
                        'string'    => true,
                        'nullable'  => true,
                    ],
                    'comment'      => [
                        'sometimes' => true,
                        'string'    => true,
                        'nullable'  => true,
                    ],
                ],
            ],
            //-------------STATUS NOTIFICATIONS-------------
            'notification' => [
                'statuses'  => [
                    'allowed_attributes' => [
                        'accounts',
                        'orders',
                        'campaigns',
                        'payments',
                        'creatives',
                    ],
                ],
                'accounts'  => [
                    'sometimes' => true,
                    'required'  => true,
                    'array'     => true,
                ],
                'orders'    => [
                    'sometimes' => true,
                    'required'  => true,
                    'array'     => true,
                ],
                'campaigns' => [
                    'sometimes' => true,
                    'required'  => true,
                    'array'     => true,
                ],
                'payments'  => [
                    'sometimes' => true,
                    'required'  => true,
                    'array'     => true,
                ],
                'creatives' => [
                    'sometimes' => true,
                    'required'  => true,
                    'array'     => true,
                ],
            ],
            'request'      => [
                'required' => true,
                'array'    => true,
                'min'      => 2,
                'max'      => 999,
            ],
            'payload'      => [
                'required' => true,
                'array'    => true,
                'min'      => 1,
                'max'      => 999,
            ],
            'id'           => [
                'required' => true,
                'uuid'     => true,
            ],
        ],
    ];

    /**
     * @return array
     */
    public function getDaapiRules(): array
    {
        $accountTypes = $this->getAccountTypes();
        $userStatuses = $this->getUserStatuses();
        $userTypes = $this->getUserTypes();
        $costModels = $this->getCostModels();
        $quantityTypes = $this->getQuantityTypes();
        $accountStatuses = $this->getAccountStatuses();
        $campaignStatuses = $this->getCampaignStatuses();
        $creativeStatuses = $this->getCreativeStatuses();

        $rules = $this->daapiRules;

        Arr::set($rules, 'daapi.callback.accounts.account_status.in', $accountStatuses);
        Arr::set($rules, 'daapi.callback.accounts.account_type.in', $accountTypes);
        Arr::set($rules, 'daapi.callback.accounts.user_status.in', $userStatuses);
        Arr::set($rules, 'daapi.callback.accounts.user_type.in', $userTypes);
        Arr::set($rules, 'daapi.callback.campaigns.status.in', $campaignStatuses);
        Arr::set($rules, 'daapi.callback.campaigns.line_item_cost_model.in', $costModels);
        Arr::set($rules, 'daapi.callback.campaigns.line_item_quantity_type.in', $quantityTypes);
        Arr::set($rules, 'daapi.callback.creatives.status.in', $creativeStatuses);

        return $rules;
    }
}
