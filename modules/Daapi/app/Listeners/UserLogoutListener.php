<?php

namespace Modules\Daapi\Listeners;

use Illuminate\Auth\Events\Logout;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\Daapi\Actions\Cookie\ForgetTokenCookieAction;

class UserLogoutListener
{
    /**
     * @var ForgetTokenCookieAction
     */
    private $forgetTokenCookieAction;

    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @param ForgetTokenCookieAction $forgetTokenCookieAction
     * @param AuthService             $authService
     */
    public function __construct(ForgetTokenCookieAction $forgetTokenCookieAction, AuthService $authService)
    {
        $this->forgetTokenCookieAction = $forgetTokenCookieAction;
        $this->authService             = $authService;
    }


    /**
     * @param Logout $event
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function handle(Logout $event): void
    {
        $this->forgetTokenCookieAction->handle();
        $this->authService->flushTokens($event->user->getKey());
    }
}
