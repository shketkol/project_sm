<?php

namespace Modules\Daapi\Listeners\Notifications;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\Alerts\SetFailedPaymentAlertAction;
use Modules\Daapi\Events\Order\OrderNotificationEvent;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use Modules\Daapi\Exceptions\StatusNotFoundException;
use Modules\Daapi\Exceptions\StatusUpdateFailedException;
use Modules\Haapi\Helpers\HaapiResponseHelper;
use Modules\Payment\Actions\Notifications\ProcessOrderStatusAction;
use Modules\User\Actions\Notifications\UpdateUserStatusAction;
use Modules\User\Actions\Payment\SetRetryPaymentAction;
use Modules\User\Actions\Payment\UnsetRetryPaymentAction;
use Modules\User\Exceptions\UserNotFoundException;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;
use Modules\User\Repositories\Contracts\UserRepository;
use Psr\Log\LoggerInterface;
use SM\SMException;

class OrderNotificationListener
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UpdateUserStatusAction
     */
    private $updateUserStatusAction;

    /**
     * @var ProcessOrderStatusAction
     */
    private $processOrderStatusAction;

    /**
     * @var SetRetryPaymentAction
     */
    private $setRetryPaymentAction;

    /**
     * @var UnsetRetryPaymentAction
     */
    private $unsetRetryPaymentAction;

    /**
     * @var SetFailedPaymentAlertAction
     */
    private $setFailedPaymentAlertAction;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * OrderNotificationListener constructor.
     *
     * @param DatabaseManager $databaseManager
     * @param LoggerInterface $log
     * @param UpdateUserStatusAction $updateUserStatusAction
     * @param ProcessOrderStatusAction $processOrderStatusAction
     * @param SetRetryPaymentAction $setRetryPaymentAction
     * @param UnsetRetryPaymentAction $unsetRetryPaymentAction
     * @param SetFailedPaymentAlertAction $setFailedPaymentAlertAction
     * @param UserRepository $userRepository
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        UpdateUserStatusAction $updateUserStatusAction,
        ProcessOrderStatusAction $processOrderStatusAction,
        SetRetryPaymentAction $setRetryPaymentAction,
        UnsetRetryPaymentAction $unsetRetryPaymentAction,
        SetFailedPaymentAlertAction $setFailedPaymentAlertAction,
        UserRepository $userRepository
    ) {
        $this->databaseManager                = $databaseManager;
        $this->log                            = $log;
        $this->updateUserStatusAction         = $updateUserStatusAction;
        $this->processOrderStatusAction       = $processOrderStatusAction;
        $this->setRetryPaymentAction          = $setRetryPaymentAction;
        $this->unsetRetryPaymentAction        = $unsetRetryPaymentAction;
        $this->setFailedPaymentAlertAction    = $setFailedPaymentAlertAction;
        $this->userRepository                 = $userRepository;
    }

    /**
     * @param OrderNotificationEvent $event
     *
     * @throws StatusUpdateFailedException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function handle(OrderNotificationEvent $event): void
    {
        $this->log->info('Started sending order status notifications.', [
            'payload' => $event->payload,
        ]);

        try {
            $this->execute($event);
        } catch (\Throwable $exception) {
            $message = 'Failed sending order status notifications.';
            $this->log->warning($message, [
                'reason' => $exception->getMessage(),
            ]);

            throw StatusUpdateFailedException::create($message, $exception);
        }

        $this->log->info('Finished sending order status notifications.', [
            'payload' => $event->payload,
        ]);
    }

    /**
     * @param OrderNotificationEvent $event
     *
     * @throws CanNotApplyStatusException
     * @throws SMException
     * @throws UserNotFoundException
     * @throws \Throwable
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private function execute(OrderNotificationEvent $event): void
    {
        $accountStatusPayload = Arr::get($event->payload, 'accounts');
        $accountStatusPayload = array_shift($accountStatusPayload);

        $ordersStatusPayload = Arr::get($event->payload, 'orders');

        foreach ($ordersStatusPayload as $value) {
            $this->databaseManager->beginTransaction();

            try {
                $id = Arr::get($value, 'id');
                $status = Arr::get($value, 'status');

                $this->validateOrderStatus($status);

                $this->processOrderStatusAction->handle($id, $status);
            } catch (\Throwable $exception) {
                $this->log->warning("Failed to send notification while change status, order with ID {$id}.", [
                    'to_status' => $status,
                ]);

                $this->databaseManager->rollBack();

                throw $exception;
            }
            $this->databaseManager->commit();
        }

        $this->databaseManager->beginTransaction();

        try {
            $accountExternalId = Arr::get($accountStatusPayload, 'id');
            $user = $this->userRepository->getByAccountExternalId($accountExternalId);
            $this->updateUserStatusAction->handle(
                $accountExternalId,
                Arr::get($accountStatusPayload, 'status')
            );

            $this->handleAlerts(
                $accountStatusPayload,
                $ordersStatusPayload
            );

            $this->updateRetryPaymentStatus(
                $user,
                $accountStatusPayload,
                $ordersStatusPayload
            );
        } catch (\Throwable $exception) {
            $this->log->warning("Failed to update account status.");

            $this->databaseManager->rollBack();

            throw $exception;
        }
        $this->databaseManager->commit();
    }

    /**
     * @param array $accountStatusPayload
     *
     * @return bool
     */
    protected function isUserStatusUnpaid(array $accountStatusPayload): bool
    {
        $userStatus = Arr::get($accountStatusPayload, 'status');
        return User::getStatusIdByTransactionFlow($userStatus, 'user') === UserStatus::ID_INACTIVE_UNPAID;
    }

    /**
     * @param array $accountStatusPayload
     * @param array $ordersStatusPayload
     *
     * @return void
     */
    protected function handleAlerts(
        array $accountStatusPayload,
        array $ordersStatusPayload
    ): void {
        if ($this->isUserStatusUnpaid($accountStatusPayload)) {
            $this->setFailedPaymentAlertAction->handle($ordersStatusPayload);
        }
    }

    /**
     * @param User   $user
     * @param array  $accountStatusPayload
     * @param array  $ordersStatusPayload
     *
     * @return void
     */
    private function updateRetryPaymentStatus(
        User $user,
        array $accountStatusPayload,
        array $ordersStatusPayload
    ): void {
        $canRetryPayment = HaapiResponseHelper::valueToBoolean(
            Arr::get($accountStatusPayload, 'manualRetryPayment')
        );

        if ($this->isUserStatusUnpaid($accountStatusPayload)) {
            $this->setRetryPaymentAction->handle($user, $canRetryPayment, $ordersStatusPayload);
        } else {
            $this->unsetRetryPaymentAction->handle($user);
        }

        $user->save();
    }

    /**
     * @param string $status
     *
     * @throws StatusNotFoundException
     */
    private function validateOrderStatus(string $status)
    {
        $statuses = array_keys(config('daapi.status_mapping.campaign_payment'));

        if (!in_array(strtolower($status), $statuses)) {
            $this->log->warning('Status not found.', [
                'status' => $status,
            ]);

            throw StatusNotFoundException::create($status);
        }
    }
}
