<?php

namespace Modules\Daapi\Listeners\Notifications;

use Modules\Daapi\Actions\ActAsAdmin;

class BaseNotificationListener
{
    use ActAsAdmin;
}
