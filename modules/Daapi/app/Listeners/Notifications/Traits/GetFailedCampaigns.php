<?php

namespace Modules\Daapi\Listeners\Notifications\Traits;

use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;
use Modules\Campaign\Repositories\Contracts\CampaignRepository;
use Modules\Payment\Models\OrderStatus;

trait GetFailedCampaigns
{
    /**
     * @param array $ordersData
     *
     * @return Collection
     */
    protected function getFailedCampaigns(array $ordersData): Collection
    {
        $campaignRepository = app(CampaignRepository::class);
        $failedOrderIds = $this->getFailedOrderIds($ordersData);
        return $campaignRepository->findWhereIn('order_id', $failedOrderIds);
    }

    /**
     * @param array $ordersData
     *
     * @return array
     */
    protected function getFailedOrderIds(array $ordersData): array
    {
        $failedStatus = OrderStatus::getHaapiStatusName(OrderStatus::FAILED);
        return Arr::pluck(
            array_filter($ordersData, function ($item) use ($failedStatus): bool {
                return strtolower($item['status']) === $failedStatus;
            }),
            'id'
        );
    }
}
