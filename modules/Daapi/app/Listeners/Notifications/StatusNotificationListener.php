<?php

namespace Modules\Daapi\Listeners\Notifications;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Daapi\Events\StatusNotificationEvent;
use Modules\Daapi\Exceptions\NotificationTypeNotFoundException;
use Modules\Daapi\Exceptions\StatusUpdateFailedException;
use Psr\Log\LoggerInterface;

class StatusNotificationListener
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param DatabaseManager $databaseManager
     * @param LoggerInterface $log
     */
    public function __construct(DatabaseManager $databaseManager, LoggerInterface $log)
    {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
    }

    /**
     * @param StatusNotificationEvent $event
     *
     * @throws \Throwable
     */
    public function handle(StatusNotificationEvent $event): void
    {
        $this->log->info('Started updating status notifications.', [
            'payload' => $event->payload,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $this->execute($event);
        } catch (\Throwable $exception) {
            $message = 'Failed updating status notifications.';
            $this->log->warning($message, [
                'reason' => $exception->getMessage(),
            ]);

            $this->databaseManager->rollBack();

            throw StatusUpdateFailedException::create($message, $exception);
        }

        $this->databaseManager->commit();

        $this->log->info('Finished updating status notifications.', [
            'payload' => $event->payload,
        ]);
    }

    /**
     * @param StatusNotificationEvent $event
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws NotificationTypeNotFoundException
     */
    private function execute(StatusNotificationEvent $event): void
    {
        foreach ($event->payload as $type => $statusArray) {
            $path = 'daapi.notification.status.' . $type;
            $class = config($path);

            if (is_null($class)) {
                $this->log->warning('Status not found.', [
                    'status'      => $type,
                    'config_path' => $path,
                ]);

                throw NotificationTypeNotFoundException::create($type);
            }

            /** @var \Modules\Daapi\Actions\Notification\UpdateStatusBaseAction $action */
            $action = app()->make($class);

            foreach ($statusArray as $value) {
                $action->handle(
                    Arr::get($value, 'id'),
                    Arr::get($value, 'status'),
                    Arr::only($value, ['reason', 'comments'])
                );
            }
        }
    }
}
