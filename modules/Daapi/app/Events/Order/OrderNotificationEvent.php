<?php

namespace Modules\Daapi\Events\Order;

class OrderNotificationEvent
{
    /**
     * @var array
     */
    public $payload;

    /**
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }
}
