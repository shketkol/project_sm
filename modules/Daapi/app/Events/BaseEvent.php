<?php

namespace Modules\Daapi\Events;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class BaseEvent
{
    use Dispatchable, SerializesModels;

    /**
     * Key to retrieve status code
     */
    public const STATUS_KEY = 'haapi.response.statusCode';

    /**
     * All response array
     *
     * @var mixed
     */
    protected $haapi;

    /**
     * BaseEvent constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->haapi = Arr::get($request->all(), 'haapi', []);
    }

    /**
     * Get payload from request
     *
     * @return array
     */
    protected function getPayload(): array
    {
        return $this->getParam('response.payload');
    }

    /**
     * Get Reject Reason
     *
     * @return string
     */
    public function getReason(): string
    {
        return Arr::get($this->haapi, 'response.payload.reason', '');
    }

    /**
     * Get params from response haapi
     *
     * @param string $key
     * @return mixed
     */
    protected function getParam(string $key)
    {
        return Arr::get($this->haapi, $key, []);
    }

    /**
     * Response status code
     *
     * @return int
     */
    public function status(): int
    {
        return (int)$this->getParam('response.statusCode');
    }

    /**
     * Response message
     *
     * @return string
     */
    public function message(): string
    {
        return $this->getParam('response.statusMsg');
    }

    /**
     * Request Id
     *
     * @return string
     */
    public function requestId(): string
    {
        return $this->getParam('request.id');
    }

    /**
     * Request type
     *
     * @return string
     */
    public function type(): string
    {
        return $this->getParam('request.type');
    }

    /**
     * Request payload
     *
     * @return array
     */
    public function requestPayload(): array
    {
        return $this->getParam('request.payload');
    }

    /**
     * Response Id
     *
     * @return string
     */
    public function responseId(): string
    {
        return $this->getParam('response.id');
    }
}
