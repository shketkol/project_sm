<?php

namespace Modules\Daapi\Events\Creative;

use Illuminate\Http\Request;
use Modules\Daapi\DataTransferObjects\CreativeUpdateData;
use Modules\Daapi\DataTransferObjects\Types\CreativeUpdate\CreativeData;
use Modules\Daapi\Events\BaseEvent;

/**
 * @package Modules\Daapi\Events\Creative
 */
class CreativeUpdated extends BaseEvent
{
    /**
     * Request data
     *
     * @var CreativeUpdateData
     */
    private $data;

    /**
     * Update a new event instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = new CreativeUpdateData($this->getPayload());
    }

    /**
     * Updated creative's campaign id
     *
     * @return string
     */
    public function getCampaignId(): string
    {
        return $this->data->campaign->id;
    }

    /**
     * Updated creative
     *
     * @return CreativeData
     */
    public function getCreative(): CreativeData
    {
        return $this->data->campaign->creative;
    }
}
