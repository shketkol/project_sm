<?php

namespace Modules\Daapi\Events;

use Illuminate\Support\Arr;
use Modules\Daapi\DataTransferObjects\UserCreateData;

class RejectedEvent extends BaseEvent
{
    /**
     * @var UserCreateData
     */
    protected $data;

    /**
     * Get payload from request
     *
     * @return array
     */
    protected function getPayload(): array
    {
        return $this->getParam('request.payload');
    }

    /**
     * Response message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->data->message;
    }

    /**
     * Get Reject Reason
     *
     * @return string
     */
    public function getReason(): string
    {
        return Arr::get($this->haapi, 'response.payload.reason', '');
    }
}
