<?php

namespace Modules\Daapi\Events\Account;

use Illuminate\Http\Request;
use Modules\Daapi\DataTransferObjects\AccountCreateData;
use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Daapi\Events\BaseEvent;

class AccountCreated extends BaseEvent
{
    /**
     * @var AccountCreateData
     */
    private $data;

    /**
     * AccountCreated constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = new AccountCreateData($this->getPayload());
    }

    /**
     * Response message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->data->message;
    }

    /**
     * Created Account
     *
     * @return AccountData
     */
    public function getAccount(): AccountData
    {
        return $this->data->account;
    }

    /**
     * Created User
     *
     * @return UserData
     */
    public function getUser(): UserData
    {
        return $this->data->user;
    }
}
