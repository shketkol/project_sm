<?php

namespace Modules\Daapi\Events\Account;

use Illuminate\Http\Request;
use Modules\Daapi\DataTransferObjects\AccountUpdateData;
use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Modules\Daapi\Events\BaseEvent;

class AccountUpdated extends BaseEvent
{
    /**
     * @var AccountUpdateData
     */
    private $data;

    /**
     * AccountCreated constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = new AccountUpdateData($this->getPayload());
    }

    /**
     * Response message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->data->message;
    }

    /**
     * Created Account
     *
     * @return AccountData
     */
    public function getAccount(): AccountData
    {
        return $this->data->account;
    }
}
