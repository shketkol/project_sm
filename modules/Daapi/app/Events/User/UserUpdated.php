<?php

namespace Modules\Daapi\Events\User;

use Illuminate\Http\Request;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Daapi\DataTransferObjects\UserUpdateData;
use Modules\Daapi\Events\BaseEvent;

/**
 * Class UserCreated
 *
 * @package Modules\Daapi\Events\User
 */
class UserUpdated extends BaseEvent
{
    /**
     * @var UserUpdateData
     */
    private $data;

    /**
     * Create a new event instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = new UserUpdateData($this->getPayload());
    }

    /**
     * Mapped response message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->data->message;
    }

    /**
     * Created User
     *
     * @return UserData
     */
    public function getUser(): UserData
    {
        return $this->data->user;
    }
}
