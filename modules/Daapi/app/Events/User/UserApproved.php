<?php

namespace Modules\Daapi\Events\User;

use Modules\Daapi\Events\BaseEvent;
use Illuminate\Http\Request;
use Modules\Daapi\DataTransferObjects\Types\UserDataPlain;
use Modules\Daapi\DataTransferObjects\UserCreateData;
use Modules\Daapi\DataTransferObjects\Types\AccountData;

class UserApproved extends BaseEvent
{
    /**
     * @var UserCreateData
     */
    private $data;

    /**
     * AccountCreated constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->data = new UserCreateData($this->getPayload());
    }

    /**
     * Created Account
     *
     * @return AccountData
     */
    public function getAccount(): AccountData
    {
        return $this->data->account;
    }

    /**
     * Created User
     *
     * @return UserDataPlain
     */
    public function getUser(): UserDataPlain
    {
        return $this->data->user;
    }
}
