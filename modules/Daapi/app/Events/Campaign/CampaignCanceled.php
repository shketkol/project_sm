<?php

namespace Modules\Daapi\Events\Campaign;

use Illuminate\Http\Request;
use Modules\Daapi\DataTransferObjects\CampaignCancelData;
use Modules\Daapi\Events\BaseEvent;

/**
 * @package Modules\Daapi\Events\User
 */
class CampaignCanceled extends BaseEvent
{
    /**
     * Request data
     *
     * @var CampaignCancelData
     */
    private $data;

    /**
     * Create a new event instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = new CampaignCancelData($this->getPayload());
    }

    /**
     * Canceled campaign id
     *
     * @return string
     */
    public function getCampaignId(): string
    {
        return $this->data->campaignId;
    }
}
