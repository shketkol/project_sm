<?php

namespace Modules\Daapi\Events\Campaign;

use Illuminate\Http\Request;
use Modules\Daapi\DataTransferObjects\CampaignCreateData;
use Modules\Daapi\DataTransferObjects\Types\CampaignData;
use Modules\Daapi\Events\BaseEvent;

/**
 * @package Modules\Daapi\Events\User
 */
class CampaignCreated extends BaseEvent
{
    /**
     * Request data
     *
     * @var CampaignCreateData
     */
    private $data;

    /**
     * Create a new event instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = new CampaignCreateData($this->getPayload());
    }

    /**
     * Created campaign data
     *
     * @return CampaignData
     */
    public function getCampaign(): CampaignData
    {
        return $this->data->campaign;
    }
}
