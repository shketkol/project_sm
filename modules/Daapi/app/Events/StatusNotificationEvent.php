<?php

namespace Modules\Daapi\Events;

class StatusNotificationEvent
{
    /**
     * @var array
     */
    public $payload;

    /**
     * StatusNotificationEvent constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }
}
