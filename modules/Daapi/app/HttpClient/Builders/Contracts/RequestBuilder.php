<?php

namespace Modules\Daapi\HttpClient\Builders\Contracts;

use Modules\Daapi\HttpClient\Requests\DaapiRequest;

interface RequestBuilder
{
    /**
     * @param array $data
     *
     * @return DaapiRequest
     */
    public function make(array $data): DaapiRequest;
}
