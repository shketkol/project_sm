<?php

namespace Modules\Daapi\HttpClient\Builders\Contracts;

use Modules\Daapi\HttpClient\Requests\DaapiRequest;
use Modules\Daapi\HttpClient\Responses\DaapiResponse;

interface ResponseBuilder
{
    /**
     * @param DaapiRequest $request
     * @param int          $statusCode
     * @param array        $payload
     *
     * @return DaapiResponse
     */
    public function make(DaapiRequest $request, int $statusCode, array $payload): DaapiResponse;
}
