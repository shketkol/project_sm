<?php

namespace Modules\Daapi\HttpClient\Builders;

use Illuminate\Support\Arr;
use Modules\Daapi\HttpClient\Requests\DaapiRequest;
use Modules\Daapi\HttpClient\Builders\Contracts\RequestBuilder;

class DaapiRequestBuilder implements RequestBuilder
{
    /**
     * @var DaapiRequest
     */
    protected $request;

    /**
     * @param DaapiRequest $request
     */
    public function __construct(DaapiRequest $request)
    {
        $this->request = $request;
    }

    /**
     * @param array $data
     *
     * @return DaapiRequest
     */
    public function make(array $data): DaapiRequest
    {
        $body = Arr::get($data, 'request');

        $this->request->setId(Arr::get($body, 'id', ''));
        $this->request->setPayload(Arr::get($body, 'payload', []));
        $this->request->setBody([
            'request' => [
                'id'      => $this->request->getId(),
                'payload' => $this->request->getPayload(),
            ],
        ]);

        return $this->request;
    }
}
