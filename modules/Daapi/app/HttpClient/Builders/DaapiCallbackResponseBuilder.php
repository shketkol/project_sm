<?php

namespace Modules\Daapi\HttpClient\Builders;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class DaapiCallbackResponseBuilder
{
    /**
     * @param Request $request
     * @param int     $statusCode
     * @param array   $payload
     *
     * @return JsonResponse
     */
    public function make(Request $request, int $statusCode, array $payload): JsonResponse
    {
        return response()->json(
            $this->getResponseArray(
                $request->toArray(),
                $statusCode,
                $payload
            ),
            $statusCode
        );
    }

    /**
     * @param array $requestArray
     * @param       $statusCode
     * @param       $payload
     *
     * @return array
     */
    public function getResponseArray(array $requestArray, $statusCode, $payload): array
    {
        return [
            'request'  => [
                'haapiRequestId' => Arr::get($requestArray, 'haapi.request.id'),
            ],
            'response' => [
                'haapiResponseId' => Arr::get($requestArray, 'haapi.response.id'),
                'statusCode'      => $statusCode,
                'payload'         => $payload,
            ],
        ];
    }
}
