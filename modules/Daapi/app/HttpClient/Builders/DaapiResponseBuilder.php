<?php

namespace Modules\Daapi\HttpClient\Builders;

use Illuminate\Support\Arr;
use Modules\Daapi\HttpClient\Builders\Contracts\ResponseBuilder;
use Modules\Daapi\HttpClient\Requests\DaapiRequest;
use Modules\Daapi\HttpClient\Responses\DaapiResponse;
use Ramsey\Uuid\Uuid;

class DaapiResponseBuilder implements ResponseBuilder
{
    /**
     * @var DaapiResponse
     */
    private $daapiResponse;

    /**
     * DaapiResponseBuilder constructor.
     */
    public function __construct()
    {
        $this->daapiResponse = new DaapiResponse();
    }

    /**
     * @param DaapiRequest $request
     * @param int          $statusCode
     * @param array        $payload
     *
     * @return DaapiResponse
     */
    public function make(DaapiRequest $request, int $statusCode, array $payload): DaapiResponse
    {
        $this->daapiResponse->setId(Uuid::uuid4()->toString());
        $this->daapiResponse->setRequest($request->getBody());
        $this->daapiResponse->setPayload($payload);
        $this->daapiResponse->setStatusCode($statusCode);
        $this->daapiResponse->setBody(
            [
                'request' => [
                    'id' => Arr::get($request->getBody(), 'request.id'),
                    'payload' => $request->getPayload()
                ],
                'response' => [
                    'id' => $this->daapiResponse->getId(),
                    'statusCode' => $statusCode,
                    'payload' => $payload
                ]
            ]
        );

        return $this->daapiResponse;
    }
}
