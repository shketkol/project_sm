<?php

namespace Modules\Daapi\HttpClient\Requests;

use Illuminate\Support\Arr;

class DaapiRequest
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var array
     */
    private $payload;

    /**
     * @var array
     */
    private $body;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * @param array $body
     */
    public function setBody(array $body): void
    {
        $this->body = $body;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key)
    {
        return Arr::get($this->payload, $key);
    }

    /**
     * @param string $key
     * @param        $value
     */
    public function set(string $key, $value)
    {
        Arr::set($this->payload, $key, $value);
    }
}
