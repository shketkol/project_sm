<?php

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Invitation\Models\Invitation;
use Psr\Log\LoggerInterface;

class UniqueEmails extends Migration
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->databaseManager = app(DatabaseManager::class);
        $this->log = app(LoggerInterface::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up(): void
    {
        $this->log->info('[Invitation][Migration] Started removing duplicated emails.');
        $this->databaseManager->beginTransaction();

        try {
            $invitations = $this->findDuplicatedInvitations();
            if ($invitations->isNotEmpty()) {
                $this->deleteDuplicatedInvitations($invitations);
            }

            $this->makeEmailUniqueColumn();

            $this->databaseManager->commit();
        } catch (\Throwable $exception) {
            $this->log->warning('[Invitation][Migration] Failed removing duplicated emails.');
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->log->info('[Invitation][Migration] Finished removing duplicated emails.');
    }

    /**
     * @return Collection
     */
    private function findDuplicatedInvitations(): Collection
    {
        $duplicates = Invitation::query()
            ->select([
                'email',
                DB::raw('COUNT(email) as count'),
            ])
            ->groupBy('email')
            ->having('count', '>', 1)
            ->get()
            ->pluck('email')
            ->toArray();

        $invitations = Invitation::query()
            ->whereIn('email', $duplicates)
            ->get()
            ->groupBy('email');

        if ($invitations->isEmpty()) {
            $this->log->info('[Invitation][Migration] Not found duplicated emails.', [
                'invitations' => $invitations->toArray(),
            ]);

            return $invitations;
        }

        $this->log->info('[Invitation][Migration] Found duplicated emails.', [
            'invitations' => $invitations->toArray(),
        ]);

        return $invitations;
    }

    /**
     * We will keep last record since it is would be newest email in advertiser email box
     * And delete duplicated invitations
     *
     * @param Collection|Invitation[][] $invitations
     *
     * @throws Exception
     */
    private function deleteDuplicatedInvitations(Collection $invitations): void
    {
        $invitations->each(function (Collection $models): void {
            $models->reverse()->values()->each(function (Invitation $invitation, int $key): void {
                // keep last, zero because collection is reversed
                if ($key === 0) {
                    return;
                }

                $this->deleteInvitation($invitation);
            });
        });
    }

    /**
     * @param Invitation $invitation
     *
     * @throws Exception
     */
    private function deleteInvitation(Invitation $invitation): void
    {
        $this->log->info('[Invitation][Migration] Duplicated invitation to be deleted.', [
            'invitation' => $invitation->toArray(),
        ]);

        $invitation->delete();

        $this->log->info('[Invitation][Migration] Duplicated invitation deleted.');
    }

    /**
     * Migration
     */
    private function makeEmailUniqueColumn(): void
    {
        $this->log->info('[Invitation][Migration] Started making email unique column.');

        Schema::table('invitations', function (Blueprint $table): void {
            $table->unique('email');
        });

        $this->log->info('[Invitation][Migration] Finished making email unique column.');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('invitations', function (Blueprint $table): void {
            $table->dropUnique(['email']);
        });
    }
}
