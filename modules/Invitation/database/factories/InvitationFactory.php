<?php

namespace Modules\Invitation\Database\Factories;

use App\Factories\Factory;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Models\InvitationStatus;

class InvitationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invitation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'status_id' => $this->faker->randomElement([
                InvitationStatus::ID_INVITED,
                InvitationStatus::ID_PROCESSING,
                InvitationStatus::ID_REVOKED,
                InvitationStatus::ID_COMPLETED,
            ]),
            'code'      => $this->faker->regexify('[A-Za-z0-9]{10}'),
            'email'     => $this->faker->email,
        ];
    }
}
