<?php

namespace Modules\Invitation\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Invitation\Models\InvitationStatus;

class InvitationStatusesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $statuses = [
            [
                'id'   => InvitationStatus::ID_INVITED,
                'name' => InvitationStatus::INVITED,
            ],
            [
                'id'   => InvitationStatus::ID_PROCESSING,
                'name' => InvitationStatus::PROCESSING,
            ],
            [
                'id'   => InvitationStatus::ID_REVOKED,
                'name' => InvitationStatus::REVOKED,
            ],
            [
                'id'   => InvitationStatus::ID_COMPLETED,
                'name' => InvitationStatus::COMPLETED,
            ],
            [
                'id'   => InvitationStatus::ID_OPENED,
                'name' => InvitationStatus::OPENED,
            ],
            [
                'id'   => InvitationStatus::ID_IGNORED,
                'name' => InvitationStatus::IGNORED,
            ],
        ];

        foreach ($statuses as $status) {
            InvitationStatus::updateOrCreate(
                ['id' => Arr::get($status, 'id')],
                $status
            );
        }
    }
}
