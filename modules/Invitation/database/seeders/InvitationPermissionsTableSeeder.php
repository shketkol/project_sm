<?php

namespace Modules\Invitation\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Invitation\Policies\InvitationPolicy;
use Modules\User\Models\Role;

class InvitationPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN      => [
            InvitationPolicy::PERMISSION_LIST_INVITATION,
            InvitationPolicy::PERMISSION_CREATE_INVITATION,
            InvitationPolicy::PERMISSION_DELETE_INVITATION,
        ],
        Role::ID_ADMIN_READ_ONLY => [
            InvitationPolicy::PERMISSION_LIST_INVITATION,
            InvitationPolicy::PERMISSION_CREATE_INVITATION,
            InvitationPolicy::PERMISSION_DELETE_INVITATION,
        ]
    ];
}
