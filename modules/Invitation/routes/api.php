<?php

/**
 * Statistic routes
 */
Route::group([
    'middleware' => 'auth:admin',
], function () {
    // Store individual
    Route::post('/', 'StoreInvitationController')
        ->name('store')
        ->middleware('can:invitation.create');

    // Store bulk
    Route::post('/store-bulk', 'StoreBulkInvitationController')
        ->name('storeBulk')
        ->middleware('can:invitation.create');

    // Validate bulk
    Route::post('/validate-bulk', 'ValidateBulkInvitationController')
        ->name('validateBulk')
        ->middleware('can:invitation.create');

    // Validate csv
    Route::post('/validate-csv', 'ValidateCsvController')
        ->name('validateCsv')
        ->middleware('can:invitation.create');

    Route::group([
        'prefix' => '/{invitation}',
        'where'  => [
            'creative' => '[0-9]+',
        ],
    ], function () {
        // Destroy
        Route::delete('/delete', 'DestroyInvitationController')
            ->name('destroy')
            ->middleware('can:invitation.delete,invitation');
        // Resend
        Route::post('/resend', 'ResendInvitationController')
            ->name('resend')
            ->middleware('can:invitation.resend,invitation');
        // Revoke
        Route::post('/revoke', 'RevokeInvitationController')
            ->name('revoke')
            ->middleware('can:invitation.revoke,invitation');
        // Resume
        Route::post('/resume', 'ResumeInvitationController')
            ->name('resume')
            ->middleware('can:invitation.resume,invitation');
    });
});

// Validate invite code
Route::post('/validate-code', 'ValidateInviteCodeController')->name('validateCode');
