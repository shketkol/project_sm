<?php

/**
 * Authorized users.
 */
Route::middleware(['auth:admin'])->group(function () {
    /**
     * Invitation index page
     */
    Route::get('/', 'IndexController')->name('index');
});
