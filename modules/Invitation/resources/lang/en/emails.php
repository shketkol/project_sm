<?php

return [
    'invitation' => [
        'subject' => ':publisher_company_full_name Beta access code',
        'title'   => ':publisher_company_full_name Beta Access Code',
        'body'    => [
            'hello'            => 'Hello,',
            'we_are_excited'   => 'We’re excited to have you try :publisher_company_full_name!',
            'to_sign_up'       => 'To sign up, click this :link',
            'beta_access_link' => 'Beta Access Link',
            'please_note'      => 'Please note that this link will work only with your email address and will expire after you sign up and create an account.',
            'already_have'     => 'If you have already created an account, please use your password to log in to :link.',
        ],
    ],
];
