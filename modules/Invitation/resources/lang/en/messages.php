<?php

return [
    'invite_was_successfully_created' => 'Invite was successfully created',
    'invite_was_successfully_revoke'  => 'Invite has been successfully revoked',
    'invite_was_successfully_resumed' => 'Invite has been successfully resumed',
    'invite_was_successfully_resend'  => 'Invite has been successfully resent',
    'invite_code_valid'               => 'Invite code is valid',
    'you_have_removed_the_invite'     => 'You\'ve removed the selected invite.',
    'invitation_was_not_deleted'      => 'Invite was not deleted.',
    'invite_code_required'            => 'Hulu Ad Manager is currently in a closed beta.',
    'have_error_while_bulk'           => 'There are issues with some of the Invite emails. To learn more, please review the list below.',
    'have_error_while_bulk_unique'    => ':amount Duplicate emails.',
    'have_error_while_bulk_format'    => ':amount Invalid formatting.',
    'have_error_while_bulk_encoding'  => 'Make sure the CSV file is saved using UTF-8 compatible encoding.',
    'success_emails_while_bulk'       => 'The Invites to be sent to :amount emails listed below',
];
