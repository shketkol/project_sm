<?php

use Modules\Invitation\Models\InvitationStatus;

return [
    'invitations'                    => 'Invites',
    'code'                           => 'Invite code',
    'name'                           => 'Name',
    'email'                          => 'Email',
    'emails'                         => 'Emails',
    'create_invite'                  => 'Create Invite',
    'send_invite'                    => 'Send invite',
    'resend_invite'                  => 'Resend Invite',
    'revoke_invite'                  => 'Revoke Invite',
    'resume_invite'                  => 'Resume Invite',
    'search_invitations_placeholder' => 'Search Invites',
    'update_code'                    => 'Update code',
    'invite_created'                 => 'Invite Created',
    'inviter'                        => 'Inviter',
    'actions'                        => 'Actions',
    'invite_resent_at'               => 'Invite Re-Sent',
    'are_you_sure_want_to_delete'    => 'Do you want to delete this invite?',
    'emails_placeholder'             => 'Enter multiple emails separated by commas',
    'delete_invite'                  => 'Delete invite',
    'special_ads_category'           => 'Special Ads Category',
    'statuses'                       => [
        InvitationStatus::INVITED    => 'Invited',
        InvitationStatus::PROCESSING => 'Processing',
        InvitationStatus::REVOKED    => 'Revoked',
        InvitationStatus::COMPLETED  => 'Completed',
        InvitationStatus::OPENED     => 'Opened',
        InvitationStatus::IGNORED    => 'Ignored'
    ],
    'invite_type'                    => 'Invite type',
    'confirm_bulk_invites'           => 'Confirm Invites',
    'tabs'                           => [
        'individual_invite' => 'Individual Invite',
        'bulk_invite'       => 'Bulk Invites'
    ]
];
