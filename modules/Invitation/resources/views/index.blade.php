@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <the-admin-invitation-index></the-admin-invitation-index>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/invitation.js') }}"></script>
@endpush
