@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('invitation::emails.invitation.body.hello') }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('invitation::emails.invitation.body.we_are_excited', [
        'publisher_company_full_name' => $publisherCompanyFullName
    ]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('invitation::emails.invitation.body.to_sign_up', [
            'link' => view('common.email.part.link', [
                'link' => $inviteLink,
                'text' => __('invitation::emails.invitation.body.beta_access_link'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('invitation::emails.invitation.body.please_note') }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('invitation::emails.invitation.body.already_have', [
            'link' => view('common.email.part.link', [
                'link' => route('login.form'),
                'text' => $publisherCompanyFullName,
            ])->render(function ($content) {
                return trim($content);
            }),
        ]) !!}
    </p>
@stop
