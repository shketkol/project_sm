<?php

use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Models\InvitationStatus;

return [
    /**
     * Invitation workflow graph
     */
    'invitation' => [
        // class of your domain object
        'class'         => Invitation::class,

        // name of the graph (default is "default")
        'graph'         => 'invitation',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status_id',

        // list of all possible states
        'states'        => [
            [
                'name' => InvitationStatus::ID_INVITED,
            ],
            [
                'name' => InvitationStatus::ID_PROCESSING,
            ],
            [
                'name' => InvitationStatus::ID_REVOKED,
            ],
            [
                'name' => InvitationStatus::ID_COMPLETED,
            ],
            [
                'name' => InvitationStatus::ID_OPENED,
            ],
            [
                'name' => InvitationStatus::ID_IGNORED,
            ],
        ],

        // list of all possible transitions
        'transitions'   => [
            InvitationStatus::INVITED    => [
                'from' => [InvitationStatus::ID_PROCESSING, InvitationStatus::ID_REVOKED, InvitationStatus::ID_OPENED],
                'to'   => InvitationStatus::ID_INVITED,
            ],
            InvitationStatus::PROCESSING => [
                'from' => [InvitationStatus::ID_OPENED, InvitationStatus::ID_INVITED],
                'to'   => InvitationStatus::ID_PROCESSING,
            ],
            InvitationStatus::COMPLETED  => [
                'from' => [InvitationStatus::ID_PROCESSING, InvitationStatus::ID_INVITED],
                'to'   => InvitationStatus::ID_COMPLETED,
            ],
            InvitationStatus::REVOKED    => [
                'from' => [InvitationStatus::ID_INVITED, InvitationStatus::ID_OPENED],
                'to'   => InvitationStatus::ID_REVOKED,
            ],
            InvitationStatus::OPENED  => [
                'from' => [InvitationStatus::ID_INVITED, InvitationStatus::ID_IGNORED],
                'to'   => InvitationStatus::ID_OPENED,
            ],
            InvitationStatus::IGNORED  => [
                'from' => [InvitationStatus::ID_INVITED],
                'to'   => InvitationStatus::ID_IGNORED,
            ],
        ],

        // list of all callbacks
        'callbacks'     => [
        ],
    ],
];
