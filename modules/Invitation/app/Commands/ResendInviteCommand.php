<?php

namespace Modules\Invitation\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Invitation\Actions\Traits\SendInvite;
use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Repositories\InvitationRepository;
use Psr\Log\LoggerInterface;

class ResendInviteCommand extends Command
{
    use SendInvite;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'platform:invite:resend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resend invite.';

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var InvitationRepository
     */
    private $repository;

    /**
     * @param InvitationRepository $repository
     * @param LoggerInterface $log
     */
    public function __construct(InvitationRepository $repository, LoggerInterface $log)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->log = $log;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        $invitations = $this->repository
            ->byInviteStatus()
            ->byDateResent()
            ->all();

        $this->log->info('Invitations to resend', $invitations->pluck('id')->toArray());

        foreach ($invitations as $invitation) {
            if ($invitation->count_attempts >= config('invitation.invite_resent_attempts')) {
                $invitation->applyInternalStatus(InvitationStatus::IGNORED);
                continue;
            }

            ++$invitation->count_attempts;
            $invitation->invite_resent_at = Carbon::now();
            $invitation->save();

            // Send invite to user
            $this->sendInvite($invitation);
        }

        $this->log->info('Invitations resend finished');
    }
}
