<?php

namespace Modules\Invitation\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 */
class InvitationStatus extends Model
{
    /**
     * Available statuses.
     */
    public const ID_INVITED    = 1;
    public const ID_PROCESSING = 2;
    public const ID_REVOKED    = 3;
    public const ID_COMPLETED  = 4;
    public const ID_OPENED     = 5;
    public const ID_IGNORED    = 6;

    /**
     * A not used code.
     */
    public const INVITED = 'invited';

    /**
     * Processing code by advertiser.
     */
    public const PROCESSING = 'processing';

    /**
     * Revoked code.
     */
    public const REVOKED = 'revoked';

    /**
     * Completed code.
     */
    public const COMPLETED = 'completed';

    /**
     * Open sign up with invite code.
     */
    public const OPENED = 'opened';

    /**
     * Ignore invite.
     */
    public const IGNORED = 'ignored';

    /**
     * Allowed status
     */
    public const ALLOWED = [
        self::ID_INVITED,
        self::ID_IGNORED,
        self::ID_OPENED,
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
