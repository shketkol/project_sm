<?php

namespace Modules\Invitation\Models\Traits;

use Modules\Invitation\Models\Invitation;

trait CreateInvite
{
    /**
     * Create test invite code using factory.
     *
     * @param array $attributes
     *
     * @return Invitation
     */
    private function createTestInviteCode(array $attributes = []): Invitation
    {
        return Invitation::factory()->create($attributes);
    }
}
