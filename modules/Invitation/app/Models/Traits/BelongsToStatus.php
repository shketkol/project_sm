<?php

namespace Modules\Invitation\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Invitation\Models\InvitationStatus;

/**
 * @property InvitationStatus $status
 */
trait BelongsToStatus
{
    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(InvitationStatus::class, 'status_id');
    }
}
