<?php

namespace Modules\Invitation\Models;

use App\Traits\State;
use Carbon\Carbon;
use App\Traits\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Invitation\Models\Traits\BelongsToInviter;
use Modules\Invitation\Models\Traits\BelongsToStatus;
use Modules\User\Models\Traits\BelongsToUser;

/**
 * @property int     $id
 * @property string  $email
 * @property string  $code
 * @property string  $identification_key SalesForce ID
 * @property integer $status_id
 * @property int     $user_id
 * @property int     $invited_by
 * @property bool    $account_type_category
 * @property int     $count_attempts
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @property Carbon  $expired_at
 * @property Carbon  $invite_resent_at
 */
class Invitation extends Model
{
    use BelongsToUser,
        BelongsToInviter,
        BelongsToStatus,
        State,
        HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        'email',
        'code',
        'account_type_category',
        'status_id',
        'user_id',
        'invited_by',
        'expired_at',
        'identification_key',
        'count_attempts',
        'invite_resent_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'expired_at' => 'datetime',
    ];

    /**
     * Invitation state's flow.
     *
     * @var string
     */
    protected $flow = 'invitation';
}
