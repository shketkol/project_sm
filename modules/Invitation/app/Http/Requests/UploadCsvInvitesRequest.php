<?php

namespace Modules\Invitation\Http\Requests;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Invitation\Http\Requests\Traits\UploadCsv;

class UploadCsvInvitesRequest extends FormRequest
{
    use UploadCsv;

    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'csv' => $validationRules->only(
                'invitation.csv',
                ['required', 'max_filesize']
            ),
        ];
    }

    /**
     * Data request
     */
    public function toArray(): array
    {
        return [
            'content' => $this->data,
        ];
    }
}
