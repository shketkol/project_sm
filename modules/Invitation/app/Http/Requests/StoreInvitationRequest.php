<?php

namespace Modules\Invitation\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Modules\User\Models\AccountType;

class StoreInvitationRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'email' => $validationRules->only(
                'invitation.email',
                ['required', 'string', 'max', 'email', 'unique:invitations,email']
            ),
            'code' => $validationRules->only(
                'invitation.code',
                ['required', 'string', 'max', 'unique:invitations,code']
            ),
            'account_type_category' => $validationRules->only(
                'invitation.account_type_category',
                ['required', 'boolean']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'email'                 => $this->email,
            'code'                  => $this->code,
            'account_type_category' => $this->account_type_category ?
                AccountType::ID_SPECIAL_ADS :
                AccountType::ID_COMMON,
        ];
    }
}
