<?php

namespace Modules\Invitation\Http\Requests\Traits;

trait UploadCsv
{
    /**
     * Data
     *
     * @var array
     */
    private $data;

    /**
     * Data for validate
     *
     * @return array
     */
    public function validationData(): array
    {
        $file = $this->file('csv');
        $path = $file->getRealPath();
        $this->data = trim(str_replace('"', '', file_get_contents($path)));

        $data['csv'] = $file->getSize();
        $data['content'] = $this->data;

        return $data;
    }

    /**
     * Data request
     */
    public function toArray(): array
    {
        return [
            'content' => $this->data,
        ];
    }
}
