<?php

namespace Modules\Invitation\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class StoreBulkInvitationRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'emails'                => $validationRules->only(
                'invitation.emails_array',
                ['required', 'array']
            ),
            'emails.*'              => $validationRules->only(
                'invitation.email',
                ['required', 'string', 'max', 'email', 'unique:invitations,email']
            ),
            'account_type_category' => $validationRules->only(
                'invitation.account_type_category',
                ['required', 'boolean']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'emails'                => $this->emails,
            'account_type_category' => $this->account_type_category,
        ];
    }
}
