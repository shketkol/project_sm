<?php

namespace Modules\Invitation\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class ValidateBulkInvitationRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'emails' => $validationRules->only(
                'emails',
                ['required', 'string']
            )
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'emails' => $this->emails,
        ];
    }
}
