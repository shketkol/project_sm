<?php

namespace Modules\Invitation\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class ValidateInviteCodeRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'code' => $validationRules->only(
                'invitation.signup_invite_code',
                ['required', 'string', 'max', 'valid_invite_code']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'code' => $this->code,
        ];
    }
}
