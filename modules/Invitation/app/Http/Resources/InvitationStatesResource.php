<?php

namespace Modules\Invitation\Http\Resources;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Invitation\Models\Invitation;

/**
 * @mixin Invitation
 */
class InvitationStatesResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var \Modules\User\Models\User $user */
        $user = app(Guard::class)->user();

        return [
            'can_revoke' => $user->can('invitation.revoke', $this->resource),
            'can_resume' => $user->can('invitation.resume', $this->resource),
            'can_delete' => $user->can('invitation.delete', $this->resource),
            'can_resend' => $user->can('invitation.resend', $this->resource),
        ];
    }
}
