<?php

namespace Modules\Invitation\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Invitation\Models\Invitation;

/**
 * @mixin Invitation
 * @property Invitation $resource
 */
class InvitationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'                => $this->resource->id,
            'company_name'      => $this->resource->user->company_name ?? '-',
            'user_id'           => $this->resource->user_id ?? '-',
            'inviter_name'      => $this->resource->inviter->account_name ?? '-',
            'email'             => $this->resource->email,
            'code'              => $this->resource->code,
            'invitation_status' => $this->resource->status->name,
            'created_at'        => $this->resource->created_at,
            'count_attempts'    => $this->resource->count_attempts,
            'invite_resent_at'  => $this->resource->invite_resent_at,
            'states'            => new InvitationStatesResource($this->resource),
        ];
    }
}
