<?php

namespace Modules\Invitation\Http\Controllers;

/**
 * Class IndexController
 * @package Modules\Faq\Http\Controllers
 */
class IndexController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        return view('invitation::index');
    }
}
