<?php

namespace Modules\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Invitation\Actions\ResendInvitationAction;
use Modules\Invitation\Http\Resources\InvitationResource;
use Modules\Invitation\Models\Invitation;

class ResendInvitationController extends Controller
{
    /**
     * @param ResendInvitationAction $action
     * @param Invitation $invitation
     * @return InvitationResource
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function __invoke(Invitation $invitation, ResendInvitationAction $action): InvitationResource
    {
        $invitation = $action->handle($invitation);

        return (new InvitationResource($invitation))
            ->additional(['data' => [
                'message' => __('invitation::messages.invite_was_successfully_resend'),
            ]]);
    }
}
