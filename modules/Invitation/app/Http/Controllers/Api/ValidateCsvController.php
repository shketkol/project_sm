<?php

namespace Modules\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Modules\Invitation\Actions\ValidateBulkInvitationAction;
use Modules\Invitation\Http\Requests\UploadCsvInvitesRequest;

class ValidateCsvController extends Controller
{
    /**
     * @param UploadCsvInvitesRequest      $request
     * @param ValidateBulkInvitationAction $action
     *
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function __invoke(UploadCsvInvitesRequest $request, ValidateBulkInvitationAction $action): JsonResponse
    {
        $data = $action->handle(Arr::get($request->toArray(), 'content'));
        return response()->json(
            ['data' => $data]
        );
    }
}
