<?php

namespace Modules\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Invitation\Actions\DestroyInvitationAction;
use Modules\Invitation\Models\Invitation;

class DestroyInvitationController extends Controller
{
    /**
     * Delete the specified resource.
     *
     * @param Invitation $invitation
     * @param DestroyInvitationAction $action
     * @return \Illuminate\Contracts\Routing\ResponseFactory|JsonResponse
     * @throws \Modules\Report\Exceptions\ReportNotDeletedException
     * @throws \Modules\Invitation\Exceptions\InvitationNotDeletedException
     */
    public function __invoke(Invitation $invitation, DestroyInvitationAction $action): JsonResponse
    {
        $action->handle($invitation);

        return response()->json(
            ['data' => ['message' => __('invitation::messages.you_have_removed_the_invite')]]
        );
    }
}
