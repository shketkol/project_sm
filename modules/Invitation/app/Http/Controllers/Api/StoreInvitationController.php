<?php

namespace Modules\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;
use Modules\Invitation\Actions\StoreInvitationAction;
use Modules\Invitation\Http\Requests\StoreInvitationRequest;

class StoreInvitationController extends Controller
{
    /**
     * @param StoreInvitationAction  $action
     * @param StoreInvitationRequest $request
     * @param Authenticatable        $admin
     *
     * @return ResponseFactory|JsonResponse
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(
        StoreInvitationAction $action,
        StoreInvitationRequest $request,
        Authenticatable $admin
    ): JsonResponse {
        $action->handle($request->toArray(), $admin);
        return response()->json(
            ['data' => ['message' => __('invitation::messages.invite_was_successfully_created')]]
        );
    }
}
