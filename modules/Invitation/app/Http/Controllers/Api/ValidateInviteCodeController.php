<?php

namespace Modules\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Invitation\Actions\OpenedInviteCodeAction;
use Modules\Invitation\Http\Requests\ValidateInviteCodeRequest;

class ValidateInviteCodeController extends Controller
{
    /**
     * @param ValidateInviteCodeRequest $request
     * @param OpenedInviteCodeAction $action
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function __invoke(
        ValidateInviteCodeRequest $request,
        OpenedInviteCodeAction $action
    ): Response {
        return response(['email' => $action->handle($request->post('code'))]);
    }
}
