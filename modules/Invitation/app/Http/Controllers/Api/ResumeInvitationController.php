<?php

namespace Modules\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Invitation\Actions\ResumeInvitationAction;
use Modules\Invitation\Actions\RevokeInvitationAction;
use Modules\Invitation\Http\Resources\InvitationResource;
use Modules\Invitation\Models\Invitation;

class ResumeInvitationController extends Controller
{
    /**
     * @param RevokeInvitationAction $action
     * @param Invitation $invitation
     * @return InvitationResource
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function __invoke(ResumeInvitationAction $action, Invitation $invitation): InvitationResource
    {
        $invitation = $action->handle($invitation);

        return (new InvitationResource($invitation))
            ->additional(['data' => [
                'message' => __('invitation::messages.invite_was_successfully_resumed'),
            ]]);
    }
}
