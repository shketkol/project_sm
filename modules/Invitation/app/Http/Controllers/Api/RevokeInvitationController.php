<?php

namespace Modules\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Invitation\Actions\RevokeInvitationAction;
use Modules\Invitation\Http\Resources\InvitationResource;
use Modules\Invitation\Models\Invitation;

class RevokeInvitationController extends Controller
{
    /**
     * @param RevokeInvitationAction $action
     * @param Invitation $invitation
     * @return InvitationResource
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function __invoke(RevokeInvitationAction $action, Invitation $invitation): InvitationResource
    {
        $invitation = $action->handle($invitation);

        return (new InvitationResource($invitation))
            ->additional(['data' => [
                'message' => __('invitation::messages.invite_was_successfully_revoke'),
            ]]);
    }
}
