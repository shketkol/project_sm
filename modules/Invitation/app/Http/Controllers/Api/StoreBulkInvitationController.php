<?php

namespace Modules\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;
use Modules\Invitation\Actions\StoreBulkInvitationAction;
use Modules\Invitation\Http\Requests\StoreBulkInvitationRequest;

class StoreBulkInvitationController extends Controller
{
    /**
     * @param StoreBulkInvitationAction $action
     * @param StoreBulkInvitationRequest $request
     * @param Authenticatable $admin
     *
     * @return ResponseFactory|JsonResponse
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(
        StoreBulkInvitationAction $action,
        StoreBulkInvitationRequest $request,
        Authenticatable $admin
    ): JsonResponse {
        $action->handle($request->get('emails'), $request->get('account_type_category'), $admin);
        return response()->json(
            ['data' => ['message' => __('invitation::messages.invite_was_successfully_created')]]
        );
    }
}
