<?php

namespace Modules\Invitation\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Arr;
use Modules\Invitation\Actions\ValidateBulkInvitationAction;
use Modules\Invitation\Http\Requests\ValidateBulkInvitationRequest;

class ValidateBulkInvitationController extends Controller
{
    /**
     * @param ValidateBulkInvitationAction  $action
     * @param ValidateBulkInvitationRequest $request
     *
     * @return ResponseFactory|JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function __invoke(
        ValidateBulkInvitationAction $action,
        ValidateBulkInvitationRequest $request
    ): JsonResponse {
        $data = $action->handle(Arr::get($request->toArray(), 'emails'));
        return response()->json(
            ['data' => $data]
        );
    }
}
