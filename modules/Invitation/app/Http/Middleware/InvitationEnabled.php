<?php

namespace Modules\Invitation\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Daapi\Http\Controllers\Traits\CallbackResponse;

class InvitationEnabled
{
    use CallbackResponse;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return Response
     */
    public function handle(Request $request, Closure $next)
    {
        if (config('invitation.enabled')) {
            return $next($request);
        }

        return $this->forbiddenResponse($request);
    }
}
