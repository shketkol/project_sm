<?php

namespace Modules\Invitation\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Log\Logger;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Notifications\Notification;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Mail\Invite as Mail;
use Illuminate\Contracts\Queue\ShouldQueue;

class Invite extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * @var Invitation
     */
    protected $invite;

    /**
     * @param Invitation $invite
     */
    public function __construct(Invitation $invite)
    {
        $this->invite = $invite;
    }

    /**
     * Get the notification's channels.
     *
     * @param AnonymousNotifiable $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(AnonymousNotifiable $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param AnonymousNotifiable $notifiable
     *
     * @return Mailable
     */
    public function toMail(AnonymousNotifiable $notifiable): Mailable
    {
        app(Logger::class)->info('Sending invite (on-demand) notification.', [
            'notification_class' => get_class($this),
            'notification_id'    => $this->id,
        ]);

        /** @var \App\Mail\Mail $mail */
        $mail = new $this->mailClass($this->getPayload());
        $mail->to($notifiable->routes['mail']);

        return $mail;
    }

    /**
     * Get notification payload.
     *
     * @return array
     */
    protected function getPayload(): array
    {
        return [
            'title'      => __('invitation::emails.invitation.title', [
                'publisher_company_full_name' => config('general.company_full_name'),
            ]),
            'inviteLink' => route('register.form', [
                'code' => $this->invite->code
            ]),
            'publisherCompanyFullName' => config('general.company_full_name'),
        ];
    }
}
