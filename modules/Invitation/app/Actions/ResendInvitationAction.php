<?php

namespace Modules\Invitation\Actions;

use Carbon\Carbon;
use Illuminate\Log\Logger;
use Modules\Invitation\Actions\Traits\SendInvite;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Models\InvitationStatus;

class ResendInvitationAction
{
    use SendInvite;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * @param Invitation $invite
     * @return Invitation
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function handle(Invitation $invite): Invitation
    {
        $this->log->info('Resending code', ['data' => $invite]);

        ++$invite->count_attempts;
        $invite->invite_resent_at = Carbon::now();
        if (!$invite->inState(InvitationStatus::ID_IGNORED) &&
            !$invite->inState(InvitationStatus::ID_OPENED)
        ) {
            $invite->applyInternalStatus(InvitationStatus::INVITED);
        }
        $invite->save();

        // Send invite to user
        $this->sendInvite($invite);
        $invite->refresh();

        return $invite;
    }
}
