<?php

namespace Modules\Invitation\Actions;

use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Repositories\InvitationRepository;

class OpenedInviteCodeAction
{
    /**
     * @var InvitationRepository
     */
    protected $repository;

    /**
     * @param InvitationRepository $repository
     */
    public function __construct(InvitationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $code
     *
     * @return string
     */
    public function handle(string $code): string
    {
        $invitation = $this->repository->findWhere(['code' => $code])->first();
        $invitation->applyInternalStatus(InvitationStatus::OPENED);

        return $invitation->email;
    }
}
