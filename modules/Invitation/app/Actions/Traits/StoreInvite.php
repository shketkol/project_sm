<?php

namespace Modules\Invitation\Actions\Traits;

use Illuminate\Support\Arr;
use Modules\Invitation\Exceptions\InvitationNotCreatedException;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Models\InvitationStatus;
use Modules\User\Models\User;

trait StoreInvite
{
    /**
     * @param array $data
     * @param User $admin
     *
     * @return Invitation
     * @throws \App\Exceptions\BaseException
     */
    protected function store(array $data, User $admin): Invitation
    {
        $this->log->info('Storing new invitation code.', [
            'data' => $data,
            'user_id' => $admin->id,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $invite = $this->storeInvitation($data, $admin);

            // Send invite to user
            $this->sendInvite($invite);
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();

            throw InvitationNotCreatedException::createFrom($throwable);
        }

        $this->databaseManager->commit();

        return $invite;
    }

    /**
     * @param array $data
     * @param User  $admin
     *
     * @return Invitation
     */
    protected function storeInvitation(array $data, User $admin): Invitation
    {
        Arr::set($data, 'status_id', InvitationStatus::ID_INVITED);
        Arr::set($data, 'invited_by', $admin->id);

        return $this->repository->create($data);
    }
}
