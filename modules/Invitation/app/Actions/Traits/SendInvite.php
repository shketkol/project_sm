<?php

namespace Modules\Invitation\Actions\Traits;

use Illuminate\Support\Facades\Notification;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Notifications\Invite;

trait SendInvite
{
    /**
     * @param Invitation $invite
     * @return void
     */
    protected function sendInvite(Invitation $invite): void
    {
        Notification::route('mail', $invite->email)->notify(new Invite($invite));
    }
}
