<?php

namespace Modules\Invitation\Actions;

use App\Helpers\CsvHelper;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\ValidationException;
use Modules\Invitation\Repositories\InvitationRepository;

class ValidateBulkInvitationAction
{
    use CsvHelper;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ValidationRules
     */
    protected $validationRules;

    /**
     * @var InvitationRepository
     */
    protected $repository;

    /**
     * @param InvitationRepository $repository
     * @param DatabaseManager      $databaseManager
     * @param Logger               $log
     * @param ValidationRules      $validationRules
     */
    public function __construct(
        InvitationRepository $repository,
        DatabaseManager $databaseManager,
        Logger $log,
        ValidationRules $validationRules
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
        $this->validationRules = $validationRules;
    }

    /**
     * @param string $emails
     *
     * @return array
     * @throws ValidationException
     */
    public function handle(string $emails): array
    {
        $emails = $this->parse($emails);
        $emailsContentValidator = $this->createEmailsValidator($emails);

        if ($emailsContentValidator->fails()) {
            throw new ValidationException($emailsContentValidator);
        }

        $result = [
            'success'  => [],
            'unique'   => [],
            'format'   => [],
            'encoding' => [],
        ];

        foreach ($emails as $email) {
            if (in_array($email, $result['success'])) {
                $result['unique'][] = $email;
                continue;
            }

            $validator = $this->validateEmail($email);
            if (!$validator->fails()) {
                $result['success'][] = $email;
                continue;
            }

            $messageBag = $validator->getMessageBag()->get('email');
            foreach ($messageBag as $message) {
                // invalid encoding
                if (!$this->isValidUTF8($email)) {
                    $result['encoding'][] = $this->convertToUTF8($email);
                    break;
                }

                // invalid format
                if ($message === __('validation.custom.email.email')) {
                    $result['format'][] = $email;
                    break;
                }

                if ($message === __('validation.custom.email.unique')) {
                    $result['unique'][] = $email;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $email
     *
     * @return Validator
     */
    protected function validateEmail(string $email): Validator
    {
        return ValidatorFacade::make(
            [
                'email' => $email,
            ],
            [
                'email' => $this->validationRules->only(
                    'invitation.email',
                    ['required', 'string', 'max', 'email', 'unique:invitations,email']
                ),
            ]
        );
    }

    /**
     * @param $emails
     *
     * @return Validator
     */
    protected function createEmailsValidator($emails): Validator
    {
        return ValidatorFacade::make(
            [
                'emails' => $emails,
            ],
            [
                'emails' => $this->validationRules->only(
                    'invitation.emails_array',
                    ['required', 'array']
                ),
            ],
            [
                'emails.required' => __('validation.custom.invites_csv.required'),
                'emails.array'    => __('validation.custom.invites_csv.required'),
            ]
        );
    }
}
