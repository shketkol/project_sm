<?php

namespace Modules\Invitation\Actions;

use Illuminate\Support\Str;

class GenerateInvitationCodeAction
{
    /**
     * @return string
     */
    public function handle(): string
    {
        return Str::random(10);
    }
}
