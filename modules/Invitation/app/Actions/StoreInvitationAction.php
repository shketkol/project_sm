<?php

namespace Modules\Invitation\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Invitation\Actions\Traits\SendInvite;
use Modules\Invitation\Actions\Traits\StoreInvite;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Repositories\InvitationRepository;
use Modules\User\Models\User;

class StoreInvitationAction
{
    use SendInvite,
        StoreInvite;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var InvitationRepository
     */
    protected $repository;

    /**
     * @param InvitationRepository $repository
     * @param DatabaseManager      $databaseManager
     * @param Logger               $log
     */
    public function __construct(
        InvitationRepository $repository,
        DatabaseManager $databaseManager,
        Logger $log
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @param User  $admin
     *
     * @return Invitation
     * @throws \App\Exceptions\BaseException
     */
    public function handle(array $data, User $admin): Invitation
    {
        return $this->store($data, $admin);
    }
}
