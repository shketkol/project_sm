<?php

namespace Modules\Invitation\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Invitation\Actions\Traits\SendInvite;
use Modules\Invitation\Actions\Traits\StoreInvite;
use Modules\Invitation\Repositories\InvitationRepository;
use Modules\User\Models\AccountType;
use Modules\User\Models\User;

class StoreBulkInvitationAction
{
    use SendInvite,
        StoreInvite;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var InvitationRepository
     */
    protected $repository;

    /**
     * @var GenerateInvitationCodeAction
     */
    protected $generateCodeAction;

    /**
     * @param InvitationRepository         $repository
     * @param DatabaseManager              $databaseManager
     * @param Logger                       $log
     * @param GenerateInvitationCodeAction $generateCodeAction
     */
    public function __construct(
        InvitationRepository $repository,
        DatabaseManager $databaseManager,
        Logger $log,
        GenerateInvitationCodeAction $generateCodeAction
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
        $this->generateCodeAction = $generateCodeAction;
    }

    /**
     * @param array $emails
     * @param bool $specialCategory
     * @param User $admin
     *
     * @return void
     * @throws \App\Exceptions\BaseException
     */
    public function handle(array $emails, bool $specialCategory, User $admin): void
    {
        foreach ($emails as $email) {
            $this->store([
                'email'                 => $email,
                'code'                  => $this->generateCodeAction->handle(),
                'account_type_category' => $specialCategory ? AccountType::ID_SPECIAL_ADS : AccountType::ID_COMMON,
            ], $admin);
        }
    }
}
