<?php

namespace Modules\Invitation\Actions;

use Illuminate\Log\Logger;
use Modules\Invitation\Exceptions\InvitationNotDeletedException;
use Modules\Invitation\Models\Invitation;

class DestroyInvitationAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * Delete the invite.
     *
     * @param Invitation $invitation
     * @throws InvitationNotDeletedException
     */
    public function handle(Invitation $invitation): void
    {
        $this->log->info('Deleting invitation.', ['invitation_id' => $invitation->id]);

        if (!$invitation->delete()) {
            $this->log->error('Invitation was not deleted.', ['invitation_id' => $invitation->id]);
            throw new InvitationNotDeletedException(__('invitation::messages.invitation_was_not_deleted'));
        }

        $this->log->info('Invitation was successfully deleted.', ['invitation_id' => $invitation->id]);
    }
}
