<?php

namespace Modules\Invitation\Actions;

use Illuminate\Log\Logger;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Models\InvitationStatus;

class ResumeInvitationAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * @param Invitation $invite
     * @return Invitation
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function handle(Invitation $invite): Invitation
    {
        $this->log->info('Resume invite code', ['data' => $invite]);

        $invite->applyInternalStatus(InvitationStatus::INVITED);
        $invite->refresh();

        return $invite;
    }
}
