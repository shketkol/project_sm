<?php

namespace Modules\Invitation;

use App\Providers\ModuleServiceProvider;
use Modules\Invitation\Commands\ResendInviteCommand;
use Modules\Invitation\Policies\InvitationPolicy;

class InvitationServiceProvider extends ModuleServiceProvider
{
    /**
     * Get module prefix
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'invitation';
    }

    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'invitation' => InvitationPolicy::class,
    ];

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        ResendInviteCommand::class,
    ];

    /**
     * Boot provider.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadRoutes();
        $this->loadConfigs(['state-machine', 'invitation']);
        $this->commands($this->commands);
    }
}
