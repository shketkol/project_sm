<?php

namespace Modules\Invitation\Repositories;

use App\Repositories\Repository;
use Illuminate\Container\Container as Application;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Invitation\Exceptions\InvitationNotFoundException;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Repositories\Criteria\CodeCriteria;
use Modules\Invitation\Repositories\Criteria\EmailCriteria;
use Modules\Invitation\Repositories\Criteria\InviteStatusCriteria;
use Modules\Invitation\Repositories\Criteria\ResentDateCriteria;
use Modules\Invitation\Repositories\Criteria\StatusCriteria;
use Modules\User\Models\User;

class InvitationRepository extends Repository
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * InvitationRepository constructor.
     * @param Application $app
     * @param Logger $log
     * @param DatabaseManager $databaseManager
     */
    public function __construct(Application $app, Logger $log, DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;
        parent::__construct($app, $log);
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Invitation::class;
    }

    /**
     * @param string $code
     * @return string
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws InvitationNotFoundException
     */
    public function getEmailByInviteCode(string $code): string
    {
        $invitation = $this->byCode($code)->first();

        if (is_null($invitation)) {
            throw InvitationNotFoundException::create($code);
        }

        return $invitation->email;
    }

    /**
     * @param string $code
     * @return void
     * @throws InvitationNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function applyProcessingStatus(string $code): void
    {
        $invitation = $this->byCode($code)->first();

        if (is_null($invitation)) {
            throw InvitationNotFoundException::create($code);
        }

        $invitation->applyInternalStatus(InvitationStatus::PROCESSING);
    }

    /**
     * @param string $email
     * @param User $user
     * @throws \Exception
     * @return void
     */
    public function updateStatusByEmail(string $email, User $user): void
    {
        $invitation = $this->byEmail($email)->first();

        if (!$invitation) {
            return;
        }

        $invitation->applyInternalStatus(InvitationStatus::COMPLETED);
        $invitation->user_id = $user->id;
        $invitation->save();
    }

    /**
     * @param string $email
     * @return InvitationRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byEmail(string $email): InvitationRepository
    {
        return $this->pushCriteria(new EmailCriteria($email));
    }

    /**
     * @param string $code
     * @return InvitationRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byCode(string $code): InvitationRepository
    {
        return $this->pushCriteria(new CodeCriteria($code));
    }

    /**
     * @return InvitationRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byAllowedStatusesIds(): InvitationRepository
    {
        return $this->pushCriteria(new StatusCriteria);
    }

    /**
     * @return InvitationRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byDateResent(): InvitationRepository
    {
        return $this->pushCriteria(new ResentDateCriteria);
    }

    /**
     * @return InvitationRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byInviteStatus(): InvitationRepository
    {
        return $this->pushCriteria(new InviteStatusCriteria);
    }
}
