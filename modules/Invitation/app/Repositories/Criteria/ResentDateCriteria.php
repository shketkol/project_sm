<?php

namespace Modules\Invitation\Repositories\Criteria;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ResentDateCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where(
            DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"),
            '>=',
            config('invitation.invite_resent_start_date')
        )
        ->where(
            DB::raw("DATE_FORMAT(IF(invite_resent_at IS NULL, created_at, invite_resent_at), '%Y-%m-%d')"),
            '<=',
            Carbon::now()
                ->sub(config('invitation.invite_resent_interval'), 'day')
                ->format(config('date.format.daapi'))
        );
    }
}
