<?php

namespace Modules\Invitation\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Modules\Invitation\Models\InvitationStatus;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class StatusCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->whereIn('status_id', InvitationStatus::ALLOWED);
    }
}
