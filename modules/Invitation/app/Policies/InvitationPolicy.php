<?php

namespace Modules\Invitation\Policies;

use App\Policies\Policy;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Models\InvitationStatus;
use Modules\User\Models\User;

class InvitationPolicy extends Policy
{
    /**
     * Model class.
     *
     * @var string
     */
    protected $model = Invitation::class;

    public const PERMISSION_LIST_INVITATION   = 'list_invitation';
    public const PERMISSION_CREATE_INVITATION = 'create_invitation';
    public const PERMISSION_DELETE_INVITATION = 'delete_invitation';

    /**
     * Returns true if user can view list of all invitations code.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_INVITATION);
    }

    /**
     * Returns true if user can create invitations code.
     *
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        if (!$this->enabled()) {
            return false;
        }

        return $user->hasPermissionTo(self::PERMISSION_CREATE_INVITATION);
    }

    /**
     * Returns true if user can delete invitations code.
     *
     * @param User       $user
     * @param Invitation $invitation
     *
     * @return bool
     */
    public function delete(User $user, Invitation $invitation): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_DELETE_INVITATION)) {
            return false;
        }

        if ($invitation->status_id === InvitationStatus::ID_PROCESSING) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can resend invitations code.
     *
     * @param User       $user
     * @param Invitation $invitation
     *
     * @return bool
     */
    public function resend(User $user, Invitation $invitation): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_CREATE_INVITATION)) {
            return false;
        }

        return ($invitation->status_id !== InvitationStatus::ID_COMPLETED &&
            $invitation->status_id !== InvitationStatus::ID_REVOKED);
    }

    /**
     * Returns true if user can revoke invitations code.
     *
     * @param User       $user
     * @param Invitation $invitation
     *
     * @return bool
     */
    public function revoke(User $user, Invitation $invitation): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_CREATE_INVITATION)) {
            return false;
        }

        return $invitation->status_id === InvitationStatus::ID_INVITED;
    }

    /**
     * Returns true if user can resume revoked invitations code.
     *
     * @param User       $user
     * @param Invitation $invitation
     *
     * @return bool
     */
    public function resume(User $user, Invitation $invitation): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_CREATE_INVITATION)) {
            return false;
        }

        return $invitation->status_id === InvitationStatus::ID_REVOKED;
    }

    /**
     * @return bool
     */
    public function enabled(): bool
    {
        return config('invitation.enabled');
    }
}
