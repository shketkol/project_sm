<?php

namespace Modules\Invitation\DataTable;

use App\DataTable\DataTable;
use Illuminate\Database\Eloquent\Builder;
use Modules\Invitation\DataTable\Repositories\Criteria\InviterCriteria;
use Modules\Invitation\DataTable\Repositories\Filters\InvitationStatusFilter;
use Modules\Invitation\DataTable\Repositories\Criteria\AdvertiserCriteria;
use Modules\Invitation\DataTable\Repositories\InvitationsDataTableRepository;
use Modules\Invitation\DataTable\Transformers\InvitationsTransformer;
use Modules\Report\DataTable\Repositories\Contracts\ReportsDataTableRepository;
use Modules\Invitation\DataTable\Repositories\Criteria\StatusCriteria;

class InvitationDataTable extends DataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'invitations';

    /**
     * Transformer class
     *
     * @var string
     */
    protected $transformer = InvitationsTransformer::class;

    /**
     * Repository
     *
     * @var string
     */
    protected $repository = InvitationsDataTableRepository::class;

    /**
     * Add aliases according model relation.
     * Required for search
     *
     * @var array
     */
    protected $relationAliases = [
        'company_name' => 'users.company_name',
        'inviter_name' => 'inviters.account_name',
    ];

    /**
     * Filters
     *
     * @var array
     */
    protected $filters = [
        InvitationStatusFilter::class,
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        /** @var ReportsDataTableRepository $repository */
        $repository = $this->getRepository();

        return $this->applySortCriteria($repository)
            ->pushCriteria(new StatusCriteria())
            ->pushCriteria(new AdvertiserCriteria())
            ->pushCriteria(new InviterCriteria())
            ->invitations();
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('invitation.list');
    }
}
