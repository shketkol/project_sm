<?php

namespace Modules\Invitation\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Modules\Invitation\Models\InvitationStatus;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class UsedCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where(['status_id' => InvitationStatus::ID_PROCESSING]);
    }
}
