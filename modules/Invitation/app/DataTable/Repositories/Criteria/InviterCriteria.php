<?php

namespace Modules\Invitation\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class InviterCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->addSelect([
            'inviters.account_name AS inviter_name',
            'inviters.id AS inviter_id',
        ])->leftJoin(
            'users as inviters',
            'invitations.invited_by',
            '=',
            'inviters.id'
        );
    }
}
