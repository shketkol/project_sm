<?php

namespace Modules\Invitation\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class StatusCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->select(
            'invitation_statuses.name AS invitation_status'
        )->leftJoin(
            'invitation_statuses',
            'invitations.status_id',
            '=',
            'invitation_statuses.id'
        );
    }
}
