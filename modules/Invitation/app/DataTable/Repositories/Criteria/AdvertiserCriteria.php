<?php

namespace Modules\Invitation\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AdvertiserCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->addSelect([
            'users.company_name AS company_name',
            'users.id AS company_id',
        ])->leftJoin(
            'users',
            'invitations.user_id',
            '=',
            'users.id'
        );
    }
}
