<?php

namespace Modules\Invitation\DataTable\Repositories;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\DataTable\Repositories\Contracts\InvitationsDataTableRepository
    as InvitationsDataTableRepositoryInterface;

class InvitationsDataTableRepository extends Repository implements InvitationsDataTableRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Invitation::class;
    }

    /**
     * Get invitations list
     *
     * @return Builder
     */
    public function invitations(): Builder
    {
        $this->applyCriteria();

        return $this->model->addSelect('invitations.*');
    }
}
