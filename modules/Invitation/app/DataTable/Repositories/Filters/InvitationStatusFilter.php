<?php

namespace Modules\Invitation\DataTable\Repositories\Filters;

use App\DataTable\Filters\DataTableFilter;
use Illuminate\Database\Eloquent\Builder;
use Modules\Invitation\Models\InvitationStatus;
use Yajra\DataTables\DataTableAbstract;

class InvitationStatusFilter extends DataTableFilter
{
    /**
     * Filter name
     *
     * @var string
     */
    public static $name = 'invitation-status';

    /**
     * @param DataTableAbstract $dataTable
     * @return void
     */
    public function filter(DataTableAbstract $dataTable): void
    {
        $this->makeFilter($dataTable, 'invitation_status', function (Builder $query, $values) {
            $query->orWhereIn('invitation_statuses.id', $values);
        });
    }

    /**
     * Get filter options
     *
     * @return array
     */
    public function options(): array
    {
        return InvitationStatus::all()
            ->pluck('name', 'id')
            ->mapWithKeys(function (string $value, int $key) {
                return [$key => __("invitation::labels.statuses.{$value}")];
            })
            ->toArray();
    }
}
