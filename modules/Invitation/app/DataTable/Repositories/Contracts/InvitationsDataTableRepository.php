<?php

namespace Modules\Invitation\DataTable\Repositories\Contracts;

use App\Repositories\Contracts\Repository;
use Illuminate\Database\Eloquent\Builder;

interface InvitationsDataTableRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string;

    /**
     * Get invitations list
     *
     * @return Builder
     */
    public function invitations(): Builder;
}
