<?php

namespace Modules\Invitation\DataTable\Transformers;

use App\DataTable\Transformers\DataTableTransformer;
use Illuminate\Database\Eloquent\Model;
use Modules\Invitation\Http\Resources\InvitationStatesResource;
use Modules\User\Repositories\UserRepository;

class InvitationsTransformer extends DataTableTransformer
{

    /**
     * @var \Modules\User\Models\User
     */
    private $techAdmin;

    /**
     * @param UserRepository $repository
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(UserRepository $repository)
    {
        $this->techAdmin = $repository->getTechnicalAdmin();
    }

    /**
     * Mapping fields.
     *
     * @var array
     */
    protected $mapMap = [
        'id'    => [
            'name'    => 'id',
            'default' => '-',
        ],
        'company_name' => [
            'name'    => 'company_name',
            'default' => '-',
        ],
        'user_id' => [
            'name'    => 'user_id',
            'default' => '-',
        ],
        'email' => [
            'name'    => 'email',
            'default' => '-',
        ],
        'inviter_name' => [
            'name'    => 'inviter_name',
            'default' => '-',
        ],
        'invitation_status' => [
            'name'    => 'invitation_status',
            'default' => '-',
        ],
        'created_at' => [
            'name'    => 'created_at',
            'default' => '-',
        ],
        'invite_resent_at' => [
            'name'    => 'invite_resent_at',
            'default' => '-',
        ],
        'count_attempts' => [
            'name'    => 'count_attempts',
            'default' => '-',
        ],
    ];

    /**
     * Do transform.
     *
     * @param \Illuminate\Database\Eloquent\Model|\Modules\Invitation\Models\Invitation $model
     * @return array
     */
    public function transform(Model $model): array
    {
        $mapped = parent::transform($model);

        // Add states
        $mapped['states'] = new InvitationStatesResource($model);

        // Inviter name
        $mapped['inviter_name'] = $this->techAdmin->id === $model['inviter_id'] ? 'HAM' : $mapped['inviter_name'];

        return $mapped;
    }
}
