<?php

namespace Modules\Invitation\Mail;

use App\Mail\Mail;

class Invite extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('invitation::emails.invitation.subject', [
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('invitation::emails.invite')
            ->with($this->payload);
    }
}
