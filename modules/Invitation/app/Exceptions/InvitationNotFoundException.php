<?php

namespace Modules\Invitation\Exceptions;

use App\Exceptions\ModelNotFoundException;
use Illuminate\Http\Response;

class InvitationNotFoundException extends ModelNotFoundException
{
    /**
     * @param string $code
     * @return InvitationNotFoundException
     */
    public static function create(string $code): self
    {
        $message = 'Invitation was not found code: ' . $code;
        return new self($message, Response::HTTP_NOT_FOUND);
    }
}
