<?php

namespace Modules\Invitation\Exceptions;

use App\Exceptions\ModelNotDeletedException;

class InvitationNotDeletedException extends ModelNotDeletedException
{
    //
}
