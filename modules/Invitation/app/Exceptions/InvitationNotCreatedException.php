<?php

namespace Modules\Invitation\Exceptions;

use App\Exceptions\ModelNotCreatedException;

class InvitationNotCreatedException extends ModelNotCreatedException
{
    //
}
