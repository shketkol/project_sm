<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignStatus;

class FillSubmittedAtColumnForDraftsInCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('campaigns')
            ->where('status_id', CampaignStatus::ID_DRAFT)
            ->update(['submitted_at' => DB::raw('created_at')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('campaigns')
            ->where('status_id', CampaignStatus::ID_DRAFT)
            ->update(['submitted_at' => null]);
    }
}
