<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInventoryCheckHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('inventory_check_history', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('campaign_id')->unsigned()->index();
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');

            $table->dateTime('date_start');
            $table->dateTime('date_end');

            $table->bigInteger('ordered_impressions');
            $table->bigInteger('available_impressions');
            $table->bigInteger('min_daily_impressions');

            $table->float('campaign_budget');
            $table->unsignedDouble('available_budget');

            $table->bigInteger('status_id')->unsigned()->index();
            $table->foreign('status_id')->references('id')->on('inventory_check_statuses')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('inventory_check_history');
    }
}
