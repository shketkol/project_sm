<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToTimeZone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('timezones', function (Blueprint $table) {
            $table->string('full_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        if (Schema::hasColumn('campaigns', 'full_code')) {
            Schema::table('campaigns', function (Blueprint $table) {
                $table->dropColumn('full_code');
            });
        }
    }
}
