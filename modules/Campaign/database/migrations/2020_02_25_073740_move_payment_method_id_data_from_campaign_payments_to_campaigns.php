<?php

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Payment\Models\OrderStatus;

class MovePaymentMethodIdDataFromCampaignPaymentsToCampaigns extends Migration
{
    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->repository = app(CampaignRepository::class);
        $this->databaseManager = app(DatabaseManager::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function up()
    {
        $campaignPayments = DB::table('campaign_payments')->get();
        $this->databaseManager->beginTransaction();

        try {
            foreach ($campaignPayments as $campaignPayment) {
                $campaign = $this->repository->find($campaignPayment->campaign_id);
                if ($campaign) {
                    $campaign->payment_method_id = $campaignPayment->payment_method_id;
                    $campaign->save();
                }
            }

            $this->databaseManager->commit();
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function down()
    {
        $campaigns = $this->repository->byStatus([
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_READY,
            CampaignStatus::ID_PENDING_APPROVAL,
            CampaignStatus::ID_PAUSED,
        ])->get();

        $data = [];

        DB::table('campaign_payment_statuses')->insert([
            [
                'id'   => 1,
                'name' => OrderStatus::PENDING,
            ],
            [
                'id'   => 2,
                'name' => OrderStatus::CURRENT,
            ],
            [
                'id'   => 3,
                'name' => OrderStatus::FAILED,
            ],
            [
                'id'   => 4,
                'name' => OrderStatus::SETTLED,
            ],
        ]);

        foreach ($campaigns as $campaign) {
            $data[] = [
                'payment_method_id' => $campaign->payment_method_id,
                'campaign_id' => $campaign->id,
                'status_id' => 1,
                'last_activity' => date(config('date.format.db_date_time')),
                'external_id' => $campaign->order_id,
            ];
        }

        if ($data) {
            DB::table('campaign_payments')->insert($data);
        }
    }
}

