<?php

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RemoveExcludedGenresInCampaigns extends Migration
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->databaseManager = app(DatabaseManager::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function up()
    {
        $this->databaseManager->beginTransaction();

        try {
            DB::table('campaign_genres')->where('excluded', 1)->delete();
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
