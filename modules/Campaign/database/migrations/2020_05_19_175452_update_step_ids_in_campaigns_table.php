<?php

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Migrations\Migration;

/**
 * The purpose of this migration is updating states of campaigns after changing order of some wizard steps:
 * Locations(4), Genres(5), Audiences(6)
 * =>
 * Audiences(4), Locations(5), Genres(6)
 */
class UpdateStepIdsInCampaignsTable extends Migration
{
    const OLD_STEP_ID_LOCATIONS = 4;
    const OLD_GENRES_STEP_ID = 5;
    const NEW_STEP_ID_AUDIENCES = 4;
    const NEW_STEP_ID_LOCATIONS = 5;
    const NEW_STEP_ID_GENRES = 6;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->databaseManager = app(DatabaseManager::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function up()
    {
        $this->databaseManager->beginTransaction();

        // Among the draft campaigns that were stopped at one of the next steps (Locations(4), Genres(5), Audiences(6))
        // we need to update only those ones that were stopped at the "Genres" step.
        try {
            DB::table('campaigns')
              ->where('step_id', self::OLD_GENRES_STEP_ID)
              ->update(['step_id' => self::NEW_STEP_ID_AUDIENCES]);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        $this->databaseManager->beginTransaction();

        try {
            DB::table('campaigns')
              ->where('step_id', self::NEW_STEP_ID_LOCATIONS)
              ->update(['step_id' => self::OLD_STEP_ID_LOCATIONS]);

            DB::table('campaigns')
              ->where('step_id', self::NEW_STEP_ID_GENRES)
              ->update(['step_id' => self::OLD_GENRES_STEP_ID]);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }
}
