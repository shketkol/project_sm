<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->float('budget')->default(0.0)->nullable();
            $table->float('cpm')->default(0.0);
            $table->integer('impressions')->default(0)->nullable();
            $table->float('cost');
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->string('name')->nullable();

            $table->bigInteger('company_id')->unsigned()->index();
            $table->foreign('company_id')->references('id')->on('user_companies')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('campaigns');
        Schema::enableForeignKeyConstraints();
    }
}
