<?php

namespace Modules\Campaign\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignStep;

class CampaignStepsTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $steps = [
            [
                'id'   => CampaignStep::ID_NAME,
                'name' => CampaignStep::NAME,
            ],
            [
                'id'   => CampaignStep::ID_SCHEDULE,
                'name' => CampaignStep::SCHEDULE,
            ],
            [
                'id'   => CampaignStep::ID_AGES,
                'name' => CampaignStep::AGES,
            ],
            [
                'id'   => CampaignStep::ID_LOCATIONS,
                'name' => CampaignStep::LOCATIONS,
            ],
            [
                'id'   => CampaignStep::ID_GENRES,
                'name' => CampaignStep::GENRES,
            ],
            [
                'id'   => CampaignStep::ID_DEVICES,
                'name' => CampaignStep::DEVICES,
            ],
            [
                'id'   => CampaignStep::ID_AUDIENCES,
                'name' => CampaignStep::AUDIENCES,
            ],
            [
                'id'   => CampaignStep::ID_BUDGET,
                'name' => CampaignStep::BUDGET,
            ],
            [
                'id'   => CampaignStep::ID_CREATIVE,
                'name' => CampaignStep::CREATIVE,
            ],
            [
                'id'   => CampaignStep::ID_REVIEW,
                'name' => CampaignStep::REVIEW,
            ],
            [
                'id'   => CampaignStep::ID_PAYMENT,
                'name' => CampaignStep::PAYMENT,
            ],
        ];

        foreach ($steps as $step) {
            CampaignStep::updateOrCreate(
                ['id' => Arr::get($step, 'id')],
                $step
            );
        }

        DB::table('campaign_steps')
            ->where('id', '>', CampaignStep::ID_PAYMENT)
            ->delete();
    }
}
