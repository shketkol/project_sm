<?php

Route::middleware(['auth:web,admin'])->group(function () {
    /**
     * Campaign index page
     */
    Route::get('', 'IndexController')->name('index');

    // Create
    Route::get('/create', 'CreateCampaignController')->name('create')->middleware('can:campaign.create');
    Route::get('/create/{group?}/{step?}', 'CreateCampaignController')->middleware('can:campaign.create');

    // Specified campaign
    Route::group(['prefix' => '{campaign}'], function () {
        // Show
        Route::get('/', 'IndexController')->name('show');

        // Edit
        Route::get('/edit', 'EditCampaignController')->name('edit')->middleware('can:campaign.update,campaign');
        Route::get('/edit/{group?}/{step?}', 'EditCampaignController')->middleware('can:campaign.update,campaign');

        // Clone
        Route::get('/clone', 'CloneCampaignController')->name('clone')->middleware('can:campaign.update,campaign');
    });
});
