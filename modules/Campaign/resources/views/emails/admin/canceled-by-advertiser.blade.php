@extends('common.email.layout-admin')

@section('body')

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('emails.hi', ['name' => $firstName]) }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('campaign::emails.admin.canceled_by_advertiser.body.advertiser_has_canceled_their_campaign', [
            'advertiser_company' => view('common.email.part.link', [
                'link' => $companyDetails,
                'text' => $companyName,
            ])->render(function ($content) {
                    return trim($content);
                }),
            'campaign_name' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => $campaignName,
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('campaign::emails.admin.canceled_by_advertiser.body.for_more_information_about_the_campaign', [
            'campaign_details' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => __('campaign::labels.campaign_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>
@stop
