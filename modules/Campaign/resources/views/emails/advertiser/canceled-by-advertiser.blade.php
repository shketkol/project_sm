@extends('common.email.layout')

@section('body')

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('campaign::emails.advertiser.canceled_by_advertiser.body.you_have_successfully_canceled_your_campaign', [
            'campaign_name' => e($campaignName),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('campaign::emails.advertiser.canceled_by_advertiser.body.for_more_information_about_the_campaign', [
            'campaign_details' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => __('campaign::labels.campaign_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('campaign::emails.advertiser.canceled_by_advertiser.body.to_book_a_new_campaign') }}
    </p>

    @include('common.email.part.button', [
        'link' => $bookNewCampaign,
        'text' => __('actions.submit_new_campaign'),
    ])

    @include('common.email.part.if-you-have-any-questions')
@stop
