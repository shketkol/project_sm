@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('campaign::emails.advertiser.completed.body.your_campaign_has_completed', [
            'campaign_name' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => $campaignName,
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('campaign::emails.advertiser.completed.body.for_more_information_about_campaign', [
            'campaign_details' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => __('campaign::labels.campaign_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('campaign::emails.advertiser.completed.body.thank_you_for_booking_your_campaign', [
            'publisher_company_full_name' => $publisherCompanyFullName
        ]) }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('campaign::emails.advertiser.completed.body.to_create_a_new_campaign') }}
    </p>

    @include('common.email.part.button', [
        'link' => $createCampaign,
        'text' => __('actions.create_campaign'),
    ])
@stop
