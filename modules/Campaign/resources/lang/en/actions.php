<?php

return [
    'cancel'                 => 'Cancel',
    'pause'                  => 'Pause',
    'resume'                 => 'Resume',
    'exit'                   => 'Exit',
    'expand_targeting'       => 'Expand your targeting',
    'cancel_inventory_check' => 'Cancel Inventory Check',
    'edit_campaign'          => 'Edit campaign',
    'delete_campaign'        => 'Delete campaign',
    'download_report'        => 'Download Report',
    'close_campaign'         => 'Close campaign',
    'add_to_campaign'        => 'Add to campaign',
    'remove_from_campaign'   => 'Remove from campaign',
    'clone'                  => 'Duplicate Campaign',
];
