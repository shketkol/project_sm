<?php

use Modules\Campaign\Models\CampaignStatus;

return [
    'inventory_check'         => [
        'result'       => [
            'danger_schedule' => 'To help ensure smooth delivery for your campaign, you need a minimum of :impressions impressions per day. Please increase the number of your targeted impressions or shorten the date range of your campaign.',
            'danger_budget'   => 'We may not have enough inventory for your request. To create this campaign, you\'ll need to expand your targeting options and/or date range.',
            'success'         => 'Great! The forecast looks good based on this campaign\'s targeting selections.',
            'warning'         => 'Heads up! You haven\'t selected any targeting. Although we permit "run of site" campaigns, you may want to select some targeting criteria to make sure you reach your target audience.',
            'error'           => 'Click <b>CHECK INVENTORY</b> to verify inventory availability first.',
        ],
        'content'      => [
            'danger_budget'   => [
                'cta_label'          => 'Expand targeting',
                'budget_value'       => 'To fulfill our minimum budget level of <span class="text-label-sm">:value$</span>.',
                'click_the_button'   => 'Click the button below.',
                'include_selections' => 'Include wider selections for your targeting options.',
                'check_inventory'    => 'Check inventory availability again.',
            ],
            'danger_schedule' => [
                'cta_label' => 'Change Schedule',
                'message'   => '<div><p>To help ensure smooth delivery for your campaign, you need a minimum of <span class="text-label-sm">:value</span> impressions per day.</p><p>Please increase the number of your targeted impressions or shorten the date range of your campaign.</p></div>',
            ],
        ],
        'fail'         => [
            'forbidden'       => 'This action is not allowed.',
            'invalid_request' => 'Something went wrong! Please, check your campaign data and try again.',
            'error'           => 'Something went wrong. Please, try again later.',
        ],
        'availability' => [
            'unavailable'    => 'Unavailable',
            'available'      => 'Plenty availability',
            'some_available' => 'Limited availability',
            'no_data'        => '-',
        ],
        'tooltips'     => [
            'unavailable'    => 'With your selected date range and targeting, there is not enough available inventory. Please adjust your campaign settings.',
            'available'      => 'With your selected date range and targeting, there is plenty of available inventory.',
            'some_available' => 'With your selected date range and targeting, there is currently inventory available. Additional targeting may cause unavailability.',
            'no_data'        => '-',
        ],
    ],
    'dynamic_inventory_check' => [
        'error'    => [
            'unavailable' => 'For the selected date range and targeting, there is only inventory available for a $:budget budget',
            // @todo find better message
            'denied'      => 'After previous failure you\'ve made targeting even more narrow or changed nothing. We would not run inventory check in this case.',
        ],
        'schedule' => [
            'narrow' => 'Narrow',
            'expand' => 'Expand',
        ],
        'help'     => [
            'title'   => 'Still having trouble?',
            'message' => 'Learn more about inventory availability',
        ],
    ],
    'billing_date'            => [
        'fail' => 'Something went wrong! Please, check your campaign data and try again.',
    ],
    'validation'              => [
        'dates' => [
            'invalid' => 'Please select new start and end dates for your campaign.',
        ],
    ],
    'cancel'                  => [
        'title'       => 'Cancel campaign',
        'content'     => 'Do you want to cancel this campaign? You will not be able to resume it. If you cancel after your campaign has started, impressions will stop being delivered once your request is processed.',
        'description' => 'Note: You will be billed for all delivered impressions through cancellation.',
        'accepted'    => 'Campaign cancellation request was accepted',
    ],
    'pause'                   => [
        'title'       => 'Pause campaign',
        'content'     => 'Do you want to pause this campaign? You can resume your campaign any time before the scheduled end date. If you pause after your campaign has started, impressions will stop being delivered once your request is processed.',
        'description' => 'Note: Pausing a campaign will not change its scheduled end date.',
    ],
    'resume'                  => [
        'failed' => 'You cannot resume campaign which was paused not by you.',
    ],
    'remove'                  => [
        'title'       => 'Delete campaign',
        'content'     => 'Do you want to delete this campaign? If you delete a campaign, Hulu will delete all information associated with the campaign.',
        'description' => 'Deleting is permanent.',
    ],
    'success'                 => 'You\'ve submitted your campaign.',
    'review_creative'         => 'Please allow 3 business days for us to review your Ad.',
    'success_no_creative_p1'  => 'You\'ve submitted your campaign.',
    'success_no_creative_p2'  => 'To avoid missing the start date, upload your Ad as soon as possible, as it may take up to 3 business days to review it.',
    'delete'                  => [
        'not_deletable' => 'Campaign was not deleted. Only draft campaign can be deleted.',
        'failed'        => 'Campaign was not deleted.',
        'success'       => 'Campaign deleted.',
    ],
    'creative'                => [
        'edit' => [
            'not_editable' => 'Ad in campaign with status "completed" or "cancelled" can not be changed.',
            'failed'       => 'Campaign Ad was not updated.',
        ],
    ],
    'there_are_no_campaigns'  => 'There are no campaigns yet!',
    'create_first_campaign'   => 'Add your first campaign!',
    'status_updated'          => 'You\'ve updated the campaign\'s status.',
    'status_not_updated'      => 'Status cannot be changed.',
    'payment_method_missing'  => 'To create a campaign, please add a payment method.',
    'cpm_failed'              => 'Error while retrieving CPM. Please try again later',
    'order'                   => [
        'failed' => 'Failed to order campaign.',
    ],
    'update_start_date'       => 'Please note that the original intended Start Date of this campaign has passed. We have automatically adjusted it to tomorrow\'s date so that you may continue to Edit.',
    'campaign_wizard_tip_p1'  => 'Here we\'ll show a summary of your campaign settings.',
    'campaign_wizard_tip_p2'  => 'You can go back to any step to make changes before submitting.',
    'status'                  => [
        'same' => 'You cannot :action the Campaign, as it has already been :status by :actor. Please refresh the page.',
    ],
    'action'                  => [
        CampaignStatus::PAUSED   => 'pause',
        CampaignStatus::LIVE     => 'resume',
        CampaignStatus::CANCELED => 'cancel',
    ],
    'missing_ad'              => 'Missing Ad',
    'charts'                  => [
        'performance_not_available'  => 'We’ll start displaying performance data on the second delivery day.',
        'no_available_data'          => 'This data is not available for this campaign.',
        'genres_data_not_started'    => 'We’ll display Show Genre data once the campaign goes live.',
        'genres_data_not_available'  => 'Show Genre data is temporarily unavailable. Please try again later.',
        'devices_data_not_started'   => 'We’ll display Platform data once the campaign goes live.',
        'devices_data_not_available' => 'Platform data is temporarily unavailable. Please try again later.',

    ],
    'fail_clone'              => 'Campaign was not duplicate',
    'special_ads'             => [
        'targetings_removed' => 'We\'ve reset your targeting selections to <b>All</b> and limited the available options based on the <a target="_blank" href=":url">Hulu Ad Manager Guidelines for Special Ads Categories.</a>',
    ],
    'impressions_message'     => 'Estimate with current targeting. This will change with targeting selections.',
    'min_budget_message'      => 'This budget will not meet the required :value impressions per day. Please increase your budget or shorten your date range.',
    'change_budget'           => 'Change Budget',
    'change_schedule'         => 'Change Schedule',
];
