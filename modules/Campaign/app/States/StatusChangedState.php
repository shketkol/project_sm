<?php

namespace Modules\Campaign\States;

use Carbon\Carbon;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Notifications\Advertiser\Started;

class StatusChangedState extends CampaignState
{
    /**
     * @param Campaign $campaign
     */
    public function after(Campaign $campaign): void
    {
        $campaign->status_changed_at = Carbon::now();
        $campaign->save();
    }
}
