<?php

namespace Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Models\Traits\MorphToTargeting;

/**
 * @property int    $id
 * @property int    $history_id
 * @property string $targetable_type
 * @property int    $targetable_id
 * @property bool   $excluded
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class InventoryCheckTargeting extends Model
{
    use MorphToTargeting;

    /**
     * @var array
     */
    protected $fillable = [
        'history_id',
        'targetable_type',
        'targetable_id',
        'excluded',
    ];
}
