<?php

namespace Modules\Campaign\Models;

use App\Models\Traits\BelongsToTimezone;
use App\Traits\HasFactory;
use App\Traits\State;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Models\Traits\BelongsToStatus;
use Modules\Campaign\Models\Traits\BelongsToStep;
use Modules\Campaign\Models\Traits\Budget;
use Modules\Campaign\Models\Traits\CampaignLifetime;
use Modules\Campaign\Models\Traits\Cost;
use Modules\Campaign\Models\Traits\Deletable;
use Modules\Campaign\Models\Traits\Editable;
use Modules\Campaign\Models\Traits\PassedStepsRelations;
use Modules\Creative\Models\Traits\BelongsToManyCreatives;
use Modules\Notification\Models\Traits\MorphAlerts;
use Modules\Promocode\Models\Traits\BelongsToPromocode;
use Modules\Targeting\Models\Traits\BelongsToManyAudiences;
use Modules\Targeting\Models\Traits\BelongsToManyDeviceGroups;
use Modules\Targeting\Models\Traits\BelongsToManyGenres;
use Modules\Targeting\Models\Traits\BelongsToManyLocations;
use Modules\Targeting\Models\Traits\BelongsToManyTargetingAgeGroups;
use Modules\Targeting\Models\Traits\BelongsToManyTargetingGenders;
use Modules\Targeting\Models\Traits\BelongsToManyZipcodes;
use Modules\User\Models\Traits\BelongsToUser;
use Modules\Payment\Models\Traits\BelongsToPaymentMethod;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $status_id
 * @property integer $status_changed_by
 * @property Carbon  $status_changed_at
 * @property integer $timezone_id
 * @property string  $external_id
 * @property float   $budget
 * @property float   $cpm
 * @property float   $discounted_cpm
 * @property int     $impressions
 * @property int     $min_impressions
 * @property float   $discounted_cost
 * @property string  $name
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @property Carbon  $submitted_at
 * @property integer $step_id
 * @property integer $order_id
 * @property integer $promocode_id
 * @property integer $payment_method_id
 *
 * @property Carbon  $date_start
 * @property Carbon  $start_date_with_timezone
 * @property Carbon  $startDateWithTimezone
 * @see Campaign::getStartDateWithTimezoneAttribute()
 *
 * @property Carbon  $date_end
 * @property Carbon  $end_date_with_timezone
 * @property Carbon  $endDateWithTimezone
 * @see Campaign::getEndDateWithTimezoneAttribute()
 */
class Campaign extends Model
{
    use BelongsToManyCreatives,
        BelongsToManyGenres,
        BelongsToManyDeviceGroups,
        BelongsToManyLocations,
        BelongsToManyZipcodes,
        BelongsToManyTargetingAgeGroups,
        BelongsToManyTargetingGenders,
        BelongsToManyAudiences,
        BelongsToPaymentMethod,
        BelongsToStatus,
        BelongsToStep,
        BelongsToTimezone,
        BelongsToUser,
        CampaignLifetime,
        Deletable,
        Editable,
        BelongsToPromocode,
        MorphAlerts,
        State,
        Cost,
        Budget,
        PassedStepsRelations,
        HasFactory;

    /**
     * @see targetingAgeGroups
     */
    public const RELATION_AGE_GROUPS_NAME = 'targetingAgeGroups';

    /**
     * @see targetingGenders
     */
    public const RELATION_GENDERS_NAME = 'targetingGenders';

    /**
     * @see audiences
     */
    public const RELATION_AUDIENCES_NAME = 'audiences';

    /**
     * @see locations
     */
    public const RELATION_LOCATIONS_NAME = 'locations';

    /**
     * @see zipcodes
     */
    public const RELATION_ZIPCODES_NAME = 'zipcodes';

    /**
     * @see genres
     */
    public const RELATION_GENRES_NAME = 'genres';

    /**
     * @var array
     */
    protected $fillable = [
        'budget',
        'cpm',
        'impressions',
        'min_impressions',
        'name',
        'date_start',
        'date_end',
        'user_id',
        'payment_method_id',
        'status_id',
        'timezone_id',
        'external_id',
        'order_id',
        'discounted_cost',
        'submitted_at',
        'status_changed_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date_start',
        'date_end',
        'submitted_at',
        'status_changed_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'budget' => 'float',
    ];

    /**
     * Campaign state's flow.
     *
     * @var string
     */
    protected $flow = 'campaign';

    /**
     * HAAPI external id.
     *
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->external_id;
    }

    /**
     * Get campaign start date with timezone offset
     *
     * @return Carbon|null
     */
    public function getStartDateWithTimezoneAttribute(): ?Carbon
    {
        if (!$this->date_start) {
            return null;
        }

        return Carbon::createFromFormat(
            config('date.format.db_date_time'),
            $this->date_start,
            $this->timezone->full_code
        );
    }

    /**
     * Get campaign end date with timezone offset
     *
     * @return Carbon|null
     */
    public function getEndDateWithTimezoneAttribute(): ?Carbon
    {
        if (!$this->date_end) {
            return null;
        }

        return Carbon::createFromFormat(
            config('date.format.db_date_time'),
            $this->date_end,
            $this->timezone->full_code
        );
    }

    /**
     * Delete promocode in the campaign
     *
     * @return void
     */
    public function deletePromocode(): void
    {
        $this->promocode_id = null;
        $this->discounted_cost = null;
        $this->save();
    }
}
