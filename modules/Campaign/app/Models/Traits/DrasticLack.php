<?php

namespace Modules\Campaign\Models\Traits;

trait DrasticLack
{
    /**
     * @var int
     */
    private $drasticLack = 0;

    /**
     * @return bool
     */
    public function isDrasticLack(): bool
    {
        return $this->available_impressions <= $this->getDrasticLackValue();
    }

    /**
     * @return int
     */
    public function getDrasticLackValue(): int
    {
        return $this->drasticLack;
    }
}
