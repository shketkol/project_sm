<?php

namespace Modules\Campaign\Models\Traits;

use Modules\Campaign\Models\Campaign;
use Psr\Log\LoggerInterface;

/**
 * Backward Compatibility for removed cost attribute
 *
 * @property float $cost
 * @mixin Campaign
 * @deprecated after a while this entire trait must be purged.
 */
trait Cost
{
    /**
     * @return float
     */
    public function getCostAttribute(): float
    {
        /** @var LoggerInterface $log */
        $log = app(LoggerInterface::class);
        $log->info('Application requested to get removed "cost" property! Investigate and change to budget.');
        $log->debug('Backtrace: ', ['backtrace' => debug_backtrace()]);

        return $this->budget;
    }

    /**
     * @param float $cost
     *
     * @return void
     */
    public function setCostAttribute(float $cost): void
    {
        /** @var LoggerInterface $log */
        $log = app(LoggerInterface::class);
        $log->info('Application requested to set removed "cost" property! Investigate and change to budget.');
        $log->debug('Backtrace: ', ['backtrace' => debug_backtrace()]);

        $this->budget = $cost;
    }
}
