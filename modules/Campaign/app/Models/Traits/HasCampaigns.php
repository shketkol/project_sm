<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Campaign\Models\Campaign;

/**
 * @property Campaign[] $campaigns
 */
trait HasCampaigns
{
    /**
     * @return HasMany
     */
    public function campaigns(): HasMany
    {
        return $this->hasMany(Campaign::class, 'status_id');
    }
}
