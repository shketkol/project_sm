<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Campaign\Models\Campaign;

/**
 * Trait BelongsToCampaign
 *
 * @package Modules\Campaign\Models\Traits
 * @property Campaign $campaign
 */
trait BelongsToCampaign
{
    /**
     * @return BelongsTo
     */
    public function campaign(): BelongsTo
    {
        return $this->belongsTo(Campaign::class);
    }
}
