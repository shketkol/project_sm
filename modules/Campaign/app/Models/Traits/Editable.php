<?php

namespace Modules\Campaign\Models\Traits;

use Modules\Campaign\Models\CampaignStatus;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeStatus;
use Modules\User\Models\User;

trait Editable
{
    /**
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->status_id === CampaignStatus::ID_DRAFT;
    }

    /**
     * @return bool
     */
    public function isEditableCreative(): bool
    {
        /** @var Creative $creative */
        $creative = $this->getActiveCreative();

        /** @var User $user */
        $user = $this->user;

        $approvedCreatives = $user->creatives()->where('status_id', CreativeStatus::ID_APPROVED)->count();

        if ($this->inState([CampaignStatus::ID_CANCELED, CampaignStatus::ID_COMPLETED])) {
            return false;
        }

        if ($creative &&
            $creative->inState(CreativeStatus::ID_PROCESSING) &&
            $this->inState(CampaignStatus::ID_PROCESSING)
        ) {
            return false;
        }

        if ($this->inState([CampaignStatus::ID_PENDING_APPROVAL])
            && $creative && !$creative->inState(CreativeStatus::ID_REJECTED)
        ) {
            return false;
        }

        if ($this->inState([CampaignStatus::ID_READY]) &&
            $creative && $this->creativeOnProcessing($creative)
        ) {
            return false;
        }

        if ($this->inState([CampaignStatus::ID_READY]) && $approvedCreatives < 2) {
            return false;
        }

        if ($this->inState([CampaignStatus::ID_LIVE]) &&
            $creative &&
            $creative->inState([CreativeStatus::ID_APPROVED])
        ) {
            return false;
        }

        if ($this->inState(CampaignStatus::ID_PAUSED) &&
            $creative &&
            $creative->inState([
                CreativeStatus::ID_PENDING_APPROVAL,
                CreativeStatus::ID_APPROVED,
                CreativeStatus::ID_PROCESSING
            ])
        ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isOnlyReplaceableCreative(): bool
    {
        /** @var Creative $creative */
        $creative = $this->getActiveCreative();

        if ($this->inState([CampaignStatus::ID_PENDING_APPROVAL]) &&
            $creative && $creative->inState([CreativeStatus::ID_REJECTED])
        ) {
            return true;
        }

        if ($this->inState([CampaignStatus::ID_READY]) &&
            $creative && $creative->inState([CreativeStatus::ID_APPROVED])
        ) {
            return true;
        }

        if ($this->inState([CampaignStatus::ID_PAUSED]) &&
            $creative && $creative->inState([CreativeStatus::ID_REJECTED])
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param Creative $creative
     * @return bool
     */
    protected function creativeOnProcessing(Creative $creative)
    {
        return $creative->inState([CreativeStatus::ID_PENDING_APPROVAL, CreativeStatus::ID_PROCESSING]);
    }

    /**
     * @return bool
     */
    public function inProcessing(): bool
    {
        return $this->inState(CampaignStatus::ID_PROCESSING);
    }

    /**
     * @return bool
     */
    public function inSuspended(): bool
    {
        return $this->inState(CampaignStatus::ID_SUSPENDED);
    }
}
