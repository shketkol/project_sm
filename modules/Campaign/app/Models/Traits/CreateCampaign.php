<?php

namespace Modules\Campaign\Models\Traits;

use Carbon\CarbonImmutable;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Demo\Helpers\ExternalIdHelper;

trait CreateCampaign
{
    /**
     * Create test campaign using factory.
     *
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Campaign|Campaign[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestCampaign(array $attributes = [], int $count = null)
    {
        return Campaign::factory()->count($count)->create($attributes);
    }

    /**
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Campaign|Campaign[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestReadyCampaign(array $attributes = [], int $count = null)
    {
        Arr::set($attributes, 'status_id', CampaignStatus::ID_READY);
        Arr::set($attributes, 'external_id', ExternalIdHelper::createCampaignExternalId());

        $campaign = $this->createTestCampaign($attributes, $count);
        $this->notificationHelper->createCompletedCampaignNotification($campaign);

        return $campaign;
    }

    /**
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Campaign|Campaign[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestLiveCampaign(array $attributes = [], int $count = null)
    {
        Arr::set($attributes, 'status_id', CampaignStatus::ID_LIVE);
        Arr::set($attributes, 'date_start', CarbonImmutable::now()->sub(2, ' day'));
        Arr::set($attributes, 'external_id', ExternalIdHelper::createCampaignExternalId());

        $campaign = $this->createTestCampaign($attributes, $count);
        $this->notificationHelper->createStartedCampaignNotification($campaign);
        $this->notificationHelper->createTransactionIsMadeNotification($campaign);

        return $campaign;
    }

    /**
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Campaign|Campaign[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestCompletedCampaign(array $attributes = [], int $count = null)
    {
        Arr::set($attributes, 'status_id', CampaignStatus::ID_COMPLETED);
        Arr::set($attributes, 'date_start', CarbonImmutable::now()->sub(5, ' day'));
        Arr::set($attributes, 'date_end', CarbonImmutable::now()->sub(1, ' day'));
        Arr::set($attributes, 'external_id', ExternalIdHelper::createCampaignExternalId());

        return $this->createTestCampaign($attributes, $count);
    }

    /**
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Campaign|Campaign[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestPausedCampaign(array $attributes = [], int $count = null)
    {
        Arr::set($attributes, 'status_id', CampaignStatus::ID_PAUSED);
        Arr::set($attributes, 'status_changed_at', CarbonImmutable::now()->sub(3, ' day'));
        Arr::set($attributes, 'date_start', CarbonImmutable::now()->sub(5, ' day'));
        Arr::set($attributes, 'external_id', ExternalIdHelper::createCampaignExternalId());

        return $this->createTestCampaign($attributes, $count);
    }

    /**
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Campaign|Campaign[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestCanceledCampaign(array $attributes = [], int $count = null)
    {
        Arr::set($attributes, 'status_id', CampaignStatus::ID_CANCELED);
        Arr::set($attributes, 'status_changed_at', CarbonImmutable::now()->sub(3, ' day'));
        Arr::set($attributes, 'date_start', CarbonImmutable::now()->sub(5, ' day'));
        Arr::set($attributes, 'external_id', ExternalIdHelper::createCampaignExternalId());

        return $this->createTestCampaign($attributes, $count);
    }
}
