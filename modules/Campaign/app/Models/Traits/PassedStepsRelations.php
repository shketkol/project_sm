<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Support\Arr;
use Modules\Campaign\Models\CampaignStep;

trait PassedStepsRelations
{
    /**
     * @return $this
     */
    public function loadPassedStepsRelations()
    {
        return $this->load($this->getPassedStepsRelations());
    }

    /**
     * Get the relation names of the steps that have been passed.
     *
     * @return array
     */
    protected function getPassedStepsRelations()
    {
        return Arr::flatten(
            array_filter(CampaignStep::STEPS_RELATIONS, function ($key) {
                return $key <= $this->step_id;
            }, ARRAY_FILTER_USE_KEY)
        );
    }
}
