<?php

namespace Modules\Campaign\Models\Traits;

trait Budget
{
    /**
     * @return float|null
     */
    public function getLowestCpm(): ?float
    {
        return $this->discounted_cpm ?: $this->cpm;
    }

    /**
     * @return float|null
     */
    public function getLowestBudget(): ?float
    {
        return $this->discounted_cost ?: $this->budget;
    }
}
