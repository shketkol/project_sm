<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Campaign\Models\Campaign;

/**
 * Trait BelongsToManyCampaigns
 *
 * @package Modules\Campaign\Models\Traits
 * @property Campaign[]|\Illuminate\Database\Eloquent\Collection $campaigns
 */
trait HasManyCampaigns
{
    /**
     * @return HasMany
     */
    public function campaigns(): HasMany
    {
        return $this->hasMany(Campaign::class);
    }
}
