<?php

namespace Modules\Campaign\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Campaign\Models\Campaign;

class CampaignStepUpdated
{
    use SerializesModels;

    /**
     * @var Campaign
     */
    public $campaign;

    /**
     * Create a new event instance.
     *
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }
}
