<?php

namespace Modules\Campaign\Actions;

use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignGetPrice;
use Modules\Haapi\DataTransferObjects\Campaign\CampaignPriceParams;
use Modules\Targeting\Actions\GetCampaignTargetingsAction;
use Modules\Targeting\Exceptions\TypeNotFoundException;

class GetCampaignPriceAction
{
    /** Constants to use in HAAPI Inventory Check */
    private const PRODUCT_ID_KEY = 'daapi.campaign.productId';

    /**
     * @var CampaignGetPrice
     */
    protected $campaignGetPrice;

    /**
     * @var GetCampaignTargetingsAction
     */
    protected $getCampaignTargetings;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @param CampaignGetPrice            $campaignGetPrice
     * @param Logger                      $log
     * @param GetCampaignTargetingsAction $getCampaignTargetings
     */
    public function __construct(
        CampaignGetPrice $campaignGetPrice,
        Logger $log,
        GetCampaignTargetingsAction $getCampaignTargetings
    ) {
        $this->campaignGetPrice = $campaignGetPrice;
        $this->log = $log;
        $this->getCampaignTargetings = $getCampaignTargetings;
    }

    /**
     * Get campaign price.
     *
     * @param Campaign $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     */
    public function handle(Campaign $campaign): array
    {
        $this->log->info('Fetching campaign price.', ['campaign_id' => $campaign->id]);

        $payload = $this->fetchCampaignPrice($campaign);

        $this->log->info('Campaign price fetched.', [
            'campaign_id' => $campaign->id,
            'payload'     => $payload,
        ]);

        return $payload;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     */
    private function fetchCampaignPrice(Campaign $campaign): array
    {
        $userId = Auth::user()->getAuthIdentifier();

        $campaignPriceParams = new CampaignPriceParams([
            'targets'   => $this->getCampaignTargetings->handle($campaign),
            'productId' => config(self::PRODUCT_ID_KEY),
        ]);

        $response = $this->campaignGetPrice->handle($campaignPriceParams, $userId);

        return $response->getPayload();
    }
}
