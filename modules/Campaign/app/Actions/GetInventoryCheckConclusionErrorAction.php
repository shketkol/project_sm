<?php

namespace Modules\Campaign\Actions;

use Carbon\Carbon;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;

class GetInventoryCheckConclusionErrorAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * @param array $message
     * @param Campaign $campaign
     * @param int $minDailyImpressions
     * @return array[]
     */
    public function handle(array $messages): array
    {
        return [
            'data'        => [
                'type' => GetInventoryCheckConclusionAction::UNPROCESSABLE,
            ],
            'suggestions' => $this->findSuggestions($messages),
            'updated'     => Carbon::now(),
        ];
    }

    /**
     * @param array $message
     *
     * @return array
     */
    private function findSuggestions(array $messages): array
    {
        $message = Arr::first($messages);
        $key = Arr::first(array_keys($messages));

        if ($key === 'budget') {
            $action = [
                $key => __('campaign::messages.change_' . $key),
            ];
        } else {
            $action = [
                'steps' => [
                    $key => __('campaign::messages.change_' . $key),
                ]
            ];
        }

        return [
            'message' => $message,
            'action'  => $action
        ];
    }
}
