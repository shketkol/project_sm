<?php

namespace Modules\Campaign\Actions;

use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;

class DeletePromocodeAction
{
    /**
     * Handle action.
     *
     * @param Campaign $campaign
     *
     * @return void
     */
    public function handle(Campaign $campaign): void
    {
        if ($campaign->status_id === CampaignStatus::ID_DRAFT && $campaign->promocode_id) {
            $campaign->deletePromocode();
        }
    }
}
