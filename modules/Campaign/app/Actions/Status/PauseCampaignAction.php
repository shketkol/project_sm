<?php

namespace Modules\Campaign\Actions\Status;

use Illuminate\Contracts\Notifications\Dispatcher;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Actions\Traits\ValidateStatusEvaluation;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Notifications\Advertiser\Paused\CampaignPausedByAdmin;
use Modules\Campaign\Notifications\Advertiser\Paused\CampaignPausedByAdvertiser;
use Modules\Haapi\Actions\Campaign\CampaignPause;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class PauseCampaignAction
{
    use ValidateStatusEvaluation;

    /**
     * @var UpdateCampaignStatusAction
     */
    private $updateStatusAction;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var Dispatcher
     */
    private $notification;

    /**
     * @var CampaignPause
     */
    private $campaignPauseAction;

    /**
     * PauseCampaignAction constructor.
     *
     * @param CampaignPause              $campaignPauseAction
     * @param Dispatcher                 $notification
     * @param LoggerInterface            $log
     * @param UpdateCampaignStatusAction $updateStatusAction
     */
    public function __construct(
        CampaignPause $campaignPauseAction,
        Dispatcher $notification,
        LoggerInterface $log,
        UpdateCampaignStatusAction $updateStatusAction
    ) {
        $this->campaignPauseAction = $campaignPauseAction;
        $this->log = $log;
        $this->notification = $notification;
        $this->updateStatusAction = $updateStatusAction;
    }

    /**
     * Pause campaign.
     *
     * @param Campaign $campaign
     * @param User     $user
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(Campaign $campaign, User $user): Campaign
    {
        $this->log->info('Campaign pause started.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        $this->canApply($campaign, $user, CampaignStatus::PAUSED);

        $this->campaignPauseAction->handle($campaign->external_id, Auth::id());
        $campaign = $this->updateStatusAction->handle($campaign, CampaignStatus::PAUSED, $user);

        $this->log->info('Campaign was paused.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        $this->sendNotification($campaign, $user);

        return $campaign;
    }

    /**
     * Send email and on-site notifications.
     *
     * @param Campaign $campaign
     * @param User     $user
     */
    private function sendNotification(Campaign $campaign, User $user): void
    {
        // Advertiser has paused the campaign
        if ($user->isAdvertiser()) {
            $this->notification->send($campaign->user, new CampaignPausedByAdvertiser($campaign, $user));

            return;
        }

        $this->notification->send($campaign->user, new CampaignPausedByAdmin($campaign, $user));
    }
}
