<?php

namespace Modules\Campaign\Actions\Traits;

use Illuminate\Support\Arr;

trait GetCampaignCost
{
    /**
     * Get total cost for the campaign.
     *
     * Must be in the GetCampaignImpressionsDetailAction response format.
     * @param array $impressionsDetails
     *
     * @return float
     */
    protected function getCost(array $impressionsDetails): float
    {
        return Arr::get($impressionsDetails, 'cost');
    }
}
