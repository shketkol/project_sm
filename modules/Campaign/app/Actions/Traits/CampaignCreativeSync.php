<?php

namespace Modules\Campaign\Actions\Traits;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Actions\Alerts\SetCampaignAlertsAction;
use Modules\Campaign\Actions\UpdateCampaignStepAction;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Creative\Models\CreativeStatus;
use Modules\Haapi\Actions\Creative\Contracts\CreativeUpdate;
use Modules\Haapi\Actions\Creative\Contracts\CreativeUpload;
use Modules\Haapi\DataTransferObjects\Creative\CreativeUpdateParam;
use Modules\Haapi\DataTransferObjects\Creative\CreativeUploadParam;
use Modules\Notification\Actions\DestroyAlertsAction;

trait CampaignCreativeSync
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var \Modules\Haapi\Actions\Creative\CreativeUpdate
     */
    protected $creativeUpdate;

    /**
     * @var \Illuminate\Database\DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var \Modules\Haapi\Actions\Creative\Contracts\CreativeUpload
     */
    protected $creativeUpload;

    /**
     * @var DestroyAlertsAction
     */
    protected $destroyAlertsAction;

    /**
     * @var SetCampaignAlertsAction
     */
    protected $setCampaignAlertsAction;

    /**
     * @var UpdateCampaignStepAction
     */
    protected $stepAction;

    /**
     * UpdateCampaignCreativeAction constructor.
     *
     * @param CreativeUpdate           $creativeUpdate
     * @param CreativeUpload           $creativeUpload
     * @param DatabaseManager          $databaseManager
     * @param DestroyAlertsAction      $destroyAlertsAction
     * @param Logger                   $log
     * @param SetCampaignAlertsAction  $setCampaignAlertsAction
     * @param UpdateCampaignStepAction $stepAction
     */
    public function __construct(
        CreativeUpdate $creativeUpdate,
        CreativeUpload $creativeUpload,
        DatabaseManager $databaseManager,
        DestroyAlertsAction $destroyAlertsAction,
        Logger $log,
        SetCampaignAlertsAction $setCampaignAlertsAction,
        UpdateCampaignStepAction $stepAction
    ) {
        $this->creativeUpdate = $creativeUpdate;
        $this->creativeUpload = $creativeUpload;
        $this->databaseManager = $databaseManager;
        $this->destroyAlertsAction = $destroyAlertsAction;
        $this->log = $log;
        $this->setCampaignAlertsAction = $setCampaignAlertsAction;
        $this->stepAction = $stepAction;
    }

    /**
     * Sync campaign creative with HAAPI
     *
     * @param \Modules\Campaign\Models\Campaign $campaign
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \SM\SMException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     */
    protected function sync(Campaign $campaign): void
    {
        $campaignId = $campaign->external_id;
        $userId = Auth::id();

        /**
         * If campaign has no ext id or in Draft mode then do nothing
         */
        if (!$campaignId || $campaign->inState(CampaignStatus::ID_DRAFT)) {
            return;
        }

        $creative = $campaign->getActiveCreative();

        if (!$creative) {
            return;
        }

        if ($creative->external_id) {
            $this->creativeUpdate->handle(new CreativeUpdateParam([
                'campaignId' => $campaignId,
                'creativeId' => $creative->external_id,
            ]), $userId);
        } else {
            $this->creativeUpload->handle(new CreativeUploadParam([
                'campaignId'    => $campaign->external_id,
                'accountId'     => Auth::user()->account_external_id,
                'fileName'      => $creative->name . '.' . $creative->extension,
                'sourceUrl'     => $creative->source_url,
                'contentLength' => (int)$creative->filesize,
            ]), $userId);

            $creative->applyInternalStatus(CreativeStatus::PROCESSING);
            $creative->save();
        }

        $campaign->applyInternalStatus(CampaignStatus::PROCESSING);
        $campaign->save();
        $campaign->load(['creatives', 'status']);
    }
}
