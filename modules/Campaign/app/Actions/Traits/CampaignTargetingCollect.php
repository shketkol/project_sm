<?php

namespace Modules\Campaign\Actions\Traits;

use Illuminate\Database\Eloquent\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\TargetingValue;

trait CampaignTargetingCollect
{
    /**
     * @var string[]
     */
    protected $campaignTargetingRelations = [
        'genres',
        'zipcodes',
        'dma',
        'cities',
        'states',
        'targetingAgeGroups',
        'audiences',
        'deviceGroups',
    ];

    /**
     * @var string[]
     */
    protected $dbRelations = [
        'targetingAgeGroups',
        'audiences',
        'locations',
        'zipcodes',
        'genres',
        'deviceGroups',
    ];

    /**
     * Collect campaign targetings into array
     *
     * @param Campaign $campaign
     * @param bool     $visible
     *
     * @return array
     */
    public function collectCampaignTargetings(Campaign $campaign, $visible = false): array
    {
        $targetingsArray = [];

        foreach ($this->campaignTargetingRelations as $relation) {
            $nonVisibleTargetings = $campaign->{$relation}()->where('visible', $visible)->get();
            if ($nonVisibleTargetings->isNotEmpty()) {
                $targetingsArray[$relation] = $nonVisibleTargetings;
            }
        }

        return $targetingsArray;
    }

    /**
     * Collect campaign all targetings into array
     *
     * @param Campaign $campaign
     *
     * @return array
     */
    public function collectAllCampaignTargetings(Campaign $campaign): array
    {
        $targetingsArray = [];

        foreach ($this->campaignTargetingRelations as $relation) {
            $nonVisibleTargetings = $campaign->{$relation}()->get();
            if ($nonVisibleTargetings->isNotEmpty()) {
                $targetingsArray[$relation] = $nonVisibleTargetings;
            }
        }

        return $targetingsArray;
    }

    /**
     * Collect campaign all targetings into array the way it saved in the database
     *
     * @param Campaign $campaign
     *
     * @return Collection[]|TargetingValue[][]
     */
    public function collectCampaignTargetingsForDatabase(Campaign $campaign): array
    {
        $collection = [];
        foreach ($this->dbRelations as $relation) {
            $collection[$relation] = $campaign->{$relation}()->get();
        }

        return $collection;
    }
}
