<?php

namespace Modules\Campaign\Actions\Traits;

use Illuminate\Support\Arr;

trait GetCampaignDeliveredImpressions
{
    /**
     * Get all delivered impressions for the campaign.
     *
     * Must be in the GetCampaignImpressionsDetailAction response format.
     * @param array $impressionsDetails
     *
     * @return float
     */
    protected function getDeliveredImpressions(array $impressionsDetails): float
    {
        return Arr::get($impressionsDetails, 'impressions');
    }
}
