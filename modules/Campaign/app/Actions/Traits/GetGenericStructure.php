<?php

namespace Modules\Campaign\Actions\Traits;

trait GetGenericStructure
{
    /**
     * @return array
     */
    private function getGenericStructure(): array
    {
        return [
            'cost'                 => 0,
            'performance'          => [
                'total'       => null,
                'startDate'   => null,
                'endDate'     => null,
                'performance' => [],
            ],
            'impressions'          => 0,
            'impressionCounts'     => [],

            /** @deprecated ? */
            'deliveredImpressions' => 0,
        ];
    }
}
