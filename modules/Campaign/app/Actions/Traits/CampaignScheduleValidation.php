<?php

namespace Modules\Campaign\Actions\Traits;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Modules\Campaign\Models\Campaign;

/**
 * @property ValidationRules $validationRules
 */
trait CampaignScheduleValidation
{
    /**
     * Get validation for schedule step.
     *
     * @param Campaign $campaign
     *
     * @return array
     */
    protected function getScheduleValidation(Campaign $campaign): array
    {
        $dateFormat = config('date.format.php');

        if ($campaign->date_start && $campaign->date_end) {
            $timestamp = strtotime(sprintf(
                '%s +%d %s',
                $campaign->date_start->format($dateFormat),
                config('campaign.wizard.date.max_duration.value'),
                config('campaign.wizard.date.max_duration.type')
            ));

            $this->validationRules->set('campaign.details.date.end.before', date($dateFormat, $timestamp));
        }

        return [
            'date.start'  => $this->validationRules->only(
                'campaign.details.date.start',
                ['required', 'date', 'date_format', 'before', 'after']
            ),
            'date.end'    => $this->validationRules->only(
                'campaign.details.date.end',
                ['required', 'date', 'date_format', 'before']
            ),
            'timezone_id' => $this->validationRules->only('campaign.details.timezone_id', ['required', 'exists']),
        ];
    }

    /**
     * @return array
     */
    protected function getScheduleValidationMessages(): array
    {
        return [
            'date.start.after' => __('campaign::messages.validation.dates.invalid'),
        ];
    }
}
