<?php

namespace Modules\Campaign\Actions\Traits;

use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;

trait SavesStep
{
    /**
     * Save campaign step.
     *
     * @param Campaign $campaign
     * @param int $stepId
     * @return $this
     * @throws CampaignNotUpdatedException
     */
    protected function saveStep(Campaign $campaign, int $stepId)
    {
        $this->associateStep($campaign, $stepId);

        if (!$campaign->save()) {
            throw CampaignNotUpdatedException::create('Campaign save was not updated.');
        }

        return $this;
    }

    /**
     * Associate step with the campaign.
     *
     * @param Campaign $campaign
     * @param int $stepId
     */
    protected function associateStep(Campaign $campaign, int $stepId): void
    {
        // We should not override already stored step
        if ($campaign->step && $campaign->step_id > $stepId) {
            return;
        }

        $campaign->step()->associate($stepId);
    }
}
