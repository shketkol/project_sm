<?php

namespace Modules\Campaign\Actions\Traits;

use App\Services\ValidationRulesService\Contracts\ValidationRules;

/**
 * @property ValidationRules $validationRules
 */
trait CampaignBudgetValidation
{
    /**
     * Get validation for budget step.
     *
     * @return array
     */
    protected function getBudgetValidation(): array
    {
        return [
            'cost' => $this->validationRules->only(
                'campaign.budget.cost',
                ['required', 'numeric', 'min', 'max']
            ),
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    protected function getBudgetValidationMessages(): array
    {
        return [
            'cost.required' => __('validation.custom.campaign.cost.required'),
            'cost.min'      => __('validation.custom.campaign.cost.min'),
            'cost.max'      => __('validation.custom.campaign.cost.max'),
        ];
    }
}
