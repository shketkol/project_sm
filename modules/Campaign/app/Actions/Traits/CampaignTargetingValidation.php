<?php

namespace Modules\Campaign\Actions\Traits;

use App\Services\ValidationRulesService\Contracts\ValidationRules;

/**
 * @property ValidationRules $validationRules
 */
trait CampaignTargetingValidation
{
    /**
     * @return array
     */
    protected function getAudiencesValidation(): array
    {
        return [
            'audiences' => $this->validationRules->only(
                'targeting.audience.ids',
                ['array', 'targeting_to_account']
            ),
        ];
    }

    /**
     * @return array
     */
    protected function getZipcodesValidation(): array
    {
        return [
            'zipcodes' => $this->validationRules->only(
                'targeting.zipcode.ids',
                ['array', 'targeting_to_account']
            ),
        ];
    }

    /**
     * @return array
     */
    protected function getGenresValidation(): array
    {
        return [
            'genres' => $this->validationRules->only(
                'targeting.genre.ids',
                ['array', 'targeting_to_account']
            ),
        ];
    }

    /**
     * Get validation for age groups step.
     *
     * @return array
     */
    protected function getAgeGroupsValidation(): array
    {
        return [
            'age_groups'  => $this->validationRules->only(
                'targeting.age_group',
                ['required', 'min', 'array', 'targeting_to_account']
            )
        ];
    }

    /**
     * Get validation for device groups step.
     *
     * @return array
     */
    protected function getDeviceGroupsValidation(): array
    {
        return [
            'device_groups'  => $this->validationRules->only(
                'targeting.device_group.groups',
                ['required', 'min', 'array']
            )
        ];
    }

    /**
     * @return array
     */
    protected function getAgeGroupsValidationMessages(): array
    {
        return [
            'age_groups.required' => __('targeting::messages.empty_age_groups'),
        ];
    }

    /**
     * @return array
     */
    protected function getDeviceGroupsValidationMessages(): array
    {
        return [
            'device_groups.required' => __('targeting::messages.empty_device_groups'),
        ];
    }
}
