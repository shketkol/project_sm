<?php

namespace Modules\Campaign\Actions\Traits;

use App\Services\ValidationRulesService\Contracts\ValidationRules;

/**
 * @property ValidationRules $validationRules
 */
trait CampaignMinImpressionValidation
{
    /**
     * @return array
     */
    protected function getMinImpressionValidation(): array
    {
        return [
            'impressions.min_per_day' => $this->validationRules->only(
                'campaign.budget.impressions.min_per_day',
                ['min_per_day']
            )
        ];
    }
}
