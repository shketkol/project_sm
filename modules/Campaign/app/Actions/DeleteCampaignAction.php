<?php

namespace Modules\Campaign\Actions;

use Illuminate\Log\Logger;
use Modules\Campaign\Exceptions\CampaignNotDeletedException;
use Modules\Campaign\Exceptions\CancelCampaignActivityRequestException;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Mappers\Campaign\ActionMapper;

class DeleteCampaignAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var SendDraftActivityCampaignAction
     */
    private $sendDraftActivity;

    /**
     * @param Logger                          $log
     * @param SendDraftActivityCampaignAction $sendDraftActivity
     */
    public function __construct(Logger $log, SendDraftActivityCampaignAction $sendDraftActivity)
    {
        $this->log = $log;
        $this->sendDraftActivity = $sendDraftActivity;
    }

    /**
     * Delete campaign.
     *
     * @param Campaign $campaign
     *
     * @return void
     * @throws CampaignNotDeletedException
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(Campaign $campaign): void
    {
        $this->log->info('Starting deleting campaign.', ['campaign_id' => $campaign->id]);

        if (!$campaign->isDeletable()) {
            throw CampaignNotDeletedException::create(trans('campaign::messages.delete.not_deletable'));
        }

        $replica = $this->makeReplica($campaign);

        if (!$campaign->delete()) {
            $this->log->warning('Campaign was not deleted.');
            throw CampaignNotDeletedException::create(trans('campaign::messages.delete.failed'));
        }

        $this->sendDraftActivity($replica);

        $this->log->info('Campaign was deleted.', ['campaign_id' => $campaign->id]);
    }

    /**
     * Since we could not send request to HAAPI before campaign is deleted
     * We have to store object data, delete campaign and after that send info to HAAPI
     * This method would not create any record in DB
     *
     * @param Campaign $campaign
     *
     * @return Campaign
     */
    private function makeReplica(Campaign $campaign): Campaign
    {
        $replica = $campaign->replicate();

        $replica->id = $campaign->id;
        $replica->created_at = $campaign->created_at;
        $replica->updated_at = $campaign->updated_at;

        return $replica;
    }

    /**
     * @param Campaign $replica
     *
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignExternalIdOverrideException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    private function sendDraftActivity(Campaign $replica): void
    {
        $data = [
            'action_name' => ActionMapper::CAMPAIGN_DELETED,
            'last_page'   => ActionMapper::CAMPAIGN_DETAILS,
        ];

        try {
            $this->sendDraftActivity->handle($replica, $data);
        } catch (CancelCampaignActivityRequestException $exception) {
            $this->log->info('[Campaign][Draft Activity] Request cancelled.', [
                'campaign_id' => $replica->id,
                'data'        => $data,
                'reason'      => $exception->getMessage(),
            ]);
        } catch (InvalidRequestException $exception) {
            $this->log->warning('[Campaign][Draft Activity] Failed sending activity to HAAPI due to validation.', [
                'campaign_id' => $replica->id,
                'data'        => $data,
                'reason'      => $exception->getMessage(),
            ]);
        } catch (\Throwable $exception) {
            $this->log->error('[Campaign][Draft Activity] Failed sending activity to HAAPI due to error.', [
                'campaign_id' => $replica->id,
                'data'        => $data,
                'reason'      => $exception->getMessage(),
            ]);
        }
    }
}
