<?php

namespace Modules\Campaign\Actions;

use Carbon\Carbon;
use Illuminate\Log\Logger;
use Modules\Campaign\Http\Controllers\Api\Traits\StoreCampaign;
use Modules\Campaign\Models\Campaign;

class CheckDateRangeAction
{
    use StoreCampaign;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * Check campaign date range.
     *
     * @param Campaign $campaign
     *
     * @return string|null
     */
    public function handle(Campaign $campaign): ?string
    {
        if (!$campaign->date_start && !$campaign->date_end) {
            return null;
        }

        $tomorrow = Carbon::tomorrow(config('campaign.wizard.date.timezone'))
            ->format(config('date.format.db_date_time'));

        if ($campaign->date_start >= $tomorrow) {
            return null;
        }

        $campaign = $this->checkDates($campaign);
        $campaign->save();
        $campaign->refresh();

        return __('campaign::messages.update_start_date');
    }
}
