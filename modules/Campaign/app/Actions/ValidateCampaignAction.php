<?php

namespace Modules\Campaign\Actions;

use App\Helpers\NumberFormatHelper;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Traits\CampaignScheduleValidation;
use Modules\Campaign\Actions\Traits\CampaignTargetingValidation;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;

class ValidateCampaignAction
{
    use CampaignScheduleValidation,
        CampaignTargetingValidation;

    /**
     * @var string
     */
    protected $dateFormat;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ValidationRules
     */
    protected $validationRules;

    /**
     * @var UpdateCampaignStepAction
     */
    protected $stepAction;

    /**
     * @var ValidateCampaignTargetingAction
     */
    protected $validateTargetingAction;

    /**
     * @param Logger                          $log
     * @param ValidationRules                 $validationRules
     * @param UpdateCampaignStepAction        $stepAction
     * @param ValidateCampaignTargetingAction $validateTargetingAction
     */
    public function __construct(
        Logger $log,
        ValidationRules $validationRules,
        UpdateCampaignStepAction $stepAction,
        ValidateCampaignTargetingAction $validateTargetingAction
    ) {
        $this->dateFormat = config('date.format.php');
        $this->log = $log;
        $this->validationRules = $validationRules;
        $this->stepAction = $stepAction;
        $this->validateTargetingAction = $validateTargetingAction;
    }

    /**
     * Handle action.
     *
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws ValidationException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException
     */
    public function handle(Campaign $campaign): Campaign
    {
        $validator = Validator::make($this->getData($campaign), $this->getRules($campaign), $this->getMessages());

        if (!$validator->fails()) {
            $this->validateTargetingAction->handle($campaign);
            $this->stepAction->handle($campaign, CampaignStep::ID_REVIEW);

            return $campaign;
        }

        throw new ValidationException($validator);
    }

    /**
     * Get data for validation.
     *
     * @param Campaign $campaign
     *
     * @return array
     */
    protected function getData(Campaign $campaign): array
    {
        return [
            'name'          => $campaign->name,
            'timezone_id'   => $campaign->timezone_id,
            'date'          => [
                'start' => $campaign->date_start ? $campaign->date_start->format($this->dateFormat) : null,
                'end'   => $campaign->date_end ? $campaign->date_end->format($this->dateFormat) : null,
            ],
            'cost'          => $campaign->budget,
            'impressions'   => $campaign->impressions,
            'age_groups'    => $campaign->targetingAgeGroups->pluck('id')->toArray(),
            'device_groups' => $campaign->deviceGroups->pluck('id')->toArray(),
        ];
    }

    /**
     * Get rules for validation.
     *
     * @param Campaign $campaign
     *
     * @return array
     */
    protected function getRules(Campaign $campaign): array
    {
        return array_merge(
            $this->getNameValidation(),
            $this->getScheduleValidation($campaign),
            $this->getBudgetValidation(),
            $this->getAgeGroupsValidation(),
            $this->getDeviceGroupsValidation()
        );
    }

    /**
     * Get validation for name step.
     *
     * @return array
     */
    protected function getNameValidation(): array
    {
        return [
            'name' => $this->validationRules->only(
                'campaign.details.name',
                ['required', 'string', 'max', 'min', 'regex']
            ),
        ];
    }

    /**
     * Get validation for budget step.
     *
     * @return array
     */
    protected function getBudgetValidation(): array
    {
        return [
            'cost'        => $this->validationRules->only(
                'campaign.budget.cost',
                ['required', 'numeric', 'min', 'max']
            ),
            'impressions' => $this->validationRules->only(
                'campaign.budget.impressions',
                ['required', 'numeric', 'min', 'max']
            ),
        ];
    }

    /**
     * Get custom validation messages.
     *
     * @return array
     */
    protected function getMessages(): array
    {
        return array_merge(
            [
                'cost.max' => __('validation.custom.campaign.cost.max', [
                    'max' => NumberFormatHelper::floatCurrency(
                        $this->validationRules->get('campaign.budget.cost.max')
                    ),
                ])
            ],
            $this->getAgeGroupsValidationMessages(),
            $this->getDeviceGroupsValidationMessages()
        );
    }
}
