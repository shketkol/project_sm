<?php

namespace Modules\Campaign\Actions;

use App\Helpers\EnvironmentHelper;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Exceptions\CampaignExternalIdOverrideException;
use Modules\Campaign\Exceptions\CancelCampaignActivityRequestException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Repositories\Contracts\CampaignRepository;
use Modules\Haapi\Actions\Campaign\CampaignDraftSave;
use Modules\Haapi\Actions\Campaign\GetDraftActivityCampaignParamsAction;
use Modules\Haapi\DataTransferObjects\Campaign\DraftActivityCampaignParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\User\Models\UserStatus;

class SendDraftActivityCampaignAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var GetDraftActivityCampaignParamsAction
     */
    private $paramsAction;

    /**
     * @var CampaignDraftSave
     */
    private $draftSave;

    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * @param Logger                               $log
     * @param GetDraftActivityCampaignParamsAction $paramsAction
     * @param CampaignDraftSave                    $draftSave
     * @param CampaignRepository                   $campaignRepository
     */
    public function __construct(
        Logger $log,
        GetDraftActivityCampaignParamsAction $paramsAction,
        CampaignDraftSave $draftSave,
        CampaignRepository $campaignRepository
    ) {
        $this->log = $log;
        $this->paramsAction = $paramsAction;
        $this->draftSave = $draftSave;
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return HaapiResponse
     * @throws CampaignExternalIdOverrideException
     * @throws CancelCampaignActivityRequestException
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function handle(Campaign $campaign, array $data): HaapiResponse
    {
        $this->checkIsRequestAllowed($campaign);

        $this->log->info('[Campaign][Draft Activity] Started sending activity to HAAPI.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        $params = new DraftActivityCampaignParams($this->paramsAction->handle($campaign, $data));
        $response = $this->draftSave->handle($params, $campaign->user_id);
        $this->storeExternalId($campaign, Arr::get($response->getPayload(), 'campaign.id'));

        $this->log->info('[Campaign][Draft Activity] Finished sending activity to HAAPI.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        return $response;
    }

    /**
     * @param Campaign $campaign
     * @param string   $externalId
     *
     * @return Campaign
     * @throws CampaignExternalIdOverrideException
     */
    private function storeExternalId(Campaign $campaign, string $externalId): Campaign
    {
        $this->log->info('[Campaign][Draft Activity] Started updating campaign external id.', [
            'campaign_id' => $campaign->id,
            'external_id' => $externalId,
        ]);

        if (!$campaign->exists) {
            $this->log->info('[Campaign][Draft Activity] Skipped updating campaign external id. Replica provided.', [
                'campaign_id' => $campaign->id,
                'external_id' => $externalId,
            ]);

            return $campaign;
        }

        $this->checkExternalIdOverride($campaign, $externalId);

        if ($campaign->external_id) {
            $this->log->info('[Campaign][Draft Activity] Skipped updating campaign external id. Already exists.', [
                'campaign_id' => $campaign->id,
                'external_id' => $externalId,
            ]);

            return $campaign;
        }

        return $this->saveExternalId($campaign, $externalId);
    }

    /**
     * @param Campaign $campaign
     * @param string   $externalId
     *
     * @throws CampaignExternalIdOverrideException
     */
    private function checkExternalIdOverride(Campaign $campaign, string $externalId): void
    {
        if ($campaign->external_id && $campaign->external_id !== $externalId) {
            $this->log->error('[Campaign][Draft Activity] HAAPI external id does not match with saved.', [
                'campaign_id' => $campaign->id,
                'external_id' => $externalId,
            ]);

            throw CampaignExternalIdOverrideException::create($campaign, $externalId);
        }
    }

    /**
     * @param Campaign $campaign
     * @param string   $externalId
     *
     * @return Campaign
     */
    private function saveExternalId(Campaign $campaign, string $externalId): Campaign
    {
        $campaign = $this->campaignRepository->update(['external_id' => $externalId], $campaign->id);

        $this->log->info('[Campaign][Draft Activity] Finished updating campaign external id.', [
            'campaign_id' => $campaign->id,
            'external_id' => $externalId,
        ]);

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     *
     * @throws CancelCampaignActivityRequestException
     */
    private function checkIsRequestAllowed(Campaign $campaign): void
    {
        if (EnvironmentHelper::isLocalEnv()) {
            $this->log->info('[Campaign][Draft Activity] Cancelled. Environment not allowed.', [
                'campaign_id' => $campaign->id,
            ]);

            throw CancelCampaignActivityRequestException::create($campaign);
        }

        if ($campaign->user->inState(UserStatus::ID_PENDING_MANUAL_REVIEW)) {
            $this->log->info('[Campaign][Draft Activity] Cancelled. User is in pending status.', [
                'campaign_id' => $campaign->id,
            ]);

            throw CancelCampaignActivityRequestException::create($campaign);
        }
    }
}
