<?php

namespace Modules\Campaign\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Campaign\Exceptions\CampaignNotCreatedException;
use Modules\Campaign\Http\Controllers\Api\Traits\StoreCampaign;
use Modules\Campaign\Models\Campaign;

class StoreCampaignAction
{
    use StoreCampaign;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @param DatabaseManager $databaseManager
     * @param Logger          $log
     */
    public function __construct(DatabaseManager $databaseManager, Logger $log)
    {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
    }

    /**
     * Store new campaign.
     *
     * @param array $data
     *
     * @return Campaign
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function handle(array $data = []): Campaign
    {
        $this->log->info('Storing new campaign.', ['data' => $data]);

        $this->databaseManager->beginTransaction();

        try {
            $campaign = $this->storeCampaign($data);
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();

            throw CampaignNotCreatedException::createFrom($throwable);
        }

        $this->databaseManager->commit();

        return $campaign->refresh();
    }
}
