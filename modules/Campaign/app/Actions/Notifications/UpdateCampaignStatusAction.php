<?php

namespace Modules\Campaign\Actions\Notifications;

use Illuminate\Log\Logger;
use Modules\Campaign\Exceptions\CampaignNotFoundException;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Daapi\Actions\Notification\UpdateStatusBaseAction;

class UpdateCampaignStatusAction extends UpdateStatusBaseAction
{
    /**
     * @param Logger             $log
     * @param CampaignRepository $repository
     */
    public function __construct(Logger $log, CampaignRepository $repository)
    {
        parent::__construct($log, $repository);
    }

    /**
     * @param string     $externalId
     * @param string     $status
     * @param array|null $additionalData
     *
     * @throws CampaignNotFoundException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Modules\Daapi\Exceptions\StatusNotFoundException
     * @throws \SM\SMException
     */
    public function handle(string $externalId, string $status, ?array $additionalData): void
    {
        /** @var \Modules\Campaign\Models\Campaign $campaign */
        $campaign = $this->getEntity($externalId);

        if (!$campaign) {
            throw CampaignNotFoundException::create($externalId, 'change status');
        }

        $oldStatus = $campaign->status->name;

        $this->log->info('Updating campaign status.', [
            'campaign_id' => $campaign->id,
            'from_status' => $oldStatus,
            'to_status'   => $status,
        ]);

        $this->applyState($status);

        $this->log->info('Campaign status was successfully updated.', [
            'campaign_id' => $campaign->id,
            'from_status' => $oldStatus,
            'to_status'   => $status,
        ]);
    }
}
