<?php

namespace Modules\Campaign\Actions\SpecialAds;

use App\Exceptions\InvalidArgumentException;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignAgeGroupsAction;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignGendersAction;
use Modules\Targeting\Actions\Audience\UpdateCampaignAudiencesAction;
use Modules\Targeting\Actions\Genre\UpdateCampaignGenresAction;
use Modules\Targeting\Actions\Location\Zipcode\UpdateCampaignZipcodesAction;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Repositories\AgeGroupRepository;
use Modules\Targeting\Repositories\GenderRepository;
use Modules\User\Models\AccountType;

class RemoveSpecialAdsTargetingsAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var GetRelationByPermissionAction
     */
    private $relationByPermissionAction;

    /**
     * @var GenderRepository
     */
    private $genderRepository;

    /**
     * @var UpdateCampaignGendersAction
     */
    private $updateGendersAction;

    /**
     * @var UpdateCampaignAgeGroupsAction
     */
    private $updateAgeGroupsAction;

    /**
     * @var AgeGroupRepository
     */
    private $ageGroupRepository;

    /**
     * @var UpdateCampaignAudiencesAction
     */
    private $updateAudiencesAction;

    /**
     * @var UpdateCampaignGenresAction
     */
    private $updateGenresAction;

    /**
     * @var UpdateCampaignZipcodesAction
     */
    private $updateZipcodesAction;

    /**
     * @param Logger                        $log
     * @param DatabaseManager               $databaseManager
     * @param GetRelationByPermissionAction $relationByPermissionAction
     * @param GenderRepository              $genderRepository
     * @param UpdateCampaignGendersAction   $updateGendersAction
     * @param UpdateCampaignAgeGroupsAction $updateAgeGroupsAction
     * @param AgeGroupRepository            $ageGroupRepository
     * @param UpdateCampaignAudiencesAction $updateAudiencesAction
     * @param UpdateCampaignGenresAction    $updateGenresAction
     * @param UpdateCampaignZipcodesAction  $updateZipcodesAction
     */
    public function __construct(
        Logger $log,
        DatabaseManager $databaseManager,
        GetRelationByPermissionAction $relationByPermissionAction,
        GenderRepository $genderRepository,
        UpdateCampaignGendersAction $updateGendersAction,
        UpdateCampaignAgeGroupsAction $updateAgeGroupsAction,
        AgeGroupRepository $ageGroupRepository,
        UpdateCampaignAudiencesAction $updateAudiencesAction,
        UpdateCampaignGenresAction $updateGenresAction,
        UpdateCampaignZipcodesAction $updateZipcodesAction
    ) {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
        $this->relationByPermissionAction = $relationByPermissionAction;
        $this->genderRepository = $genderRepository;
        $this->updateGendersAction = $updateGendersAction;
        $this->updateAgeGroupsAction = $updateAgeGroupsAction;
        $this->ageGroupRepository = $ageGroupRepository;
        $this->updateAudiencesAction = $updateAudiencesAction;
        $this->updateGenresAction = $updateGenresAction;
        $this->updateZipcodesAction = $updateZipcodesAction;
    }

    /**
     * @param Campaign $campaign
     *
     * @return string|null
     * @throws \Throwable
     */
    public function handle(Campaign $campaign): ?string
    {
        if ($campaign->user->account_type_id === AccountType::ID_COMMON) {
            return null;
        }

        $this->log->info('Started removing targetings that forbidden to advertiser account type.', [
            'campaign_id'     => $campaign->id,
            'account_type_id' => $campaign->user->account_type_id,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $changes = $this->removeForbiddenTargetings($campaign);
        } catch (\Throwable $exception) {
            $this->log->warning('Failed to remove targetings that forbidden to advertiser account type.', [
                'campaign_id'     => $campaign->id,
                'account_type_id' => $campaign->user->account_type_id,
                'reason'          => $exception->getMessage(),
            ]);
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('Finished removing targetings that forbidden to advertiser account type.', [
            'campaign_id'     => $campaign->id,
            'account_type_id' => $campaign->user->account_type_id,
        ]);

        if ($changes === 0) {
            return null;
        }

        return __(
            'campaign::messages.special_ads.targetings_removed',
            [
                'url' => config('general.links.faq.special_ads_categories'),
            ]
        );
    }

    /**
     * @param Campaign $campaign
     *
     * @return int
     * @throws \App\Exceptions\InvalidArgumentException
     */
    private function removeForbiddenTargetings(Campaign $campaign): int
    {
        $targetingPermissions = $this->getForbiddenTargetingPermissions($campaign);
        $changes = 0;

        if (empty($targetingPermissions)) {
            return $changes;
        }

        foreach ($targetingPermissions as $permission) {
            $relation = $this->relationByPermissionAction->handle($permission);
            $changes += $this->handleForbiddenTargetingRelation($campaign, $relation);
        }

        return $changes;
    }

    /**
     * @param Campaign $campaign
     * @param string   $relation
     *
     * @return int
     * @throws InvalidArgumentException
     */
    private function handleForbiddenTargetingRelation(Campaign $campaign, string $relation): int
    {
        $map = [
            Campaign::RELATION_GENDERS_NAME    => function (Campaign $campaign): int {
                return $this->setGenderToAll($campaign);
            },
            Campaign::RELATION_AGE_GROUPS_NAME => function (Campaign $campaign): int {
                return $this->setAgeGroupsToAll($campaign);
            },
            Campaign::RELATION_AUDIENCES_NAME  => function (Campaign $campaign): int {
                return $this->setAudiencesToAll($campaign);
            },
            Campaign::RELATION_GENRES_NAME     => function (Campaign $campaign): int {
                return $this->setGenresToAll($campaign);
            },
            Campaign::RELATION_ZIPCODES_NAME   => function (Campaign $campaign): int {
                return $this->setZipcodesToAll($campaign);
            },
        ];

        $callable = Arr::get($map, $relation);

        if (is_null($callable)) {
            throw new InvalidArgumentException(sprintf('Campaign relation "%s" does not have handler.', $relation));
        }

        return $callable($campaign);
    }

    /**
     * @param Campaign $campaign
     *
     * @return int
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function setGenderToAll(Campaign $campaign): int
    {
        $allGender = $this->getAllGenderModel();
        $campaignGendersIds = $campaign->targetingGenders->pluck('id')->toArray();
        $this->updateGendersAction->handle($campaign, $allGender->id);
        $count = count(array_diff($campaignGendersIds, [$allGender->id]));

        $this->log->info('Changes after setting gender to all.', [
            'campaign_id' => $campaign->id,
            'changes'     => $count,
        ]);

        return $count;
    }

    /**
     * @return Gender
     */
    private function getAllGenderModel(): Gender
    {
        return $this->genderRepository->getAllGenderModel();
    }

    /**
     * @param Campaign $campaign
     *
     * @return int
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function setAgeGroupsToAll(Campaign $campaign): int
    {
        $gender = $this->getAllGenderModel();
        $allAgeGroupsIds = $this->ageGroupRepository->findWhere([['gender_id', '=', $gender->id]])
            ->pluck('id')
            ->toArray();

        $campaignAgeGroupsIds = $campaign->targetingAgeGroups->pluck('id')->toArray();
        $this->updateAgeGroupsAction->handle($campaign, $allAgeGroupsIds);
        $count = count(array_diff($campaignAgeGroupsIds, $allAgeGroupsIds));

        $this->log->info('Changes after setting age groups to all.', [
            'campaign_id' => $campaign->id,
            'changes'     => $count,
        ]);

        return $count;
    }

    /**
     * @param Campaign $campaign
     *
     * @return int
     * @throws \Throwable
     */
    private function setZipcodesToAll(Campaign $campaign): int
    {
        $count = $campaign->zipcodes()->count();

        if ($count === 0) {
            return $count;
        }

        $this->updateZipcodesAction->handle($campaign, []);

        $this->log->info('Changes after setting zipcodes to all.', [
            'campaign_id' => $campaign->id,
            'changes'     => $count,
        ]);

        return $count;
    }

    /**
     * @param Campaign $campaign
     *
     * @return int
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Throwable
     */
    private function setGenresToAll(Campaign $campaign): int
    {
        $count = $campaign->genres()->count();

        if ($count === 0) {
            return $count;
        }

        $this->updateGenresAction->handle($campaign, []);

        $this->log->info('Changes after setting genres to all.', [
            'campaign_id' => $campaign->id,
            'changes'     => $count,
        ]);

        return $count;
    }

    /**
     * @param Campaign $campaign
     *
     * @return int
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Throwable
     */
    private function setAudiencesToAll(Campaign $campaign): int
    {
        $count = $campaign->audiences()->count();

        if ($count === 0) {
            return $count;
        }

        $this->updateAudiencesAction->handle($campaign, []);

        $this->log->info('Changes after setting audience to all.', [
            'campaign_id' => $campaign->id,
            'changes'     => $count,
        ]);

        return $count;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     */
    private function getForbiddenTargetingPermissions(Campaign $campaign): array
    {
        $permissions = array_keys($campaign->user->getTargetingPermissions());

        $targetings = array_filter($permissions, function (string $permission) use ($campaign): bool {
            return !$campaign->user->canAccessTargeting($permission);
        });

        if (empty($targetings)) {
            $this->log->info('Not found any targetings that forbidden to advertiser account type.', [
                'campaign_id'     => $campaign->id,
                'account_type_id' => $campaign->user->account_type_id,
                'targetings'      => $targetings,
            ]);

            return $targetings;
        }

        $this->log->info('Found targetings that forbidden to advertiser account type.', [
            'campaign_id'     => $campaign->id,
            'account_type_id' => $campaign->user->account_type_id,
            'targetings'      => $targetings,
        ]);

        return $targetings;
    }
}
