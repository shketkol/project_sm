<?php

namespace Modules\Campaign\Actions\SpecialAds;

use App\Exceptions\InvalidArgumentException;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Zipcode;

class GetRelationByPermissionAction
{
    /**
     * Targeting permission to campaign targeting relation mapper
     *
     * @var string[]
     */
    private const PERMISSIONS_MAPPER = [
        Gender::TYPE_NAME   => Campaign::RELATION_GENDERS_NAME,
        AgeGroup::TYPE_NAME => Campaign::RELATION_AGE_GROUPS_NAME,
        Audience::TYPE_NAME => Campaign::RELATION_AUDIENCES_NAME,
        Location::TYPE_NAME => Campaign::RELATION_LOCATIONS_NAME,
        Zipcode::TYPE_NAME  => Campaign::RELATION_ZIPCODES_NAME,
        Genre::TYPE_NAME    => Campaign::RELATION_GENRES_NAME,
    ];

    /**
     * @param string $permission
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function handle(string $permission): string
    {
        $relation = Arr::get(self::PERMISSIONS_MAPPER, $permission);

        if (is_null($relation)) {
            throw new InvalidArgumentException(sprintf(
                'Targeting relation not found for "%s" permission.',
                $permission
            ));
        }

        return $relation;
    }
}
