<?php

namespace Modules\Campaign\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\User\Models\User;
use SM\SMException;

class GetCampaignEditRulesAction
{
    /**
     * Get rules that available for advertiser to edit in campaign.
     *
     * @param User|Authenticatable $user
     * @param Campaign             $campaign
     *
     * @return array
     * @throws SMException
     */
    public function handle(User $user, Campaign $campaign): array
    {
        return [
            'can_pause'                => $user->can('campaign.pause', $campaign)
                                       && $campaign->canApply(CampaignStatus::PAUSED),
            'can_cancel'               => $user->can('campaign.cancel', $campaign)
                                       && $campaign->canApply(CampaignStatus::CANCELED),
            'can_resume'               => $user->can('campaign.resume', $campaign)
                                       && $campaign->canApply(CampaignStatus::LIVE),
            'can_edit'                 => $user->can('campaign.update', $campaign),
            'can_delete'               => $user->can('campaign.delete', $campaign),
            'can_duplicate'            => $user->can('campaign.clone', $campaign),
            'can_open_advertiser_page' => $user->can('advertiser.view', $campaign->user),
            'can_download_report'      => $user->can('campaign.downloadReport', $campaign),
        ];
    }
}
