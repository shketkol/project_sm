<?php

namespace Modules\Campaign\Actions;

use Modules\Campaign\Actions\Traits\CampaignCreativeSync;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Notification\Models\AlertType;

class UpdateCampaignCreativeAction
{
    use CampaignCreativeSync;

    /**
     * Update campaign creative.
     *
     * @param Campaign $campaign
     * @param int|null $creativeId
     *
     * @return Campaign
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Exception
     */
    public function handle(Campaign $campaign, ?int $creativeId = null): Campaign
    {
        $this->databaseManager->beginTransaction();
        try {
            $creativeId ? $this->attach($campaign, $creativeId) : $this->detach($campaign);
            $this->stepAction->handle($campaign, CampaignStep::ID_CREATIVE);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            $this->log->warning('Campaign creative was not updated.', [
                'campaign_id' => $campaign->id,
                'reason'      => $exception->getMessage(),
            ]);

            throw CampaignNotUpdatedException::create($exception->getMessage(), $exception);
        }
        $this->databaseManager->commit();

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     * @param int      $creativeId
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function attach(Campaign $campaign, int $creativeId): void
    {
        $this->log->info('Attach creative to campaign', [
            'campaign_id' => $campaign->getKey(),
            'creative_id' => $creativeId,
        ]);

        $campaign->creatives()->attach($creativeId);
        $campaign->refresh();
        $this->sync($campaign);

        $this->log->info('Campaign creative was successfully attached.', [
            'campaign_id' => $campaign->getKey(),
            'creative_id' => $creativeId,
        ]);

        $this->destroyAlertsAction->handle($campaign, [
            AlertType::ID_CANT_GO_LIVE,
            AlertType::ID_CREATIVE_MISSING,
        ]);
    }

    /**
     * @param Campaign $campaign
     */
    protected function detach(Campaign $campaign): void
    {
        $this->log->info('Detach all creatives from campaign', [
            'campaign_id' => $campaign->getKey(),
        ]);
        $campaign->creatives()->detach();

        $this->log->info('Campaign creative was successfully detached', [
            'campaign_id' => $campaign->getKey(),
        ]);

        $this->setCampaignAlertsAction->handle($campaign);
    }
}
