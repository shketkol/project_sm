<?php

namespace Modules\Campaign\Actions;

use App\Repositories\Contracts\TimezoneRepository;
use Carbon\Carbon;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Illuminate\Config\Repository as Config;

class UpdateCampaignScheduleAction
{
    /**
     * @var Carbon
     */
    protected $carbon;

    /**
     * @var mixed
     */
    protected $dateFormat;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var TimezoneRepository
     */
    protected $timezoneRepository;

    /**
     * @var UpdateCampaignStepAction
     */
    protected $stepAction;

    /**
     * UpdateCampaignScheduleAction constructor.
     *
     * @param Carbon                   $carbon
     * @param Config                   $config
     * @param Logger                   $log
     * @param TimezoneRepository       $timezoneRepository
     * @param UpdateCampaignStepAction $stepAction
     */
    public function __construct(
        Carbon $carbon,
        Config $config,
        Logger $log,
        TimezoneRepository $timezoneRepository,
        UpdateCampaignStepAction $stepAction
    ) {
        $this->carbon = $carbon;
        $this->dateFormat = $config->get('date.format.php');
        $this->log = $log;
        $this->timezoneRepository = $timezoneRepository;
        $this->stepAction = $stepAction;
    }

    /**
     * Update date range for the campaign.
     *
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Campaign $campaign, array $data = []): Campaign
    {
        $this->log->info('Updating campaign schedule.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        $this->setStartDate($campaign, Arr::get($data, 'date.start'));
        $this->setEndDate($campaign, Arr::get($data, 'date.end'));
        $this->setTimezone($campaign, Arr::get($data, 'timezone_id'));
        $this->stepAction->handle($campaign, CampaignStep::ID_SCHEDULE);

        if (!$campaign->save()) {
            $this->log->warning('Campaign schedule was not updated.', ['campaign_id' => $campaign->id]);
            throw CampaignNotUpdatedException::create();
        }

        $this->log->info('Campaign schedule was successfully updated.', ['campaign_id' => $campaign->id]);

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     * @param int|null $timezoneId
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function setTimezone(Campaign $campaign, ?int $timezoneId = null): void
    {
        if (is_null($timezoneId)) {
            return;
        }

        /** @var \App\Models\Timezone $timezone */
        $timezone = $this->timezoneRepository->findSoft((int)$timezoneId);
        $campaign->timezone_id = $timezone ? $timezone->id : null;
    }

    /**
     * @param Campaign    $campaign
     * @param string|null $start
     */
    protected function setStartDate(Campaign $campaign, ?string $start = null): void
    {
        if (is_null($start)) {
            return;
        }

        $date = $this->carbon->createFromFormat($this->dateFormat, $start)
            ->setTime(
                config('campaign.wizard.time.start.hours'),
                config('campaign.wizard.time.start.minutes'),
                config('campaign.wizard.time.start.seconds')
            );

        $campaign->date_start = $date;
    }

    /**
     * @param Campaign    $campaign
     * @param string|null $end
     */
    protected function setEndDate(Campaign $campaign, ?string $end = null): void
    {
        if (is_null($end)) {
            return;
        }

        $date = $this->carbon->createFromFormat($this->dateFormat, $end)
            ->setTime(
                config('campaign.wizard.time.end.hours'),
                config('campaign.wizard.time.end.minutes'),
                config('campaign.wizard.time.end.seconds')
            );

        $campaign->date_end = $date;
    }
}
