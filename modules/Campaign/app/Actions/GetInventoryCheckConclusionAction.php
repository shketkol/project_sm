<?php

namespace Modules\Campaign\Actions;

use Carbon\Carbon;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Campaign\Actions\Traits\Targetings;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Models\InventoryCheckStatus;
use Modules\Campaign\Models\InventoryCheckTargeting;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\DeviceGroup;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\TargetingValue;
use Modules\Targeting\Models\Zipcode;
use Modules\Targeting\Repositories\AgeGroupRepository;
use Modules\Targeting\Repositories\DeviceGroupRepository;

class GetInventoryCheckConclusionAction
{
    use Targetings;

    public const AVAILABLE = 'available';
    public const UNAVAILABLE = 'unavailable';
    public const UNPROCESSABLE = 'unprocessable';

    /**
     * Targeting values used on front-end same as __('campaign::labels.inventory_steps') keys
     */
    private const LOCATION = 'locations';
    private const ZIPCODE = 'locations';
    private const AGE_GROUP = 'ages';
    private const AUDIENCE = 'audiences';
    private const DEVICE_GROUP = 'devices';
    private const GENRE = 'genres';
    private const DATE_RANGE = 'schedule';

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var AgeGroupRepository
     */
    private $ageGroupRepository;

    /**
     * @var InventoryCheckHistoryRepository
     */
    private $historyRepository;

    /**
     * @var DeviceGroupRepository
     */
    private $deviceRepository;

    /**
     * @param Logger                          $log
     * @param AgeGroupRepository              $ageGroupRepository
     * @param InventoryCheckHistoryRepository $historyRepository
     * @param DeviceGroupRepository           $deviceRepository
     */
    public function __construct(
        Logger $log,
        AgeGroupRepository $ageGroupRepository,
        InventoryCheckHistoryRepository $historyRepository,
        DeviceGroupRepository $deviceRepository
    ) {
        $this->log = $log;
        $this->ageGroupRepository = $ageGroupRepository;
        $this->historyRepository = $historyRepository;
        $this->deviceRepository = $deviceRepository;
    }

    /**
     * @param InventoryCheckHistory $history
     * @param Carbon|null           $updatedAt
     *
     * @return array[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(InventoryCheckHistory $history, ?Carbon $updatedAt = null): array
    {
        $conclusion = [
            'data'        => $this->getResultTypeData($history),
            'suggestions' => $this->findSuggestions($history),
            'updated'     => $updatedAt ?: $history->created_at,
        ];

        if (config('inventory-check.dynamic_inventory_check.debug.enabled')) {
            Arr::set($conclusion, 'debug', $this->findDetailedReasonsForFail($history));
        }

        // save history before inventory check so if second request would come earlier we would new which one is first
        return $conclusion;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function findSuggestions(InventoryCheckHistory $history): array
    {
        if ($history->status_id === InventoryCheckStatus::ID_AVAILABILITY_UNDER_MIN_SPEND) {
            return $this->findSuggestionsForUnderMinSpend($history);
        }

        if ($history->status_id === InventoryCheckStatus::ID_YX_CONNECTION_FAILED) {
            return $this->findSuggestionsForError($history);
        }

        if ($history->status_id === InventoryCheckStatus::ID_UNAVAILABLE) {
            return $this->findSuggestionsForUnavailable($history);
        }

        if ($history->status_id === InventoryCheckStatus::ID_REQUEST_UNDER_MIN_PER_DAY) {
            return $this->findSuggestionsForUnderMinPerDay($history);
        }

        return [];
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function findSuggestionsForUnderMinPerDay(InventoryCheckHistory $history): array
    {
        $steps = $this->findShortReasonsForFail($history);
        $suggestions = [
            'message' => __('campaign::messages.min_budget_message', [
                'value' => $history->min_daily_impressions,
            ]),
            'action'  => [
                'steps'  => $this->sortActionSteps($steps),
                'budget' => __('campaign::messages.change_budget'),
            ],
        ];

        return $this->prepareSuggestion($suggestions, $history);
    }

    /**
     * @param array                 $suggestions
     * @param InventoryCheckHistory $history
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function prepareSuggestion(array $suggestions, InventoryCheckHistory $history): array
    {
        if ($this->shouldAddHelpMessage($history)) {
            Arr::set($suggestions, 'help', $this->getHelpMessages());
        }

        return $suggestions;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function shouldAddHelpMessage(InventoryCheckHistory $history): bool
    {
        $count = $this->historyRepository->countFailed($history->campaign);
        $needForHelp = config('inventory-check.dynamic_inventory_check.unavailable.times_before_faq');

        return $count >= $needForHelp;
    }

    /**
     * @return string[]
     */
    private function getHelpMessages(): array
    {
        return [
            'title'   => __('campaign::messages.dynamic_inventory_check.help.title'),
            'message' => __('campaign::messages.dynamic_inventory_check.help.message'),
            'link'    => config('inventory-check.dynamic_inventory_check.unavailable.faq_link'),
        ];
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return int
     */
    private function calculateMinimumImpressionsForCampaign(InventoryCheckHistory $history): int
    {
        return $history->campaign->getDuration() * $history->min_daily_impressions;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function findSuggestionsForUnderMinSpend(InventoryCheckHistory $history): array
    {
        $steps = $this->findShortReasonsForFail($history);
        $suggestions = [
            'action' => [
                'steps' => $this->sortActionSteps($steps),
            ],
        ];

        return $this->prepareSuggestion($suggestions, $history);
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     */
    private function findSuggestionsForError(InventoryCheckHistory $history): array
    {
        return [
            // @todo find better message
            'message' => 'An error has occurred. Could you please restart inventory check in a few moments?',
        ];
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function findSuggestionsForUnavailable(InventoryCheckHistory $history): array
    {
        $steps = $this->findShortReasonsForFail($history);
        $availableBudget = number_format($history->available_budget, 2);

        $suggestions = [
            'message' => __('campaign::messages.dynamic_inventory_check.error.unavailable', [
                'budget' => $availableBudget,
            ]),
            'action'  => [
                'budget' => sprintf('Change budget to $%s.', $availableBudget),
                'steps'  => $this->sortActionSteps($steps),
            ],
            'payload' => [
                'availableBudget' => round($history->available_budget, 2),
            ],
        ];

        return $this->prepareSuggestion($suggestions, $history);
    }

    /**
     * @param string[] $steps
     *
     * @return array
     */
    private function sortActionSteps(array $steps): array
    {
        $order = [
            self::GENRE,
            self::AUDIENCE,
            self::AGE_GROUP,
            self::LOCATION,
            self::ZIPCODE,
            self::DEVICE_GROUP,
            self::DATE_RANGE,
        ];

        // sort with pre-defined order
        $sorted = array_replace(array_flip($order), $steps);

        // remove empty steps
        $collection = collect($sorted)->filter(function ($value): bool {
            return is_string($value);
        });

        return $collection->toArray();
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     */
    private function getResultTypeData(InventoryCheckHistory $history): array
    {
        $result = [
            'type'               => self::AVAILABLE,
            'value'              => __('campaign::messages.inventory_check.availability.' . self::AVAILABLE),
            'message'            => __('campaign::messages.inventory_check.tooltips.' . self::AVAILABLE),
            'indicator_position' => $this->calculateDeliveryIndicatorArrowPosition($history),
        ];

        if ($history->ordered_impressions > $history->available_impressions) {
            Arr::set($result, 'type', self::UNAVAILABLE);
            Arr::set(
                $result,
                'message', __('campaign::messages.inventory_check.tooltips.' . self::UNAVAILABLE)
            );
            Arr::set(
                $result,
                'value', __('campaign::messages.inventory_check.availability.' . self::UNAVAILABLE)
            );
            return $result;
        }

        // at least twice as big inventory then ordered
        if (($history->ordered_impressions * 2) < $history->available_impressions) {
            return $result;
        }

        if ($history->ordered_impressions <= $history->available_impressions) {
            Arr::set(
                $result,
                'message', __('campaign::messages.inventory_check.tooltips.some_available')
            );
            Arr::set(
                $result,
                'value', __('campaign::messages.inventory_check.availability.some_available')
            );

            return $result;
        }

        return $result;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return array
     */
    private function findShortReasonsForFail(InventoryCheckHistory $history): array
    {
        if ($history->isDrasticLack()) {
            $history = $this->historyRepository->findFirstDrasticFailedHistory($history);
        }

        [
            'dates'      => $dates,
            'targetings' => $targetings,
        ] = $this->findDiffBetweenSuccessfulAndFailedInventoryCheck($history);

        $messages = [];

        if ($this->areDatesChanged($dates)) {
            if ($this->areDatesBecomeWider($dates)) {
                $messages[self::DATE_RANGE] = __('campaign::messages.dynamic_inventory_check.schedule.narrow');
            } else {
                $messages[self::DATE_RANGE] = __('campaign::messages.dynamic_inventory_check.schedule.expand');
            }
        }

        if ($this->shouldShowTargetingsSuggestions($targetings, $messages, $history)) {
            $targetingsSuggestions = $this->prepareTargetingSuggestions($history);
            $messages = array_merge($messages, $targetingsSuggestions);
        }

        return $messages;
    }

    /**
     * @param array                 $targetings
     * @param array                 $messages
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function shouldShowTargetingsSuggestions(
        array $targetings,
        array $messages,
        InventoryCheckHistory $history
    ): bool {
        if ($this->isTargetingsChanged($targetings)) {
            return true;
        }

        if (empty($messages) && $history->status_id === InventoryCheckStatus::ID_UNAVAILABLE) {
            return true;
        }

        return false;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     */
    private function prepareTargetingSuggestions(InventoryCheckHistory $history): array
    {
        $messages = [];
        $historyTargetings = $this->getTargetingsFromHistory($history->targetings);

        foreach ($historyTargetings as $targeting) {
            $messages = array_merge($messages, $this->createTargetMoreMessage($targeting));
        }

        if (Arr::has($messages, self::AGE_GROUP) && $this->checkIfAllAgesChosen($history)) {
            $messages = $this->removeTargetingFromMessages($messages, self::AGE_GROUP);
        }

        if (Arr::has($messages, self::DEVICE_GROUP) && $this->checkIfAllDevicesChosen($history)) {
            $messages = $this->removeTargetingFromMessages($messages, self::DEVICE_GROUP);
        }

        return $messages;
    }

    /**
     * @param TargetingValue $targeting
     *
     * @return string[]
     */
    private function createTargetMoreMessage(TargetingValue $targeting): array
    {
        return [$this->formatTargetingClassToName(get_class($targeting)) => 'Target more'];
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function checkIfAllDevicesChosen(InventoryCheckHistory $history): bool
    {
        $selectedDevices = $this->getSelectedDevicesFromHistory($history);
        $allDevices = $this->deviceRepository->all();

        return $selectedDevices->count() === $allDevices->count();
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return Collection|DeviceGroup[]
     */
    private function getSelectedDevicesFromHistory(InventoryCheckHistory $history): Collection
    {
        $devices = $history->targetings->filter(function (InventoryCheckTargeting $targeting): bool {
            return $targeting->targetable_type === DeviceGroup::class;
        });

        return $devices->map(function (InventoryCheckTargeting $targeting): DeviceGroup {
            return $targeting->targetable;
        });
    }

    /**
     * @param array  $messages
     * @param string $targeting
     *
     * @return array
     */
    private function removeTargetingFromMessages(array $messages, string $targeting): array
    {
        Arr::forget($messages, $targeting);

        return $messages;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function checkIfAllAgesChosen(InventoryCheckHistory $history): bool
    {
        /** @var Collection|InventoryCheckHistory[] $ages */
        $ages = $history->targetings->filter(function (InventoryCheckTargeting $targeting): bool {
            return ($targeting->targetable instanceof AgeGroup);
        });

        $ages = $this->getTargetingsFromHistory($ages);

        return $this->areSelectedAllTargetings($ages);
    }

    /**
     * @param Collection $selectedAges
     *
     * @return bool
     */
    private function areSelectedAllTargetings(Collection $selectedAges): bool
    {
        $all = $this->ageGroupRepository->all();
        $allGender = Gender::getAllGender();

        // at first check all
        $agesAll = $all->filter(function (AgeGroup $age) use ($allGender): bool {
            return $age->gender_id === $allGender->id;
        });

        if ($agesAll->diff($selectedAges)->isEmpty()) {
            return true;
        }

        // then check all by all males and all females
        $agesMaleAndFemale = $all->filter(function (AgeGroup $age) use ($allGender): bool {
            return $age->gender_id !== $allGender->id;
        });

        if ($agesMaleAndFemale->diff($selectedAges)->isEmpty()) {
            return true;
        }

        return false;
    }

    /**
     * @param string $targetingClassName
     *
     * @return string
     */
    private function formatTargetingClassToName(string $targetingClassName): string
    {
        $map = [
            Location::class    => self::LOCATION,
            Zipcode::class     => self::ZIPCODE,
            AgeGroup::class    => self::AGE_GROUP,
            Audience::class    => self::AUDIENCE,
            DeviceGroup::class => self::DEVICE_GROUP,
            Genre::class       => self::GENRE,
        ];

        return Arr::get($map, $targetingClassName);
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return bool
     */
    private function areDatesBecomeWider(array $dates): bool
    {
        $succeededStart = $this->getSucceededStartDate($dates);
        $failedStart = $this->getFailedStartDate($dates);
        $succeededEnd = $this->getSucceededEndDate($dates);
        $failedEnd = $this->getFailedEndDate($dates);

        if (is_null($succeededStart) || is_null($succeededEnd)) {
            return false;
        }

        $failedLength = $failedStart->diffInDays($failedEnd);
        $succeededLength = $succeededStart->diffInDays($succeededEnd);

        return $failedLength > $succeededLength;
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return Carbon|null
     */
    private function getSucceededStartDate(array $dates): ?Carbon
    {
        return Arr::get($dates, 'succeeded.date_start');
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return Carbon|null
     */
    private function getSucceededEndDate(array $dates): ?Carbon
    {
        return Arr::get($dates, 'succeeded.date_end');
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return Carbon
     */
    private function getFailedStartDate(array $dates): Carbon
    {
        return Arr::get($dates, 'failed.date_start');
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return Carbon
     */
    private function getFailedEndDate(array $dates): Carbon
    {
        return Arr::get($dates, 'failed.date_end');
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     */
    private function findDetailedReasonsForFail(InventoryCheckHistory $history): array
    {
        if ($history->status_id === InventoryCheckStatus::ID_AVAILABLE) {
            return ['👌'];
        }

        [
            'dates'      => $dates,
            'targetings' => $targetings,
        ] = $this->findDiffBetweenSuccessfulAndFailedInventoryCheck($history);

        $messages = ['Possible reasons that failed inventory check.'];

        if ($this->areDatesChanged($dates)) {
            $messages[] = $this->getDatesChangedMessage($dates);
        }

        if ($this->isTargetingsChanged($targetings)) {
            $messages[] = $this->getTargetingsChanged($targetings);
        }

        // if diff is empty but inventory failed -> add message that probably inventory changed size in YX
        if (!$this->areDatesChanged($dates) && !$this->isTargetingsChanged($targetings)) {
            $messages[] = 'Inventory check failed not because of changed choices.';
        }

        return $messages;
    }

    /**
     * @param Collection[][] $targetings
     *
     * @return array
     */
    private function getTargetingsChanged(array $targetings): array
    {
        $added = $this->getAddedTargetings($targetings);
        $removed = $this->getRemovedTargetings($targetings);

        // only removed
        if ($added->isEmpty() && $removed->isNotEmpty()) {
            return [
                'message'    => 'Removed targetings also removed most of impressions.',
                'targetings' => [
                    'added'   => $this->targetingsToString($added),
                    'removed' => $this->targetingsToString($removed),
                ],
            ];
        }

        // only added
        if ($removed->isEmpty() && $added->isNotEmpty()) {
            return [
                'message'    => 'Added targetings don\'t have enough of impressions.',
                'targetings' => [
                    'added'   => $this->targetingsToString($added),
                    'removed' => $this->targetingsToString($removed),
                ],
            ];
        }

        // added and removed
        return [
            'message'    => 'Inventory check was successful for removed targetings but failed for added.',
            'targetings' => [
                'added'   => $this->targetingsToString($added),
                'removed' => $this->targetingsToString($removed),
            ],
        ];
    }

    /**
     * @param Collection[][] $targetings
     *
     * @return Collection
     */
    private function getAddedTargetings(array $targetings): Collection
    {
        return Arr::get($targetings, 'added');
    }

    /**
     * @param Collection[][] $targetings
     *
     * @return Collection
     */
    private function getRemovedTargetings(array $targetings): Collection
    {
        return Arr::get($targetings, 'removed');
    }

    /**
     * @param Collection $targetings
     *
     * @return Collection
     */
    private function targetingsToString(Collection $targetings): Collection
    {
        $data = $targetings->map(function (TargetingValue $targeting): array {
            return [
                'name'     => $targeting->name,
                'type'     => $this->getTargetingNameFromClass(get_class($targeting)),
                'excluded' => $targeting->excluded, // dynamic attribute that not exists in original model
            ];
        });

        return $data->map(function (array $targeting): string {
            return sprintf(
                '%s: %s - %s',
                Arr::get($targeting, 'type'),
                Arr::get($targeting, 'name'),
                (bool)Arr::get($targeting, 'excluded') === true ? 'excluded' : 'included',
            );
        });
    }

    /**
     * @param string $class
     *
     * @return string
     */
    private function getTargetingNameFromClass(string $class): string
    {
        return Arr::last(explode('\\', $class));
    }

    /**
     * @param array $dates
     *
     * @return string
     */
    private function getDatesChangedMessage(array $dates): string
    {
        $succeededStart = $this->getSucceededStartDate($dates);
        $succeededEnd = $this->getSucceededEndDate($dates);

        $failedStart = $this->getFailedStartDate($dates);
        $failedEnd = $this->getFailedEndDate($dates);

        if (is_null($succeededStart) && is_null($succeededEnd)) {
            return sprintf(
                'Those dates (%s - %s) don\'t have enough of impressions.',
                $failedStart,
                $failedEnd
            );
        }

        // start date is changed only
        if ($succeededStart !== $failedStart && $succeededEnd === $failedEnd) {
            return sprintf(
                'Start dates (%s -> %s) were the reason of failing inventory check.',
                $succeededStart,
                $failedStart
            );
        }

        // end date is changed only
        if ($succeededEnd !== $failedEnd && $succeededStart !== $failedStart) {
            return sprintf(
                'End dates (%s -> %s) were the reason of failing inventory check.',
                $succeededEnd,
                $failedEnd
            );
        }

        // both dates is changed
        return sprintf(
            'Changed start and end dates (Start: %s -> %s, End: %s -> %s) don\'t have enough of impressions.',
            $succeededStart,
            $failedStart,
            $succeededEnd,
            $failedEnd
        );
    }

    /**
     * @param array $dates
     *
     * @return bool
     */
    private function areDatesChanged(array $dates): bool
    {
        return !empty($dates);
    }

    /**
     * @param array $targetings
     *
     * @return bool
     */
    private function isTargetingsChanged(array $targetings): bool
    {
        return $this->getAddedTargetings($targetings)->isNotEmpty()
            || $this->getRemovedTargetings($targetings)->isNotEmpty();
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return array[]
     */
    private function findDiffBetweenSuccessfulAndFailedInventoryCheck(InventoryCheckHistory $history): array
    {
        $latestSucceededId = $this->historyRepository->findLastSucceededHistoryId($history);

        if (is_null($latestSucceededId)) {
            return [
                'dates'      => [
                    'succeeded' => [],
                    'failed'    => [
                        'date_start' => $history->date_start,
                        'date_end'   => $history->date_end,
                    ],
                ],
                'targetings' => [
                    'added'   => $this->getTargetingsFromHistory($history->targetings),
                    'removed' => collect([]),
                ],
            ];
        }

        $latestSucceeded = $this->historyRepository->findById($latestSucceededId);

        $targetingsDiff = $this->findAddedAndRemovedTargetings($latestSucceeded->targetings, $history->targetings);
        $datesDiff = $this->findChangedDates($latestSucceeded, $history);

        return [
            'dates'      => $datesDiff,
            'targetings' => $targetingsDiff,
        ];
    }

    /**
     * @param InventoryCheckHistory $succeeded
     * @param InventoryCheckHistory $failed
     *
     * @return array[]
     */
    private function findChangedDates(InventoryCheckHistory $succeeded, InventoryCheckHistory $failed): array
    {
        if ($this->isSameDate($succeeded, $failed)) {
            return [];
        }

        return [
            'succeeded' => [
                'date_start' => $succeeded->date_start,
                'date_end'   => $succeeded->date_end,
            ],
            'failed'    => [
                'date_start' => $failed->date_start,
                'date_end'   => $failed->date_end,
            ],
        ];
    }

    /**
     * @param InventoryCheckHistory $history1
     * @param InventoryCheckHistory $history2
     *
     * @return bool
     */
    private function isSameDate(InventoryCheckHistory $history1, InventoryCheckHistory $history2): bool
    {
        return $history1->date_start->eq($history2->date_start) && $history1->date_end->eq($history2->date_end);
    }

    /**
     * @param Collection|InventoryCheckTargeting[] $succeeded
     * @param Collection|InventoryCheckTargeting[] $failed
     *
     * @return array
     */
    private function findAddedAndRemovedTargetings(Collection $succeeded, Collection $failed): array
    {
        $removedHistory = $this->getTargetingsDiff($succeeded, $failed);
        $addedHistory = $this->getTargetingsDiff($failed, $succeeded);

        return [
            'added'   => $this->getTargetingsFromHistory($addedHistory),
            'removed' => $this->getTargetingsFromHistory($removedHistory),
        ];
    }

    /**
     * @param Collection|InventoryCheckTargeting[] $collection1
     * @param Collection|InventoryCheckTargeting[] $collection2
     *
     * @return Collection
     */
    private function getTargetingsDiff(Collection $collection1, Collection $collection2): Collection
    {
        return $collection1->filter(function (InventoryCheckTargeting $targeting1) use ($collection2): bool {
            return is_null($collection2->first(function (InventoryCheckTargeting $targeting2) use ($targeting1): bool {
                return $this->isSameTargetable($targeting1, $targeting2);
            }));
        });
    }

    /**
     * @param InventoryCheckTargeting $targeting1
     * @param InventoryCheckTargeting $targeting2
     *
     * @return bool
     */
    private function isSameTargetable(InventoryCheckTargeting $targeting1, InventoryCheckTargeting $targeting2): bool
    {
        return $targeting1->targetable_type === $targeting2->targetable_type
            && $targeting1->targetable_id === $targeting2->targetable_id
            && $targeting1->excluded === $targeting2->excluded;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return float
     */
    private function calculateDeliveryIndicatorArrowPosition(InventoryCheckHistory $history): float
    {
        if ($history->available_impressions === 0) {
            return 0;
        }

        $minValue = $this->calculateMinimumImpressionsForCampaign($history);
        $maxValue = $history->ordered_impressions * 4; // approximately

        if ($maxValue > $history->available_impressions) {
            $maxValue = $history->available_impressions;
        }

        // Set arrow between 0% and 10% (red arc)
        $minPercentage = 10; // based on chartColor from InventoryDeliveryIndicator.vue and myself
        if ($minValue > $maxValue) {
            return $this->round($maxValue * $minPercentage / $minValue);
        }

        // Set arrow between 10% and 30% (yellow arc)
        // find arrow position if it's more then minimum but less then ordered
        // ordered_impressions = 10% - because it's part from red to yellow arc (30% - 10%)
        // maxValue percentage = available_impressions * 10% / ordered_impressions
        // and then +10% to start from red arc
        $minOrderedPercentage = 25; // based on chartColor from InventoryDeliveryIndicator.vue and myself
        if ($history->ordered_impressions > $maxValue) {
            return $this->round(($maxValue * ($minOrderedPercentage - $minPercentage) / $history->ordered_impressions)
                + $minPercentage);
        }

        // Set arrow between 30% and 100% (green arc)
        // according to graph in HD-1099 our delivery indicator split in 4 parts with step 25%:
        // 1 part: 0 - 25%
        // 2 part: 25 - 50%
        // 3 part: 50 - 75%
        // 4 part: 75 - 100%
        // so to place arrow in delivery indicator if we have more then 100% available impressions
        // we would use this formula: available_impressions / ordered_impressions * 25
        $arcSize = 25;
        $currentPercent = $this->round($maxValue / $history->ordered_impressions * $arcSize);

        if ($currentPercent > 100) {
            $currentPercent = 100;
        }

        return $currentPercent;
    }

    /**
     * @param float $value
     *
     * @return float
     */
    private function round(float $value): float
    {
        return number_format($value, 2);
    }
}
