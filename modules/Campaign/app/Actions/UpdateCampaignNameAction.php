<?php

namespace Modules\Campaign\Actions;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Log\Logger;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;

class UpdateCampaignNameAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ValidationRules
     */
    protected $validationRules;

    /**
     * @var UpdateCampaignStepAction
     */
    protected $stepAction;

    /**
     * UpdateCampaignNameAction constructor.
     *
     * @param Logger                   $log
     * @param ValidationRules          $validationRules
     * @param UpdateCampaignStepAction $stepAction
     */
    public function __construct(
        Logger $log,
        ValidationRules $validationRules,
        UpdateCampaignStepAction $stepAction
    ) {
        $this->log = $log;
        $this->validationRules = $validationRules;
        $this->stepAction = $stepAction;
    }

    /**
     * Update campaign name.
     *
     * @param Campaign $campaign
     * @param string|null  $name
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     */
    public function handle(Campaign $campaign, ?string $name = ''): Campaign
    {
        $this->log->info('Updating campaign name.', [
            'campaign_id' => $campaign->id,
            'name'        => $name,
        ]);

        if (!is_null($name)) {
            $campaign->name = substr($name, 0, $this->validationRules->get('campaign.details.name.max'));
        }
        $this->stepAction->handle($campaign, CampaignStep::ID_NAME);

        if (!$campaign->save()) {
            $this->log->warning('Campaign name was not updated.', ['campaign_id' => $campaign->id]);
            throw CampaignNotUpdatedException::create();
        }

        $this->log->info('Campaign name was successfully saved.', ['campaign_id' => $campaign->id]);

        return $campaign;
    }
}
