<?php

namespace Modules\Campaign\Actions;

use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Traits\CampaignTargetingCollect;
use Modules\Campaign\Models\Campaign;

class UnattachCampaignNonVisibleTargetingAction
{
    use CampaignTargetingCollect;

    /**
     * Handle action.
     *
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws ValidationException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    public function handle(Campaign $campaign): void
    {
        $toUnAttachArray = $this->collectCampaignTargetings($campaign, false);
        foreach ($toUnAttachArray as $targetingRelationName => $collection) {
            $campaign->{$targetingRelationName}()->detach($collection->pluck('id'));
        }
    }
}
