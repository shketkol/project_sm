<?php

namespace Modules\Campaign\Actions\Alerts;

use Carbon\Carbon;
use Modules\Daapi\Listeners\Notifications\Traits\GetFailedCampaigns;
use Modules\Notification\Actions\SetAlertAction;
use Modules\Notification\Models\AlertType;

class SetFailedPaymentAlertAction extends SetAlertAction
{
    use GetFailedCampaigns;

    /**
     * @param array $ordersStatusPayload
     *
     * @return bool
     */
    public function handle(array $ordersStatusPayload): bool
    {
        $eventTime = Carbon::now()->addWeek();
        $campaigns = $this->getFailedCampaigns($ordersStatusPayload);

        return $this->storeAlertAction->handle(
            $campaigns->first(),
            AlertType::ID_FAILED_PAYMENT,
            $eventTime
        );
    }
}
