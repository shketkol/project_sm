<?php

namespace Modules\Campaign\Actions\Alerts;

use Modules\Campaign\Models\Campaign;

class SetCampaignAlertsAction
{
    /**
     * @var SetCantGoLiveAlertAction
     */
    protected $setCantGoLiveAlertAction;

    /**
     * @var SetCreativeMissingAlertAction
     */
    protected $setCreativeMissingAlertAction;

    /**
     * @param SetCantGoLiveAlertAction $setCantGoLiveAlertAction
     * @param SetCreativeMissingAlertAction $setCreativeMissingAlertAction
     */
    public function __construct(
        SetCantGoLiveAlertAction $setCantGoLiveAlertAction,
        SetCreativeMissingAlertAction $setCreativeMissingAlertAction
    ) {
        $this->setCantGoLiveAlertAction = $setCantGoLiveAlertAction;
        $this->setCreativeMissingAlertAction = $setCreativeMissingAlertAction;
    }

    /**
     * Set all campaign alerts.
     *
     * @param Campaign $campaign
     * @param bool     $force
     * @param bool     $receivedRejected
     *
     * @return bool
     */
    public function handle(Campaign $campaign, bool $force = false, bool $receivedRejected = false): bool
    {
        if (!$campaign->date_start) {
            return true;
        }

        $creative = $campaign->getActiveCreative();

        if (!$force && $creative && !($creative->isRejected() || $receivedRejected)) {
            return true;
        }

        return $this->setCreativeMissingAlertAction->handle($campaign)
            && $this->setCantGoLiveAlertAction->handle($campaign);
    }
}
