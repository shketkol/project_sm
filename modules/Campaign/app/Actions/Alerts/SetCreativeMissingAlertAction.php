<?php

namespace Modules\Campaign\Actions\Alerts;

use Modules\Campaign\Models\Campaign;
use Modules\Notification\Models\AlertType;

class SetCreativeMissingAlertAction extends SetCampaignAlertAction
{
    /**
     * Set campaign 'Creative missing' alert.
     *
     * @param Campaign $campaign
     * @return bool
     */
    public function handle(Campaign $campaign): bool
    {
        return $this->storeAlertAction->handle(
            $campaign,
            AlertType::ID_CREATIVE_MISSING,
            $campaign->start_date_with_timezone
        );
    }
}
