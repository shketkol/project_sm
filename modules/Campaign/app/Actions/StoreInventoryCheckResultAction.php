<?php

namespace Modules\Campaign\Actions;

use App\Exceptions\InvalidArgumentException;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\Traits\Targetings;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Models\InventoryCheckTargeting;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;
use Modules\Campaign\Repositories\InventoryCheckStatusRepository;
use Modules\Targeting\Models\TargetingValue;
use Psr\Log\LoggerInterface;

class StoreInventoryCheckResultAction
{
    use Targetings;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var InventoryCheckHistoryRepository
     */
    private $historyRepository;

    /**
     * @var InventoryCheckStatusRepository
     */
    private $statusRepository;

    /**
     * @param LoggerInterface                 $log
     * @param DatabaseManager                 $databaseManager
     * @param InventoryCheckHistoryRepository $historyRepository
     * @param InventoryCheckStatusRepository  $statusRepository
     */
    public function __construct(
        LoggerInterface $log,
        DatabaseManager $databaseManager,
        InventoryCheckHistoryRepository $historyRepository,
        InventoryCheckStatusRepository $statusRepository
    ) {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
        $this->historyRepository = $historyRepository;
        $this->statusRepository = $statusRepository;
    }

    /**
     * @param Campaign $campaign
     * @param array    $payload
     *
     * @return InventoryCheckHistory
     * @throws InvalidArgumentException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Throwable
     */
    public function handle(Campaign $campaign, array $payload): InventoryCheckHistory
    {
        $this->log->info('[Inventory Check History] Started saving.', [
            'campaign_id' => $campaign->id,
            'payload'     => $payload,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $attributes = $this->prepareAttributes($campaign, $payload);
            $history = $this->historyRepository->create($attributes);
            $this->saveTargetings($history, $campaign);
        } catch (\Throwable $exception) {
            $this->log->info('[Inventory Check History] Failed saving.', [
                'campaign_id' => $campaign->id,
                'payload'     => $payload,
                'reason'      => $exception->getMessage(),
            ]);

            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('[Inventory Check History] Finished saving.', [
            'campaign_id' => $campaign->id,
            'payload'     => $payload,
            'history_id'  => $history->id,
        ]);

        return $history;
    }

    /**
     * @param Campaign $campaign
     * @param array    $payload
     *
     * @return array
     * @throws InvalidArgumentException
     */
    private function prepareAttributes(Campaign $campaign, array $payload): array
    {
        return [
            'campaign_id'           => $campaign->id,
            'date_start'            => $campaign->date_start,
            'date_end'              => $campaign->date_end,
            'ordered_impressions'   => Arr::get($payload, 'quantity'),
            'available_impressions' => Arr::get($payload, 'availableImpressions'),
            'min_daily_impressions' => Arr::get($payload, 'minDailyImpressions'),
            'campaign_budget'       => $campaign->budget,
            'available_budget'      => Arr::get($payload, 'availableBudget'),
            'status_id'             => $this->findStatusId(Arr::get($payload, 'statusCode')),
        ];
    }

    /**
     * @param string $status
     *
     * @return int
     * @throws InvalidArgumentException
     */
    private function findStatusId(string $status): int
    {
        $model = $this->statusRepository->findByField('name', $status)->first();

        if (!is_null($model)) {
            return $model->id;
        }

        throw new InvalidArgumentException(sprintf(
            'Inventory check status "%s" not found.',
            $status
        ));
    }

    /**
     * @param InventoryCheckHistory $history
     * @param Campaign              $campaign
     */
    private function saveTargetings(InventoryCheckHistory $history, Campaign $campaign): void
    {
        $targetings = $this->getTargetingsFromCampaign($campaign);
        $attributes = $targetings->map(function (TargetingValue $targeting) use ($history): array {
            return [
                'history_id'      => $history->id,
                'targetable_type' => get_class($targeting),
                'targetable_id'   => $targeting->id,
                'excluded'        => $targeting->getIsExcluded(),
            ];
        });

        InventoryCheckTargeting::query()->insert($attributes->toArray());
    }
}
