<?php

namespace Modules\Campaign\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Campaign\CampaignImpressionsMetrics;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsMetricsParams;
use Modules\User\Models\User;

class GetCampaignImpressionsMetricsAction
{
    /**
     * @var CampaignImpressionsMetrics
     */
    protected $action;

    /**
     * @param CampaignImpressionsMetrics $action
     */
    public function __construct(CampaignImpressionsMetrics $action)
    {
        $this->action = $action;
    }

    /**
     * @param Campaign             $campaign
     * @param User|Authenticatable $user
     * @param string               $type
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(Campaign $campaign, User $user, string $type): array
    {
        return $campaign->getExternalId() ? $this->getImpressionsMetrics($campaign, $user, $type) : [];
    }

    /**
     * Get all impressions metrics for the campaign by type.
     *
     * @param Campaign $campaign
     * @param User     $user
     * @param string   $type
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    protected function getImpressionsMetrics(Campaign $campaign, User $user, string $type): array
    {
        return $this->action->handle(new ImpressionsMetricsParams([
            'campaignId' => $campaign->getExternalId(),
            'type'       => $type,
        ]), $user->getKey());
    }
}
