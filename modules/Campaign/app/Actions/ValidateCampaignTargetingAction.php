<?php

namespace Modules\Campaign\Actions;

use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Traits\CampaignTargetingCollect;
use Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException;
use Modules\Campaign\Models\Campaign;

class ValidateCampaignTargetingAction
{
    use CampaignTargetingCollect;

    /**
     * Handle action.
     *
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws ValidationException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws CampaignUsesNonVisibleTargetingException
     */
    public function handle(Campaign $campaign): void
    {
        $nonVisible = $this->collectCampaignTargetings($campaign, false);

        if (count($nonVisible)) {
            throw new CampaignUsesNonVisibleTargetingException($campaign, $this->formMessage($nonVisible));
        }
    }

    /**
     * @param array $nonVisible
     * @return string
     */
    protected function formMessage(array $nonVisible): string
    {
        $intro = e(trans('targeting::messages.targetings_no_available'));
        $message = "<p class='message-intro'>$intro</p>";
        foreach ($nonVisible as $targetingGroupName => $targetingCollection) {
            $targeting = e(trans('targeting::labels.labels.'. $targetingGroupName));
            $message .= "<b>$targeting:</b><ul>";
            if ($targetingGroupName === "targetingAgeGroups") {
                $this->invisibleAgeGroupMessage($targetingCollection, $message);
                continue;
            }

            foreach ($targetingCollection as $targeting) {
                $name = e($targeting->name);
                $message .= "<li>$name</li>";
            }
            $message .= '</ul>';
        }

        return $message;
    }

    /**
     * Get invisible age groups full name
     *
     * @param $ageGroups
     * @param string $message
     */
    private function invisibleAgeGroupMessage($ageGroups, string &$message)
    {
        foreach ($ageGroups as $ageGroup) {
            $genderName = e(Str::ucfirst($ageGroup->gender->name));
            $ageGroupName = e($ageGroup->name);
            $message .= "<li>$genderName $ageGroupName</li>";
        }
    }
}
