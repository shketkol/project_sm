<?php

namespace Modules\Campaign\Actions;

use App\Exceptions\BaseException;
use App\Helpers\Math\CalculationHelper;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\Traits\CampaignTargetingCollect;
use Modules\Campaign\Exceptions\CampaignNotCreatedException;
use Modules\Campaign\Http\Controllers\Api\Traits\StoreCampaign;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignGendersAction;

class CloneCampaignAction
{
    use StoreCampaign,
        CampaignTargetingCollect;

    public const NAME_PREFIX = 'Copy - ';

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var UpdateCampaignGendersAction
     */
    protected $actionGender;

    /**
     * @var GetCampaignPriceAction
     */
    protected $getPriceAction;

    /**
     * @param DatabaseManager $databaseManager
     * @param UpdateCampaignGendersAction $actionGender
     * @param GetCampaignPriceAction $getPriceAction
     * @param Logger $log
     */
    public function __construct(
        DatabaseManager $databaseManager,
        UpdateCampaignGendersAction $actionGender,
        GetCampaignPriceAction $getPriceAction,
        Logger $log
    ) {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
        $this->actionGender = $actionGender;
        $this->getPriceAction = $getPriceAction;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws BaseException
     */
    public function handle(Campaign $campaign): Campaign
    {
        $this->log->info('Start clone campaign.', ['origin_campaign_id' => $campaign->id]);

        $this->databaseManager->beginTransaction();

        $data = $this->getCampaignData($campaign);

        try {
            $clonedCampaign = $this->storeCampaign($data, $campaign->step_id);
            $targetingValues = $this->collectAllCampaignTargetings($campaign);

            $this->setTargetingValues($clonedCampaign, $targetingValues);
            if ($clonedCampaign->budget > 0) {
                $this->setPrice($clonedCampaign);
            }
            $this->attachCreative($campaign, $clonedCampaign);
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();

            $this->log->info('Fail clone campaign.', [
                'origin_campaign_id' => $campaign->id,
            ]);

            throw CampaignNotCreatedException::createFrom($throwable);
        }

        $this->databaseManager->commit();

        $this->log->info('Finish clone campaign.', [
            'origin_campaign_id' => $campaign->id,
            'cloned_campaign_id' => $clonedCampaign->id
        ]);

        return $clonedCampaign;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     */
    private function getCampaignData(Campaign $campaign): array
    {
        $attributesForClone = [
            'budget',
            'cpm',
            'date_start',
            'date_end',
            'name',
            'timezone_id'
        ];

        $tomorrow = Carbon::tomorrow(config('campaign.wizard.date.timezone'))
            ->format(config('date.format.db_date_time'));

        if ($campaign->date_start && $campaign->date_end && $campaign->date_start < $tomorrow) {
            $campaign = $this->createNewDates($campaign);
        }

        $attributes = $campaign->getAttributes();
        $attributes = Arr::only($attributes, $attributesForClone);
        Arr::set($attributes, 'name', self::NAME_PREFIX . Arr::get($attributes, 'name'));

        return $attributes;
    }

    /**
     * @param Campaign $campaign
     * @param array $targetingValues
     *
     * @return void
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function setTargetingValues(Campaign $campaign, array $targetingValues): void
    {
        foreach ($targetingValues as $key => $targetingValue) {
            if (in_array($key, CampaignStep::STEPS_RELATIONS[CampaignStep::ID_AGES])) {
                $this->setGender($campaign, $targetingValue);
            }

            $data = Arr::pluck($targetingValue->toArray(), 'pivot');
            $data = array_map(function ($item) {
                return Arr::except($item, 'campaign_id');
            }, $data);

            $campaign->$key()->attach($data);
        }
    }

    /**
     * @param Campaign $campaign
     * @param Collection $agesGroups
     *
     * @return void
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function setGender(Campaign $campaign, Collection $agesGroups): void
    {
        $genderId = $agesGroups->first()->gender_id;
        $this->actionGender->handle($campaign, $genderId);
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    private function setPrice(Campaign $campaign): void
    {
        $cpm = Arr::get($this->getPriceAction->handle($campaign), 'unitCost');
        $campaign->cpm = $cpm;
        $campaign->impressions = CalculationHelper::calculateImpressions($cpm, $campaign->budget);
        $campaign->save();
    }

    /**
     * @param Campaign $originCampaign
     * @param Campaign $clonedCampaign
     *
     * @return void
     */
    private function attachCreative(Campaign $originCampaign, Campaign $clonedCampaign): void
    {
        $creatives = $originCampaign->creatives;
        if ($creatives) {
            $clonedCampaign->creatives()->attach($creatives);
        }
    }
}
