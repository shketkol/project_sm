<?php

namespace Modules\Campaign\Actions;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Traits\CampaignBudgetValidation;
use Modules\Campaign\Actions\Traits\CampaignMinImpressionValidation;
use Modules\Campaign\Actions\Traits\CampaignScheduleValidation;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignInventoryCheck;
use Modules\Haapi\DataTransferObjects\Campaign\InventoryCheckParams;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Targeting\Actions\GetCampaignTargetingsAction;
use Modules\Targeting\Exceptions\TypeNotFoundException;

class GetDynamicInventoryCheckAction
{
    use CampaignScheduleValidation,
        CampaignBudgetValidation,
        CampaignMinImpressionValidation;

    /** Constants to use in HAAPI Inventory Check */
    private const PRODUCT_ID_KEY = 'daapi.campaign.productId';
    private const QUANTITY_TYPE = 'IMPRESSIONS';

    /**
     * Validation fields mapper
     */
    private const VALIDATION_MAP = [
        'schedule' => [
            'date',
        ],
        'budget' => [
            'cost',
            'impressions',
        ],
    ];

    /**
     * @var CampaignInventoryCheck
     */
    protected $campaignInventoryCheck;

    /**
     * @var GetCampaignTargetingsAction
     */
    protected $getCampaignTargetings;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ValidationRules
     */
    protected $validationRules;

    /**
     * @var ValidateCampaignTargetingAction
     */
    protected $validateTargetingAction;

    /**
     * @var StoreInventoryCheckResultAction
     */
    protected $storeInventoryCheck;

    /**
     * @var GetInventoryCheckConclusionAction
     */
    protected $conclusion;

    /**
     * @var GetInventoryCheckConclusionErrorAction
     */
    protected $conclusionError;

    /**
     * @var UpdateCampaignBudgetAction
     */
    protected $updateBudget;

    /**
     * @param CampaignInventoryCheck                 $campaignInventoryCheck
     * @param Logger                                 $log
     * @param GetCampaignTargetingsAction            $getCampaignTargetings
     * @param ValidationRules                        $validationRules
     * @param ValidateCampaignTargetingAction        $validateTargetingAction
     * @param StoreInventoryCheckResultAction        $storeInventoryCheck
     * @param GetInventoryCheckConclusionAction      $conclusion
     * @param GetInventoryCheckConclusionErrorAction $conclusionError
     * @param UpdateCampaignBudgetAction             $updateBudget
     */
    public function __construct(
        CampaignInventoryCheck $campaignInventoryCheck,
        Logger $log,
        GetCampaignTargetingsAction $getCampaignTargetings,
        ValidationRules $validationRules,
        ValidateCampaignTargetingAction $validateTargetingAction,
        StoreInventoryCheckResultAction $storeInventoryCheck,
        GetInventoryCheckConclusionAction $conclusion,
        GetInventoryCheckConclusionErrorAction $conclusionError,
        UpdateCampaignBudgetAction $updateBudget
    ) {
        $this->campaignInventoryCheck = $campaignInventoryCheck;
        $this->log = $log;
        $this->getCampaignTargetings = $getCampaignTargetings;
        $this->validationRules = $validationRules;
        $this->validateTargetingAction = $validateTargetingAction;
        $this->storeInventoryCheck = $storeInventoryCheck;
        $this->conclusion = $conclusion;
        $this->conclusionError = $conclusionError;
        $this->updateBudget = $updateBudget;
    }

    /**
     * Get expected delivery result.
     *
     * @param Campaign $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     * @throws ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Throwable
     */
    public function handle(Campaign $campaign): array
    {
        $this->log->info('Delivery indicator used.', ['campaign_id' => $campaign->id]);

        try {
            $errors = $this->validateCampaign($campaign);
            if ($errors) {
                return $this->conclusionError->handle($errors);
            }

            $response = $this->performInventoryCheck($campaign);
            $history = $this->storeInventoryCheck->handle($campaign, $response->getPayload());

            $this->log->info('Delivery indicator result.', [
                'campaign_id' => $campaign->id,
                'budget'      => $response->get('availableBudget'),
                'impressions' => $response->get('availableImpressions'),
                'floorBudget' => $response->get('systemFloorBudget'),
                'history_id'  => $history->id,
            ]);

            return $this->conclusion->handle($history);
        } catch (InternalErrorException $exception) {
            $this->log->warning('Fail inventory check', [
                'campaign_id' => $campaign->id,
                'reason'      => $exception->getMessage(),
            ]);
            return $this->internalError();
        }
    }

    /**
     * @param Campaign $campaign
     *
     * @return mixed
     * @throws ValidationException
     * @throws \Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    protected function validateCampaign(Campaign $campaign)
    {
        $validator = Validator::make(
            $this->getCampaignDataToValidate($campaign),
            array_merge(
                $this->getScheduleValidation($campaign),
                $this->getMinImpressionValidation(),
                $this->getBudgetValidation(),
            ),
            array_merge(
                $this->getScheduleValidationMessages(),
                $this->getBudgetValidationMessages(),
            )
        );

        if ($validator->fails()) {
            return $this->formatValidationData($validator->errors()->toArray());
        }

        $this->validateTargetingAction->handle($campaign);
    }

    /**
     * @param array $errors
     * @return array
     */
    protected function formatValidationData(array $errors): array
    {
        $result = [];
        foreach ($errors as $field => $error) {
            $parts = explode('.', $field);
            $shortKey = count($parts) ? Arr::first($parts) : $field;
            foreach (self::VALIDATION_MAP as $resultField => $sourceFieldArray) {
                if (in_array($shortKey, $sourceFieldArray)) {
                    Arr::set($result, $resultField, Arr::first($error));
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     */
    protected function getCampaignDataToValidate(Campaign $campaign): array
    {
        $dateFormat = config('date.format.php');
        return [
            'timezone_id' => $campaign->timezone_id,
            'date'        => [
                'start' => $campaign->date_start ? $campaign->date_start->format($dateFormat) : null,
                'end'   => $campaign->date_end ? $campaign->date_end->format($dateFormat) : null,
            ],
            'cost'        => $campaign->budget,
            'impressions' => [
                'min_per_day' => [
                    'duration'  => $campaign->getDuration(),
                    'minPerDay' => $campaign->min_impressions,
                    'cpm'       => $campaign->cpm,
                    'cost'      => $campaign->budget
                ]
            ]
        ];
    }

    /**
     * @param Campaign $campaign
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws TypeNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function performInventoryCheck(Campaign $campaign): HaapiResponse
    {
        $userId = Auth::user()->getAuthIdentifier();

        $inventoryCheckParams = new InventoryCheckParams([
            'campaignSourceId' => $campaign->id,
            'startDate'        => $campaign->startDateWithTimezone->format(config('date.format.haapi')),
            'endDate'          => $campaign->endDateWithTimezone->format(config('date.format.haapi')),
            'targets'          => $this->getCampaignTargetings->handle($campaign),
            'productId'        => config(self::PRODUCT_ID_KEY),
            'quantity'         => $campaign->impressions,
            'quantityType'     => self::QUANTITY_TYPE,
        ]);

        return $this->campaignInventoryCheck->handle($inventoryCheckParams, $userId);
    }

    /**
     * @return array
     */
    private function internalError(): array
    {
        return [
            'title'   => __('campaign::labels.modal_inventory_fail.something_went_wrong'),
            'message' => trans('campaign::messages.inventory_check.fail.error'),
            'type'    => GetCampaignInventoryCheckAction::TYPE_INTERNAL_ERROR,
            'payload' => [],
        ];
    }
}
