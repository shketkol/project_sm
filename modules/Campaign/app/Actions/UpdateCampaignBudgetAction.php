<?php

namespace Modules\Campaign\Actions;

use App\Helpers\Math\CalculationHelper;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;

class UpdateCampaignBudgetAction
{
    /**
     * Field limitations. It is used to store data in database even if it has invalid values.
     *
     * @var array
     */
    protected $fieldLimitations = [
        'cpm'        => [
            'min' => 0,
            'max' => 1000,
        ],
        'cost'        => [
            'min' => 0,
            'max' => 999999.99,
        ],
        'impressions' => [
            'min' => 0,
            'max' => 99999999999,
        ],
        'min_impressions' => [
            'min' => 0,
            'max' => 99999999999,
        ],
    ];

    /**
     * @var GetCampaignPriceAction
     */
    protected $getCampaignPriceAction;

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var UpdateCampaignStepAction
     */
    private $stepAction;

    /**
     * UpdateCampaignBudgetAction constructor.
     *
     * @param \Modules\Campaign\Actions\GetCampaignPriceAction $getCampaignPriceAction
     * @param Logger                                           $log
     * @param UpdateCampaignStepAction                         $stepAction
     */
    public function __construct(
        GetCampaignPriceAction $getCampaignPriceAction,
        Logger $log,
        UpdateCampaignStepAction $stepAction
    ) {
        $this->getCampaignPriceAction = $getCampaignPriceAction;
        $this->log = $log;
        $this->stepAction = $stepAction;
    }

    /**
     * Update campaign budget.
     *
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function handle(Campaign $campaign, array $data = []): Campaign
    {
        $this->log->info('Updating campaign budget.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        $budgetData = $this->getBudgetData($campaign, $data);
        $campaign->fill($budgetData);

        if (count($data)) {
            $this->stepAction->handle($campaign, CampaignStep::ID_BUDGET);
        }

        if (!$campaign->save()) {
            $this->log->warning('Campaign budget was not updated.', ['campaign_id' => $campaign->id]);
            throw CampaignNotUpdatedException::create();
        }

        $this->log->info('Campaign budget was successfully updated.', ['campaign_id' => $campaign->id]);

        return $campaign->refresh();
    }

    /**
     * Get budget data (all attributes to fill the model).
     *
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return array
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    protected function getBudgetData(Campaign $campaign, array $data = []): array
    {

        $response = $this->getCampaignPriceAction->handle($campaign);
        $cpm = Arr::get($response, 'unitCost');
        $minImpressions = Arr::get($response, 'minDailyImpressions');
        (float)$cost = Arr::get($data, 'cost') !== null ? Arr::get($data, 'cost') : $campaign->budget;

        return [
            'cpm'         => $this->filterBudgetData('cpm', $cpm),
            'budget'      => $this->filterBudgetData('cost', $cost),
            'cost'        => $this->filterBudgetData('cost', $cost),
            'min_impressions' => $this->filterBudgetData('min_impressions', $minImpressions),
            'impressions' => $this->filterBudgetData(
                'impressions',
                CalculationHelper::calculateImpressions($cpm, $cost)
            ),
        ];
    }

    /**
     * Filter data to allow saving in database.
     *
     * @param string $field
     * @param mixed  $value
     *
     * @return mixed
     */
    protected function filterBudgetData(string $field, $value)
    {
        $limitations = Arr::get($this->fieldLimitations, $field);

        // Set min value
        $min = Arr::get($limitations, 'min');
        $value = max($value, $min);

        // Set max value
        $max = Arr::get($limitations, 'max');
        $value = min($value, $max);

        return $value;
    }
}
