<?php

namespace Modules\Campaign\Actions;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Traits\CampaignScheduleValidation;
use Modules\Campaign\Actions\Traits\CampaignTargetingValidation;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignInventoryCheck;
use Modules\Haapi\DataTransferObjects\Campaign\InventoryCheckParams;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Targeting\Actions\GetCampaignTargetingsAction;
use Modules\Targeting\Exceptions\TypeNotFoundException;

class GetCampaignInventoryCheckAction
{
    use CampaignScheduleValidation,
        CampaignTargetingValidation;

    /** Types */
    public const TYPE_SUCCESS = 'success';
    public const TYPE_WARNING = 'warning';
    public const TYPE_DANGER_BUDGET = 'dangerBudget';
    public const TYPE_DANGER_SCHEDULE = 'dangerSchedule';
    public const TYPE_INTERNAL_ERROR = 'fail';

    /** Constants to use in HAAPI Inventory Check */
    private const PRODUCT_ID_KEY = 'daapi.campaign.productId';
    private const QUANTITY_TYPE = 'IMPRESSIONS';

    /** Constants to use for identifier inventory check flow */
    public const TYPE_FLOW_SUCCESS = 'success';
    public const TYPE_FLOW_WARNING = 'warning';
    public const TYPE_FLOW_DANGER = 'danger';

    /** Constant to use for showing message */
    private const FLOW_TYPES = [
        self::TYPE_FLOW_SUCCESS => [self::TYPE_SUCCESS],
        self::TYPE_FLOW_DANGER  => [
            self::TYPE_DANGER_BUDGET,
            self::TYPE_DANGER_SCHEDULE,
            self::TYPE_INTERNAL_ERROR,
        ],
        self::TYPE_FLOW_WARNING => [
            self::TYPE_WARNING,
        ],
    ];

    /**
     * @var CampaignInventoryCheck
     */
    protected $campaignInventoryCheck;

    /**
     * @var GetCampaignTargetingsAction
     */
    protected $getCampaignTargetings;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ValidationRules
     */
    protected $validationRules;

    /**
     * @var ValidateCampaignTargetingAction
     */
    protected $validateTargetingAction;

    /**
     * @var StoreInventoryCheckResultAction
     */
    protected $storeInventoryCheck;

    /**
     * @param CampaignInventoryCheck          $campaignInventoryCheck
     * @param Logger                          $log
     * @param GetCampaignTargetingsAction     $getCampaignTargetings
     * @param ValidationRules                 $validationRules
     * @param ValidateCampaignTargetingAction $validateTargetingAction
     * @param StoreInventoryCheckResultAction $storeInventoryCheck
     */
    public function __construct(
        CampaignInventoryCheck $campaignInventoryCheck,
        Logger $log,
        GetCampaignTargetingsAction $getCampaignTargetings,
        ValidationRules $validationRules,
        ValidateCampaignTargetingAction $validateTargetingAction,
        StoreInventoryCheckResultAction $storeInventoryCheck
    ) {
        $this->campaignInventoryCheck = $campaignInventoryCheck;
        $this->log = $log;
        $this->getCampaignTargetings = $getCampaignTargetings;
        $this->validationRules = $validationRules;
        $this->validateTargetingAction = $validateTargetingAction;
        $this->storeInventoryCheck = $storeInventoryCheck;
    }

    /**
     * Get expected delivery result.
     *
     * @param Campaign $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     * @throws ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Throwable
     */
    public function handle(Campaign $campaign): array
    {
        $this->log->info('Delivery indicator used.', ['campaign_id' => $campaign->id]);

        try {
            $this->validateCampaign($campaign);
            $response = $this->performInventoryCheck($campaign);
            $history = $this->storeInventoryCheck->handle($campaign, $response->getPayload());
            $type = $this->campaignInventoryCheck->getType();

            $this->log->info('Delivery indicator result.', [
                'campaign_id' => $campaign->id,
                'type'        => $type,
                'budget'      => $response->get('availableBudget'),
                'impressions' => $response->get('availableImpressions'),
                'floorBudget' => $response->get('systemFloorBudget'),
                'history_id'  => $history->id,
            ]);

            return [
                'title'       => $this->getTitle($type),
                'message'     => $this->getMessage($type, $response),
                'messageType' => $this->getTypeFow($type),
                'payload'     => $this->getPayload($type, $response),
                'type'        => $type,
                'typeFlow'    => $this->getTypeFow($type),
            ];
        } catch (InternalErrorException $exception) {
            $this->log->warning('Fail inventory check', [
                'campaign_id' => $campaign->id,
                'reason'      => $exception->getMessage(),
            ]);
            return $this->internalError();
        }
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws ValidationException
     * @throws \Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    protected function validateCampaign(Campaign $campaign): void
    {
        $validator = Validator::make(
            $this->getCampaignDataToValidate($campaign),
            array_merge(
                $this->getScheduleValidation($campaign),
                $this->getAgeGroupsValidation(),
                $this->getAudiencesValidation(),
                $this->getZipcodesValidation(),
                $this->getGenresValidation(),
                $this->getDeviceGroupsValidation()
            ),
            array_merge(
                $this->getScheduleValidationMessages(),
                $this->getAgeGroupsValidationMessages(),
                $this->getDeviceGroupsValidationMessages()
            )
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->validateTargetingAction->handle($campaign);
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     */
    protected function getCampaignDataToValidate(Campaign $campaign): array
    {
        $dateFormat = config('date.format.php');
        return [
            'timezone_id'   => $campaign->timezone_id,
            'date'          => [
                'start' => $campaign->date_start ? $campaign->date_start->format($dateFormat) : null,
                'end'   => $campaign->date_end ? $campaign->date_end->format($dateFormat) : null,
            ],
            'age_groups'    => $campaign->targetingAgeGroups->pluck('id')->toArray(),
            'audiences'     => $campaign->audiences->pluck('id')->toArray(),
            'zipcodes'      => $campaign->zipcodes->pluck('id')->toArray(),
            'genres'        => $campaign->genres->pluck('id')->toArray(),
            'device_groups' => $campaign->deviceGroups->pluck('id')->toArray(),
        ];
    }

    /**
     * @param Campaign $campaign
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws TypeNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function performInventoryCheck(Campaign $campaign): HaapiResponse
    {
        $userId = Auth::user()->getAuthIdentifier();

        $inventoryCheckParams = new InventoryCheckParams([
            'campaignSourceId' => $campaign->id,
            'startDate'        => $campaign->startDateWithTimezone->format(config('date.format.haapi')),
            'endDate'          => $campaign->endDateWithTimezone->format(config('date.format.haapi')),
            'targets'          => $this->getCampaignTargetings->handle($campaign),
            'productId'        => config(self::PRODUCT_ID_KEY),
            'quantity'         => $campaign->impressions,
            'quantityType'     => self::QUANTITY_TYPE,
        ]);

        return $this->campaignInventoryCheck->handle($inventoryCheckParams, $userId);
    }

    /**
     * Get payload data.
     *
     * @param string        $type
     * @param HaapiResponse $response
     *
     * @return array
     */
    protected function getPayload(string $type, HaapiResponse $response): array
    {
        $data = [
            'budget'      => $response->get('availableBudget'),
            'impressions' => $response->get('availableImpressions'),
            'floorBudget' => $response->get('systemFloorBudget'),
        ];

        if ($type === self::TYPE_DANGER_SCHEDULE) {
            $data['minDailyImpressions'] = $response->get('minDailyImpressions');
        }

        return $data;
    }

    /**
     * Get message for the result type.
     *
     * @param string        $type
     * @param HaapiResponse $response
     *
     * @return string
     */
    protected function getMessage(string $type, HaapiResponse $response): string
    {
        $snakeType = Str::snake($type);

        switch ($type) {
            case self::TYPE_DANGER_SCHEDULE:
                return __('campaign::messages.inventory_check.result.danger_schedule', [
                    'impressions' => $response->get('minDailyImpressions'),
                ]);
            default:
                return __("campaign::messages.inventory_check.result.{$snakeType}");
        }
    }

    /**
     * @param string $type
     *
     * @return string
     */
    protected function getTitle(string $type): string
    {
        $typeMap = [
            self::TYPE_WARNING         => __('campaign::labels.modal_inventory_info.inventory_high_demand'),
            self::TYPE_DANGER_BUDGET   => __('campaign::labels.modal_inventory_info.inventory_high_demand'),
            self::TYPE_DANGER_SCHEDULE => __('campaign::labels.modal_inventory_info.daily_impressions_low'),
        ];

        return Arr::get($typeMap, $type, '');
    }

    /**
     * @param string $type
     *
     * @return string
     */
    public function getTypeFow(string $type): string
    {
        $type = Arr::where(self::FLOW_TYPES, function (array $item) use ($type): bool {
            return in_array($type, $item);
        });

        return Arr::first(array_keys($type));
    }

    /**
     * @return array
     */
    private function internalError(): array
    {
        return [
            'title'       => __('campaign::labels.modal_inventory_fail.something_went_wrong'),
            'message'     => trans('campaign::messages.inventory_check.fail.error'),
            'messageType' => $this->getTypeFow(GetCampaignInventoryCheckAction::TYPE_INTERNAL_ERROR),
            'type'        => GetCampaignInventoryCheckAction::TYPE_INTERNAL_ERROR,
            'typeFlow'    => $this->getTypeFow(GetCampaignInventoryCheckAction::TYPE_INTERNAL_ERROR),
            'payload'     => [],
        ];
    }
}
