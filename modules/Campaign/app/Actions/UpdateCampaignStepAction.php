<?php

namespace Modules\Campaign\Actions;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Log\Logger;
use Modules\Campaign\Actions\Traits\SavesStep;
use Modules\Campaign\Events\CampaignStepUpdated;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;

class UpdateCampaignStepAction
{
    use SavesStep;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * UpdateCampaignStepAction constructor.
     *
     * @param Logger     $log
     * @param Dispatcher $dispatcher
     */
    public function __construct(Logger $log, Dispatcher $dispatcher)
    {
        $this->log = $log;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Handle action.
     *
     * @param Campaign $campaign
     * @param int      $stepId
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     */
    public function handle(Campaign $campaign, int $stepId): Campaign
    {
        $this->log->info('Updating campaign step.', [
            'campaign_id' => $campaign->id,
            'step_id'     => $stepId,
        ]);

        $this->saveStep($campaign, $stepId);
        $this->dispatcher->dispatch(new CampaignStepUpdated($campaign));

        $this->log->info('Campaign step was successfully updated.', ['campaign_id' => $campaign->id]);

        return $campaign;
    }
}
