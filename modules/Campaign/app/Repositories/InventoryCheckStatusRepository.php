<?php

namespace Modules\Campaign\Repositories;

use App\Repositories\Repository;
use Modules\Campaign\Models\InventoryCheckStatus;

class InventoryCheckStatusRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return InventoryCheckStatus::class;
    }
}
