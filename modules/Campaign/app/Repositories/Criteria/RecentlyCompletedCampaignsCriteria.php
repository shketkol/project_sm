<?php

namespace Modules\Campaign\Repositories\Criteria;

use App\Exceptions\InvalidArgumentException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Models\CampaignStatus;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class RecentlyCompletedCampaignsCriteria implements CriteriaInterface
{
    /**
     * @var string[]
     */
    private const INTERVAL_TYPES = ['HOUR', 'DAY', 'WEEK', 'MONTH'];

    /**
     * @var int
     */
    private $intervalValue;

    /**
     * @var string
     */
    private $intervalType;

    /**
     * @param int    $intervalValue
     * @param string $intervalType self::INTERVAL_TYPES
     *
     * @throws InvalidArgumentException
     */
    public function __construct(int $intervalValue, string $intervalType)
    {
        $this->intervalValue = $intervalValue;

        if (!in_array($intervalType, self::INTERVAL_TYPES, true)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid interval type was given: "%s". Available types: "%s".',
                $intervalType,
                implode(', ', self::INTERVAL_TYPES)
            ));
        }

        $this->intervalType = $intervalType;
    }

    /**
     * @param Model|Builder       $model
     * @param RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        if ($this->intervalType !== 'MONTH') {
            return $model->where('campaigns.status_id', '=', CampaignStatus::ID_COMPLETED)
                ->whereRaw(sprintf(
                    'DATE_SUB(SUBDATE(NOW(),1), INTERVAL ? %s) < campaigns.date_end',
                    $this->intervalType
                ), [$this->intervalValue]);
        }

        // for month we must take not just some previous date but 1 month from 2 months ago
        // for example if today is any day of October we should take from 1 of August to 31 of August
        return $model->where('campaigns.status_id', '=', CampaignStatus::ID_COMPLETED)
            // take first day of the month that was 2 month ago
            ->whereRaw(sprintf(
                'LAST_DAY(NOW() - INTERVAL ? + 1 %s) + INTERVAL 1 DAY <= campaigns.date_end',
                $this->intervalType
            ), [$this->intervalValue])
            // take last day of the month that was 2 month ago
            ->whereRaw(sprintf('LAST_DAY(NOW() - INTERVAL ? %s) >= campaigns.date_end', $this->intervalType), [
                $this->intervalValue,
            ]);
    }
}
