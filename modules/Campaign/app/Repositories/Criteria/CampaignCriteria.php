<?php

namespace Modules\Campaign\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class CampaignCriteria implements CriteriaInterface
{
    /**
     * @var int
     */
    private $campaignId;

    /**
     * @param int $campaignId
     */
    public function __construct(int $campaignId)
    {
        $this->campaignId = $campaignId;
    }

    /**
     * @param Builder             $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where('campaign_id', '=', $this->campaignId);
    }
}
