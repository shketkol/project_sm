<?php

namespace Modules\Campaign\Repositories;

use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Models\InventoryCheckStatus;
use Modules\Campaign\Repositories\Criteria\CampaignCriteria;
use Modules\Campaign\Repositories\Criteria\InventoryCheckNotStatusCriteria;

class InventoryCheckHistoryRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return InventoryCheckHistory::class;
    }

    /**
     * @param Campaign $campaign
     *
     * @return InventoryCheckHistory|null
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findLast(Campaign $campaign): ?InventoryCheckHistory
    {
        return $this->pushCriteria(new CampaignCriteria($campaign->id))
            ->orderBy('id', 'desc')
            ->limit(1)
            ->get()
            ->first();
    }

    /**
     * @param Campaign $campaign
     *
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function countFailed(Campaign $campaign): int
    {
        return $this
            ->pushCriteria(new CampaignCriteria($campaign->id))
            ->pushCriteria(new InventoryCheckNotStatusCriteria([InventoryCheckStatus::ID_AVAILABLE]))
            ->count();
    }

    /**
     * @param InventoryCheckHistory $history
     * @param int|null              $latestSucceededId
     *
     * @return int|null
     */
    private function findFirstFailedHistoryId(InventoryCheckHistory $history, ?int $latestSucceededId): ?int
    {
        $query = DB::table($history->getTable())
            ->select(DB::raw('MIN(id) as id'))
            ->where('available_impressions', '<=', $history->getDrasticLackValue())
            ->where('status_id', '!=', InventoryCheckStatus::ID_AVAILABLE)
            ->where('campaign_id', '=', $history->campaign_id)
            ->where('id', '<=', $history->id);

        if (!is_null($latestSucceededId)) {
            $query = $query->where('id', '>', $latestSucceededId);
        }

        return $query->get()->first()->id;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return InventoryCheckHistory
     */
    public function findFirstDrasticFailedHistory(InventoryCheckHistory $history): InventoryCheckHistory
    {
        $latestSucceededId = $this->findLastSucceededHistoryId($history);
        $firstFailedId = $this->findFirstFailedHistoryId($history, $latestSucceededId);

        return $this->findById($firstFailedId);
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return int|null
     */
    public function findLastSucceededHistoryId(InventoryCheckHistory $history): ?int
    {
        return DB::table($history->getTable())
            ->select(DB::raw('MAX(id) as id'))
            ->where('status_id', '=', InventoryCheckStatus::ID_AVAILABLE)
            ->where('id', '<=', $history->id)
            ->where('campaign_id', '=', $history->campaign_id)
            ->get()
            ->first()
            ->id;
    }

    /**
     * @param int $id
     *
     * @return InventoryCheckHistory
     */
    public function findById(int $id): InventoryCheckHistory
    {
        return InventoryCheckHistory::query()->find($id);
    }
}
