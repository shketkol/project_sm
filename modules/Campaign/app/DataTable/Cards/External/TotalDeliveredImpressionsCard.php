<?php

namespace Modules\Campaign\DataTable\Cards\External;

use App\DataTable\Cards\Card;
use App\DataTable\Requests\BaseRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Response;
use Modules\Haapi\Actions\Report\TotalImpressionsGet;
use Modules\User\Repositories\UserRepository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TotalDeliveredImpressionsCard extends Card
{
    /**
     * @var string
     */
    public static $name = 'lifetime-delivered-impressions';

    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'number';

    /**
     * @var TotalImpressionsGet
     */
    protected $action;

    /**
     * TotalDeliveredImpressionsCard constructor.
     *
     * @param TotalImpressionsGet  $action
     * @param BaseRequest          $request
     * @param UserRepository       $userRepository
     * @param Authenticatable|null $user
     */
    public function __construct(
        TotalImpressionsGet $action,
        BaseRequest $request,
        UserRepository $userRepository,
        ?Authenticatable $user = null
    ) {
        $this->setUser($user);
        $this->setAdvertiser($user);

        if ($request->user_id) {
            $this->setAdvertiser($userRepository->find((int)$request->user_id));
        }

        $this->title = trans('campaign::labels.campaign.cards.total_delivered_impressions');
        $this->action = $action;
    }

    /**
     * Collect data
     *
     * @return float
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function collect(): float
    {
        if (!$this->getUser()->can('campaign.list') || $this->getUser()->isDeactivated()) {
            throw new AccessDeniedHttpException(Response::$statusTexts[Response::HTTP_FORBIDDEN]);
        }

        return $this->action->handle($this->getAdvertiser()->getExternalId(), $this->getUser()->id);
    }
}
