<?php

namespace Modules\Campaign\DataTable\Cards\External;

use App\DataTable\Cards\Card;
use App\DataTable\Requests\BaseRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Response;
use Modules\Haapi\Actions\Report\AdminTotalImpressionsGet;
use Modules\Haapi\Actions\Report\TotalImpressionsGet;
use Modules\User\Repositories\UserRepository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AdminTotalDeliveredImpressionsCard extends Card
{
    /**
     * @var string
     */
    public static $name = 'admin-lifetime-delivered-impressions';

    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'number';

    /**
     * @var TotalImpressionsGet
     */
    protected $action;

    /**
     * TotalDeliveredImpressionsCard constructor.
     *
     * @param AdminTotalImpressionsGet  $action
     * @param BaseRequest          $request
     * @param UserRepository       $userRepository
     * @param Authenticatable|null $user
     */
    public function __construct(
        AdminTotalImpressionsGet $action,
        BaseRequest $request,
        UserRepository $userRepository,
        ?Authenticatable $user = null
    ) {
        $this->setUser($user);
        $this->setAdvertiser($user);

        if ($request->user_id) {
            $this->setAdvertiser($userRepository->find((int)$request->user_id));
        }

        if (!$this->getUser()->can('campaign.list')) {
            throw new AccessDeniedHttpException(Response::$statusTexts[Response::HTTP_FORBIDDEN]);
        }

        $this->title = trans('campaign::labels.campaign.cards.total_delivered_impressions');
        $this->action = $action;
    }

    /**
     * Collect data
     *
     * @return float
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function collect(): float
    {
        return $this->action->handle($this->getAdvertiser()->getExternalId(), $this->getUser()->id);
    }
}
