<?php

namespace Modules\Campaign\DataTable\Cards;

use Modules\Campaign\DataTable\Repositories\Criteria\ActiveCampaignsCriteria;

class TotalActiveCampaignsCard extends ActiveCampaignsCard
{
    /**
     * Collect data
     *
     * @return float
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new ActiveCampaignsCriteria())
            ->campaigns()
            ->count('campaigns.id');
    }
}
