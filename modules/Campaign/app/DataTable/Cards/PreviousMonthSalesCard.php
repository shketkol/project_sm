<?php

namespace Modules\Campaign\DataTable\Cards;

use App\DataTable\Cards\Card;
use Carbon\Carbon;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository;
use Modules\Campaign\DataTable\Repositories\Criteria\PreviousMonthCriteria;

class PreviousMonthSalesCard extends Card
{
    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'number';

    /**
     * @var \Modules\Campaign\DataTable\Repositories\CampaignsDataTableRepository
     */
    private $repository;

    /**
     * PreviousMonthSalesCard constructor.
     *
     * @param CampaignsDataTableRepository $repository
     * @throws \Exception
     */
    public function __construct(CampaignsDataTableRepository $repository)
    {
        $previousMonth = new Carbon('first day of last month');
        $this->title = __('months.' . strtolower($previousMonth->format('F'))) . ' ' . __('campaign::labels.sales');
        $this->repository = $repository;
    }

    /**
     * Collect data
     *
     * @return float
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new PreviousMonthCriteria())
            ->sum('budget');
    }
}
