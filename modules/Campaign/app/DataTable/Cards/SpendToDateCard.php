<?php

namespace Modules\Campaign\DataTable\Cards;

use App\DataTable\Cards\Card;
use App\DataTable\Requests\BaseRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository;
use Modules\Haapi\Actions\Campaign\CampaignImpressionsDetail;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsDetailParams;
use Modules\User\Repositories\UserRepository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SpendToDateCard extends Card
{
    /**
     * @var string
     */
    public static $name = 'spend-to-date';

    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'currency';

    /**
     * @var CampaignsDataTableRepository
     */
    private $repository;

    /**
     * @var CampaignImpressionsDetail
     */
    private $action;

    /**
     * @param CampaignsDataTableRepository $repository
     * @param BaseRequest                  $request
     * @param UserRepository               $userRepository
     * @param CampaignImpressionsDetail    $action
     * @param Authenticatable|null         $user
     */
    public function __construct(
        CampaignsDataTableRepository $repository,
        BaseRequest $request,
        UserRepository $userRepository,
        CampaignImpressionsDetail $action,
        ?Authenticatable $user = null
    ) {
        $this->setUser($user);
        $this->setAdvertiser($user);
        if ($request->user_id) {
            $advertiser = $userRepository->find((int)$request->user_id);
            if (!$this->getUser()->can('advertiser.view', $advertiser)) {
                throw new AccessDeniedHttpException(Response::$statusTexts[Response::HTTP_FORBIDDEN]);
            }

            $this->setAdvertiser($advertiser);
        }

        $this->title = trans('campaign::labels.campaign.cards.spend_to_date');
        $this->repository = $repository;
        $this->action = $action;
    }

    /**
     * Collect data
     *
     * @return float
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Daapi\Exceptions\AdminNotFoundException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function collect(): float
    {
        $data = $this->action->handle(new ImpressionsDetailParams([
            'advertiserId' => $this->getAdvertiser()->getExternalId(),
            'campaignIds'  => [],
            'rollUpType'   => ImpressionsDetailParams::ROLL_UP_LIFETIME,
        ]), $this->getUser()->id);

        $payload = Arr::first($data);

        return array_sum(Arr::pluck($payload, 'cost'));
    }
}
