<?php

namespace Modules\Campaign\DataTable\Transformers;

use App\DataTable\Transformers\DataTableTransformer;
use App\Helpers\CurrentUser;
use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Actions\GetCampaignEditRulesAction;

class CampaignsTransformer extends DataTableTransformer
{
    use CurrentUser;

    /**
     * Mapping fields.
     *
     * @var array
     */
    protected $mapMap = [
        'id'              => [
            'name'    => 'id',
            'default' => '-'
        ],
        'name'            => [
            'name'    => 'name',
            'default' => '-'
        ],
        'order_date'      => [
            'name'    => 'submitted_at',
            'default' => '-'
        ],
        'order_id'        => [
            'name'    => 'order_id',
            'default' => '-',
        ],
        'date_start'      => [
            'name'    => 'date_start',
            'default' => '-'
        ],
        'date_end'        => [
            'name'    => 'date_end',
            'default' => '-'
        ],
        'budget'          => [
            'name'    => 'budget',
            'default' => '-'
        ],
        'impressions'     => [
            'name'    => 'impressions',
            'default' => '-'
        ],
        'campaign_status' => [
            'name'    => 'campaign_status',
            'default' => '-'
        ],
        'live_status' => [
            'name'    => 'live_status',
            'default' => '-'
        ],
        'creative_status' => [
            'name'    => 'creative_status',
            'default' => ''
        ],
        'company_name'    => [
            'name'    => 'company_name',
            'default' => ''
        ],
        'company_id'    => [
            'name'    => 'company_id',
            'default' => ''
        ],
        'payment_status'    => [
            'name'    => 'payment_status',
            'default' => ''
        ],
        'last_activity'    => [
            'name'    => 'last_activity',
            'default' => ''
        ],
        'external_id'    => [
            'name'    => 'external_id',
            'default' => ''
        ]
    ];


    /**
     * Do transform.
     *
     * @param Model|\Modules\Broadcast\Models\Broadcast $model
     *
     * @return array
     */
    public function transform(Model $model): array
    {
        $mapped = parent::transform($model);
        $user = $this->getCurrentUser();
        $editRules = app(GetCampaignEditRulesAction::class)->handle($user, $model);

        // Add states
        $mapped['states'] = $editRules;

        return $mapped;
    }
}
