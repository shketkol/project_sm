<?php

namespace Modules\Campaign\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddCreativesCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model
            ->leftJoin('campaign_creative', 'campaign_creative.campaign_id', '=', 'campaigns.id')
            ->leftJoin('creatives', 'campaign_creative.creative_id', '=', 'creatives.id');
    }
}
