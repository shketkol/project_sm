<?php

namespace Modules\Campaign\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddOrderStatusCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model
            ->addSelect([
                'campaign_payment_statuses.name as payment_status',
                'campaign_payments.last_activity as last_activity',
            ])
            ->leftJoin('campaign_payments', 'campaigns.id', '=', 'campaign_payments.campaign_id')
            ->leftJoin('campaign_payment_statuses', 'campaign_payments.status_id', '=', 'campaign_payment_statuses.id');
    }
}
