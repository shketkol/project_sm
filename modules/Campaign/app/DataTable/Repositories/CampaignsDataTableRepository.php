<?php

namespace Modules\Campaign\DataTable\Repositories;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository
    as CampaignsDataTableRepositoryInterface;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Models\CreativeStatus;

class CampaignsDataTableRepository extends Repository implements CampaignsDataTableRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Campaign::class;
    }

    /**
     * Get user campaigns
     *
     * @return Builder
     */
    public function campaigns(): Builder
    {
        $this->applyCriteria();

        $selectParams = [
            'campaigns.id',
            'campaigns.impressions',
            'campaigns.date_start',
            'campaigns.date_end',
            'campaigns.name',
            'campaigns.created_at',
            'campaigns.updated_at',
            'campaigns.submitted_at',
            'campaigns.order_id',
            'campaigns.external_id',
            'campaigns.user_id',
            'campaigns.status_id',
            'IFNULL(campaigns.discounted_cost, campaigns.budget) as budget'
        ];

        return $this->model->selectRaw(implode(', ', $selectParams));
    }

    /**
     * Required actions query
     *
     * @return Builder
     */
    public function requiredActions(): Builder
    {
        $this->applyCriteria();

        return $this->model
            ->addSelect('campaigns.id')
            ->where('creatives.status_id', '=', CreativeStatus::ID_REJECTED);
    }
}
