<?php

namespace Modules\Campaign\Helpers;

use Carbon\Carbon;

/**
 * Class DateHelper
 *
 * @package Modules\Campaign\Helpers
 */
class DateHelper
{

    /**
     * Get minimal start date of campaign.
     *
     * @return Carbon
     */
    public static function getMinStartDate()
    {
        return self::addBusinessDays(Carbon::today(), config('campaign.wizard.date.start.min.value'));
    }

    /**
     * Add N business days to the provided date.
     *
     * @param Carbon $date
     * @param int    $days
     *
     * @return Carbon
     */
    public static function addBusinessDays(Carbon $date, int $days): Carbon
    {
        return $date->addWeekdays($days);
    }
}
