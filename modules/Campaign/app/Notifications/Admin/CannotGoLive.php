<?php

namespace Modules\Campaign\Notifications\Admin;

use App\Notifications\Traits\AdminNotification;
use Modules\Campaign\Mail\Admin\CannotGoLive as Mail;
use Modules\Campaign\Notifications\CampaignNotification;
use Modules\User\Models\User;

class CannotGoLive extends CampaignNotification
{
    use AdminNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'         => $notifiable->first_name,
            'companyName'       => $this->campaign->user->company_name,
            'campaignName'      => $this->campaign->name,
            'orderId'           => $this->campaign->order_id,
            'campaignDetails'   => $this->getDetailsUrl(),
            'advertiserDetails' => route(
                'advertisers.show',
                [
                    //TODO HULU-1617 - replace link
                    'advertiser' => $this->campaign->user->id,
                ]
            ),
        ];
    }
}
