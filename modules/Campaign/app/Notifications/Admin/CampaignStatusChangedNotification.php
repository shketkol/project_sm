<?php

namespace Modules\Campaign\Notifications\Admin;

use App\Notifications\Traits\AdminNotification;
use Modules\Campaign\Notifications\CampaignNotification;
use Modules\User\Models\User;

abstract class CampaignStatusChangedNotification extends CampaignNotification
{
    use AdminNotification;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'companyName'     => $this->actor->company_name,
            'campaignName'    => $this->campaign->name,
            'campaignDetails' => $this->getDetailsUrl(),
        ];
    }
}
