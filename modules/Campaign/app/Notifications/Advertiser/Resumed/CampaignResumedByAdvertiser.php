<?php

namespace Modules\Campaign\Notifications\Advertiser\Resumed;

use App\Notifications\Traits\AdvertiserNotification;
use Modules\Campaign\Mail\Advertiser\Resumed\CampaignResumedByAdvertiser as Mail;
use Modules\Campaign\Notifications\CampaignNotification;
use Modules\User\Models\User;

class CampaignResumedByAdvertiser extends CampaignNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'campaignName'    => $this->campaign->name,
            'campaignDetails' => $this->getDetailsUrl(),
            'title'           => __('campaign::emails.advertiser.resumed_by_advertiser.title'),
            'titleIcon'       => 'play',
        ];
    }

    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(User $notifiable): array
    {
        return ['mail'];
    }
}
