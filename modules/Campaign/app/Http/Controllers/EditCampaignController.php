<?php

namespace Modules\Campaign\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;

class EditCampaignController extends Controller
{
    /**
     * Display edit campaign page.
     *
     * @param Campaign $campaign
     *
     * @return View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(Campaign $campaign, InventoryCheckHistoryRepository $repository): View
    {
        $forceStepNavigation = null;

        /*
         * Force navigate to the budget step to cover migration scenario described in
         * modules/Campaign/database/migrations/2021_04_16_083005_update_step_id_in_draft_campaigns.php
        */
        if($campaign->step_id > CampaignStep::ID_SCHEDULE &&
            $campaign->step_id < CampaignStep::ID_CREATIVE &&
            !$repository->findLast($campaign)
        ) {
            $forceStepNavigation = CampaignStep::BUDGET;
        }

        return view('campaign::edit', ['id' => $campaign->id, 'forceNavigateTo' => $forceStepNavigation]);
    }
}
