<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Campaign\Actions\UpdateCampaignNameAction;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

class UpdateCampaignNameController extends Controller
{
    /**
     * Update name of the campaign.
     *
     * @param Campaign                 $campaign
     * @param Request                  $request
     * @param UpdateCampaignNameAction $action
     *
     * @return CampaignResource
     * @throws CampaignNotUpdatedException
     */
    public function __invoke(
        Campaign $campaign,
        Request $request,
        UpdateCampaignNameAction $action
    ): CampaignResource {
        return new CampaignResource($action->handle($campaign, $request->get('name', '')));
    }
}
