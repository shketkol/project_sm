<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\DeleteCampaignAction;
use Modules\Campaign\Http\Resources\CampaignDeleteResource;
use Modules\Campaign\Models\Campaign;

class DeleteCampaignController extends Controller
{
    /**
     * @param Campaign             $campaign
     * @param DeleteCampaignAction $action
     *
     * @return CampaignDeleteResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotDeletedException
     */
    public function __invoke(Campaign $campaign, DeleteCampaignAction $action): CampaignDeleteResource
    {
        $action->handle($campaign);

        return new CampaignDeleteResource(['message' => trans('campaign::messages.delete.success')]);
    }
}
