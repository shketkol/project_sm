<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\CheckDateRangeAction;
use Modules\Campaign\Actions\SpecialAds\RemoveSpecialAdsTargetingsAction;
use Modules\Campaign\Actions\UpdateCampaignBudgetAction;
use Modules\Campaign\Http\Resources\CampaignEditResource;
use Modules\Campaign\Models\Campaign;

class EditCampaignController extends Controller
{
    /**
     * Display edit campaign page.
     *
     * @param Campaign                         $campaign
     * @param CheckDateRangeAction             $checkDateRangeAction
     * @param RemoveSpecialAdsTargetingsAction $removeTargetingsAction
     *
     * @return CampaignEditResource
     * @throws \Throwable
     */
    public function __invoke(
        Campaign $campaign,
        CheckDateRangeAction $checkDateRangeAction,
        RemoveSpecialAdsTargetingsAction $removeTargetingsAction,
        UpdateCampaignBudgetAction $updateCampaignBudgetAction
    ): CampaignEditResource {
        $messages = [];
        $messages[] = $checkDateRangeAction->handle($campaign);
        $messages[] = $removeTargetingsAction->handle($campaign);
        $updateCampaignBudgetAction->handle($campaign);

        return new CampaignEditResource($campaign->loadPassedStepsRelations(), $messages);
    }
}
