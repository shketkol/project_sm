<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\ReplaceCampaignCreativeAction;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Http\Requests\ReplaceCampaignCreativeRequest;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

class ReplaceCampaignCreativeController extends Controller
{
    /**
     * Update campaign creative.
     *
     * @param Campaign                      $campaign
     * @param ReplaceCampaignCreativeAction  $action
     * @param ReplaceCampaignCreativeRequest $request
     *
     * @return CampaignResource
     * @throws CampaignNotUpdatedException
     */
    public function __invoke(
        Campaign $campaign,
        ReplaceCampaignCreativeAction $action,
        ReplaceCampaignCreativeRequest $request
    ): CampaignResource {
        return new CampaignResource($action->handle(
            $campaign,
            $request->creativeId
        ));
    }
}
