<?php

namespace Modules\Campaign\Http\Controllers\Api\Status;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Actions\Status\PauseCampaignAction;
use Modules\Campaign\Http\Resources\CampaignStatusResource;
use Modules\Campaign\Models\Campaign;

class PauseCampaignController extends Controller
{
    /**
     * Pause campaign.
     *
     * @param Campaign            $campaign
     * @param PauseCampaignAction $action
     *
     * @return CampaignStatusResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \SM\SMException
     * @throws \Throwable
     */
    public function __invoke(Campaign $campaign, PauseCampaignAction $action): CampaignStatusResource
    {
        $campaign = $action->handle($campaign, Auth::user());
        return new CampaignStatusResource($campaign->loadPassedStepsRelations());
    }
}
