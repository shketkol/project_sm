<?php

namespace Modules\Campaign\Http\Controllers\Api\Status;

use App\Helpers\CurrentUser;
use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\Status\ResumeCampaignAction;
use Modules\Campaign\Http\Resources\CampaignStatusResource;
use Modules\Campaign\Models\Campaign;

class ResumeCampaignController extends Controller
{
    use CurrentUser;

    /**
     * Resume campaign.
     *
     * @param Campaign             $campaign
     * @param ResumeCampaignAction $action
     *
     * @return CampaignStatusResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Throwable
     */
    public function __invoke(Campaign $campaign, ResumeCampaignAction $action): CampaignStatusResource
    {
        $campaign = $action->handle($campaign, $this->getCurrentUser());
        return new CampaignStatusResource($campaign->loadPassedStepsRelations());
    }
}
