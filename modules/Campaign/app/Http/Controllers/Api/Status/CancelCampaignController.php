<?php

namespace Modules\Campaign\Http\Controllers\Api\Status;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Actions\Status\CancelCampaignAction;
use Modules\Campaign\Http\Resources\CampaignStatusResource;
use Modules\Campaign\Models\Campaign;

class CancelCampaignController extends Controller
{
    /**
     * Cancel campaign.
     *
     * @param Campaign             $campaign
     * @param CancelCampaignAction $action
     *
     * @return CampaignStatusResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __invoke(Campaign $campaign, CancelCampaignAction $action): CampaignStatusResource
    {
        $campaign = $action->handle($campaign, Auth::user());
        return new CampaignStatusResource($campaign->loadPassedStepsRelations());
    }
}
