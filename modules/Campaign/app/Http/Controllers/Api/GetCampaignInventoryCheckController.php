<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Campaign\Actions\GetCampaignInventoryCheckAction;
use Modules\Campaign\Http\Resources\InventoryCheckResource;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;

class GetCampaignInventoryCheckController extends Controller
{
    /**
     * Get expected inventory check result.
     *
     * @param Campaign                        $campaign
     * @param GetCampaignInventoryCheckAction $action
     *
     * @return InventoryCheckResource|Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function __invoke(
        Campaign $campaign,
        GetCampaignInventoryCheckAction $action
    ) {
        try {
            $response = $action->handle($campaign);

            return new InventoryCheckResource($response);
        } catch (InvalidRequestException $exception) {
            return $this->invalidRequest();
        } catch (UnauthorizedException | ForbiddenException $exception) {
            return $this->forbidden();
        } catch (HaapiException $exception) {
            return $this->failed();
        }
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    private function forbidden(): Response
    {
        return response(
            ['message' => trans('campaign::messages.inventory_check.fail')],
            Response::HTTP_FORBIDDEN
        );
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    private function invalidRequest(): Response
    {
        return response(
            ['message' => trans('campaign::messages.inventory_check.fail.invalid_request')],
            Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    private function failed(): Response
    {
        return response(
            ['message' => trans('campaign::messages.inventory_check.fail.error')],
            Response::HTTP_BAD_REQUEST
        );
    }
}
