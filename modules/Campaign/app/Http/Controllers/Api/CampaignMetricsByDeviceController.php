<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Campaign\Actions\GetCampaignImpressionsMetricsAction;
use Modules\Campaign\Http\Resources\CampaignDevicesMetricsResource;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsMetricsParams;

class CampaignMetricsByDeviceController extends Controller
{
    /**
     * Get campaigns impressions grouped by devices.
     *
     * @param Campaign                            $campaign
     * @param Authenticatable                     $user
     * @param GetCampaignImpressionsMetricsAction $action
     *
     * @return CampaignDevicesMetricsResource
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function __invoke(
        Campaign $campaign,
        Authenticatable $user,
        GetCampaignImpressionsMetricsAction $action
    ): CampaignDevicesMetricsResource {
        return new CampaignDevicesMetricsResource(
            $action->handle($campaign, $user, ImpressionsMetricsParams::TYPE_DEVICE)
        );
    }
}
