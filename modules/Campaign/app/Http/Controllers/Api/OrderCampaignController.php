<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\OrderCampaignAction;
use Modules\Campaign\Http\Requests\OrderCampaignRequest;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

class OrderCampaignController extends Controller
{
    /**
     * Order campaign (complete wizard).
     *
     * @param Campaign             $campaign
     * @param OrderCampaignAction  $action
     * @param OrderCampaignRequest $request
     *
     * @return CampaignResource
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function __invoke(Campaign $campaign, OrderCampaignAction $action, OrderCampaignRequest $request)
    {
        return new CampaignResource($action->handle($campaign, $request->paymentId));
    }
}
