<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Campaign\Actions\StoreCampaignAction;
use Modules\Campaign\Http\Requests\StoreCampaignRequest;
use Modules\Campaign\Http\Resources\CampaignResource;

class StoreCampaignController extends Controller
{
    /**
     * Store new draft campaign in database.
     *
     * @param StoreCampaignAction  $action
     * @param StoreCampaignRequest $request
     *
     * @return JsonResponse
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function __invoke(StoreCampaignAction $action, StoreCampaignRequest $request): JsonResponse
    {
        return (new CampaignResource($action->handle($request->all())))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
}
