<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\CheckAllowedInventoryCheckAction;
use Modules\Campaign\Actions\GetDynamicInventoryCheckAction;
use Modules\Campaign\Http\Resources\InventoryCheckResource;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Exceptions\ForbiddenException;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;

class GetDynamicInventoryCheckController extends Controller
{
    /**
     * Get dynamic inventory check result.
     *
     * @param Campaign                         $campaign
     * @param GetDynamicInventoryCheckAction   $inventoryCheck
     * @param CheckAllowedInventoryCheckAction $allowedCheck
     *
     * @return InventoryCheckResource|Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \Throwable
     */
    public function __invoke(
        Campaign $campaign,
        GetDynamicInventoryCheckAction $inventoryCheck,
        CheckAllowedInventoryCheckAction $allowedCheck
    ) {
        try {
            $result = $allowedCheck->handle($campaign);
            $response = Arr::get($result, 'response', []);

            if (Arr::get($result, 'allowed', true)) {
                $response = $inventoryCheck->handle($campaign);
            }

            return new InventoryCheckResource($response);
        } catch (InvalidRequestException $exception) {
            return $this->invalidRequest();
        } catch (UnauthorizedException | ForbiddenException $exception) {
            return $this->forbidden();
        } catch (HaapiException $exception) {
            return $this->failed();
        }
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    private function forbidden(): Response
    {
        return response(
            ['message' => trans('campaign::messages.inventory_check.fail')],
            Response::HTTP_FORBIDDEN
        );
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    private function invalidRequest(): Response
    {
        return response(
            ['message' => trans('campaign::messages.inventory_check.fail.invalid_request')],
            Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    private function failed(): Response
    {
        return response(
            ['message' => trans('campaign::messages.inventory_check.fail.error')],
            Response::HTTP_BAD_REQUEST
        );
    }
}
