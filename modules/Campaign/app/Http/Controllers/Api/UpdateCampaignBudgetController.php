<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Campaign\Actions\UpdateCampaignBudgetAction;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

class UpdateCampaignBudgetController extends Controller
{
    /**
     * Update campaign budget.
     *
     * @param Campaign                    $campaign
     * @param Request                     $request
     * @param UpdateCampaignBudgetAction  $action
     *
     * @return CampaignResource
     * @throws CampaignNotUpdatedException
     */
    public function __invoke(Campaign $campaign, Request $request, UpdateCampaignBudgetAction $action): CampaignResource
    {
        $campaign = $action->handle(
            $campaign,
            $request->only(['cost'])
        );
        $campaign->loadPassedStepsRelations();
        return new CampaignResource($campaign);
    }
}
