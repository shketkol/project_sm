<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\DeletePromocodeAction;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

class DeletePromocodeController extends Controller
{
    /**
     * @param Campaign $campaign
     * @param DeletePromocodeAction $action
     * @return CampaignResource
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignNotOrderedException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Promocode\Exceptions\PromocodeCouldNotBeAppliedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \Throwable
     */
    public function __invoke(Campaign $campaign, DeletePromocodeAction $action): CampaignResource
    {
        $action->handle($campaign);
        return new CampaignResource($campaign);
    }
}
