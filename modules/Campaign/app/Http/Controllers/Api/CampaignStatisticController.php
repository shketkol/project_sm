<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\GetCampaignStatisticsAction;
use Modules\Campaign\Http\Resources\CampaignStatisticResource;
use Modules\Campaign\Models\Campaign;

class CampaignStatisticController extends Controller
{
    /**
     * Get campaigns Cost, Performance and Delivered impressions.
     *
     * @param Campaign                    $campaign
     * @param GetCampaignStatisticsAction $action
     *
     * @return CampaignStatisticResource
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function __invoke(
        Campaign $campaign,
        GetCampaignStatisticsAction $action
    ): CampaignStatisticResource {
        return new CampaignStatisticResource($action->handle($campaign));
    }
}
