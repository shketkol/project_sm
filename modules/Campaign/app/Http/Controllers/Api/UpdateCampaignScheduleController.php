<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Campaign\Actions\UpdateCampaignScheduleAction;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

class UpdateCampaignScheduleController extends Controller
{
    /**
     * Update campaign schedule.
     *
     * @param Campaign                     $campaign
     * @param Request                      $request
     * @param UpdateCampaignScheduleAction $action
     *
     * @return CampaignResource
     * @throws CampaignNotUpdatedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        Campaign $campaign,
        Request $request,
        UpdateCampaignScheduleAction $action
    ): CampaignResource {
        return new CampaignResource($action->handle(
            $campaign,
            $request->only(['date', 'timezone_id'])
        ));
    }
}
