<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Campaign\Actions\UpdateCampaignStepAction;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

class UpdateCampaignStepController extends Controller
{
    /**
     * Id of current step. This should be moved into an action.
     */
    public static $stepId;

    /**
     * Update campaign step.
     *
     * @param Campaign                 $campaign
     * @param Request                  $request
     * @param UpdateCampaignStepAction $action
     *
     * @return CampaignResource
     * @throws CampaignNotUpdatedException
     */
    public function __invoke(Campaign $campaign, Request $request, UpdateCampaignStepAction $action): CampaignResource
    {
        return new CampaignResource($action->handle($campaign, static::$stepId));
    }
}
