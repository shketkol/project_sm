<?php

namespace Modules\Campaign\Http\Controllers\Api\Traits;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Arr;
use Modules\Campaign\Exceptions\CampaignNotCreatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Repositories\CampaignRepository;

trait StoreCampaign
{
    /**
     * Store new campaign instance.
     *
     * @param array $data
     * @param int $step
     *
     * @return Campaign
     * @throws CampaignNotCreatedException
     */
    protected function storeCampaign(array $data, int $step = CampaignStep::ID_NAME): Campaign
    {
        $repository = app(CampaignRepository::class);
        $validationRules = app(ValidationRules::class);
        $auth = app(Guard::class);

        $campaign = $repository->newInstance([]);

        Arr::set($data, 'name', substr(
            Arr::get($data, 'name'),
            0,
            $validationRules->get('campaign.details.name.max')
        ));

        Arr::set($data, 'submitted_at', Carbon::now());

        $campaign->fill($data);

        $campaign->user()->associate($auth->user());
        $campaign->status()->associate(CampaignStatus::ID_DRAFT);
        $campaign->step()->associate($step);

        if (!$campaign->save()) {
            throw new CampaignNotCreatedException('Campaign was not created');
        }

        $this->log->info('Campaign was successfully saved.', ['campaign_id' => $campaign->id]);

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     */
    protected function checkDates(Campaign $campaign): Campaign
    {
        $tomorrow = Carbon::tomorrow(config('campaign.wizard.date.timezone'))
            ->format(config('date.format.db_date_time'));

        $this->log->info('Update campaign start/end dates.', ['campaign_id' => $campaign->id]);

        if ($campaign->date_end <= $tomorrow) {
            $campaign->date_end = $this->getEndDateUpdated($campaign);
        }

        $campaign->date_start = $tomorrow;

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     */
    protected function createNewDates(Campaign $campaign): Campaign
    {
        $campaign->date_end = $this->getEndDateUpdated($campaign);

        $tomorrow = Carbon::tomorrow(config('campaign.wizard.date.timezone'))
            ->format(config('date.format.db_date_time'));

        $campaign->date_start = $tomorrow;

        return $campaign;
    }


    /**
     * @param Campaign $campaign
     *
     * @return string
     */
    private function getEndDateUpdated(Campaign $campaign): string
    {
        $startDate = Carbon::parse($campaign->date_start);
        $endDate = Carbon::parse($campaign->date_end);
        $diff = $startDate->diffInDays($endDate);

        return Carbon
            ::tomorrow(config('campaign.wizard.date.timezone'))
            ->addDays($diff)
            ->endOfDay()
            ->format(config('date.format.db_date_time'));
    }
}
