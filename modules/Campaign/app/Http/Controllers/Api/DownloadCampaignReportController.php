<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Campaign\Models\Campaign;
use Modules\Report\Actions\Metrics\GenerateReportMetricsAction;
use Modules\Report\Helpers\GenerateReportFilename;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class DownloadCampaignReportController extends Controller
{
    use GenerateReportFilename;

    /**
     * @param Campaign                    $campaign
     * @param Authenticatable             $user
     * @param GenerateReportMetricsAction $action
     *
     * @return BinaryFileResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Report\Exceptions\ReportNotGeneratedException
     */
    public function __invoke(
        Campaign $campaign,
        Authenticatable $user,
        GenerateReportMetricsAction $action
    ): BinaryFileResponse {
        $report = $action->handle($campaign, $user);
        $name = $this->createReportMetricsFilename($campaign->name);

        return $report->download($name);
    }
}
