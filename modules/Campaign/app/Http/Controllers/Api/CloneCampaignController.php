<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Campaign\Actions\CloneCampaignAction;
use Modules\Campaign\Models\Campaign;

class CloneCampaignController extends Controller
{
    /**
     * @param Campaign $campaign
     * @param CloneCampaignAction $action
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function __invoke(Campaign $campaign, CloneCampaignAction $action)
    {
        try {
            $clonedCampaign = $action->handle($campaign);
        } catch (\Throwable $throwable) {
            return response(
                ['message' => trans('campaign::messages.fail_clone')],
                Response::HTTP_BAD_REQUEST
            );
        };

        return response(
            ['route' => route('campaigns.clone', ['campaign' => $clonedCampaign->id])],
            Response::HTTP_OK
        );
    }
}
