<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\ValidateCampaignAction;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

class ValidateCampaignController extends Controller
{
    /**
     * Validate stored campaign data.
     *
     * @param Campaign               $campaign
     * @param ValidateCampaignAction $action
     *
     * @return CampaignResource
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    public function __invoke(Campaign $campaign, ValidateCampaignAction $action)
    {
        return new CampaignResource($action->handle($campaign));
    }
}
