<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Http\Resources\CampaignPermissionsResource;
use Modules\Campaign\Models\Campaign;

/**
 * @package Modules\Campaign\Http\Controllers\Api
 */
class GetCampaignPermissionsController extends Controller
{
    /**
     * Show campaign permission state including creative drafts amount limit status.
     *
     * @param Campaign $campaign
     *
     * @return CampaignPermissionsResource
     */
    public function __invoke(Campaign $campaign): CampaignPermissionsResource
    {
        return new CampaignPermissionsResource($campaign);
    }
}
