<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\UpdateCampaignCreativeAction;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Http\Requests\UpdateCampaignCreativeRequest;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

class UpdateCampaignCreativeController extends Controller
{
    /**
     * Update campaign creative.
     *
     * @param Campaign                      $campaign
     * @param UpdateCampaignCreativeAction  $action
     * @param UpdateCampaignCreativeRequest $request
     *
     * @return CampaignResource
     * @throws CampaignNotUpdatedException
     */
    public function __invoke(
        Campaign $campaign,
        UpdateCampaignCreativeAction $action,
        UpdateCampaignCreativeRequest $request
    ): CampaignResource {
        return new CampaignResource($action->handle(
            $campaign,
            $request->creativeId
        ));
    }
}
