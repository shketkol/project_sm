<?php

namespace Modules\Campaign\Http\Controllers\Api;

use Modules\Campaign\Models\CampaignStep;

class UpdateCampaignCreativeStepController extends UpdateCampaignStepController
{
    public static $stepId = CampaignStep::ID_CREATIVE;
}
