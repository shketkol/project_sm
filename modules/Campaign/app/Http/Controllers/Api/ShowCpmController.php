<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\GetCampaignPriceAction;
use Modules\Campaign\Http\Resources\CpmResource;
use Modules\Campaign\Models\Campaign;

class ShowCpmController extends Controller
{
    /**
     * Get cpm for the campaign.
     *
     * @param Campaign               $campaign
     *
     * @param GetCampaignPriceAction $getCampaignPrice
     *
     * @return CpmResource
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function __invoke(Campaign $campaign, GetCampaignPriceAction $getCampaignPrice): CpmResource
    {
        $priceResource = $getCampaignPrice->handle($campaign);

        return new CpmResource([
            'minDailyImpressions' => Arr::get($priceResource, 'minDailyImpressions'),
            'cpm'                 => Arr::get($priceResource, 'unitCost'),
        ]);
    }
}
