<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Actions\DeletePromocodeAction;
use Modules\Campaign\Actions\GetCampaignEditRulesAction;
use Modules\Campaign\Http\Resources\CampaignDetailsResource;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;

/**
 * Class CampaignDetailsController
 *
 * @package Modules\Campaign\Http\Controllers\Api
 */
class CampaignDetailsController extends Controller
{
    /**
     * @var GetCampaignEditRulesAction
     */
    private $getEditRulesAction;

    /**
     * CampaignDetailsController constructor.
     *
     * @param GetCampaignEditRulesAction $getEditRulesAction
     */
    public function __construct(
        GetCampaignEditRulesAction $getEditRulesAction
    ) {
        $this->getEditRulesAction = $getEditRulesAction;
    }

    /**
     * @param Campaign              $campaign
     * @param DeletePromocodeAction $deletePromocodeAction
     *
     * @return CampaignDetailsResource
     * @throws \SM\SMException
     */
    public function __invoke(Campaign $campaign, DeletePromocodeAction $deletePromocodeAction): CampaignDetailsResource
    {
        $deletePromocodeAction->handle($campaign);
        $campaign->load('creatives');
        $campaign->loadPassedStepsRelations();

        return new CampaignDetailsResource([
            'details'   => new CampaignResource($campaign),
            'states'    => $this->getEditRulesAction->handle(Auth::user(), $campaign),
            'campaign'  => $campaign,
        ]);
    }
}
