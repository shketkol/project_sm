<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;
use Modules\Daapi\Http\Controllers\Controller;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignAgeGroupsAction;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignGendersAction;
use Modules\Targeting\Http\Requests\UpdateTargetingAgeGroupsRequest;

class UpdateCampaignAgeGroupsController extends Controller
{
    /**
     * Save targeting age groups and update campaign step.
     *
     * @param Campaign $campaign
     * @param UpdateTargetingAgeGroupsRequest $request
     * @param UpdateCampaignAgeGroupsAction $actionAges
     * @param UpdateCampaignGendersAction $actionGender
     * @return CampaignResource
     * @throws CampaignNotUpdatedException
     */
    public function __invoke(
        Campaign $campaign,
        UpdateTargetingAgeGroupsRequest $request,
        UpdateCampaignAgeGroupsAction $actionAges,
        UpdateCampaignGendersAction $actionGender
    ): CampaignResource {
        $actionGender->handle($campaign, $request->genderId);
        $actionAges->handle($campaign, $request->ageGroups);
        $campaign->refresh()->loadPassedStepsRelations();
        return new CampaignResource($campaign);
    }
}
