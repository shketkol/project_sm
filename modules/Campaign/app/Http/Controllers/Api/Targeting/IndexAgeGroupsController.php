<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\AgeGroup\IndexAgeGroupsAction;
use Modules\Targeting\Http\Resources\AgesWithGenderResource;
use Modules\User\Models\User;

class IndexAgeGroupsController extends Controller
{
    /**
     * @param Campaign $campaign
     * @param IndexAgeGroupsAction $action
     *
     * @return AnonymousResourceCollection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        Campaign $campaign,
        IndexAgeGroupsAction $action
    ): AnonymousResourceCollection {
        return AgesWithGenderResource::collection($action->handle($campaign));
    }
}
