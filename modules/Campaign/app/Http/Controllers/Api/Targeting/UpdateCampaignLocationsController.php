<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\UpdateCampaignBudgetAction;
use Modules\Campaign\Actions\UpdateCampaignStepAction;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Targeting\Actions\Location\UpdateCampaignLocationsAction;
use Modules\Targeting\Actions\Location\Zipcode\UpdateCampaignZipcodesAction;
use Modules\Targeting\Http\Requests\UpdateTargetingLocationsRequest;

class UpdateCampaignLocationsController extends Controller
{
    /**
     * Save targeting zipcodes and locations and update campaign step.
     *
     * @param Campaign                        $campaign
     * @param UpdateTargetingLocationsRequest $request
     * @param UpdateCampaignLocationsAction   $locationsAction
     * @param UpdateCampaignZipcodesAction    $zipcodesAction
     * @param UpdateCampaignStepAction        $stepAction
     *
     * @return CampaignResource
     * @throws \Throwable
     */
    public function __invoke(
        Campaign $campaign,
        UpdateTargetingLocationsRequest $request,
        UpdateCampaignLocationsAction $locationsAction,
        UpdateCampaignZipcodesAction $zipcodesAction,
        UpdateCampaignStepAction $stepAction,
        UpdateCampaignBudgetAction $budgetAction
    ): CampaignResource {
        $zipcodesAction->handle($campaign, $request->get('zipcodes', []));
        $locationsAction->handle($campaign, $request->get('locations', []));
        $budgetAction->handle($campaign);

        $stepAction->handle($campaign, CampaignStep::ID_LOCATIONS);

        $campaign->refresh()->loadPassedStepsRelations();

        return new CampaignResource($campaign);
    }
}
