<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\Genre\SearchGenresAction;
use Modules\Targeting\Http\Requests\SearchRequest;
use Modules\Targeting\Http\Resources\GenreResource;

class SearchGenresController extends Controller
{
    /**
     * @param Campaign           $campaign
     * @param SearchGenresAction $action
     * @param SearchRequest      $request
     *
     * @return AnonymousResourceCollection
     * @throws \Modules\Targeting\Exceptions\ClassNotMappedException
     */
    public function __invoke(
        Campaign $campaign,
        SearchGenresAction $action,
        SearchRequest $request
    ): AnonymousResourceCollection {
        $query = $request->get('query') ?: '';
        return GenreResource::collection($action->handle($campaign, $query)->sortBy('name'));
    }
}
