<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\Audience\UpdateCampaignAudiencesAction;
use Modules\Targeting\Http\Requests\UpdateTargetingAudiencesRequest;

class UpdateCampaignAudiencesController extends Controller
{
    /**
     * Save targeting audiences and update campaign step.
     *
     * @param Campaign                     $campaign
     * @param UpdateTargetingAudiencesRequest $request
     * @param UpdateCampaignAudiencesAction   $action
     *
     * @return CampaignResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    public function __invoke(
        Campaign $campaign,
        UpdateTargetingAudiencesRequest $request,
        UpdateCampaignAudiencesAction $action
    ): CampaignResource {
        $action->handle($campaign, $request->toData());
        $campaign->refresh()->loadPassedStepsRelations();

        return new CampaignResource($campaign);
    }
}
