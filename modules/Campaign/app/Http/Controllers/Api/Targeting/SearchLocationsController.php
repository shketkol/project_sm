<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\Location\SearchLocationsAction;
use Modules\Targeting\Http\Requests\SearchRequest;
use Modules\Targeting\Http\Resources\LocationResource;

class SearchLocationsController extends Controller
{
    /**
     * @param Campaign              $campaign
     * @param SearchLocationsAction $action
     * @param SearchRequest         $request
     *
     * @return AnonymousResourceCollection
     * @throws \Modules\Targeting\Exceptions\ClassNotMappedException
     */
    public function __invoke(
        Campaign $campaign,
        SearchLocationsAction $action,
        SearchRequest $request
    ): AnonymousResourceCollection {
        $query = $request->get('query') ?: '';
        return LocationResource::collection($action->handle($campaign, $query));
    }
}
