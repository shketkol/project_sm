<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Helpers\ArrayHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\Audience\ShowAudienceTiersAction;

class IndexAudiencesController extends Controller
{
    use ArrayHelper;

    /**
     * Get list of all available audiences with tiers.
     *
     * @param Campaign $campaign
     * @param ShowAudienceTiersAction $action
     *
     * @return JsonResponse
     */
    public function __invoke(Campaign $campaign, ShowAudienceTiersAction $action): JsonResponse
    {
        return response()->json(['data' => $this->sortByField($action->handle($campaign), 'name')]);
    }
}
