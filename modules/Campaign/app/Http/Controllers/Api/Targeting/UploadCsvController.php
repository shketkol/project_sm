<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Helpers\CsvHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\Location\Zipcode\SearchZipcodesAction;
use Modules\Targeting\Exceptions\ClassNotMappedException;
use Modules\Targeting\Http\Requests\UploadCsvRequest;
use Modules\Targeting\Http\Resources\ZipcodeResource;

class UploadCsvController extends Controller
{
    use CsvHelper;

    /**
     * @param Campaign             $campaign
     * @param SearchZipcodesAction $action
     * @param UploadCsvRequest     $request
     *
     * @return JsonResponse
     * @throws ClassNotMappedException
     */
    public function __invoke(
        Campaign $campaign,
        SearchZipcodesAction $action,
        UploadCsvRequest $request
    ): JsonResponse {
        $data = $this->parse($request->toArray()['content']);

        $collection = $action->handle($campaign, $data);

        $found = ZipcodeResource::collection($collection)->response($request)->getData()->data;
        $notFound = array_values(array_diff($data, array_column($found, 'name')));
        $notFound = array_map(function (string $string): string {
            return $this->isValidUTF8($string) ? $string : $this->convertToUTF8($string);
        }, $notFound);

        return response()->json(['data' => [
            'found'     => $found,
            'not_found' => $notFound,
        ]]);
    }
}
