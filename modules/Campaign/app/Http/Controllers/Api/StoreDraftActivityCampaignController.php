<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Campaign\Actions\SendDraftActivityCampaignAction;
use Modules\Campaign\Exceptions\CancelCampaignActivityRequestException;
use Modules\Campaign\Exceptions\InvalidCampaignActivityRequestException;
use Modules\Campaign\Http\Requests\StoreDraftActivityCampaignRequest;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Psr\Log\LoggerInterface;

class StoreDraftActivityCampaignController extends Controller
{
    /**
     * @param Campaign                          $campaign
     * @param StoreDraftActivityCampaignRequest $request
     * @param SendDraftActivityCampaignAction   $action
     * @param LoggerInterface                   $log
     *
     * @return JsonResponse
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignExternalIdOverrideException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \Modules\Campaign\Exceptions\InvalidCampaignActivityRequestException
     */
    public function __invoke(
        Campaign $campaign,
        StoreDraftActivityCampaignRequest $request,
        SendDraftActivityCampaignAction $action,
        LoggerInterface $log
    ): JsonResponse {
        try {
            $action->handle($campaign, $request->validated());
        } catch (CancelCampaignActivityRequestException $exception) {
            $log->info('[Campaign][Draft Activity] Request cancelled.', [
                'campaign_id' => $campaign->id,
                'data'        => $request->validated(),
                'reason'      => $exception->getMessage(),
            ]);
        } catch (InvalidRequestException $exception) {
            throw InvalidCampaignActivityRequestException::createFrom($exception);
        }

        return response()->json(['data' => ['message' => trans('messages.success')]]);
    }
}
