<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Http\Resources\GetBillingDateResource;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Payment\OrderGetBillingDate;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetBillingDateParams;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\UnauthorizedException;

class GetCampaignBillingDateController extends Controller
{
    /**
     * Get next expected billing date.
     *
     * @param Campaign            $campaign
     * @param OrderGetBillingDate $action
     *
     * @return GetBillingDateResource|Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws UnauthorizedException
     */
    public function __invoke(
        Campaign $campaign,
        OrderGetBillingDate $action
    ) {
        try {
            $billingDateParams = new OrderGetBillingDateParams([
                'orderStartDate' => $campaign->date_start,
            ]);

            $response = $action->handle($billingDateParams, Auth::id());

            return new GetBillingDateResource($response);
        } catch (UnauthorizedException $exception) {
            throw $exception;
        } catch (HaapiException $exception) {
            return $this->failed();
        }
    }

    /**
     * @return ResponseFactory|Response
     */
    private function failed(): Response
    {
        return response(
            ['message' => trans('campaign::messages.billing_date.fail')],
            Response::HTTP_BAD_REQUEST
        );
    }
}
