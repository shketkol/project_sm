<?php

namespace Modules\Campaign\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;

class CloneCampaignController extends Controller
{
    /**
     * @param Campaign $campaign
     *
     * @return Factory|View
     */
    public function __invoke(Campaign $campaign)
    {
        return view('campaign::edit', [
            'id' => $campaign->id,
            'forceNavigateTo' => CampaignStep::NAME
        ]);
    }
}
