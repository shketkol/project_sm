<?php

namespace Modules\Campaign\Http\Resources;

use Illuminate\Support\Arr;

class CampaignFrequencyResource extends CampaignResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'frequency'     => number_format(Arr::get($this->resource, 'frequency'), 1),
            'uniqueDevices' => Arr::get($this->resource, 'uniqueDevices'),
        ];
    }
}
