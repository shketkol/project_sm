<?php

namespace Modules\Campaign\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetBillingDateData;

/**
 * @package Modules\Campaign\Http\Resources
 * @mixin OrderGetBillingDateData
 */
class GetBillingDateResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $carbonDate = Carbon::createFromFormat(config('date.format.php'), $this->billingDate);

        return [
            'billingPeriodName' => $carbonDate->format(config('date.format.api')),
            'billingDate'       => $carbonDate,
        ];
    }
}
