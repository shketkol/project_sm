<?php

namespace Modules\Campaign\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignStatisticResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'cost'                 => $this->resource['cost'],
            'performance'          => $this->resource['performance'],
            'deliveredImpressions' => $this->resource['deliveredImpressions'],
        ];
    }
}
