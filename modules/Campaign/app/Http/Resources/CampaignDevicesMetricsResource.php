<?php

namespace Modules\Campaign\Http\Resources;

use App\Helpers\Math\CalculationHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Modules\Campaign\Mappers\Charts\DevicesMapper;

class CampaignDevicesMetricsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $generalDataByDevice = array_filter(
            array_map(array(DevicesMapper::class, 'map'), Arr::get($this->resource, 'totalImpressionsByType', []))
        );

        $generalDataByDevice = CalculationHelper::largestReminderPercentage(
            $generalDataByDevice,
            ['multiplier' => 100]
        );

        return [
            'generalImpressionsByDevice' => $generalDataByDevice,
        ];
    }
}
