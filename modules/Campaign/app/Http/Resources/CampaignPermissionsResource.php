<?php

namespace Modules\Campaign\Http\Resources;

use App\Helpers\CurrentUser;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @package Modules\Campaign\Http\Resources
 * @mixin \Modules\Campaign\Models\Campaign
 */
class CampaignPermissionsResource extends JsonResource
{
    use CurrentUser;
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $user = $this->getCurrentUser();

        return [
            'canManageCreative'      => $user->can('campaign.manageCampaignCreative', $this->resource),
            'canOnlyReplaceCreative' => $user->can('campaign.replaceCampaignCreative', $this->resource),
            'creativeDraftAllowed'   => $user->can('creative.placeDraft'),
        ];
    }
}
