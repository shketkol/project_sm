<?php

namespace Modules\Campaign\Http\Resources;

use App\Helpers\CurrentUser;
use App\Helpers\DateFormatHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;
use Modules\Campaign\Actions\GetCampaignEditRulesAction;
use Modules\Creative\DataTable\Repositories\Contracts\CreativesDataTableRepository;
use Modules\Creative\Http\Resources\CreativeResource;
use Modules\Promocode\Http\Resources\PromocodeResource;
use Modules\Targeting\Http\Resources\AgeGroupsFormatAction;
use Modules\Targeting\Http\Resources\AudienceResource;
use Modules\Targeting\Http\Resources\DeviceGroupResource;
use Modules\Targeting\Http\Resources\GenderResource;
use Modules\Targeting\Http\Resources\GenreResource;
use Modules\Targeting\Http\Resources\LocationResource;
use Modules\Targeting\Http\Resources\ZipcodeResource;

/**
 * @mixin \Modules\Campaign\Models\Campaign
 */
class CampaignResource extends JsonResource
{
    use CurrentUser;

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $creative = $this->getActiveCreative();
        $user = $this->getCurrentUser();

        $countUploadedCreatives = app(CreativesDataTableRepository::class)->getUserCreativesCount($user);

        $editRules = app(GetCampaignEditRulesAction::class)->handle($user, $this->resource);

        $ages = $this->whenLoaded('targetingAgeGroups');
        // Format the age groups only if the relation "targetingAgeGroups" is loaded.
        if (!($ages instanceof MissingValue)) {
            $ages = AgeGroupsFormatAction::handle($this->targetingAgeGroups, $this->targetingGenders);
        }

        return [
            'id'                    => $this->getKey(),
            'name'                  => $this->name,
            'company_name'          => $this->user->company_name,
            'advertiser_id'         => $this->user->id,
            'date_start'            => $this->date_start,
            'date_end'              => $this->date_end,
            'timezone_id'           => $this->timezone_id,
            'timezone'              => $this->timezone->code,
            'discounted_cost'       => $this->discounted_cost,
            'discount_money'        => $this->getDiscountMoney(),
            'cost'                  => $this->budget,
            'budget'                => $this->budget,
            'impressions'           => $this->impressions,
            'min_daily_impressions' => 1000,
            'status_id'             => $this->status_id,
            'status_change_actor'   => $this->status_change_actor,
            // The date is hardcoded(must be displayed) to ET at FE
            'status_changed_at'     => $this->status_changed_at ?
                DateFormatHelper::toTimezone(
                    $this->status_changed_at,
                    config('date.default_timezone_full_code')
                ) :
                null,
            'cpm'                   => $this->cpm,
            'discounted_cpm'        => $this->discounted_cpm,
            'status'                => $this->status->name,
            'order_id'              => $this->order_id,
            'creative'              => $creative ? new CreativeResource($creative) : null,
            'countUploadedAds'      => $countUploadedCreatives,
            'targeting'             => [
                'genres'       => GenreResource::collection($this->whenLoaded('genres')),
                'deviceGroups' => DeviceGroupResource::collection($this->whenLoaded('deviceGroups')),
                'gender'       => GenderResource::collection($this->whenLoaded('targetingGenders')),
                'ages'         => $ages,
                'locations'    => LocationResource::collection($this->whenLoaded('locations')),
                'zipcodes'     => ZipcodeResource::collection($this->whenLoaded('zipcodes')),
                'audiences'    => AudienceResource::collection($this->whenLoaded('audiences')),
            ],
            'permissions'           => new CampaignPermissionsResource($this->resource),
            'states'                => $editRules,
            'promocode'             => new PromocodeResource($this->promocode),
            'payment_method_id'     => $this->payment_method_id,
        ];
    }
}
