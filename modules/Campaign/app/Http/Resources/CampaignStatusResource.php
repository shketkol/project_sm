<?php

namespace Modules\Campaign\Http\Resources;

use Modules\Campaign\Actions\GetCampaignEditRulesAction;

/**
 * @package Modules\Campaign\Http\Resources
 * @mixin \Modules\Campaign\Models\Campaign
 */
class CampaignStatusResource extends CampaignResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     * @throws \SM\SMException
     */
    public function toArray($request): array
    {
        /*** @var GetCampaignEditRulesAction $getEditRulesAction */
        $getEditRulesAction = app(GetCampaignEditRulesAction::class);

        return [
            'details'     => parent::toArray($request),
            'states'      => $getEditRulesAction->handle($this->getCurrentUser(), $this->resource),
        ];
    }
}
