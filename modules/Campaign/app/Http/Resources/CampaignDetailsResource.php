<?php

namespace Modules\Campaign\Http\Resources;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class CampaignDetailsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var \Modules\User\Models\User $user */
        $user = app(Guard::class)->user();

        return [
            'details'     => Arr::get($this->resource, 'details'),
            'states'      => Arr::get($this->resource, 'states'),
            'permissions' => new CampaignPermissionsResource(Arr::get($this->resource, 'campaign')),
        ];
    }
}
