<?php

namespace Modules\Campaign\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class StoreDraftActivityCampaignRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'action_name' => $validationRules->only(
                'campaign.wizard.action_name',
                ['nullable', 'string', 'min', 'max', 'in']
            ),
            'last_page'   => $validationRules->only(
                'campaign.wizard.last_page',
                ['required', 'string', 'min', 'max', 'in']
            ),
        ];
    }
}
