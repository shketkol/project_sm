<?php

namespace Modules\Campaign\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

/**
 * @property int $paymentId
 */
class OrderCampaignRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'paymentId' => $validationRules->only(
                'campaign.payment.name',
                ['payment_method']
            ),
        ];
    }
}
