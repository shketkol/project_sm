<?php

namespace Modules\Campaign\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

/**
 * @property int|null $creativeId
 */
class UpdateCampaignCreativeRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'creativeId' => $validationRules->only(
                'campaign.creative',
                ['sometimes', 'required', 'integer', 'min', 'max']
            ),
        ];
    }
}
