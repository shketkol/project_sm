<?php

namespace Modules\Campaign\Policies;

use App\Policies\Policy;
use App\Policies\Traits\Checks;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Zipcode;
use Modules\User\Models\User;

class CampaignPolicy extends Policy
{
    use Checks;

    /**
     * Model class.
     *
     * @var string
     */
    protected $model = Campaign::class;

    /*** Permissions. */
    public const PERMISSION_CREATE_CAMPAIGN          = 'create_campaign';
    public const PERMISSION_DELETE_CAMPAIGN          = 'delete_campaign';
    public const PERMISSION_LIST_CAMPAIGN            = 'list_campaign';
    public const PERMISSION_LIST_ADVERTISER_CAMPAIGN = 'list_advertiser_campaign';
    public const PERMISSION_UPDATE_CAMPAIGN          = 'update_campaign';
    public const PERMISSION_VIEW_CAMPAIGN            = 'view_campaign';
    public const PERMISSION_CANCEL_CAMPAIGN          = 'cancel_campaign';
    public const PERMISSION_PAUSE_CAMPAIGN           = 'pause_campaign';
    public const PERMISSION_RESUME_CAMPAIGN          = 'resume_campaign';
    public const PERMISSION_MANAGE_CAMPAIGN_CREATIVE = 'manage_campaign_creative';
    public const PERMISSION_CLONE_CAMPAIGN           = 'clone_campaign';

    /**
     * Returns true if user can view list of all campaigns.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_CAMPAIGN);
    }

    /**
     * Returns true if user can view list campaigns of given advertiser.
     *
     * @param User $user
     * @param User $advertiser
     *
     * @return bool
     */
    public function listAdvertiserCampaigns(User $user, ?User $advertiser): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_ADVERTISER_CAMPAIGN) &&
            $this->hasAccessToCampaigns($user, $advertiser);
    }

    /**
     * Returns true if user can store a new campaign draft.
     *
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CREATE_CAMPAIGN);
    }

    /**
     * Returns true if user can view the campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function view(User $user, Campaign $campaign): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_VIEW_CAMPAIGN)) {
            return false;
        }

        if ($user->isAdmin()) {
            return true;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update the campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function update(User $user, Campaign $campaign): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_CAMPAIGN)) {
            return false;
        }
        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        if (!$campaign->isEditable()) {
            return false;
        }

        if ($campaign->inProcessing()) {
            return false;
        }

        if ($campaign->inSuspended()) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can clone the campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function clone(User $user, Campaign $campaign): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_CLONE_CAMPAIGN)) {
            return false;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can order the campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function order(User $user, Campaign $campaign): bool
    {
        return $this->update($user, $campaign) && $user->isActive();
    }

    /**
     * Returns true if user can delete the campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function delete(User $user, Campaign $campaign): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_DELETE_CAMPAIGN)) {
            return false;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        if (!$campaign->isDeletable()) {
            return false;
        }

        if ($campaign->inProcessing()) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can cancel campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function cancel(User $user, Campaign $campaign): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_CANCEL_CAMPAIGN)) {
            return false;
        }

        if ($campaign->user->isDeactivated()) {
            return false;
        }

        if ($campaign->inProcessing()) {
            return false;
        }

        if ($user->isSuperAdmin()) {
            return true;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can pause campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function pause(User $user, Campaign $campaign): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_PAUSE_CAMPAIGN)) {
            return false;
        }

        if ($campaign->user->isDeactivated()) {
            return false;
        }

        if ($campaign->inProcessing()) {
            return false;
        }

        if (!$campaign->isLiveRange()) {
            return false;
        }

        if ($user->isSuperAdmin()) {
            return true;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can resume campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function resume(User $user, Campaign $campaign): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_RESUME_CAMPAIGN)) {
            return false;
        }

        if ($campaign->user->isDeactivated()) {
            return false;
        }

        if (!$campaign->isLiveRange() || !$campaign->isPaused()) {
            return false;
        }

        if ($campaign->inProcessing()) {
            return false;
        }

        // if changed by advertiser and current user is same advertiser -> allow
        if ($user->is($campaign->statusChangedBy)) {
            return true;
        }

        // if changed by full rights admin and current user is also full rights admin -> allow
        if ($user->isSuperAdmin() && optional($campaign->statusChangedBy)->isSuperAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * Returns true if user can attach/detach creative to/from campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function manageCampaignCreative(User $user, Campaign $campaign): bool
    {
        if (!$this->baseManageCreative($user, $campaign)) {
            return false;
        }

        if (!$campaign->isEditableCreative()) {
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function replaceCampaignCreative(User $user, Campaign $campaign): bool
    {
        if (!$this->baseManageCreative($user, $campaign)) {
            return false;
        }

        if ($campaign->isOnlyReplaceableCreative()) {
            return true;
        }

        return false;
    }

    /**
     * @param User      $user
     * @param User|null $advertiser
     *
     * @return bool
     */
    private function hasAccessToCampaigns(User $user, ?User $advertiser): bool
    {
        return $this->isAdmin($user) || $user->is($advertiser);
    }

    /**
     * @param User $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function baseManageCreative(User $user, Campaign $campaign): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_MANAGE_CAMPAIGN_CREATIVE)) {
            return false;
        }

        if ($user->isDeactivated() && !$campaign->inState(CampaignStatus::ID_DRAFT)) {
            return false;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        if ($campaign->inState(CampaignStatus::ID_PROCESSING)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can validate promocode in the campaign.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function validatePromocode(User $user, Campaign $campaign): bool
    {
        return $this->update($user, $campaign);
    }

    /**
     * Check if user can access age targetings.
     *
     * @param User $user
     *
     * @return bool
     */
    public function accessAgeTargetings(User $user): bool
    {
        return $user->canAccessTargeting(AgeGroup::TYPE_NAME);
    }

    /**
     * Check if user can access audience targetings.
     *
     * @param User $user
     *
     * @return bool
     */
    public function accessAudienceTargetings(User $user): bool
    {
        return $user->canAccessTargeting(Audience::TYPE_NAME);
    }

    /**
     * Check if user can access location targetings.
     *
     * @param User $user
     *
     * @return bool
     */
    public function accessLocationTargetings(User $user): bool
    {
        return $user->canAccessTargeting(Location::TYPE_NAME);
    }

    /**
     * Check if user can access zip targetings.
     *
     * @param User $user
     *
     * @return bool
     */
    public function accessZipTargetings(User $user): bool
    {
        return $user->canAccessTargeting(Zipcode::TYPE_NAME);
    }


    /**
     * Check if user can access genre targetings.
     *
     * @param User $user
     *
     * @return bool
     */
    public function accessGenreTargetings(User $user): bool
    {
        return $user->canAccessTargeting(Genre::TYPE_NAME);
    }

    /**
     * Check if user can send draft activity.
     *
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    public function accessActivity(User $user, Campaign $campaign): bool
    {
        if (!$this->update($user, $campaign)) {
            return false;
        }

        if ($user->isPendingManualReview() || $user->isInactive()) {
            return false;
        }

        return true;
    }

    public function downloadReport(User $user, Campaign $campaign):bool
    {
        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        // TODO Added check previous state
        // From Live -> Pause
        // From Live -> Canceled
        if ($campaign->inState([
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_COMPLETED,
            CampaignStatus::ID_PAUSED,
            CampaignStatus::ID_CANCELED,
            CampaignStatus::ID_PROCESSING,
            CampaignStatus::ID_SUSPENDED,
        ]) && !empty($campaign->order_id)) {
            return true;
        }

        return false;
    }
}
