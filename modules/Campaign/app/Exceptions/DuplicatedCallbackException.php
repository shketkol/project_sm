<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\BaseException;

class DuplicatedCallbackException extends BaseException
{
    /**
     * @var string
     */
    protected $logLevel = BaseException::LOG_LEVEL_WARNING;
}
