<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\ValidationWithActionException;
use Illuminate\Http\Request;
use Modules\Campaign\Models\Campaign;

class CampaignUsesNonVisibleTargetingException extends ValidationWithActionException
{
    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * ValidationWithActionException constructor.
     * @param Campaign $campaign
     * @param string $message
     */
    public function __construct(Campaign $campaign, string $message)
    {
        $this->campaign = $campaign;
        parent::__construct($message);
    }

    /**
     * @return string
     */
    public function getActionRouteName(): string
    {
        return 'api.campaigns.targeting.nonVisible.update';
    }

    /**
     * @return string
     */
    public function getActionHttpMethod(): string
    {
        return Request::METHOD_PATCH;
    }

    /**
     * @return array
     */
    public function getRequestParams(): array
    {
        return [
            'campaign' => $this->campaign->getKey(),
        ];
    }
}
