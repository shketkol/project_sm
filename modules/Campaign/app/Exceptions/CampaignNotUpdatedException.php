<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\ModelNotUpdatedException;
use Throwable;

class CampaignNotUpdatedException extends ModelNotUpdatedException
{
    /**
     * Create exception
     * @param string $message
     * @param \Throwable|null $previous
     * @return \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    public static function create(string $message = '', Throwable $previous = null): self
    {
        return new self($message, 0, $previous);
    }
}
