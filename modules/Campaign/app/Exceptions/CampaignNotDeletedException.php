<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\ModelNotDeletedException;

class CampaignNotDeletedException extends ModelNotDeletedException
{
    /**
     * @param string $message
     *
     * @return CampaignNotDeletedException
     */
    public static function create(string $message): self
    {
        return new self($message);
    }
}
