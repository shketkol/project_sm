<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\Response;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Exceptions\InvalidRequestException;

class InvalidCampaignActivityRequestException extends InvalidRequestException
{
    //
}
