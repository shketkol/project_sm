<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Campaign\Models\Campaign;

class CampaignExternalIdOverrideException extends BaseException
{
    /**
     * @inheritdoc
     */
    public const STATUS_CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * @param Campaign $campaign
     * @param string   $newExternalId
     *
     * @return self
     */
    public static function create(Campaign $campaign, string $newExternalId): self
    {
        return new self(sprintf(
            'Cannot update external_id ("%s") since Campaign #%d already has one ("%s").',
            $newExternalId,
            $campaign->id,
            $campaign->external_id
        ));
    }

    /**
     * Report the exception.
     *
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json([
            'message' => $this->message,
        ], self::STATUS_CODE);
    }
}
