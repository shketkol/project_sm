<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\Response;
use Modules\Campaign\Models\Campaign;

class CancelCampaignActivityRequestException extends BaseException
{
    /**
     * @inheritdoc
     */
    public const STATUS_CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * @param Campaign $campaign
     *
     * @return self
     */
    public static function create(Campaign $campaign): self
    {
        return new self(sprintf('Campaign #%d activity request was cancelled.', $campaign->id));
    }
}
