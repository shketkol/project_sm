<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\ModelNotCreatedException;

class CampaignNotCreatedException extends ModelNotCreatedException
{
    //
}
