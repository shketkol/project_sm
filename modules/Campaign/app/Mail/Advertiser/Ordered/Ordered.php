<?php

namespace Modules\Campaign\Mail\Advertiser\Ordered;

use App\Mail\Mail;

class Ordered extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.ordered.subject'))
            ->view('campaign::emails.advertiser.ordered')
            ->with($this->payload);
    }
}
