<?php

namespace Modules\Campaign\Mail\Advertiser\Resumed;

use App\Mail\Mail;

class CampaignResumedByAdvertiser extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->subject(__('campaign::emails.advertiser.resumed_by_advertiser.subject'))
            ->view('campaign::emails.advertiser.resumed-by-advertiser')
            ->with($this->payload);
    }
}
