<?php

namespace Modules\Campaign\Mail\Advertiser\Resumed;

use App\Mail\Mail;

class CampaignResumedByAdmin extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.resumed_by_admin.subject', [
                'publisher_company_name' => config('general.company_name')
            ]))
            ->view('campaign::emails.advertiser.resumed-by-admin')
            ->with($this->payload);
    }
}
