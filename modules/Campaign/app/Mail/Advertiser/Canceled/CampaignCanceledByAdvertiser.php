<?php

namespace Modules\Campaign\Mail\Advertiser\Canceled;

use App\Mail\Mail;
use Illuminate\Support\Arr;

class CampaignCanceledByAdvertiser extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.canceled_by_advertiser.subject', [
                'publisher_company_name' => config('general.company_name'),
                'campaign_name' => Arr::get($this->payload, 'campaignName')
            ]))
            ->view('campaign::emails.advertiser.canceled-by-advertiser')
            ->with($this->payload);
    }
}
