<?php

namespace Modules\Campaign\Mail\Advertiser;

use App\Mail\Mail;

class Started extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.started.subject'))
            ->view('campaign::emails.advertiser.started')
            ->with($this->payload);
    }
}
