<?php

namespace Modules\Campaign\Mail\Admin\Ordered;

use App\Mail\Mail;

class Ordered extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.admin.ordered.subject', [
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('campaign::emails.admin.ordered')
            ->with($this->payload);
    }
}
