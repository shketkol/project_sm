<?php

use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;

return [
    /**
     * Campaign workflow graph
     */
    'campaign' => [
        // class of your domain object
        'class'         => Campaign::class,

        // name of the graph (default is "default")
        'graph'         => 'campaign',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status_id',

        // list of all possible states
        'states'        => [
            [
                'name' => CampaignStatus::ID_DRAFT,
            ],
            [
                'name' => CampaignStatus::ID_PENDING_APPROVAL,
            ],
            [
                'name' => CampaignStatus::ID_READY,
            ],
            [
                'name' => CampaignStatus::ID_LIVE,
            ],
            [
                'name' => CampaignStatus::ID_PAUSED,
            ],
            [
                'name' => CampaignStatus::ID_COMPLETED,
            ],
            [
                'name' => CampaignStatus::ID_CANCELED,
            ],
            [
                'name' => CampaignStatus::ID_PROCESSING,
            ],
            [
                'name' => CampaignStatus::ID_SUSPENDED,
            ],
        ],

        // list of all possible transitions
        'transitions'   => [
            CampaignStatus::DRAFT            => [
                'from' => [CampaignStatus::ID_PROCESSING],
                'to'   => CampaignStatus::ID_DRAFT,
            ],
            CampaignStatus::PROCESSING       => [
                'from' => [
                    CampaignStatus::ID_DRAFT,
                    CampaignStatus::ID_READY,
                    CampaignStatus::ID_PENDING_APPROVAL,
                    CampaignStatus::ID_PAUSED,
                    CampaignStatus::ID_LIVE,
                ],
                'to'   => CampaignStatus::ID_PROCESSING,
            ],
            CampaignStatus::PENDING_APPROVAL => [
                'from' => [CampaignStatus::ID_DRAFT, CampaignStatus::ID_PROCESSING],
                'to'   => CampaignStatus::ID_PENDING_APPROVAL,
            ],
            CampaignStatus::READY            => [
                'from' => [
                    CampaignStatus::ID_PENDING_APPROVAL,
                    CampaignStatus::ID_PAUSED,
                    CampaignStatus::ID_PROCESSING,
                    CampaignStatus::ID_SUSPENDED,
                ],
                'to'   => CampaignStatus::ID_READY,
            ],
            CampaignStatus::LIVE             => [
                'from' => [
                    CampaignStatus::ID_READY,
                    CampaignStatus::ID_PAUSED,
                    CampaignStatus::ID_PROCESSING,
                    CampaignStatus::ID_PENDING_APPROVAL,
                    CampaignStatus::ID_SUSPENDED,
                ],
                'to'   => CampaignStatus::ID_LIVE,
            ],
            CampaignStatus::PAUSED           => [
                'from' => [CampaignStatus::ID_LIVE, CampaignStatus::ID_READY, CampaignStatus::ID_PROCESSING],
                'to'   => CampaignStatus::ID_PAUSED,
            ],
            CampaignStatus::COMPLETED        => [
                'from' => [
                    CampaignStatus::ID_PENDING_APPROVAL,
                    CampaignStatus::ID_LIVE,
                    CampaignStatus::ID_PAUSED,
                    CampaignStatus::ID_SUSPENDED,
                ],
                'to'   => CampaignStatus::ID_COMPLETED,
            ],
            CampaignStatus::CANCELED         => [
                'from' => [
                    CampaignStatus::ID_PROCESSING,
                    CampaignStatus::ID_PENDING_APPROVAL,
                    CampaignStatus::ID_READY,
                    CampaignStatus::ID_LIVE,
                    CampaignStatus::ID_PAUSED,
                ],
                'to'   => CampaignStatus::ID_CANCELED,
            ],
            CampaignStatus::SUSPENDED        => [
                'from' => [
                    CampaignStatus::ID_READY,
                    CampaignStatus::ID_LIVE,
                ],
                'to'   => CampaignStatus::ID_SUSPENDED,
            ],
        ],

        // list of all callbacks
        'callbacks'     => [
            // will be called when testing a transition
            'guard'  => [],

            // will be called before applying a transition
            'before' => [
                'before_live' => [
                    // call the callback on a specific transition
                    'on'   => CampaignStatus::LIVE,
                    // will call the method of this class
                    'do'   => ['state-machine.campaign.states.live', 'before'],
                    // arguments for the callback
                    'args' => ['object'],
                ],
            ],

            // will be called after applying a transition
            'after'  => [
                'after_completed' => [
                    // call the callback on a specific transition
                    'on'   => CampaignStatus::COMPLETED,
                    // will call the method of this class
                    'do'   => ['state-machine.campaign.states.completed', 'after'],
                    // arguments for the callback
                    'args' => ['object'],
                ],

                'after_status_changed' => [
                    // will call the method of this class
                    'do'   => ['state-machine.campaign.states.status-changed', 'after'],
                    // arguments for the callback
                    'args' => ['object'],
                ],
            ],
        ],
    ],
];
