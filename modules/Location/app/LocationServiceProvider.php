<?php

namespace Modules\Location;

use App\Providers\ModuleServiceProvider;

class LocationServiceProvider extends ModuleServiceProvider
{
    /**
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'location';
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadRoutes();
        $this->loadConfigs(['location']);
    }
}
