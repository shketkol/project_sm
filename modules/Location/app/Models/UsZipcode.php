<?php

namespace Modules\Location\Models;

use App\Models\Traits\BelongsToManyUsStates;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property string $name
 */
class UsZipcode extends Model
{
    use BelongsToManyUsStates;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
