<?php

namespace Modules\Location\Models;

use App\Models\Traits\BelongsToManyUsZipcodes;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property string $name
 */
class UsState extends Model
{
    use BelongsToManyUsZipcodes;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
