<?php

namespace Modules\Location\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Location\Actions\SearchZipcodesAction;
use Modules\Location\Http\Requests\SearchZipcodesRequest;
use Modules\Location\Http\Resources\UsZipcodeResource;

class SearchZipcodesController extends Controller
{
    /**
     * @param SearchZipcodesAction  $action
     * @param SearchZipcodesRequest $request
     *
     * @return JsonResponse
     */
    public function __invoke(SearchZipcodesAction $action, SearchZipcodesRequest $request): JsonResponse
    {
        $query = $request->get('query') ?: '';
        $state = $request->get('state') ?: '';

        return UsZipcodeResource::collection($action->handle($query, $state))->response();
    }
}
