<?php

namespace Modules\Location\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \Modules\Location\Models\UsZipcode
 */
class UsZipcodeResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'zip_code' => $this->name,
            'states'   => UsStateResource::collection($this->whenLoaded('states')),
        ];
    }
}
