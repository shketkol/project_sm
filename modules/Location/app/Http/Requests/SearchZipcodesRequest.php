<?php

namespace Modules\Location\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\ValidationRules;

class SearchZipcodesRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'query' => $validationRules->only(
                'location.zipcode.query',
                ['required', 'string', 'regex', 'min', 'max']
            ),
            'state' => $validationRules->only(
                'location.state',
                ['string', 'exists', 'min', 'max']
            ),
        ];
    }
}
