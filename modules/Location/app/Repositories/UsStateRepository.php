<?php

namespace Modules\Location\Repositories;

use App\Repositories\Repository;
use Modules\Location\Models\UsState;

class UsStateRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return UsState::class;
    }
}
