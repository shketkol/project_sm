<?php

namespace Modules\Location\Repositories;

use App\Repositories\Repository;
use Modules\Location\Models\UsZipcode;

class UsZipcodeRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return UsZipcode::class;
    }
}
