<?php

namespace Modules\Location\Actions;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Modules\Location\Repositories\UsZipcodeRepository;
use Psr\Log\LoggerInterface;

class SearchZipcodesAction
{
    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var UsZipcodeRepository
     */
    protected $repository;

    /**
     * @param UsZipcodeRepository $repository
     * @param LoggerInterface     $log
     */
    public function __construct(UsZipcodeRepository $repository, LoggerInterface $log)
    {
        $this->repository = $repository;
        $this->log = $log;
    }

    /**
     * @param string $query
     * @param string $state
     *
     * @return Collection
     */
    public function handle(string $query, string $state): Collection
    {
        $this->log->info('Searching us_zipcode.', [
            'query' => $query,
        ]);

        $builder = $this->repository->with('states');

        if ($state) {
            $builder->whereHas('states', function (Builder $query) use ($state): Builder {
                return $query->where('name', $state);
            });
        }

        $result = $builder->findWhere([
            ['name', 'like', $query . '%'],
        ])->take(config('location.search.limit'));

        $this->log->info('Found us_zipcode values.', [
            'query' => $query,
            'count' => $result->count(),
        ]);

        return $result;
    }
}
