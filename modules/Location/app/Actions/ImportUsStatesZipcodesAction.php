<?php

namespace Modules\Location\Actions;

use App\Exceptions\BaseException;
use Modules\Location\Models\UsState;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\DB;
use Psr\Log\LoggerInterface;

class ImportUsStatesZipcodesAction
{
    /**
     * @var int
     */
    private const BATCH_SIZE = 1000;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param DatabaseManager $databaseManager
     * @param LoggerInterface $log
     */
    public function __construct(DatabaseManager $databaseManager, LoggerInterface $log)
    {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
    }

    /**
     * Format:
     * [
     *     'NY' => ['00501', '00544', '06390'],
     *     ...
     * ]
     *
     * @param array<string, array> $data
     *
     * @throws \Throwable
     */
    public function handle(array $data): void
    {
        $this->log->info('Started import us_states, us_zipcodes and us_states_zipcodes tables.');

        if (empty($data)) {
            $this->log->info('Skipped import. No data were provided.');
            return;
        }

        $this->databaseManager->beginTransaction();

        try {
            $zipcodes = call_user_func_array('array_merge', $data);
            $this->importZipcodes(...$zipcodes);
            $this->syncStatesAndZipcodes($data);
        } catch (\Throwable $exception) {
            $this->log->warning('Import failed.', ['reason' => $exception->getMessage()]);
            $this->databaseManager->rollBack();
            throw BaseException::createFrom($exception);
        }

        $this->databaseManager->commit();
        $this->log->info('Finished import us_states, us_zipcodes and us_states_zipcodes tables.');
    }

    /**
     * @param array $data
     */
    private function syncStatesAndZipcodes(array $data): void
    {
        $this->log->info('Started sync us_states_zipcodes table.');

        foreach ($data as $state => $zipcodes) {
            $state = $this->findOrCreateState($state);
            $ids = $this->findZipcodeIds($zipcodes);
            $state->zipcodes()->sync($ids);
        }

        $this->log->info('Finished sync us_states_zipcodes table.');
    }

    /**
     * @param array $zipcodes
     *
     * @return array
     */
    private function findZipcodeIds(array $zipcodes): array
    {
        $ids = [];

        $total = count($zipcodes);
        if ($total === 0) {
            return $ids;
        }

        for ($counter = 0; $counter <= $total; $counter += self::BATCH_SIZE) {
            $slice = array_slice($zipcodes, $counter, self::BATCH_SIZE);
            $idsFromDB = $this->findZipcodesByName('id', ...$slice);

            array_push($ids, ...$idsFromDB);
        }

        return $ids;
    }

    /**
     * @param string   $column
     * @param string[] $zipcodes
     *
     * @return array
     */
    private function findZipcodesByName(string $column, string ...$zipcodes): array
    {
        return DB::table('us_zipcodes')
            ->select([$column])
            ->whereIn('name', $zipcodes)
            ->get()
            ->pluck($column)
            ->toArray();
    }

    /**
     * @param string $name
     *
     * @return UsState
     */
    private function findOrCreateState(string $name): UsState
    {
        return UsState::firstOrCreate(['name' => $name]);
    }

    /**
     * @param string[] $zipcodes
     */
    private function importZipcodes(string ...$zipcodes): void
    {
        $total = count($zipcodes);

        $this->log->info('Started import us_zipcodes table.', [
            'total' => $total,
        ]);

        if ($total === 0) {
            $this->log->info('Skipped import us_zipcodes table. No data were provided.');
            return;
        }

        for ($counter = 0; $counter <= $total; $counter += self::BATCH_SIZE) {
            $slice = array_slice($zipcodes, $counter, self::BATCH_SIZE);
            $namesFromDB = $this->findZipcodesByName('name', ...$slice);

            $diff = array_diff($slice, $namesFromDB);
            $this->insertZipcodes(...$diff);
        }

        $this->log->info('Finished import us_zipcodes table.', [
            'total' => $total,
        ]);
    }

    /**
     * @param string ...$zipcodes
     */
    private function insertZipcodes(string ...$zipcodes): void
    {
        $data = array_map(function (string $zipcode): array {
            return ['name' => $zipcode];
        }, $zipcodes);

        DB::table('us_zipcodes')->insert($data);

        $this->log->info('Imported data into us_zipcodes table.', [
            'count' => count($data),
        ]);
    }
}
