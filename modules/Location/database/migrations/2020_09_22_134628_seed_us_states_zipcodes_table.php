<?php

use Modules\Location\Actions\ImportUsStatesZipcodesAction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class SeedUsStatesZipcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up(): void
    {
        $data = require base_path('modules/Location/database/states_zipcodes.php');

        /** @var ImportUsStatesZipcodesAction $action */
        $action = app(ImportUsStatesZipcodesAction::class);
        $action->handle($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('us_zipcodes')->delete();
        DB::table('us_states_zipcodes')->delete();
    }
}
