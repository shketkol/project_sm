<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsStatesZipcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('us_states_zipcodes', function (Blueprint $table) {
            $table->primary(['zipcode_id', 'state_id']);

            $table->unsignedInteger('zipcode_id')->unsigned();
            $table->foreign('zipcode_id')
                ->references('id')
                ->on('us_zipcodes')
                ->onDelete('cascade');

            $table->unsignedInteger('state_id')->unsigned();
            $table->foreign('state_id')
                ->references('id')
                ->on('us_states')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('us_states_zipcodes');
    }
}
