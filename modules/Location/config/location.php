<?php

return [
    'search' => [
        'limit' => env('LOCATION_SEARCH_LIMIT', 50),
    ],
];
