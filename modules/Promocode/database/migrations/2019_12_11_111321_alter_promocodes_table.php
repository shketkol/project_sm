<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('promo_codes', function (Blueprint $table) {
            $table->string('type')->after('code');
            $table->float('value')->after('type');

            $table->unique('code');

            if (Schema::hasColumn('promo_codes', 'discount')) {
                $table->dropColumn('discount');
            }

            if (Schema::hasColumn('promo_codes', 'date_start')) {
                $table->dropColumn('date_start');
            }

            if (Schema::hasColumn('promo_codes', 'date_end')) {
                $table->dropColumn('date_end');
            }

            if (Schema::hasColumn('promo_codes', 'limit')) {
                $table->dropColumn('limit');
            }

            if (Schema::hasColumn('promo_codes', 'active')) {
                $table->dropColumn('active');
            }

            if (Schema::hasColumn('promo_codes', 'min_purchase')) {
                $table->dropColumn('min_purchase');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('promo_codes', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('value');
            $table->dropUnique(['code']);
        });
    }
}
