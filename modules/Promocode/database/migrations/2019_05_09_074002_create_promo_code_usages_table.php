<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocodeUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('promo_code_usages')) {
            Schema::create('promo_code_usages', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('promo_code_id')->unsigned()->nullable();
                $table->foreign('promo_code_id')->references('id')->on('promo_codes')->onDelete('cascade');
                $table->bigInteger('company_id')->unsigned()->nullable();
                $table->foreign('company_id')->references('id')->on('user_companies')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_code_usages');
    }
}
