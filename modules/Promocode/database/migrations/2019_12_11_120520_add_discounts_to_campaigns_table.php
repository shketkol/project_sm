<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountsToCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->float('discounted_cpm')->after('cpm')->nullable();
            $table->float('discounted_cost')->after('cost')->nullable();

            $table->unsignedBigInteger('promocode_id')->nullable();
            $table->foreign('promocode_id')
                ->references('id')
                ->on('promo_codes')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn('discounted_cpm');
            $table->dropColumn('discounted_cost');
            $table->dropForeign(['promocode_id']);
            $table->dropColumn('promocode_id');
        });
    }
}
