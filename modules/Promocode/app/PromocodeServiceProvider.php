<?php

namespace Modules\Promocode;

use App\Providers\ModuleServiceProvider;

class PromocodeServiceProvider extends ModuleServiceProvider
{
    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'promocode';
    }

    /**
     * Boot provider.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadTranslations();
        $this->loadRoutes();
    }
}
