<?php

namespace Modules\Promocode\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;
use Modules\Promocode\Actions\PromocodeValidateAction;
use Modules\Promocode\Http\Requests\PromocodeValidateRequest;

class PromocodeValidateController extends Controller
{
    /**
     * @param Campaign                 $campaign
     * @param Guard                    $auth
     * @param PromocodeValidateRequest $request
     * @param PromocodeValidateAction  $action
     *
     * @return CampaignResource
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignNotOrderedException
     * @throws \Modules\Promocode\Exceptions\PromocodeCouldNotBeAppliedException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \Throwable
     */
    public function __invoke(
        Campaign $campaign,
        Guard $auth,
        PromocodeValidateRequest $request,
        PromocodeValidateAction $action
    ) {
        $action->handle($campaign, $auth->user(), $request->get('promocode'));

        return new CampaignResource($campaign);
    }
}
