<?php

namespace Modules\Promocode\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Promocode\Models\Promocode;

/**
 * @mixin Promocode
 */
class PromocodeResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'    => $this->id,
            'code'  => $this->code,
            'type'  => $this->type,
            'value' => $this->value,
        ];
    }
}
