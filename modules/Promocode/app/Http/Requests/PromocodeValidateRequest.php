<?php

namespace Modules\Promocode\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class PromocodeValidateRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'promocode' => $validationRules->only(
                'campaign.promocode',
                ['required', 'string', 'min', 'max']
            ),
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'promocode.required' => __('validation.promocode.required'),
        ];
    }
}
