<?php

namespace Modules\Promocode\Listeners;

use Modules\Campaign\Events\CampaignStepUpdated;
use Modules\Promocode\Actions\DiscardPromocodeAction;

class DiscardPromocodeListener
{
    /**
     * @var DiscardPromocodeAction
     */
    private $discardPromocodeAction;

    /**
     * @param DiscardPromocodeAction $discardPromocodeAction
     */
    public function __construct(DiscardPromocodeAction $discardPromocodeAction)
    {
        $this->discardPromocodeAction = $discardPromocodeAction;
    }

    /**
     * Handle the event.
     *
     * @param CampaignStepUpdated $event
     *
     * @throws \Throwable
     */
    public function handle(CampaignStepUpdated $event): void
    {
        $this->discardPromocodeAction->handle($event->campaign);
    }
}
