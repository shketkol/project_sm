<?php

namespace Modules\Promocode\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Promocode\Exceptions\PromocodeCouldNotBeAppliedException;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\PromoCode\PromocodeValidate;
use Modules\Haapi\DataTransferObjects\Promocode\PromocodeValidateData;
use Modules\Haapi\DataTransferObjects\Promocode\PromocodeValidateParams;
use Modules\Promocode\Models\Promocode;
use Modules\Promocode\Repositories\PromocodeRepository;
use Modules\Targeting\Actions\GetCampaignTargetingsAction;
use Modules\User\Models\User;

class PromocodeValidateAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var PromocodeValidate
     */
    private $promocodeValidate;

    /**
     * @var GetCampaignTargetingsAction
     */
    private $getTargetingsAction;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var PromocodeRepository
     */
    private $promocodeRepository;

    /**
     * @param Logger                      $log
     * @param PromocodeValidate           $promocodeValidate
     * @param GetCampaignTargetingsAction $getTargetingsAction
     * @param DatabaseManager             $databaseManager
     * @param PromocodeRepository         $promocodeRepository
     */
    public function __construct(
        Logger $log,
        PromocodeValidate $promocodeValidate,
        GetCampaignTargetingsAction $getTargetingsAction,
        DatabaseManager $databaseManager,
        PromocodeRepository $promocodeRepository
    ) {
        $this->log = $log;
        $this->promocodeValidate = $promocodeValidate;
        $this->getTargetingsAction = $getTargetingsAction;
        $this->databaseManager = $databaseManager;
        $this->promocodeRepository = $promocodeRepository;
    }

    /**
     * @param Campaign             $campaign
     * @param User|Authenticatable $user
     * @param string               $promocode
     *
     * @throws PromocodeCouldNotBeAppliedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignNotOrderedException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \Throwable
     */
    public function handle(Campaign $campaign, User $user, string $promocode)
    {
        $this->log->info('Started validating promocode.', [
            'campaign_id' => $campaign->id,
            'promocode'   => $promocode,
        ]);

        $this->checkPromocodeCanBeApplied($campaign);
        $params = $this->createParamsFromPromocode($campaign, $user, $promocode);
        $promocodeData = $this->haapiValidate($params, $user);
        $this->applyPromocodeToCampaign($campaign, $promocode, $promocodeData);

        $this->log->info('Finished validating promocode.', [
            'campaign_id' => $campaign->id,
            'promocode'   => $promocode,
        ]);
    }

    /**
     * @param Campaign $campaign
     *
     * @return bool
     * @throws PromocodeCouldNotBeAppliedException
     */
    private function checkPromocodeCanBeApplied(Campaign $campaign): bool
    {
        $this->log->info('Checking if campaign is valid for promocode to be applied.', [
            'campaign_id' => $campaign->id,
        ]);

        if (!$campaign->promocodeCanBeApplied()) {
            $this->log->warning('Campaign is not valid for promocode to be applied.', [
                'campaign_id' => $campaign->id,
            ]);
            throw PromocodeCouldNotBeAppliedException::create($campaign);
        }

        $this->log->info('Campaign is valid for promocode to be applied.', [
            'campaign_id' => $campaign->id,
        ]);

        return true;
    }

    /**
     * @param Campaign $campaign
     * @param User     $user
     * @param string   $promocode
     *
     * @return PromocodeValidateParams
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    private function createParamsFromPromocode(
        Campaign $campaign,
        User $user,
        string $promocode
    ): PromocodeValidateParams {
        $this->log->info('Creating params for haapi promocode validate request.', [
            'campaign_id' => $campaign->id,
            'promocode'   => $promocode,
        ]);

        return new PromocodeValidateParams([
            'accountId'     => $user->getExternalId(),
            'startDate'     => $campaign->startDateWithTimezone->format(config('date.format.daapi')),
            'endDate'       => $campaign->endDateWithTimezone->format(config('date.format.daapi')),
            'quantity'      => $campaign->impressions,
            'total'         => $campaign->budget,
            'unitCost'      => $campaign->cpm,
            'promoCodeName' => $promocode,
            'targets'       => $this->getTargetingsAction->handle($campaign),
            'displayBudget' => $campaign->budget,
        ]);
    }

    /**
     * @param PromocodeValidateParams $params
     * @param User                    $user
     *
     * @return PromocodeValidateData
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    private function haapiValidate(PromocodeValidateParams $params, User $user): PromocodeValidateData
    {
        $this->log->info('Sending haapi promocode validate request.');

        return $this->promocodeValidate->handle($params, $user->id);
    }

    /**
     * @param Campaign              $campaign
     * @param string                $code
     * @param PromocodeValidateData $data
     *
     * @throws \Throwable
     */
    private function applyPromocodeToCampaign(Campaign $campaign, string $code, PromocodeValidateData $data): void
    {
        $this->log->info('Started applying promocode to campaign.', [
            'campaign_id' => $campaign->id,
            'promocode'   => $code,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $promocode = $this->findOrCreatePromocode($code, $data);
            $this->savePromocodeToCampaign($promocode, $campaign);
            $this->addDiscountsToCampaign($campaign, $data);
        } catch (\Throwable $exception) {
            $message = 'Failed applying promocode to campaign.';
            $this->log->warning($message, [
                'campaign_id' => $campaign->id,
                'promocode'   => $code,
                'reason'      => $exception->getMessage() ?? get_class($exception),
            ]);
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->log->info('Finished applying promocode to campaign.', [
            'campaign_id' => $campaign->id,
            'promocode'   => $code,
        ]);

        $this->databaseManager->commit();
    }

    /**
     * @param string                $code
     * @param PromocodeValidateData $data
     *
     * @return Promocode
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function findOrCreatePromocode(string $code, PromocodeValidateData $data): Promocode
    {
        $this->log->info('Searching promocode in DB.', [
            'promocode' => $code,
        ]);
        $promocode = $this->promocodeRepository->byCode($code)->first();

        if (!is_null($promocode)) {
            $this->log->info('Promocode found in DB.', [
                'promocode'    => $code,
                'promocode_id' => $promocode->id,
            ]);

            return $promocode;
        }

        $this->log->info('Promocode not found in DB. Creating a new one.', [
            'promocode' => $code,
        ]);

        $promocode = $this->promocodeRepository->create([
            'code'  => $code,
            'type'  => $data->discountType,
            'value' => $data->discountValue,
        ]);

        $this->log->info('Promocode created.', [
            'promocode'    => $code,
            'promocode_id' => $promocode->id,
        ]);

        return $promocode;
    }

    /**
     * @param Campaign              $campaign
     * @param PromocodeValidateData $data
     */
    private function addDiscountsToCampaign(Campaign $campaign, PromocodeValidateData $data): void
    {
        $this->log->info('Started saving discounted prices to campaign.', [
            'campaign_id' => $campaign->id,
        ]);

        $campaign->discounted_cost = $data->displayDiscountedBudget;
        $campaign->discounted_cpm = $data->discountedUnitCost;
        $campaign->save();

        $this->log->info('Finished saving discounted prices to campaign.', [
            'campaign_id' => $campaign->id,
        ]);
    }

    /**
     * @param Promocode $promocode
     * @param Campaign  $campaign
     */
    private function savePromocodeToCampaign(Promocode $promocode, Campaign $campaign): void
    {
        $this->log->info('Started saving promocode to campaign.', [
            'promocode_id' => $promocode->id,
            'campaign_id'  => $campaign->id,
        ]);

        $promocode->campaigns()->save($campaign);

        $this->log->info('Finished saving promocode to campaign.', [
            'promocode_id' => $promocode->id,
            'campaign_id'  => $campaign->id,
        ]);
    }
}
