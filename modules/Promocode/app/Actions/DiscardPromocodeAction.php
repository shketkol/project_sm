<?php

namespace Modules\Promocode\Actions;

use Illuminate\Log\Logger;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;

class DiscardPromocodeAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * @param Campaign $campaign
     *
     * @throws \Throwable
     */
    public function handle(Campaign $campaign)
    {
        $this->log->info('Started removing promocode.', [
            'campaign_id' => $campaign->id,
        ]);

        $this->discardPromocodeFromCampaign($campaign);

        $this->log->info('Finished removing promocode.', [
            'campaign_id' => $campaign->id,
        ]);
    }

    /**
     * @param Campaign $campaign
     */
    private function discardPromocodeFromCampaign(Campaign $campaign): void
    {
        if (is_null($campaign->promocode_id)) {
            $this->log->info('Campaign does not have promocode.', [
                'campaign_id' => $campaign->id,
            ]);
            return;
        }

        if ($campaign->status_id !== CampaignStatus::ID_DRAFT) {
            $this->log->info('Campaign is not in DRAFT state, and cannot be editable', [
                'campaign_id' => $campaign->id,
            ]);
            return;
        }

        $campaign->promocode_id = null;
        $campaign->discounted_cost = null;
        $campaign->discounted_cpm = null;
        $campaign->save();
    }
}
