<?php

namespace Modules\Promocode\Exceptions;

use App\Exceptions\BaseException;
use Modules\Campaign\Models\Campaign;

class PromocodeCouldNotBeAppliedException extends BaseException
{
    /**
     * @param Campaign $campaign
     *
     * @return self
     */
    public static function create(Campaign $campaign): self
    {
        return new self(sprintf(
            'Promocode can not be applied to Campaign#%d. Required fields are empty: %s.',
            $campaign->id,
            implode(', ', $campaign->getInvalidCampaignFieldsForPromocode())
        ));
    }
}
