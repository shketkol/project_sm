<?php

namespace Modules\Promocode\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class PromocodeCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private $promocode;

    /**
     * @param string $promocode
     */
    public function __construct(string $promocode)
    {
        $this->promocode = $promocode;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where('code', $this->promocode);
    }
}
