<?php

namespace Modules\Promocode\Repositories;

use App\Repositories\Repository;
use Modules\Promocode\Models\Promocode;
use Modules\Promocode\Repositories\Criteria\PromocodeCriteria;

class PromocodeRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Promocode::class;
    }

    /**
     * @param string $promocode
     *
     * @return PromocodeRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byCode(string $promocode): self
    {
        return $this->pushCriteria(new PromocodeCriteria($promocode));
    }
}
