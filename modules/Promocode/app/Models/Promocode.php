<?php

namespace Modules\Promocode\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Models\Traits\HasManyCampaigns;

/**
 * @property integer        $id
 * @property string         $code
 * @property string         $type
 * @property integer        $value
 * @property string         $external_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Promocode extends Model
{
    use HasManyCampaigns;

    /**
     * @var string
     */
    protected $table = 'promo_codes';

    /**
     * @var array
     */
    protected $fillable = [
        'code',
        'type',
        'value',
        'external_id'
    ];
}
