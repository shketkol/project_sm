<?php

namespace Modules\Promocode\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Promocode\Models\Promocode;

/**
 * @property Promocode|null $promocode
 */
trait BelongsToPromocode
{
    /**
     * @return BelongsTo
     */
    public function promocode(): BelongsTo
    {
        return $this->belongsTo(Promocode::class);
    }

    /**
     * @return bool
     */
    public function promocodeCanBeApplied(): bool
    {
        return count($this->getInvalidCampaignFieldsForPromocode()) === 0;
    }

    /**
     * @return array
     */
    public function getInvalidCampaignFieldsForPromocode(): array
    {
        $required = [
            'date_start',
            'date_end',
            'impressions',
        ];

        return array_filter($required, function (string $field): bool {
            return empty($this->{$field});
        });
    }

    /**
     * @return bool
     */
    public function hasPromocode(): bool
    {
        return !is_null($this->promocode_id);
    }

    /**
     * Get discount money after promocode applying.
     *
     * @return float
     */
    public function getDiscountMoney(): float
    {
        if (!$this->hasPromocode()) {
            return 0;
        }
        return $this->budget - $this->discounted_cost;
    }
}
