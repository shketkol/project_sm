<?php

return [
    'applied_promocode' => 'The <span>:promocode_name</span> code was applied.',
    'invalid_promocode' => 'Promo code is invalid.',
];
