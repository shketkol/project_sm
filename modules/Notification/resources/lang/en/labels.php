<?php

use Modules\Notification\Models\EmailIcon;
use Modules\Notification\Models\NotificationCategory;

return [
    'alerts'                  => 'Alerts',
    'notifications'           => 'Notifications',
    'view_all'                => 'View all notifications',
    'view_notifications'      => 'View notifications',
    'read_all'                => 'Mark all as read',
    'table_title'             => 'Notifications',
    'notification'            => 'Notification',
    'done'                    => 'Done',
    'cancel'                  => 'Cancel',
    'notification_category'   => 'Category',
    'notification_categories' => [
        NotificationCategory::ACCOUNT     => 'Account',
        NotificationCategory::REPORT      => 'Report',
        NotificationCategory::CAMPAIGN    => 'Campaign',
        NotificationCategory::CREATIVE    => 'Ad',
        NotificationCategory::TRANSACTION => 'Transaction',
    ],
    'email_icons' => [
        EmailIcon::TYPE_CANCEL_WINDOW   => 'Cancel Window',
        EmailIcon::TYPE_CASH            => 'Cash',
        EmailIcon::TYPE_FINISH          => 'Finish',
        EmailIcon::TYPE_LOCK            => 'Lock',
        EmailIcon::TYPE_MAIL            => 'Mail',
        EmailIcon::TYPE_MARKED          => 'Marked',
        EmailIcon::TYPE_NOTICE          => 'Notice',
        EmailIcon::TYPE_NOTICE_WINDOW   => 'Notice Window',
        EmailIcon::TYPE_PAUSE           => 'Pause',
        EmailIcon::TYPE_PLAY            => 'Play',
        EmailIcon::TYPE_QUESTION_WINDOW => 'Question Window',
        EmailIcon::TYPE_REJECTED        => 'Rejected',
        EmailIcon::TYPE_SCHEDULE        => 'Schedule',
        EmailIcon::TYPE_THUMBS_UP       => 'Thumbs Up'
    ],
];
