<?php

return [
    'no_notifications'     => 'No notifications right now!',
    'read_error'           => 'Notification was not marked as read',
    'marked_as_read'       => 'All messages have been marked as read.',
    'reminder_was_removed' => 'Reminder was removed',
];
