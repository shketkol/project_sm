@extends('layouts.app')
@section('content')
    <div class="vue-app">
        <notifications-page />
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/notification.js') }}"></script>
@endpush
