<?php

Route::pattern('notification', '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}');

Route::group([
    'middleware' => ['auth', 'can:notification.list'],
], function () {
    // Widget
    Route::get('/widget', 'NotificationWidgetController')->name('widget');

    // Mark all notification as read
    Route::patch('read-all', 'MarkAllReadNotificationController')->name('read.all');

    // Mark widget as read
    Route::patch('user-notified', 'UserNotifiedController')->name('user-notified');

    // Delete reminder
    Route::delete('/delete-reminder/{reminder}', 'DeleteReminderController')
         ->name('delete-reminder')
         ->middleware('can:reminder.delete,reminder');

    // Specified resource
    Route::group([
        'prefix'     => '/{notification}',
        'middleware' => 'can:notification.view,notification',
    ], function () {
        // Details
        Route::get('', 'ShowNotificationController')->name('show');

        // Mark read
        Route::patch('read', 'MarkReadNotificationController')->name('read');
    });
});
