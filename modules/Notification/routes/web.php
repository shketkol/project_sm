<?php

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'IndexNotificationController')->name('index')->middleware('can:notification.list');
});
