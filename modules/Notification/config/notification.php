<?php

return [
    /*
    |--------------------------------------------------------------------------
    | BCC emails (comma-separated, whitespaces are allowed) for failed payment alerts.
    |--------------------------------------------------------------------------
    */
    'failed_payment_bcc' => env('FAILED_PAYMENT_BCC'),

    /*
    |--------------------------------------------------------------------------
    | CC emails (comma-separated, whitespaces are allowed) for failed payment alerts.
    |--------------------------------------------------------------------------
    */
    'failed_payment_cc'  => env('FAILED_PAYMENT_CC'),
];
