<?php

namespace Modules\Notification\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property string $notifiable_type
 * @property int    $notifiable_id
 * @property string $mailable_type
 * @property int    $mailable_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class EmailHistory extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'email_history';

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'notifiable_type',
        'notifiable_id',
        'mailable_type',
        'mailable_id',
    ];
}
