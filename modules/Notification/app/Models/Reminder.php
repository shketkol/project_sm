<?php

namespace Modules\Notification\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Notification\Models\Traits\BelongsToReminderType;

/**
 * @property integer      $id
 * @property integer      $type_id
 * @property string       $user_id
 * @property array        $data
 * @property ReminderType $reminderType
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Reminder extends Model
{
    use BelongsToReminderType;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'type_id', 'data'];
}
