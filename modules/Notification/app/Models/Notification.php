<?php

namespace Modules\Notification\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\HasFactory;
use Illuminate\Notifications\DatabaseNotification;
use Modules\Notification\Models\Traits\GetNotificationAttributes;
use Modules\Notification\Models\Traits\BelongsToNotificationCategory;
use Modules\User\Models\User;

/**
 * @property string $id
 * @property string $notification_category_id
 * @property string $type
 * @property int    $type_id
 * @property string $notifiable_type
 * @property int    $notifiable_id
 * @property array  $data
 * @property Carbon $read_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User   $notifiable
 * @mixin Builder
 */
class Notification extends DatabaseNotification
{
    use GetNotificationAttributes,
        BelongsToNotificationCategory,
        HasFactory;
}
