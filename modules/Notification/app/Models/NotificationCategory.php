<?php

namespace Modules\Notification\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string  $name
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @mixin Builder
 */
class NotificationCategory extends Model
{
    /**
     * Default categories.
     */
    public const ACCOUNT     = 'account';
    public const REPORT      = 'report';
    public const CAMPAIGN    = 'campaign';
    public const CREATIVE    = 'ad';
    public const TRANSACTION = 'transaction';

    public const ID_ACCOUNT     = 1;
    public const ID_REPORT      = 2;
    public const ID_CAMPAIGN    = 3;
    public const ID_CREATIVE    = 4;
    public const ID_TRANSACTION = 5;

    /**
     * @var array
     */
    protected $fillable = ['name'];
}
