<?php

namespace Modules\Notification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 */
class EmailIcon extends Model
{
    public const TYPE_CANCEL_WINDOW = 'cancel_window';
    public const TYPE_CASH = 'cash';
    public const TYPE_FINISH = 'finish';
    public const TYPE_LOCK = 'lock';
    public const TYPE_MAIL = 'mail';
    public const TYPE_MARKED = 'marked';
    public const TYPE_NOTICE = 'notice';
    public const TYPE_NOTICE_WINDOW = 'notice_window';
    public const TYPE_PAUSE = 'pause';
    public const TYPE_PLAY = 'play';
    public const TYPE_QUESTION_WINDOW = 'question_window';
    public const TYPE_REJECTED = 'rejected';
    public const TYPE_SCHEDULE = 'schedule';
    public const TYPE_THUMBS_UP = 'thumbs_up';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
