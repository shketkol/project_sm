<?php

namespace Modules\Notification\Models;

use Carbon\Carbon;

/**
 * @property $item Campaign
 */
class FailedPaymentAlert extends RepeatableAlert
{
    public const REPEATS_NUMBER = 3;

    /**
     * @return int
     */
    protected function getInterval(): int
    {
        return Carbon::DAYS_PER_WEEK * Carbon::HOURS_PER_DAY;
    }

    /**
     * @return int
     */
    protected function getTotalRepeats(): int
    {
        return self::REPEATS_NUMBER;
    }
}
