<?php

namespace Modules\Notification\Models\Traits;

use Illuminate\Support\Collection;
use Modules\Notification\Models\Alert;

/**
 * Trait MorphAlerts
 * @property Collection|null $alerts
 * @package Modules\Notification\Models\Traits
 */
trait MorphAlerts
{
    /**
     * Get the campaign's alerts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function alerts()
    {
        return $this->morphMany(Alert::class, 'item');
    }
}
