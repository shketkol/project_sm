<?php

namespace Modules\Notification\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Notification\Models\NotificationCategory;

/**
 * @property NotificationCategory $category
 */
trait BelongsToNotificationCategory
{
    /**
     * Get the notification category.
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(NotificationCategory::class, 'notification_category_id');
    }
}
