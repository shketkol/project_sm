<?php

namespace Modules\Notification\Models\Traits;

use Illuminate\Database\Eloquent\Collection;
use Modules\Notification\Models\Notification;

trait CreateNotification
{
    /**
     * Create test notification using factory.
     *
     * @param array $attributes
     *
     * @return Notification
     */
    private function createTestNotification(array $attributes = []): Notification
    {
        return Notification::factory()->create($attributes);
    }

    /**
     * @param array $attributes
     * @param int   $count
     *
     * @return Collection|Notification[]
     */
    private function createTestNotifications(array $attributes = [], int $count = 1): Collection
    {
        return Notification::factory()->count($count)->create($attributes);
    }
}
