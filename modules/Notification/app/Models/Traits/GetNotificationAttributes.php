<?php

namespace Modules\Notification\Models\Traits;

use App\DataTable\Transformers\Traits\StripTags;

/**
 * @property-read string  $content
 * @property-read string  $content_preview
 * @property-read boolean $is_read
 */
trait GetNotificationAttributes
{
    use StripTags;

    /**
     * @return string
     */
    public function getContentAttribute(): string
    {
        return call_user_func([$this->type, 'getContent'], $this->data);
    }

    /**
     * @return string
     */
    public function getContentPreviewAttribute(): string
    {
        return $this->stripTags($this->content);
    }

    /**
     * @return bool
     */
    public function getIsReadAttribute(): bool
    {
        return $this->read();
    }
}
