<?php

namespace Modules\Notification\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Notification\Models\ReminderType;

/**
 * @property ReminderType $type
 */
trait BelongsToReminderType
{
    /**
     * Get the type for the alert.
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(ReminderType::class);
    }
}
