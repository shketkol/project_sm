<?php

namespace Modules\Notification\Models\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Modules\Notification\Models\Contracts\Alert as AlertContract;
use Modules\Notification\Models\Factories\AlertFactory;

trait AlertAttributes
{
    /**
     * Get all of the owning models.
     *
     * @return MorphTo
     */
    public function item(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Create a new model instance that is existing.
     *
     * @param  array  $attributes
     * @param  string|null  $connection
     * @return static
     */
    public function newFromBuilder($attributes = [], $connection = null): AlertContract
    {
        /** @var Model|AlertContract $model */
        $model = AlertFactory::getAlertInstance(data_get($attributes, 'type_id'));

        $model->setRawAttributes((array) $attributes, true);
        $model->setConnection($connection ?: $this->getConnectionName());
        $model->fireModelEvent('retrieved', false);

        return $model;
    }

    /**
     * Check if alert is ready to be sent.
     *
     * @return bool
     */
    public function getReadyAttribute(): bool
    {
        $now = Carbon::now();
        $eventTime = Carbon::parse($this->event_time);
        $diff = $now->diffInMinutes($eventTime, false);

        // alerts that should be send before event time
        if ($this->type->minutes_before) {
            return $diff <= $this->type->minutes_before;
        }

        // alerts that should be send after event time
        return $now->gt($eventTime);
    }
}
