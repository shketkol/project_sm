<?php

namespace Modules\Notification\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Notification\Models\Traits\AlertAttributes;
use Modules\Notification\Models\Traits\BelongsToAlertType;
use Modules\Notification\Models\Contracts\Alert as AlertContract;

/**
 * @property integer   $id
 * @property integer   $type_id
 * @property string    $event_time
 * @property string    $item_type
 * @property integer   $item_id
 * @property string    $date_sent
 * @property string    $deleted_at
 * @property integer   $repeats
 * @property AlertType $alertType
 * @property bool      $ready
 * @property Model     $item
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
abstract class BaseAlert extends Model implements AlertContract
{
    use BelongsToAlertType, AlertAttributes;

    /**
     * @var array
     */
    protected $fillable = ['type_id', 'event_time', 'item_type', 'item_id', 'date_sent', 'deleted_at', 'repeats'];

    /**
     * @var string
     */
    protected $table = 'alerts';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
