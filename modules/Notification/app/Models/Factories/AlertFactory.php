<?php

namespace Modules\Notification\Models\Factories;

use Illuminate\Support\Arr;
use Modules\Notification\Models\Alert;
use Modules\Notification\Models\AlertType;
use Modules\Notification\Models\Contracts\Alert as AlertContract;
use Modules\Notification\Models\FailedPaymentAlert;

class AlertFactory
{
    /**
     * Alert type to concrete Alert class map.
     */
    protected const MAP = [
        AlertType::ID_CREATIVE_MISSING => Alert::class,
        AlertType::ID_CANT_GO_LIVE => Alert::class,
        AlertType::ID_FAILED_PAYMENT => FailedPaymentAlert::class,
    ];

    /**
     * @param int $typeId
     *
     * @return AlertContract
     */
    public static function getAlertInstance(int $typeId): AlertContract
    {
        $alertClass = Arr::get(self::MAP, $typeId) ?? Alert::class;
        $instance = (new $alertClass)->newInstance([], true);
        return $instance;
    }
}
