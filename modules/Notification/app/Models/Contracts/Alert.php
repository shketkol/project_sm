<?php

namespace Modules\Notification\Models\Contracts;

use Illuminate\Database\Eloquent\Relations\MorphTo;

interface Alert
{
    /**
     * Get all of the owning models.
     *
     * @return MorphTo
     */
    public function item(): MorphTo;

    /**
     * Check if alert is ready to be sent.
     *
     * @return bool
     */
    public function getReadyAttribute(): bool;
}
