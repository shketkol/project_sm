<?php

namespace Modules\Notification\Models\Contracts;

interface RepeatableAlert
{
    /**
     * @return bool
     */
    public function repeat(): bool;

    /**
     * @return int
     */
    public function getCurrentRepeat(): int;
}
