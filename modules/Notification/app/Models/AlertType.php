<?php

namespace Modules\Notification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string  $name
 * @property integer $minutes_before
 */
class AlertType extends Model
{
    /**
     * Default types.
     */
    public const CREATIVE_MISSING = 'creative-missing';
    public const CANT_GO_LIVE = 'cant-go-live';
    public const FAILED_PAYMENT = 'failed-payment';

    public const ID_CREATIVE_MISSING = 1;
    public const ID_CANT_GO_LIVE = 2;
    public const ID_FAILED_PAYMENT = 3;

    /**
     * @var array
     */
    protected $fillable = ['name', 'minutes_before'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
