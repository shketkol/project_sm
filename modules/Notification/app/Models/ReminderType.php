<?php

namespace Modules\Notification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string  $name
 */
class ReminderType extends Model
{
    /**
     * Type IDs.
     */
    public const ID_RETRY_PAYMENT            = 1;
    public const ID_RETRY_PAYMENT_PROCESSING = 2;
    public const ID_SPECIAL_ADS              = 3;

    /**
     * Types.
     */
    public const RETRY_PAYMENT            = 'retry-payment';
    public const RETRY_PAYMENT_PROCESSING = 'retry-payment-processing';
    public const SPECIAL_ADS              = 'special_ads';

    /**
     * The list of available types.
     */
    public const TYPES = [
        self::ID_RETRY_PAYMENT            => self::RETRY_PAYMENT,
        self::ID_RETRY_PAYMENT_PROCESSING => self::RETRY_PAYMENT_PROCESSING,
        self::ID_SPECIAL_ADS              => self::SPECIAL_ADS,
    ];

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
