<?php

namespace Modules\Notification\Models;

use Carbon\Carbon;
use Modules\Notification\Models\Contracts\RepeatableAlert as RepeatableAlertContract;

abstract class RepeatableAlert extends BaseAlert implements RepeatableAlertContract
{
    /**
     * @return bool
     */
    protected function shouldRepeat(): bool
    {
        return $this->getCurrentRepeat() < $this->getTotalRepeats();
    }

    /**
     * Get the number of possible repeats.
     *
     * @return int
     */
    abstract protected function getTotalRepeats(): int;

    /**
     * @return int
     */
    public function getCurrentRepeat(): int
    {
        return $this->repeats;
    }

    /**
     * Get interval between repeats (in hours).
     *
     * @return int
     */
    abstract protected function getInterval(): int;

    /**
     * @return Carbon
     */
    protected function getNextEventTime()
    {
        return Carbon::now()->addHours($this->getInterval());
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function repeat(): bool
    {
        if (!$this->shouldRepeat()) {
            return false;
        }

        $this->event_time = $this->getNextEventTime();
        $this->date_sent = Carbon::now();
        $this->repeats++;
        $this->saveOrFail();

        return true;
    }
}
