<?php

namespace Modules\Notification\DataTable;

use App\DataTable\DataTable;
use Illuminate\Database\Eloquent\Builder;
use Modules\Notification\DataTable\Filters\NotificationCategoryFilter;
use Modules\Notification\DataTable\Repositories\Contracts\NotificationsDataTableRepository;
use Modules\Notification\DataTable\Repositories\Criteria\AddNotificationCategoryCriteria;
use Modules\Notification\DataTable\Repositories\Criteria\UserCriteria;
use Modules\Notification\DataTable\Transformers\NotificationsTransformer;

class NotificationDataTable extends DataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'notifications';

    /**
     * Transformer class
     *
     * @var string
     */
    protected $transformer = NotificationsTransformer::class;

    /**
     * Repository
     *
     * @var string
     */
    protected $repository = NotificationsDataTableRepository::class;

    /**
     * Filters
     *
     * @var array
     */
    protected $filters = [
        NotificationCategoryFilter::class,
    ];

    /**
     * Add aliases according model relation.
     * Required for search
     *
     * @var array
     */
    protected $relationAliases = [
        'category' => 'notification_categories.name',
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        /** @var NotificationsDataTableRepository $repository */
        $repository = $this->getRepository();

        return $this->applySortCriteria($repository)
            ->pushCriteria(new UserCriteria($this->getUser()))
            ->pushCriteria(new AddNotificationCategoryCriteria())
            ->notifications();
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('notification.list');
    }
}
