<?php


namespace Modules\Notification\DataTable\Filters;

use App\DataTable\Filters\DataTableFilter;
use Illuminate\Database\Eloquent\Builder;
use Modules\Notification\Models\NotificationCategory;
use Yajra\DataTables\DataTableAbstract;

class NotificationCategoryFilter extends DataTableFilter
{
    /**
     * Filter name
     *
     * @var string
     */
    public static $name = 'notification-category';

    /**
     * DataTableFilter constructor.
     *
     * @param \Yajra\DataTables\DataTableAbstract $dataTable
     */
    public function filter(DataTableAbstract $dataTable): void
    {
        $this->makeFilter($dataTable, 'notification_categories.name', function (Builder $query, $values) {
            $query->orWhereIn('notification_categories.id', $values);
        });
    }

    /**
     * Get filter options
     *
     * @return array
     */
    public function options(): array
    {
        return NotificationCategory::all()
            ->pluck('name', 'id')
            ->mapWithKeys(function (string $value, int $key) {
                return [$key => trans("notification::labels.notification_categories.$value")];
            })
            ->toArray();
    }
}
