<?php

namespace Modules\Notification\DataTable\Transformers;

use App\DataTable\Transformers\DataTableTransformer;

class NotificationsTransformer extends DataTableTransformer
{
    /**
     * Mapping fields.
     *
     * @var array
     */
    protected $mapMap = [
        'id'              => [
            'name'    => 'id',
            'default' => '-',
        ],
        'category'        => [
            'name'    => 'category',
            'default' => '-',
        ],
        'content_preview' => [
            'name'    => 'content_preview',
            'default' => '-',
        ],
        'content'         => [
            'name'    => 'content',
            'default' => '-',
        ],
        'is_read'         => [
            'name'    => 'is_read',
            'default' => '-',
        ],
        'created_at'      => [
            'name'    => 'created_at',
            'default' => '-',
        ],
    ];
}
