<?php

namespace Modules\Notification\DataTable\Repositories\Contracts;

use App\Repositories\Contracts\Repository;
use Illuminate\Database\Eloquent\Builder;

interface NotificationsDataTableRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string;

    /**
     * Get user Notifications
     *
     * @return Builder
     */
    public function notifications(): Builder;
}
