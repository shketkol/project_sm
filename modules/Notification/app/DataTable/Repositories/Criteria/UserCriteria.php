<?php

namespace Modules\Notification\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Modules\User\Models\User;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class UserCriteria implements CriteriaInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserCriteria constructor.
     *
     * @param User|\Illuminate\Contracts\Auth\Authenticatable $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model
            ->where('notifications.notifiable_id', '=', $this->user->id)
            ->where('notifications.notifiable_type', '=', get_class($this->user));
    }
}
