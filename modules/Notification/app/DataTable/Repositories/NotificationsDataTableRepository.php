<?php

namespace Modules\Notification\DataTable\Repositories;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Notification\DataTable\Repositories\Contracts\NotificationsDataTableRepository
    as NotificationsDataTableRepositoryInterface;
use Modules\Notification\Models\Notification;

class NotificationsDataTableRepository extends Repository implements NotificationsDataTableRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Notification::class;
    }

    /**
     * Get user notifications
     *
     * @return Builder
     */
    public function notifications(): Builder
    {
        $this->applyCriteria();

        return $this->model->addSelect('notifications.*');
    }
}
