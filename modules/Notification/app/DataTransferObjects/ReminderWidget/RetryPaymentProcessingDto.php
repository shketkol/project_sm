<?php

namespace Modules\Notification\DataTransferObjects\ReminderWidget;

use Modules\Notification\Models\Reminder;
use Modules\Notification\Models\ReminderType;

class RetryPaymentProcessingDto extends ReminderWidget
{
    /**
     * @param Reminder $reminder
     *
     * @return ReminderWidget
     */
    public static function create(Reminder $reminder): ReminderWidget
    {
        return new self([
            'id'            => $reminder->getKey(),
            'reminderType'  => ReminderType::TYPES[$reminder->type_id] ?? '',
            'messageType'   => self::MESSAGE_TYPE_WARNING,
            'customClasses' => 'half-width',
            'title'         => __('payment::reminders.retry_payment_processing.title'),
            'message'       => __('payment::reminders.retry_payment_processing.message'),
        ]);
    }
}
