<?php

namespace Modules\Notification\DataTransferObjects\ReminderWidget;

use Modules\Notification\Models\Reminder;
use Modules\Notification\Models\ReminderType;

class SpecialAdsDto extends ReminderWidget
{
    /**
     * @param Reminder $reminder
     *
     * @return ReminderWidget
     */
    public static function create(Reminder $reminder): ReminderWidget
    {
        return new self([
            'id'           => $reminder->getKey(),
            'reminderType' => ReminderType::TYPES[$reminder->type_id] ?? '',
            'messageType'  => self::MESSAGE_TYPE_WARNING,
            'title'        => __('user::reminders.special_ads.title'),
            'message'      => __(
                'user::reminders.special_ads.message',
                [
                    'url' => config('general.links.faq.special_ads_categories'),
                ]
            ),
            'ctaItems'     => [
                [
                    'type'  => ReminderWidget::CTA_TYPE_DROP_REMINDER,
                    'title' => __('user::reminders.special_ads.cta_title'),
                ],
            ],
        ]);
    }
}
