<?php

namespace Modules\Notification\DataTransferObjects\ReminderWidget;

use App\Helpers\HtmlHelper;
use Illuminate\Support\Arr;
use Modules\Notification\Models\Reminder;

class RetryPaymentDto extends ReminderWidget
{
    /**
     * @param Reminder $reminder
     *
     * @return array
     */
    protected static function prepareData(Reminder $reminder): array
    {
        $campaigns = Arr::get($reminder->data, 'campaigns', []);

        return array_merge(
            parent::prepareData($reminder),
            [
                'messageType' => self::MESSAGE_TYPE_DANGER,
                'title'       => __('payment::reminders.retry_payment.title'),
                'message'     => ! $campaigns || count($campaigns) > 1
                    ? __('payment::reminders.retry_payment.message_multiple_campaigns')
                    : __('payment::reminders.retry_payment.message_single_campaign', [
                        'campaign' => e(Arr::get($campaigns, '0.name')),
                        'href'     => route('campaigns.show', ['campaign' => Arr::get($campaigns, '0.id')]),
                    ]),
                'ctaItems'    => [
                    [
                        'type'  => ReminderWidget::CTA_TYPE_LINK,
                        'title' => __('payment::reminders.retry_payment.cta_title'),
                        'route' => 'users.profile',
                    ],
                ],
            ]
        );
    }
}
