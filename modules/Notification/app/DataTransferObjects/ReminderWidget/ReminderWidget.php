<?php

namespace Modules\Notification\DataTransferObjects\ReminderWidget;

use Modules\Notification\Models\Reminder;
use Modules\Notification\Models\ReminderType;
use Spatie\DataTransferObject\DataTransferObject;

class ReminderWidget extends DataTransferObject
{
    /**
     * Message-box types.
     */
    const MESSAGE_TYPE_DANGER = 'danger';
    const MESSAGE_TYPE_WARNING = 'warning';

    /**
     * CTA button types.
     */
    const CTA_TYPE_LINK = 'link';
    const CTA_TYPE_DROP_REMINDER = 'drop-reminder';

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $reminderType = '';

    /**
     * @var string
     */
    public $messageType = '';

    /**
     * @var string
     */
    public $customClasses = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $message = '';

    /**
     * @var array
     */
    public $ctaItems = [];

    /**
     * @var array
     */
    public $payload = [];

    /**
     * @param Reminder $reminder
     *
     * @return ReminderWidget
     */
    public static function create(Reminder $reminder): ReminderWidget
    {
        return new static(static::prepareData($reminder));
    }

    /**
     * @param Reminder $reminder
     *
     * @return array
     */
    protected static function prepareData(Reminder $reminder): array
    {
        return [
            'id'           => $reminder->id,
            'reminderType' => ReminderType::TYPES[$reminder->type_id] ?? '',
            'payload'      => $reminder->data,
        ];
    }
}
