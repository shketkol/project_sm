<?php

namespace Modules\Notification\Listeners;

use Illuminate\Notifications\Events\NotificationSent;

class NotifiedListener
{
    /**
     * Handle the event.
     *
     * @param  NotificationSent $event
     * @return void
     */
    public function handle(NotificationSent $event)
    {
        if ($event->channel === 'database' && $event->notifiable->notified_at) {
            $event->notifiable->notified_at = null;
            $event->notifiable->save();
        }
    }
}
