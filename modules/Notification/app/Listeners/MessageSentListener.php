<?php

namespace Modules\Notification\Listeners;

use Illuminate\Mail\Events\MessageSent;
use Illuminate\Support\Arr;
use Modules\Broadcast\Notifications\BroadcastNotification;
use Modules\Notification\Models\EmailHistory;
use Modules\Notification\Repositories\EmailHistoryRepository;
use Psr\Log\LoggerInterface;

class MessageSentListener
{
    /**
     * Allowed emails to store
     *
     * @var string[]
     */
    private $allowed = [
        BroadcastNotification::class,
    ];

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var EmailHistoryRepository
     */
    private $repository;

    /**
     * @param LoggerInterface        $log
     * @param EmailHistoryRepository $repository
     */
    public function __construct(LoggerInterface $log, EmailHistoryRepository $repository)
    {
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param MessageSent $event
     *
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(MessageSent $event): void
    {
        $this->log->info('[Sent emails tracker] Started storing.');
        $mailableType = Arr::get($event->data, 'mailable_type');

        if (!in_array($mailableType, $this->allowed, true)) {
            $this->log->info('[Sent emails tracker] Cancelled. Email is not allowed to be stored.', [
                'mailable_type' => $mailableType,
            ]);
            return;
        }

        $attributes = [
            'notifiable_type' => Arr::get($event->data, 'notifiable_type'),
            'notifiable_id'   => Arr::get($event->data, 'notifiable_id'),
            'mailable_type'   => $mailableType,
            'mailable_id'     => Arr::get($event->data, 'mailable_id'),
        ];

        /** @var EmailHistory $history */
        $history = $this->repository->create($attributes);

        $this->log->info('[Sent emails tracker] Finished storing.', [
            'attributes' => $attributes,
            'history_id' => $history->id,
        ]);
    }
}
