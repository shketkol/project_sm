<?php

namespace Modules\Notification\Listeners;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Notifications\Advertiser\Missing;
use Modules\Notification\Models\Alert;

class CreativeMissingListener
{
    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * NoCreativeListener constructor.
     * @param Dispatcher $notification
     */
    public function __construct(Dispatcher $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  Alert $alert
     * @return void
     */
    public function handle(Alert $alert)
    {
        /** @var Campaign $campaign */
        $campaign = $alert->item;

        if ($campaign->skipAlerts()) {
            return;
        }

        $this->notification->send(
            $campaign->user,
            new Missing($campaign)
        );
    }
}
