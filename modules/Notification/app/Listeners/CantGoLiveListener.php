<?php

namespace Modules\Notification\Listeners;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Notifications\Admin\CannotGoLive as AdminNotification;
use Modules\Campaign\Notifications\Advertiser\CannotGoLive as AdvertiserNotification;
use Modules\Notification\Models\Alert;
use Modules\User\Repositories\Contracts\UserRepository;

class CantGoLiveListener
{
    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * CantGoLiveListener constructor.
     * @param Dispatcher $notification
     * @param UserRepository $userRepository
     */
    public function __construct(Dispatcher $notification, UserRepository $userRepository)
    {
        $this->notification = $notification;
        $this->userRepository = $userRepository;
    }

    /**
     * Handle the event.
     *
     * @param Alert $alert
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Alert $alert)
    {
        /** @var Campaign $campaign */
        $campaign = $alert->item;

        if ($campaign->skipAlerts()) {
            return;
        }

        // Send notification to advertiser
        $this->notification->send(
            $campaign->user,
            new AdvertiserNotification($campaign)
        );

        // Send notification to admin
        $this->notification->send(
            $this->userRepository->getAdmin(),
            new AdminNotification($campaign)
        );
    }
}
