<?php

namespace Modules\Notification\Listeners;

use Illuminate\Contracts\Notifications\Dispatcher;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Notification\Models\FailedPaymentAlert;
use Modules\User\Models\User;
use Modules\User\Notifications\Advertiser\BadPaymentRepeatFirst;
use Modules\User\Notifications\Advertiser\BadPaymentRepeatSecond;
use Modules\User\Notifications\Advertiser\BadPaymentRepeatThird;
use Modules\User\Notifications\UserNotification;
use Psr\Log\LoggerInterface;

class FailedPaymentListener
{
    /**
     * Repeat notifications mapping.
     */
    protected const NOTIFICATIONS_MAPPING = [
        1 => BadPaymentRepeatFirst::class,
        2 => BadPaymentRepeatSecond::class,
        3 => BadPaymentRepeatThird::class,
    ];

    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @param Dispatcher $notification
     */
    public function __construct(Dispatcher $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param FailedPaymentAlert $alert
     *
     * @return void
     */
    public function handle(FailedPaymentAlert $alert): void
    {
        $campaign = $alert->item;
        $user = $campaign->user;
        $notification = $this->getNotification($alert, $campaign, $user);

        if (!$notification) {
            return;
        }

        $notification->setBcc(config('notification.failed_payment_bcc'));
        $notification->setCc(config('notification.failed_payment_cc'));

        $this->notification->send($user, $notification);
    }

    /**
     * @param FailedPaymentAlert $alert
     * @param Campaign           $campaign
     * @param User               $user
     *
     * @return UserNotification|null
     */
    protected function getNotification(
        FailedPaymentAlert $alert,
        Campaign $campaign,
        User $user
    ): ?UserNotification {
        $className = Arr::get(self::NOTIFICATIONS_MAPPING, $alert->getCurrentRepeat());

        if (!$className) {
            $this->log->error('There are no appropriate repeatable notification', [
                'alert_id'       => $alert->getKey(),
                'current_repeat' => $alert->getCurrentRepeat(),
                'campaign_id'    => $campaign->getKey(),
            ]);

            return null;
        }

        return new $className($campaign, $user);
    }
}
