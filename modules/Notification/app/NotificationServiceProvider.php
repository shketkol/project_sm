<?php

namespace Modules\Notification;

use App\Providers\ModuleServiceProvider;
use Illuminate\Notifications\Channels\DatabaseChannel as BaseDatabaseChannel;
use Illuminate\Support\Facades\Event;
use Modules\Notification\Commands\SendAlerts;
use Modules\Notification\Console\Commands\NotificationMakeCommand;
use Modules\Notification\DataTable\Repositories\Contracts\NotificationsDataTableRepository
    as NotificationsDataTableRepositoryInterface;
use Modules\Notification\DataTable\Repositories\NotificationsDataTableRepository;
use Modules\Notification\Listeners\CantGoLiveListener;
use Modules\Notification\Listeners\CreativeMissingListener;
use Modules\Notification\Listeners\FailedPaymentListener;
use Modules\Notification\Models\AlertType;
use Modules\Notification\Notifications\Channels\DatabaseChannel;
use Modules\Notification\Policies\NotificationPolicy;
use Modules\Notification\Policies\ReminderPolicy;

class NotificationServiceProvider extends ModuleServiceProvider
{
    /**
     * @var array
     */
    public $bindings = [
        NotificationsDataTableRepositoryInterface::class => NotificationsDataTableRepository::class,
    ];

    /**
     *
     * @var array
     */
    protected $commands = [
        SendAlerts::class,
        NotificationMakeCommand::class,
    ];

    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'notification' => NotificationPolicy::class,
        'reminder'     => ReminderPolicy::class,
    ];

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'notification';
    }

    /**
     * Boot provider.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();

        $this->commands($this->commands);
        $this->subscribeListeners();
        $this->loadRoutes();
        $this->loadConfigs(['notification']);

        $this->app->bind(BaseDatabaseChannel::class, function () {
            return $this->app->make(DatabaseChannel::class);
        });
    }

    /**
     * Subscribe event listeners.
     */
    protected function subscribeListeners(): void
    {
        Event::listen('alert.' . AlertType::CREATIVE_MISSING, CreativeMissingListener::class);
        Event::listen('alert.' . AlertType::CANT_GO_LIVE, CantGoLiveListener::class);
        Event::listen('alert.' . AlertType::FAILED_PAYMENT, FailedPaymentListener::class);
    }
}
