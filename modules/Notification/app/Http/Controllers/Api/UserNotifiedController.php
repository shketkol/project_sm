<?php

namespace Modules\Notification\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Modules\Notification\Actions\UserNotifiedAction;

class UserNotifiedController extends Controller
{
    /**
     * @param UserNotifiedAction $action
     * @param Authenticatable $user
     *
     * @return \Illuminate\Http\Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(UserNotifiedAction $action, Authenticatable $user): JsonResponse
    {
        return response()->json(['data' => ['notified' => $action->handle($user)]]);
    }
}
