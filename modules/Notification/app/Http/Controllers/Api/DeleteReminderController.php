<?php

namespace Modules\Notification\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Notification\Models\Reminder;
use Psr\Log\LoggerInterface;

class DeleteReminderController extends Controller
{
    /**
     * @param Reminder        $reminder
     * @param LoggerInterface $log
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function __invoke(
        Reminder $reminder,
        LoggerInterface $log
    ): JsonResponse {
        $log->info('Destroying reminder.', [
            'id'      => $reminder->getKey(),
            'user_id' => $reminder->user_id,
            'type_id' => $reminder->type_id,
        ]);

        $reminder->delete();

        return response()->json(['data' => [
            'deleted_id'  => $reminder->getKey(),
        ]]);
    }
}
