<?php

namespace Modules\Notification\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Notification\Http\Resources\NotificationResource;
use Modules\Notification\Models\Notification;

class ShowNotificationController extends Controller
{
    /**
     * @param Notification $notification
     *
     * @return NotificationResource
     */
    public function __invoke(Notification $notification): NotificationResource
    {
        return new NotificationResource($notification);
    }
}
