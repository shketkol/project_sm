<?php

namespace Modules\Notification\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Modules\Notification\Actions\GetNotificationWidgetDataAction;
use Modules\Notification\Actions\GetRemindersWidgetDataAction;
use Modules\Notification\Http\Resources\WidgetsResource;

class NotificationWidgetController extends Controller
{
    /**
     * @param GetNotificationWidgetDataAction $getNotificationsAction
     * @param GetRemindersWidgetDataAction    $getRemindersAction
     * @param Guard                           $guard
     *
     * @return WidgetsResource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        GetNotificationWidgetDataAction $getNotificationsAction,
        GetRemindersWidgetDataAction $getRemindersAction,
        Guard $guard
    ): WidgetsResource {
        return new WidgetsResource([
            'notifications' => $getNotificationsAction->handle($guard->user()),
            'reminders'     => $getRemindersAction->handle($guard->user()),
        ]);
    }
}
