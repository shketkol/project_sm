<?php

namespace Modules\Notification\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Modules\Notification\Actions\MarkAllReadNotificationAction;

class MarkAllReadNotificationController extends Controller
{
    /**
     * @param MarkAllReadNotificationAction $action
     * @param Guard                         $guard
     *
     * @return JsonResponse
     */
    public function __invoke(MarkAllReadNotificationAction $action, Guard $guard): JsonResponse
    {
        $action->handle($guard->user());

        return response()->json(['data' => [
            'message' => __('notification::messages.marked_as_read'),
        ]]);
    }
}
