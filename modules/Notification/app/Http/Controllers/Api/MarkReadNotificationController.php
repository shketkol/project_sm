<?php

namespace Modules\Notification\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Modules\Notification\Actions\MarkReadNotificationAction;
use Modules\Notification\Models\Notification;

class MarkReadNotificationController extends Controller
{
    /**
     * @param Notification               $notification
     * @param MarkReadNotificationAction $action
     * @param Guard                      $guard
     *
     * @return JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(Notification $notification, MarkReadNotificationAction $action, Guard $guard): JsonResponse
    {
        $action->handle($notification, $guard->user());

        return response()->json(['data' => [
            'message' => __('messages.success'),
        ]]);
    }
}
