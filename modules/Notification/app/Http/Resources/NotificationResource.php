<?php

namespace Modules\Notification\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Notification\Models\Traits\GetNotificationAttributes;

/**
 * @mixin \Modules\Notification\Models\Notification
 */
class NotificationResource extends JsonResource
{
    use GetNotificationAttributes;

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'              => $this->id,
            'category'        => optional($this->category)->name,
            'content'         => $this->content,
            'content_preview' => $this->content_preview,
            'is_read'         => $this->is_read,
            'created_at'      => $this->created_at,
        ];
    }
}
