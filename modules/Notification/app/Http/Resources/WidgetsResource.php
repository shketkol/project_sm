<?php

namespace Modules\Notification\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class WidgetsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'notifications' => [
                'notified' => Arr::get($this->resource, 'notifications.notified'),
                'total'    => Arr::get($this->resource, 'notifications.count'),
                'list'     => NotificationResource::collection(
                    Arr::get($this->resource, 'notifications.notifications')
                ),
            ],
            'reminders' => ReminderResource::collection(Arr::get($this->resource, 'reminders')),
        ];
    }
}
