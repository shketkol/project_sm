<?php

namespace Modules\Notification\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Notification\DataTransferObjects\ReminderWidget\ReminderWidget;

/**
 * @mixin ReminderWidget
 */
class ReminderResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'             => $this->id,
            'reminder_type'  => $this->reminderType,
            'message_type'   => $this->messageType,
            'custom_classes' => $this->customClasses,
            'title'          => $this->title,
            'message'        => $this->message,
            'ctaItems'       => $this->ctaItems,
            'payload'        => $this->payload,
        ];
    }
}
