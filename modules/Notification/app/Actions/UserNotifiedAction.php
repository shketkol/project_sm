<?php

namespace Modules\Notification\Actions;

use Illuminate\Support\Carbon;
use Modules\User\Models\User;

class UserNotifiedAction
{
    /**
     * @param User|\Illuminate\Contracts\Auth\Authenticatable $user
     *
     * @return bool
     */
    public function handle(User $user): bool
    {
        if (!$user->notified_at) {
            $user->update(['notified_at' => Carbon::now()]);
            $user->refresh();
        }

        return !!$user->notified_at;
    }
}
