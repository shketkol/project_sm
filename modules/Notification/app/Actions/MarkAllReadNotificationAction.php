<?php

namespace Modules\Notification\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class MarkAllReadNotificationAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * @param User|Authenticatable $user
     *
     * @return void
     */
    public function handle(User $user): void
    {
        $this->log->info('Started to mark all notifications as read.', [
            'user_id' => $user->id,
        ]);

        $user->unreadNotifications->markAsRead();

        $this->log->info('Finished to mark all notifications as read.', [
            'user_id' => $user->id,
        ]);
    }
}
