<?php

namespace Modules\Notification\Actions;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Log\Logger;

class StoreAlertAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * StoreAlertAction constructor.
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * Store alert.
     *
     * @param Model $model
     * @param int $typeId
     * @param Carbon $eventTime
     * @return bool
     */
    public function handle(Model $model, int $typeId, Carbon $eventTime): bool
    {
        $alert = $model->alerts()->where('type_id', $typeId)->first();

        if (!$alert) {
            $alert = $model->alerts()->create([
                'type_id'    => $typeId,
                'event_time' => $eventTime,
                'repeats'    => 1,
            ]);

            $this->log->info('Created alert.', [
                'model_id' => $model->id,
                'type_id'  => $typeId,
            ]);
        }

        $this->log->info('Alert is updated.', [
            'event_time' => $eventTime,
        ]);

        return $alert->fill([
            'event_time' => $eventTime,
            'repeats'    => 1,
        ])->saveOrFail();
    }
}
