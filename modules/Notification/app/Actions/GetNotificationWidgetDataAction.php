<?php

namespace Modules\Notification\Actions;

use Illuminate\Database\Eloquent\Collection;
use Modules\User\Models\User;

class GetNotificationWidgetDataAction
{
    /**
     * How much notifications to show in widget?
     *
     * @var int
     */
    public const NOTIFICATIONS_LIMIT = 4;

    /**
     * @param User|\Illuminate\Contracts\Auth\Authenticatable $user
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(User $user): array
    {
        return [
            'notified'      => $this->isNotified($user),
            'count'         => $this->getNotificationsCount($user),
            'notifications' => $this->getNotifications($user),
        ];
    }

    /**
     * @param User $user
     *
     * @return Collection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getNotifications(User $user): Collection
    {
        return $user->notifications()->limit(self::NOTIFICATIONS_LIMIT)->get();
    }

    /**
     * @param User $user
     *
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getNotificationsCount(User $user): int
    {
        return $user->notifications()->whereNull('read_at')->count();
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function isNotified(User $user): bool
    {
        return !!$user->notified_at || !$user->notifications->count();
    }
}
