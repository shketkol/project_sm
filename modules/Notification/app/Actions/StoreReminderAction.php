<?php

namespace Modules\Notification\Actions;

use Illuminate\Log\Logger;
use Modules\Notification\Models\Reminder;
use Modules\User\Models\User;

class StoreReminderAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * @param User  $user
     * @param int   $typeId
     * @param array $data
     *
     * @return void
     */
    public function handle(User $user, int $typeId, array $data = [])
    {
        Reminder::updateOrCreate(
            [
                'user_id' => $user->getKey(),
                'type_id' => $typeId,
            ],
            [
                'data' => $data,
            ]
        );

        $this->log->info('Set reminder', [
            'user_id' => $user->getKey(),
            'type_id' => $typeId,
            'data'    => $data,
        ]);
    }
}
