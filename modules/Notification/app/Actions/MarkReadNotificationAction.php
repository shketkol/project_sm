<?php

namespace Modules\Notification\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Notification\Models\Notification;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class MarkReadNotificationAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * @param Notification         $notification
     * @param User|Authenticatable $user
     *
     * @return void
     */
    public function handle(Notification $notification, User $user): void
    {
        $this->log->info('Started to mark notification as read.', [
            'notification_id' => $notification->id,
            'user_id'         => $user->id,
        ]);

        $notification->markAsRead();

        $this->log->info('Finished to mark notification as read.', [
            'notification_id' => $notification->id,
            'user_id'         => $user->id,
        ]);
    }
}
