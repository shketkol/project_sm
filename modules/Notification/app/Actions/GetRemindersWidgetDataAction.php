<?php

namespace Modules\Notification\Actions;

use Illuminate\Support\Collection;
use Modules\Notification\DataTransferObjects\ReminderWidget\RetryPaymentDto;
use Modules\Notification\DataTransferObjects\ReminderWidget\RetryPaymentProcessingDto;
use Modules\Notification\DataTransferObjects\ReminderWidget\SpecialAdsDto;
use Modules\Notification\Models\Reminder;
use Modules\Notification\Models\ReminderType;
use Modules\Notification\Repositories\ReminderRepository;
use Modules\User\Models\User;

class GetRemindersWidgetDataAction
{
    /**
     * @var ReminderRepository
     */
    protected $repository;

    /**
     * @param ReminderRepository $repository
     */
    public function __construct(ReminderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param User|\Illuminate\Contracts\Auth\Authenticatable $user
     *
     * @return Collection
     */
    public function handle(User $user): Collection
    {
        $reminder = $this->repository->findUsersLastReminder($user->getKey());
        if (!$reminder) {
            return collect();
        }

        $reminderDto = $this->getReminderDto($reminder);
        if (!$reminderDto) {
            return collect();
        }


        return collect([$reminderDto::create($reminder)]);
    }

    /**
     * @param Reminder $reminder
     *
     * @return string|null
     */
    protected function getReminderDto(Reminder $reminder): ?string
    {
        return [
            ReminderType::ID_RETRY_PAYMENT => RetryPaymentDto::class,
            ReminderType::ID_RETRY_PAYMENT_PROCESSING => RetryPaymentProcessingDto::class,
            ReminderType::ID_SPECIAL_ADS => SpecialAdsDto::class,
        ][$reminder->type_id] ?? null;
    }
}
