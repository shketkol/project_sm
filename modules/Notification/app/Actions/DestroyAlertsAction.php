<?php

namespace Modules\Notification\Actions;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Log\Logger;
use Modules\Campaign\Models\Campaign;
use Modules\Notification\Models\Alert;
use Modules\Notification\Models\AlertType;
use Modules\User\Models\User;

class DestroyAlertsAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * Destroy alert.
     *
     * @param Model      $model
     * @param array|null $alertTypes
     *
     * @return bool
     */
    public function handle(Model $model, ?array $alertTypes = []): bool
    {
        $this->log->info('Destroying alerts.', [
            'model_id' => $model->id,
        ]);

        if($key = array_search(AlertType::ID_FAILED_PAYMENT, $alertTypes) !== false) {
            $this->destroyBadPaymentAlert($model);
            unset($alertTypes[$key]);
        }

        return $model->alerts()->whereHas('type', function ($query) use ($alertTypes) {
            if ($alertTypes) {
                $query->whereIn('type_id', $alertTypes);
            }
        })->forceDelete();
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function destroyBadPaymentAlert(User $user): bool
    {
        $this->log->info('Destroying user\'s bad payment alerts.', [
            'user_id' => $user->getKey(),
        ]);

        // As BadPayment alert is being bound to the campaign we need to calculate it using user.
        return Alert::whereHasMorph(
            'item',
            Campaign::class,
            function (Builder $query) use ($user) {
                $query->where([
                    'user_id' => $user->getKey(),
                    'type_id' => AlertType::ID_FAILED_PAYMENT,
                ]);
            }
        )->forceDelete();
    }
}
