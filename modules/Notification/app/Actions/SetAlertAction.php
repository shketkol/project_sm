<?php

namespace Modules\Notification\Actions;

class SetAlertAction
{
    /**
     * @var StoreAlertAction
     */
    protected $storeAlertAction;

    /**
     * SetCampaignAlertAction constructor.
     * @param StoreAlertAction $storeAlertAction
     */
    public function __construct(StoreAlertAction $storeAlertAction)
    {
        $this->storeAlertAction = $storeAlertAction;
    }
}
