<?php

namespace Modules\Notification\Actions;

use Illuminate\Log\Logger;
use Modules\Notification\Repositories\ReminderRepository;
use Modules\User\Models\User;

class DestroyReminderAction
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var ReminderRepository
     */
    protected $repository;

    /**
     * DestroyReminderAction constructor.
     *
     * @param Logger             $log
     * @param ReminderRepository $repository
     */
    public function __construct(Logger $log, ReminderRepository $repository)
    {
        $this->log        = $log;
        $this->repository = $repository;
    }

    /**
     * @param User $user
     * @param int  $typeId
     *
     * @return bool|null
     */
    public function handle(User $user, int $typeId): ?bool
    {
        $this->log->info('Destroying reminder.', [
            'user_id' => $user->id,
            'type_id' => $typeId,
        ]);

        return $this->repository->deleteWhere([
            'user_id' => $user->getKey(),
            'type_id' => $typeId,
        ]);
    }
}
