<?php

namespace Modules\Notification\Policies;

use App\Policies\Policy;
use Modules\Notification\Models\Reminder;
use Modules\User\Models\User;

class ReminderPolicy extends Policy
{
    /**
     * Model class.
     *
     * @var string
     */
    protected $model = Reminder::class;

    /**
     * Permissions.
     */
    public const PERMISSION_DELETE_REMINDER = 'delete_reminder';

    /**
     * Returns true if user can delete the reminder.
     *
     * @param User     $user
     * @param Reminder $reminder
     *
     * @return bool
     */
    public function delete(User $user, Reminder $reminder): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_DELETE_REMINDER)) {
            return false;
        }

        if ($reminder->user_id !== $user->id) {
            return false;
        }

        return true;
    }
}
