<?php

namespace Modules\Notification\Policies;

use App\Policies\Policy;
use Modules\Notification\Models\Notification;
use Modules\User\Models\User;

class NotificationPolicy extends Policy
{
    /**
     * Model class.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Permissions.
     */
    public const PERMISSION_LIST_NOTIFICATION = 'list_notification';
    public const PERMISSION_VIEW_NOTIFICATION = 'view_notification';

    /**
     * Returns true if user can view list of all notifications.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_NOTIFICATION);
    }

    /**
     * Returns true if user can view the notification.
     *
     * @param User         $user
     * @param Notification $notification
     *
     * @return bool
     */
    public function view(User $user, Notification $notification): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_VIEW_NOTIFICATION)) {
            return false;
        }

        if (!$user->is($notification->notifiable)) {
            return false;
        }

        return true;
    }
}
