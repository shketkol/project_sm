<?php

namespace Modules\Notification\Console\Commands;

use Illuminate\Console\Command;
use Modules\Notification\Models\Traits\CreateNotification;

class NotificationMakeCommand extends Command
{
    use CreateNotification;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:user_notification {user_id} {count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create test notification.';

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        $userId = (int)$this->argument('user_id');
        $count = (int)$this->argument('count');

        $this->createTestNotifications(['notifiable_id' => $userId], $count);
    }
}
