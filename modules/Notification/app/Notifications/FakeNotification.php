<?php

namespace Modules\Notification\Notifications;

use App\Notifications\Notification;
use Faker\Factory;
use Modules\Notification\Models\NotificationCategory;
use Modules\User\Models\User;

class FakeNotification extends Notification
{
    /**
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [];
    }

    /**
     * @param array $data
     *
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public static function getContent(array $data): string
    {
        return e(Factory::create()->sentence);
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return rand(NotificationCategory::ID_ACCOUNT, NotificationCategory::ID_TRANSACTION);
    }
}
