<?php

namespace Modules\Notification\Notifications\Channels;

use Illuminate\Notifications\Channels\DatabaseChannel as BaseDatabaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Arr;
use Modules\Broadcast\Notifications\BroadcastNotification;

class DatabaseChannel extends BaseDatabaseChannel
{
    /**
     * Build an array payload for the DatabaseNotification Model.
     *
     * @param mixed                                        $notifiable
     * @param Notification|\App\Notifications\Notification $notification
     *
     * @return array
     * @throws \App\Exceptions\MethodMustBeOverriddenException
     */
    protected function buildPayload($notifiable, Notification $notification): array
    {
        $payload = parent::buildPayload($notifiable, $notification);
        Arr::set($payload, 'notification_category_id', $notification->getCategoryId());
        $payload = $this->addBroadcastId($notification, $payload);

        return $payload;
    }

    /**
     * @param Notification $notification
     * @param array        $payload
     *
     * @return array
     */
    private function addBroadcastId(Notification $notification, array $payload): array
    {
        if (!($notification instanceof BroadcastNotification)) {
            return $payload;
        }

        Arr::set($payload, 'type_id', $notification->getBroadcast()->id);

        return $payload;
    }
}
