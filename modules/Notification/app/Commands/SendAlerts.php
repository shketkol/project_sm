<?php

namespace Modules\Notification\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Log\Logger;
use Modules\Notification\Models\Alert;
use Modules\Notification\Models\Contracts\RepeatableAlert;
use Modules\Notification\Models\Contracts\Alert as AlertContract;
use Modules\User\Repositories\Contracts\UserRepository;

class SendAlerts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'platform:alert:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email alerts to the users';

    /**
     * @var Alert
     */
    protected $alert;

    /**
     * @var Carbon
     */
    protected $carbon;


    /**
     * @var Dispatcher
     */
    protected $event;

    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var UserRepository
     */
    protected $userRepository;


    /**
     * SendAlerts constructor.
     * @param Alert $alert
     * @param Carbon $carbon
     * @param Dispatcher $event
     * @param UserRepository $userRepository
     * @param Logger $log
     */
    public function __construct(
        Alert $alert,
        Carbon $carbon,
        Dispatcher $event,
        UserRepository $userRepository,
        Logger $log
    ) {
        parent::__construct();

        $this->alert = $alert;
        $this->carbon = $carbon;
        $this->event = $event;
        $this->log = $log;
        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->alert->all()->each(function ($alert) {
            if (!$this->check($alert)) {
                return;
            }

            $this->log->info("Alert #{$alert->id} will be sent.", [
                'type' => $alert->type->name,
                'item' => $alert->item_type,
                'id'   => $alert->item_id,
            ]);

            $this->send($alert);
        });
    }

    /**
     * Check if alert has to be sent.
     *
     * @param  AlertContract $alert
     * @return bool
     */
    protected function check(AlertContract $alert): bool
    {
        return $alert->ready && $alert->item;
    }

    /**
     * @param AlertContract $alert
     *
     * @return void
     * @throws \Exception
     */
    protected function send(AlertContract $alert): void
    {
        $this->event->dispatch('alert.' . $alert->type->name, [
            'alert' => $alert,
        ]);

        if (!$alert instanceof RepeatableAlert) {
            $alert->delete();
            return;
        }

        if (!$alert->repeat()) {
            $alert->delete();
        }
    }
}
