<?php

namespace Modules\Notification\Repositories;

use App\Repositories\Repository;
use Modules\Notification\Models\EmailHistory;

class EmailHistoryRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return EmailHistory::class;
    }
}
