<?php

namespace Modules\Notification\Repositories;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;
use Modules\Notification\Models\NotificationCategory;

class NotificationCategoryRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return NotificationCategory::class;
    }
}
