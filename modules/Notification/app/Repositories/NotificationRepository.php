<?php

namespace Modules\Notification\Repositories;

use App\Repositories\Repository;
use Modules\Notification\Models\Notification;

class NotificationRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Notification::class;
    }
}
