<?php

namespace Modules\Notification\Repositories;

use App\Repositories\Repository;
use Modules\Notification\Models\Reminder;

class ReminderRepository extends Repository
{
    /**
     * @return string
     */
    public function model()
    {
        return Reminder::class;
    }

    /**
     * @param int $userId
     *
     * @return Reminder|null
     */
    public function findUsersLastReminder(int $userId): ?Reminder
    {
        return $this
            ->orderBy('created_at', 'DESC')
            ->findByField('user_id', $userId)
            ->first();
    }
}
