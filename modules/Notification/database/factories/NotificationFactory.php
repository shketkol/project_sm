<?php

namespace Modules\Notification\Database\Factories;

use App\Factories\Factory;
use Modules\Notification\Models\Notification;
use Modules\Notification\Models\NotificationCategory;
use Modules\Notification\Notifications\FakeNotification;
use Modules\User\Models\User;

class NotificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'id'                       => $this->faker->uuid,
            'notification_category_id' => rand(NotificationCategory::ID_ACCOUNT, NotificationCategory::ID_TRANSACTION),
            'type'                     => FakeNotification::class,
            'notifiable_type'          => User::class,
            'notifiable_id'            => User::factory(),
            'data'                     => [
                'first'  => $this->faker->sentence,
                'second' => $this->faker->sentence,
                'third'  => $this->faker->sentence,
            ],
            'read_at'                  => null,
        ];
    }
}
