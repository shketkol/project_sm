<?php

namespace Modules\Notification\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Notification\Policies\NotificationPolicy;
use Modules\User\Models\Role;

class NotificationPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [],
        Role::ID_ADMIN_READ_ONLY => [],
        Role::ID_ADVERTISER      => [
            NotificationPolicy::PERMISSION_LIST_NOTIFICATION,
            NotificationPolicy::PERMISSION_VIEW_NOTIFICATION,
        ],
    ];
}
