<?php

namespace Modules\Notification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Notification\Models\NotificationCategory;

class NotificationCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $categories = [
            [
                'id'   => NotificationCategory::ID_ACCOUNT,
                'name' => NotificationCategory::ACCOUNT,
            ],
            [
                'id'   => NotificationCategory::ID_REPORT,
                'name' => NotificationCategory::REPORT,
            ],
            [
                'id'   => NotificationCategory::ID_CAMPAIGN,
                'name' => NotificationCategory::CAMPAIGN,
            ],
            [
                'id'   => NotificationCategory::ID_CREATIVE,
                'name' => NotificationCategory::CREATIVE,
            ],
            [
                'id'   => NotificationCategory::ID_TRANSACTION,
                'name' => NotificationCategory::TRANSACTION,
            ],
        ];

        foreach ($categories as $category) {
            NotificationCategory::updateOrCreate(
                ['id' => Arr::get($category, 'id')],
                $category
            );
        }
    }
}
