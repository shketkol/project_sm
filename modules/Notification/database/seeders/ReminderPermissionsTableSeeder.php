<?php

namespace Modules\Notification\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Notification\Policies\ReminderPolicy;
use Modules\User\Models\Role;

class ReminderPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [],
        Role::ID_ADMIN_READ_ONLY => [],
        Role::ID_ADVERTISER      => [
            ReminderPolicy::PERMISSION_DELETE_REMINDER,
        ],
    ];
}
