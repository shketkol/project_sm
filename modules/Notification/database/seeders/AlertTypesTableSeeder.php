<?php

namespace Modules\Notification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Notification\Models\AlertType;

class AlertTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $alerts = [
            [
                'id'             => AlertType::ID_CREATIVE_MISSING,
                'name'           => AlertType::CREATIVE_MISSING,
                'minutes_before' => 3 * 24 * 60, // 3 days
            ],
            [
                'id'             => AlertType::ID_CANT_GO_LIVE,
                'name'           => AlertType::CANT_GO_LIVE,
                'minutes_before' => 0,
            ],
            [
                'id'             => AlertType::ID_FAILED_PAYMENT,
                'name'           => AlertType::FAILED_PAYMENT,
                'minutes_before' => 0,
            ],
        ];

        foreach ($alerts as $alert) {
            AlertType::firstOrCreate(Arr::only($alert, ['id', 'name']))->update($alert);
        }
    }
}
