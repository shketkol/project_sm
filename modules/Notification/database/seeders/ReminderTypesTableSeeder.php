<?php

namespace Modules\Notification\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Notification\Models\ReminderType;

class ReminderTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (ReminderType::TYPES as $id => $type) {
            $reminderType = [
                'id'   => $id,
                'name' => $type,
            ];
            ReminderType::firstOrCreate($reminderType)->update($reminderType);
        }
    }
}
