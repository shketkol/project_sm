<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class FixPsr4IssuesInDb extends Migration
{
    /**
     * Fully qualified class names
     *
     * @var string[][]
     */
    private $fqcns = [
        [
            'old' => 'Modules\\Creative\\app\\Notifications\\Advertiser\\Removed',
            'new' => 'Modules\\Creative\\Notifications\\Advertiser\\Removed',
        ],
        [
            'old' => 'Modules\\Campaign\\app\\Notifications\\Admin\\CampaignStatusChangedNotification',
            'new' => 'Modules\\Campaign\\Notifications\\Admin\\CampaignStatusChangedNotification',
        ],
    ];


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        foreach ($this->fqcns as $fqcn) {
            DB::table('notifications')
                ->where('type', Arr::get($fqcn, 'old'))
                ->update(['type' => Arr::get($fqcn, 'new')]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        foreach ($this->fqcns as $fqcn) {
            DB::table('notifications')
                ->where('type', Arr::get($fqcn, 'new'))
                ->update(['type' => Arr::get($fqcn, 'old')]);
        }
    }
}
