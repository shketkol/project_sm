<?php

use Modules\Targeting\Models\DeviceGroup;

return [
    // Device groups that are supported by application.
    'groups' => [
        DeviceGroup::NAME_LIVING_ROOM,
        DeviceGroup::NAME_MOBILE,
        DeviceGroup::NAME_COMPUTER,
    ],
];
