<?php

// Map HAAPI genders props to translation codes used at platform
return [
    'ages'    => 'all',
    'all'     => 'all',
    'adults'  => 'all',
    'males'   => 'males',
    'females' => 'females',
    'teens'   => 'all',
];
