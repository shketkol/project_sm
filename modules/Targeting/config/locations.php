<?php

return [
    'max_count_zip_codes' => 400,
    'max_filesize'        => 5120,
    'csv_regexp'        => "/[;,\n]/",
];
