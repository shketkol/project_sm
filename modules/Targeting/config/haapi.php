<?php

return [
    'targeting' => [
        /*
         * 5 minutes is maximum expected time for the most time sufficient import with max limit
         * Import speed approximately:
         * - 50 000 records per minute with `targeting.values_list.limit` set to 1000
         * - 14 000 records per minute with `targeting.values_list.limit` set to 100
         */
        'timeout' => env('HAAPI_TARGETING_TIMEOUT', 300), // 5m

        'types_list'  => [
            'limit' => env('HAAPI_TARGETING_TYPES_LIST_LIMIT', 200), // 1000 is max allowed by haapi
        ],
        'values_list' => [
            'limit' => env('HAAPI_TARGETING_VALUES_LIST_LIMIT', 1000), // 1000 is max allowed by haapi
        ],
    ],
];
