<?php

return [
    'limit' => env('TARGETING_SEARCH_LIMIT', 200),
];
