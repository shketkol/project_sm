<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateCampaignDeviceGroupsTableAddDefaultForExcludedField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::create('campaign_device_groups', function (Blueprint $table) {
        Schema::table('campaign_device_groups', function ($table) {
            $table->boolean('excluded')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_device_groups', function ($table) {
            $table->boolean('excluded')->default(null)->change();
        });
    }
}
