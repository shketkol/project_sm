<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAgeGroupsTableAllMalesFemales extends Migration
{
    /**
     * "All females" and "All Males" ages options should be related to "females" and "males" genders.
     * @var array
     */
    private $changes = [
        'All Females' => 'females',
        'All Males' => 'males',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->changes as $ageGroupName => $genderName) {
            $gender = DB::table('genders')->where('name', $genderName)->first();

            if (!$gender) {
                continue;
            }

            DB::table('age_groups')->where('name', $ageGroupName)->update(['gender_id' => $gender->id]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
