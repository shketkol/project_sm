<?php

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\CampaignStep;
use Modules\Targeting\Models\AgeGroup;

class RenameAgeGroups extends Migration
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->databaseManager = app(DatabaseManager::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function up()
    {
        $this->databaseManager->beginTransaction();

        try {
            foreach (AgeGroup::all() as $item) {
                $name = explode(' ', $item->name);
                $item->name = $name[1];
                $item->save();
            }
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        $this->databaseManager->beginTransaction();

        try {
            foreach (AgeGroup::all() as $item) {
                $genderName = $item->gender->name;
                $item->name = ucfirst($genderName . ' ' . $item->name);
                $item->save();
            }
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }
}
