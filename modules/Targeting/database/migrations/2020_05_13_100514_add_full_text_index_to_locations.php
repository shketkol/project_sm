<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;
use Modules\Targeting\Models\Location;

class AddFullTextIndexToLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Artisan::call('scout:mysql-index', ['model' => Location::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Artisan::call('scout:mysql-index', [
            'model'  => Location::class,
            '--drop' => true,
        ]);
    }
}
