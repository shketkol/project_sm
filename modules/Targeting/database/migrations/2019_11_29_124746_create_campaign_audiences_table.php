<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignAudiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('campaign_audiences', function (Blueprint $table) {
            $table->primary(['audience_id', 'campaign_id']);

            $table->bigInteger('audience_id')->unsigned();
            $table->foreign('audience_id')->references('id')->on('audiences')->onDelete('cascade');

            $table->bigInteger('campaign_id')->unsigned();
            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('campaign_audiences');
    }
}
