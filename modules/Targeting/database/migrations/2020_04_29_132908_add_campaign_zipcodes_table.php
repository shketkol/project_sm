<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampaignZipcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('campaign_zipcodes', function (Blueprint $table) {
            $table->primary(['zipcode_id', 'campaign_id']);

            $table->bigInteger('zipcode_id')->unsigned();
            $table->foreign('zipcode_id')->references('id')->on('zipcodes')->onDelete('cascade');

            $table->bigInteger('campaign_id')->unsigned();
            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');

            $table->boolean('excluded');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('campaign_zipcodes');
    }
}
