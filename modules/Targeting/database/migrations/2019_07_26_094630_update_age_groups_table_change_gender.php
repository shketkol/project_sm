<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAgeGroupsTableChangeGender extends Migration
{
    /**
     * Changes mapping ("Teens" and "Adults" age groups should become "All").
     *
     * @var array
     */
    private $changes = [
        'teens'  => 'all',
        'adults' => 'all',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->changes as $from => $to) {
            $fromModel = DB::table('genders')->where('name', $from)->first();
            $toModel = DB::table('genders')->where('name', $to)->first();

            if (!$fromModel || !$toModel) {
                continue;
            }

            DB::table('age_groups')->where('gender_id', $fromModel->id)->update(['gender_id' => $toModel->id]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
