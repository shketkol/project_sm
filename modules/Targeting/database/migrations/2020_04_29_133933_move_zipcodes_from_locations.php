<?php

use App\Traits\ConsoleOutput;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Modules\Targeting\Repositories\LocationRepository;
use Modules\Targeting\Repositories\ZipcodeRepository;

class MoveZipcodesFromLocations extends Migration
{
    use ConsoleOutput;

    /**
     * @var int
     */
    private const BATCH_SIZE = 1000;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var ZipcodeRepository
     */
    private $zipcodeRepository;

    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->databaseManager = app(DatabaseManager::class);
        $this->zipcodeRepository = app(ZipcodeRepository::class);
        $this->locationRepository = app(LocationRepository::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up(): void
    {
        $this->databaseManager->beginTransaction();

        try {
            $campaignZipcodes = $this->getCampaignZipcodesFromCampaignLocationsTable();
            $this->copyZipcodesFromLocationsTable();
            $this->deleteZipcodesFromLocationTable();
            $this->insertZipcodesSelectedByAdvertiserToCampaignZipcodesTable($campaignZipcodes);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * @param array $zipcodes
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function insertZipcodesSelectedByAdvertiserToCampaignZipcodesTable(array $zipcodes): void
    {
        $total = count($zipcodes);
        $this->info(sprintf('Total zipcodes to save in campaign_zipcodes table: %d.', $total));

        if ($total === 0) {
            return;
        }

        $this->startProgressBar($total);

        for ($counter = 0; $counter <= $total; $counter += self::BATCH_SIZE) {
            $slice = array_slice($zipcodes, $counter, self::BATCH_SIZE);
            $externalIds = array_column($slice, 'external_id');

            $zipcodesFromDB = DB::table('zipcodes')
                ->select(['id', 'external_id'])
                ->whereIn('external_id', $externalIds)
                ->get()
                ->keyBy('external_id')
                ->toArray();

            $campaignZipcodes = [];
            foreach ($slice as $data) {
                $campaignZipcodes[] = [
                    'zipcode_id'  => $zipcodesFromDB[$data->external_id]->id,
                    'campaign_id' => $data->campaign_id,
                    'excluded'    => $data->excluded,
                ];
            }

            $this->insertCampaignZipcodes($campaignZipcodes);

            $this->advanceProgressBar(count($slice));
        }

        $this->finishProgressBar();
    }

    /**
     * @param array $data
     */
    private function insertCampaignZipcodes(array $data): void
    {
        DB::table('campaign_zipcodes')->insert($data);
    }

    /**
     * Delete zipcodes from locations table
     * Zipcodes from campaign_locations would be also deleted with foreign key on delete
     */
    private function deleteZipcodesFromLocationTable(): void
    {
        DB::table('locations')->where('type', 'zipcode')->delete();
    }

    /**
     * Insert all zipcodes from locations table to zipcodes table
     */
    private function copyZipcodesFromLocationsTable(): void
    {
        $total = $this->getTotalZipcodesAmountInLocationsTable();
        $this->info(sprintf('Total zipcodes to move from locations table: %d.', $total));

        if ($total === 0) {
            return;
        }

        $this->startProgressBar($total);

        for ($counter = 0; $counter <= $total; $counter += self::BATCH_SIZE) {
            $zipcodes = $this->getZipcodesFromLocationsTable($counter);
            $this->zipcodeRepository->insert($zipcodes);

            $this->advanceProgressBar(self::BATCH_SIZE);
        }

        $this->finishProgressBar();
    }

    /**
     * Get total zipcodes count from locations table
     *
     * @return int
     */
    private function getTotalZipcodesAmountInLocationsTable(): int
    {
        return DB::table('locations')->where('type', 'zipcode')->count();
    }

    /**
     * @param int $start
     *
     * @return array
     */
    private function getZipcodesFromLocationsTable(int $start): array
    {
        $getZipcodesQuery = /** @lang MySQL */
            <<<MYSQL
select external_id, name, visible, created_at, updated_at
from locations
where locations.type = ?
limit ?, ?;
MYSQL;

        $records = DB::select($getZipcodesQuery, ['zipcode', $start, self::BATCH_SIZE]);

        // convert to array for insert
        return collect($records)->map(function ($record) {
            return (array)$record;
        })->toArray();
    }

    /**
     * Get all zipcode values from campaign_locations table
     *
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function getCampaignZipcodesFromCampaignLocationsTable(): array
    {
        $campaignZipcodesQuery = /** @lang MySQL */
            <<<SQL
select locations.external_id, campaign_locations.campaign_id, campaign_locations.excluded
from locations
left join campaign_locations on campaign_locations.location_id = locations.id
where locations.type = ?
and campaign_locations.campaign_id is not null;
SQL;

        $result = DB::select($campaignZipcodesQuery, ['zipcode']);

        $this->info(sprintf('Zipcodes used in campaigns: %d.', count($result)));

        return $result;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down(): void
    {
        $this->databaseManager->beginTransaction();

        try {
            $campaignZipcodes = $this->getCampaignZipcodesFromCampaignZipcodesTable();
            $this->copyZipcodesFromZipcodesTable();
            $this->deleteZipcodesFromZipcodesTable();
            $this->insertZipcodesSelectedByAdvertiserToCampaignLocationsTable($campaignZipcodes);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * Get all zipcode values from campaign_zipcodes table
     *
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function getCampaignZipcodesFromCampaignZipcodesTable(): array
    {
        $campaignZipcodesQuery = /** @lang MySQL */
            <<<SQL
select zipcodes.external_id, campaign_zipcodes.campaign_id, campaign_zipcodes.excluded
from zipcodes
left join campaign_zipcodes on campaign_zipcodes.zipcode_id = zipcodes.id
where campaign_zipcodes.campaign_id is not null;
SQL;

        $result = DB::select($campaignZipcodesQuery);

        $this->info(sprintf('Zipcodes used in campaigns: %d.', count($result)));

        return $result;
    }

    /**
     * Insert all zipcodes from zipcodes table to locations table
     */
    private function copyZipcodesFromZipcodesTable(): void
    {
        $total = $this->getTotalZipcodesAmountInZipcodesTable();
        $this->info(sprintf('Total zipcodes to move from zipcodes table: %d.', $total));

        if ($total === 0) {
            return;
        }

        $this->startProgressBar($total);

        for ($counter = 0; $counter <= $total; $counter += self::BATCH_SIZE) {
            $zipcodes = $this->getZipcodesFromZipcodesTable($counter);
            $this->locationRepository->insert($zipcodes);

            $this->advanceProgressBar(self::BATCH_SIZE);
        }

        $this->finishProgressBar();
    }

    /**
     * Get total zipcodes count from zipcodes table
     *
     * @return int
     */
    private function getTotalZipcodesAmountInZipcodesTable(): int
    {
        return DB::table('zipcodes')->count();
    }

    /**
     * @param int $start
     *
     * @return array
     */
    private function getZipcodesFromZipcodesTable(int $start): array
    {
        $records = DB::table('zipcodes')
            ->select([
                'external_id',
                'name',
                'visible',
                'created_at',
                'updated_at',
                DB::raw('"zipcode" as type'),
            ])
            ->offset($start)
            ->take(self::BATCH_SIZE)
            ->get();

        // convert to array for insert
        return collect($records)->map(function ($record) {
            return (array)$record;
        })->toArray();
    }

    /**
     * Delete zipcodes from zipcodes table
     * Zipcodes from campaign_zipcodes would be also deleted with foreign key on delete
     */
    private function deleteZipcodesFromZipcodesTable(): void
    {
        DB::table('zipcodes')->delete();
    }

    /**
     * @param array $zipcodes
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function insertZipcodesSelectedByAdvertiserToCampaignLocationsTable(array $zipcodes): void
    {
        $total = count($zipcodes);
        $this->info(sprintf('Total zipcodes to save in campaign_locations table: %d.', $total));

        if ($total === 0) {
            return;
        }

        $this->startProgressBar($total);

        for ($counter = 0; $counter <= $total; $counter += self::BATCH_SIZE) {
            $slice = array_slice($zipcodes, $counter, self::BATCH_SIZE);
            $externalIds = array_column($slice, 'external_id');

            $zipcodesFromDB = DB::table('locations')
                ->select(['id', 'external_id'])
                ->whereIn('external_id', $externalIds)
                ->get()
                ->keyBy('external_id')
                ->toArray();

            $campaignZipcodes = [];
            foreach ($slice as $data) {
                $campaignZipcodes[] = [
                    'location_id'  => $zipcodesFromDB[$data->external_id]->id,
                    'campaign_id' => $data->campaign_id,
                    'excluded'    => $data->excluded,
                ];
            }

            DB::table('campaign_locations')->insert($campaignZipcodes);

            $this->advanceProgressBar(count($slice));
        }

        $this->finishProgressBar();
    }
}
