<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;
use Modules\Targeting\Models\Genre;

class AddFullTextIndexToGenres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Artisan::call('scout:mysql-index', ['model' => Genre::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Artisan::call('scout:mysql-index', [
            'model'  => Genre::class,
            '--drop' => true,
        ]);
    }
}
