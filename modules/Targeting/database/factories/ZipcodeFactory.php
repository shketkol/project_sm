<?php

namespace Modules\Targeting\Database\Factories;

use App\Factories\Factory;
use Carbon\Carbon;
use Modules\Targeting\Models\Zipcode;

class ZipcodeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Zipcode::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'external_id' => $this->faker->uuid,
            'name'        => str_pad(rand(0, 99999), 5, '0', STR_PAD_LEFT),
            'visible'     => true,
            'created_at'  => Carbon::now(),
            'updated_at'  => null,
        ];
    }
}
