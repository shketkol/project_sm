<?php

namespace Modules\Targeting\Database\Factories;

use App\Factories\Factory;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Modules\Targeting\Models\DeviceGroup;

class DeviceGroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DeviceGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name'       => Str::snake($this->faker->words(2, true)),
            'visible'    => true,
            'created_at' => Carbon::now(),
            'updated_at' => null,
        ];
    }
}
