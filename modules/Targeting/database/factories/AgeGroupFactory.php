<?php

namespace Modules\Targeting\Database\Factories;

use App\Factories\Factory;
use Carbon\Carbon;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Gender;

class AgeGroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AgeGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'external_id' => $this->faker->uuid,
            'name'        => $this->faker->catchPhrase,
            'gender_id'   => Gender::factory(),
            'visible'     => true,
            'created_at'  => Carbon::now(),
            'updated_at'  => null,
        ];
    }
}
