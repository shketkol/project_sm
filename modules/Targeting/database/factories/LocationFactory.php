<?php

namespace Modules\Targeting\Database\Factories;

use App\Factories\Factory;
use Carbon\Carbon;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Type;

class LocationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Location::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'external_id' => $this->faker->uuid,
            'name'        => $this->faker->catchPhrase,
            'type'        => $this->faker->randomElement([
                Type::HAAPI_DMA_NAME,
                Type::HAAPI_CITY_NAME,
                Type::HAAPI_STATE_NAME,
            ]),
            'visible'     => true,
            'created_at'  => Carbon::now(),
            'updated_at'  => null,
        ];
    }
}
