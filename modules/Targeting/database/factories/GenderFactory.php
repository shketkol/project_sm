<?php

namespace Modules\Targeting\Database\Factories;

use App\Factories\Factory;
use Carbon\Carbon;
use Modules\Targeting\Models\Gender;

class GenderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Gender::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name'       => $this->faker->catchPhrase,
            'created_at' => Carbon::now(),
            'updated_at' => null,
        ];
    }
}
