<?php

namespace Modules\Targeting\Database\Factories;

use App\Factories\Factory;
use Carbon\Carbon;
use Modules\Targeting\Models\Audience;

class AudienceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Audience::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $firstName = $this->faker->catchPhrase;
        $secondName = $this->faker->catchPhrase;
        $thirdName = $this->faker->catchPhrase;

        return [
            'external_id' => $this->faker->uuid,
            'name'        => $thirdName,
            'tiers'       => implode('.', [$firstName, $secondName, $thirdName]),
            'visible'     => true,
            'created_at'  => Carbon::now(),
            'updated_at'  => null,
        ];
    }
}
