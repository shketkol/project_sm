<?php

namespace Modules\Targeting\Exceptions;

use App\Exceptions\BaseException;
use Throwable;

class UpdateFailedException extends BaseException
{
    /**
     * @param string         $message
     * @param Throwable|null $previous
     *
     * @return UpdateFailedException
     */
    public static function create(string $message, Throwable $previous = null): self
    {
        return new self($message, 0, $previous);
    }
}
