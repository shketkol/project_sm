<?php

namespace Modules\Targeting\Exceptions;

use App\Exceptions\BaseException;

class TypeNotFoundException extends BaseException
{
    /**
     * @param string $name
     *
     * @return TypeNotFoundException
     */
    public static function create(string $name): self
    {
        return new self(sprintf('Type with name "%s" was not found.', $name));
    }
}
