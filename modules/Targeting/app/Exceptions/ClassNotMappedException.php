<?php

namespace Modules\Targeting\Exceptions;

use App\Exceptions\BaseException;

class ClassNotMappedException extends BaseException
{
    /**
     * @param string $name
     *
     * @return self
     */
    public static function create(string $name): self
    {
        return new self(sprintf('Class "%s" not mapped to be used in search.', $name));
    }
}
