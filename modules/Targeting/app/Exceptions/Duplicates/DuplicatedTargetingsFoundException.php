<?php

namespace Modules\Targeting\Exceptions\Duplicates;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Targeting\Repositories\Contracts\TargetingRepository;

abstract class DuplicatedTargetingsFoundException extends BaseException
{
    /**
     * @var array
     */
    protected $duplicates = [];

    /**
     * @var TargetingRepository
     */
    protected $repository;

    /**
     * @param array $duplicates
     *
     * @return DuplicatedTargetingsFoundException
     */
    abstract public static function create(array $duplicates);

    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     * @return Response|JsonResponse
     */
    public function render(Request $request)
    {
        $message = $this->createMessage();

        if ($request->expectsJson()) {
            return response()->json(['data' => [
                'message' => $message,
            ]], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response($message, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @return string
     */
    protected function createMessage(): string
    {
        $names = $this->findDuplicatedNames();
        $names = implode(', ', $names);
        $this->message = trans('targeting::messages.duplicates', ['names' => $names]);

        return $this->message;
    }

    /**
     * @return array
     */
    protected function findDuplicatedNames(): array
    {
        return $this->repository->findWhereIn('id', array_keys($this->duplicates))
            ->pluck('name')
            ->toArray();
    }
}
