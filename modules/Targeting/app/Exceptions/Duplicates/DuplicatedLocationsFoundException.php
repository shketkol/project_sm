<?php

namespace Modules\Targeting\Exceptions\Duplicates;

use Modules\Targeting\Repositories\Contracts\LocationRepository;

class DuplicatedLocationsFoundException extends DuplicatedTargetingsFoundException
{
    /**
     * @param array $duplicates
     *
     * @return self
     */
    public static function create(array $duplicates): self
    {
        $exception = new self();
        $exception->duplicates = $duplicates;
        $exception->repository = app(LocationRepository::class);
        $exception->message = sprintf('Duplicated location ids found: "%s".', implode(', ', $duplicates));

        return $exception;
    }
}
