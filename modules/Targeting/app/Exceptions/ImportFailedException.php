<?php

namespace Modules\Targeting\Exceptions;

use App\Exceptions\BaseException;
use Throwable;

class ImportFailedException extends BaseException
{
    /**
     * @param string         $message
     * @param Throwable|null $previous
     *
     * @return ImportFailedException
     */
    public static function create(string $message, Throwable $previous = null): self
    {
        return new self($message, 0, $previous);
    }
}
