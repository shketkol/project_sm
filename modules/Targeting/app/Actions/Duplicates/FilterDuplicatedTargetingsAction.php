<?php

namespace Modules\Targeting\Actions\Duplicates;

use Modules\Targeting\Exceptions\Duplicates\DuplicatedTargetingsFoundException;

class FilterDuplicatedTargetingsAction
{
    /**
     * @var string
     */
    protected $column;

    /**
     * @var string|DuplicatedTargetingsFoundException
     */
    protected $exceptionClass;

    /**
     * Remove duplicated values and check if same targetings included and excluded at the same time.
     *
     * @param array $data
     *
     * @return array
     * @throws DuplicatedTargetingsFoundException
     */
    public function handle(array $data): array
    {
        $data = $this->removeDuplicates($data);
        $duplicates = $this->findDuplicates($data, $this->column);

        if (count($duplicates) > 0) {
            throw $this->exceptionClass::create($duplicates);
        }

        return $data;
    }

    /**
     * Remove *full* duplicates (targeting_id and included fields are equal)
     *
     * @param array $data
     *
     * @return array
     */
    private function removeDuplicates(array $data): array
    {
        return array_unique($data, SORT_REGULAR);
    }

    /**
     * Find duplicated targetings where (targeting_id is found with included = 0 and included = 1)
     *
     * @param array  $data
     * @param string $column
     *
     * @return array
     */
    private function findDuplicates(array $data, string $column): array
    {
        // find duplicated targetings with included and excluded values
        $targetings = array_column($data, $column);

        // count targeting ids to found if there are any targeting id is duplicated
        $countTargetings = array_count_values($targetings);

        // return targeting that are found more than once
        return array_filter($countTargetings, function ($count) {
            return $count > 1;
        });
    }
}
