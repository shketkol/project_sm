<?php

namespace Modules\Targeting\Actions\Duplicates;

use Modules\Targeting\Exceptions\Duplicates\DuplicatedLocationsFoundException;

class FilterDuplicatedLocationsAction extends FilterDuplicatedTargetingsAction
{
    /**
     * @var string
     */
    protected $column = 'location_id';

    /**
     * @var string
     */
    protected $exceptionClass = DuplicatedLocationsFoundException::class;
}
