<?php

namespace Modules\Targeting\Actions;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Exceptions\TypeNotFoundException;
use Modules\Targeting\Models\TargetingValue;
use Modules\Targeting\Models\Type;
use Modules\Targeting\Repositories\Contracts\TypeRepository;

class GetCampaignTargetingsAction
{
    /**
     * @var TypeRepository
     */
    protected $typeRepository;

    /**
     * GetCampaignTargetingsAction constructor.
     *
     * @param TypeRepository           $typeRepository
     */
    public function __construct(TypeRepository $typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     */
    public function handle(Campaign $campaign): array
    {
        $targetings = [
            [
                'collection' => $campaign->genres()->visible()->get(),
                'name'       => Type::HAAPI_GENRE_NAME,
            ],
            [
                'collection' => $campaign->zipcodes()->visible()->get(),
                'name'       => Type::HAAPI_ZIPCODE_NAME,
            ],
            [
                'collection' => $campaign->dma()->visible()->get(),
                'name'       => Type::HAAPI_DMA_NAME,
            ],
            [
                'collection' => $campaign->cities()->visible()->get(),
                'name'       => Type::HAAPI_CITY_NAME,
            ],
            [
                'collection' => $campaign->states()->visible()->get(),
                'name'       => Type::HAAPI_STATE_NAME,
            ],
            [
                'collection' => $campaign->targetingAgeGroups()->visible()->get(),
                'name'       => Type::HAAPI_AGE_GROUP_NAME,
            ],
            [
                'collection' => $campaign->getDevices(),
                'name'       => Type::HAAPI_DEVICE_NAME,
            ],
            [
                'collection' => $campaign->audiences()->visible()->get(),
                'name'       => Type::HAAPI_AUDIENCE_NAME,
            ],
        ];

        $result = [];
        foreach ($targetings as $targeting) {
            $data = $this->getTargetings(Arr::get($targeting, 'collection'), Arr::get($targeting, 'name'));
            if (count($data)) {
                $result[] = $data;
            }
        }

        return $result;
    }

    /**
     * @param Collection $targetings
     * @param string     $name
     *
     * @return array
     * @throws TypeNotFoundException
     */
    private function getTargetings(Collection $targetings, string $name): array
    {
        if (!$targetings->count()) {
            return [];
        }

        return [
            'typeId' => $this->getTypeExternalId($name),
            'values' => $this->formatTargetingValues($targetings),
        ];
    }

    /**
     * @param string $name
     *
     * @return string
     * @throws TypeNotFoundException
     */
    private function getTypeExternalId(string $name): string
    {
        /** @var Type $type */
        $type = $this->typeRepository->findWhere(['name' => $name])->first();

        if (is_null($type)) {
            throw TypeNotFoundException::create($name);
        }

        return $type->external_id;
    }

    /**
     * @param Collection|TargetingValue[] $collection
     *
     * @return array
     */
    private function formatTargetingValues(Collection $collection): array
    {
        return $collection->map(function (TargetingValue $targeting) {
            return [
                'value'     => $targeting->external_id,
                'exclusion' => (int) $targeting->getIsExcluded(),
            ];
        })->toArray();
    }
}
