<?php

namespace Modules\Targeting\Actions\Audience;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Audience\Traits\StructureAudienceFormat;
use Modules\Targeting\Actions\UpdateValuesAction;
use Modules\Targeting\Repositories\Contracts\AudienceRepository;
use Psr\Log\LoggerInterface;

class UpdateAudienceAction extends UpdateValuesAction
{
    use StructureAudienceFormat;

    /**
     * @param AudienceRepository $repository
     * @param LoggerInterface    $log
     */
    public function __construct(AudienceRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }

    /**
     * @param TargetingValuesCollection $values
     */
    public function handle(TargetingValuesCollection $values): void
    {
        $values = $this->prepareStructure($values->toArray());
        $data = $this->filterChanged($values);

        if ($data->isEmpty()) {
            return;
        }

        foreach ($data as $value) {
            $this->repository->updateByExternalId($value, Arr::get($value, 'external_id'));
        }
    }
}
