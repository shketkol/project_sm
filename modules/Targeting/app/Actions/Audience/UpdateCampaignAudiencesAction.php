<?php

namespace Modules\Targeting\Actions\Audience;

use Illuminate\Database\DatabaseManager;
use Modules\Campaign\Actions\UpdateCampaignBudgetAction;
use Modules\Campaign\Actions\UpdateCampaignStepAction;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Targeting\Actions\Traits\PrepareTargetingsToSync;
use Modules\Targeting\Repositories\AudienceRepository;
use Psr\Log\LoggerInterface;

class UpdateCampaignAudiencesAction
{
    use PrepareTargetingsToSync;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UpdateCampaignStepAction
     */
    private $stepAction;

    /**
     * @var UpdateCampaignBudgetAction
     */
    private $budgetAction;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var AudienceRepository
     */
    protected $repository;

    /**
     * @param DatabaseManager          $databaseManager
     * @param UpdateCampaignStepAction $stepAction
     * @param LoggerInterface          $log
     * @param AudienceRepository       $repository
     */
    public function __construct(
        DatabaseManager $databaseManager,
        UpdateCampaignStepAction $stepAction,
        UpdateCampaignBudgetAction $budgetAction,
        LoggerInterface $log,
        AudienceRepository $repository
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->budgetAction = $budgetAction;
        $this->stepAction = $stepAction;
        $this->repository = $repository;
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Throwable
     */
    public function handle(Campaign $campaign, array $data): Campaign
    {
        $data = $this->removeNonVisibleTargetingsFromNamedArray($campaign, $data, 'audience_id');
        $this->updateAudiences($campaign, $data);
        $this->budgetAction->handle($campaign);

        return $this->updateCampaignStep($campaign);
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    private function updateAudiences(Campaign $campaign, array $data): void
    {
        $this->log->info('Started sync targeting audiences in campaign.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $campaign->audiences()->detach();
            $campaign->audiences()->attach($data);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('Completed sync targeting audiences in campaign.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function updateCampaignStep(Campaign $campaign): Campaign
    {
        return $this->stepAction->handle($campaign, CampaignStep::ID_AUDIENCES);
    }
}
