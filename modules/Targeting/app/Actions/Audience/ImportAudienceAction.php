<?php

namespace Modules\Targeting\Actions\Audience;

use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Audience\Traits\StructureAudienceFormat;
use Modules\Targeting\Actions\ImportValuesAction;
use Modules\Targeting\Repositories\Contracts\AudienceRepository;
use Psr\Log\LoggerInterface;

class ImportAudienceAction extends ImportValuesAction
{
    use StructureAudienceFormat;

    /**
     * @param AudienceRepository $repository
     * @param LoggerInterface    $log
     */
    public function __construct(AudienceRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }

    /**
     * @param TargetingValuesCollection $values
     *
     * @return bool
     */
    public function handle(TargetingValuesCollection $values): bool
    {
        $data = $this->filterExisted($values);

        if ($data->isEmpty()) {
            return true;
        }

        $audiences = $this->prepareStructure($data);

        return $this->repository->insert($audiences);
    }
}
