<?php

namespace Modules\Targeting\Actions\Audience;

use Modules\Haapi\Actions\Campaign\TargetingValuesList;
use Modules\Targeting\Actions\FetchValuesAction;
use Modules\Targeting\Models\Type;
use Modules\Targeting\Repositories\Contracts\TypeRepository;
use Psr\Log\LoggerInterface;

class FetchAudienceAction extends FetchValuesAction
{
    /**
     * FetchAudienceAction constructor.
     *
     * @param TargetingValuesList $fetchValuesAction
     * @param LoggerInterface     $log
     * @param TypeRepository      $typeRepository
     */
    public function __construct(
        TargetingValuesList $fetchValuesAction,
        LoggerInterface $log,
        TypeRepository $typeRepository
    ) {
        parent::__construct($fetchValuesAction, $log, $typeRepository, Type::HAAPI_AUDIENCE_NAME);
    }
}
