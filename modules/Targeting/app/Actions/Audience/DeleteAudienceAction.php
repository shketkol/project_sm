<?php

namespace Modules\Targeting\Actions\Audience;

use Modules\Targeting\Actions\Audience\Traits\StructureAudienceFormat;
use Modules\Targeting\Actions\DeleteValuesAction;
use Modules\Targeting\Repositories\Contracts\AudienceRepository;
use Psr\Log\LoggerInterface;

class DeleteAudienceAction extends DeleteValuesAction
{
    use StructureAudienceFormat;

    /**
     * @param AudienceRepository $repository
     * @param LoggerInterface    $log
     */
    public function __construct(
        AudienceRepository $repository,
        LoggerInterface $log
    ) {
        parent::__construct($repository, $log);
    }
}
