<?php

namespace Modules\Targeting\Actions\Audience\Traits;

use Illuminate\Support\Arr;

trait StructureAudienceFormat
{
    /**
     * @param iterable $values
     *
     * @return array
     */
    private function prepareStructure(iterable $values): array
    {
        $result = [];
        foreach ($values as $value) {
            $result[] = array_merge($this->getAdditionalFields(), $this->formatTiers($value));
        }

        return $result;
    }

    /**
     * @param array $value
     *
     * @return array
     */
    private function formatTiers(array $value): array
    {
        $tiers = Arr::get($value['hierarchy'], 'tiers', []);
        $tiers = $this->convert($tiers);
        $path = implode('.', $tiers);

        return [
            'name'        => Arr::last($tiers),
            'tiers'       => $path,
            'external_id' => Arr::get($value, 'external_id'),
            'visible'     => Arr::get($value, 'visible'),
        ];
    }

    /**
     * @param array $tiers
     *
     * @return array
     */
    private function convert(array $tiers): array
    {
        return array_map(function (string $tier, int $key): string {
            $tier = trim($tier);

            //format subcategories "OUTDOOR SPORTS/ADVENTURE ENTHUSIASTS -> Outdoor Sports / Adventure Enthusiasts"
            if ($key) {
                $tier = $this->addSpaces($tier);
            }

            return $tier;
        }, $tiers, array_keys($tiers));
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function addSpaces(string $name): string
    {
        return str_replace('/', ' / ', $name);
    }
}
