<?php

namespace Modules\Targeting\Actions\Audience;

use App\Helpers\ArrayHelper;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Repositories\Contracts\AudienceRepository;

class ShowAudienceTiersAction
{
    use ArrayHelper;

    /**
     * @var AudienceRepository
     */
    private $repository;

    /**
     * @param AudienceRepository $repository
     */
    public function __construct(AudienceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     */
    public function handle(Campaign $campaign): array
    {
        /** @var Collection|Audience[] $all */
        $all = $this->repository->withSavedNonVisible($campaign)->all();

        $result = [];
        foreach ($all as $audience) {
            if (!(Arr::has($result, $audience->tiers) && $audience->id)) {
                Arr::set($result, $audience->tiers, (object)[
                    'id'   => $audience->id,
                    'path' => $audience->tiers,
                ]);
            }
        }

        return $this->format($result);
    }

    /**
     * @param array  $categories
     * @param string $path
     *
     * @return array
     */
    private function format(array $categories, string $path = ''): array
    {
        $result = [];
        foreach ($categories as $category => $data) {
            $currentPath = $path ? "{$path}.{$category}" : $category;

            $formatted = [
                'name'        => $category,
                'description' => $this->getDescription($currentPath),
            ];

            // handle children
            if (is_array($data)) {
                Arr::set($formatted, 'children', $this->sortByField($this->format($data, $currentPath), 'name'));
            }

            // set value
            if (!is_array($data)) {
                Arr::set($formatted, 'value', $data->id);
                Arr::set($formatted, 'path', $data->path);
            }

            $result[] = $formatted;
        }

        return $result;
    }

    /**
     * @param string $path
     *
     * @return mixed
     */
    protected function getDescription(string $path): string
    {
        $descriptions = [
            'Behavior'                => trans('campaign::labels.infotips.behavior'),
            'Demographics'            => trans('campaign::labels.infotips.demographics'),
            'Demographics.Milestones' => trans('campaign::labels.infotips.demographics_milestones'),
            'Interest'                => trans('campaign::labels.infotips.interest'),
            'Ownership'               => trans('campaign::labels.infotips.ownership'),
        ];

        return array_key_exists($path, $descriptions) ? $descriptions[$path] : '';
    }
}
