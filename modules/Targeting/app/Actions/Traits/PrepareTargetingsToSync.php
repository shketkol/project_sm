<?php

namespace Modules\Targeting\Actions\Traits;

use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Repositories\Contracts\TargetingRepository;

/**
 * @property TargetingRepository $repository
 */
trait PrepareTargetingsToSync
{
    /**
     * Check and remove manually added targetings if so exists.
     * For example targeting non visible in DB but Advertiser added it just changing the payload
     * This method works for simple arrays of ids: [1, 2, 3]
     *
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return array
     */
    private function removeNonVisibleTargetings(Campaign $campaign, array $data): array
    {
        return $this->repository->distinct()->withSavedNonVisible($campaign)
            ->findWhereIn('id', $data)
            ->pluck('id')
            ->toArray();
    }

    /**
     * Check and remove manually added targetings if so exists.
     * For example targeting non visible in DB but Advertiser added it just changing the payload
     * This method works for complex arrays of ids and inclusions: [["id": 1, "excluded": 0], ["id": 2, "excluded": 1]]
     *
     * @param Campaign $campaign
     * @param array    $data
     * @param string   $field
     *
     * @return array
     */
    private function removeNonVisibleTargetingsFromNamedArray(Campaign $campaign, array $data, string $field): array
    {
        $ids = array_column($data, $field);
        $allowedIds = $this->removeNonVisibleTargetings($campaign, $ids);

        $allowedData = [];
        foreach ($data as $value) {
            if (in_array(Arr::get($value, $field), $allowedIds)) {
                $allowedData[] = $value;
            }
        }

        return $allowedData;
    }
}
