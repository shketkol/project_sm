<?php

namespace Modules\Targeting\Actions\Device;

use Modules\Targeting\Actions\IndexValuesAction;
use Modules\Targeting\Repositories\DeviceGroupRepository;

class IndexDevicesAction extends IndexValuesAction
{
    /**
     * @param DeviceGroupRepository $repository
     */
    public function __construct(DeviceGroupRepository $repository)
    {
        parent::__construct($repository);
    }
}
