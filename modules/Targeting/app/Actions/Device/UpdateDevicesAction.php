<?php

namespace Modules\Targeting\Actions\Device;

use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Device\Traits\ActionsHelper;
use Modules\Targeting\Actions\UpdateValuesAction;
use Modules\Targeting\Repositories\Contracts\DeviceRepository;
use Psr\Log\LoggerInterface;

class UpdateDevicesAction extends UpdateValuesAction
{
    use ActionsHelper;

    /**
     * @param DeviceRepository $repository
     * @param LoggerInterface $log
     */
    public function __construct(DeviceRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }

    /**
     * @param TargetingValuesCollection $collection
     *
     * @return void
     */
    public function handle(TargetingValuesCollection $collection): void
    {
        $data = $this->prepareStructureForDTO($collection->toArray());
        parent::handle(TargetingValuesCollection::create($data));
    }
}
