<?php

namespace Modules\Targeting\Actions\Device\Traits;

use Illuminate\Support\Arr;

trait ActionsHelper
{
    /**
     * @param array $devices
     *
     * @return array
     */
    private function prepareStructure(array $devices): array
    {
        foreach ($devices as &$device) {
            Arr::forget($device, 'hierarchy');
        }
        return $devices;
    }

    /**
     * @param array $devices
     *
     * @return array
     */
    private function prepareStructureForDTO(array $devices): array
    {
        $devices = $this->prepareStructure($devices);
        foreach ($devices as &$device) {
            $device['id'] = $device['external_id'];
            Arr::forget($device, 'external_id');
        }
        return $devices;
    }
}
