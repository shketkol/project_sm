<?php

namespace Modules\Targeting\Actions\Device;

use Modules\Targeting\Repositories\Contracts\DeviceGroupRepository;

class ActualizeDeviceGroupsAction
{
    /**
     * @param DeviceGroupRepository
     */
    protected $repository;

    /**
     * ActualizeDeviceGroupsAction constructor.
     *
     * @param DeviceGroupRepository $repository
     */
    public function __construct(DeviceGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(): void
    {
        $this->repository->removeEmptyGroups();
        $this->repository->actualizeVisibleFlag();
    }
}
