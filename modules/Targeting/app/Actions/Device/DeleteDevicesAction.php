<?php

namespace Modules\Targeting\Actions\Device;

use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\DeleteValuesAction;
use Modules\Targeting\Repositories\Contracts\DeviceGroupRepository;
use Modules\Targeting\Repositories\DeviceRepository;
use Psr\Log\LoggerInterface;

class DeleteDevicesAction extends DeleteValuesAction
{
    /**
     * @param DeviceGroupRepository
     */
    protected $deviceGroupRepository;

    /**
     * @var ActualizeDeviceGroupsAction
     */
    private $actualizeAction;

    /**
     * @param DeviceRepository      $repository
     * @param DeviceGroupRepository $deviceGroupRepository
     * @param LoggerInterface       $log
     */
    public function __construct(
        DeviceRepository $repository,
        DeviceGroupRepository $deviceGroupRepository,
        ActualizeDeviceGroupsAction $actualizeAction,
        LoggerInterface $log
    ) {
        $this->deviceGroupRepository = $deviceGroupRepository;
        $this->actualizeAction       = $actualizeAction;
        parent::__construct($repository, $log);
    }

    /**
     * @param TargetingValuesCollection $values
     *
     * @return bool
     */
    public function handle(TargetingValuesCollection $values): void
    {
        parent::handle($values);
        $this->actualizeAction->handle();
    }
}
