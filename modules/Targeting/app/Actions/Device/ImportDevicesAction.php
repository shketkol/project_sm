<?php

namespace Modules\Targeting\Actions\Device;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Device\Traits\ActionsHelper;
use Modules\Targeting\Actions\ImportValuesAction;
use Modules\Targeting\Repositories\Contracts\DeviceGroupRepository;
use Modules\Targeting\Repositories\Contracts\DeviceRepository;
use Psr\Log\LoggerInterface;

class ImportDevicesAction extends ImportValuesAction
{
    use ActionsHelper;

    /**
     * @param DeviceGroupRepository
     */
    protected $deviceGroupRepository;

    /**
     * ImportDevicesAction constructor.
     *
     * @param DeviceRepository      $repository
     * @param DeviceGroupRepository $deviceGroupRepository
     * @param LoggerInterface       $log
     */
    public function __construct(
        DeviceRepository $repository,
        DeviceGroupRepository $deviceGroupRepository,
        LoggerInterface $log
    ) {
        $this->deviceGroupRepository = $deviceGroupRepository;
        parent::__construct($repository, $log);
    }

    /**
     * @param TargetingValuesCollection $values
     * @return Collection
     */
    public function handle(TargetingValuesCollection $values): bool
    {
        $values = $this->filterUnsupportedDeviceGroups($values);
        $data   = $this->filterExisted($values);

        if ($data->isEmpty()) {
            return true;
        }

        $devices = $this->fillDeviceGroup($values->toArray(), $data->toArray());
        $devices = $this->prepareStructure($devices);
        return $this->repository->insert($devices);
    }

    /**
     * @param TargetingValuesCollection $devices
     *
     * @return TargetingValuesCollection
     */
    private function filterUnsupportedDeviceGroups(TargetingValuesCollection $devices)
    {
        $filteredValues = array_filter($devices->items(), function ($device) {
            return in_array(
                $this->getDeviceGroupName($device->toArray()),
                config('devices.groups')
            );
        });

        return new TargetingValuesCollection($filteredValues);
    }

    /**
     * @param array $allDevices
     * @param array $newDevices
     *
     * @return array
     */
    protected function fillDeviceGroup(array $allDevices, array $newDevices): array
    {
        foreach ($newDevices as $key => &$device) {
            $deviceGroup = $this->deviceGroupRepository->firstOrCreate([
                'name' => $this->getDeviceGroupName($allDevices[$key])
            ]);
            $device['group_id'] = $deviceGroup->getKey();
        }
        return $newDevices;
    }

    /**
     * @param array $device
     *
     * @return string
     */
    protected function getDeviceGroupName(array $device): string
    {
        $name = Arr::get($device, 'hierarchy.tiers.0');
        return Str::snake($name);
    }
}
