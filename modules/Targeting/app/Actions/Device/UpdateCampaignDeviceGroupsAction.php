<?php

namespace Modules\Targeting\Actions\Device;

use Modules\Campaign\Actions\UpdateCampaignBudgetAction;
use Modules\Campaign\Actions\UpdateCampaignStepAction;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Targeting\Actions\Traits\PrepareTargetingsToSync;
use Modules\Targeting\Repositories\Contracts\DeviceGroupRepository;
use Psr\Log\LoggerInterface;

class UpdateCampaignDeviceGroupsAction
{
    use PrepareTargetingsToSync;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UpdateCampaignStepAction
     */
    private $stepAction;

    /**
     * @var UpdateCampaignBudgetAction
     */
    private $budgetAction;

    /**
     * @var DeviceGroupRepository
     */
    private $repository;

    /**
     * @param UpdateCampaignStepAction $stepAction
     * @param LoggerInterface          $log
     * @param DeviceGroupRepository    $repository
     */
    public function __construct(
        UpdateCampaignStepAction $stepAction,
        UpdateCampaignBudgetAction $budgetAction,
        LoggerInterface $log,
        DeviceGroupRepository $repository
    ) {
        $this->log = $log;
        $this->stepAction = $stepAction;
        $this->repository = $repository;
        $this->budgetAction = $budgetAction;
    }

    /**
     * @param Campaign $campaign
     * @param array $data
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function handle(Campaign $campaign, array $data): Campaign
    {
        $data = $this->removeNonVisibleTargetingsFromNamedArray($campaign, $data, 'device_group_id');
        $this->updateDeviceGroups($campaign, $data);
        $this->budgetAction->handle($campaign);

        return $this->updateCampaignStep($campaign);
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return array
     */
    private function updateDeviceGroups(Campaign $campaign, array $data): array
    {
        $this->log->info('Started sync targeting devices in campaign.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        $campaign->deviceGroups()->detach();
        $result = $campaign->deviceGroups()->sync($data);

        $this->log->info('Completed sync targeting devices in campaign.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
            'result'      => $result,
        ]);

        return $result;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function updateCampaignStep(Campaign $campaign): Campaign
    {
        return $this->stepAction->handle($campaign, CampaignStep::ID_DEVICES);
    }
}
