<?php

namespace Modules\Targeting\Actions;

use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Repositories\Contracts\TargetingRepository;

class IndexValuesAction
{
    /**
     * @var TargetingRepository
     */
    protected $repository;

    /**
     * @param TargetingRepository $repository
     */
    public function __construct(TargetingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Collection
     */
    public function handle(Campaign $campaign): Collection
    {
        return $this->repository->withSavedNonVisible($campaign)->all();
    }
}
