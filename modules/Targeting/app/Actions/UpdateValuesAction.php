<?php

namespace Modules\Targeting\Actions;

use App\Repositories\Contracts\Repository;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Contracts\UpdateAction;
use Modules\Targeting\Models\TargetingValue;
use Psr\Log\LoggerInterface;

class UpdateValuesAction implements UpdateAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var Repository|\App\Repositories\Repository
     */
    protected $repository;

    /**
     * @param Repository      $repository
     * @param LoggerInterface $log
     */
    public function __construct(Repository $repository, LoggerInterface $log)
    {
        $this->repository = $repository;
        $this->log = $log;
    }

    /**
     * Update records from haapi data
     *
     * @param TargetingValuesCollection $collection
     *
     * @return void
     */
    public function handle(TargetingValuesCollection $collection): void
    {
        $data = $this->filterChanged($collection->toArray());

        if ($data->isEmpty()) {
            return;
        }

        foreach ($data as $value) {
            $this->repository->updateByExternalId($value, Arr::get($value, 'external_id'));
        }
    }

    /**
     * Filter records that differs between HAAPI and DB
     *
     * @param array $targetingValuesArray
     *
     * @return Collection
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function filterChanged(array $targetingValuesArray): Collection
    {
        return $this->getChanged($targetingValuesArray)
            ->map(function ($item, $index) use ($targetingValuesArray) {
                return array_merge(
                    Arr::get($targetingValuesArray, $index),
                    $this->getAdditionalFields()
                );
            });
    }

    /**
     * @return array
     */
    protected function getAdditionalFields(): array
    {
        $now = Carbon::now()->toDateTimeString();

        return [
            'updated_at' => $now,
        ];
    }

    /**
     * @param array $targetingValuesArray
     *
     * @return Collection
     */
    protected function getChanged(array $targetingValuesArray): Collection
    {
        $externalIds = array_column($targetingValuesArray, 'external_id');
        $changedRecords = $this->findChangedRecords($targetingValuesArray, $externalIds);

        return collect($changedRecords);
    }

    /**
     * Get ids that exist in DB and check if any value differs between HAAPI and DB
     *
     * @param array $targetingValuesArray
     * @param array $externalIds
     *
     * @return array
     */
    protected function findChangedRecords(array $targetingValuesArray, array $externalIds): array
    {
        $column = 'external_id';
        $existedModels = $this->findExistedModels($externalIds, $column);

        if ($existedModels->count() === 0) {
            return array_column($targetingValuesArray, $column);
        }

        return $this->getDiff($targetingValuesArray, $existedModels, $column);
    }

    /**
     * @param array      $targetingValuesArray
     * @param Collection $existedModels
     * @param string     $column
     *
     * @return array
     */
    protected function getDiff(array $targetingValuesArray, Collection $existedModels, string $column)
    {
        $diff = [];
        foreach ($targetingValuesArray as $index => $targetingValueArray) {
            $externalId = Arr::get($targetingValueArray, $column);
            /** @var TargetingValue $existedModel */
            $existedModel = Arr::get($existedModels, $externalId);

            if (is_null($existedModel)) {
                $diff[$index] = $externalId;
                continue;
            }

            foreach ($targetingValueArray as $key => $value) {
                // if at least one value differs between HAAPI and DB -> update
                if ($existedModel->{$key} !== $value) {
                    $diff[$index] = $externalId;
                    continue 2;
                }
            }
        }

        return $diff;
    }

    /**
     * Find existed external ids in DB.
     *
     * @param array  $values
     * @param string $column
     *
     * @return Collection|TargetingValue[]
     */
    protected function findExistedModels(array $values, string $column): Collection
    {
        return $this->repository->findWhereIn($column, $values)->keyBy($column);
    }
}
