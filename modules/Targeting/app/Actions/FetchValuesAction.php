<?php

namespace Modules\Targeting\Actions;

use Modules\Haapi\Actions\Campaign\TargetingValuesList;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingResponseData;
use Modules\Targeting\Actions\Contracts\FetchAction;
use Modules\Targeting\Exceptions\TypeNotFoundException;
use Modules\Targeting\Models\Type;
use Modules\Targeting\Repositories\Contracts\TypeRepository;
use Psr\Log\LoggerInterface;

class FetchValuesAction implements FetchAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var string
     */
    private $haapiTargetName;

    /**
     * @var TargetingValuesList
     */
    private $valuesListAction;

    /**
     * @var TypeRepository
     */
    private $typeRepository;

    /**
     * @param TargetingValuesList $valuesListAction
     * @param LoggerInterface     $log
     * @param TypeRepository      $typeRepository
     * @param string              $haapiTargetName
     */
    public function __construct(
        TargetingValuesList $valuesListAction,
        LoggerInterface $log,
        TypeRepository $typeRepository,
        string $haapiTargetName
    ) {
        $this->valuesListAction = $valuesListAction;
        $this->log = $log;
        $this->haapiTargetName = $haapiTargetName;
        $this->typeRepository = $typeRepository;
    }

    /**
     * Action to fetch values.
     *
     * @param int    $limit
     * @param int    $start
     * @param string $externalId
     *
     * @return TargetingResponseData
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function handle(int $limit, int $start = 0, ?string $externalId = null): TargetingResponseData
    {
        if (is_null($externalId)) {
            $externalId = $this->findExternalId($this->haapiTargetName);
        }

        return $this->fetch($externalId, $start, $limit);
    }

    /**
     * @param string $name
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    private function findExternalId(string $name): string
    {
        $this->log->info('Searching targeting type external id.', [
            'target_name' => $name,
        ]);

        /** @var Type $type */
        $type = $this->typeRepository->findWhere(['name' => $name])->first();

        if (is_null($type)) {
            throw TypeNotFoundException::create($name);
        }

        $this->log->info('Finished searching targeting type external id.', [
            'target_name' => $name,
            'external_id' => $type->external_id,
        ]);

        return $type->external_id;
    }

    /**
     * Action to fetch values list from HAAPI.
     *
     * @param string $externalId HAAPI List target type id
     * @param int    $start
     * @param int    $limit
     *
     * @return TargetingResponseData
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function fetch(string $externalId, int $start, int $limit): TargetingResponseData
    {
        $this->log->info('Started fetching targeting values from HAAPI.', [
            'limit'       => $limit,
            'external_id' => $externalId,
        ]);

        $valuesPayload = $this->valuesListAction->handle($externalId, $limit, $start)->getPayload();
        $data = TargetingResponseData::create(array_merge($valuesPayload, ['externalId' => $externalId]));

        $this->log->info('Finished fetching targeting values from HAAPI.', [
            'limit'                => $data->limit,
            'next_available_after' => $data->next,
            'external_id'          => $data->externalId,
            'total'                => $data->targetValues->count(),
        ]);

        return $data;
    }
}
