<?php

namespace Modules\Targeting\Actions\AgeGroup;

use Modules\Campaign\Actions\UpdateCampaignStepAction;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Psr\Log\LoggerInterface;

class UpdateCampaignGendersAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UpdateCampaignStepAction
     */
    private $stepAction;

    /**
     * @param UpdateCampaignStepAction $stepAction
     * @param LoggerInterface          $log
     */
    public function __construct(UpdateCampaignStepAction $stepAction, LoggerInterface $log)
    {
        $this->log = $log;
        $this->stepAction = $stepAction;
    }

    /**
     * @param Campaign $campaign
     * @param int|null    $genderId
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     *
     * Early return because it possible to leave the step before genders options are loaded
     */
    public function handle(Campaign $campaign, ?int $genderId): Campaign
    {
        if (!$genderId) {
            return $campaign;
        }

        $this->updateGenders($campaign, $genderId);
        return $this->updateCampaignStep($campaign);
    }

    /**
     * @param Campaign $campaign
     * @param int    $genderId
     *
     * @return array
     */
    private function updateGenders(Campaign $campaign, int $genderId): array
    {
        $this->log->info('Started sync targeting genders in campaign.', [
            'campaign_id' => $campaign->id,
            'id'          => $genderId,
        ]);

        $result = $campaign->targetingGenders()->sync([$genderId]);

        if (count($result['detached'])) {
            $campaign->targetingAgeGroups()->detach();
        }

        $this->log->info('Completed sync targeting genders in campaign.', [
            'campaign_id' => $campaign->id,
            'id'          => $genderId,
            'result'      => $result,
        ]);

        return $result;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function updateCampaignStep(Campaign $campaign): Campaign
    {
        return $this->stepAction->handle($campaign, CampaignStep::ID_AGES);
    }
}
