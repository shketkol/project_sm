<?php

namespace Modules\Targeting\Actions\AgeGroup;

use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Repositories\AgeGroupRepository;
use Modules\Targeting\Repositories\GenderRepository;
use Psr\Log\LoggerInterface;

class IndexAgeGroupsAction
{
    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var AgeGroupRepository
     */
    protected $ageGroupRepository;

    /**
     * @var GenderRepository
     */
    protected $genderRepository;

    /**
     * @param LoggerInterface    $log
     * @param AgeGroupRepository $ageGroupRepository
     * @param GenderRepository   $genderRepository
     */
    public function __construct(
        LoggerInterface $log,
        AgeGroupRepository $ageGroupRepository,
        GenderRepository $genderRepository
    ) {
        $this->log = $log;
        $this->ageGroupRepository = $ageGroupRepository;
        $this->genderRepository = $genderRepository;
    }

    /**
     * @param Campaign $campaign
     * @param int|null $genderId
     *
     * @return Collection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Campaign $campaign, ?int $genderId = null): Collection
    {
        $this->log->info('Getting targeting age groups for gender', [
            'campaign_id' => $campaign->id,
            'genderId'    => $genderId,
        ]);

        if (is_null($genderId)) {
            return $this->ageGroupRepository->withSavedNonVisible($campaign)->all();
        }

        $gender = $this->genderRepository->findWhere(['id' => $genderId])->first();

        if (optional($gender)->name === Gender::ALL) {
            return $this->ageGroupRepository->withSavedNonVisible($campaign)->all();
        }

        return $this->ageGroupRepository->withSavedNonVisible($campaign)->findByField('gender_id', $genderId);
    }
}
