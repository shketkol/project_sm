<?php

namespace Modules\Targeting\Actions\AgeGroup;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Targeting\Actions\UpdateValuesAction;
use Modules\Targeting\Commands\Traits\AgeGroupsHelper;
use Modules\Targeting\Repositories\Contracts\AgeGroupRepository;
use Modules\Targeting\Repositories\Contracts\GenderRepository;
use Psr\Log\LoggerInterface;

class UpdateAgeGroupAction extends UpdateValuesAction
{
    use AgeGroupsHelper;

    /**
     * @param GenderRepository
     */
    protected $genderRepository;

    /**
     * @param AgeGroupRepository $repository
     * @param GenderRepository $genderRepository
     * @param LoggerInterface $log
     */
    public function __construct(
        AgeGroupRepository $repository,
        GenderRepository $genderRepository,
        LoggerInterface $log
    ) {
        $this->genderRepository = $genderRepository;
        parent::__construct($repository, $log);
    }

    /**
     * Filter records that differs between HAAPI and DB
     *
     * @param array $targetingValuesArray
     *
     * @return Collection
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function filterChanged(array $targetingValuesArray): Collection
    {
        return $this->getChanged($targetingValuesArray)
            ->map(function ($item, $index) use ($targetingValuesArray) {
                return array_merge(
                    $this->getAttributes(Arr::get($targetingValuesArray, $index)),
                    $this->getAdditionalFields()
                );
            });
    }
}
