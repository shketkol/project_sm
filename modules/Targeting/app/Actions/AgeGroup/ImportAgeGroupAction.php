<?php

namespace Modules\Targeting\Actions\AgeGroup;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\ImportValuesAction;
use Modules\Targeting\Commands\Traits\AgeGroupsHelper;
use Modules\Targeting\Repositories\Contracts\AgeGroupRepository;
use Modules\Targeting\Repositories\Contracts\GenderRepository;
use Psr\Log\LoggerInterface;

class ImportAgeGroupAction extends ImportValuesAction
{
    use AgeGroupsHelper;

    /**
     * @param GenderRepository
     */
    protected $genderRepository;

    /**
     * @param AgeGroupRepository $repository
     * @param GenderRepository $genderRepository
     * @param LoggerInterface $log
     */
    public function __construct(
        AgeGroupRepository $repository,
        GenderRepository $genderRepository,
        LoggerInterface $log
    ) {
        $this->genderRepository = $genderRepository;
        parent::__construct($repository, $log);
    }

    /**
     * @param TargetingValuesCollection $values
     * @return Collection
     */
    public function handle(TargetingValuesCollection $values): bool
    {
        $data = $this->filterExisted($values);

        if ($data->isEmpty()) {
            return true;
        }

        $ageGroupArray = $this->fillGender($values->toArray(), $data->toArray());
        return $this->repository->insert($ageGroupArray);
    }

    /**
     * Fill ageGroup with appropriate gender
     *
     * @param array $fullAgeGroups
     * @param array $data
     * @return array
     */
    protected function fillGender(array $fullAgeGroups, array $data): array
    {
        foreach ($data as $key => &$ageGroup) {
            $gender = $this->genderRepository->firstOrCreate([
                'name' => $this->retrieveGender($fullAgeGroups[$key]['name'])
            ]);
            $ageGroup['gender_id'] = $gender->getKey();
        }

        return $data;
    }

    /**
     * @param string $string
     * @return string
     */
    protected function retrieveGender(string $string): string
    {
        $genderArray = explode(' ', $string);
        $name = strtolower(Arr::first($genderArray));

        if ($name === 'all') {
            $name = strtolower($genderArray[1] ?? $name);
        }

        return config("genders.{$name}");
    }

    /**
     * Filter records that already exists in DB.
     *
     * @param TargetingValuesCollection $collection
     *
     * @return Collection
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function filterExisted(TargetingValuesCollection $collection): Collection
    {
        return $this->getDiff($collection)
            ->map(function ($item, $index) use ($collection) {
                return array_merge(
                    $this->getAttributes($collection->offsetGet($index)->toArray()),
                    $this->getAdditionalFields()
                );
            });
    }
}
