<?php

namespace Modules\Targeting\Actions\AgeGroup;

use Modules\Campaign\Actions\UpdateCampaignBudgetAction;
use Modules\Campaign\Actions\UpdateCampaignStepAction;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Targeting\Actions\Traits\PrepareTargetingsToSync;
use Modules\Targeting\Repositories\AgeGroupRepository;
use Psr\Log\LoggerInterface;

class UpdateCampaignAgeGroupsAction
{
    use PrepareTargetingsToSync;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var UpdateCampaignStepAction
     */
    protected $stepAction;

    /**
     * @var UpdateCampaignBudgetAction
     */
    private $budgetAction;

    /**
     * @var AgeGroupRepository
     */
    protected $repository;

    /**
     * @param UpdateCampaignStepAction $stepAction
     * @param LoggerInterface          $log
     * @param AgeGroupRepository       $repository
     */
    public function __construct(
        UpdateCampaignStepAction $stepAction,
        UpdateCampaignBudgetAction $budgetAction,
        LoggerInterface $log,
        AgeGroupRepository $repository
    ) {
        $this->log = $log;
        $this->stepAction = $stepAction;
        $this->repository = $repository;
        $this->budgetAction = $budgetAction;
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    public function handle(Campaign $campaign, array $data): Campaign
    {
        $data = $this->removeNonVisibleTargetings($campaign, $data);
        $this->updateAgeGroups($campaign, $data);
        $this->budgetAction->handle($campaign);

        return $this->updateCampaignStep($campaign);
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return array
     */
    protected function updateAgeGroups(Campaign $campaign, array $data): array
    {
        $this->log->info('Started sync targeting age groups in campaign.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        $campaign->targetingAgeGroups()->detach();
        $result = $campaign->targetingAgeGroups()->sync($data);

        $this->log->info('Completed sync targeting age groups in campaign.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
            'result'      => $result,
        ]);

        return $result;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function updateCampaignStep(Campaign $campaign): Campaign
    {
        return $this->stepAction->handle($campaign, CampaignStep::ID_AGES);
    }
}
