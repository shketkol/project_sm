<?php

namespace Modules\Targeting\Actions\AgeGroup;

use Modules\Targeting\Actions\DeleteValuesAction;
use Modules\Targeting\Repositories\Contracts\AgeGroupRepository;
use Psr\Log\LoggerInterface;

class DeleteAgeGroupAction extends DeleteValuesAction
{
    /**
     * @param AgeGroupRepository $repository
     * @param LoggerInterface $log
     */
    public function __construct(
        AgeGroupRepository $repository,
        LoggerInterface $log
    ) {
        parent::__construct($repository, $log);
    }
}
