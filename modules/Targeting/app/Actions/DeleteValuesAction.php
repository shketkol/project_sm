<?php

namespace Modules\Targeting\Actions;

use App\Repositories\Contracts\Repository;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Contracts\DeleteAction;
use Psr\Log\LoggerInterface;

class DeleteValuesAction implements DeleteAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var Repository|\App\Repositories\Repository
     */
    protected $repository;

    /**
     * @param Repository      $repository
     * @param LoggerInterface $log
     */
    public function __construct(Repository $repository, LoggerInterface $log)
    {
        $this->repository = $repository;
        $this->log = $log;
    }

    /**
     * @param TargetingValuesCollection $values
     *
     * @return bool
     */
    public function handle(TargetingValuesCollection $values): void
    {
        $data = $this->getDiff($values);

        if ($data->isEmpty()) {
            return;
        }

        $this->repository->deleteByExternalId($data->toArray());
    }

    /**
     * Get ids that exist in DB and should be updated.
     *
     * @param TargetingValuesCollection $collection
     *
     * @return Collection
     */
    public function getDiff(TargetingValuesCollection $collection): Collection
    {
        $ids = array_column($collection->toArray(), 'external_id');
        $existedIds = $this->findExistedIds();
        $diff = array_diff($existedIds, $ids);

        return collect($diff);
    }

    /**
     * Find existed external ids in DB.
     *
     * @param array $ids
     *
     * @return array
     */
    private function findExistedIds(): array
    {
        return Arr::pluck($this->repository->select('external_id')->get()->toArray(), 'external_id');
    }
}
