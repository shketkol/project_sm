<?php

namespace Modules\Targeting\Actions\Genre;

use Modules\Targeting\Actions\SearchValuesAction;
use Modules\Targeting\Repositories\Contracts\GenreRepository;
use Psr\Log\LoggerInterface;

class SearchGenresAction extends SearchValuesAction
{
    /**
     * @param GenreRepository $repository
     * @param LoggerInterface $log
     */
    public function __construct(GenreRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }
}
