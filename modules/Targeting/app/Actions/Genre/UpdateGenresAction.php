<?php

namespace Modules\Targeting\Actions\Genre;

use Modules\Targeting\Actions\UpdateValuesAction;
use Modules\Targeting\Repositories\Contracts\GenreRepository;
use Psr\Log\LoggerInterface;

class UpdateGenresAction extends UpdateValuesAction
{
    /**
     * @param GenreRepository $repository
     * @param LoggerInterface $log
     */
    public function __construct(GenreRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }
}
