<?php

namespace Modules\Targeting\Actions\Genre;

use Modules\Campaign\Actions\UpdateCampaignBudgetAction;
use Modules\Campaign\Actions\UpdateCampaignStepAction;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Targeting\Actions\Traits\PrepareTargetingsToSync;
use Modules\Targeting\Repositories\GenreRepository;
use Psr\Log\LoggerInterface;

class UpdateCampaignGenresAction
{
    use PrepareTargetingsToSync;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UpdateCampaignStepAction
     */
    private $stepAction;

    /**
     * @var UpdateCampaignBudgetAction
     */
    private $budgetAction;

    /**
     * @var GenreRepository
     */
    private $repository;

    /**
     * @param UpdateCampaignStepAction $stepAction
     * @param LoggerInterface          $log
     * @param GenreRepository         $repository
     */
    public function __construct(
        UpdateCampaignStepAction $stepAction,
        UpdateCampaignBudgetAction $budgetAction,
        LoggerInterface $log,
        GenreRepository $repository
    ) {
        $this->log = $log;
        $this->stepAction = $stepAction;
        $this->budgetAction = $budgetAction;
        $this->repository = $repository;
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    public function handle(Campaign $campaign, array $data): Campaign
    {
        $data = $this->removeNonVisibleTargetingsFromNamedArray($campaign, $data, 'genre_id');
        $this->updateGenres($campaign, $data);
        $this->budgetAction->handle($campaign);

        return $this->updateCampaignStep($campaign);
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return array
     */
    private function updateGenres(Campaign $campaign, array $data): array
    {
        $this->log->info('Started sync targeting genres in campaign.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        $campaign->genres()->detach();
        $result = $campaign->genres()->sync($data);

        $this->log->info('Completed sync targeting genres in campaign.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
            'result'      => $result,
        ]);

        return $result;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function updateCampaignStep(Campaign $campaign): Campaign
    {
        return $this->stepAction->handle($campaign, CampaignStep::ID_GENRES);
    }
}
