<?php

namespace Modules\Targeting\Actions\Genre;

use Modules\Targeting\Actions\DeleteValuesAction;
use Modules\Targeting\Repositories\GenreRepository;
use Psr\Log\LoggerInterface;

class DeleteGenresAction extends DeleteValuesAction
{
    /**
     * @param GenreRepository $repository
     * @param LoggerInterface $log
     */
    public function __construct(
        GenreRepository $repository,
        LoggerInterface $log
    ) {
        parent::__construct($repository, $log);
    }
}
