<?php

namespace Modules\Targeting\Actions;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Exceptions\ClassNotMappedException;
use Modules\Targeting\Models\DeviceGroup;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\TargetingValue;
use Modules\Targeting\Models\Zipcode;
use Modules\Targeting\Repositories\Contracts\TargetingRepository;
use Psr\Log\LoggerInterface;

class SearchValuesAction
{
    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var TargetingRepository
     */
    protected $repository;

    /**
     * @param TargetingRepository $repository
     * @param LoggerInterface     $log
     */
    public function __construct(TargetingRepository $repository, LoggerInterface $log)
    {
        $this->repository = $repository;
        $this->log = $log;
    }

    /**
     * Since Scout search (ElasticSearch) index store only model without relations
     * We have to filter search records afterwards
     * To give different search results for different Campaigns
     *
     * How it works:
     * 1. Search in ElasticSearch all results by query
     * 2. ElasticSearch would return up to 200 results
     * 3. Loop through results and filter:
     *   a. If targeting is visible -> add it to results
     *   b. If targeting non visible but already stored in Campaign -> add to results
     *   c. If targeting non visible and does not stored in Campaign -> remove from results
     * 4. This method would save sort used in ElasticSearch
     *
     * @param Campaign $campaign
     * @param string   $query
     *
     * @return Collection
     * @throws ClassNotMappedException
     */
    public function handle(Campaign $campaign, $query): Collection
    {
        $result = $this->searchScout($query);
        return $this->filterAvailableForCampaign($campaign, $result);
    }

    /**
     * @param Campaign   $campaign
     * @param Collection $collection
     *
     * @return Collection
     * @throws ClassNotMappedException
     */
    protected function filterAvailableForCampaign(Campaign $campaign, Collection $collection): Collection
    {
        $campaignTargetingValues = $this->getCampaignTargetingValues($campaign);

        return $collection->filter(function (TargetingValue $model) use ($campaignTargetingValues) {
            if ($model->visible) {
                return true;
            }

            if ($campaignTargetingValues->contains($model->id)) {
                return true;
            }

            return false;
        });
    }

    /**
     * @param Campaign $campaign
     *
     * @return Collection
     * @throws ClassNotMappedException
     */
    private function getCampaignTargetingValues(Campaign $campaign): Collection
    {
        // since search used for locations and genres we need to determine relation to fetch data from it
        $map = [
            /** @see Campaign::$locations */
            Location::class      => 'locations',

            /** @see Campaign::$zipcodes */
            Zipcode::class       => 'zipcodes',

            /** @see Campaign::$genres */
            Genre::class         => 'genres',

            /** @see Campaign::$devices */
            DeviceGroup::class   => 'deviceGroups',
        ];

        $relation = Arr::get($map, $this->repository->model());

        if (is_null($relation)) {
            throw ClassNotMappedException::create($this->repository->model());
        }

        return $campaign->{$relation}->keyBy('id');
    }

    /**
     * @param string $query
     *
     * @return Collection
     */
    private function searchScout(string $query): Collection
    {
        $this->log->info('Searching targeting value.', [
            'query'      => $query,
            'repository' => get_class($this->repository),
        ]);

        $result = $this->repository->search($query)
            ->take(config('search.limit'))
            ->get();

        $this->log->info('Found targeting values.', [
            'query'      => $query,
            'count'      => $result->count(),
            'repository' => get_class($this->repository),
        ]);

        return $result;
    }
}
