<?php

namespace Modules\Targeting\Actions;

use App\Repositories\Contracts\Repository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Contracts\ImportAction;
use Modules\Targeting\Models\TargetingValue;
use Modules\Targeting\Models\Type;
use Psr\Log\LoggerInterface;

class ImportValuesAction implements ImportAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var Repository
     */
    protected $repository;

    /**
     * @param Repository      $repository
     * @param LoggerInterface $log
     */
    public function __construct(Repository $repository, LoggerInterface $log)
    {
        $this->repository = $repository;
        $this->log = $log;
    }

    /**
     * Supported only insert
     * Update records would be done either by haapi callback or re-import
     *
     * @param TargetingValuesCollection $collection
     *
     * @return bool
     */
    public function handle(TargetingValuesCollection $collection): bool
    {
        $data = $this->filterExisted($collection);

        if ($data->isEmpty()) {
            return true;
        }

        return $this->repository->insert($data->toArray());
    }

    /**
     * Filter records that already exists in DB.
     *
     * @param TargetingValuesCollection $collection
     *
     * @return Collection
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function filterExisted(TargetingValuesCollection $collection): Collection
    {
        return $this->getDiff($collection)
            ->map(function ($item, $index) use ($collection) {
                return array_merge(
                    $collection->offsetGet($index)->toArray(),
                    $this->getAdditionalFields()
                );
            });
    }

    /**
     * Since we using bulk insert
     *
     * @return array
     */
    protected function getAdditionalFields(): array
    {
        $now = Carbon::now()->toDateTimeString();

        return [
            'created_at' => $now,
            'updated_at' => $now,
        ];
    }

    /**
     * Get ids that not exist in DB and should be imported.
     *
     * @param TargetingValuesCollection $collection
     *
     * @return Collection
     */
    protected function getDiff(TargetingValuesCollection $collection): Collection
    {
        $ids = array_column($collection->toArray(), 'external_id');
        $existedIds = $this->findExistedIds($ids);
        $diff = array_diff($ids, $existedIds);

        return collect($diff);
    }

    /**
     * Find existed external ids in DB.
     *
     * @param array $ids
     *
     * @return array
     */
    private function findExistedIds(array $ids): array
    {
        $query = $this->repository;

        if (method_exists($this->repository, 'withNonVisible')) {
            $query = $query->withNonVisible();
        }

        return $query->findWhereIn('external_id', $ids)
            ->map(function (Model $model): string {
                /** @var TargetingValue|Type $model */
                return $model->external_id;
            })
            ->toArray();
    }
}
