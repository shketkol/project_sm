<?php

namespace Modules\Targeting\Actions\Contracts;

use Modules\Haapi\DataTransferObjects\Targeting\TargetingResponseData;

interface FetchAction
{
    /**
     * Action to fetch targeting values from HAAPI.
     *
     * @param int    $limit
     * @param int    $start
     * @param string $externalId
     *
     * @return TargetingResponseData
     */
    public function handle(int $limit, int $start = 0, ?string $externalId = null): TargetingResponseData;
}
