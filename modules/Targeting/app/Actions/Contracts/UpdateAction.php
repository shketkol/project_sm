<?php

namespace Modules\Targeting\Actions\Contracts;

use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;

interface UpdateAction
{
    /**
     * Action to import targeting values (store or update) in DB.
     *
     * @param TargetingValuesCollection $collection
     *
     * @return void
     */
    public function handle(TargetingValuesCollection $collection): void;
}
