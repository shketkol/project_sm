<?php

namespace Modules\Targeting\Actions\Contracts;

use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;

interface ImportAction
{
    /**
     * Action to import targeting values (store or update) in DB.
     *
     * @param TargetingValuesCollection $collection
     *
     * @return bool
     */
    public function handle(TargetingValuesCollection $collection): bool;
}
