<?php

namespace Modules\Targeting\Actions\Types;

use Modules\Targeting\Actions\ImportValuesAction;
use Modules\Targeting\Repositories\Contracts\TypeRepository;
use Psr\Log\LoggerInterface;

class ImportTypesAction extends ImportValuesAction
{
    /**
     * @param TypeRepository  $repository
     * @param LoggerInterface $log
     */
    public function __construct(TypeRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }
}
