<?php

namespace Modules\Targeting\Actions\Types;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\UpdateValuesAction;
use Modules\Targeting\Repositories\Contracts\TypeRepository;
use Psr\Log\LoggerInterface;

class UpdateTypesAction extends UpdateValuesAction
{
    /**
     * @param TypeRepository  $repository
     * @param LoggerInterface $log
     */
    public function __construct(TypeRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }

    /**
     * Update records from haapi data
     *
     * @param TargetingValuesCollection $collection
     *
     * @return void
     */
    public function handle(TargetingValuesCollection $collection): void
    {
        $data = $this->filterChanged($collection->toArray());

        if ($data->isEmpty()) {
            return;
        }

        foreach ($data as $value) {
            $this->repository->updateByName($value, Arr::get($value, 'name'));
        }
    }

    /**
     * Get ids that exist in DB and check if any value differs between HAAPI and DB
     *
     * @param array $targetingTypesArray
     * @param array $externalIds
     *
     * @return array
     */
    protected function findChangedRecords(array $targetingTypesArray, array $externalIds): array
    {
        $column = 'name';
        $names = array_column($targetingTypesArray, $column);
        $existedModels = $this->findExistedModels($names, $column);

        if ($existedModels->count() === 0) {
            return $names;
        }

        return $this->getDiff($targetingTypesArray, $existedModels, $column);
    }
}
