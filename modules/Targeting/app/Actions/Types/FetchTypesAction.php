<?php

namespace Modules\Targeting\Actions\Types;

use Illuminate\Support\Arr;
use Modules\Haapi\Actions\Campaign\TargetingTypeList;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Psr\Log\LoggerInterface;

class FetchTypesAction
{
    /**
     * @var TargetingTypeList
     */
    private $typeListAction;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * FetchTypeListAction constructor.
     *
     * @param TargetingTypeList $typeListAction
     * @param LoggerInterface   $log
     */
    public function __construct(TargetingTypeList $typeListAction, LoggerInterface $log)
    {
        $this->typeListAction = $typeListAction;
        $this->log = $log;
    }

    /**
     * Action to fetch types list from HAAPI.
     *
     * @return TargetingValuesCollection
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(): TargetingValuesCollection
    {
        $limit = config('haapi.targeting.types_list.limit');
        $this->log->info('Started import targeting types list from HAAPI.', ['limit' => $limit]);

        $payload = $this->typeListAction->handle($limit)->getPayload();
        $data = Arr::get($payload, 'targetTypes', []);
        $types = TargetingValuesCollection::create($data);

        $this->log->info('Finished import targeting types list from HAAPI.', [
            'limit' => $limit,
            'total' => $types->count(),
        ]);

        return $types;
    }
}
