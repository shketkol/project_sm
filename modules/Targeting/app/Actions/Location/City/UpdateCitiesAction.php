<?php

namespace Modules\Targeting\Actions\Location\City;

use Modules\Targeting\Actions\UpdateValuesAction;
use Modules\Targeting\Models\Type;
use Modules\Targeting\Repositories\Contracts\LocationRepository;
use Psr\Log\LoggerInterface;

class UpdateCitiesAction extends UpdateValuesAction
{
    /**
     * @param LocationRepository $repository
     * @param LoggerInterface    $log
     */
    public function __construct(LocationRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }

    /**
     * @return array
     */
    protected function getAdditionalFields(): array
    {
        return array_merge(
            parent::getAdditionalFields(),
            ['type' => Type::HAAPI_CITY_NAME]
        );
    }
}
