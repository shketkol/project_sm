<?php

namespace Modules\Targeting\Actions\Location\Zipcode;

use Illuminate\Database\DatabaseManager;
use Modules\Campaign\Actions\UpdateCampaignBudgetAction;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\Duplicates\FilterDuplicatedLocationsAction;
use Modules\Targeting\Actions\Traits\PrepareTargetingsToSync;
use Modules\Targeting\Repositories\ZipcodeRepository;
use Psr\Log\LoggerInterface;

class UpdateCampaignZipcodesAction
{
    use PrepareTargetingsToSync;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var FilterDuplicatedLocationsAction
     */
    private $filterDuplicatesAction;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var ZipcodeRepository
     */
    private $repository;

    /**
     * @param LoggerInterface                 $log
     * @param FilterDuplicatedLocationsAction $filterDuplicatesAction
     * @param DatabaseManager                 $databaseManager
     * @param ZipcodeRepository               $repository
     */
    public function __construct(
        LoggerInterface $log,
        FilterDuplicatedLocationsAction $filterDuplicatesAction,
        DatabaseManager $databaseManager,
        ZipcodeRepository $repository
    ) {
        $this->log = $log;
        $this->filterDuplicatesAction = $filterDuplicatesAction;
        $this->databaseManager = $databaseManager;
        $this->repository = $repository;
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return Campaign
     * @throws \Throwable
     */
    public function handle(Campaign $campaign, array $data): Campaign
    {
        $this->log->info('Campaign zipcodes update started.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $data = $this->filterDuplicatesAction->handle($data);
            $data = $this->removeNonVisibleTargetingsFromNamedArray($campaign, $data, 'zipcode_id');
            $result = $this->updateZipcodes($campaign, $data);
        } catch (\Throwable $exception) {
            $this->log->warning('Campaign zipcodes update failed.', [
                'campaign_id' => $campaign->id,
                'data'        => $data,
                'reason'      => $exception->getMessage(),
            ]);
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('Campaign zipcodes update finished.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
            'result'      => $result,
        ]);

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return array
     */
    private function updateZipcodes(Campaign $campaign, array $data): array
    {
        $campaign->zipcodes()->detach();
        return $campaign->zipcodes()->sync($data);
    }
}
