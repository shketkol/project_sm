<?php

namespace Modules\Targeting\Actions\Location\Zipcode;

use Modules\Targeting\Actions\ImportValuesAction;
use Modules\Targeting\Repositories\ZipcodeRepository;
use Psr\Log\LoggerInterface;

class ImportZipcodesAction extends ImportValuesAction
{
    /**
     * @param ZipcodeRepository $repository
     * @param LoggerInterface   $log
     */
    public function __construct(ZipcodeRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }
}
