<?php

namespace Modules\Targeting\Actions\Location\Zipcode;

use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\SearchValuesAction;
use Modules\Targeting\Repositories\ZipcodeRepository;
use Psr\Log\LoggerInterface;

class SearchZipcodesAction extends SearchValuesAction
{
    /**
     * @param ZipcodeRepository $repository
     * @param LoggerInterface   $log
     */
    public function __construct(ZipcodeRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }

    /**
     * @param Campaign $campaign
     * @param array    $zipcodes
     *
     * @return Collection
     * @throws \Modules\Targeting\Exceptions\ClassNotMappedException
     */
    public function handle(Campaign $campaign, $zipcodes): Collection
    {
        $result = $this->search($zipcodes);
        return $this->filterAvailableForCampaign($campaign, $result);
    }

    /**
     * @param array $zipcodes
     *
     * @return Collection
     */
    protected function search($zipcodes): Collection
    {
        $this->log->info('Searching targeting zipcodes.', [
            'zipcodes'   => $zipcodes,
            'repository' => get_class($this->repository),
        ]);

        $result = $this->repository->findWhereIn('name', $zipcodes);

        $this->log->info('Found targeting zipcodes.', [
            'zipcodes'        => $zipcodes,
            'count_found'     => $result->count(),
            'count_not_found' => count($zipcodes) - $result->count(),
            'repository'      => get_class($this->repository),
        ]);

        return $result;
    }
}
