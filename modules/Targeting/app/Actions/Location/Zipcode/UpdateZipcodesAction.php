<?php

namespace Modules\Targeting\Actions\Location\Zipcode;

use Modules\Targeting\Actions\UpdateValuesAction;
use Modules\Targeting\Repositories\ZipcodeRepository;
use Psr\Log\LoggerInterface;

class UpdateZipcodesAction extends UpdateValuesAction
{
    /**
     * @param ZipcodeRepository $repository
     * @param LoggerInterface    $log
     */
    public function __construct(ZipcodeRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }
}
