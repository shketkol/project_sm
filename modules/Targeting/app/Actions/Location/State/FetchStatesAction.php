<?php

namespace Modules\Targeting\Actions\Location\State;

use Modules\Haapi\Actions\Campaign\TargetingValuesList;
use Modules\Targeting\Actions\FetchValuesAction;
use Modules\Targeting\Models\Type;
use Modules\Targeting\Repositories\Contracts\TypeRepository;
use Psr\Log\LoggerInterface;

class FetchStatesAction extends FetchValuesAction
{
    /**
     * @param TargetingValuesList $valuesListAction
     * @param LoggerInterface     $log
     * @param TypeRepository      $typeRepository
     */
    public function __construct(
        TargetingValuesList $valuesListAction,
        LoggerInterface $log,
        TypeRepository $typeRepository
    ) {
        parent::__construct($valuesListAction, $log, $typeRepository, Type::HAAPI_STATE_NAME);
    }
}
