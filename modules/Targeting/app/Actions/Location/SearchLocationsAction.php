<?php

namespace Modules\Targeting\Actions\Location;

use Modules\Targeting\Actions\SearchValuesAction;
use Modules\Targeting\Repositories\Contracts\LocationRepository;
use Psr\Log\LoggerInterface;

class SearchLocationsAction extends SearchValuesAction
{
    /**
     * @param LocationRepository $repository
     * @param LoggerInterface    $log
     */
    public function __construct(LocationRepository $repository, LoggerInterface $log)
    {
        parent::__construct($repository, $log);
    }
}
