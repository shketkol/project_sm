<?php

namespace Modules\Targeting\Actions\Location;

use Illuminate\Database\DatabaseManager;
use Modules\Campaign\Actions\UpdateCampaignBudgetAction;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Actions\Duplicates\FilterDuplicatedLocationsAction;
use Modules\Targeting\Actions\Traits\PrepareTargetingsToSync;
use Modules\Targeting\Repositories\LocationRepository;
use Psr\Log\LoggerInterface;

class UpdateCampaignLocationsAction
{
    use PrepareTargetingsToSync;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var FilterDuplicatedLocationsAction
     */
    private $filterDuplicatesAction;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var LocationRepository
     */
    private $repository;

    /**
     * @param LoggerInterface                 $log
     * @param FilterDuplicatedLocationsAction $filterDuplicatesAction
     * @param DatabaseManager                 $databaseManager
     * @param LocationRepository              $repository
     */
    public function __construct(
        LoggerInterface $log,
        FilterDuplicatedLocationsAction $filterDuplicatesAction,
        DatabaseManager $databaseManager,
        LocationRepository $repository
    ) {
        $this->log = $log;
        $this->filterDuplicatesAction = $filterDuplicatesAction;
        $this->databaseManager = $databaseManager;
        $this->repository = $repository;
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return Campaign
     * @throws \Throwable
     */
    public function handle(Campaign $campaign, array $data): Campaign
    {
        $this->log->info('Campaign location update started.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $data = $this->filterDuplicatesAction->handle($data);
            $data = $this->removeNonVisibleTargetingsFromNamedArray($campaign, $data, 'location_id');
            $result = $this->updateLocations($campaign, $data);
        } catch (\Throwable $exception) {
            $this->log->warning('Campaign location update failed.', [
                'campaign_id' => $campaign->id,
                'data'        => $data,
                'reason'      => $exception->getMessage(),
            ]);
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('Campaign location update finished.', [
            'campaign_id' => $campaign->id,
            'data'        => $data,
            'result'      => $result,
        ]);

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     * @param array    $data
     *
     * @return array
     */
    private function updateLocations(Campaign $campaign, array $data): array
    {
        $campaign->locations()->detach();
        return $campaign->locations()->sync($data);
    }
}
