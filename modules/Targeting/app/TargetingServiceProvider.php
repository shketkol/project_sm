<?php

namespace Modules\Targeting;

use App\Providers\ModuleServiceProvider;
use Modules\Targeting\Commands\DeleteTargetingValuesCommand;
use Modules\Targeting\Commands\ImportAgeGroupsCommand;
use Modules\Targeting\Commands\ImportAudiencesCommand;
use Modules\Targeting\Commands\ImportCitiesCommand;
use Modules\Targeting\Commands\ImportDmaCommand;
use Modules\Targeting\Commands\ImportGenresCommand;
use Modules\Targeting\Commands\ImportDevicesCommand;
use Modules\Targeting\Commands\ImportStatesCommand;
use Modules\Targeting\Commands\ImportTargetingTypesCommand;
use Modules\Targeting\Commands\ImportZipcodesCommand;
use Modules\Targeting\Repositories\AudienceRepository;
use Modules\Targeting\Repositories\Contracts\AudienceRepository as AudienceRepositoryInterface;
use Modules\Targeting\Repositories\Contracts\DeviceRepository as DeviceRepositoryInterface;
use Modules\Targeting\Repositories\Contracts\DeviceGroupRepository as DeviceGroupRepositoryInterface;
use Modules\Targeting\Repositories\Contracts\GenreRepository as GenreRepositoryInterface;
use Modules\Targeting\Repositories\Contracts\LocationRepository as LocationRepositoryInterface;
use Modules\Targeting\Repositories\Contracts\TypeRepository as TypeRepositoryInterface;
use Modules\Targeting\Repositories\DeviceRepository;
use Modules\Targeting\Repositories\DeviceGroupRepository;
use Modules\Targeting\Repositories\GenreRepository;
use Modules\Targeting\Repositories\Contracts\AgeGroupRepository as AgeGroupRepositoryInterface;
use Modules\Targeting\Repositories\AgeGroupRepository;
use Modules\Targeting\Repositories\Contracts\GenderRepository as GenderRepositoryInterface;
use Modules\Targeting\Repositories\GenderRepository;
use Modules\Targeting\Repositories\LocationRepository;
use Modules\Targeting\Repositories\TypeRepository;

class TargetingServiceProvider extends ModuleServiceProvider
{
    /**
     * @var array
     */
    public $bindings = [
        DeviceRepositoryInterface::class      => DeviceRepository::class,
        DeviceGroupRepositoryInterface::class => DeviceGroupRepository::class,
        GenreRepositoryInterface::class       => GenreRepository::class,
        AgeGroupRepositoryInterface::class    => AgeGroupRepository::class,
        GenderRepositoryInterface::class      => GenderRepository::class,
        LocationRepositoryInterface::class    => LocationRepository::class,
        TypeRepositoryInterface::class        => TypeRepository::class,
        AudienceRepositoryInterface::class    => AudienceRepository::class,
    ];

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        ImportGenresCommand::class,
        ImportDevicesCommand::class,
        ImportAgeGroupsCommand::class,
        ImportZipcodesCommand::class,
        ImportDmaCommand::class,
        ImportCitiesCommand::class,
        ImportStatesCommand::class,
        ImportTargetingTypesCommand::class,
        ImportAudiencesCommand::class,
        DeleteTargetingValuesCommand::class,
    ];

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'targeting';
    }

    /**
     * Boot provider.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->commands($this->commands);
        $this->loadConfigs(['haapi', 'search', 'genders', 'locations', 'devices']);
        $this->loadRoutes();
        $this->loadTranslations();
    }
}
