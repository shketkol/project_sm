<?php

namespace Modules\Targeting\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * @property integer $id
 * @property string  $external_id
 * @property string  $name
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @mixin Builder
 */
class Type extends Model
{
    /**
     * List target type name in HAAPI request.
     */
    public const HAAPI_AGE_GROUP_NAME = 'ng_demo';
    public const HAAPI_GENRE_NAME     = 'genre';
    public const HAAPI_DEVICE_NAME    = 'actual_distribution_platform';
    public const HAAPI_ZIPCODE_NAME   = 'zipcode';
    public const HAAPI_DMA_NAME       = 'dma';
    public const HAAPI_CITY_NAME      = 'city';
    public const HAAPI_STATE_NAME     = 'state';
    public const HAAPI_AUDIENCE_NAME  = 'krux';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'external_id',
    ];
}
