<?php

namespace Modules\Targeting\Models;

/**
 * @property string $type
 */
class Location extends TargetingValue
{
    public const TYPE_NAME = 'location';
    public const PERMISSION_NAME = 'targeting_locations';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'external_id',
        'type',
        'visible',
    ];
}
