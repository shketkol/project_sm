<?php

namespace Modules\Targeting\Models;

class Device extends TargetingValue
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'external_id',
        'group_id',
        'visible',
    ];
}
