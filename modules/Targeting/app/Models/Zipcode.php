<?php

namespace Modules\Targeting\Models;

class Zipcode extends TargetingValue
{
    public const TYPE_NAME = 'zip';
    public const PERMISSION_NAME = 'targeting_zips';
}
