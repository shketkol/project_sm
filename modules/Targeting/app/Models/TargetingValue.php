<?php

namespace Modules\Targeting\Models;

use App\Traits\HasFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Query\Builder;
use Modules\Targeting\Models\Traits\Scopes;
use Modules\Targeting\Models\Traits\Searchable;
use Modules\Targeting\Models\Traits\TargetingValueAccessors;

/**
 * @property integer    $id
 * @property string     $external_id
 * @property string     $name
 * @property boolean    $visible
 * @property Carbon     $created_at
 * @property Carbon     $updated_at
 * @mixin Builder
 * @property-read Pivot $pivot
 */
abstract class TargetingValue extends Model
{
    use Searchable,
        Scopes,
        TargetingValueAccessors,
        HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'external_id',
        'visible',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'visible' => 'boolean',
    ];
}
