<?php

namespace Modules\Targeting\Models;

use Modules\Targeting\Models\Traits\HasManyDevices;

class DeviceGroup extends TargetingValue
{
    use HasManyDevices;

    public const NAME_LIVING_ROOM = 'living_room';
    public const NAME_MOBILE = 'mobile';
    public const NAME_COMPUTER = 'computer';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'visible',
    ];
}
