<?php

namespace Modules\Targeting\Models;

/**
 * @property string $tiers
 */
class Audience extends TargetingValue
{
    public const TYPE_NAME = 'audience';
    public const PERMISSION_NAME = 'targeting_audiences';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'tiers',
        'external_id',
        'visible',
    ];
}
