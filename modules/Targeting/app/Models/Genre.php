<?php

namespace Modules\Targeting\Models;

class Genre extends TargetingValue
{
    public const TYPE_NAME = 'genre';
    public const PERMISSION_NAME = 'targeting_genres';
}
