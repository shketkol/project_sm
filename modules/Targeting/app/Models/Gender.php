<?php

namespace Modules\Targeting\Models;

use Carbon\Carbon;
use App\Traits\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Targeting\Models\Traits\HasManyAgeGroups;

/**
 * @property integer $id
 * @property string  $name
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Gender extends Model
{
    use HasManyAgeGroups,
        HasFactory;

    public const TYPE_NAME = 'gender';
    public const PERMISSION_NAME = 'targeting_ages';

    /**
     * Stored name for imported from HAAPI value.
     */
    public const ALL = 'all';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @return string
     */
    public function getTranslatedNameAttribute(): string
    {
        return trans('targeting::labels.genders.' . $this->name);
    }

    /**
     * @return Model|Gender
     */
    public static function getAllGender()
    {
        return self::where('name', self::ALL)->firstOrFail();
    }
}
