<?php

namespace Modules\Targeting\Models;

use Modules\Targeting\Models\Traits\BelongsToGender;

/**
 * @property integer $gender_id
 */
class AgeGroup extends TargetingValue
{
    use BelongsToGender;

    public const TYPE_NAME = 'age_group';
    public const PERMISSION_NAME = 'targeting_ages';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'external_id',
        'gender_id',
        'visible',
    ];
}
