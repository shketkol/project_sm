<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Scopes
{
    /**
     * Scope a query to only include visible targetings.
     *
     * @param Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible(Builder $query): Builder
    {
        return $query->where('visible', true);
    }

    /**
     * Scope a query to only include invisible targetings.
     *
     * @param Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInvisible(Builder $query): Builder
    {
        return $query->where('visible', false);
    }
}
