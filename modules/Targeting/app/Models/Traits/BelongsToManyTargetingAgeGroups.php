<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Modules\Targeting\Models\AgeGroup;

/**
 * @property AgeGroup[]|Collection $targetingAgeGroups
 */
trait BelongsToManyTargetingAgeGroups
{
    /**
     * @return BelongsToMany
     */
    public function targetingAgeGroups(): BelongsToMany
    {
        return $this->belongsToMany(AgeGroup::class, 'campaign_age_groups');
    }
}
