<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Modules\Targeting\Models\DeviceGroup;

/**
 * @property DeviceGroup[]|Collection $deviceGroups
 * @property DeviceGroup[]|Collection $deviceGroupsIncluded
 * @property DeviceGroup[]|Collection $deviceGroupsExcluded
 */
trait BelongsToManyDeviceGroups
{
    /**
     * @return BelongsToMany
     */
    public function deviceGroups(): BelongsToMany
    {
        return $this->belongsToMany(DeviceGroup::class, 'campaign_device_groups')
            ->withPivot('excluded');
    }

    /**
     * @return BelongsToMany
     */
    public function deviceGroupsIncluded(): BelongsToMany
    {
        return $this->deviceGroups()->wherePivot('excluded', '=', false);
    }

    /**
     * @return BelongsToMany
     */
    public function deviceGroupsExcluded(): BelongsToMany
    {
        return $this->deviceGroups()->wherePivot('excluded', '=', true);
    }

    /**
     * Get the devices list from devices groups attached to the campaign.
     *
     * @return Collection
     */
    public function getDevices(): Collection
    {
        return $this
            ->deviceGroups()
            ->with('devicesVisible')
            ->get()
            ->pluck('devicesVisible')
            ->flatten();
    }
}
