<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Type;

/**
 * @property Location[]|Collection $locations
 * @property Location[]|Collection $locationsIncluded
 * @property Location[]|Collection $locationsExcluded
 * @property Location[]|Collection $dma
 * @property Location[]|Collection $cities
 * @property Location[]|Collection $states
 */
trait BelongsToManyLocations
{
    /**
     * @return BelongsToMany
     */
    public function locations(): BelongsToMany
    {
        return $this->belongsToMany(Location::class, 'campaign_locations')
            ->withPivot('excluded');
    }

    /**
     * @return BelongsToMany
     */
    public function locationsIncluded(): BelongsToMany
    {
        return $this->locations()->wherePivot('excluded', '=', false);
    }

    /**
     * @return BelongsToMany
     */
    public function locationsExcluded(): BelongsToMany
    {
        return $this->locations()->wherePivot('excluded', '=', true);
    }

    /**
     * @return BelongsToMany
     */
    public function dma(): BelongsToMany
    {
        return $this->locations()->where(['type' => Type::HAAPI_DMA_NAME]);
    }

    /**
     * @return BelongsToMany
     */
    public function cities(): BelongsToMany
    {
        return $this->locations()->where(['type' => Type::HAAPI_CITY_NAME]);
    }

    /**
     * @return BelongsToMany
     */
    public function states(): BelongsToMany
    {
        return $this->locations()->where(['type' => Type::HAAPI_STATE_NAME]);
    }
}
