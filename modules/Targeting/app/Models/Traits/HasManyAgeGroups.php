<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Targeting\Models\AgeGroup;

trait HasManyAgeGroups
{
    /**
     * @return HasMany
     */
    public function ageGroups(): HasMany
    {
        return $this->hasMany(AgeGroup::class, 'gender_id');
    }
}
