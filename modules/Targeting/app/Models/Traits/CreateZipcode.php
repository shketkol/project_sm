<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\TargetingValue;
use Modules\Targeting\Models\Zipcode;

trait CreateZipcode
{
    /**
     * Create test targeting location using factory.
     *
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Zipcode|Zipcode[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestTargetingZipcode(array $attributes = [], int $count = null)
    {
        return Zipcode::factory()->count($count)->create($attributes);
    }

    /**
     * @param Collection $collection
     *
     * @return Collection
     */
    private function createTargetingZipcodesPayload(Collection $collection): Collection
    {
        return $collection->map(function (TargetingValue $value) {
            return [
                'zipcode_id' => $value->id,
                'excluded'    => rand(0, 1),
            ];
        });
    }

    /**
     * @param Campaign   $campaign
     * @param Collection $collection
     */
    private function syncCampaignTargetingZipcodes(Campaign $campaign, Collection $collection): void
    {
        $campaign->zipcodes()->sync($collection->toArray());
    }
}
