<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Modules\Targeting\Models\Device;

/**
 * @property Device[]|Collection $devices
 */
trait HasManyDevices
{
    /**
     * @return HasMany
     */
    public function devices(): HasMany
    {
        return $this->hasMany(Device::class, 'group_id');
    }

    /**
     * @return HasMany
     */
    public function devicesVisible(): HasMany
    {
        return $this->devices()->visible();
    }
}
