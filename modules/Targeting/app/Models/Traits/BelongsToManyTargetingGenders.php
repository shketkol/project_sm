<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Modules\Targeting\Models\Gender;

/**
 * @property Gender[]|Collection $targetingGenders
 */
trait BelongsToManyTargetingGenders
{
    /**
     * @return BelongsToMany
     */
    public function targetingGenders(): BelongsToMany
    {
        return $this->belongsToMany(Gender::class, 'campaign_genders');
    }
}
