<?php

namespace Modules\Targeting\Models\Traits;

trait TargetingValueAccessors
{
    /**
     * Check if the targeting value has relation with the campaign and return it's "excluded" attribute,
     * false - otherwise.
     *
     * @return bool
     */
    public function getIsExcluded(): bool
    {
        return $this->pivot && $this->pivot->excluded;
    }
}
