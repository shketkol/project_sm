<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Modules\Targeting\Models\Audience;

/**
 * @property Audience[]|Collection $audiences
 */
trait BelongsToManyAudiences
{
    /**
     * @return BelongsToMany
     */
    public function audiences(): BelongsToMany
    {
        return $this->belongsToMany(Audience::class, 'campaign_audiences');
    }
}
