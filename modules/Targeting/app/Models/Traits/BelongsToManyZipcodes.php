<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Modules\Targeting\Models\Zipcode;

/**
 * @property Zipcode[]|Collection $zipcodes
 */
trait BelongsToManyZipcodes
{
    /**
     * @return BelongsToMany
     */
    public function zipcodes(): BelongsToMany
    {
        return $this->belongsToMany(Zipcode::class, 'campaign_zipcodes')
            ->withPivot('excluded');
    }

    /**
     * @return BelongsToMany
     */
    public function zipCodesIncluded(): BelongsToMany
    {
        return $this->zipcodes()->wherePivot('excluded', '=', false);
    }

    /**
     * @return BelongsToMany
     */
    public function zipCodesExcluded(): BelongsToMany
    {
        return $this->zipcodes()->wherePivot('excluded', '=', true);
    }
}
