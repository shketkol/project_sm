<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Modules\Targeting\Models\Genre;

/**
 * @property Genre[]|Collection $genres
 * @property Genre[]|Collection $genresIncluded
 * @property Genre[]|Collection $genresExcluded
 */
trait BelongsToManyGenres
{
    /**
     * @return BelongsToMany
     */
    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class, 'campaign_genres')
            ->withPivot('excluded');
    }

    /**
     * @return BelongsToMany
     */
    public function genresIncluded(): BelongsToMany
    {
        return $this->genres()->wherePivot('excluded', '=', false);
    }

    /**
     * @return BelongsToMany
     */
    public function genresExcluded(): BelongsToMany
    {
        return $this->genres()->wherePivot('excluded', '=', true);
    }
}
