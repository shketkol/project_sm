<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\TargetingValue;

trait CreateAgeGroup
{
    /**
     * Create test targeting ageGroup using factory.
     *
     * @param array    $attributes
     * @param int|null $count
     *
     * @return AgeGroup|AgeGroup[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestTargetingAgeGroup(array $attributes = [], int $count = null)
    {
        return AgeGroup::factory()->count($count)->create($attributes);
    }

    /**
     * @param Collection $collection
     *
     * @return Collection
     */
    private function createTargetingAgeGroupsPayload(Collection $collection): Collection
    {
        return $collection->map(function (TargetingValue $value) {
            return $value->id;
        });
    }

    /**
     * @param Campaign   $campaign
     * @param Collection $collection
     */
    private function syncCampaignTargetingAgeGroups(Campaign $campaign, Collection $collection): void
    {
        $campaign->targetingAgeGroups()->sync($collection->toArray());
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    protected function createTargetingGendersPayload(Collection $collection): Collection
    {
        return $collection->map(function (TargetingValue $value) {
            return $value->gender->id;
        });
    }

    /**
     * @param Campaign $campaign
     * @param Collection $collection
     */
    protected function syncTargetingGenders(Campaign $campaign, Collection $collection): void
    {
        $campaign->targetingGenders()->sync($collection->toArray());
    }
}
