<?php

namespace Modules\Targeting\Models\Traits;

use Modules\Targeting\Models\Gender;

trait CreateGender
{
    /**
     * Create test targeting gender using factory.
     *
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Gender|Gender[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestTargetingGender(array $attributes = [], int $count = null)
    {
        return Gender::factory()->count($count)->create($attributes);
    }
}
