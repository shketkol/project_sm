<?php

namespace Modules\Targeting\Models\Traits;

trait ScoutIndex
{
    /**
     * Disable Laravel Scout index.
     */
    private function disableScoutIndex(): void
    {
        config(['scout.driver' => null]);
    }
}
