<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\TargetingValue;

trait CreateLocation
{
    /**
     * Create test targeting location using factory.
     *
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Location|Location[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestTargetingLocation(array $attributes = [], int $count = null)
    {
        return Location::factory()->count($count)->create($attributes);
    }

    /**
     * @param Collection $collection
     *
     * @return Collection
     */
    private function createTargetingLocationsPayload(Collection $collection): Collection
    {
        return $collection->map(function (TargetingValue $value) {
            return [
                'location_id' => $value->id,
                'excluded'    => rand(0, 1),
            ];
        });
    }

    /**
     * @param Campaign   $campaign
     * @param Collection $collection
     */
    private function syncCampaignTargetingLocations(Campaign $campaign, Collection $collection): void
    {
        $campaign->locations()->sync($collection->toArray());
    }
}
