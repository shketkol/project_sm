<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\TargetingValue;

trait CreateGenre
{
    /**
     * Create test targeting genre using factory.
     *
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Genre|Genre[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestTargetingGenre(array $attributes = [], int $count = null)
    {
        return Genre::factory()->count($count)->create($attributes);
    }

    /**
     * @param Collection $collection
     *
     * @return Collection
     */
    private function createTargetingGenresPayload(Collection $collection): Collection
    {
        return $collection->map(function (TargetingValue $value) {
            return [
                'genre_id' => $value->id,
                'excluded' => rand(0, 1),
            ];
        });
    }

    /**
     * @param Campaign   $campaign
     * @param Collection $collection
     */
    private function syncCampaignTargetingGenre(Campaign $campaign, Collection $collection): void
    {
        $campaign->genres()->sync($collection->toArray());
    }
}
