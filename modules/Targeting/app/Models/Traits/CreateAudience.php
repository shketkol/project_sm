<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Support\Collection;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\TargetingValue;

trait CreateAudience
{
    /**
     * Create test targeting audience using factory.
     *
     * @param array    $attributes
     * @param int|null $count
     *
     * @return Audience|Audience[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestTargetingAudience(array $attributes = [], int $count = null)
    {
        return Audience::factory()->count($count)->create($attributes);
    }

    /**
     * @param Collection $collection
     *
     * @return Collection
     */
    private function createTargetingAudiencePayload(Collection $collection): Collection
    {
        return $collection->map(function (TargetingValue $value) {
            return [
                'audience_id' => $value->id,
            ];
        });
    }
}
