<?php

namespace Modules\Targeting\Models\Traits;

use Illuminate\Support\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\DeviceGroup;

trait CreateDeviceGroup
{
    /**
     * @param array    $attributes
     * @param int|null $count
     *
     * @return DeviceGroup|DeviceGroup[]|\Illuminate\Database\Eloquent\Collection
     */
    private function createTestDeviceGroup(array $attributes = [], int $count = null)
    {
        return DeviceGroup::factory()->count($count)->create($attributes);
    }

    /**
     * @param Collection $collection
     *
     * @return Collection
     */
    private function createDeviceGroupPayload(Collection $collection): Collection
    {
        return $collection->pluck('id');
    }

    /**
     * @param Campaign   $campaign
     * @param Collection $collection
     */
    private function syncCampaignDeviceGroup(Campaign $campaign, Collection $collection): void
    {
        $campaign->deviceGroups()->sync($collection->toArray());
    }
}
