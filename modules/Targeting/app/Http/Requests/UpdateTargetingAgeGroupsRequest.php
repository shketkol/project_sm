<?php

namespace Modules\Targeting\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class UpdateTargetingAgeGroupsRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'ageGroups' => $validationRules->only(
                'targeting.age_group',
                ['checkAgeGroups', 'targeting_to_account']
            ),
            'ageGroups.*' => $validationRules->only(
                'targeting.age_group_id',
                ['required', 'integer', 'min', 'max']
            ),
            'genderId' => $validationRules->only(
                'targeting.gender.id',
                ['nullable', 'integer', 'min', 'max', 'targeting_to_account']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        return $this->all();
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'ageGroups.required' => __('targeting::messages.empty_age_groups'),
        ];
    }
}
