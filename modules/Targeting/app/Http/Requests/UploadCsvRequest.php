<?php

namespace Modules\Targeting\Http\Requests;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Invitation\Http\Requests\Traits\UploadCsv;

class UploadCsvRequest extends FormRequest
{
    use UploadCsv;

    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'csv' => $validationRules->only(
                'targeting.location.zip_csv',
                ['required', 'max_filesize']
            ),
            'content' => $validationRules->only(
                'targeting.location.zip_csv',
                ['format_csv', 'max_count_csv']
            ),
        ];
    }
}
