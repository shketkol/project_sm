<?php

namespace Modules\Targeting\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\ValidationRules;

/**
 * @property int|null $genderId
 */
class IndexAgeGroupsRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'genderId' => $validationRules->only('targeting.gender.id', ['min', 'max', 'integer']),
        ];
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        return $this->all();
    }
}
