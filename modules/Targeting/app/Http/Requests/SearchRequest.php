<?php

namespace Modules\Targeting\Http\Requests;

use App\Http\Requests\SearchRequest as BaseSearchRequest;

/**
 * @property string $query
 */
class SearchRequest extends BaseSearchRequest
{
    /**
     * @var string
     */
    protected $filterRuleName =  'targeting.search.filter_query';

    /**
     * @var string
     */
    protected $validationRuleName = 'targeting.search.query';
}
