<?php

namespace Modules\Targeting\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class SearchZipcodesRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'zipcodes'   => $validationRules->only(
                'targeting.search.values',
                ['required', 'min', 'array']
            ),
            'zipcodes.*' => $validationRules->only(
                'targeting.search.zipcode',
                ['required', 'string']
            ),
        ];
    }
}
