<?php

namespace Modules\Targeting\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class UpdateTargetingDeviceGroupsRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'values.*.device_group_id' => $validationRules->only(
                'targeting.device_group.id',
                ['required', 'integer', 'min', 'max', 'exists']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        return $this->get('values', []);
    }
}
