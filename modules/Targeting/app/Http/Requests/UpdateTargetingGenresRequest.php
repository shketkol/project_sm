<?php

namespace Modules\Targeting\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class UpdateTargetingGenresRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'genres' => $validationRules->only(
                'targeting.genre.ids',
                ['array', 'targeting_to_account']
            ),
            'genres.*.genre_id' => $validationRules->only(
                'targeting.genre.id',
                ['required', 'integer', 'min', 'max', 'exists']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        return $this->get('genres', []);
    }
}
