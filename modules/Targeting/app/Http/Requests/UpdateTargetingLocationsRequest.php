<?php

namespace Modules\Targeting\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class UpdateTargetingLocationsRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'locations'               => $validationRules->only(
                'targeting.values',
                ['array']
            ),
            'locations.*.location_id' => $validationRules->only(
                'targeting.location.id',
                ['required', 'integer', 'exists', 'min', 'max']
            ),
            'locations.*.excluded'    => $validationRules->only(
                'targeting.excluded',
                ['required', 'integer', 'min', 'max']
            ),
            'zipcodes'                => $validationRules->only(
                'targeting.zipcode.ids',
                ['array', 'targeting_to_account']
            ),
            'zipcodes.*.zipcode_id'   => $validationRules->only(
                'targeting.zipcode.id',
                ['required', 'integer', 'exists', 'min', 'max']
            ),
            'zipcodes.*.excluded'     => $validationRules->only(
                'targeting.excluded',
                ['required', 'integer', 'min', 'max']
            ),
        ];
    }
}
