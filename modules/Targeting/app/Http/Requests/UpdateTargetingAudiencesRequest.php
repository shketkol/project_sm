<?php

namespace Modules\Targeting\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class UpdateTargetingAudiencesRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'audiences'               => $validationRules->only(
                'targeting.audience.ids',
                ['array', 'targeting_to_account']
            ),
            'audiences.*.audience_id' => $validationRules->only(
                'targeting.audience.id',
                ['required', 'integer', 'min', 'max', 'exists']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        return $this->get('audiences', []);
    }
}
