<?php

namespace Modules\Targeting\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \Modules\Targeting\Models\Device
 */
class DeviceGroupResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'label'       => __("targeting::labels.devices.options.{$this->name}"),
            'description' => __("campaign::labels.infotips.devices.{$this->name}"),
        ];
    }
}
