<?php

namespace Modules\Targeting\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \Modules\Targeting\Models\Location
 */
class LocationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'excluded' => $this->whenPivotLoaded('campaign_locations', function () {
                return $this->pivot->excluded;
            }),
        ];
    }
}
