<?php

namespace Modules\Targeting\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \Modules\Targeting\Models\Zipcode
 */
class ZipcodeResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'excluded' => $this->whenPivotLoaded('campaign_zipcodes', function () {
                return $this->pivot->excluded;
            }),
        ];
    }
}
