<?php

namespace Modules\Targeting\Http\Resources;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Targeting\Commands\Traits\AgeGroupsHelper;
use Modules\Targeting\Http\Resources\Traits\AgeGroupsName;
use Modules\Targeting\Repositories\GenderRepository;

/**
 * @mixin \Modules\Targeting\Models\AgeGroup
 */
class AgeGroupsFormatAction
{
    use AgeGroupsName, AgeGroupsHelper;

    /**
     * Get age groups data
     *
     * @param Collection $ageGroups
     * @param Collection $campaignGenders
     *
     * @return array
     */
    public static function handle(Collection $ageGroups, Collection $campaignGenders): array
    {
        $issetGenderMix = self::checkGenderMix($ageGroups, $campaignGenders);
        $repository = app(GenderRepository::class);

        $genders = $repository->findWhereIn('id', $ageGroups->pluck('gender_id')->unique()->toArray())
            ->keyBy('id')
            ->toArray();

        return array_map(function ($item) use ($issetGenderMix, $genders) {
            return [
                'id'   => Arr::get($item, 'id'),
                'name' => !$issetGenderMix ? Arr::get($item, 'name') : self::getName($item, $genders),
            ];
        }, $ageGroups->toArray());
    }

    /**
     * Get age group name
     *
     * @param array $item
     * @param array $genders
     *
     * @return string
     */
    private static function getName(array $item, array $genders): string
    {
        $genderName = $genders[$item['gender_id']]['name'];

        return self::getFullName($genderName, $item['name']);
    }
}
