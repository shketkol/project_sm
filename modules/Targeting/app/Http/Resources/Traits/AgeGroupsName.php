<?php

namespace Modules\Targeting\Http\Resources\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Modules\Targeting\Models\AgeGroup;

trait AgeGroupsName
{
    /**
     * Get mix age group with gender type
     *
     * @param AgeGroup|Collection $item
     *
     * @return string
     */
    protected function getMixName($item): string
    {
        $genderName = $item->gender->name;

        return self::getFullName($genderName, $item->name);
    }

    /**
     * Get full age group name
     *
     * @param string $genderName
     * @param string $ageName
     *
     * @return string
     */
    protected static function getFullName(string $genderName, string $ageName): string
    {
        return Str::ucfirst($genderName . ' ' . $ageName);
    }
}
