<?php

namespace Modules\Targeting\Http\Controllers\Api\Genders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Targeting\Http\Resources\GenderResource;
use Modules\Targeting\Repositories\Contracts\GenderRepository;

class IndexGendersController extends Controller
{
    /**
     * Get list of all available genders.
     *
     * @param GenderRepository $genderRepository
     * @return AnonymousResourceCollection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(GenderRepository $genderRepository): AnonymousResourceCollection
    {
        return GenderResource::collection($genderRepository->notEmptyCriteria()->all());
    }
}
