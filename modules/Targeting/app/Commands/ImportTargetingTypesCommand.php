<?php

namespace Modules\Targeting\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Types\FetchTypesAction;
use Modules\Targeting\Actions\Types\ImportTypesAction;
use Modules\Targeting\Actions\Types\UpdateTypesAction;
use Modules\Targeting\Commands\Traits\Progress;
use Modules\Targeting\Exceptions\ImportFailedException;
use Psr\Log\LoggerInterface;

class ImportTargetingTypesCommand extends Command
{
    use Progress;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:types:import {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import campaign targeting types from HAAPI to DB.';

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var FetchTypesAction
     */
    protected $fetchAction;

    /**
     * @var ImportTypesAction
     */
    protected $importAction;

    /**
     * @var UpdateTypesAction
     */
    protected $updateAction;

    /**
     * @param DatabaseManager   $databaseManager
     * @param LoggerInterface   $log
     * @param FetchTypesAction  $fetchAction
     * @param ImportTypesAction $importAction
     * @param UpdateTypesAction $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchTypesAction $fetchAction,
        ImportTypesAction $importAction,
        UpdateTypesAction $updateAction
    ) {
        parent::__construct();

        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->fetchAction = $fetchAction;
        $this->importAction = $importAction;
        $this->updateAction = $updateAction;
    }

    /**
     * @throws ImportFailedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->info(sprintf('Started import campaign targeting from "%s".', $this->getSourceName()));

        ['limit' => $limit] = $this->getImportOptions();

        $this->startProgressBar($limit);
        $this->runImport();
        $this->finishProgressBar();

        $this->info('Import done.');
    }

    /**
     * Import targeting types.
     *
     * @throws ImportFailedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    private function runImport(): void
    {
        ['limit' => $limit] = $this->getImportOptions();
        $data = $this->fetchAction->handle();
        $amount = $data->count();

        if (0 === $amount) {
            $this->info('Nothing to import.');
            return;
        }

        $this->import($data);

        $this->updateProgressBarSteps($limit, $amount);
    }

    /**
     * @return string
     */
    private function getSourceName(): string
    {
        return get_class($this->fetchAction);
    }

    /**
     * @return array
     */
    private function getImportOptions(): array
    {
        return [
            'limit' => (int)config('haapi.targeting.types_list.limit'),
        ];
    }

    /**
     * @param TargetingValuesCollection $data
     *
     * @throws ImportFailedException
     * @throws \Exception
     */
    private function import(TargetingValuesCollection $data): void
    {
        $this->databaseManager->beginTransaction();

        try {
            if ($this->option('update')) {
                $this->updateAction->handle($data);
            } else {
                $this->importAction->handle($data);
            }

            $this->advanceProgressBar($data->count());
        } catch (\Throwable $exception) {
            $message = sprintf(
                'Import campaign targeting from "%s" failed. Reason: "%s".',
                $this->getSourceName(),
                $exception->getMessage()
            );

            $this->error($message);
            $this->databaseManager->rollBack();

            throw ImportFailedException::create($message, $exception);
        }

        $this->databaseManager->commit();
    }
}
