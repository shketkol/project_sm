<?php

namespace Modules\Targeting\Commands;

use Illuminate\Database\DatabaseManager;
use Modules\Targeting\Actions\Location\Zipcode\ImportZipcodesAction;
use Modules\Targeting\Actions\Location\Zipcode\UpdateZipcodesAction;
use Modules\Targeting\Actions\Location\Zipcode\FetchZipcodesAction;
use Psr\Log\LoggerInterface;

class ImportZipcodesCommand extends ImportTargetingValuesCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:zipcodes:import {--start=0} {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import campaign targeting zipcodes from HAAPI to DB.';

    /**
     * @param DatabaseManager      $databaseManager
     * @param LoggerInterface      $log
     * @param FetchZipcodesAction  $fetchAction
     * @param ImportZipcodesAction $importAction
     * @param UpdateZipcodesAction $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchZipcodesAction $fetchAction,
        ImportZipcodesAction $importAction,
        UpdateZipcodesAction $updateAction
    ) {
        parent::__construct($databaseManager, $log, $fetchAction, $importAction, $updateAction);
    }
}
