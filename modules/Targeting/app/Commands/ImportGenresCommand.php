<?php

namespace Modules\Targeting\Commands;

use Illuminate\Database\DatabaseManager;
use Modules\Targeting\Actions\Genre\FetchGenresAction;
use Modules\Targeting\Actions\Genre\ImportGenresAction;
use Modules\Targeting\Actions\Genre\UpdateGenresAction;
use Psr\Log\LoggerInterface;

class ImportGenresCommand extends ImportTargetingValuesCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:genres:import {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import campaign targeting genres from HAAPI to DB.';

    /**
     * @param DatabaseManager    $databaseManager
     * @param LoggerInterface    $log
     * @param FetchGenresAction  $fetchAction
     * @param ImportGenresAction $importAction
     * @param UpdateGenresAction $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchGenresAction $fetchAction,
        ImportGenresAction $importAction,
        UpdateGenresAction $updateAction
    ) {
        parent::__construct($databaseManager, $log, $fetchAction, $importAction, $updateAction);
    }
}
