<?php

namespace Modules\Targeting\Commands;

use Illuminate\Database\DatabaseManager;
use Modules\Targeting\Actions\Audience\ImportAudienceAction;
use Modules\Targeting\Actions\Audience\FetchAudienceAction;
use Modules\Targeting\Actions\Audience\UpdateAudienceAction;
use Psr\Log\LoggerInterface;

class ImportAudiencesCommand extends ImportTargetingValuesCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:audiences:import {--start=0} {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import campaign targeting audience from HAAPI to DB.';

    /**
     * @param DatabaseManager $databaseManager
     * @param LoggerInterface $log
     * @param FetchAudienceAction $fetchAction
     * @param ImportAudienceAction $importAction
     * @param UpdateAudienceAction $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchAudienceAction $fetchAction,
        ImportAudienceAction $importAction,
        UpdateAudienceAction $updateAction
    ) {
        parent::__construct($databaseManager, $log, $fetchAction, $importAction, $updateAction);
    }
}
