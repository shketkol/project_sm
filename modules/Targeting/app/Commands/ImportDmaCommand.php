<?php

namespace Modules\Targeting\Commands;

use Illuminate\Database\DatabaseManager;
use Modules\Targeting\Actions\Location\Dma\ImportDmaAction;
use Modules\Targeting\Actions\Location\Dma\FetchDmaAction;
use Modules\Targeting\Actions\Location\Dma\UpdateDmaAction;
use Psr\Log\LoggerInterface;

class ImportDmaCommand extends ImportTargetingValuesCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:dma:import {--start=0} {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import campaign targeting DMA (Designated Market Area) from HAAPI to DB.';

    /**
     * @param DatabaseManager $databaseManager
     * @param LoggerInterface $log
     * @param FetchDmaAction  $fetchAction
     * @param ImportDmaAction $importAction
     * @param UpdateDmaAction $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchDmaAction $fetchAction,
        ImportDmaAction $importAction,
        UpdateDmaAction $updateAction
    ) {
        parent::__construct($databaseManager, $log, $fetchAction, $importAction, $updateAction);
    }
}
