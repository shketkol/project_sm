<?php

namespace Modules\Targeting\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Modules\Targeting\Actions\Contracts\FetchAction;
use Modules\Targeting\Actions\Contracts\ImportAction;
use Modules\Targeting\Actions\Contracts\UpdateAction;
use Modules\Targeting\Commands\Traits\ImportTargetingProcess;
use Modules\Targeting\Commands\Traits\Progress;
use Modules\Targeting\Exceptions\ImportFailedException;
use Psr\Log\LoggerInterface;

abstract class ImportTargetingValuesCommand extends Command
{
    use Progress, ImportTargetingProcess;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var FetchAction
     */
    protected $fetchAction;

    /**
     * @var ImportAction
     */
    protected $importAction;

    /**
     * @var UpdateAction
     */
    protected $updateAction;

    /**
     * @param DatabaseManager $databaseManager
     * @param LoggerInterface $log
     * @param FetchAction     $fetchAction
     * @param ImportAction    $importAction
     * @param UpdateAction      $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchAction $fetchAction,
        ImportAction $importAction,
        UpdateAction $updateAction
    ) {
        parent::__construct();

        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->fetchAction = $fetchAction;
        $this->importAction = $importAction;
        $this->updateAction = $updateAction;
    }

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        $isUpdateValues = $this->option('update');
        if ($isUpdateValues) {
            $this->info(sprintf('Started update campaign targeting from "%s".', $this->getSourceName()));
        } else {
            $this->info(sprintf('Started import campaign targeting from "%s".', $this->getSourceName()));
        }

        ['limit' => $limit] = $this->getImportOptions();

        $this->startProgressBar($limit);
        $this->paginateImport($isUpdateValues);
        $this->finishProgressBar();

        if ($isUpdateValues) {
            $this->info('Update done.');
        } else {
            $this->info('Import done.');
        }
    }

    /**
     * Paginate import until there are next records or until timeout.
     *
     * @param bool $isUpdateValues
     * @throws ImportFailedException
     * @throws \Throwable
     */
    private function paginateImport(bool $isUpdateValues): void
    {
        ['limit' => $limit, 'start' => $start, 'externalId' => $externalId] = $this->getImportOptions();

        do {
            if ($this->isTimeout()) {
                $this->error(PHP_EOL . 'Timeout import.');
                break;
            }

            $data = $this->fetchAction->handle($limit, $start, $externalId);
            $amount = $data->targetValues->count();

            if (0 === $amount) {
                $this->info('Nothing to import.');
                break;
            }

            if ($isUpdateValues) {
                $this->update($data);
            } else {
                $this->import($data);
            }

            $start = $data->next;
            $externalId = $data->externalId;

            $this->updateProgressBarSteps($limit, $amount);
        } while (!is_null($data->next));
    }
}
