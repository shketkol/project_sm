<?php

namespace Modules\Targeting\Commands;

use Illuminate\Database\DatabaseManager;
use Modules\Targeting\Actions\Location\City\FetchCitiesAction;
use Modules\Targeting\Actions\Location\City\ImportCitiesAction;
use Modules\Targeting\Actions\Location\City\UpdateCitiesAction;
use Psr\Log\LoggerInterface;

class ImportCitiesCommand extends ImportTargetingValuesCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:cities:import {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import campaign targeting cities from HAAPI to DB.';

    /**
     * @param DatabaseManager    $databaseManager
     * @param LoggerInterface    $log
     * @param FetchCitiesAction  $fetchAction
     * @param ImportCitiesAction $importAction
     * @param UpdateCitiesAction $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchCitiesAction $fetchAction,
        ImportCitiesAction $importAction,
        UpdateCitiesAction $updateAction
    ) {
        parent::__construct($databaseManager, $log, $fetchAction, $importAction, $updateAction);
    }
}
