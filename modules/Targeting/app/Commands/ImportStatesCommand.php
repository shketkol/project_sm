<?php

namespace Modules\Targeting\Commands;

use Illuminate\Database\DatabaseManager;
use Modules\Targeting\Actions\Location\State\ImportStatesAction;
use Modules\Targeting\Actions\Location\State\FetchStatesAction;
use Modules\Targeting\Actions\Location\State\UpdateStatesAction;
use Psr\Log\LoggerInterface;

class ImportStatesCommand extends ImportTargetingValuesCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:states:import {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import campaign targeting states from HAAPI to DB.';

    /**
     * @param DatabaseManager    $databaseManager
     * @param LoggerInterface    $log
     * @param FetchStatesAction  $fetchAction
     * @param ImportStatesAction $importAction
     * @param UpdateStatesAction $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchStatesAction $fetchAction,
        ImportStatesAction $importAction,
        UpdateStatesAction $updateAction
    ) {
        parent::__construct($databaseManager, $log, $fetchAction, $importAction, $updateAction);
    }
}
