<?php

namespace Modules\Targeting\Commands;

use Illuminate\Database\DatabaseManager;
use Modules\Targeting\Actions\Device\ActualizeDeviceGroupsAction;
use Modules\Targeting\Actions\Device\FetchDevicesAction;
use Modules\Targeting\Actions\Device\ImportDevicesAction;
use Modules\Targeting\Actions\Device\UpdateDevicesAction;
use Psr\Log\LoggerInterface;

class ImportDevicesCommand extends ImportTargetingValuesCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:devices:import {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import campaign targeting devices from HAAPI to DB.';

    /**
     * @var ActualizeDeviceGroupsAction
     */
    private $actualizeAction;

    /**
     * @param DatabaseManager     $databaseManager
     * @param LoggerInterface     $log
     * @param FetchDevicesAction  $fetchAction
     * @param ImportDevicesAction $importAction
     * @param UpdateDevicesAction $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchDevicesAction $fetchAction,
        ImportDevicesAction $importAction,
        UpdateDevicesAction $updateAction,
        ActualizeDeviceGroupsAction $actualizeAction
    ) {
        $this->actualizeAction = $actualizeAction;
        parent::__construct($databaseManager, $log, $fetchAction, $importAction, $updateAction);
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        parent::handle();

        $this->info('Started device groups actualization (Setting visibility flags, Removing groups without devices).');
        $this->actualizeAction->handle();
        $this->info('Device groups actualization is finished.');
    }
}
