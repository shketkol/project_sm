<?php

namespace Modules\Targeting\Commands\Traits;

use Carbon\Carbon;

/**
 * @property \Symfony\Component\Console\Helper\ProgressBar $bar
 * @property \Symfony\Component\Console\Style\SymfonyStyle $output
 */
trait Progress
{
    /**
     * @var \Symfony\Component\Console\Helper\ProgressBar
     */
    private $bar;

    /**
     * @param int $limit
     */
    private function startProgressBar(int $limit): void
    {
        $this->bar = $this->output->createProgressBar($limit);
        $this->bar->setFormat($this->bar::getFormatDefinition('debug'));
        $this->bar->start();
    }

    /**
     * @param int $limit
     * @param int $amount
     */
    private function updateProgressBarSteps(int $limit, int $amount): void
    {
        // we've imported as many as limit allowed
        if ($limit === $amount) {
            $this->bar->setMaxSteps($this->bar->getMaxSteps() + $amount);
        }

        // we've imported less then import allowed
        if ($amount < $limit) {
            $this->bar->setMaxSteps($this->bar->getMaxSteps() - $limit - $amount);
        }
    }

    /**
     * Advances the progress output.
     *
     * @param int $step
     */
    private function advanceProgressBar(int $step = 1): void
    {
        $this->bar->advance($step);
    }

    /**
     * Finishes the progress output.
     */
    private function finishProgressBar(): void
    {
        $this->bar->finish();
        $this->output->writeln(PHP_EOL);
    }

    /**
     * @return bool
     */
    private function isTimeout(): bool
    {
        $now = Carbon::now();
        $end = Carbon::createFromTimestamp($this->bar->getStartTime() + $this->getTimeout());

        return $now->gt($end);
    }

    /**
     * @return int
     */
    private function getTimeout(): int
    {
        return config('haapi.targeting.timeout');
    }
}
