<?php

namespace Modules\Targeting\Commands\Traits;

use Illuminate\Support\Collection;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Repositories\AgeGroupRepository;

trait AgeGroupsHelper
{
    /**
     * Get prepared attributes
     *
     * @param array $attributes
     *
     * @return array
     */
    protected function getAttributes(array $attributes)
    {
        $name = explode(' ', $attributes['name']);
        $attributes['name'] = $name[1];
        return $attributes;
    }

    /**
     * Check has a gender mix
     *
     * @param Collection      $ageGroups
     * @param Collection|null $campaignGenders
     *
     * @return bool
     */
    public static function checkGenderMix(Collection $ageGroups, Collection $campaignGenders = null): bool
    {
        $uniqueGendersIdsFromAgeGroup = $ageGroups->pluck('gender_id')->unique();

        // more than one -> mix
        if ($uniqueGendersIdsFromAgeGroup->count() !== 1) {
            return true;
        }

        // no campaign genders and only one gender in age_groups -> not mix
        if (is_null($campaignGenders)) {
            return false;
        }

        // only one gender in age_groups and campaign gender is 'all' -> not mix
        if ($campaignGenders->first()->name === Gender::ALL
            && $uniqueGendersIdsFromAgeGroup->first() === $campaignGenders->first()->id) {
            return false;
        }

        // only one gender (male or female) in age_groups but campaign gender is 'all' -> mix
        if ($uniqueGendersIdsFromAgeGroup->first() !== $campaignGenders->first()->id) {
            return true;
        }

        // any other -> not mix
        return false;
    }

    /**
     * Check has a gender mix
     *
     * @param AgeGroupRepository $repository
     * @param array              $values
     *
     * @return bool
     */
    public static function checkGenderMixByValues(AgeGroupRepository $repository, array $values): bool
    {
        return self::checkGenderMix($repository->findWhereIn('id', $values));
    }
}
