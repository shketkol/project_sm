<?php

namespace Modules\Targeting\Commands\Traits;

use Modules\Haapi\DataTransferObjects\Targeting\TargetingResponseData;
use Modules\Targeting\Exceptions\ImportFailedException;
use Modules\Targeting\Exceptions\UpdateFailedException;

/**
 * @property \Modules\Targeting\Actions\Contracts\ImportAction $importAction
 * @property \Modules\Targeting\Actions\Contracts\UpdateAction $updateAction
 */
trait ImportTargetingProcess
{
    /**
     * @return string
     */
    private function getSourceName(): string
    {
        return static::class;
    }

    /**
     * @return array
     */
    private function getImportOptions(): array
    {
        return [
            'limit'      => (int)config('haapi.targeting.values_list.limit'),
            'start'      => (int)($this->hasOption('start') ? $this->option('start') : 0),
            'externalId' => null,
        ];
    }

    /**
     * @param TargetingResponseData $data
     *
     * @throws \Throwable
     * @throws ImportFailedException
     */
    private function import(TargetingResponseData $data): void
    {
        $this->databaseManager->beginTransaction();

        try {
            $this->importAction->handle($data->targetValues);
            $this->advanceProgressBar($data->targetValues->count());
        } catch (\Throwable $exception) {
            $message = sprintf(
                'Import campaign targeting from "%s" failed at "%d". Reason: "%s".',
                $this->getSourceName(),
                $data->next,
                $exception->getMessage()
            );

            $this->error($message);
            $this->databaseManager->rollBack();

            throw ImportFailedException::create($message, $exception);
        }

        $this->databaseManager->commit();
    }

    /**
     * @param TargetingResponseData $data
     *
     * @throws \Throwable
     * @throws UpdateFailedException
     */
    private function update(TargetingResponseData $data): void
    {
        $this->databaseManager->beginTransaction();

        try {
            $this->updateAction->handle($data->targetValues);
            $this->advanceProgressBar($data->targetValues->count());
        } catch (\Throwable $exception) {
            $message = sprintf(
                'Update campaign targeting from "%s" failed at "%d". Reason: "%s".',
                $this->getSourceName(),
                $data->next,
                $exception->getMessage()
            );

            $this->error($message);
            $this->databaseManager->rollBack();

            throw UpdateFailedException::create($message, $exception);
        }

        $this->databaseManager->commit();
    }
}
