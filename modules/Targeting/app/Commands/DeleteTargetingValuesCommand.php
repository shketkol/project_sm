<?php

namespace Modules\Targeting\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingResponseData;
use Modules\Targeting\Actions\AgeGroup\DeleteAgeGroupAction;
use Modules\Targeting\Actions\AgeGroup\FetchAgeGroupAction;
use Modules\Targeting\Actions\Audience\DeleteAudienceAction;
use Modules\Targeting\Actions\Audience\FetchAudienceAction;
use Modules\Targeting\Actions\DeleteValuesAction;
use Modules\Targeting\Actions\Device\DeleteDevicesAction;
use Modules\Targeting\Actions\Device\FetchDevicesAction;
use Modules\Targeting\Actions\FetchValuesAction;
use Modules\Targeting\Actions\Genre\DeleteGenresAction;
use Modules\Targeting\Actions\Genre\FetchGenresAction;
use Modules\Targeting\Commands\Traits\ImportTargetingProcess;
use Modules\Targeting\Commands\Traits\Progress;
use Modules\Targeting\Exceptions\DeleteFailedException;
use Modules\Targeting\Exceptions\ImportFailedException;
use Psr\Log\LoggerInterface;

class DeleteTargetingValuesCommand extends Command
{
    use Progress, ImportTargetingProcess;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var DeleteValuesAction
     */
    protected $deleteAction;

    /**
     * @var FetchValuesAction
     */
    protected $fetchAction;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:delete {--age_groups} {--audiences} {--genres} {--devices}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete old targeting values';

    /**
     * @param DatabaseManager $databaseManager
     * @param LoggerInterface $log
     * @return void
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;

        parent::__construct();
    }

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->info('Started delete campaign targeting values.');
        foreach ($this->options() as $key => $value) {
            if ($value && Arr::has($this->mappedOptions(), $key)) {
                $fetchClass = Arr::get($this->mappedOptions(), "{$key}.fetchAction");
                $deleteClass = Arr::get($this->mappedOptions(), "{$key}.deleteAction");

                $this->fetchAction = app($fetchClass);
                $this->deleteAction = app($deleteClass);

                $this->runProcess();
            }
        }

        $this->info(PHP_EOL . 'Delete done.');
    }

    /**
     * @return void
     * @throws ImportFailedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \Modules\Targeting\Exceptions\UpdateFailedException
     * @throws \Throwable
     */
    private function runProcess(): void
    {
        $this->info(sprintf('Started delete campaign targeting from "%s".', $this->getSourceName()));

        ['limit' => $limit] = $this->getImportOptions();

        $this->startProgressBar($limit);
        $this->paginateDelete();
        $this->finishProgressBar();

        $this->info(sprintf('Finished delete campaign targeting from "%s".', $this->getSourceName()));
    }

    /**
     * @return array
     */
    private function mappedOptions(): array
    {
        return [
            'age_groups' => [
                'fetchAction' => FetchAgeGroupAction::class,
                'deleteAction' => DeleteAgeGroupAction::class
            ],
            'audiences' => [
                'fetchAction' => FetchAudienceAction::class,
                'deleteAction' => DeleteAudienceAction::class
            ],
            'genres' => [
                'fetchAction' => FetchGenresAction::class,
                'deleteAction' => DeleteGenresAction::class
            ],
            'devices' => [
                'fetchAction' => FetchDevicesAction::class,
                'deleteAction' => DeleteDevicesAction::class
            ]
        ];
    }

    /**
     * Paginate delete until there are next records or until timeout.
     *
     * @return void
     * @throws ImportFailedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \Modules\Targeting\Exceptions\UpdateFailedException
     * @throws \Throwable
     */
    private function paginateDelete(): void
    {
        ['limit' => $limit, 'start' => $start, 'externalId' => $externalId] = $this->getImportOptions();

        do {
            if ($this->isTimeout()) {
                $this->error(PHP_EOL . 'Timeout import.');
                break;
            }

            $data = $this->fetchAction->handle($limit, $start, $externalId);
            $amount = $data->targetValues->count();

            if (0 === $amount) {
                $this->info('Nothing to import.');
                break;
            }

            $this->delete($data);

            $start = $data->next;
            $externalId = $data->externalId;

            $this->updateProgressBarSteps($limit, $amount);
        } while (!is_null($data->next));
    }

    /**
     * @param TargetingResponseData $data
     * @return void
     * @throws DeleteFailedException
     * @throws \Modules\Targeting\Exceptions\UpdateFailedException
     */
    private function delete(TargetingResponseData $data): void
    {
        $this->databaseManager->beginTransaction();

        try {
            $this->deleteAction->handle($data->targetValues);
            $this->advanceProgressBar($data->targetValues->count());
        } catch (\Throwable $exception) {
            $message = sprintf(
                'Delete campaign targeting from "%s" failed at "%d". Reason: "%s".',
                $this->getSourceName(),
                $data->next,
                $exception->getMessage()
            );

            $this->error($message);
            $this->databaseManager->rollBack();

            throw DeleteFailedException::create($message, $exception);
        }

        $this->databaseManager->commit();
    }
}
