<?php

namespace Modules\Targeting\Commands;

use Illuminate\Database\DatabaseManager;
use Modules\Targeting\Actions\AgeGroup\FetchAgeGroupAction;
use Modules\Targeting\Actions\AgeGroup\ImportAgeGroupAction;
use Modules\Targeting\Actions\AgeGroup\UpdateAgeGroupAction;
use Psr\Log\LoggerInterface;

class ImportAgeGroupsCommand extends ImportTargetingValuesCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'targeting:age-groups:import {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import campaign targeting genders and age groups from HAAPI to DB.';

    /**
     * @param DatabaseManager $databaseManager
     * @param LoggerInterface $log
     * @param FetchAgeGroupAction $fetchAction
     * @param ImportAgeGroupAction $importAction
     * @param UpdateAgeGroupAction $updateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        FetchAgeGroupAction $fetchAction,
        ImportAgeGroupAction $importAction,
        UpdateAgeGroupAction $updateAction
    ) {
        parent::__construct($databaseManager, $log, $fetchAction, $importAction, $updateAction);
    }
}
