<?php

namespace Modules\Targeting\Repositories;

use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Repositories\Contracts\GenreRepository as GenreRepositoryInterface;
use Modules\Targeting\Repositories\Criteria\Genre\AddCampaignGenreCriteria;

class GenreRepository extends TargetingRepository implements GenreRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Genre::class;
    }

    /**
     * @param Campaign $campaign
     *
     * @return self
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function withSavedNonVisible(Campaign $campaign)
    {
        parent::withSavedNonVisible($campaign);

        return $this->pushCriteria(new AddCampaignGenreCriteria());
    }
}
