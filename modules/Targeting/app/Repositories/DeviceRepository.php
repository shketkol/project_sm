<?php

namespace Modules\Targeting\Repositories;

use DB;
use Modules\Targeting\Models\Device;
use Modules\Targeting\Repositories\Contracts\DeviceRepository as DeviceRepositoryInterface;

class DeviceRepository extends TargetingRepository implements DeviceRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Device::class;
    }
}
