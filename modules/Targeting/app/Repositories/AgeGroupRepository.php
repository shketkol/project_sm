<?php

namespace Modules\Targeting\Repositories;

use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Repositories\Contracts\AgeGroupRepository as AgeGroupRepositoryInterface;
use Modules\Targeting\Repositories\Criteria\AgeGroup\AddCampaignAgeGroupCriteria;
use Modules\Targeting\Repositories\Criteria\AgeGroup\OnlyAllGenderValuesCriteria;

class AgeGroupRepository extends TargetingRepository implements AgeGroupRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AgeGroup::class;
    }

    /**
     * @param Campaign $campaign
     *
     * @return self
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function withSavedNonVisible(Campaign $campaign)
    {
        parent::withSavedNonVisible($campaign);

        return $this->pushCriteria(new AddCampaignAgeGroupCriteria());
    }

    /**
     * @return AgeGroupRepositoryInterface
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function onlyAllGenderCriteria(): AgeGroupRepositoryInterface
    {
        return $this->pushCriteria(app(OnlyAllGenderValuesCriteria::class));
    }
}
