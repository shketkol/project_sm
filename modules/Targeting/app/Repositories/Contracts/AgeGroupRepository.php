<?php

namespace Modules\Targeting\Repositories\Contracts;

interface AgeGroupRepository extends TargetingRepository
{
    /**
     * @return AgeGroupRepository
     */
    public function onlyAllGenderCriteria(): AgeGroupRepository;
}
