<?php

namespace Modules\Targeting\Repositories\Contracts;

use Modules\Campaign\Models\Campaign;

interface DeviceGroupRepository extends TargetingRepository
{
    /**
     * @param Campaign $campaign
     *
     * @return $this
     */
    public function withSavedNonVisible(Campaign $campaign);

    /**
     * @return void
     */
    public function actualizeVisibleFlag();

    /**
     * @return void
     */
    public function removeEmptyGroups();
}
