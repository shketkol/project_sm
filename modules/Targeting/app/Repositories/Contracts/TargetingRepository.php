<?php

namespace Modules\Targeting\Repositories\Contracts;

use App\Repositories\Contracts\Repository;
use Modules\Campaign\Models\Campaign;

interface TargetingRepository extends Repository
{
    /**
     * @param Campaign $campaign
     *
     * @return self
     */
    public function withSavedNonVisible(Campaign $campaign);
}
