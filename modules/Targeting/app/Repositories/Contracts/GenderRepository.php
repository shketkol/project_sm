<?php

namespace Modules\Targeting\Repositories\Contracts;

use App\Repositories\Contracts\Repository;
use Modules\Targeting\Models\Gender;

interface GenderRepository extends Repository
{
    /**
     * Return only genders with age groups.
     *
     * @return self
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function notEmptyCriteria(): self;

    /**
     * @return Gender|null
     */
    public function getAllGenderModel(): ?Gender;
}
