<?php

namespace Modules\Targeting\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Modules\Campaign\Models\Campaign;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WithNonVisibleCriteria implements CriteriaInterface
{
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @param RepositoryInterface|Builder $model
     * @param RepositoryInterface         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function (Builder $query) {
            $query->orWhere('visible', true)
                ->orWhere('campaign_id', $this->campaign->id);
        })->groupBy(['id']);
    }
}
