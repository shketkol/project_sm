<?php

namespace Modules\Targeting\Repositories\Criteria\AgeGroup;

use Modules\Targeting\Models\Gender;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class OnlyAllGenderValuesCriteria implements CriteriaInterface
{
    /**
     * @param \Prettus\Repository\Contracts\RepositoryInterface $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model
            ->select('age_groups.*')
            ->join('genders', 'age_groups.gender_id', '=', 'genders.id')
            ->where('genders.name', '=', Gender::ALL);
    }
}
