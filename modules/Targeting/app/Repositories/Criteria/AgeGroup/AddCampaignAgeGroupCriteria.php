<?php

namespace Modules\Targeting\Repositories\Criteria\AgeGroup;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddCampaignAgeGroupCriteria implements CriteriaInterface
{
    /**
     * @param \Prettus\Repository\Contracts\RepositoryInterface $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select('age_groups.*')
            ->leftJoin('campaign_age_groups', 'age_group_id', '=', 'age_groups.id');
    }
}
