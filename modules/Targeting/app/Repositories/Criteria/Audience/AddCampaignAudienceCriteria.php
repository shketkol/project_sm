<?php

namespace Modules\Targeting\Repositories\Criteria\Audience;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddCampaignAudienceCriteria implements CriteriaInterface
{
    /**
     * @param \Prettus\Repository\Contracts\RepositoryInterface $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select('audiences.*')
            ->leftJoin('campaign_audiences', 'audience_id', '=', 'audiences.id');
    }
}
