<?php

namespace Modules\Targeting\Repositories\Criteria\Zipcode;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddCampaignZipcodeCriteria implements CriteriaInterface
{
    /**
     * @param \Prettus\Repository\Contracts\RepositoryInterface $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select('zipcodes.*')
            ->leftJoin('campaign_zipcodes', 'zipcode_id', '=', 'zipcodes.id');
    }
}
