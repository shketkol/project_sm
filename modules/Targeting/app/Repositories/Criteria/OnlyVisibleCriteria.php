<?php

namespace Modules\Targeting\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class OnlyVisibleCriteria implements CriteriaInterface
{
    /**
     * @param RepositoryInterface|Builder $model
     * @param RepositoryInterface         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('visible', true);
    }
}
