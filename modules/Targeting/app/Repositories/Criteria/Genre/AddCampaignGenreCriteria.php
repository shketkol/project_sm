<?php

namespace Modules\Targeting\Repositories\Criteria\Genre;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddCampaignGenreCriteria implements CriteriaInterface
{
    /**
     * @param \Prettus\Repository\Contracts\RepositoryInterface $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select('genres.*')
            ->leftJoin('campaign_genres', 'genre_id', '=', 'genres.id');
    }
}
