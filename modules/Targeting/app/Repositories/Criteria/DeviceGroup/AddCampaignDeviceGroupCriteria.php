<?php

namespace Modules\Targeting\Repositories\Criteria\DeviceGroup;

use Modules\Targeting\Models\DeviceGroup;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddCampaignDeviceGroupCriteria implements CriteriaInterface
{
    /**
     * @param DeviceGroup                                       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select('device_groups.*')
                     ->leftJoin('campaign_device_groups', 'device_group_id', '=', 'device_groups.id');
    }
}
