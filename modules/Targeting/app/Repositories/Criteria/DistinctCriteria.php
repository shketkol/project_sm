<?php

namespace Modules\Targeting\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class DistinctCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private $column;

    /**
     * DistinctCriteria constructor.
     *
     * @param string $column
     */
    public function __construct(string $column)
    {
        $this->column = $column;
    }

    /**
     * @param RepositoryInterface|Builder $model
     * @param RepositoryInterface         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select($this->column)->distinct();
    }
}
