<?php

namespace Modules\Targeting\Repositories\Criteria\Location;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddCampaignLocationCriteria implements CriteriaInterface
{
    /**
     * @param \Prettus\Repository\Contracts\RepositoryInterface $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select('locations.*')
            ->leftJoin('campaign_locations', 'location_id', '=', 'locations.id');
    }
}
