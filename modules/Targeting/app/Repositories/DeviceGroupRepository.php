<?php

namespace Modules\Targeting\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\DeviceGroup;
use Modules\Targeting\Repositories\Contracts\DeviceGroupRepository as DeviceGroupRepositoryInterface;
use Modules\Targeting\Repositories\Criteria\DeviceGroup\AddCampaignDeviceGroupCriteria;

class DeviceGroupRepository extends TargetingRepository implements DeviceGroupRepositoryInterface
{
    /**
     * @return string
     */
    public function model(): string
    {
        return DeviceGroup::class;
    }

    /**
     * @param Campaign $campaign
     *
     * @return self
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function withSavedNonVisible(Campaign $campaign): DeviceGroupRepository
    {
        parent::withSavedNonVisible($campaign);

        return $this->pushCriteria(new AddCampaignDeviceGroupCriteria());
    }

    /**
     * @return void
     */
    public function actualizeVisibleFlag(): void
    {
        DB::update('
            UPDATE device_groups dg
            LEFT JOIN (
              SELECT group_id, SUM(visible) as total
              FROM devices
              GROUP BY group_id
            ) visibles ON dg.id = visibles.group_id
            SET dg.`visible` = IF(visibles.total > 0, 1, 0);
        ');
    }

    /**
     * Remove device groups which doesn't have related devices.
     *
     * @return void
     */
    public function removeEmptyGroups()
    {
        $this->model
            ->whereNotExists(function ($query) {
                $query
                    ->select('id')
                    ->from('devices')
                    ->whereRaw('devices.group_id = device_groups.id')
                    ->limit(1);
            })
            ->delete();
    }
}
