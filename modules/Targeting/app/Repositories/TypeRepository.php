<?php

namespace Modules\Targeting\Repositories;

use App\Repositories\Repository;
use Modules\Targeting\Models\Type;
use Modules\Targeting\Repositories\Contracts\TypeRepository as TypeRepositoryInterface;

class TypeRepository extends Repository implements TypeRepositoryInterface
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Type::class;
    }
}
