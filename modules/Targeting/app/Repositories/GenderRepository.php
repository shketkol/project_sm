<?php

namespace Modules\Targeting\Repositories;

use App\Repositories\Repository;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Repositories\Contracts\GenderRepository as GenderRepositoryInterface;
use Modules\Targeting\Repositories\Criteria\Gender\NotEmptyCriteria;

class GenderRepository extends Repository implements GenderRepositoryInterface
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Gender::class;
    }

    /**
     * @return Gender|null
     */
    public function getAllGenderModel(): ?Gender
    {
        return $this->findByField('name', Gender::ALL)->first();
    }

    /**
     * Return only genders with age gropus.
     *
     * @return GenderRepositoryInterface
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function notEmptyCriteria(): GenderRepositoryInterface
    {
        return $this->pushCriteria(app(NotEmptyCriteria::class));
    }
}
