<?php

namespace Modules\Targeting\Repositories;

use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Repositories\Contracts\LocationRepository as LocationRepositoryInterface;
use Modules\Targeting\Repositories\Criteria\Location\AddCampaignLocationCriteria;

class LocationRepository extends TargetingRepository implements LocationRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Location::class;
    }

    /**
     * @param Campaign $campaign
     *
     * @return self
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function withSavedNonVisible(Campaign $campaign)
    {
        parent::withSavedNonVisible($campaign);

        return $this->pushCriteria(new AddCampaignLocationCriteria());
    }
}
