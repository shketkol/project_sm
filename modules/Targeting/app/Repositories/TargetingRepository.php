<?php

namespace Modules\Targeting\Repositories;

use App\Repositories\Repository;
use Laravel\Scout\Builder;
use Laravel\Scout\Searchable;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Repositories\Contracts\TargetingRepository as TargetingRepositoryInterface;
use Modules\Targeting\Repositories\Criteria\DistinctCriteria;
use Modules\Targeting\Repositories\Criteria\OnlyVisibleCriteria;
use Modules\Targeting\Repositories\Criteria\WithNonVisibleCriteria;

/**
 * @method Builder search(string $query = '', callable $callback = null)
 * @see Searchable::search()
 */
abstract class TargetingRepository extends Repository implements TargetingRepositoryInterface
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot(): void
    {
        // by default we show only visible targetings
        $this->pushCriteria(new OnlyVisibleCriteria());
    }

    /**
     * @return TargetingRepository
     */
    public function withNonVisible()
    {
        return $this->popCriteria(new OnlyVisibleCriteria());
    }

    /**
     * @param Campaign $campaign
     *
     * @return self
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function withSavedNonVisible(Campaign $campaign)
    {
        return $this->popCriteria(new OnlyVisibleCriteria())
            ->pushCriteria(new WithNonVisibleCriteria($campaign));
    }

    /**
     * @param string $column
     *
     * @return self
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function distinct(string $column = 'id')
    {
        return $this->pushCriteria(new DistinctCriteria($column));
    }
}
