<?php

namespace Modules\Targeting\Repositories;

use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\Zipcode;
use Modules\Targeting\Repositories\Contracts\LocationRepository as LocationRepositoryInterface;
use Modules\Targeting\Repositories\Criteria\Zipcode\AddCampaignZipcodeCriteria;

class ZipcodeRepository extends TargetingRepository implements LocationRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Zipcode::class;
    }

    /**
     * @param Campaign $campaign
     *
     * @return self
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function withSavedNonVisible(Campaign $campaign)
    {
        parent::withSavedNonVisible($campaign);

        return $this->pushCriteria(new AddCampaignZipcodeCriteria());
    }
}
