<?php

namespace Modules\Targeting\Repositories;

use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Repositories\Contracts\AudienceRepository as AudienceRepositoryInterface;
use Modules\Targeting\Repositories\Criteria\Audience\AddCampaignAudienceCriteria;

class AudienceRepository extends TargetingRepository implements AudienceRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Audience::class;
    }

    /**
     * @param Campaign $campaign
     *
     * @return self
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function withSavedNonVisible(Campaign $campaign)
    {
        parent::withSavedNonVisible($campaign);

        return $this->pushCriteria(new AddCampaignAudienceCriteria());
    }
}
