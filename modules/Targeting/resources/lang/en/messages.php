<?php

return [
    'duplicates'                => 'Duplicated targetings with names: ":names" were found.',
    'targetings_no_available'   => 'The following targeting options are no longer available:',
    'targeting_removed_success' => 'No longer available targeting options have been successfully removed from the campaign',
    'empty_age_groups'          => 'Please go to <b>Targeting > Gender and Age</b> and select at least one age group.',
    'empty_age_groups_step'     => 'Select at least one age group',
    'empty_device_groups'       => 'Please go to <b>Targeting > Platforms</b> and select at least one platform.',
    'empty_device_groups_step'  => 'Select at least one platform',
    'fail_zip_codes'            => 'Please enter valid 5-digit ZIP codes.',
    'fail_zip_codes_max_count'  => 'Make sure the CSV file does not exceed 400 ZIP codes.',
    'fail_max_file_size'        => 'Make sure the CSV file size does not exceed 5 kB.',
    'fail_file_zip_format'      => 'Make sure the CSV file contains only ZIP codes separated by commas.',
    'failed_devices_loading'    => 'There was an error during platforms loading. Please try again later.',
];
