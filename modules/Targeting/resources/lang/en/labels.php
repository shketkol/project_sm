<?php

return [
    'genres'             => [
        'all'         => 'All Genres',
        'table_label' => 'Show genre',
        'placeholder' => 'Select a content genre',
    ],
    'devices'            => [
        'options' => [
            'living_room' => 'Living Room',
            'computer'    => 'Computer',
            'mobile'      => 'Mobile',
        ],
    ],
    'genders'            => [
        'children' => 'Children',
        'all'      => 'All',
        'adults'   => 'Adults',
        'males'    => 'Male',
        'females'  => 'Female',
        'teens'    => 'Teens',
    ],
    'targeting_options'  => 'Targeting Options',
    'zipcodes'           => 'ZIP Codes',
    'dma'                => 'DMA',
    'cities'             => 'Cities',
    'states'             => 'States',
    'targetingAgeGroups' => 'Age Groups',
    'included'           => ':amount Included',
    'audiences'          => [
        'all'    => 'All Audiences',
        'clear_button' => 'Clear :category',
    ],
    'locations'          => [
        'zip_code'             => 'ZIP Code',
        'city_state_dma'       => 'City / State / DMA',
        'city_state_annotated' => 'City / State / :dma',
        'upload_csv_button'    => 'Upload csv',
        'all'                  => 'All Locations',
    ],
    'exclude'            => 'Exclude',
    // Do not change format!
    'labels'             => [
        'zipcodes'           => 'ZIP Codes',
        'dma'                => 'DMA',
        'cities'             => 'Cities',
        'states'             => 'States',
        'targetingAgeGroups' => 'Age Groups',
        'audiences'          => 'Audiences',
        'deviceGroups'       => 'Platforms',
        'genres'             => 'Show Genres',
    ],
    'annotations'        => [
        'dma' => [
            'term'        => 'DMA',
            'description' => 'A DMA (<em>designated market area</em>) is a region that constitutes an American television market. A DMA usually includes a metropolitan area and its surrounding suburbs.',
        ],
    ],
];
