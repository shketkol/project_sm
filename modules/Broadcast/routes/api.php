<?php

Route::middleware(['auth:admin'])->group(function () {
    // Store
    Route::post('/', 'StoreBroadcastController')->name('store')->middleware('can:broadcast.create');

    // Affected advertisers
    Route::post('/count-affected-advertisers', 'CountAffectedAdvertisersController')
        ->name('count.affected.advertisers')
        ->middleware('can:broadcast.create');

    // Get broadcast params for modal form
    Route::get('/params', 'GetParamsController')
        ->name('params')
        ->middleware('can:broadcast.view');

    // Specified resource
    Route::group([
        'prefix' => '/{broadcast}',
        'where'  => [
            'broadcast' => '[0-9]+',
        ],
    ], function () {
        // Show
        Route::get('', 'ShowBroadcastController')->name('show')->middleware('can:broadcast.view');

        // Update
        Route::patch('/update', 'UpdateBroadcastController')
            ->name('edit')
            ->middleware('can:broadcast.update,broadcast');

        // Schedule
        Route::patch('/schedule', 'ScheduleBroadcastController')
            ->name('schedule')
            ->middleware('can:broadcast.schedule,broadcast');

        // Unschedule
        Route::patch('/unschedule', 'UnscheduleBroadcastController')
            ->name('unschedule')
            ->middleware('can:broadcast.unschedule,broadcast');

        // Clone
        Route::get('/clone', 'CloneBroadcastController')
            ->name('clone')
            ->middleware('can:broadcast.clone,broadcast');

        // Send test email
        Route::post('/test-email', 'SendTestEmailController')
            ->name('send.test.email')
            ->middleware('can:broadcast.sendTest,broadcast');

        // Send now
        Route::get('/send-now', 'SendNowBroadcastController')
            ->name('sendNow')
            ->middleware('can:broadcast.sendNow,broadcast');
    });
});
