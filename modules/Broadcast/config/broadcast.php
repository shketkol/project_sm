<?php

use Modules\Broadcast\Models\BroadcastStatus;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;

return [
    'job'                     => [
        'batch_size' => env('BROADCAST_JOB_BATCH_SIZE', 1000),
    ],
    'segment_groups'          => [
        BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS => [
            'max' => env('BROADCAST_SEGMENT_GROUPS_SELECTED_ADVERTISERS_LIMIT', 100),
        ],
    ],
    'pending_status_ids'      => [
        BroadcastStatus::ID_PROCESSING
    ],
];
