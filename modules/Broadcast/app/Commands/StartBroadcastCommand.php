<?php

namespace Modules\Broadcast\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Collection;
use Modules\Broadcast\Jobs\ProcessBroadcast;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastStatus;
use Modules\Broadcast\Repositories\BroadcastRepository;
use Modules\Broadcast\Repositories\Criteria\AddStatusCriteria;
use Modules\Broadcast\Repositories\Criteria\ScheduledBeforeCriteria;
use Psr\Log\LoggerInterface;

class StartBroadcastCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'broadcast:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start broadcast.';

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var BroadcastRepository
     */
    private $repository;

    /**
     * @param DatabaseManager     $databaseManager
     * @param Dispatcher          $dispatcher
     * @param LoggerInterface     $log
     * @param BroadcastRepository $repository
     */
    public function __construct(
        DatabaseManager $databaseManager,
        Dispatcher $dispatcher,
        LoggerInterface $log,
        BroadcastRepository $repository
    ) {
        parent::__construct();
        $this->databaseManager = $databaseManager;
        $this->dispatcher = $dispatcher;
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->log->info('[Broadcast command] Started dispatching broadcasts.');
        $this->databaseManager->beginTransaction();

        try {
            $broadcasts = $this->getBroadcasts();
            $this->dispatchBroadcasts($broadcasts);
        } catch (\Throwable $exception) {
            $this->log->warning('[Broadcast command] Failed dispatching broadcasts.', [
                'reason' => $exception->getMessage(),
            ]);

            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();
        $this->log->info('[Broadcast command] Finished dispatching broadcasts.');
    }

    /**
     * @return Collection|Broadcast[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function getBroadcasts(): Collection
    {
        $broadcasts = $this->repository
            ->pushCriteria(new ScheduledBeforeCriteria(Carbon::now()))
            ->pushCriteria(new AddStatusCriteria([BroadcastStatus::ID_SCHEDULED]))
            ->all();

        if ($broadcasts->isEmpty()) {
            $this->log->info('[Broadcast command] No scheduled broadcasts found.');
            return $broadcasts;
        }

        $this->log->info('[Broadcast command] Found broadcasts.', [
            'count' => $broadcasts->count(),
        ]);

        return $broadcasts;
    }

    /**
     * @param Collection|Broadcast[] $broadcasts
     *
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function dispatchBroadcasts(Collection $broadcasts): void
    {
        $broadcasts->each(function (Broadcast $broadcast): void {
            $broadcast->applyInternalStatus(BroadcastStatus::PROCESSING);

            $this->dispatcher->dispatch(new ProcessBroadcast($broadcast));

            $this->log->info('[Broadcast command] Broadcast dispatched.', [
                'broadcast_id' => $broadcast->id,
            ]);
        });
    }
}
