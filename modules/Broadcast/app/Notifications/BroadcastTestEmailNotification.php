<?php

namespace Modules\Broadcast\Notifications;

use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Support\Arr;
use Modules\User\Models\User;

class BroadcastTestEmailNotification extends BroadcastNotification
{
    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(User $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param User $notifiable
     *
     * @return Mailable
     * @throws \App\Exceptions\BaseException
     */
    public function toMail(User $notifiable): Mailable
    {
        return new $this->mailClass($this->getPayload($notifiable));
    }

    /**
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        $payload = [];

        if ($this->broadcast->hasEmail()) {
            Arr::set($payload, 'email', [
                'subject' => $this->broadcast->email->subject,
                'header'  => $this->broadcast->email->header,
                'icon'    => $this->broadcast->email->icon,
                'body'    => $this->template()->handle($this->broadcast->email->getMailHtmlMarkDown(), $notifiable),
            ]);
        }

        return $payload;
    }
}
