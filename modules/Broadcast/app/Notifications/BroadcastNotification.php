<?php

namespace Modules\Broadcast\Notifications;

use App\Models\QueuePriority;
use App\Notifications\Notification;
use Illuminate\Support\Arr;
use Modules\Broadcast\Actions\CheckUserReceivedBroadcastAction;
use Modules\Broadcast\Actions\Template\ReplaceTemplateVariablesAction;
use Modules\Broadcast\Mail\BroadcastEmail;
use Modules\Broadcast\Models\Broadcast;
use Modules\User\Models\User;

class BroadcastNotification extends Notification
{
    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = BroadcastEmail::class;

    /**
     * @var Broadcast
     */
    protected $broadcast;

    /**
     * @param Broadcast $broadcast
     */
    public function __construct(Broadcast $broadcast)
    {
        $this->broadcast = $broadcast;
        $this->onQueue(QueuePriority::low());
    }

    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via(User $notifiable): array
    {
        $via = [];

        if ($this->broadcast->hasEmail() && !$this->isReceivedEmail($notifiable)) {
            $via[] = 'mail';
        }

        if ($this->broadcast->hasNotification() && !$this->isReceivedNotification($notifiable)) {
            $via[] = 'database';
        }

        return $via;
    }

    /**
     * @param User $notifiable
     *
     * @return bool
     */
    private function isReceivedEmail(User $notifiable): bool
    {
        return $this->checkUserReceivedBroadcast()->handle($notifiable, $this->broadcast, ['check_only_email' => true]);
    }

    /**
     * @param User $notifiable
     *
     * @return bool
     */
    private function isReceivedNotification(User $notifiable): bool
    {
        return $this->checkUserReceivedBroadcast()->handle($notifiable, $this->broadcast, [
            'check_only_notification' => true,
        ]);
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->broadcast->notification->notification_category_id;
    }

    /**
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        $payload = [];

        if ($this->broadcast->hasEmail()) {
            Arr::set($payload, 'email', [
                'subject' => $this->broadcast->email->subject,
                'header'  => $this->broadcast->email->header,
                'icon'    => $this->broadcast->email->icon,
                'body'    => $this->template()->handle($this->broadcast->email->getMailHtmlMarkDown(), $notifiable),
            ]);

            Arr::set($payload, 'mailable_type', get_class($this));
            Arr::set($payload, 'mailable_id', $this->broadcast->id);
            Arr::set($payload, 'notifiable_type', get_class($notifiable));
            Arr::set($payload, 'notifiable_id', $notifiable->id);
        }

        if ($this->broadcast->hasNotification()) {
            Arr::set($payload, 'notification', [
                'notification_category_id' => $this->broadcast->notification->notification_category_id,
                'body'                     => $this->template()->handle(
                    $this->broadcast->notification->body,
                    $notifiable
                ),
            ]);
        }

        return $payload;
    }

    /**
     * @return ReplaceTemplateVariablesAction
     */
    protected function template(): ReplaceTemplateVariablesAction
    {
        return app(ReplaceTemplateVariablesAction::class);
    }

    /**
     * @return CheckUserReceivedBroadcastAction
     */
    private function checkUserReceivedBroadcast(): CheckUserReceivedBroadcastAction
    {
        return app(CheckUserReceivedBroadcastAction::class);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        return Arr::get($data, 'notification.body');
    }

    /**
     * @return Broadcast
     */
    public function getBroadcast(): Broadcast
    {
        return $this->broadcast;
    }
}
