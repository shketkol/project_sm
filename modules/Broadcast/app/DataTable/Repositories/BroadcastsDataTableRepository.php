<?php

namespace Modules\Broadcast\DataTable\Repositories;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\DataTable\Repositories\Contracts\BroadcastsDataTableRepository
    as BroadcastsDataTableRepositoryInterface;

class BroadcastsDataTableRepository extends Repository implements BroadcastsDataTableRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Broadcast::class;
    }

    /**
     * Get broadcasts list
     *
     * @return Builder
     */
    public function broadcasts(): Builder
    {
        $this->applyCriteria();

        return $this->model->addSelect('broadcasts.*');
    }
}
