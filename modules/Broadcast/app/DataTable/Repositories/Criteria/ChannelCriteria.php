<?php

namespace Modules\Broadcast\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Modules\Broadcast\Models\BroadcastChannel;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ChannelCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->addSelect(
            DB::raw(
                "IF(broadcasts.email_id IS NOT NULL and broadcasts.notification_id IS NULL, '" .
                BroadcastChannel::EMAIL ."', IF(broadcasts.email_id IS NULL and broadcasts.notification_id" .
                " IS NOT NULL, '" . BroadcastChannel::IN_APP_NOTIFICATION . "', '" . BroadcastChannel::ALL ."'))" .
                " AS channel"
            )
        );
    }
}
