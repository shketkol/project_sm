<?php

namespace Modules\Broadcast\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class BroadcasterCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->addSelect([
            'broadcaster.account_name AS broadcaster_name',
        ])->leftJoin(
            'users as broadcaster',
            'broadcasts.user_id',
            '=',
            'broadcaster.id'
        );
    }
}
