<?php

namespace Modules\Broadcast\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SegmentCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->addSelect(
            'broadcast_segments.group_id AS segment_id'
        )->leftJoin(
            'broadcast_segments',
            'broadcast_segments.broadcast_id',
            '=',
            'broadcasts.id'
        );
    }
}
