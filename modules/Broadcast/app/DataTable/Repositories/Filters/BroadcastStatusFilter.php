<?php

namespace Modules\Broadcast\DataTable\Repositories\Filters;

use App\DataTable\Filters\DataTableFilter;
use Illuminate\Database\Eloquent\Builder;
use Modules\Broadcast\Models\BroadcastStatus;
use Yajra\DataTables\DataTableAbstract;

class BroadcastStatusFilter extends DataTableFilter
{
    /**
     * Filter name
     *
     * @var string
     */
    public static $name = 'broadcast-status';

    /**
     * @param DataTableAbstract $dataTable
     * @return void
     */
    public function filter(DataTableAbstract $dataTable): void
    {
        $this->makeFilter($dataTable, 'broadcast_status', function (Builder $query, $values) {
            $query->orWhereIn('broadcast_statuses.id', $values);
        });
    }

    /**
     * Get filter options
     *
     * @return array
     */
    public function options(): array
    {
        return BroadcastStatus::all()
            ->pluck('name', 'id')
            ->mapWithKeys(function (string $value, int $key) {
                return [$key => __("broadcast::labels.statuses.{$value}")];
            })
            ->toArray();
    }
}
