<?php

namespace Modules\Broadcast\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Notification\Models\Traits\BelongsToNotificationCategory;

/**
 * @property integer        $id
 * @property integer        $notification_category_id
 * @property string         $body
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class BroadcastNotification extends Model
{
    use BelongsToNotificationCategory;

    /**
     * @var array
     */
    protected $fillable = [
        'notification_category_id',
        'body',
    ];
}
