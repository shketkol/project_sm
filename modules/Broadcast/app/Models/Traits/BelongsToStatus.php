<?php

namespace Modules\Broadcast\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Broadcast\Models\BroadcastStatus;

/**
 * @property BroadcastStatus $status
 */
trait BelongsToStatus
{
    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(BroadcastStatus::class, 'status_id');
    }
}
