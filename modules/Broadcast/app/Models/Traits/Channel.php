<?php

namespace Modules\Broadcast\Models\Traits;

use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastChannel;

trait Channel
{
    /**
     * @return string
     */
    public function channel(): string
    {
        return BroadcastChannel::getChannel($this);
    }
}
