<?php

namespace Modules\Broadcast\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;
use Modules\Broadcast\Repositories\Criteria\PaginatorCriteria;
use Modules\User\DataTable\Repositories\Criteria\AddRolesCriteria;
use Modules\User\Models\Role;
use Modules\User\Models\User;
use Modules\User\Repositories\Criteria\RolesCriteria;
use Modules\User\Repositories\UserRepository;

trait BelongsToManyRecipients
{
    /**
     * @param int $limit
     * @param int $offset
     *
     * @return Collection
     */
    public function recipients(int $limit = 0, int $offset = 0): Collection
    {
        $map = [
            'default'                                      => function (): Collection {
                return collect([]);
            },
            BroadcastSegmentGroup::ID_ALL_ADVERTISERS      => function (int $limit, int $offset): Collection {
                return $this->getAllAdvertisers($limit, $offset);
            },
            BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS => function (int $limit, int $offset): Collection {
                return $this->getSelectedAdvertisers($limit, $offset);
            },
            BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS => function (int $limit, int $offset): Collection {
                return $this->selectedRecipients()->limit($limit)->offset($offset)->get();
            },
        ];

        $callable = Arr::get($map, $this->id, Arr::get($map, 'default'));

        return $callable($limit, $offset);
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return Collection
     */
    private function getSelectedAdvertisers(int $limit = 0, int $offset = 0): Collection
    {
        $configLimit = config(sprintf(
            'broadcast.segment_groups.%d.max',
            BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS
        ));

        if ($limit <= 0 || $limit > $configLimit) {
            $limit = $configLimit;
        }

        return $this->selectedRecipients()->limit($limit)->offset($offset)->get();
    }

    /**
     * @return BelongsToMany
     */
    public function selectedRecipients(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'broadcast_segment_users',
            'group_id',
            'user_id'
        )
            ->withPivot('broadcast_id')
            ->wherePivot('broadcast_id', '=', $this->getOriginal('pivot_broadcast_id'));
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return Collection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getAllAdvertisers(int $limit, int $offset): Collection
    {
        /** @var UserRepository $repository */
        $repository = app(UserRepository::class);

        return $repository
            ->pushCriteria(new AddRolesCriteria())
            ->pushCriteria(new RolesCriteria(Role::ID_ADVERTISER))
            ->pushCriteria(new PaginatorCriteria($limit, $offset))
            ->get();
    }
}
