<?php

namespace Modules\Broadcast\Models\Traits;

use Modules\Broadcast\Models\BroadcastStatus;

trait Editable
{
    /**
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->inState(BroadcastStatus::ID_DRAFT);
    }
}
