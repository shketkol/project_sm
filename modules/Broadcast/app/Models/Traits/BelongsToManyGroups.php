<?php

namespace Modules\Broadcast\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;

/**
 * @property BroadcastSegmentGroup[]|Collection $segmentGroups
 */
trait BelongsToManyGroups
{
    /**
     * @return BelongsToMany
     */
    public function segmentGroups(): BelongsToMany
    {
        return $this->belongsToMany(
            BroadcastSegmentGroup::class,
            'broadcast_segments',
            'broadcast_id',
            'group_id',
        );
    }
}
