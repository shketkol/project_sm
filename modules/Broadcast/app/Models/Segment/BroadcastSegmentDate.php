<?php

namespace Modules\Broadcast\Models\Segment;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer        $id
 * @property string         $name
 * @property \Carbon\Carbon $date_start
 * @property \Carbon\Carbon $date_end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class BroadcastSegmentDate extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'group_id',
        'broadcast_id',
        'date_start',
        'date_end',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'date_start' => 'datetime',
        'date_end'   => 'datetime',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
