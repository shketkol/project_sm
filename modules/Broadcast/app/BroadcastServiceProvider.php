<?php

namespace Modules\Broadcast;

use App\Providers\ModuleServiceProvider;
use Modules\Broadcast\Commands\StartBroadcastCommand;
use Modules\Broadcast\Policies\BroadcastPolicy;

class BroadcastServiceProvider extends ModuleServiceProvider
{
    /**
     * @var bool
     */
    protected $pluralRoutePrefix = false;

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'broadcast';
    }

    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'broadcast' => BroadcastPolicy::class,
    ];

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        StartBroadcastCommand::class,
    ];

    /**
     * Boot provider.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadRoutes();
        $this->loadConfigs(['broadcast', 'state-machine']);
        $this->commands($this->commands);
    }
}
