<?php

namespace Modules\Broadcast\Exceptions;

use App\Exceptions\ModelNotUpdatedException;

class BroadcastNotScheduledException extends ModelNotUpdatedException
{
    //
}
