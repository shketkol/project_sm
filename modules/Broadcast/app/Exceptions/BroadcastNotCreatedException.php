<?php

namespace Modules\Broadcast\Exceptions;

use App\Exceptions\ModelNotCreatedException;

class BroadcastNotCreatedException extends ModelNotCreatedException
{
    //
}
