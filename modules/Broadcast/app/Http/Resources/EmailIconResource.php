<?php

namespace Modules\Broadcast\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Notification\Models\EmailIcon;

/**
 * @mixin EmailIcon
 * @property EmailIcon $resource
 */
class EmailIconResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'value' => $this->resource->name,
            'label' => __("notification::labels.email_icons.{$this->resource->name}"),
            'image' => url("/images/email/icons/png/{$this->resource->name}.png")
        ];
    }
}
