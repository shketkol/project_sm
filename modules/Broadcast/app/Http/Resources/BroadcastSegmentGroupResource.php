<?php

namespace Modules\Broadcast\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Modules\Advertiser\Http\Resources\AdvertiserBriefResource;
use Modules\Broadcast\Actions\Affected\CountAffectedAdvertisersAction;

/**
 * @mixin \Modules\Broadcast\Models\Segment\BroadcastSegmentGroup
 */
class BroadcastSegmentGroupResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function toArray($request): array
    {
        $data = [
            'id'   => $this->id,
            'name' => $this->name,
        ];

        if ($this->isSelectedAdvertisers()) {
            $data = $this->addSelectedAdvertisers($data);
        }

        if ($this->isAffectedAdvertisers()) {
            $data = $this->addAffectedAdvertisers($data);
        }

        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function addSelectedAdvertisers(array $data): array
    {
        Arr::set($data, 'recipients', AdvertiserBriefResource::collection($this->recipients()));

        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function addAffectedAdvertisers(array $data): array
    {
        Arr::set($data, 'dates', BroadcastSegmentDateResource::make($this->date));
        Arr::set($data, 'affected_count', $this->date ?
            $this->countAffected($this->date->date_start, $this->date->date_end) :
            0);

        return $data;
    }

    /**
     * @param string $start
     * @param string $end
     *
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function countAffected(string $start, string $end): int
    {
        /** @var CountAffectedAdvertisersAction $action */
        $action = app(CountAffectedAdvertisersAction::class);

        return $action->handle($start, $end);
    }
}
