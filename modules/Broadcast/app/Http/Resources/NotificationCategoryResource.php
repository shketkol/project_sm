<?php

namespace Modules\Broadcast\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Notification\Models\NotificationCategory;

/**
 * @mixin NotificationCategory
 * @property NotificationCategory $resource
 */
class NotificationCategoryResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'value' => $this->resource->id,
            'label' => __("notification::labels.notification_categories.{$this->resource->name}"),
        ];
    }
}
