<?php

namespace Modules\Broadcast\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \Modules\Broadcast\Models\BroadcastEmail
 */
class BroadcastEmailResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'      => $this->id,
            'subject' => $this->subject,
            'header'  => $this->header,
            'icon'    => $this->icon,
            'body'    => $this->body,
        ];
    }
}
