<?php

namespace Modules\Broadcast\Http\Resources;

use App\Helpers\DateFormatHelper;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \Modules\Broadcast\Models\Segment\BroadcastSegmentDate
 */
class BroadcastSegmentDateResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'date_start' => DateFormatHelper::toTimezone($this->date_start, config('date.default_timezone_full_code')),
            'date_end'   => DateFormatHelper::toTimezone($this->date_end, config('date.default_timezone_full_code')),
        ];
    }
}
