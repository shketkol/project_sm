<?php

namespace Modules\Broadcast\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \Modules\Broadcast\Models\BroadcastNotification
 */
class BroadcastNotificationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'       => $this->id,
            'body'     => $this->body,
            'category' => [
                'id'   => $this->notification_category_id,
                'name' => optional($this->category)->name,
            ],
        ];
    }
}
