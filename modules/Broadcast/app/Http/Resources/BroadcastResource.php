<?php

namespace Modules\Broadcast\Http\Resources;

use App\Helpers\DateFormatHelper;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \Modules\Broadcast\Models\Broadcast
 */
class BroadcastResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'user_id'       => $this->user->id,
            'status'        => [
                'id'   => $this->status_id,
                'name' => $this->status->name,
            ],
            'schedule_date' => $this->schedule_date ?
                DateFormatHelper::toTimezone($this->schedule_date, config('date.default_timezone_full_code')) :
                null,
            'created_at'    => $this->created_at,
            'channel'       => $this->channel(),
            'email'         => BroadcastEmailResource::make($this->email),
            'notification'  => BroadcastNotificationResource::make($this->notification),
            'groups'        => BroadcastSegmentGroupResource::make($this->segmentGroups()->first()),
            'states'        => $this->getStates(),
        ];
    }

    /**
     * Returns list of all states.
     *
     * @return array
     */
    protected function getStates(): array
    {
        /** @var \Modules\User\Models\User $user */
        $user = app(Guard::class)->user();

        return [
            'can_save'            => $user->can('broadcast.create', $this->getModel()),
            'can_update'          => $user->can('broadcast.update', $this->getModel()),
            'can_schedule'        => $user->can('broadcast.schedule', $this->getModel()),
            'can_unschedule'      => $user->can('broadcast.unschedule', $this->getModel()),
            'can_send_test_email' => $user->can('broadcast.sendTest', $this->getModel()),
            'can_send_now'        => $user->can('broadcast.sendNow', $this->getModel()),
        ];
    }
}
