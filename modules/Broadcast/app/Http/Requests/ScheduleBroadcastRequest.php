<?php

namespace Modules\Broadcast\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Modules\Broadcast\Http\Requests\Traits\BroadcastValidationRules;

class ScheduleBroadcastRequest extends Request
{
    use BroadcastValidationRules;

    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        $rules = [
            'schedule_date'                         => $validationRules->only(
                'broadcast.schedule_date',
                ['required', 'date', 'valid_broadcast_date']
            ),
            'channel'                               => $validationRules->only(
                'broadcast.channel',
                ['required', 'string', 'in']
            ),

            // email
            'email'                                 => $validationRules->only(
                'broadcast.email.payload',
                ['required_if', 'array']
            ),
            'email.subject'                         => $validationRules->only(
                'broadcast.email.subject',
                ['required_if', 'string', 'max', 'regex']
            ),
            'email.header'                          => $validationRules->only(
                'broadcast.email.header',
                ['required_if', 'string', 'max', 'regex']
            ),
            'email.body'                            => $validationRules->only(
                'broadcast.email.body',
                ['required_if', 'string', 'max']
            ),

            // notification
            'notification'                          => $validationRules->only(
                'broadcast.notification.payload',
                ['required_if', 'array']
            ),
            'notification.notification_category_id' => $validationRules->only(
                'broadcast.notification.category.id',
                ['required_if', 'integer', 'min', 'max', 'exists']
            ),
            'notification.body'                     => $validationRules->only(
                'broadcast.notification.body',
                ['required_if', 'string', 'max']
            ),
        ];

        $rules = array_merge($rules, $this->getRequiredRules($validationRules));
        $rules = array_merge($rules, $this->getRequiredSegmentGroupRules($validationRules));
        $rules = array_merge($rules, $this->getOptionalRules($validationRules));

        return $rules;
    }

   /**
    * Get custom attributes for validator errors.
    *
    * @return array
    */
    public function attributes()
    {
        return [
            'schedule_date' => __('broadcast::labels.scheduled_date'),
        ];
    }
}
