<?php

namespace Modules\Broadcast\Http\Requests\Traits;

use App\Services\ValidationRulesService\Contracts\ValidationRules;

trait BroadcastValidationRules
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    private function getRequiredRules(ValidationRules $validationRules): array
    {
        return [
            'name' => $validationRules->only(
                'broadcast.name',
                ['required', 'string', 'max', 'regex']
            ),
        ];
    }

    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    private function getRequiredSegmentGroupRules(ValidationRules $validationRules): array
    {
        return [
            // segment group - selected advertisers
            'segment_group.advertisers'      => $validationRules->only(
                'broadcast.segment_group.advertisers',
                ['required_if', 'array', 'is_advertisers', 'max']
            ),

            // segment group - affected advertisers
            'segment_group.dates'            => $validationRules->only(
                'broadcast.affected_advertisers.dates',
                ['required_if', 'array', 'has_affected_advertisers']
            ),
            'segment_group.dates.date_start' => $validationRules->only(
                'broadcast.affected_advertisers.date_start',
                ['required_if', 'nullable', 'date', 'before', 'after_or_equal']
            ),
            'segment_group.dates.date_end'   => $validationRules->only(
                'broadcast.affected_advertisers.date_end',
                ['required_if', 'nullable', 'date', 'before', 'after_or_equal']
            ),
        ];
    }

    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    private function getOptionalRules(ValidationRules $validationRules): array
    {
        return [
            // segment group
            'segment_group'    => $validationRules->only(
                'broadcast.segment_group.payload',
                ['array']
            ),
            'segment_group.id' => $validationRules->only(
                'broadcast.segment_group.id',
                ['integer', 'min', 'max', 'exists']
            ),

            // email
            'email.icon'       => $validationRules->only(
                'broadcast.email.icon',
                ['string', 'exists', 'nullable']
            ),
        ];
    }
}
