<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Modules\Broadcast\Actions\Affected\CountAffectedAdvertisersAction;
use Modules\Broadcast\Http\Controllers\Api\Traits\ThrowAffectedCountException;
use Modules\Broadcast\Http\Requests\CountAffectedAdvertisersRequest;

class CountAffectedAdvertisersController extends Controller
{
    use ThrowAffectedCountException;

    /**
     * @param CountAffectedAdvertisersRequest $request
     * @param CountAffectedAdvertisersAction  $action
     *
     * @return JsonResponse
     * @throws ValidationException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        CountAffectedAdvertisersRequest $request,
        CountAffectedAdvertisersAction $action
    ): JsonResponse {
        $count = $action->handle($request->get('date_start'), $request->get('date_end'));

        if ($this->isNotEnough($count)) {
            throw $this->createException($count);
        }

        return response()->json(['data' => [
            'count' => $count,
        ]]);
    }
}
