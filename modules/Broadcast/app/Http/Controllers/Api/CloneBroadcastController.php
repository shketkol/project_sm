<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Response;
use Modules\Broadcast\Actions\CloneBroadcastAction;
use Modules\Broadcast\Http\Resources\BroadcastResource;
use Modules\Broadcast\Models\Broadcast;

class CloneBroadcastController
{
    /**
     * @param CloneBroadcastAction $action
     * @param Broadcast $broadcast
     * @param Authenticatable $admin
     *
     * @return \Illuminate\Http\JsonResponse|Response
     * @throws \Throwable
     */
    public function __invoke(
        CloneBroadcastAction $action,
        Broadcast $broadcast,
        Authenticatable $admin
    ) {
        try {
            $clonedBroadcast = $action->handle($admin, $broadcast);
        } catch (\Throwable $throwable) {
            return response(
                ['message' => trans('broadcast::messages.fail_clone')],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        };

        return (new BroadcastResource($clonedBroadcast))->response();
    }
}
