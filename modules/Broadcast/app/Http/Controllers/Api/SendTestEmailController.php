<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Modules\Broadcast\Actions\SendTestEmailAction;
use Modules\Broadcast\Http\Requests\SendTestEmailRequest;
use Modules\Broadcast\Models\Broadcast;

class SendTestEmailController
{
    /**
     * @param SendTestEmailAction $action
     * @param Broadcast $broadcast
     * @param SendTestEmailRequest $request
     *
     * @return Response
     * @throws ValidationException
     */
    public function __invoke(
        SendTestEmailAction $action,
        Broadcast $broadcast,
        SendTestEmailRequest $request
    ) {
        $action->handle($request->get('test_email'), $broadcast);

        return response(
            ['message' => trans('broadcast::messages.success_sent_test_email')],
        );
    }
}
