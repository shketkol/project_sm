<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Modules\Broadcast\Actions\ScheduleBroadcastAction;
use Modules\Broadcast\Actions\Update\UpdateBroadcastAction;
use Modules\Broadcast\Http\Requests\ScheduleBroadcastRequest;
use Modules\Broadcast\Http\Resources\BroadcastResource;
use Modules\Broadcast\Models\Broadcast;

class ScheduleBroadcastController
{
    /**
     * @param Broadcast                $broadcast
     * @param ScheduleBroadcastAction  $scheduleAction
     * @param UpdateBroadcastAction    $updateAction
     * @param Authenticatable          $admin
     * @param ScheduleBroadcastRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function __invoke(
        Broadcast $broadcast,
        ScheduleBroadcastAction $scheduleAction,
        UpdateBroadcastAction $updateAction,
        Authenticatable $admin,
        ScheduleBroadcastRequest $request
    ): JsonResponse {
        $updateAction->handle($admin, $broadcast, $request->all());
        $broadcast->refresh();
        $broadcast = $scheduleAction->handle($admin, $broadcast);
        return (new BroadcastResource($broadcast))->response();
    }
}
