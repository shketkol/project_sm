<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Modules\Broadcast\Actions\SendNowBroadcastAction;
use Modules\Broadcast\Http\Resources\BroadcastResource;
use Modules\Broadcast\Models\Broadcast;

class SendNowBroadcastController
{
    /**
     * @param Broadcast $broadcast
     *
     * @param SendNowBroadcastAction $action
     * @return JsonResponse
     * @throws \Throwable
     */
    public function __invoke(Broadcast $broadcast, SendNowBroadcastAction $action): JsonResponse
    {
        $broadcast = $action->handle($broadcast);

        return (new BroadcastResource($broadcast))
            ->additional(['data' => [
                'message' => __('broadcast::messages.send_now_success'),
            ]])
            ->response();
    }
}
