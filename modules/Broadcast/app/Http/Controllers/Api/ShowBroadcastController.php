<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Modules\Broadcast\Http\Resources\BroadcastResource;
use Modules\Broadcast\Models\Broadcast;

class ShowBroadcastController
{
    /**
     * @param Broadcast $broadcast
     *
     * @return JsonResponse
     */
    public function __invoke(Broadcast $broadcast): JsonResponse
    {
        return (new BroadcastResource($broadcast))->response();
    }
}
