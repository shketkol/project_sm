<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Modules\Broadcast\Actions\UnscheduleBroadcastAction;
use Modules\Broadcast\Http\Resources\BroadcastResource;
use Modules\Broadcast\Models\Broadcast;

class UnscheduleBroadcastController
{
    /**
     * @param Broadcast                 $broadcast
     * @param UnscheduleBroadcastAction $action
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function __invoke(Broadcast $broadcast, UnscheduleBroadcastAction $action): JsonResponse
    {
        $broadcast = $action->handle($broadcast);
        return (new BroadcastResource($broadcast))->response();
    }
}
