<?php

namespace Modules\Broadcast\Actions;

use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastStatus;
use Psr\Log\LoggerInterface;

class UnscheduleBroadcastAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * UnscheduleBroadcastAction constructor.
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * @param Broadcast $broadcast
     *
     * @return Broadcast
     */
    public function handle(Broadcast $broadcast): Broadcast
    {
        $this->log->info('Started unschedule broadcast action', ['broadcast_id' => $broadcast->getKey()]);

        $broadcast->applyInternalStatus(BroadcastStatus::DRAFT);

        return $broadcast;
    }
}
