<?php

namespace Modules\Broadcast\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\DatabaseManager;
use Modules\Broadcast\Exceptions\BroadcastNotScheduledException;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastStatus;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class ScheduleBroadcastAction
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var ValidateBroadcastAction
     */
    private $validateBroadcastAction;

    /**
     * @param DatabaseManager         $databaseManager
     * @param LoggerInterface         $log
     * @param Dispatcher              $dispatcher
     * @param ValidateBroadcastAction $validateBroadcastAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        Dispatcher $dispatcher,
        ValidateBroadcastAction $validateBroadcastAction
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->dispatcher = $dispatcher;
        $this->validateBroadcastAction = $validateBroadcastAction;
    }

    /**
     * @param User|Authenticatable $admin
     * @param Broadcast            $broadcast
     *
     * @return Broadcast
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function handle(User $admin, Broadcast $broadcast): Broadcast
    {
        $this->log->info('Started schedule broadcast.', ['broadcast_id' => $broadcast->id]);

        $this->validateBroadcastAction->handle($broadcast);

        $this->databaseManager->beginTransaction();

        try {
            $broadcast = $this->schedule($admin, $broadcast);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            $this->log->warning('Broadcast not scheduled.', ['reason' => $exception->getMessage()]);

            throw BroadcastNotScheduledException::createFrom($exception);
        }

        $this->databaseManager->commit();
        $this->log->info('Finished broadcast schedule.', ['broadcast_id' => $broadcast->id]);

        return $broadcast;
    }

    /**
     * @param User      $admin
     * @param Broadcast $broadcast
     *
     * @return Broadcast
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    private function schedule(User $admin, Broadcast $broadcast): Broadcast
    {
        if (!$admin->is($broadcast->user)) {
            $this->log->info('Broadcast owner has changed.', [
                'broadcast_id' => $broadcast->id,
                'old_user_id'  => $broadcast->user->id,
                'new_user_id'  => $admin->id,
            ]);
        }

        $broadcast->applyInternalStatus(BroadcastStatus::SCHEDULED);

        return $broadcast->refresh();
    }
}
