<?php

namespace Modules\Broadcast\Actions\Store;

use Illuminate\Support\Arr;
use Modules\Broadcast\Actions\Update\UpdateBroadcastSegmentDateAction;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\Segment\BroadcastSegmentDate;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;
use Modules\Broadcast\Repositories\BroadcastSegmentDateRepository;
use Psr\Log\LoggerInterface;

class StoreBroadcastSegmentDateAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var BroadcastSegmentDateRepository
     */
    private $segmentDateRepository;

    /**
     * @var UpdateBroadcastSegmentDateAction
     */
    private $updateDateAction;

    /**
     * @param LoggerInterface                  $log
     * @param BroadcastSegmentDateRepository   $segmentDateRepository
     * @param UpdateBroadcastSegmentDateAction $updateDateAction
     */
    public function __construct(
        LoggerInterface $log,
        BroadcastSegmentDateRepository $segmentDateRepository,
        UpdateBroadcastSegmentDateAction $updateDateAction
    ) {
        $this->log = $log;
        $this->segmentDateRepository = $segmentDateRepository;
        $this->updateDateAction = $updateDateAction;
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(Broadcast $broadcast, array $data): void
    {
        $start = Arr::get($data, 'dates.date_start');
        $end = Arr::get($data, 'dates.date_end');

        if (!$start || !$end) {
            $this->log->info('[Broadcast Segment Group Date] No data found. Skipped save.', [
                'broadcast_id' => $broadcast->id,
                'data'         => $data,
            ]);

            return;
        }

        $this->log->info('[Broadcast Segment Group Date] Save started.', [
            'broadcast_id' => $broadcast->id,
            'start'        => $start,
            'end'          => $end,
        ]);

        /** @var BroadcastSegmentGroup $segmentGroup */
        $segmentGroup = $broadcast->segmentGroups()->first();

        $attributes = [
            'date_start'   => $start,
            'date_end'     => $end,
            'broadcast_id' => $broadcast->id,
            'group_id'     => $segmentGroup->id,
        ];

        if ($segmentGroup->date()->exists()) {
            $this->update($segmentGroup, $attributes);
            return;
        }

        /** @var BroadcastSegmentDate $date */
        $date = $this->segmentDateRepository->create($attributes);

        $this->log->info('[Broadcast Segment Group Date] Save finished.', [
            'date_id' => $date->id,
            'data'    => $attributes,
            'start'   => $start,
            'end'     => $end,
        ]);
    }

    /**
     * @param BroadcastSegmentGroup $segmentGroup
     * @param array                 $data
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function update(BroadcastSegmentGroup $segmentGroup, array $data): void
    {
        $this->log->info('[Broadcast Segment Group Date] Model already exists.', [
            'group_id' => $segmentGroup->id,
            'data'     => $data,
        ]);

        $this->updateDateAction->handle($segmentGroup, $data);
    }
}
