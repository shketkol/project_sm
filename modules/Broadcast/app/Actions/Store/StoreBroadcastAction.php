<?php

namespace Modules\Broadcast\Actions\Store;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Broadcast\Actions\Traits\StoreBroadcast;
use Modules\Broadcast\Exceptions\BroadcastNotCreatedException;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Repositories\BroadcastRepository;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class StoreBroadcastAction
{
    use StoreBroadcast;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var BroadcastRepository
     */
    private $repository;

    /**
     * @param DatabaseManager     $databaseManager
     * @param LoggerInterface     $log
     * @param BroadcastRepository $repository
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        BroadcastRepository $repository
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param User|Authenticatable $admin
     * @param array                $data
     *
     * @return Broadcast
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function handle(User $admin, array $data): Broadcast
    {
        $this->log->info('Storing new broadcast with relations.', ['data' => $data]);
        $this->databaseManager->beginTransaction();

        try {
            $broadcast = $this->store($admin, $data);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            $this->log->warning('Broadcast not created.', ['reason' => $exception->getMessage()]);

            throw BroadcastNotCreatedException::createFrom($exception);
        }

        $this->databaseManager->commit();
        $this->log->info('Broadcast with relations created.', ['broadcast_id' => $broadcast->id]);

        return $broadcast;
    }
}
