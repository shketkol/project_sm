<?php

namespace Modules\Broadcast\Actions\Store;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Broadcast\Actions\Affected\GetAffectedAdvertisersAction;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;
use Psr\Log\LoggerInterface;

class StoreBroadcastSegmentGroupAction
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var GetAffectedAdvertisersAction
     */
    private $getAffectedAdvertisersAction;

    /**
     * @var StoreBroadcastSegmentDateAction
     */
    private $storeDateAction;

    /**
     * @param DatabaseManager                 $databaseManager
     * @param LoggerInterface                 $log
     * @param GetAffectedAdvertisersAction    $getAffectedAdvertisersAction
     * @param StoreBroadcastSegmentDateAction $storeDateAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        GetAffectedAdvertisersAction $getAffectedAdvertisersAction,
        StoreBroadcastSegmentDateAction $storeDateAction
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->getAffectedAdvertisersAction = $getAffectedAdvertisersAction;
        $this->storeDateAction = $storeDateAction;
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(Broadcast $broadcast, array $data): void
    {
        if (empty($data)) {
            return;
        }

        $this->attachSegmentGroup($broadcast, Arr::get($data, 'id'));
        $this->storeSegmentGroupPayload($broadcast, $data);
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function storeSegmentGroupPayload(Broadcast $broadcast, array $data): void
    {
        $groupId = Arr::get($data, 'id');

        if ($groupId === BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS) {
            $this->storeAdvertisers($broadcast, Arr::get($data, 'advertisers', []));
            return;
        }

        if ($groupId === BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS) {
            $this->storeAffectedAdvertisers($broadcast, $data);
            return;
        }
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function storeAffectedAdvertisers(Broadcast $broadcast, array $data): void
    {
        $dateStart = Arr::get($data, 'dates.date_start');
        $dateEnd = Arr::get($data, 'dates.date_end');

        if (!$dateStart || !$dateEnd) {
            $this->log->info('[Broadcast Segment Group Date] No data found. Skipped save.', [
                'broadcast_id' => $broadcast->id,
                'data'         => $data,
            ]);

            return;
        }

        $this->storeDateAction->handle($broadcast, $data);

        $ids = $this->getAffectedAdvertisersAction->handle(
            Arr::get($data, 'dates.date_start'),
            Arr::get($data, 'dates.date_end')
        );

        $this->storeAdvertisers($broadcast, $ids);
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $ids
     */
    private function storeAdvertisers(Broadcast $broadcast, array $ids): void
    {
        if (empty($ids)) {
            return;
        }

        $this->log->info('[Broadcast Segment Group] Started saving advertisers.', ['ids' => $ids]);

        /** @var BroadcastSegmentGroup $segmentGroup */
        $segmentGroup = $broadcast->segmentGroups()->first();

        $data = array_map(function (int $id) use ($broadcast, $segmentGroup): array {
            return [
                'user_id'      => $id,
                'broadcast_id' => $broadcast->id,
                'group_id'     => $segmentGroup->id,
            ];
        }, $ids);

        $result = $segmentGroup->selectedRecipients()->detach();

        if ($result > 0) {
            $this->log->info('[Broadcast Segment Group] Detached previously saved advertisers.', [
                'data'   => $data,
                'result' => $result,
            ]);
        }

        $result = $segmentGroup->selectedRecipients()->attach($data);

        $this->log->info('[Broadcast Segment Group] Finished saving advertisers.', [
            'data'   => $data,
            'result' => $result,
        ]);
    }

    /**
     * @param Broadcast $broadcast
     * @param int       $groupId
     */
    private function attachSegmentGroup(Broadcast $broadcast, int $groupId): void
    {
        $this->log->info('Attaching segment group to broadcast.', [
            'broadcast_id' => $broadcast->id,
            'group_id'     => $groupId,
        ]);

        $broadcast->segmentGroups()->detach();
        $broadcast->segmentGroups()->attach($groupId);

        $this->log->info('Segment group has been attached to broadcast.', [
            'broadcast_id' => $broadcast->id,
            'group_id'     => $groupId,
        ]);
    }
}
