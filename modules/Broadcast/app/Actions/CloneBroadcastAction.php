<?php

namespace Modules\Broadcast\Actions;

use App\Helpers\DateFormatHelper;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Broadcast\Actions\Traits\StoreBroadcast;
use Modules\Broadcast\Actions\Traits\ValidateAffectedDate;
use Modules\Broadcast\Actions\Traits\ValidateScheduleDate;
use Modules\Broadcast\Exceptions\BroadcastNotClonedException;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastChannel;
use Modules\Broadcast\Models\BroadcastStatus;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;
use Modules\Broadcast\Models\Traits\Channel;
use Modules\Broadcast\Repositories\BroadcastRepository;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class CloneBroadcastAction
{
    use StoreBroadcast,
        ValidateScheduleDate,
        ValidateAffectedDate,
        Channel;

    public const NAME_PREFIX = 'COPY - ';

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var BroadcastRepository
     */
    private $repository;

    /**
     * @param DatabaseManager     $databaseManager
     * @param LoggerInterface     $log
     * @param BroadcastRepository $repository
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        BroadcastRepository $repository
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param User|Authenticatable $admin
     * @param Broadcast            $broadcast
     *
     * @return Broadcast
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function handle(User $admin, Broadcast $broadcast): Broadcast
    {
        $this->log->info('Started clone broadcast.', ['broadcast_id' => $broadcast->id]);

        $this->databaseManager->beginTransaction();
        $data = $this->getBroadcastData($broadcast, $admin);

        try {
            $clonedBroadcast = $this->store($admin, $data);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            $this->log->warning('Broadcast cannot be cloned.', ['reason' => $exception->getMessage()]);

            throw BroadcastNotClonedException::createFrom($exception);
        }

        $this->databaseManager->commit();
        $this->log->info('Finished broadcast clone.', [
            'origin_broadcast_id' => $broadcast->id,
            'cloned_broadcast_id' => $clonedBroadcast->id,
        ]);

        return $clonedBroadcast;
    }

    /**
     * @param Broadcast            $broadcast
     * @param User|Authenticatable $user
     *
     * @return array
     */
    private function getBroadcastData(Broadcast $broadcast, User $user): array
    {
        $attributes = $broadcast->getAttributes();

        $attributesForClone = [
            'name',
            'schedule_date',
        ];

        $attributes = Arr::only($attributes, $attributesForClone);
        $attributes = $this->setCommonAttributes($attributes, $user, $broadcast);
        $attributes = $this->setScheduleDate($attributes);
        $attributes = $this->setEmail($attributes, $broadcast);
        $attributes = $this->setNotification($attributes, $broadcast);
        $attributes = $this->setSegmentGroup($attributes, $broadcast);

        return $attributes;
    }

    /**
     * @param array     $attributes
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return array
     */
    private function setCommonAttributes(array $attributes, User $user, Broadcast $broadcast): array
    {
        Arr::set($attributes, 'name', self::NAME_PREFIX . Arr::get($attributes, 'name'));
        Arr::set($attributes, 'status_id', BroadcastStatus::ID_DRAFT);
        Arr::set($attributes, 'user_id', $user->id);
        Arr::set($attributes, 'channel', BroadcastChannel::getChannel($broadcast));

        return $attributes;
    }

    /**
     * @param array $attributes
     *
     * @return array
     */
    private function setScheduleDate(array $attributes): array
    {
        $date = Arr::get($attributes, 'schedule_date');

        if ($date && !$this->validDate($date)) {
            $date = DateFormatHelper::toTimezone(
                $this->getDateSending()->toString(),
                config('app.timezone'),
                config('date.format.db_date_time')
            );
            Arr::set($attributes, 'schedule_date', $date);
        }

        return $attributes;
    }

    /**
     * @param array     $attributes
     * @param Broadcast $broadcast
     *
     * @return array
     */
    private function setEmail(array $attributes, Broadcast $broadcast): array
    {
        if (!$broadcast->hasEmail()) {
            return $attributes;
        }

        Arr::set($attributes, 'email', [
            'header'  => $broadcast->email->header,
            'subject' => $broadcast->email->subject,
            'icon'    => $broadcast->email->icon,
            'body'    => $broadcast->email->body,
        ]);

        return $attributes;
    }

    /**
     * @param array     $attributes
     * @param Broadcast $broadcast
     *
     * @return array
     */
    private function setNotification(array $attributes, Broadcast $broadcast): array
    {
        if (!$broadcast->hasNotification()) {
            return $attributes;
        }

        Arr::set($attributes, 'notification', [
            'notification_category_id' => $broadcast->notification->notification_category_id,
            'body'                     => $broadcast->notification->body,
        ]);

        return $attributes;
    }

    /**
     * @param array     $attributes
     * @param Broadcast $broadcast
     *
     * @return array
     */
    private function setSegmentGroup(array $attributes, Broadcast $broadcast): array
    {
        /** @var BroadcastSegmentGroup $segmentGroup */
        $segmentGroup = $broadcast->segmentGroups()->first();

        Arr::set($attributes, 'segment_group', [
            'id' => $segmentGroup->id,
        ]);

        if ($segmentGroup->isSelectedAdvertisers()) {
            $attributes = $this->addSelectedAdvertisersData($segmentGroup, $attributes);
        }

        if ($segmentGroup->isAffectedAdvertisers()) {
            $attributes = $this->addAffectedAdvertisersData($segmentGroup, $attributes);
        }

        return $attributes;
    }

    /**
     * @param BroadcastSegmentGroup $segmentGroup
     * @param array                 $attributes
     *
     * @return array
     */
    private function addSelectedAdvertisersData(BroadcastSegmentGroup $segmentGroup, array $attributes): array
    {
        Arr::set(
            $attributes,
            'segment_group.advertisers',
            $segmentGroup->recipients()->pluck('id')->toArray()
        );

        return $attributes;
    }

    /**
     * @param BroadcastSegmentGroup $segmentGroup
     * @param array                 $attributes
     *
     * @return array
     */
    private function addAffectedAdvertisersData(BroadcastSegmentGroup $segmentGroup, array $attributes): array
    {
        $dates = $segmentGroup->date->only(['date_start', 'date_end']);

        if (!$this->isValidAffectedDate($dates)) {
            $dates = $this->getValidAffectedDate();
        }

        Arr::set($attributes, 'segment_group.dates', $dates);

        return $attributes;
    }
}
