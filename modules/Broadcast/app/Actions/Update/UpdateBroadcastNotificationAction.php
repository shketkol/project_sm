<?php

namespace Modules\Broadcast\Actions\Update;

use Illuminate\Database\DatabaseManager;
use Modules\Broadcast\Actions\Store\StoreBroadcastNotificationAction;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastNotification;
use Modules\Broadcast\Repositories\BroadcastNotificationRepository;
use Psr\Log\LoggerInterface;

class UpdateBroadcastNotificationAction
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var BroadcastNotificationRepository
     */
    private $notificationRepository;

    /**
     * @var StoreBroadcastNotificationAction
     */
    private $storeBroadcastNotificationAction;

    /**
     * @param DatabaseManager                  $databaseManager
     * @param LoggerInterface                  $log
     * @param BroadcastNotificationRepository  $notificationRepository
     * @param StoreBroadcastNotificationAction $storeBroadcastNotificationAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        BroadcastNotificationRepository $notificationRepository,
        StoreBroadcastNotificationAction $storeBroadcastNotificationAction
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->notificationRepository = $notificationRepository;
        $this->storeBroadcastNotificationAction = $storeBroadcastNotificationAction;
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return BroadcastNotification|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function handle(Broadcast $broadcast, array $data): ?BroadcastNotification
    {
        $this->log->info('Updating broadcast notification.', [
            'broadcast_id'    => $broadcast->id,
            'notification_id' => optional($broadcast->notification)->id,
            'data'            => $data,
        ]);

        if (empty($data)) {
            return $this->deleteBroadcastNotification($broadcast);
        }

        if (is_null($broadcast->notification)) {
            return $this->createBroadcastNotification($broadcast, $data);
        }

        return $this->updateBroadcastNotification($broadcast, $data);
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return BroadcastNotification
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function createBroadcastNotification(Broadcast $broadcast, array $data): BroadcastNotification
    {
        $notification = $this->storeBroadcastNotificationAction->handle($data);
        $broadcast->notification()->associate($notification);
        $broadcast->save();

        return $notification;
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return BroadcastNotification
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function updateBroadcastNotification(Broadcast $broadcast, array $data): BroadcastNotification
    {
        $notification = $this->notificationRepository->update($data, $broadcast->notification->id);
        $this->log->info('Broadcast notification updated.', ['notification_id' => $broadcast->notification->id]);

        return $notification;
    }

    /**
     * @param Broadcast $broadcast
     *
     * @return null
     * @throws \Exception
     */
    public function deleteBroadcastNotification(Broadcast $broadcast)
    {
        if (is_null($broadcast->notification)) {
            $this->log->info('Skipped broadcast notification. No data was given.');
            return null;
        }

        $this->log->info('Started deleting broadcast notification.', [
            'broadcast_id'    => $broadcast->id,
            'notification_id' => $broadcast->notification->id,
        ]);

        $broadcast->notification->delete();

        $this->log->info('Finished deleting broadcast notification.', [
            'broadcast_id' => $broadcast->id,
        ]);

        return null;
    }
}
