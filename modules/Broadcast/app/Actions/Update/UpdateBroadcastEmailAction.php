<?php

namespace Modules\Broadcast\Actions\Update;

use Modules\Broadcast\Actions\Store\StoreBroadcastEmailAction;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastEmail;
use Modules\Broadcast\Repositories\BroadcastEmailRepository;
use Psr\Log\LoggerInterface;

class UpdateBroadcastEmailAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var BroadcastEmailRepository
     */
    private $emailRepository;

    /**
     * @var StoreBroadcastEmailAction
     */
    private $storeBroadcastEmailAction;

    /**
     * @param LoggerInterface           $log
     * @param BroadcastEmailRepository  $emailRepository
     * @param StoreBroadcastEmailAction $storeBroadcastEmailAction
     */
    public function __construct(
        LoggerInterface $log,
        BroadcastEmailRepository $emailRepository,
        StoreBroadcastEmailAction $storeBroadcastEmailAction
    ) {
        $this->log = $log;
        $this->emailRepository = $emailRepository;
        $this->storeBroadcastEmailAction = $storeBroadcastEmailAction;
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return BroadcastEmail|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function handle(Broadcast $broadcast, array $data): ?BroadcastEmail
    {
        $this->log->info('Updating broadcast email.', [
            'broadcast_id' => $broadcast->id,
            'email_id'     => optional($broadcast->email)->id,
            'data'         => $data,
        ]);

        if (empty($data)) {
            return $this->deleteBroadcastEmail($broadcast);
        }

        if (is_null($broadcast->email)) {
            return $this->createBroadcastEmail($broadcast, $data);
        }

        return $this->updateBroadcastEmail($broadcast, $data);
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return BroadcastEmail
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function createBroadcastEmail(Broadcast $broadcast, array $data): BroadcastEmail
    {
        $email = $this->storeBroadcastEmailAction->handle($data);
        $broadcast->email()->associate($email);
        $broadcast->save();

        return $email;
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return BroadcastEmail
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function updateBroadcastEmail(Broadcast $broadcast, array $data): BroadcastEmail
    {
        $email = $this->emailRepository->update($data, $broadcast->email->id);
        $this->log->info('Broadcast email updated.', ['email_id' => $broadcast->email->id]);

        return $email;
    }

    /**
     * @param Broadcast $broadcast
     *
     * @return null
     * @throws \Exception
     */
    public function deleteBroadcastEmail(Broadcast $broadcast)
    {
        if (is_null($broadcast->email)) {
            $this->log->info('Skipped broadcast email. No data was given.');
            return null;
        }

        $this->log->info('Started deleting broadcast email.', [
            'broadcast_id' => $broadcast->id,
            'email_id'     => $broadcast->email->id,
        ]);

        $broadcast->email->delete();

        $this->log->info('Finished deleting broadcast email.', [
            'broadcast_id' => $broadcast->id,
        ]);

        return null;
    }
}
