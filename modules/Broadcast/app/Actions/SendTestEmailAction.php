<?php

namespace Modules\Broadcast\Actions;

use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Log\Logger;
use Illuminate\Validation\ValidationException;
use Modules\Broadcast\Jobs\ProcessSendTestBroadcast;
use Modules\Broadcast\Models\Broadcast;
use Psr\Log\LoggerInterface;

class SendTestEmailAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var ValidateBroadcastAction
     */
    private $validateBroadcastAction;

    /**
     * @param LoggerInterface $log
     * @param Dispatcher $dispatcher
     * @param ValidateBroadcastAction $validateBroadcastAction
     */
    public function __construct(
        LoggerInterface $log,
        Dispatcher $dispatcher,
        ValidateBroadcastAction $validateBroadcastAction
    ) {
        $this->log = $log;
        $this->dispatcher = $dispatcher;
        $this->validateBroadcastAction = $validateBroadcastAction;
    }

    /**
     * @param Broadcast $broadcast
     * @param string $testEmail
     *
     * @return void
     * @throws ValidationException
     */
    public function handle(string $testEmail, Broadcast $broadcast): void
    {
        $this->log->info('Started adding the job to send test email.', ['broadcast_id' => $broadcast->id]);

        $this->validateBroadcastAction->handle($broadcast);

        $this->dispatcher->dispatch(new ProcessSendTestBroadcast($broadcast, $testEmail));

        $this->log->info('Finished adding the job to send test email.', ['broadcast_id' => $broadcast->id]);
    }
}
