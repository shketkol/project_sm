<?php

namespace Modules\Broadcast\Actions\Traits;

use Carbon\Carbon;

trait ValidateScheduleDate
{
    /**
     * @param string $date
     *
     * @return bool
     */
    private function validDate(string $date): bool
    {
        $broadcastDate = Carbon::parse($date)->setTimezone(config('date.default_timezone_full_code'));

        return $broadcastDate >= Carbon::now(config('date.default_timezone_full_code'));
    }

    /**
     * @return Carbon
     */
    private function getDateSending(): Carbon
    {
        $broadcastScheduleDate = Carbon::now(config('date.default_timezone_full_code'));
        $broadcastScheduleDate->setTime(
            config('date.schedule_broadcast.hour'),
            config('date.schedule_broadcast.minute'),
            config('date.schedule_broadcast.sec')
        );

        if ($broadcastScheduleDate < Carbon::now(config('date.default_timezone_full_code'))) {
            $broadcastScheduleDate->addDay();
        }

        return $broadcastScheduleDate;
    }
}
