<?php

namespace Modules\Broadcast\Actions\Traits;

use Illuminate\Support\Arr;
use Modules\Broadcast\Actions\Store\StoreBroadcastEmailAction;
use Modules\Broadcast\Actions\Store\StoreBroadcastNotificationAction;
use Modules\Broadcast\Actions\Store\StoreBroadcastSegmentGroupAction;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastChannel;
use Modules\Broadcast\Models\BroadcastEmail;
use Modules\Broadcast\Models\BroadcastNotification;
use Modules\Broadcast\Models\BroadcastStatus;
use Modules\Broadcast\Repositories\BroadcastRepository;
use Modules\User\Models\User;

/**
 * @property BroadcastRepository $repository
 */
trait StoreBroadcast
{
    /**
     * @param User  $admin
     * @param array $data
     *
     * @return Broadcast
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    private function store(User $admin, array $data): Broadcast
    {
        $email = $this->storeEmail($data);
        $notification = $this->storeNotification($data);

        $broadcastData = array_merge(
            Arr::except($data, ['email', 'notification', 'segment_group']),
            [
                'email_id'        => optional($email)->id,
                'notification_id' => optional($notification)->id,
                'status_id'       => BroadcastStatus::ID_DRAFT,
                'user_id'         => $admin->id,
            ]
        );

        $broadcast = $this->storeBroadcast($broadcastData);
        $this->storeSegmentGroup($broadcast, $data);

        return $broadcast;
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function storeSegmentGroup(Broadcast $broadcast, array $data): void
    {
        /** @var StoreBroadcastSegmentGroupAction $storeBroadcastSegmentGroupAction */
        $storeBroadcastSegmentGroupAction = app(StoreBroadcastSegmentGroupAction::class);
        $storeBroadcastSegmentGroupAction->handle($broadcast, Arr::get($data, 'segment_group', []));
    }

    /**
     * @param array $data
     *
     * @return BroadcastNotification|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function storeNotification(array $data): ?BroadcastNotification
    {
        $channel = Arr::get($data, 'channel');
        if ($channel !== BroadcastChannel::ALL && $channel !== BroadcastChannel::IN_APP_NOTIFICATION) {
            return null;
        }

        /** @var StoreBroadcastNotificationAction $storeBroadcastNotificationAction */
        $storeBroadcastNotificationAction = app(StoreBroadcastNotificationAction::class);

        return $storeBroadcastNotificationAction->handle(Arr::get($data, 'notification', []));
    }

    /**
     * @param array $data
     *
     * @return BroadcastEmail|null
     * @throws \Exception
     */
    private function storeEmail(array $data): ?BroadcastEmail
    {
        $channel = Arr::get($data, 'channel');
        if ($channel !== BroadcastChannel::ALL && $channel !== BroadcastChannel::EMAIL) {
            return null;
        }

        /** @var StoreBroadcastEmailAction $storeBroadcastEmailAction */
        $storeBroadcastEmailAction = app(StoreBroadcastEmailAction::class);

        return $storeBroadcastEmailAction->handle(Arr::get($data, 'email', []));
    }

    /**
     * @param array $data
     *
     * @return Broadcast
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function storeBroadcast(array $data): Broadcast
    {
        $this->log->info('Storing new broadcast.', ['data' => $data]);
        $broadcast = $this->repository->create($data);
        $this->log->info('Broadcast created.', ['broadcast_id' => $broadcast->id]);

        return $broadcast;
    }
}
