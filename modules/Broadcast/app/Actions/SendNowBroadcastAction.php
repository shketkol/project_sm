<?php

namespace Modules\Broadcast\Actions;

use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Support\Carbon;
use Modules\Broadcast\Jobs\ProcessBroadcast;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastStatus;
use Modules\Broadcast\Repositories\BroadcastRepository;
use Prettus\Validator\Exceptions\ValidatorException;
use Psr\Log\LoggerInterface;

class SendNowBroadcastAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var BroadcastRepository
     */
    private $repository;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @param LoggerInterface $log
     * @param BroadcastRepository $repository
     * @param Dispatcher $dispatcher
     */
    public function __construct(LoggerInterface $log, BroadcastRepository $repository, Dispatcher $dispatcher)
    {
        $this->log = $log;
        $this->repository = $repository;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Broadcast $broadcast
     *
     * @return Broadcast
     * @throws \Throwable
     */
    public function handle(Broadcast $broadcast): Broadcast
    {
        $this->log->info('Start send now broadcast.', [
            'broadcast_id'  => $broadcast->id,
            'schedule_date' => $broadcast->schedule_date
        ]);

        $newBroadcast = $this->setNowScheduleDate($broadcast);

        $this->log->info('Finished send now broadcast.', [
            'broadcast_id'      => $broadcast->id,
            'old_schedule_date' => $broadcast->schedule_date,
            'new_schedule_date' => $newBroadcast->schedule_date
        ]);

        return $newBroadcast;
    }

    /**
     * @param Broadcast $broadcast
     *
     * @return Broadcast
     * @throws ValidatorException
     */
    private function setNowScheduleDate(Broadcast $broadcast): Broadcast
    {
        $date = Carbon::now()->format(config('date.format.db_date_time'));
        $broadcast = $this->repository->update(['schedule_date' => $date], $broadcast->id);

        $broadcast->applyInternalStatus(BroadcastStatus::PROCESSING);
        $this->dispatcher->dispatch(new ProcessBroadcast($broadcast));

        $this->log->info('Broadcast dispatched.', [
            'broadcast_id' => $broadcast->id,
        ]);

        return $broadcast;
    }
}
