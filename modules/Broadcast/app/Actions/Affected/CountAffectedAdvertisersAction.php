<?php

namespace Modules\Broadcast\Actions\Affected;

use Carbon\Carbon;
use Modules\User\Repositories\ActivityRepository;

class CountAffectedAdvertisersAction
{
    /**
     * @var ActivityRepository
     */
    private $repository;

    /**
     * @param ActivityRepository $repository
     */
    public function __construct(ActivityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     *
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(string $startDate, string $endDate): int
    {
        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);

        return $this->repository->countActiveAdvertisers($start, $end);
    }
}
