<?php

namespace Modules\Broadcast\Repositories;

use App\Repositories\Repository;
use Modules\Broadcast\Models\BroadcastEmail;

class BroadcastEmailRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * @return string
     */
    public function model(): string
    {
        return BroadcastEmail::class;
    }
}
