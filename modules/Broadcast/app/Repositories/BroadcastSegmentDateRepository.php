<?php

namespace Modules\Broadcast\Repositories;

use App\Repositories\Repository;
use Modules\Broadcast\Models\Segment\BroadcastSegmentDate;

class BroadcastSegmentDateRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * @return string
     */
    public function model(): string
    {
        return BroadcastSegmentDate::class;
    }
}
