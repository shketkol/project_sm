<?php

namespace Modules\Broadcast\Repositories;

use App\Repositories\Repository;
use Modules\Broadcast\Models\BroadcastNotification;

class BroadcastNotificationRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * @return string
     */
    public function model(): string
    {
        return BroadcastNotification::class;
    }
}
