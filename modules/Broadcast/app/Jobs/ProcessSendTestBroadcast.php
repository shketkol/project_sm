<?php

namespace Modules\Broadcast\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Notifications\BroadcastTestEmailNotification;
use Psr\Log\LoggerInterface;

class ProcessSendTestBroadcast extends Job
{
    /**
     * @var Broadcast
     */
    public $broadcast;

    /**
     * @var string
     */
    public $email;

    /**
     * @param Broadcast $broadcast
     * @param string $email
     */
    public function __construct(Broadcast $broadcast, string $email)
    {
        $this->broadcast = $broadcast;
        $this->email = $email;
    }

    /**
     * Process broadcast job
     */
    public function handle(): void
    {
        $this->log()->info('[Broadcast send test email job] Started processing.', [
            'job_id'       => $this->job->getJobId(),
            'broadcast_id' => $this->broadcast->id,
        ]);

        try {
            $this->sendEmail();
        } catch (\Throwable $exception) {
            $this->log()->warning('[Broadcast send test email job] Failed processing.', [
                'reason' => $exception->getMessage(),
            ]);

            $this->fail($exception);
        }

        $this->log()->info('[Broadcast send test email job] Finished processing.', [
            'job_id'       => $this->job->getJobId(),
            'broadcast_id' => $this->broadcast->id,
        ]);
    }

    /**
     * @return void
     */
    private function sendEmail(): void
    {
        $mailer = app(Mailer::class);
        $mail = (new BroadcastTestEmailNotification($this->broadcast))->toMail($this->broadcast->user);
        $mailer->to($this->email)->send($mail);

        $this->log()->info('[Broadcast send test email job] Email sent.', [
            'job_id'       => $this->job->getJobId(),
            'broadcast_id' => $this->broadcast->id,
        ]);
    }

    /**
     * @return LoggerInterface
     */
    private function log(): LoggerInterface
    {
        return app('log');
    }
}
