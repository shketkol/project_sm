@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <the-admin-broadcast-index></the-admin-broadcast-index>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/broadcast.js') }}"></script>
@endpush
