<?php

return [
    'close_modal_body'               => 'Do you want to close the broadcast edit form without saving changes?',
    'fail_clone'                     => 'Broadcast was not duplicate',
    'success_sent_test_email'        => 'Test Email broadcast has been successfully sent to the specified email address.',
    'send_now_modal_warning_message' => 'Do you want to send the broadcast now instead of the specified date and time?',
    'send_now_modal_notice_message'  => 'Note: This action cannot be undone.',
    'send_now_success'               => 'The broadcast has been successfully added to the notifications queue and will be sent shortly.',
    'affected_date_description'      => 'The Broadcast will be sent to all advertisers that have been using Hulu Ad Manager during the selected date range.'
];
