<?php

namespace Modules\Broadcast\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Broadcast\Models\BroadcastStatus;

class BroadcastStatusesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $statuses = [
            [
                'id'   => BroadcastStatus::ID_DRAFT,
                'name' => BroadcastStatus::DRAFT,
            ],
            [
                'id'   => BroadcastStatus::ID_SCHEDULED,
                'name' => BroadcastStatus::SCHEDULED,
            ],
            [
                'id'   => BroadcastStatus::ID_PROCESSING,
                'name' => BroadcastStatus::PROCESSING,
            ],
            [
                'id'   => BroadcastStatus::ID_SENT,
                'name' => BroadcastStatus::SENT,
            ],
            [
                'id'   => BroadcastStatus::ID_FAILED,
                'name' => BroadcastStatus::FAILED,
            ],
        ];

        foreach ($statuses as $status) {
            BroadcastStatus::updateOrCreate(
                ['id' => Arr::get($status, 'id')],
                $status
            );
        }
    }
}
