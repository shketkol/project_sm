<?php

namespace Modules\Broadcast\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;

class BroadcastSegmentGroupsTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $groups = [
            [
                'id'   => BroadcastSegmentGroup::ID_ALL_ADVERTISERS,
                'name' => BroadcastSegmentGroup::ALL_ADVERTISERS,
            ],
            [
                'id'   => BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS,
                'name' => BroadcastSegmentGroup::SELECTED_ADVERTISERS,
            ],
            [
                'id'   => BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS,
                'name' => BroadcastSegmentGroup::AFFECTED_ADVERTISERS,
            ],
        ];

        foreach ($groups as $group) {
            BroadcastSegmentGroup::updateOrCreate(
                ['id' => Arr::get($group, 'id')],
                $group
            );
        }
    }
}
