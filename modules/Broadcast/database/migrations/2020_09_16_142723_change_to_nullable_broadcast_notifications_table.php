<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeToNullableBroadcastNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('broadcast_notifications', function (Blueprint $table) {
            $table->unsignedBigInteger('notification_category_id')->nullable()->change();
            $table->longText('body')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('broadcast_notifications', function (Blueprint $table) {
            $table->unsignedBigInteger('notification_category_id')->change();
            $table->longText('body')->change();
        });
    }
}
