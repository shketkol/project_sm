<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBroadcastSegmentDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('broadcast_segment_dates', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('group_id')->unsigned()->index();
            $table->foreign('group_id')
                ->references('id')
                ->on('broadcast_segment_groups')
                ->onDelete('cascade');

            $table->bigInteger('broadcast_id')->unsigned()->index();
            $table->foreign('broadcast_id')
                ->references('id')
                ->on('broadcasts')
                ->onDelete('cascade');

            $table->unique(['group_id', 'broadcast_id']);

            $table->timestamp('date_start')->nullable();
            $table->timestamp('date_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('broadcast_segment_dates');
    }
}
