<?php

use Illuminate\Database\Migrations\Migration;
use Modules\User\Models\Activity\Activity;
use Modules\User\Models\Activity\ActivityType;
use Illuminate\Database\Eloquent\Collection;
use Psr\Log\LoggerInterface;
use Illuminate\Database\DatabaseManager;

class DeleteExtraLogoutRecordsFromActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->log()->info('[Activity] Started deleting extra logout records.');

        $this->databaseManager()->beginTransaction();

        try {
            $this->deleteExtraLogoutRecords();
        } catch (\Throwable $exception) {
            $this->log()->warning('[Activity] Failed deleting extra logout records.', [
                'reason' => $exception->getMessage(),
            ]);
            $this->databaseManager()->rollBack();
        }

        $this->databaseManager()->commit();

        $this->log()->info('[Activity] Finished deleting extra logout records.');
    }

    /**
     * Delete extra logout records
     */
    private function deleteExtraLogoutRecords(): void
    {
        $userIds = $this->getActivityUserIds();

        $userIds->each(function (Activity $activity): void {
            $userActivities = $this->findUserActivities($activity);
            $this->deleteUserLogoutRecords($userActivities);
        });
    }

    /**
     * @param Activity $activity
     *
     * @return Activity[]|Collection
     */
    private function findUserActivities(Activity $activity): Collection
    {
        return Activity::query()
            ->where('user_id', '=', $activity->user_id)
            ->get();
    }

    /**
     * Delete logout row if previous row was also logout
     *
     * @param Activity[]|Collection $userActivities
     */
    private function deleteUserLogoutRecords(Collection $userActivities): void
    {
        $previousRowType = null;
        foreach ($userActivities as $row) {
            if ($row->type_id === ActivityType::ID_LOGOUT && $previousRowType === ActivityType::ID_LOGOUT) {
                $row->delete();
                $this->log()->info('[Activity] Deleted logout record.', [
                    'id'      => $row->id,
                    'user_id' => $row->user_id,
                ]);
            }

            $previousRowType = $row->type_id;
        }
    }

    /**
     * @return LoggerInterface
     */
    private function log(): LoggerInterface
    {
        return app(LoggerInterface::class);
    }

    /**
     * @return DatabaseManager
     */
    private function databaseManager(): DatabaseManager
    {
        return app(DatabaseManager::class);
    }

    /**
     * @return Collection
     */
    private function getActivityUserIds(): Collection
    {
        $users = Activity::query()
            ->select('user_id')
            ->groupBy('user_id')
            ->get();

        $this->log()->info('[Activity] Found affected advertisers.', [
            'count' => $users->count(),
            'users' => $users->pluck('user_id')->toArray(),
        ]);

        return $users;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        // nothing
    }
}
