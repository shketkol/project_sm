<?php

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Migrations\Migration;
use Modules\Creative\Models\CreativeError;
use Modules\Creative\Models\Pivot\CampaignCreative;
use Psr\Log\LoggerInterface;

class SeedCampaignIdToCreativeErrorsTable extends Migration
{
    /**
     * @var int
     */
    private const BATCH_SIZE = 1000;

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up(): void
    {
        $this->databaseManager()->beginTransaction();

        try {
            $this->seedCampaignId();
        } catch (\Throwable $exception) {
            $this->databaseManager()->rollBack();
            throw $exception;
        }

        $this->databaseManager()->commit();
    }

    /**
     * Seed `campaign_id` to `creative_errors` table based on `campaign_creative` data.
     */
    private function seedCampaignId(): void
    {
        $total = CampaignCreative::query()->count();

        $this->log()->info('Started import creative_errors table.', [
            'total' => $total,
        ]);

        for ($counter = 0; $counter <= $total; $counter += self::BATCH_SIZE) {
            $slice = CampaignCreative::query()->limit(self::BATCH_SIZE)->offset($counter)->get();
            $this->updateCreativeErrors($slice);
        }

        $this->log()->info('Finished import creative_errors table.', [
            'total' => $total,
        ]);
    }

    /**
     * @param Collection|CampaignCreative[] $collection
     */
    private function updateCreativeErrors(Collection $collection): void
    {
        $collection->each(function (CampaignCreative $campaignCreative): void {
            $errors = CreativeError::query()
                ->where('creative_id', '=', $campaignCreative->creative_id)
                ->whereNull('campaign_id')
                ->get();

            if ($errors->isEmpty()) {
                return;
            }

            $this->log()->info('Campaign_id in creative_errors would be updated.', [
                'creative_errors' => $errors->pluck('id')->toArray(),
            ]);

            $errors->each(function (CreativeError $creativeError) use ($campaignCreative): void {
                $creativeError->campaign_id = $campaignCreative->campaign_id;
                $creativeError->save();

                $this->log()->info('Creative error updated.', [
                    'creative_error_id'    => $creativeError->id,
                    'campaign_creative_id' => $campaignCreative->id,
                    'campaign_id'          => $campaignCreative->campaign_id,
                ]);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        //
    }

    /**
     * @return DatabaseManager
     */
    private function databaseManager(): DatabaseManager
    {
        return app(DatabaseManager::class);
    }

    /**
     * @return LoggerInterface
     */
    private function log(): LoggerInterface
    {
        return app(LoggerInterface::class);
    }
}
