<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddProResColumnToCreatives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('creatives', function (Blueprint $table) {
            $table->boolean('is_pro_res')->default(false);
        });

        if (Schema::hasColumn('creatives', 'is_pro_res')) {
            $this->markMovAsProRes();
        }
    }

    /**
     * Since most of *.mov videos are Apple ProRes we mark them to fix play issues.
     */
    private function markMovAsProRes(): void
    {
        DB::table('creatives')
            ->where('extension', '=', 'mov')
            ->update(['is_pro_res' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('creatives', function (Blueprint $table) {
            $table->dropColumn('is_pro_res');
        });
    }
}
