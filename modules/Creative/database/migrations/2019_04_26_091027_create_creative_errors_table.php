<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreativeErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creative_errors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('subject');
            $table->json('messages');

            $table->bigInteger('creative_id')->unsigned()->index();
            $table->foreign('creative_id')->references('id')->on('creatives')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creative_errors');
    }
}
