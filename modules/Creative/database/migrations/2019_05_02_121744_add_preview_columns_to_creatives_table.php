<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Creative\Models\CreativeStatus;

class AddPreviewColumnsToCreativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('creatives', function (Blueprint $table) {
            $table->string('original_key')->nullable();
            $table->string('preview_key')->nullable();
            $table->string('poster_key')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('creatives', function (Blueprint $table) {
            $table->dropColumn('original_key');
            $table->dropColumn('preview_key');
            $table->dropColumn('poster_key');
        });
    }
}
