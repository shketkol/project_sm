<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Creative\Models\CreativeStatus;

class UpdateCreativeStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('creatives', 'status_id'))
        {
            Schema::table('creatives', function (Blueprint $table) {
                $table->bigInteger('status_id')->unsigned()->default(CreativeStatus::ID_DRAFT)->index();
                $table->foreign('status_id')->references('id')->on('creative_statuses')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
