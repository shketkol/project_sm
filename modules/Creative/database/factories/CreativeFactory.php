<?php

namespace Modules\Creative\Database\Factories;

use App\Factories\Factory;
use Modules\Creative\Models\Creative;
use Modules\User\Models\User;

class CreativeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Creative::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name'         => $this->faker->company,
            'extension'    => 'mpeg',
            'duration'     => rand(1, 900),
            'width'        => rand(1, 900),
            'height'       => rand(1, 900),
            'filesize'     => rand(4000, 9999999),
            'hash'         => $this->faker->text(200),
            'status_id'    => rand(1, 4),
            'external_id'  => $this->faker->uuid,
            'original_key' => $this->faker->url,
            'poster_key'   => $this->faker->url,
            'user_id'      => User::factory(),
        ];
    }
}
