<?php

namespace Modules\Creative\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Creative\Policies\CreativePolicy;
use Modules\User\Models\Role;

class CreativePermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [
            CreativePolicy::PERMISSION_VIEW_CREATIVE,
            CreativePolicy::PERMISSION_UPDATE_CREATIVE,
            CreativePolicy::PERMISSION_LIST_ADVERTISER_CREATIVE
        ],
        Role::ID_ADMIN_READ_ONLY => [
            CreativePolicy::PERMISSION_VIEW_CREATIVE,
            CreativePolicy::PERMISSION_LIST_ADVERTISER_CREATIVE
        ],
        Role::ID_ADVERTISER      => [
            CreativePolicy::PERMISSION_CREATE_CREATIVE,
            CreativePolicy::PERMISSION_DELETE_CREATIVE,
            CreativePolicy::PERMISSION_LIST_CREATIVE,
            CreativePolicy::PERMISSION_UPDATE_CREATIVE,
            CreativePolicy::PERMISSION_VIEW_CREATIVE,
        ],
    ];
}
