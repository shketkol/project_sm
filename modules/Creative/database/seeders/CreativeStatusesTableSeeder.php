<?php

namespace Modules\Creative\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Creative\Models\CreativeStatus;

class CreativeStatusesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run()
    {
        $statuses = [
            [
                'id'   => CreativeStatus::ID_DRAFT,
                'name' => CreativeStatus::DRAFT,
            ],
            [
                'id'   => CreativeStatus::ID_PENDING_APPROVAL,
                'name' => CreativeStatus::PENDING_APPROVAL,
            ],
            [
                'id'   => CreativeStatus::ID_APPROVED,
                'name' => CreativeStatus::APPROVED,
            ],
            [
                'id'   => CreativeStatus::ID_REJECTED,
                'name' => CreativeStatus::REJECTED,
            ],
            [
                'id'   => CreativeStatus::ID_PROCESSING,
                'name' => CreativeStatus::PROCESSING,
            ],
        ];

        foreach ($statuses as $status) {
            CreativeStatus::updateOrCreate(
                ['id' => Arr::get($status, 'id')],
                $status
            );
        }
    }
}
