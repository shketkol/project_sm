<?php

namespace Modules\Creative\Listeners;

use App\Exceptions\ModelNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Repositories\CampaignRepository;
use Illuminate\Database\DatabaseManager;
use Modules\Creative\Models\Creative;
use Modules\Daapi\DataTransferObjects\Types\CreativeUpdate\CreativeData;
use Modules\Daapi\Events\Creative\CreativeUpdated;
use Modules\Daapi\Listeners\Notifications\BaseNotificationListener;
use Modules\Haapi\Actions\Campaign\CampaignGet;
use Modules\Creative\Actions\Notifications\UpdateCreativeStatusAction;
use Psr\Log\LoggerInterface;

class CreativeUpdatedListener extends BaseNotificationListener
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @var CampaignGet
     */
    private $campaignGet;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var UpdateCreativeStatusAction
     */
    private $updateCreativeAction;

    /**
     * @param LoggerInterface $log
     * @param CampaignRepository $repository
     * @param CampaignGet $campaignGet
     * @param DatabaseManager $databaseManager
     * @param UpdateCreativeStatusAction $updateCreativeAction
     */
    public function __construct(
        LoggerInterface $log,
        CampaignRepository $repository,
        CampaignGet $campaignGet,
        DatabaseManager $databaseManager,
        UpdateCreativeStatusAction $updateCreativeAction
    ) {
        $this->log = $log;
        $this->repository = $repository;
        $this->campaignGet = $campaignGet;
        $this->databaseManager = $databaseManager;
        $this->updateCreativeAction = $updateCreativeAction;
    }

    /**
     * @param CreativeUpdated $event
     *
     * @throws \App\Exceptions\BaseException
     */
    public function handle(CreativeUpdated $event): void
    {
        $campaignId = $event->getCampaignId();
        $this->log->info('Updating creative started.', ['campaign_id' => $campaignId]);

        $this->databaseManager->beginTransaction();

        try {
            $campaign = $this->repository->findCampaignByExternalId($campaignId);
            $this->updateCampaignStatus($campaign);

            $creativeData = $event->getCreative();
            $this->updateCreative($campaign->getActiveCreative(), $creativeData);
        } catch (\Throwable $exception) {
            $this->log->warning('Updating creative failed.', [
                'reason'      => $exception->getMessage(),
                'campaign_id' => $campaignId,
            ]);
            $this->databaseManager->rollBack();

            throw ModelNotUpdatedException::createFrom($exception);
        }

        $this->databaseManager->commit();

        $this->log->info('Updating creative finished.', ['campaign_id' => $campaign->id]);
    }

    /**
     * @param Creative $creative
     * @param CreativeData $creativeData
     *
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function updateCreative(Creative $creative, CreativeData $creativeData): void
    {
        $creative->external_id = $creativeData->id;
        $creative->save();
        $this->updateCreativeAction->handle(
            $creativeData->id,
            $creativeData->status,
            ['comments' => $creativeData->comment, 'reason' => $creativeData->reason]
        );
    }

    /**
     * @param Campaign $campaign
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \SM\SMException
     */
    protected function updateCampaignStatus(Campaign $campaign): void
    {
        $campaignData = $this->campaignGet->handle($campaign->external_id, $this->authAsAdmin());
        $campaign->applyHaapiStatus($campaignData->status);
    }
}
