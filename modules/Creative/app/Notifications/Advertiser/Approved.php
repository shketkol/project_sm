<?php

namespace Modules\Creative\Notifications\Advertiser;

use App\Helpers\HtmlHelper;
use App\Notifications\Traits\AdvertiserNotification;
use Illuminate\Support\Arr;
use Modules\Creative\Mail\Advertiser\Approved as Mail;
use Modules\Creative\Notifications\CreativeNotification;
use Modules\User\Models\User;

class Approved extends CreativeNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'creativeName'    => $this->creative->name,
            'creativeDetails' => $this->creativeDetailsUrl(),
            'creativeGallery' => route('creatives.index'),
            'title'           => __('creative::emails.advertiser.approved.title'),
            'titleIcon'       => 'thumbs_up',
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        /** @var HtmlHelper $html */
        $html = app(HtmlHelper::class);

        return __('creative::notifications.advertiser.approved', [
            'creative_name'    => $html->createAnchorElement(Arr::get($data, 'creativeDetails'), [
                'title' => Arr::get($data, 'creativeName'),
            ]),
            'creative_gallery' => $html->getCreativeGalleryLink(),
        ]);
    }
}
