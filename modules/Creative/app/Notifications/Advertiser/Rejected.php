<?php

namespace Modules\Creative\Notifications\Advertiser;

use App\Helpers\HtmlHelper;
use App\Notifications\Traits\AdvertiserNotification;
use Illuminate\Support\Arr;
use Modules\Creative\Mail\Advertiser\Rejected as Mail;
use Modules\Creative\Notifications\CreativeNotification;
use Modules\User\Models\User;

class Rejected extends CreativeNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        $reason = self::replaceDot($this->creative->rejection_reason);

        return [
            'firstName'              => $notifiable->first_name,
            'creativeName'           => $this->creative->name,
            'creativeDetails'        => $this->creativeDetailsUrl(),
            'reason'                 => $reason,
            'title'                  => __('creative::emails.advertiser.rejected.title'),
            'titleIcon'              => 'rejected',
            'techSpecificationsLink' => config('general.links.technical_specifications'),
            'adPoliciesLink'         => config('general.links.terms_of_use'),
            'creativeGallery'        => route('creatives.index'),
            'campaignDetails'        => route(
                'campaigns.show',
                ['campaign' => optional($this->creative->campaigns->first())->id]
            ),
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        $reason = self::replaceDot(Arr::get($data, 'reason'));

        /** @var HtmlHelper $html */
        $html = app(HtmlHelper::class);

        return __('creative::notifications.advertiser.rejected', [
            'creative_name' => $html->createAnchorElement(Arr::get($data, 'creativeDetails'), [
                'title' => Arr::get($data, 'creativeName'),
            ]),
            'reason'        => e($reason),
        ]);
    }

    /**
     * @param null|string $reason
     *
     * @return null|string
     */
    private static function replaceDot(?string $reason): ?string
    {
        if (!empty($reason) && substr($reason, -1) === ".") {
            $reason = substr($reason, 0, -1);
        }

        return $reason;
    }
}
