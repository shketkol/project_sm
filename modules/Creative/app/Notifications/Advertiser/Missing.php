<?php

namespace Modules\Creative\Notifications\Advertiser;

use App\Helpers\DateFormatHelper;
use App\Helpers\HtmlHelper;
use App\Notifications\Traits\AdvertiserNotification;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Mail\Advertiser\Missing as Mail;
use Modules\Creative\Notifications\CreativeNotification;
use Modules\User\Models\User;

class Missing extends CreativeNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'campaignName'    => $this->campaign->name,
            'title'           => __('creative::emails.advertiser.missing.title'),
            'titleIcon'       => 'question_window',
            'startDate'       => DateFormatHelper::toMonthDayYear($this->campaign->date_start),
            'campaignDetails' => route('campaigns.show', ['campaign' => $this->campaign->id]),
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        /** @var HtmlHelper $html */
        $html = app(HtmlHelper::class);

        return __('creative::notifications.advertiser.missing', [
            'campaign_name' => $html->createAnchorElement(Arr::get($data, 'campaignDetails'), [
                'title' => Arr::get($data, 'campaignName'),
            ]),
        ]);
    }
}
