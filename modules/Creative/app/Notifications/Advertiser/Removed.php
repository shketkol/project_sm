<?php

namespace Modules\Creative\Notifications\Advertiser;

use App\Notifications\Notification;
use Illuminate\Support\Arr;
use Modules\Notification\Models\NotificationCategory;
use Modules\User\Models\User;

class Removed extends Notification
{
    /**
     * @var string
     */
    protected $creativeName;

    /**
     * Removed notification constructor.
     *
     * @param string $creativeName
     */
    public function __construct(string $creativeName)
    {
        $this->creativeName = $creativeName;
    }

    /**
     * Get the notification channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_CREATIVE;
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'creativeName' => $this->creativeName,
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        return __('creative::notifications.advertiser.removed', [
            'creative_name' => e(Arr::get($data, 'creativeName'))
        ]);
    }
}
