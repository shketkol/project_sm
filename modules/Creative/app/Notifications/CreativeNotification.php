<?php

namespace Modules\Creative\Notifications;

use App\Notifications\Notification;
use Modules\Creative\Models\Creative;
use Modules\Notification\Models\NotificationCategory;

abstract class CreativeNotification extends Notification
{
    /**
     * @var Creative
     */
    protected $creative;

    /**
     * CreativeNotification constructor.
     *
     * @param Creative $creative
     */
    public function __construct(Creative $creative)
    {
        $this->creative = $creative;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_CREATIVE;
    }

    /**
     * @return string
     */
    public function creativeDetailsUrl(): string
    {
        return route('creatives.show', ['creative' => $this->creative->id]);
    }

    /**
     * @return string
     */
    public function campaignDetailsUrl(): string
    {
        return route('campaigns.show', ['campaign' => optional($this->creative->campaigns->first())->id]);
    }
}
