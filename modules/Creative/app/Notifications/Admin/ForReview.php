<?php

namespace Modules\Creative\Notifications\Admin;

use App\Notifications\Traits\AdminNotification;
use Modules\Creative\Mail\Admin\ForReview as Mail;
use Modules\Creative\Notifications\CreativeNotification;
use Modules\User\Models\User;

class ForReview extends CreativeNotification
{
    use AdminNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'        => $notifiable->first_name,
            'campaign_details' => $this->campaignDetailsUrl(),
            'advertiser'       => $this->creative->user->full_name,
        ];
    }
}
