<?php

namespace Modules\Creative\Notifications\Admin;

use App\Notifications\Traits\AdminNotification;
use Modules\Creative\Mail\Admin\Rejected as Mail;
use Modules\Creative\Notifications\CreativeNotification;
use Modules\User\Models\User;

class Rejected extends CreativeNotification
{
    use AdminNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'         => $notifiable->first_name,
            'companyName'       => $this->creative->user->company_name,
            'creativeName'      => $this->creative->name,
            'reason'            => $this->creative->rejection_reason,
            'advertiserDetails' => route('advertisers.show', [
                'advertiser' => $this->creative->user->id,
                '#creatives',
            ]),
            'creativeDetails'   => $this->creativeDetailsUrl()
        ];
    }
}
