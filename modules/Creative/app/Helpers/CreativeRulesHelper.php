<?php

namespace Modules\Creative\Helpers;

use App\Services\ValidationRulesService\ValidationRules;

class CreativeRulesHelper
{
    /**
     * @var ValidationRules
     */
    private $validationRules;

    public function __construct(ValidationRules $validationRules)
    {
        $this->validationRules = $validationRules;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'video_cnt'              => $this->validationRules->only(
                'creative.general.video_cnt',
                ['required', 'max', 'min']
            ),
            'audio_cnt'              => $this->validationRules->only(
                'creative.general.video_cnt',
                ['required', 'max', 'min']
            ),
            'extension'              => $this->validationRules->only(
                'creative.general.extension',
                ['required', 'included']
            ),
            'video_format'           => $this->validationRules->only(
                'creative.general.format',
                ['required', 'included']
            ),
            'codec'                  => $this->validationRules->only(
                'creative.general.codec',
                ['required', 'included']
            ),
            'file_size'              => $this->validationRules->only(
                'creative.general.file_size',
                ['required', 'max_filesize',]
            ),
            'duration'               => $this->validationRules->only(
                'creative.general.duration',
                ['required', 'numeric', 'between']
            ),
            'video_bit_rate'         => $this->validationRules->only(
                'creative.general.video_bit_rate',
                ['required', 'numeric', 'min_video']
            ),
            'width'                  => $this->validationRules->only(
                'creative.general.width',
                ['required', 'numeric', 'min_video']
            ),
            'height'                 => $this->validationRules->only(
                'creative.general.height',
                ['required', 'numeric', 'min_video']
            ),
            'display_aspect_ratio'   => $this->validationRules->only(
                'creative.general.display_aspect_ratio',
                ['required', 'included']
            ),
            'frame_rate_mode'        => $this->validationRules->only(
                'creative.general.frame_rate_mode',
                ['required', 'included']
            ),
            'frame_rate'             => $this->validationRules->only(
                'creative.general.frame_rate',
                ['required', 'included']
            ),
            'color_space'            => $this->validationRules->only(
                'creative.general.color_space',
                ['required', 'included']
            ),
            'chroma_subsampling'     => $this->validationRules->only(
                'creative.general.chroma_subsampling',
                ['required', 'included']
            ),
            'scan_type'              => $this->validationRules->only(
                'creative.general.scan_type',
                ['required', 'included']
            ),
            'audios.*.format'        => $this->validationRules->only(
                'creative.audio.format',
                ['required', 'included']
            ),
            'audios.*.duration'      => $this->validationRules->only(
                'creative.audio.duration',
                ['required', 'numeric', 'between']
            ),
            'audios.*.bit_rate'      => $this->validationRules->only(
                'creative.audio.bit_rate',
                ['required', 'numeric', 'min_video']
            ),
            'audios.*.channels'      => $this->validationRules->only(
                'creative.audio.channels',
                ['required', 'numeric', 'included']
            ),
            'audios.*.sampling_rate' => $this->validationRules->only(
                'creative.audio.sampling_rate',
                ['required', 'included']
            ),
            'audios.*.bit_depth'     => $this->validationRules->only(
                'creative.audio.bit_depth',
                ['nullable', 'included']
            ),
        ];
    }
}
