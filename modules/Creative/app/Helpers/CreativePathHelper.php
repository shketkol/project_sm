<?php

namespace Modules\Creative\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\Creative\Repositories\CreativeRepository;
use Modules\User\Models\User;

class CreativePathHelper
{
    /**
     * Path structure.
     */
    public const USER_FOLDER_INDEX      = 0;
    public const CREATIVES_FOLDER_INDEX = 1;
    public const FILE_NAME_INDEX        = 2;

    /**
     * How much parts should be if exploded by /
     * /cfece4bfde7f866d7b51dab9185cb5a84a9efa6b23784a9b77799f3d67e8f3eb/creatives/4402f81a-4654-40b4-bea5-1c44f3b8c60b.mp4
     *
     * cfece4bfde7f866d7b51dab9185cb5a84a9efa6b23784a9b77799f3d67e8f3eb - first part
     * creatives - second part
     * 4402f81a-4654-40b4-bea5-1c44f3b8c60b.mp4 - third part
     */
    public const PATH_PARTS = 3;


    /**
     * @var CreativeRepository
     */
    private $creativeRepository;

    /**
     * @param CreativeRepository $creativeRepository
     */
    public function __construct(CreativeRepository $creativeRepository)
    {
        $this->creativeRepository = $creativeRepository;
    }

    /**
     * @param string $path
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function creativeWithPathExists(string $path): bool
    {
        return $this->creativeRepository->byOriginalKey($path)->exists();
    }

    /**
     * @param User|\Illuminate\Contracts\Auth\Authenticatable $user
     * @param string                                          $path
     *
     * @return bool
     */
    public function hasUserAccessToCreativePath(User $user, string $path): bool
    {
        $inputParts = explode('/', ltrim($path, '/'));
        $inputUserFolder = Arr::get($inputParts, self::USER_FOLDER_INDEX);

        $expectedPath = $this->getCreativePath($user);
        $expectedParts = explode('/', $expectedPath);
        $expectedUserFolder = Arr::get($expectedParts, self::USER_FOLDER_INDEX);

        $userFolderEquals = $inputUserFolder === $expectedUserFolder;

        $inputCreativeFolder = Arr::get($inputParts, self::CREATIVES_FOLDER_INDEX);
        $expectedCreativeFolder = Arr::get($expectedParts, self::CREATIVES_FOLDER_INDEX);
        $creativeFolderEquals = $inputCreativeFolder === $expectedCreativeFolder;

        return $userFolderEquals && $creativeFolderEquals;
    }

    /**
     * @param User|\Illuminate\Contracts\Auth\Authenticatable $user
     *
     * @return string
     */
    public function getCreativePath(User $user): string
    {
        return sprintf('%s/creatives/%s', $user->getUserFolder(), Str::uuid()->toString());
    }

    /**
     * @param User|\Illuminate\Contracts\Auth\Authenticatable $user
     * @param string                                          $extension
     *
     * @return string
     */
    public function getCreativePreviewPath(User $user, string $extension): string
    {
        return sprintf('%s/images/%s.%s', $user->getUserFolder(), Str::uuid()->toString(), $extension);
    }
}
