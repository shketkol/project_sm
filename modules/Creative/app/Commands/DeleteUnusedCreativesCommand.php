<?php

namespace Modules\Creative\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Creative\Actions\DeleteCreativeAction;
use Modules\Creative\Notifications\Advertiser\Removed;
use Modules\Creative\Models\Creative;
use Modules\Creative\Repositories\Contracts\CreativeRepository;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class DeleteUnusedCreativesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'platform:creative:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete creatives which unused more then 30 days (days can be changed in config)';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DeleteCreativeAction
     */
    private $deleteAction;

    /**
     * @var CreativeRepository
     */
    private $repository;

    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * Fields that would be shown in logs.
     *
     * @var array
     */
    private $logFields = ['id', 'external_id', 'user_id', 'original_key', 'updated_at'];

    /**
     * DeleteUnusedCreatives constructor.
     *
     * @param LoggerInterface      $log
     * @param DeleteCreativeAction $deleteAction
     * @param CreativeRepository   $repository
     * @param Dispatcher           $notification
     */
    public function __construct(
        LoggerInterface $log,
        DeleteCreativeAction $deleteAction,
        CreativeRepository $repository,
        Dispatcher $notification
    ) {
        parent::__construct();
        $this->deleteAction = $deleteAction;
        $this->repository = $repository;
        $this->logger = $log;
        $this->notification = $notification;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->logger->info('Started delete unused creatives (command)');
        $this->info('Started delete unused creatives.');

        /** @var \Illuminate\Database\Eloquent\Collection $creatives */
        $creatives = $this->repository->byUnusedCriteria()->get();

        $this->logger->info('Unused creatives to delete: (command)', [
            'creatives' => $creatives->map->only($this->logFields)->toArray(),
        ]);

        $this->info(sprintf(
            'Unused creatives to delete: Count: %d. Ids: %s.',
            $creatives->count(),
            implode(', ', $creatives->pluck('id')->toArray())
        ));

        $creatives->map(function (Creative $creative) {
            $this->deleteAction->handle($creative);

            $this->sendNotification($creative->user, $creative);
        });

        $this->info('Done.');
        $this->logger->info('Finished delete unused creatives (command)');
    }

    /**
     * Send notification that Creative was removed.
     *
     * @param User     $user
     * @param Creative $creative
     *
     * @return void
     */
    protected function sendNotification(User $user, Creative $creative): void
    {
        $this->notification->send($user, new Removed($creative->name));
    }
}
