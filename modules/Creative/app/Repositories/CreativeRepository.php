<?php

namespace Modules\Creative\Repositories;

use App\Repositories\Repository;
use Modules\Creative\DataTable\Repositories\Criteria\DraftCreativeCriteria;
use Modules\Creative\Repositories\Criteria\GetDuplicateCriteria;
use Modules\Creative\Repositories\Criteria\GetOriginalKeysCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\OriginalKeyCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UnusedCreativesCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;
use Modules\Creative\Models\Creative;
use Modules\Creative\Repositories\Contracts\CreativeRepository as CreativeRepositoryContract;
use Modules\Creative\Repositories\Criteria\WithoutTrashedCriteria;
use Modules\User\Models\User;

class CreativeRepository extends Repository implements CreativeRepositoryContract
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Creative::class;
    }

    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot(): void
    {
        // by default we show only not deleted creatives
        $this->pushCriteria(new WithoutTrashedCriteria());
    }

    /**
     * @param string $externalId
     *
     * @return Creative
     */
    public function findByExternalId(string $externalId): ?Creative
    {
        return $this->findByField('external_id', $externalId)->first();
    }

    /**
     * @return CreativeRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byDraftStatus()
    {
        return $this->pushCriteria(new DraftCreativeCriteria());
    }

    /**
     * @param User $user
     *
     * @return CreativeRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byUser(User $user)
    {
        return $this->pushCriteria(new UserCriteria($user));
    }

    /**
     * @param string $originalKey
     *
     * @return CreativeRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byOriginalKey(string $originalKey)
    {
        return $this->pushCriteria(new OriginalKeyCriteria($originalKey));
    }

    /**
     * @return CreativeRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function byUnusedCriteria()
    {
        return $this->pushCriteria(new UnusedCreativesCriteria());
    }

    /**
     * @return CreativeRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getOriginalKeys()
    {
        return $this->pushCriteria(new GetOriginalKeysCriteria());
    }

    /**
     * @param string $hash
     * @param int    $advertiserId
     *
     * @return CreativeRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getDuplicate(string $hash, int $advertiserId)
    {
        return $this->pushCriteria(new GetDuplicateCriteria($hash, $advertiserId));
    }
}
