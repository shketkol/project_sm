<?php

namespace Modules\Creative\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CreativeRepository.
 *
 * @package namespace Modules\Creative\Repositories\Contracts;
 */
interface CreativeRepository extends RepositoryInterface
{

}
