<?php

namespace Modules\Creative\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class GetDuplicateCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $hash;

    /**
     * @var string
     */
    protected $advertiserId;

    /**
     * GetDuplicateCriteria constructor.
     * @param string $hash
     */
    public function __construct(string $hash, int $advertiserId)
    {
        $this->hash = $hash;
        $this->advertiserId = $advertiserId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where([['hash', '=', $this->hash], ['user_id', '=', $this->advertiserId]]);
    }
}
