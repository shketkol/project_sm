<?php

namespace Modules\Creative\Policies;

use App\Policies\Traits\Checks;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Repositories\CreativeRepository;
use Modules\User\Models\User;

class CreativePolicy
{
    use HandlesAuthorization, Checks;

    /**
     * Permissions.
     */
    public const PERMISSION_CREATE_CREATIVE          = 'create_creative';
    public const PERMISSION_DELETE_CREATIVE          = 'delete_creative';
    public const PERMISSION_LIST_CREATIVE            = 'list_creative';
    public const PERMISSION_LIST_ADVERTISER_CREATIVE = 'list_advertiser_creative';
    public const PERMISSION_UPDATE_CREATIVE          = 'update_creative';
    public const PERMISSION_VIEW_CREATIVE            = 'view_creative';

    /**
     * @var CreativeRepository
     */
    private $creativeRepository;

    /**
     * @param CreativeRepository $repository
     */
    public function __construct(CreativeRepository $repository)
    {
        $this->creativeRepository = $repository;
    }

    /**
     * Returns true if user can view list of all creatives.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_CREATIVE);
    }

    /**
     * Returns true if user can view list creatives of given advertiser.
     *
     * @param User $user
     * @param User $advertiser
     *
     * @return bool
     */
    public function listAdvertiserCreatives(User $user, ?User $advertiser): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_ADVERTISER_CREATIVE) &&
               $this->hasAccessToCreatives($user, $advertiser);
    }

    /**
     * Returns true if user can create a new creative.
     *
     * @param User $user
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function create(User $user): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_CREATE_CREATIVE)) {
            return false;
        }

        return $this->placeDraft($user);
    }

    /**
     * Returns true if user not reached campaign draft limit.
     *
     * @param User $user
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function placeDraft(User $user): bool
    {
        $countDraft = $this->creativeRepository
            ->byDraftStatus()
            ->byUser($user)
            ->count();

        return $countDraft < config('rules.creative.policies.max_draft', 10);
    }

    /**
     * Returns true if user can view the creative.
     *
     * @param User     $user
     * @param Creative $creative
     *
     * @return bool
     */
    public function view(User $user, Creative $creative): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_VIEW_CREATIVE)) {
            return false;
        }

        if (!$this->hasAccessToCreative($user, $creative)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update the creative.
     *
     * @param User     $user
     * @param Creative $creative
     *
     * @return bool
     */
    public function update(User $user, Creative $creative): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_CREATIVE) && !$user->isDeactivated()) {
            return false;
        }

        if (!$this->hasAccessToCreative($user, $creative)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can delete the creative.
     *
     * @param User     $user
     * @param Creative $creative
     *
     * @return bool
     */
    public function delete(User $user, Creative $creative): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_DELETE_CREATIVE)) {
            return false;
        }

        if (!$this->hasAccessToCreative($user, $creative)) {
            return false;
        }

        if ($creative->status_id === CreativeStatus::ID_DRAFT && $this->allCampaignsInDraft($creative)) {
            return true;
        }

        if ($creative->status_id !== CreativeStatus::ID_REJECTED || $creative->getExternalId()) {
            return false;
        }

        return true;
    }

    /**
     * Check if creative has any campaign not in DRAFT
     * @param $creative
     * @return mixed
     */
    private function allCampaignsInDraft(Creative $creative): bool
    {
        return !$creative->campaigns()
            ->where('status_id', '!=', CampaignStatus::ID_DRAFT)
            ->exists();
    }

    /**
     * @param User     $user
     * @param Creative $creative
     *
     * @return bool
     */
    private function hasAccessToCreative(User $user, Creative $creative): bool
    {
        if ($this->isAdmin($user)) {
            return true;
        }

        if ($creative->user_id === $user->id) {
            return true;
        }

        return false;
    }

    /**
     * @param User      $user
     * @param User|null $advertiser
     *
     * @return bool
     */
    private function hasAccessToCreatives(User $user, ?User $advertiser): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->is($advertiser)) {
            return true;
        }

        return false;
    }
}
