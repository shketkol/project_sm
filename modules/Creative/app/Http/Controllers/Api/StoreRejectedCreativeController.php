<?php

namespace Modules\Creative\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Creative\Actions\StoreRejectedCreativeAction;
use Modules\Creative\Http\Requests\StoreRejectedCreativeRequest;
use Modules\Creative\Http\Resources\CreativeResource;
use Modules\Haapi\Exceptions\ForbiddenException;

class StoreRejectedCreativeController extends Controller
{
    /**
     * @param StoreRejectedCreativeAction  $action
     * @param StoreRejectedCreativeRequest $request
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function __invoke(
        StoreRejectedCreativeAction $action,
        StoreRejectedCreativeRequest $request
    ): JsonResponse {
        if (!$request->allowed()) {
            throw new ForbiddenException();
        }

        $creative = $action->handle($request->toData());
        return (new CreativeResource($creative))->response();
    }
}
