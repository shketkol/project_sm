<?php

namespace Modules\Creative\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;

class GetServerTimeController extends Controller
{
    /**
     * @return Response
     */
    public function __invoke(): Response
    {
        return response(Carbon::now()->toRfc1123String());
    }
}
