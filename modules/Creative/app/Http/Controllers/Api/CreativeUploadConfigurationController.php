<?php

namespace Modules\Creative\Http\Controllers\Api;

use App\Helpers\EnvironmentHelper;
use App\Http\Controllers\Controller;
use App\Services\CredentialsService\AwsCredentials;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Carbon;
use Modules\Creative\Actions\GetCreativeUploadPathAction;
use Modules\Creative\Http\Requests\CheckDuplicateRequest;
use Modules\Creative\Http\Resources\CreativeUploadConfigurationResource;

class CreativeUploadConfigurationController extends Controller
{
    /**
     * @var \Aws\Credentials\Credentials
     */
    private $credentials;

    /**
     * AwsSignatureController constructor.
     *
     * @param AwsCredentials $awsCredentials
     */
    public function __construct(AwsCredentials $awsCredentials)
    {
        $this->credentials = $awsCredentials->get();
    }

    /**
     * @param Authenticatable $user
     * @param GetCreativeUploadPathAction $action
     * @param CheckDuplicateRequest $request
     * @return CreativeUploadConfigurationResource
     */
    public function __invoke(
        Authenticatable $user,
        GetCreativeUploadPathAction $action,
        CheckDuplicateRequest $request
    ): CreativeUploadConfigurationResource {
        return new CreativeUploadConfigurationResource([
            'awsAccessKey'     => $this->credentials->getAccessKeyId(),
            'awsSecurityKey'   => $this->credentials->getSecurityToken(),
            'authUrl'          => route('api.creatives.aws.auth'),
            's3Bucket'         => config('filesystems.disks.s3.bucket'),
            'awsDefaultRegion' => config('filesystems.disks.s3.region'),
            'cacheHours'       => config('aws.js.cache_hours'),
            'logEnabled'       => !EnvironmentHelper::isClosedEnv(),
            'uploadPath'       => $action->handle($user),
            'maxUploadRetries' => config('rules.creative.max_upload_retries'),
            'serverTimestamp'  => Carbon::now()->timestamp,
            'timeUrl'          => route('api.creatives.server-time'),
        ]);
    }
}
