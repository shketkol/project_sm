<?php

namespace Modules\Creative\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Creative\Actions\DeleteCreativeAction;
use Modules\Creative\Models\Creative;
use Psr\Log\LoggerInterface;

/**
 * Class DeleteCreativeController
 *
 * @package Modules\Creative\Http\Controllers
 */
class DeleteCreativeController extends Controller
{
    /**
     * @param Creative $creative
     * @param DeleteCreativeAction $action
     * @param LoggerInterface $log
     * @return Response
     * @throws \Modules\Creative\Exceptions\CreativeNotDeletedException
     */
    public function __invoke(Creative $creative, DeleteCreativeAction $action, LoggerInterface $log): Response
    {
        $log->info('Started deleting creative.');
        $action->handle($creative);

        return response(__('creative::messages.delete_file_success'));
    }
}
