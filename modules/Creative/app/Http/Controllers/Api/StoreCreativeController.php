<?php

namespace Modules\Creative\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Creative\Actions\Contracts\StoreCreativeAction;
use Modules\Creative\Actions\Contracts\ValidateCreativeAction;
use Modules\Creative\Actions\CreatePreviewAction;
use Modules\Creative\Http\Resources\CreativeResource;
use Modules\Creative\Http\Requests\StoreCreativeRequest;
use Modules\Haapi\Exceptions\ForbiddenException;
use Psr\Log\LoggerInterface;

class StoreCreativeController extends Controller
{
    /**
     * Previous $request validation, validation after parse metadata params
     * Store creative instance to DB
     *
     * @param StoreCreativeRequest   $request
     * @param StoreCreativeAction    $storeCreativeAction
     * @param CreatePreviewAction    $createPreviewAction
     * @param LoggerInterface        $log
     * @param ValidateCreativeAction $validateCreativeAction
     *
     * @return CreativeResource
     * @throws ForbiddenException
     * @throws \Mhor\MediaInfo\Exception\UnknownTrackTypeException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        StoreCreativeRequest $request,
        StoreCreativeAction $storeCreativeAction,
        CreatePreviewAction $createPreviewAction,
        LoggerInterface $log,
        ValidateCreativeAction $validateCreativeAction
    ): CreativeResource {
        $log->info('Started validate StoreCreativeRequest.');

        if (!$request->allowed()) {
            throw new ForbiddenException();
        }

        $request->validated();

        $log->info('Started handle video metadata.');
        $request->handle();

        $log->info('Started video metadata validation.');
        $validateCreativeAction->handle();
        $log->info('Started preview creative');
        $preview = $createPreviewAction->handle($request->s3Key);

        $log->info('Started storing creative.');
        $creative = $storeCreativeAction->handle($preview);

        $log->info('Finished storing creative.');

        return new CreativeResource($creative);
    }
}
