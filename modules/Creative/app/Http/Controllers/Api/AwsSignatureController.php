<?php

namespace Modules\Creative\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CredentialsService\AwsCredentials;
use Illuminate\Http\Response;
use Illuminate\Validation\UnauthorizedException;
use Modules\Creative\Actions\Contracts\AwsSignatureAction;
use Modules\Creative\Http\Requests\AwsSignatureRequest;
use Psr\Log\LoggerInterface;

/**
 * Class AwsSignatureController
 *
 * @package Modules\Creative\Http\Controllers
 */
class AwsSignatureController extends Controller
{
    private $credentials;

    /**
     * AwsSignatureController constructor.
     *
     * @param LoggerInterface $log
     */
    public function __construct(AwsCredentials $awsCredentials)
    {
        $this->credentials = $awsCredentials->get();
    }

    /**
     * Return response for aws authorization
     *
     * @param AwsSignatureRequest $request
     * @param AwsSignatureAction $authAction
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function __invoke(AwsSignatureRequest $request, AwsSignatureAction $authAction): Response
    {
        $request->validated();

        if ($request->publicKeyId !== $this->credentials->getAccessKeyId()) {
            return response('', 403);
        }
        $signature = $authAction->handle();

        return response($signature);
    }
}
