<?php

namespace Modules\Creative\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Creative\Http\Resources\CreativeDetailsResource;
use Modules\Creative\Models\Creative;

/**
 * Class ShowCreativeController
 * @package Modules\Creative\Http\Controllers\Api
 */
class ShowCreativeController extends Controller
{
    /**
     * Show creative details
     * @param \Modules\Creative\Models\Creative $creative
     * @return \Modules\Creative\Http\Resources\CreativeDetailsResource
     */
    public function __invoke(Creative $creative): CreativeDetailsResource
    {
        return new CreativeDetailsResource($creative);
    }
}
