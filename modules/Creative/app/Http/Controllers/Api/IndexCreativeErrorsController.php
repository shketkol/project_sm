<?php

namespace Modules\Creative\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Creative\Actions\IndexErrorsAction;
use Modules\Creative\Models\Creative;
use Psr\Log\LoggerInterface;

/**
 * Class IndexCreativeErrorsController
 *
 * @package Modules\Creative\Http\Controllers\Api
 */
class IndexCreativeErrorsController extends Controller
{
    /**
     * @param Creative $creative
     * @param IndexErrorsAction $action
     * @param LoggerInterface $log
     * @return Response
     * @throws \Exception
     */
    public function __invoke(Creative $creative, IndexErrorsAction $action, LoggerInterface $log): Response
    {
        $log->info('Started index errors for creative.');
        return response($action->handle($creative));
    }
}
