<?php

namespace Modules\Creative\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;

/**
 * Class IndexCreativeController
 *
 * @package Modules\Creative\Http\Controllers
 */
class IndexCreativeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function __invoke(): View
    {
        return view('creative::index');
    }
}
