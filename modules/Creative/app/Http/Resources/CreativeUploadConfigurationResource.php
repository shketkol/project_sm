<?php

namespace Modules\Creative\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CreativeUploadConfigurationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return $this->resource;
    }
}
