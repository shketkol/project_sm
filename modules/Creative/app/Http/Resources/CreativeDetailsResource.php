<?php

namespace Modules\Creative\Http\Resources;

use Illuminate\Contracts\Auth\Guard;

class CreativeDetailsResource extends CreativeResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $result = parent::toArray($request);

        /** @var \Modules\User\Models\User $user */
        $user = app(Guard::class)->user();

        return [
            'permissions' => [
                'canDelete' =>  $user->can('creative.delete', $this->resource),
            ]
        ] + $result;
    }
}
