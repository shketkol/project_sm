<?php

namespace Modules\Creative\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Creative\Models\Creative;

/**
 * @mixin Creative
 * @property Creative $resource
 */
class CreativeResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'              => $this->getKey(),
            'name'            => $this->name,
            'extension'       => $this->extension,
            'duration'        => $this->duration,
            'width'           => $this->width,
            'height'          => $this->height,
            'file_size'       => $this->filesize,
            'preview_url'     => $this->resource->generateDownloadUrlForAdvertiser(),
            'poster_key'      => $this->resource->generatePosterUrl('medium'),
            'original_key'    => $this->original_key,
            'creative_status' => $this->status->name,
            'upload_date'     => $this->created_at,
            'is_pro_res'      => $this->is_pro_res,
        ];
    }
}
