<?php

namespace Modules\Creative\Http\Requests;

use App\Helpers\MediaHelper;
use App\Services\ValidationRulesService\ValidationRules;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Creative\Http\Requests\Traits\CheckAllowedCampaign;
use Modules\Creative\Models\CreativeStatus;

class StoreRejectedCreativeRequest extends FormRequest
{
    use CheckAllowedCampaign;

    /**
     * Get the validation rules that apply to the request.
     *
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'file.name'      => $validationRules->only(
                'creative.basic.name',
                ['required', 'string', 'max']
            ),
            'file.extension' => $validationRules->only(
                'creative.basic.extension',
                ['required', 'string', 'max']
            ),
            'file.duration'  => $validationRules->only(
                'creative.general.duration',
                ['required', 'numeric']
            ),
            'file.width'     => $validationRules->only(
                'creative.video.width',
                ['required', 'numeric']
            ),
            'file.height'    => $validationRules->only(
                'creative.video.height',
                ['required', 'numeric']
            ),
            'file.fileSize'  => $validationRules->only(
                'creative.general.file_size',
                ['required', 'numeric']
            ),
            'file.hashSum'   => $validationRules->only(
                'creative.basic.hashSum',
                ['required', 'string', 'max']
            ),
            'errors'         => $validationRules->only(
                'creative.basic.errors',
                ['required', 'array', 'min', 'max']
            ),
            'errors.*'       => $validationRules->only(
                'creative.basic.errors',
                ['required', 'array', 'min', 'max']
            ),
            'rawData'        => $validationRules->only(
                'creative.basic.rawParams',
                ['required', 'array', 'min', 'max']
            ),
            'campaign_id'    => $validationRules->only(
                'campaign.id',
                ['required', 'integer', 'max', 'exists']
            ),
        ];
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        return [
            'creative'    => [
                'name'       => substr(MediaHelper::cutExtension($this->input('file.name')), 0, 48),
                'duration'   => $this->input('file.duration'),
                'extension'  => $this->input('file.extension'),
                'width'      => $this->input('file.width'),
                'height'     => $this->input('file.height'),
                'filesize'   => $this->input('file.fileSize'),
                'is_pro_res' => false,
                'user_id'    => $this->user()->getKey(),
                'hash'       => $this->input('file.hashSum'),
                'status_id'  => CreativeStatus::ID_REJECTED,
            ],
            'errors'      => $this->input('errors'),
            'rawData'     => $this->input('rawData'),
            'campaign_id' => $this->input('campaign_id'),
        ];
    }
}
