<?php

namespace Modules\Creative\Http\Requests\Traits;

use Illuminate\Http\Request;
use Modules\Campaign\Repositories\CampaignRepository;

/**
 * @mixin Request
 */
trait CheckAllowedCampaign
{
    /**
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function allowed(): bool
    {
        /** @var CampaignRepository $repository */
        $repository = app(CampaignRepository::class);
        $campaign = $repository->findSoft($this->input('campaign_id'));

        if (!$campaign) {
            return false;
        }

        return $this->user()->can('campaign.manageCampaignCreative', $campaign);
    }
}
