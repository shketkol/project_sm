<?php

namespace Modules\Creative\Http\Requests;

use App\Services\ValidationRulesService\ValidationRules;
use Illuminate\Foundation\Http\FormRequest;

class AwsSignatureRequest extends FormRequest
{
    /**
     * @var ValidationRules
     */
    private $validationRules;

    /**
     * AwsSignatureRequest constructor.
     *
     * @param ValidationRules $validationRules
     */
    public function __construct(ValidationRules $validationRules)
    {
        parent::__construct();
        $this->validationRules = $validationRules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'to_sign'     => ['bail', 'required'],
            'datetime'    => ['bail', 'required'],
            'publicKeyId' => $this->validationRules->only(
                'creative.basic.s3Key',
                ['required', 'string', 'max']
            ),
            'canonical_request' => $this->validationRules->only(
                'creative.basic.canonical_request',
                ['required', 'string', 'max']
            ),
        ];
    }
}
