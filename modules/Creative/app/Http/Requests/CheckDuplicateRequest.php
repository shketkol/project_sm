<?php

namespace Modules\Creative\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\ValidationRules;

class CheckDuplicateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param ValidationRules $validationRules
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'hashSum' => $validationRules->only(
                'creative.basic.hashSum',
                ['nullable', 'string', 'unique_creative', 'max']
            ),
        ];
    }
}
