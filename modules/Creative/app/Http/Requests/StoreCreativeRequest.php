<?php

namespace Modules\Creative\Http\Requests;

use App\Helpers\MediaHelper;
use App\Services\ValidationRulesService\ValidationRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Modules\Creative\Actions\ParseCreativeAction;
use Modules\Creative\Http\Requests\Traits\CheckAllowedCampaign;
use Modules\Creative\Models\CreativeStatus;

/**
 * @property string   $s3Key
 * @property string   $name
 * @property string   $hashSum
 * @property int      $video_cnt
 * @property int      $audio_cnt
 * @property string   $extension
 * @property int      $file_size
 * @property float    $duration
 * @property string   $video_format
 * @property string   $codec
 * @property int      $bit_rate
 * @property int      $width
 * @property int      $height
 * @property string   $display_aspect_ratio
 * @property string   $frame_rate_mode
 * @property float    $frame_rate
 * @property string   $color_space
 * @property array    $chroma_subsampling
 * @property int|null $bit_depth
 * @property string   $scan_type
 * @property array    $audios
 * @property int      $campaign_id
 */
class StoreCreativeRequest extends FormRequest
{
    use CheckAllowedCampaign;

    /**
     * Get the validation rules that apply to the request.
     *
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'name'        => $validationRules->only(
                'creative.basic.name',
                ['required', 'string', 'max']
            ),
            's3Key'       => $validationRules->only(
                'creative.basic.s3Key',
                ['required', 'string', 'max']
            ),
            'hashSum'     => $validationRules->only(
                'creative.basic.hashSum',
                ['required', 'string', 'max']
            ),
            'campaign_id' => $validationRules->only(
                'campaign.id',
                ['required', 'integer', 'max', 'exists']
            ),
        ];
    }

    /**
     * Retrieve base video metadata
     *
     * @return StoreCreativeRequest
     * @throws \Mhor\MediaInfo\Exception\UnknownTrackTypeException
     */
    public function handle(): self
    {
        /** @var ParseCreativeAction $action */
        $action = app('creative.action.parse');
        return $this->merge($action->handle(Storage::url($this->s3Key)));
    }

    /**
     * @return array
     */
    public function toData(): array
    {
        return [
            'name'         => substr(MediaHelper::cutExtension($this->name), 0, 48),
            'hash'         => $this->hashSum,
            'user_id'      => $this->user()->id,
            'status_id'    => CreativeStatus::ID_DRAFT,
            'extension'    => $this->extension,
            'duration'     => (double)$this->duration,
            'width'        => (int)$this->width,
            'height'       => (int)$this->height,
            'filesize'     => (double)$this->file_size,
            'original_key' => $this->s3Key,
            'codec'        => $this->codec,
            'campaign_id'  => $this->campaign_id,
        ];
    }
}
