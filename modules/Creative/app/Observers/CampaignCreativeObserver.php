<?php

namespace Modules\Creative\Observers;

use Modules\Creative\Models\Pivot\CampaignCreative;
use Psr\Log\LoggerInterface;

class CampaignCreativeObserver
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * CampaignCreativeObserver constructor.
     *
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * Handle the CampaignCreative "creating" event.
     *
     * @param CampaignCreative $pivot
     *
     * @return void
     * @throws \Exception
     */
    public function creating(CampaignCreative $pivot): void
    {
        $this->deleteActiveCreative($pivot);

        $this->log->info('Set active creative to campaign on create relation.', [
            'creative' => $pivot->creative_id,
            'campaign' => $pivot->campaign_id,
        ]);
    }

    /**
     * Handle the CampaignCreative "updating" event.
     *
     * @param CampaignCreative $pivot
     *
     * @return void
     * @throws \Exception
     */
    public function updating(CampaignCreative $pivot): void
    {
        $this->deleteActiveCreative($pivot);

        $this->log->info('Set active creative to campaign on update relation.', [
            'creative' => $pivot->creative_id,
            'campaign' => $pivot->campaign_id,
        ]);
    }

    /**
     * Delete active creative from campaign so it will be always only one active creative.
     *
     * @param CampaignCreative $pivot
     *
     * @throws \Exception
     */
    private function deleteActiveCreative(CampaignCreative $pivot): void
    {
        $activeCreative = $pivot->campaign->getActiveCreative();

        if (is_null($activeCreative)) {
            return;
        }

        $this->log->info('Deleting currently active creative from campaign.', [
            'creative' => $activeCreative->getKey(),
            'campaign' => $pivot->campaign->getKey(),
        ]);

        $pivot->campaign->creatives()->detach($activeCreative->getKey());

        $this->log->info('Active creative deleted from campaign.', [
            'creative' => $activeCreative->getKey(),
            'campaign' => $pivot->campaign->getKey(),
        ]);
    }
}
