<?php

namespace Modules\Creative\Actions;

use App\Services\SignatureService\Builders\AwsSignatureBuilder;
use Modules\Creative\Http\Requests\AwsSignatureRequest;
use Modules\Creative\Actions\Contracts\AwsSignatureAction as AwsSignatureActionContract;

/**
 * Class AwsSignatureAction
 * @package Modules\Creative\Actions
 */
class AwsSignatureAction implements AwsSignatureActionContract
{
    /**
     * @var AwsSignatureRequest
     */
    private $builder;

    /**
     * AwsSignatureAction constructor.

     * @param AwsSignatureBuilder $builder
     */
    public function __construct(AwsSignatureBuilder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * Create signature string for aws signature v.4 based on request params
     * @return string
     */
    public function handle(): string
    {
        return $this->builder->getSignature();
    }
}
