<?php

namespace Modules\Creative\Actions\Notifications;

use Illuminate\Log\Logger;
use Modules\Creative\Actions\ExternalRejectCreativeAction;
use Modules\Creative\Exceptions\CreativeNotFoundException;
use Modules\Creative\Actions\ExternalUpdateCreativeAction;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Repositories\CreativeRepository;
use Modules\Daapi\Actions\Notification\UpdateStatusBaseAction;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use SM\SMException;

class UpdateCreativeStatusAction extends UpdateStatusBaseAction
{
    /**
     * @var ExternalRejectCreativeAction
     */
    protected $rejectAction;

    /**
     * @var ExternalUpdateCreativeAction
     */
    protected $updateCreativeAction;

    /**
     * @param Logger $log
     * @param CreativeRepository $repository
     * @param ExternalRejectCreativeAction $rejectAction
     * @param ExternalUpdateCreativeAction $updateCreativeAction
     */
    public function __construct(
        Logger $log,
        CreativeRepository $repository,
        ExternalRejectCreativeAction $rejectAction,
        ExternalUpdateCreativeAction $updateCreativeAction
    ) {
        parent::__construct($log, $repository);
        $this->updateCreativeAction = $updateCreativeAction;
        $this->rejectAction = $rejectAction;
    }

    /**
     * @param string $externalId
     * @param string $status
     * @param array|null $additionalData
     *
     * @throws CanNotApplyStatusException
     * @throws CreativeNotFoundException
     * @throws SMException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Daapi\Exceptions\AdminNotFoundException
     * @throws \Throwable
     */
    public function handle(string $externalId, string $status, ?array $additionalData): void
    {
        /** @var Creative $creative */
        $creative = $this->getEntity($externalId);

        if (!$creative) {
            throw CreativeNotFoundException::create($externalId, 'change status');
        }

        $this->saveErrors($creative, $status, $additionalData);
        $this->updateCreative($creative, $status);
        $this->updateStatus($creative, $status);
    }

    /**
     * @param Creative $creative
     * @param string $status
     *
     * @throws CanNotApplyStatusException
     * @throws SMException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Daapi\Exceptions\AdminNotFoundException
     */
    private function updateStatus(Creative $creative, string $status): void
    {
        $oldStatus = $creative->status->name;

        $this->log->info('Updating creative status.', [
            'creative_id' => $creative->id,
            'from_status' => $oldStatus,
            'to_status'   => $status,
        ]);

        $creative->applyHaapiStatus($status);

        $this->log->info('Creative status was successfully updated.', [
            'creative_id' => $creative->id,
            'from_status' => $oldStatus,
            'to_status'   => $status,
        ]);
    }

    /**
     * @param Creative $creative
     * @param string $status
     * @param array $additionalData
     * @throws \Throwable
     */
    private function saveErrors(Creative $creative, string $status, array $additionalData): void
    {
        $transition = $creative->findTransition($status);
        if ($transition !== CreativeStatus::REJECTED) {
            return;
        }

        $this->rejectAction->handle($creative, $additionalData);
    }

    /**
     * @param Creative $creative
     *
     * @param string   $status
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function updateCreative(Creative $creative, string $status): void
    {
        $transition = $creative->findTransition($status);
        if ($transition !== CreativeStatus::APPROVED) {
            return;
        }

        // replace to external creative data if creative was approved
        $this->updateCreativeAction->handle($creative);
    }
}
