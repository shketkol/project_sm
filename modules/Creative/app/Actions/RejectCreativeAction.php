<?php

namespace Modules\Creative\Actions;

use App\Helpers\TranslationHelper;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\DatabaseManager;
use Modules\Creative\Actions\Traits\PrepareCreativeError;
use Modules\Creative\Exceptions\CreativeNotUpdatedException;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeError;
use Modules\Creative\Models\CreativeStatus;
use Psr\Log\LoggerInterface;
use SM\SMException;

class RejectCreativeAction
{
    use PrepareCreativeError;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var Filesystem
     */
    private $storage;

    /**
     * @param LoggerInterface $log
     * @param DatabaseManager $databaseManager
     * @param Filesystem      $storage
     */
    public function __construct(LoggerInterface $log, DatabaseManager $databaseManager, Filesystem $storage)
    {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
        $this->storage = $storage;
    }

    /**
     * @param Creative $creative
     * @param array    $errorBag
     * @param int      $campaignId
     *
     * @return void
     * @throws CreativeNotUpdatedException
     * @throws SMException
     * @throws \Throwable
     */
    public function handle(Creative $creative, array $errorBag, int $campaignId): void
    {
        $this->databaseManager->beginTransaction();
        $originalKey = $creative->original_key;

        try {
            $this->rejectCreative($creative);
            $this->saveErrors($errorBag, $creative, $campaignId);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
        $this->storage->delete($originalKey);
    }

    /**
     * @param Creative $creative
     *
     * @throws CreativeNotUpdatedException
     * @throws SMException
     */
    private function rejectCreative(Creative $creative): void
    {
        $this->log->info('Started apply "rejected" creative status.');
        $creative->getState()->apply(CreativeStatus::REJECTED);
        $creative->original_key = null;
        $this->log->info('Started saving creative with rejected status.');

        if (!$creative->save()) {
            $this->log->warning('Creative status was not changed to "rejected".', [
                'creative_id' => $creative->id,
            ]);

            throw new CreativeNotUpdatedException();
        }
    }

    /**
     * Save Errors related to creative
     *
     * @param array    $errorBag
     * @param Creative $creative
     * @param int      $campaignId
     */
    public function saveErrors(array $errorBag, Creative $creative, int $campaignId)
    {
        $this->log->info('Started saving creative errors.');
        $creative->errors()->saveMany($this->prepareErrorsArray($errorBag, $campaignId));
    }

    /**
     * @param array $errorsBag
     * @param int   $campaignId
     *
     * @return array
     */
    private function prepareErrorsArray(array $errorsBag, int $campaignId): array
    {
        $errorMessagesArray = [];
        foreach ($errorsBag as $field => $errors) {
            $error = $this->prepareBeError($field, $errors, $campaignId);
            $errorMessagesArray[] = new CreativeError($error);
        }

        return $errorMessagesArray;
    }
}
