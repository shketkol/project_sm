<?php

namespace Modules\Creative\Actions;

use Illuminate\Support\Arr;

class ProResAction
{
    /**
     * @param array $params
     *
     * @return array
     */
    public function handle(array $params): array
    {
        $codec = Arr::get($params, 'codec');

        if (is_null($codec)) {
            return $params;
        }

        Arr::set($params, 'is_pro_res', $this->isProResCodec($codec));
        Arr::forget($params, 'codec');

        return $params;
    }

    /**
     * Check if given video codec is Apple ProRes.
     *
     * @param string $codec
     *
     * @return bool
     */
    private function isProResCodec(string $codec): bool
    {
        return in_array($codec, config('codec.pro_res'));
    }
}
