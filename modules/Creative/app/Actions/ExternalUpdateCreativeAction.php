<?php

namespace Modules\Creative\Actions;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Filesystem\Filesystem;
use Modules\Creative\Models\Creative;
use Modules\Creative\Repositories\Contracts\CreativeRepository;
use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Creative\CreativeGet;
use Psr\Log\LoggerInterface;

class ExternalUpdateCreativeAction
{
    use ActAsAdmin;

    /**
     * @var CreativeRepository
     */
    private $repository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CreativeGet
     */
    private $creativeGet;

    /**
     * @var Filesystem
     */
    private $storage;

    /**
     * Set Creative Repository
     *
     * StoreCreativeAction constructor.
     *
     * @param CreativeRepository $repository
     * @param LoggerInterface    $log
     * @param CreativeGet        $creativeGet
     * @param Filesystem         $storage
     */
    public function __construct(
        CreativeRepository $repository,
        LoggerInterface $log,
        CreativeGet $creativeGet,
        Filesystem $storage
    ) {
        $this->repository = $repository;
        $this->logger = $log;
        $this->creativeGet = $creativeGet;
        $this->storage = $storage;
    }

    /**
     * @param Creative $creative
     *
     * @return void
     * @throws GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Creative $creative): void
    {
        $this->logger->info('Started replacing creative data. cause:(APPROVED)', [
            'creative_id' => $creative->external_id,
        ]);

        $originalKey = $creative->original_key;
        $creativeData = $this->creativeGet->handle($creative->external_id, $this->authAsAdmin());
        $this->repository->update($creativeData->mapToModelStructure(), $creative->getKey());

        $this->storage->delete($originalKey);
        $this->logger->info('Creative video was successfully deleted from storage.', [
            'creative_id' => $creative->external_id,
        ]);

        $this->logger->info('Creative data successfully replaced.', [
            'creative_id' => $creative->external_id,
        ]);
    }
}
