<?php

namespace Modules\Creative\Actions\Traits;

use App\Helpers\TranslationHelper;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Models\CreativeError;

trait PrepareCreativeError
{
    /**
     * @param array         $additionalData
     * @param Campaign|null $campaign
     *
     * @return array
     */
    private function prepareManualError(array $additionalData, Campaign $campaign = null): array
    {
        return [
            'subject'     => trans('creative::messages.manual_validation'),
            'field'       => CreativeError::MANUAL_VALIDATION,
            'messages'    => $this->prepareErrorMessages(Arr::get($additionalData, 'reason')),
            'campaign_id' => optional($campaign)->id,
        ];
    }

    /**
     * @param string $field
     * @param mixed  $errors
     * @param int    $campaignId
     *
     * @return array
     */
    private function prepareFeError(string $field, $errors, int $campaignId): array
    {
        return [
            'subject'     => static::FE_SUBJECT,
            'field'       => $field,
            'messages'    => $this->prepareErrorMessages($errors),
            'campaign_id' => $campaignId,
        ];
    }

    /**
     * @param string $field
     * @param mixed  $errors
     * @param int    $campaignId
     *
     * @return array
     */
    private function prepareBeError(string $field, $errors, int $campaignId): array
    {
        return [
            'subject'     => __('validation.attributes.' . TranslationHelper::multiDimensionalTranslation($field)),
            'field'       => TranslationHelper::replaceNumbers($field),
            'messages'    => $this->prepareErrorMessages($errors),
            'campaign_id' => $campaignId,
        ];
    }

    /**
     * @param mixed $errors
     *
     * @return string|null
     */
    private function prepareErrorMessages($errors): ?string
    {
        return is_null($errors) ? '[]' : json_encode(Arr::wrap($errors));
    }
}
