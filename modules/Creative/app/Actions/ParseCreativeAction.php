<?php

namespace Modules\Creative\Actions;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Arr;
use Mhor\MediaInfo\MediaInfo;
use Modules\Creative\Actions\Contracts\ParseCreativeAction as ParseCreativeActionContract;
use Modules\Creative\Exceptions\CreativeNotCreatedException;
use Modules\Creative\Exceptions\CreativeParseException;
use Modules\Creative\Helpers\CreativePathHelper;
use Psr\Log\LoggerInterface;

/**
 * Class ParseCreativeAction
 *
 * @package Modules\Creative\Actions
 */
class ParseCreativeAction implements ParseCreativeActionContract
{
    /**
     * Result array
     *
     * @var array
     */
    private $result = [];

    /**
     * Rules to mapping MediaInfo instance to readable params
     * 'file_size' => [  - mediainfo propety
     *      'request_field'     => 'filesize', key in result array
     *      'additional_method' => 'getBit', additional method to call on mediainfo instance
     *      'convert_method'    => 'toSeconds' helper format function must be declared in $this
     * ],
     *
     * @var array
     */
    private $fieldsMapping = [
        'general' => [
            'count_of_video_streams' => [
                'request_field' => 'video_cnt',
            ],

            'count_of_audio_streams' => [
                'request_field' => 'audio_cnt',
            ],

            'file_extension' => [
                'request_field' => 'extension',
            ],

            'file_size' => [
                'request_field'     => 'file_size',
                'additional_method' => 'getBit',
            ],

            'duration' => [
                'request_field'     => 'duration',
                'additional_method' => 'getMilliseconds',
                'convert_method'    => 'toSeconds',
            ],

            'format' => [
                'request_field'     => 'video_format',
                'additional_method' => 'getShortName',
            ],
        ],

        'video' => [
            'codec_id' => [
                'request_field' => 'codec',
            ],
            'width' => [
                'request_field'     => 'width',
                'additional_method' => 'getAbsoluteValue',
            ],

            'height' => [
                'request_field'     => 'height',
                'additional_method' => 'getAbsoluteValue',
            ],

            'display_aspect_ratio' => [
                'request_field'     => 'display_aspect_ratio',
                'additional_method' => 'getTextValue',
            ],

            'bit_rate' => [
                'request_field'     => 'video_bit_rate',
                'additional_method' => 'getAbsoluteValue',
            ],

            'frame_rate_mode' => [
                'request_field'     => 'frame_rate_mode',
                'additional_method' => 'getShortName',
            ],

            'frame_rate' => [
                'request_field'     => 'frame_rate',
                'additional_method' => 'getTextValue',
                'convert_method'    => 'getFirstStringPart',
            ],

            'color_space' => [
                'request_field' => 'color_space',
            ],

            'chroma_subsampling' => [
                'request_field' => 'chroma_subsampling',
            ],

            'bit_depth' => [
                'request_field'     => 'bit_depth',
                'additional_method' => 'getAbsoluteValue',
            ],

            'scan_type' => [
                'request_field'     => 'scan_type',
                'additional_method' => 'getShortName',
            ],
        ],

        'audio' => [
            'format' => [
                'request_field'     => 'format',
                'additional_method' => 'getShortName',
            ],

            'duration' => [
                'request_field'     => 'duration',
                'additional_method' => 'getMilliseconds',
                'convert_method'    => 'toSeconds',
            ],

            'bit_rate' => [
                'request_field'     => 'bit_rate',
                'additional_method' => 'getAbsoluteValue',
            ],

            'nominal_bit_rate' => [
                'request_field'     => 'bit_rate',
                'additional_method' => 'getAbsoluteValue',
            ],

            'channel_s' => [
                'request_field'     => 'channels',
                'additional_method' => 'getAbsoluteValue',
            ],

            'sampling_rate' => [
                'request_field'     => 'sampling_rate',
                'additional_method' => 'getAbsoluteValue',
            ],

            'bit_depth' => [
                'request_field'     => 'bit_depth',
                'additional_method' => 'getAbsoluteValue',
            ],
        ],
    ];

    /**
     * @var MediaInfo
     */
    private $mediaInfo;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CreativePathHelper
     */
    private $creativePathHelper;

    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private $user;

    /**
     * ParseCreativeAction constructor.
     *
     * @param MediaInfo          $mediaInfo
     * @param LoggerInterface    $log
     * @param CreativePathHelper $creativePathHelper
     * @param Guard              $auth
     */
    public function __construct(
        MediaInfo $mediaInfo,
        LoggerInterface $log,
        CreativePathHelper $creativePathHelper,
        Guard $auth
    ) {
        $this->mediaInfo = $mediaInfo;
        $this->logger = $log;
        $this->creativePathHelper = $creativePathHelper;
        $this->user = $auth->user();
    }

    /**
     * Main parse function
     *
     * @param string $videoUrl
     *
     * @return array
     * @throws \Mhor\MediaInfo\Exception\UnknownTrackTypeException
     * @throws \Exception
     */
    public function handle(string $videoUrl): array
    {
        $path = ltrim(Arr::get(parse_url($videoUrl), 'path'), '/');
        $this->checkIfUserHasAccessToVideo($path);
        $this->checkIfCreativeWithPathExists($path);

        try {
            return $this->parseCreative($videoUrl);
        } catch (\Throwable $exception) {
            $this->logger->warning('Fail parsing creative', [$exception->getMessage()]);
            throw CreativeParseException::create('Error while parsing creative.', $exception);
        }
    }

    /**
     * @param string $path
     *
     * @throws \App\Exceptions\ModelNotCreatedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function checkIfCreativeWithPathExists(string $path): void
    {
        if ($this->creativePathHelper->creativeWithPathExists($path)) {
            throw CreativeNotCreatedException::create('Creative with this path already exists.');
        }
    }

    /**
     * @param string $path
     *
     * @throws \App\Exceptions\ModelNotCreatedException
     */
    private function checkIfUserHasAccessToVideo(string $path): void
    {
        if (!$this->creativePathHelper->hasUserAccessToCreativePath($this->user, $path)) {
            throw CreativeNotCreatedException::create('User has no access to this path.');
        }
    }

    /**
     * @param string $videoUrl
     *
     * @return array
     * @throws \Mhor\MediaInfo\Exception\UnknownTrackTypeException
     */
    private function parseCreative(string $videoUrl): array
    {
        $this->logger->info('Started parse video url with mediainfo');
        $this->mediaInfo->setConfig('use_oldxml_mediainfo_output_format', true);
        $this->logger->info('Started retrieving info from video container');
        $this->mediaInfo = $this->mediaInfo->getInfo($videoUrl);
        $this->logger->info('Started parsing general info form video container');
        $this->parseGeneral();
        $this->logger->info('Started parsing video info form video container');
        $this->parseVideo();
        $this->logger->info('Started parsing audio info form video container');
        $this->parseAudios();

        return $this->result;
    }

    /**
     * Helper function to form resulted property
     *
     * @param             $source
     * @param string      $name
     * @param array       $options
     * @param string|null $scope
     *
     * @return mixed
     */
    private function formProp($source, string $name, array $options)
    {
        $prop = $source->get($name);

        if ($prop && array_key_exists('additional_method', $options)) {
            $prop = $prop->{$options['additional_method']}();
        }

        if ($prop && array_key_exists('convert_method', $options)) {
            $prop = $this->{$options['convert_method']}($prop);
        }
        return $prop;
    }

    /**
     *  Parse general mediainfo
     */
    private function parseGeneral(): void
    {
        $generals = $this->mediaInfo->getGeneral();

        foreach ($this->fieldsMapping['general'] as $name => $options) {
            if ($this->empty($this->result, $options['request_field'])) {
                $this->result[$options['request_field']] = $this->formProp($generals, $name, $options);
            }
        }
    }

    /**
     * Parse mediainfo video channel
     */
    private function parseVideo(): void
    {
        $videos = $this->mediaInfo->getVideos();

        if (!count($videos)) {
            return;
        }

        $video = $videos[0];
        foreach ($this->fieldsMapping['video'] as $name => $options) {
            if ($this->empty($this->result, $options['request_field'])) {
                $this->result[$options['request_field']] = $this->formProp($video, $name, $options);
            }
        }
    }

    /**
     * Parse mediainfo audio channels
     */
    private function parseAudios(): void
    {
        $audios = $this->mediaInfo->getAudios();

        foreach ($audios as $key => $audio) {
            $this->result['audios'][$key] = [];
            foreach ($this->fieldsMapping['audio'] as $name => $options) {
                if ($this->empty($this->result['audios'][$key], $options['request_field'])) {
                    $this->result['audios'][$key][$options['request_field']] =
                        $this->formProp($audio, $name, $options);
                }
            }
        }
    }

    /**
     * Method to detect if desired not parsed before
     *
     * @param array  $array
     * @param string $key
     *
     * @return bool
     */
    private function empty(array $array, string $key)
    {
        return !array_key_exists($key, $array) || !$array[$key];
    }

    /**
     * Helper function to convert milliseconds to seconds
     *
     * @param $milliseconds
     *
     * @return float
     */
    private function toSeconds(int $milliseconds): float
    {
        return round($milliseconds / 1000, 3);
    }

    /**
     * Helper function to get first part of string separated by spaces
     *
     * @param string $string
     *
     * @return string
     */
    private function getFirstStringPart(string $string): string
    {
        return explode(' ', $string)[0];
    }
}
