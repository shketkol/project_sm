<?php

namespace Modules\Creative\Actions;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Arr;
use Modules\Creative\Exceptions\CreativeNotCreatedException;
use Modules\Creative\Helpers\CreativePathHelper;
use Modules\Creative\Models\Creative;
use Modules\Creative\Repositories\Contracts\CreativeRepository;
use Modules\Creative\Actions\Contracts\StoreCreativeAction as StoreCreativeActionContract;
use Modules\Creative\Http\Requests\StoreCreativeRequest;
use Psr\Log\LoggerInterface;

/**
 * Class StoreCreativeAction
 *
 * @package Modules\Creative\Actions
 */
class StoreCreativeAction implements StoreCreativeActionContract
{
    /**
     * @var CreativeRepository
     */
    private $repository;

    /**
     * @var StoreCreativeRequest
     */
    private $request;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CreativePathHelper
     */
    private $creativePathHelper;

    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private $user;

    /**
     * @var ProResAction
     */
    private $proResAction;

    /**
     * @param CreativeRepository   $repository
     * @param StoreCreativeRequest $request
     * @param LoggerInterface      $log
     * @param CreativePathHelper   $creativePathHelper
     * @param Guard                $auth
     * @param ProResAction         $proResAction
     */
    public function __construct(
        CreativeRepository $repository,
        StoreCreativeRequest $request,
        LoggerInterface $log,
        CreativePathHelper $creativePathHelper,
        Guard $auth,
        ProResAction $proResAction
    ) {
        $this->repository = $repository;
        $this->request = $request;
        $this->logger = $log;
        $this->creativePathHelper = $creativePathHelper;
        $this->user = $auth->user();
        $this->proResAction = $proResAction;
    }

    /**
     * @param array $additionalParams
     *
     * @return Creative
     * @throws CreativeNotCreatedException
     * @throws \App\Exceptions\ModelNotCreatedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(array $additionalParams = []): Creative
    {
        $params = array_merge($this->request->toData(), $additionalParams);

        $path = Arr::get($params, 'original_key');
        $this->checkIfUserHasAccessToFolder($path);
        $this->checkIfCreativeWithPathExists($path);
        $params = $this->proResAction->handle($params);

        $this->logger->debug('Creative params', $params);
        $creative = $this->repository->create($params);

        if (!$creative) {
            $this->logger->warning('Creative was not stored');
            throw new CreativeNotCreatedException();
        }

        return $creative;
    }

    /**
     * @param string $path
     *
     * @throws \App\Exceptions\ModelNotCreatedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function checkIfCreativeWithPathExists(string $path): void
    {
        if ($this->creativePathHelper->creativeWithPathExists($path)) {
            throw CreativeNotCreatedException::create('Creative with this path already exists.');
        }
    }

    /**
     * @param string $path
     *
     * @throws \App\Exceptions\ModelNotCreatedException
     */
    protected function checkIfUserHasAccessToFolder(string $path): void
    {
        if (!$this->creativePathHelper->hasUserAccessToCreativePath($this->user, $path)) {
            throw CreativeNotCreatedException::create('User has no access to this path.');
        }
    }
}
