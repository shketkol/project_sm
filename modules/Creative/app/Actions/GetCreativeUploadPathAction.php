<?php

namespace Modules\Creative\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Creative\Helpers\CreativePathHelper;
use Modules\User\Models\User;

class GetCreativeUploadPathAction
{
    /**
     * @var CreativePathHelper
     */
    private $creativePathHelper;

    /**
     * @param CreativePathHelper $creativePathHelper
     */
    public function __construct(CreativePathHelper $creativePathHelper)
    {
        $this->creativePathHelper = $creativePathHelper;
    }

    /**
     * @param Authenticatable|User $user
     *
     * @return string
     */
    public function handle(Authenticatable $user): string
    {
        return $this->creativePathHelper->getCreativePath($user);
    }
}
