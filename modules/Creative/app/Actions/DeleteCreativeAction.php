<?php

namespace Modules\Creative\Actions;

use Illuminate\Contracts\Filesystem\Filesystem;
use Modules\Creative\Exceptions\CreativeNotDeletedException;
use Modules\Creative\Models\Creative;
use Psr\Log\LoggerInterface;

class DeleteCreativeAction
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Filesystem
     */
    private $storage;

    /**
     * @param LoggerInterface $log
     * @param Filesystem      $storage
     */
    public function __construct(LoggerInterface $log, Filesystem $storage)
    {
        $this->logger = $log;
        $this->storage = $storage;
    }

    /**
     * @param Creative $creative
     *
     * @return void
     * @throws CreativeNotDeletedException
     * @throws \Exception
     */
    public function handle(Creative $creative): void
    {
        $originalKey = $creative->original_key ? $creative->original_key : null;

        if (!$creative->delete()) {
            $this->logger->warning('Creative was not deleted');
            throw new CreativeNotDeletedException();
        }

        if ($originalKey) {
            $this->storage->delete($originalKey);
            $this->logger->info('Creative was deleted from the storage.', [
                'original_key' => $originalKey,
            ]);
        }
    }
}
