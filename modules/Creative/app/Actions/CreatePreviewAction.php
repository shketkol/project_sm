<?php

namespace Modules\Creative\Actions;

use App\Helpers\MediaHelper;
use App\Services\MediaService\Contracts\VideoDriver;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Filesystem\Filesystem;
use Intervention\Image\Facades\Image;
use Modules\Creative\Helpers\CreativePathHelper;
use Psr\Log\LoggerInterface;

class CreatePreviewAction
{
    /**
     * @var mixed
     */
    private $driver;

    /**
     * Main image instance
     *
     * @var \Intervention\Image\Image
     */
    private $image;

    /**
     * Media config for images
     *
     * @var array
     */
    private $config;

    /**
     * Poser name
     *
     * @var string
     */
    private $name;

    /**
     * @var Filesystem
     */
    private $storage;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * CreatePreviewAction constructor.
     *
     * @param LoggerInterface    $log
     * @param VideoDriver        $driver
     * @param Filesystem         $storage
     * @param Guard              $auth
     * @param CreativePathHelper $creativePathHelper
     */
    public function __construct(
        LoggerInterface $log,
        VideoDriver $driver,
        Filesystem $storage,
        Guard $auth,
        CreativePathHelper $creativePathHelper
    ) {
        $this->driver = $driver;
        $this->config = config('media.images');
        $this->name = $creativePathHelper->getCreativePreviewPath($auth->user(), $this->config['default_extension']);
        $this->storage = $storage;
        $this->log = $log;
    }

    /**
     * @param string $s3Key
     *
     * @return array
     */
    public function handle(string $s3Key): array
    {
        $this->image = $this->getImage($s3Key);
        $this->saveOriginal();
        $this->savePreviews();

        return [
            'original_key' => $s3Key,
            'preview_url'  => $this->storage->url($s3Key),
            'poster_key'   => $this->name,
        ];
    }

    /**
     * Save different sized previews.
     */
    private function savePreviews(): void
    {
        foreach ($this->config['variants'] as $key => $variant) {
            $this->storage->put(
                MediaHelper::imgVariant($this->name, $key),
                $this->resize($variant)
            );
        }
    }

    /**
     * @param string $s3Key
     *
     * @return \Intervention\Image\Image
     */
    private function getImage(string $s3Key): \Intervention\Image\Image
    {
        $this->log->info('Started making frame from video.');

        $this->driver->createInstance($this->storage->url($s3Key));
        $this->driver->makeFrame();
        $image = $this->driver->saveFrame();

        $this->log->info('Finished making frame from video.');

        return Image::make(base64_decode($image));
    }

    /**
     * Resize all variants defined in media conf for image
     *
     * @param array|null $variant
     *
     * @return mixed
     */
    private function resize(?array $variant = null)
    {
        return $variant ?
            $this->image->resize($variant['width'], null, function ($constraint) {
                $constraint->aspectRatio();
            })->stream() :
            $this->image->stream($this->config['default_extension'], $this->config['quality']);
    }

    /**
     * Save original poster
     */
    private function saveOriginal(): void
    {
        $this->storage->put(
            $this->name,
            $this->image->stream()
        );
    }
}
