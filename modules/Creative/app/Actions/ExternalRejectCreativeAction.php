<?php

namespace Modules\Creative\Actions;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Creative\Actions\Traits\PrepareCreativeError;
use Modules\Creative\Exceptions\CreativeErrorsNotUpdatedException;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeError;
use Psr\Log\LoggerInterface;

class ExternalRejectCreativeAction
{
    use PrepareCreativeError;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var Filesystem
     */
    private $storage;

    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * @param LoggerInterface    $log
     * @param Filesystem         $storage
     * @param CampaignRepository $campaignRepository
     */
    public function __construct(
        LoggerInterface $log,
        Filesystem $storage,
        CampaignRepository $campaignRepository
    ) {
        $this->log = $log;
        $this->storage = $storage;
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * @param Creative $creative
     * @param array    $additionalData
     *
     * @return void
     *
     * @throws CreativeErrorsNotUpdatedException
     * @throws \Exception
     */
    public function handle(Creative $creative, array $additionalData): void
    {
        $this->log->info('Started saving external creative errors.', [
            'creative_id' => $creative->id,
            'external_id' => $creative->external_id,
        ]);

        $originalKey = $creative->original_key;

        try {
            $this->removeVideoData($creative);
            $campaigns = $this->findCampaigns($creative);
            $this->storeErrorForEachCampaign($campaigns, $creative, $additionalData);
        } catch (\Throwable $exception) {
            $this->log->error($exception->getMessage());
            throw new CreativeErrorsNotUpdatedException($creative, $additionalData);
        }

        $this->storage->delete($originalKey);

        $this->log->info('Rejection reason was successfully saved to creative.', [
            'creative_id' => $creative->id,
            'external_id' => $creative->external_id,
        ]);
    }

    /**
     * @param Campaign[]|Collection $campaigns
     * @param Creative              $creative
     * @param array                 $additionalData
     */
    private function storeErrorForEachCampaign(Collection $campaigns, Creative $creative, array $additionalData): void
    {
        if ($campaigns->isEmpty()) {
            $this->log->info('Manually rejected creative does not have any campaign attached to it anymore.', [
                'creative_id' => $creative->id,
                'external_id' => $creative->external_id,
            ]);
            $errorData = $this->prepareManualError($additionalData);
            $this->createCreativeError($creative, $errorData);
            return;
        }

        foreach ($campaigns as $campaign) {
            $errorData = $this->prepareManualError($additionalData, $campaign);
            $this->createCreativeError($creative, $errorData);
        }
    }

    /**
     * @param Creative $creative
     */
    private function removeVideoData(Creative $creative): void
    {
        $creative->original_key = null;
        $creative->preview_url = null;
        $creative->save();
    }

    /**
     * @param Creative $creative
     *
     * @return Campaign[]|Collection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function findCampaigns(Creative $creative): Collection
    {
        $this->log->info('Started searching campaigns that have creative as active.', [
            'creative_id' => $creative->id,
        ]);

        $campaigns = $this->campaignRepository->byActiveCreative($creative)->get();

        $this->log->info('Finished searching campaigns that have creative as active.', [
            'creative_id' => $creative->id,
            'campaigns'   => $campaigns->pluck('id')->toArray(),
        ]);

        return $campaigns;
    }

    /**
     * @param Creative $creative
     * @param array    $errorData
     */
    protected function createCreativeError(Creative $creative, array $errorData): void
    {
        $creative->errors()->save(new CreativeError($errorData));
    }
}
