<?php

namespace Modules\Creative\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Modules\Creative\Actions\Contracts\ValidateCreativeAction as ValidateCreativeActionContract;
use Modules\Creative\Helpers\CreativeRulesHelper;
use Modules\Creative\Http\Requests\StoreCreativeRequest;
use Psr\Log\LoggerInterface;

class ValidateCreativeAction implements ValidateCreativeActionContract
{
    /**
     * @var StoreCreativeRequest
     */
    private $request;

    /**
     * @var StoreCreativeAction
     */
    private $storeCreativeAction;

    /**
     * @var RejectCreativeAction
     */
    private $rejectCreativeAction;

    /**
     * @var CreativeRulesHelper
     */
    private $creativeRulesHelper;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @param StoreCreativeRequest $request
     * @param StoreCreativeAction  $storeCreativeAction
     * @param RejectCreativeAction $rejectCreativeAction
     * @param CreativeRulesHelper  $creativeRulesHelper
     * @param LoggerInterface      $log
     * @param DatabaseManager      $databaseManager
     */
    public function __construct(
        StoreCreativeRequest $request,
        StoreCreativeAction $storeCreativeAction,
        RejectCreativeAction $rejectCreativeAction,
        CreativeRulesHelper $creativeRulesHelper,
        LoggerInterface $log,
        DatabaseManager $databaseManager
    ) {
        $this->request = $request;
        $this->storeCreativeAction = $storeCreativeAction;
        $this->rejectCreativeAction = $rejectCreativeAction;
        $this->creativeRulesHelper = $creativeRulesHelper;
        $this->log = $log;
        $this->databaseManager = $databaseManager;
    }

    /**
     * Manually validate parsed creative video
     *
     * @return bool
     * @throws ValidationException
     * @throws \Throwable
     */
    public function handle(): bool
    {
        $data = $this->request->all();
        $this->log->info('Started video validation process.');
        $this->log->debug('Creative raw params', $data);
        $validator = Validator::make($data, $this->creativeRulesHelper->getRules());

        if (!$validator->fails()) {
            return true;
        }

        $this->databaseManager->beginTransaction();

        try {
            $this->log->info('Started storing creative which fails validation');
            $creative = $this->storeCreativeAction->handle($this->request->toData());

            $this->log->info('Started rejecting creative after pre validation');
            $this->rejectCreativeAction->handle(
                $creative,
                $validator->errors()->messages(),
                $this->request->campaign_id
            );
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();

        throw new ValidationException($validator);
    }
}
