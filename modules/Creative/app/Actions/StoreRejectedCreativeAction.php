<?php

namespace Modules\Creative\Actions;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Creative\Actions\Traits\PrepareCreativeError;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeError;
use Modules\Creative\Repositories\Contracts\CreativeRepository;
use Psr\Log\LoggerInterface;

class StoreRejectedCreativeAction
{
    use PrepareCreativeError;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var CreativeRepository
     */
    private $repository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private $user;

    /**
     * Subject field for the FE validation
     */
    protected const FE_SUBJECT = 'fe_subject';

    /**
     * @param DatabaseManager    $databaseManager
     * @param CreativeRepository $repository
     * @param LoggerInterface    $log
     * @param Guard              $auth
     */
    public function __construct(
        DatabaseManager $databaseManager,
        CreativeRepository $repository,
        LoggerInterface $log,
        Guard $auth
    ) {
        $this->databaseManager = $databaseManager;
        $this->repository = $repository;
        $this->logger = $log;
        $this->user = $auth->user();
    }

    /**
     * @param array $data
     *
     * @return Creative
     * @throws \Throwable
     */
    public function handle(array $data): Creative
    {
        $this->logger->info('Declined FE-validation raw params', Arr::get($data, 'rawData'));

        $this->databaseManager->beginTransaction();

        try {
            $creative = $this->findOrCreateCreative($data);
            $this->storeErrors($creative, $data);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
        $this->logger->info('Processed FE-rejected creative', [
            'creative_id' => $creative->id,
            'campaign_id' => Arr::get($data, 'campaign_id'),
        ]);

        return $creative;
    }

    /**
     * @param Creative $creative
     * @param array    $data
     */
    private function storeErrors(Creative $creative, array $data): void
    {
        $errors = Arr::get($data, 'errors');
        $campaignId = Arr::get($data, 'campaign_id');
        $creative->errors()->saveMany($this->prepareFeErrors($errors, $campaignId));
        $this->logger->info('Stored FE-rejected creative', [
            'errors'      => $errors,
            'creative_id' => $creative->id,
            'campaign_id' => $campaignId,
        ]);
    }

    /**
     * @param array $data
     *
     * @return Creative
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function findOrCreateCreative(array $data): Creative
    {
        $creative = $this->findCreative($data);

        if ($creative) {
            return $creative;
        }

        return $this->createCreative($data);
    }

    /**
     * @param array $data
     *
     * @return Creative
     */
    private function createCreative(array $data): Creative
    {
        $creativeData = Arr::get($data, 'creative');
        return $this->repository->create($creativeData);
    }

    /**
     * @param array $data
     *
     * @return Creative|null
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function findCreative(array $data): ?Creative
    {
        return $this->repository
            ->getDuplicate(Arr::get($data, 'creative.hash'), Arr::get($data, 'creative.user_id'))
            ->first();
    }

    /**
     * @param array $errorsBag
     * @param int   $campaignId
     *
     * @return array
     */
    protected function prepareFeErrors(array $errorsBag, int $campaignId): array
    {
        $errorMessagesArray = [];
        foreach ($errorsBag as $field => $errors) {
            $error = $this->prepareFeError($field, $errors, $campaignId);
            $errorMessagesArray[] = new CreativeError($error);
        }
        return $errorMessagesArray;
    }
}
