<?php

namespace Modules\Creative\Actions;

use App\Helpers\TranslationHelper;
use Illuminate\Support\Collection;
use Modules\Creative\Helpers\CreativeRulesHelper;
use Modules\Creative\Models\Creative;
use Psr\Log\LoggerInterface;

/**
 * @package Modules\Creative\Actions
 */
class IndexErrorsAction
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $rules;

    /**
     * @param LoggerInterface     $log
     * @param CreativeRulesHelper $creativeRulesHelper
     */
    public function __construct(LoggerInterface $log, CreativeRulesHelper $creativeRulesHelper)
    {
        $this->logger = $log;
        $this->rules = $creativeRulesHelper->getRules();
    }

    /**
     * @param Creative $creative
     * @return array
     * @throws \Exception
     */
    public function handle(Creative $creative): array
    {
        $errors = $creative->errors;
        return [
            'errors'  => $this->formErrors($errors),
            'success' => $this->formSuccess($errors->pluck('field')->toArray())
        ];
    }

    /**
     * @param $errors
     * @return array
     */
    private function formSuccess($errors): array
    {
        $result = [];
        foreach ($this->rules as $key => $rule) {
            $formatedKey = TranslationHelper::multiDimensionalTranslation($key);
            if (!in_array($formatedKey, $errors)) {
                $result[] = __('validation.attributes.' . $formatedKey);
            }
        }

        return $result;
    }

    /**$
     * @param Collection $errors
     * @return array
     */
    private function formErrors(Collection $errors): array
    {
        $result = [];
        foreach ($errors as $error) {
            $result[$error->field] = $error->messages;
        }

        return $result;
    }
}
