<?php

namespace Modules\Creative\Actions\Contracts;

use App\Actions\Contracts\Action;

/**
 * Interface AwsSignatureAction
 * @package Modules\Creative\Actions\Contracts
 */
interface AwsSignatureAction extends Action
{

}
