<?php

namespace Modules\Creative\Actions\Contracts;

/**
 * Interface StoreCreativeAction
 * @package Modules\Creative\Actions\Contracts
 */
interface StoreCreativeAction
{
    /**
     * @param array $additionalParams
     * @return mixed
     */
    public function handle(array $additionalParams);
}
