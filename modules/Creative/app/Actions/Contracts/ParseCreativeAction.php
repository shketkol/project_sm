<?php

namespace Modules\Creative\Actions\Contracts;

/**
 * Interface ParseCreativeAction
 * @package Modules\Creative\Actions\Contracts
 */
interface ParseCreativeAction
{
    /**
     * Store parsed creative video
     *
     * @param string $videoUrl
     * @return array
     */
    public function handle(string $videoUrl): array;
}
