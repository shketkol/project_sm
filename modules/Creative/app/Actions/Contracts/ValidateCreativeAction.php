<?php

namespace Modules\Creative\Actions\Contracts;

/**
 * Interface ValidateCreativeAction
 * @package Modules\Creative\Actions\Contracts
 */
interface ValidateCreativeAction
{
    /**
     * Validate parsed creative video
     *
     * @return bool
     */
    public function handle(): bool;
}
