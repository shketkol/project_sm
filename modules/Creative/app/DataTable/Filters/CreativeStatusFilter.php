<?php

namespace Modules\Creative\DataTable\Filters;

use App\DataTable\Filters\DataTableFilter;
use Illuminate\Database\Eloquent\Builder;
use Modules\Creative\Models\CreativeStatus;
use Yajra\DataTables\DataTableAbstract;

class CreativeStatusFilter extends DataTableFilter
{
    /**
     * Filter name
     *
     * @var string
     */
    public static $name = 'creative-status';

    /**
     * DataTableFilter constructor.
     *
     * @param \Yajra\DataTables\DataTableAbstract $dataTable
     */
    public function filter(DataTableAbstract $dataTable): void
    {
        $this->makeFilter($dataTable, 'creative_status', function (Builder $query, $values) {
            $query->orWhereIn('creative_statuses.id', $values);
        });
    }

    /**
     * Get filter options
     *
     * @return array
     */
    public function options(): array
    {
        return CreativeStatus::all()
            ->pluck('name', 'id')
            ->mapWithKeys(function (string $value, int $key) {
                return [$key => __("creative::labels.creative.statuses.{$value}")];
            })
            ->toArray();
    }
}
