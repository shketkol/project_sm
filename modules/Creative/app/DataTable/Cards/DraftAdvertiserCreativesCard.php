<?php

namespace Modules\Creative\DataTable\Cards;

use Modules\Creative\DataTable\Repositories\Criteria\DraftCreativeCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;

class DraftAdvertiserCreativesCard extends DraftCreativesCard
{
    /**
     * @return float
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new UserCriteria($this->getAdvertiser()))
            ->pushCriteria(new DraftCreativeCriteria())
            ->creatives()
            ->count('creatives.id');
    }
}
