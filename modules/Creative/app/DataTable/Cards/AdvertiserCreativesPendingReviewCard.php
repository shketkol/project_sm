<?php

namespace Modules\Creative\DataTable\Cards;

use Modules\Creative\DataTable\Repositories\Criteria\CreativePendingReviewCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;

class AdvertiserCreativesPendingReviewCard extends CreativesPendingReviewCard
{
    /**
     * @return float
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new UserCriteria($this->getAdvertiser()))
            ->pushCriteria(new CreativePendingReviewCriteria())
            ->creatives()
            ->count('creatives.id');
    }
}
