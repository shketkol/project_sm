<?php

namespace Modules\Creative\DataTable\Cards;

use Modules\Creative\DataTable\Repositories\Criteria\ApprovedCreativeCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;

class ApprovedAdvertiserCreativesCard extends ApprovedCreativesCard
{
    /**
     * @return float
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new UserCriteria($this->getAdvertiser()))
            ->pushCriteria(new ApprovedCreativeCriteria())
            ->creatives()
            ->count('creatives.id');
    }
}
