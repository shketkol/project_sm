<?php

namespace Modules\Creative\DataTable\Cards;

use App\DataTable\Cards\Card;
use Modules\Creative\DataTable\Repositories\CreativesDataTableRepository;
use Modules\Creative\DataTable\Repositories\Criteria\DraftCreativeCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;

class DraftCreativesCard extends Card
{
    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'number';

    /**
     * @var CreativesDataTableRepository
     */
    protected $repository;

    /**
     * DraftCreativesCard constructor.
     *
     * @param CreativesDataTableRepository $repository
     */
    public function __construct(CreativesDataTableRepository $repository)
    {
        $this->title = trans('creative::labels.creative.cards.draft_creatives');
        $this->repository = $repository;
    }

    /**
     * @return float
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new UserCriteria($this->getUser()))
            ->pushCriteria(new DraftCreativeCriteria())
            ->creatives()
            ->count('creatives.id');
    }
}
