<?php

namespace Modules\Creative\DataTable\Cards;

use App\DataTable\Cards\Card;
use Modules\Creative\DataTable\Repositories\CreativesDataTableRepository;
use Modules\Creative\DataTable\Repositories\Criteria\CreativePendingReviewCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;

class CreativesPendingReviewCard extends Card
{
    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'number';

    /**
     * @var CreativesDataTableRepository
     */
    protected $repository;

    /**
     * CreativesPendingReviewCard constructor.
     *
     * @param CreativesDataTableRepository $repository
     */
    public function __construct(CreativesDataTableRepository $repository)
    {
        $this->title = trans('creative::labels.creative.cards.creatives_pending_review');
        $this->repository = $repository;
    }

    /**
     * @return float
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new UserCriteria($this->getUser()))
            ->pushCriteria(new CreativePendingReviewCriteria())
            ->creatives()
            ->count('creatives.id');
    }
}
