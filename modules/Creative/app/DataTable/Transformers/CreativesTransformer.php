<?php

namespace Modules\Creative\DataTable\Transformers;

use App\DataTable\Transformers\DataTableTransformer;

class CreativesTransformer extends DataTableTransformer
{
    /**
     * Mapping fields.
     *
     * @var array
     */
    protected $mapMap = [
        'id'          => [
            'name'    => 'id',
            'default' => '-',
        ],
        'name'        => [
            'name'    => 'name',
            'default' => '-',
        ],
        'upload_date' => [
            'name'    => 'created_at',
            'default' => '-',
        ],
        'file_size'   => [
            'name'    => 'filesize',
            'default' => '-',
        ],
        'extension'   => [
            'name'    => 'extension',
            'default' => null,
        ],
        'duration'    => [
            'name'    => 'duration',
            'default' => null
        ],
        'creative_status' => [
            'name'    => 'creative_status',
            'default' => '',
        ],
        'original_key'    => [
            'name'    => 'original_key',
            'default' => null,
        ],
        'preview_url'     => [
            'name'    => 'preview_url',
            'default' => null,
        ],
        'poster_key'     => [
            'name'    => 'poster_url',
            'default' => null,
        ],
        'external_id'    => [
            'name'    => 'external_id',
            'default' => ''
        ],
    ];
}
