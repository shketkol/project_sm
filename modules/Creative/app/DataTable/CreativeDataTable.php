<?php

namespace Modules\Creative\DataTable;

use App\DataTable\DataTable;
use Illuminate\Database\Eloquent\Builder;
use Modules\Creative\DataTable\Filters\CreativeStatusFilter;
use Modules\Creative\DataTable\Repositories\Contracts\CreativesDataTableRepository;
use Modules\Creative\DataTable\Repositories\Criteria\AddCreativeStatusCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\ExcludeOnProcessingCreativesCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\ExcludeRejectedStatusCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;
use Modules\Creative\DataTable\Transformers\CreativesTransformer;
use Modules\Creative\DataTable\Cards\ApprovedCreativesCard;
use Modules\Creative\DataTable\Cards\CreativesPendingReviewCard;
use Modules\Creative\DataTable\Cards\DraftCreativesCard;

/**
 * Class CreativeDataTable
 *
 * @package App\DataTable
 */
class CreativeDataTable extends DataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'creatives';

    /**
     * Transformer class
     *
     * @var string
     */
    protected $transformer = CreativesTransformer::class;

    /**
     * Repository
     *
     * @var string
     */
    protected $repository = CreativesDataTableRepository::class;

    /**
     * Add aliases according model relation.
     * Required for search
     *
     * @var array
     */
    protected $relationAliases = [
        'file_size'   => 'filesize',
        'upload_date' => 'created_at',
    ];

    /**
     * Available cards
     *
     * @var array
     */
    protected $cards = [
        ApprovedCreativesCard::class,
        CreativesPendingReviewCard::class,
        DraftCreativesCard::class,
    ];

    /**
     * Filters
     *
     * @var array
     */
    protected $filters = [
        CreativeStatusFilter::class,
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        /** @var CreativesDataTableRepository $repository */
        $repository = $this->getRepository();

        return $repository
            ->pushCriteria(new UserCriteria($this->getUser()))
            ->pushCriteria(new AddCreativeStatusCriteria())
            ->pushCriteria(new ExcludeRejectedStatusCriteria())
            ->pushCriteria(new ExcludeOnProcessingCreativesCriteria())
            ->creatives();
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('creative.list');
    }
}
