<?php

namespace Modules\Creative\DataTable\Repositories\Criteria;

use Modules\Creative\Models\CreativeStatus;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ExcludeRejectedStatusCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Builder             $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('creative_statuses.id', '!=', CreativeStatus::ID_REJECTED);
    }
}
