<?php

namespace Modules\Creative\DataTable\Repositories\Criteria;

use Modules\User\Models\User;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class UserCriteria implements CriteriaInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserCriteria constructor.
     *
     * @param User|\Illuminate\Contracts\Auth\Authenticatable $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Builder             $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('creatives.user_id', '=', $this->user->id);
    }
}
