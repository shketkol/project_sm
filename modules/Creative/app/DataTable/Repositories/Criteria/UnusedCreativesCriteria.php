<?php

namespace Modules\Creative\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Modules\Campaign\Models\CampaignStatus;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class UnusedCreativesCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        $date = Carbon::now()->subDays(config('rules.creative.delete_after_if_unused'));

        $creativesWithoutCampaignsQuery = $model->select('creatives.*')
            ->distinct()
            ->leftJoin('campaign_creative', 'creatives.id', '=', 'campaign_creative.creative_id')
            ->where('creatives.updated_at', '<', $date)
            ->whereNull('campaign_creative.campaign_id');

        $creativesInDraftCampaignsQuery = $model->select('creatives.*')
            ->distinct()
            ->leftJoin('campaign_creative', 'creatives.id', '=', 'campaign_creative.creative_id')
            ->leftJoin('campaigns', 'campaign_creative.campaign_id', '=', 'campaigns.id')
            ->where('creatives.updated_at', '<', $date)
            ->where('campaigns.status_id', '=', CampaignStatus::ID_DRAFT);

        // make sure that we don't delete creatives that in draft in one campaigns and not in draft in other campaigns
        $creativesNotInDraftCampaignsQuery = $model->select('creatives.id')
            ->distinct()
            ->leftJoin('campaign_creative', 'creatives.id', '=', 'campaign_creative.creative_id')
            ->leftJoin('campaigns', 'campaign_creative.campaign_id', '=', 'campaigns.id')
            ->where('creatives.updated_at', '<', $date)
            ->where('campaigns.status_id', '!=', CampaignStatus::ID_DRAFT);

        return $creativesInDraftCampaignsQuery
            ->whereNotIn('creatives.id', $creativesNotInDraftCampaignsQuery)
            ->union($creativesWithoutCampaignsQuery);
    }
}
