<?php

namespace Modules\Creative\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Modules\Creative\Models\CreativeStatus;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class CreativePendingReviewCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where('creatives.status_id', '=', CreativeStatus::ID_PENDING_APPROVAL);
    }
}
