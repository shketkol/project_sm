<?php

namespace Modules\Creative\DataTable\Repositories\Criteria;

use Modules\Creative\Models\CreativeStatus;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ExcludeOnProcessingCreativesCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Builder $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $statusIds = [CreativeStatus::ID_PENDING_APPROVAL, CreativeStatus::ID_PROCESSING];

        return $model->where(function ($query) use ($statusIds) {
            $query->whereNotIn(
                'creative_statuses.id',
                $statusIds
            )->orWhere('creatives.external_id', '!=', null);
        });
    }
}
