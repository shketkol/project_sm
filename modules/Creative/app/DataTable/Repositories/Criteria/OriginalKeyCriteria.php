<?php

namespace Modules\Creative\DataTable\Repositories\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class OriginalKeyCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private $originalKey;

    /**
     * @param string $originalKey
     */
    public function __construct(string $originalKey)
    {
        $this->originalKey = $originalKey;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Builder             $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('creatives.original_key', '=', $this->originalKey);
    }
}
