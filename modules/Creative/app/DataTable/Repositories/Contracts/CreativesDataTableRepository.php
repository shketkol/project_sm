<?php

namespace Modules\Creative\DataTable\Repositories\Contracts;

use App\Repositories\Contracts\Repository;
use Illuminate\Database\Eloquent\Builder;

interface CreativesDataTableRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string;

    /**
     * Get user creatives
     *
     * @return Builder
     */
    public function creatives(): Builder;
}
