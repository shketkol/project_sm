<?php

namespace Modules\Creative\DataTable\Repositories;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Creative\DataTable\Repositories\Criteria\AddCreativeStatusCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\ExcludeOnProcessingCreativesCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\ExcludeRejectedStatusCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;
use Modules\Creative\Models\Creative;
use Modules\Creative\DataTable\Repositories\Contracts\CreativesDataTableRepository
    as CreativesDataTableRepositoryInterface;
use Modules\Creative\Repositories\Criteria\WithoutTrashedCriteria;
use Modules\User\Models\User;

class CreativesDataTableRepository extends Repository implements CreativesDataTableRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Creative::class;
    }

    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot(): void
    {
        // by default we show only not deleted creatives
        $this->pushCriteria(new WithoutTrashedCriteria());
    }

    /**
     * Get user creatives
     *
     * @return Builder
     */
    public function creatives(): Builder
    {
        $this->applyCriteria();

        return $this->model->addSelect('creatives.*');
    }

    /**
     * @param User $user
     *
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getUserCreativesCount(User $user): int
    {
        return $this->pushCriteria(new UserCriteria($user))
        ->pushCriteria(new AddCreativeStatusCriteria())
        ->pushCriteria(new ExcludeRejectedStatusCriteria())
        ->pushCriteria(new ExcludeOnProcessingCreativesCriteria())
        ->creatives()
        ->count();
    }
}
