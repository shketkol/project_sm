<?php

namespace Modules\Creative\DataTable;

use Illuminate\Database\Eloquent\Builder;
use Modules\Creative\DataTable\Repositories\Contracts\CreativesDataTableRepository;
use Modules\Creative\DataTable\Repositories\Criteria\AddCreativeStatusCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;
use Modules\Creative\DataTable\Cards\AdvertiserCreativesPendingReviewCard;
use Modules\Creative\DataTable\Cards\ApprovedAdvertiserCreativesCard;
use Modules\Creative\DataTable\Cards\DraftAdvertiserCreativesCard;

class CreativeAdvertiserDataTable extends CreativeGalleryDataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'advertiser-creatives';

    /**
     * Available cards
     *
     * @var array
     */
    protected $cards = [
        ApprovedAdvertiserCreativesCard::class,
        AdvertiserCreativesPendingReviewCard::class,
        DraftAdvertiserCreativesCard::class,
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        /** @var CreativesDataTableRepository $repository */
        $repository = $this->getRepository();

        return $repository
            ->pushCriteria(new UserCriteria($this->getAdvertiser()))
            ->pushCriteria(new AddCreativeStatusCriteria())
            ->creatives();
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('creative.listAdvertiserCreatives', $this->getAdvertiser());
    }
}
