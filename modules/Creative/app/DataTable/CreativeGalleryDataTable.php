<?php

namespace Modules\Creative\DataTable;

use Illuminate\Database\Eloquent\Builder;
use Modules\Creative\DataTable\Repositories\Contracts\CreativesDataTableRepository;
use Modules\Creative\DataTable\Repositories\Criteria\AddCreativeStatusCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\UserCriteria;

/**
 * Class CreativeGalleryDataTable
 *
 * @package App\DataTable
 */
class CreativeGalleryDataTable extends CreativeDataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'creatives-gallery';

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        /** @var CreativesDataTableRepository $repository */
        $repository = $this->getRepository();

        return $repository
            ->pushCriteria(new UserCriteria($this->getUser()))
            ->pushCriteria(new AddCreativeStatusCriteria())
            ->creatives();
    }
}
