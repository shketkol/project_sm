<?php

namespace Modules\Creative\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Creative\Models\CreativeStatus;

/**
 * @property CreativeStatus $status
 */
trait BelongsToStatus
{
    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(CreativeStatus::class);
    }

    /**
     * Returns true when creative stored on s3 but does not pass validation.
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->status_id === CreativeStatus::ID_DRAFT;
    }

    /**
     * Check if creative is validating on hulu side.
     *
     * @return bool
     */
    public function isValidating(): bool
    {
        return $this->status_id === CreativeStatus::ID_PENDING_APPROVAL;
    }

    /**
     * Returns true if creative fails validation.
     *
     * @return bool
     */
    public function isRejected(): bool
    {
        return $this->status_id === CreativeStatus::ID_REJECTED;
    }

    /**
     * Creative approved.
     *
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->status_id === CreativeStatus::ID_APPROVED;
    }
}
