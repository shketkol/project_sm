<?php

namespace Modules\Creative\Models\Traits;

use App\Helpers\MediaHelper;
use Illuminate\Support\Facades\Storage;
use Modules\Creative\Models\Creative;

/**
 * @mixin Creative
 */
trait CreativeStorage
{
    use AWSCreative;

    /**
     * @return bool
     */
    protected function isStorageSupportTemporaryUrl(): bool
    {
        return method_exists(Storage::disk()->getDriver()->getAdapter(), 'getTemporaryUrl');
    }

    /**
     * @param string|null $variant
     *
     * @return string
     * @see /var/www/html/config/media.php
     */
    public function generatePosterUrl(string $variant = 'small'): string
    {
        if (!$this->poster_key) {
            return '';
        }

        return $this->isStorageSupportTemporaryUrl()
            ? $this->generateTemporaryPosterUrl()
            : Storage::url(MediaHelper::imgVariant($this->poster_key, $variant));
    }

    /**
     * @return string
     */
    public function generateDownloadUrlForAdvertiser(): string
    {
        if (!$this->original_key && !$this->preview_url) {
            return '';
        }

        if (!$this->original_key && $this->preview_url) {
            return $this->preview_url;
        }

        return $this->isStorageSupportTemporaryUrl()
            ? $this->generateTemporaryDownloadUrlForAdvertiser()
            : Storage::url($this->original_key);
    }

    /**
     * @return string
     */
    public function generateDownloadUrlForHaapi(): string
    {
        if (!$this->original_key) {
            return '';
        }

        return $this->isStorageSupportTemporaryUrl()
            ? $this->generateTemporaryDownloadUrlForHaapi()
            : ($this->original_key ? Storage::url($this->original_key) : '');
    }
}
