<?php

namespace Modules\Creative\Models\Traits;

use Modules\Creative\Models\Creative;

trait CreateCreative
{
    /**
     * Create test creative using factory.
     *
     * @param array $attributes
     *
     * @return Creative
     */
    private function createTestCreative(array $attributes = []): Creative
    {
        return Creative::factory()->create($attributes);
    }
}
