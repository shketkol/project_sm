<?php

namespace Modules\Creative\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\Creative\Models\Creative;

/**
 * Trait HasCreative
 *
 * @package Modules\Creative\Models\Traits
 * @property Creative $creative
 */
trait HasCreative
{
    /**
     * @return HasOne
     */
    public function creative(): HasOne
    {
        return $this->hasOne(Creative::class, 'status_id');
    }
}
