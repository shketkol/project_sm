<?php

namespace Modules\Creative\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Creative\Models\Creative;

/**
 * Trait HasManyCreatives
 *
 * @package Modules\Creative\Models\Traits
 */
trait HasManyCreatives
{
    /**
     * @return HasMany
     */
    public function creatives(): HasMany
    {
        return $this->hasMany(Creative::class, 'user_id');
    }
}
