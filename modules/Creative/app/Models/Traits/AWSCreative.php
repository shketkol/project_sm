<?php

namespace Modules\Creative\Models\Traits;

use App\Helpers\MediaHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

trait AWSCreative
{
    /**
     * @param int $expireMinutes
     *
     * @return string
     */
    private function generateTemporaryDownloadUrl(int $expireMinutes): string
    {
        return Storage::temporaryUrl($this->original_key, Carbon::now()->addMinutes($expireMinutes));
    }

    /**
     * @return string
     */
    public function generateTemporaryDownloadUrlForAdvertiser(): string
    {
        return $this->generateTemporaryDownloadUrl(config('aws.url_expiration.for_advertiser'));
    }

    /**
     * @return string
     */
    public function generateTemporaryDownloadUrlForHaapi(): string
    {
        return $this->generateTemporaryDownloadUrl(config('aws.url_expiration.for_haapi'));
    }

    /**
     * @param string|null $variant
     *
     * @return string
     * @see /var/www/html/config/media.php
     */
    public function generateTemporaryPosterUrl(string $variant = 'small'): string
    {
        $path = $this->poster_key;

        if ($variant) {
            $path = MediaHelper::imgVariant($path, $variant);
        }

        return Storage::temporaryUrl(
            $path,
            Carbon::now()->addMinutes(config('aws.url_expiration.for_advertiser'))
        );
    }
}
