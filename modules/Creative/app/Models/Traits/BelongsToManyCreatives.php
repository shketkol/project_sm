<?php

namespace Modules\Creative\Models\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Models\Pivot\CampaignCreative;

/**
 * @property Creative[]|Collection $creatives
 * @property string                $creative_status_name
 * @see BelongsToManyCreatives::getCreativeStatusNameAttribute()
 */
trait BelongsToManyCreatives
{
    /**
     * @return BelongsToMany
     */
    public function creatives(): BelongsToMany
    {
        return $this->belongsToMany(Creative::class)
            ->withTimestamps()
            ->using(CampaignCreative::class);
    }

    /**
     * Get active creative resource. Could be only one.
     *
     * @return Creative|null
     */
    public function getActiveCreative(): ?Creative
    {
        return $creative = $this->creatives()->wherePivot('is_active', true)->first();
    }

    /**
     * Get translated name of the creative status.
     * If there is no creative attached to the campaign, "Needs Creative" is returned.
     *
     * @return string
     */
    public function getCreativeStatusNameAttribute(): string
    {
        $creative = $this->getActiveCreative();

        if ($creative) {
            return $creative->status->translated_name;
        }

        return __('creative::labels.creative.statuses.' . CreativeStatus::MISSING);
    }
}
