<?php

namespace Modules\Creative\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Arr;
use Modules\Creative\Models\CreativeError;

/**
 * Trait HasErrors
 * @property string|null $rejection_reason
 * @property bool $manual_validation_failed
 * @package Modules\Creative\Models\Traits
 */
trait HasErrors
{
    /**
     * @return HasMany
     */
    public function errors(): HasMany
    {
        return $this->hasMany(CreativeError::class, 'creative_id');
    }

    /**
     * Get rejection reason error message (received from HAAPI).
     *
     * @return null|string
     */
    public function getRejectionReasonAttribute(): ?string
    {
        $reason = $this->errors()->where('field', CreativeError::MANUAL_VALIDATION)->first();

        return $reason && is_array($reason->messages) ? Arr::first($reason->messages) : null;
    }

    /**
     * Returns true if creative has been rejected by QC team.
     *
     * @return bool
     */
    public function getManualValidationFailedAttribute(): bool
    {
        return !is_null($this->rejection_reason);
    }
}
