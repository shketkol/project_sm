<?php

namespace Modules\Creative\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Creative\Models\Creative;

/**
 * Trait BelongsToCreative
 *
 * @package Modules\Creative\Models\Traits
 * @property Creative $creative
 */
trait BelongsToCreative
{
    /**
     * @return BelongsTo
     */
    public function creative(): BelongsTo
    {
        return $this->belongsTo(Creative::class);
    }
}
