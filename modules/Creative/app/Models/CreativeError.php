<?php

namespace Modules\Creative\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Models\Traits\BelongsToCampaign;
use Modules\Creative\Models\Traits\BelongsToCreative;

/**
 * @property integer $id
 * @property string  $field
 * @property integer $creative_id
 * @property integer $campaign_id
 * @property string  $subject
 * @property string  $messages
 * @property string  $created_at
 * @property string  $updated_at
 */
class CreativeError extends Model
{
    use BelongsToCreative,
        BelongsToCampaign;

    /**
     * Creative can be rejected manually.
     */
    const MANUAL_VALIDATION = 'manual_validation';

    /**
     * @var array
     */
    protected $fillable = [
        'creative_id',
        'campaign_id',
        'field',
        'subject',
        'messages',
    ];

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getMessagesAttribute($value)
    {
        return json_decode($value, true);
    }
}
