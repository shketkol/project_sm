<?php

namespace Modules\Creative\Models;

use App\Traits\HasFactory;
use App\Traits\State;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Campaign\Models\Traits\BelongsToManyCampaigns;
use Modules\Creative\Models\Traits\HasErrors;
use Modules\Creative\Models\Traits\CreativeStorage;
use Modules\User\Models\Traits\BelongsToUser;
use Modules\Creative\Models\Traits\BelongsToStatus;

/**
 * @property int    $id
 * @property string $name
 * @property string $extension
 * @property float  $duration
 * @property int    $width
 * @property int    $height
 * @property float  $filesize
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property int    $status_id
 * @property string $original_key
 * @property string $preview_url
 * @property string $poster_key
 * @property int    $user_id
 * @property string $hash
 * @property string $external_id
 * @property bool   $is_pro_res
 *
 * @property string $source_url
 * @see Creative::getSourceUrlAttribute()
 * @property string $poster_url
 * @see Creative::getPosterUrlAttribute()
 */
class Creative extends Model
{
    use SoftDeletes,
        BelongsToUser,
        BelongsToStatus,
        HasErrors,
        State,
        BelongsToManyCampaigns,
        CreativeStorage,
        HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'external_id',
        'name',
        'hash',
        'extension',
        'duration',
        'width',
        'height',
        'filesize',
        'status_id',
        'created_at',
        'original_key',
        'preview_url',
        'poster_key',
        'is_pro_res',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_pro_res' => 'boolean',
    ];

    /**
     * Creative state's flow.
     *
     * @var string
     */
    protected $flow = 'creative';

    /**
     * Attribute used for HAAPI operations.
     * After Campaign saved on HAAPI part we use link for HAAPI S3 bucket.
     *
     * @return string
     */
    public function getSourceUrlAttribute(): string
    {
        return filter_var($this->original_key, FILTER_VALIDATE_URL) ?
            $this->original_key :
            $this->generateDownloadUrlForHaapi();
    }

    /**
     * @return string
     */
    public function getPosterUrlAttribute(): string
    {
        return $this->generatePosterUrl();
    }

    /**
     * HAAPI external id.
     *
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->external_id;
    }
}
