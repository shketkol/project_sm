<?php

namespace Modules\Creative\Models\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Modules\Campaign\Models\Traits\BelongsToCampaign;
use Modules\Creative\Models\Traits\BelongsToCreative;

/**
 * @property integer $id
 * @property integer $campaign_id
 * @property integer $creative_id
 * @property boolean $is_active
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @mixin \Illuminate\Database\Eloquent\Model
 */
class CampaignCreative extends Pivot
{
    use BelongsToCreative, BelongsToCampaign;

    /**
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'creative_id',
        'is_active',
    ];
}
