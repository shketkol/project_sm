<?php

namespace Modules\Creative\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string  $name
 * @property string $translated_name
 */
class CreativeStatus extends Model
{
    /**
     * Creative just created and in the draft mode
     */
    public const DRAFT = 'draft';

    /**
     * Creative has been processed on HULU side.
     */
    public const PROCESSING = 'processing';

    /**
     * Creative in validation process
     */
    public const PENDING_APPROVAL = 'pending approval';

    /**
     * Creative has been approved
     */
    public const APPROVED = 'approved';

    /**
     * Creative has been rejected
     */
    public const REJECTED = 'rejected';

    /**
     * Creative missing (virtual status)
     */
    public const MISSING = 'missing';

    public const ID_DRAFT            = 1;
    public const ID_PENDING_APPROVAL = 2;
    public const ID_APPROVED         = 3;
    public const ID_REJECTED         = 4;
    public const ID_PROCESSING       = 5;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get translated status name.
     *
     * @return string
     */
    public function getTranslatedNameAttribute(): string
    {
        return __("creative::labels.creative.statuses.{$this->name}");
    }
}
