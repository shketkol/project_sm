<?php

namespace Modules\Creative\Exceptions;

use App\Exceptions\ModelNotCreatedException;

class CreativeNotCreatedException extends ModelNotCreatedException
{
    //
}
