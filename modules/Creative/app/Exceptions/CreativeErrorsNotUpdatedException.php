<?php

namespace Modules\Creative\Exceptions;

use App\Exceptions\ModelNotUpdatedException;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Creative\Models\Creative;
use Throwable;

class CreativeErrorsNotUpdatedException extends ModelNotUpdatedException
{
    /**
     * @var string
     */
    protected $message;

    /**
     * CreativeErrorsNotUpdatedException constructor.
     * @param Creative $model
     * @param array $additionalData
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        Creative $model,
        array $additionalData,
        $message = "",
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
        $id = $model->external_id;
        $reason = Arr::get($additionalData, 'reason');
        $comment = Arr::get($additionalData, 'comments');
        $this->message = "Can not save rejection reason: $reason, comment: $comment to creative: $id";
    }

    /**
     * @return Response
     */
    public function render(): Response
    {
        return \response($this->message, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
