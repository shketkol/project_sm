<?php

namespace Modules\Creative\Exceptions;

use App\Exceptions\BaseException;

class CreativeParseException extends BaseException
{
    /**
     * @param string     $message
     * @param \Throwable $previous
     *
     * @return static
     */
    public static function create(string $message, \Throwable $previous): self
    {
        return new self($message, self::STATUS_CODE, $previous);
    }
}
