<?php

namespace Modules\Creative\Exceptions;

use App\Exceptions\ModelNotUpdatedException;

class CreativeNotUpdatedException extends ModelNotUpdatedException
{
    //
}
