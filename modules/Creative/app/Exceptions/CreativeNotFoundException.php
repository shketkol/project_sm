<?php

namespace Modules\Creative\Exceptions;

use App\Exceptions\ModelNotFoundException;
use Illuminate\Http\Response;

class CreativeNotFoundException extends ModelNotFoundException
{
    /**
     * @param string|null $externalId
     * @param string|null $cause
     *
     * @return self
     */
    public static function create(?string $externalId = null, ?string $cause = null): self
    {
        $message = 'Creative was not found';

        if (is_null($externalId)) {
            $message .= ', external_id is missing.';
        } else {
            $message .= sprintf(' by external_id: "%s".', $externalId);
        }

        if ($cause) {
            $message .= sprintf(' (Cause: "%s").', $cause);
        }

        return new self($message, Response::HTTP_NOT_FOUND);
    }
}
