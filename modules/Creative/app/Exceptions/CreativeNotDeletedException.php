<?php

namespace Modules\Creative\Exceptions;

use App\Exceptions\ModelNotUpdatedException;

class CreativeNotDeletedException extends ModelNotUpdatedException
{
    //
}
