<?php

namespace Modules\Creative\States;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\User\Repositories\Contracts\UserRepository;

abstract class CreativeState
{
    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ValidatingState constructor.
     * @param Dispatcher $notification
     * @param UserRepository $userRepository
     */
    public function __construct(Dispatcher $notification, UserRepository $userRepository)
    {
        $this->notification = $notification;
        $this->userRepository = $userRepository;
    }
}
