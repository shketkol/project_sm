<?php

namespace Modules\Creative\States;

use Modules\Creative\Models\Creative;
use Modules\Creative\Notifications\Advertiser\Approved;

class ApprovedState extends CreativeState
{
    /**
     * @param Creative $creative
     */
    public function after(Creative $creative): void
    {
        $this->sendNotifications($creative);
    }

    /**
     * Send notifications (email and on-site) to advertiser.
     *
     * @param Creative $creative
     */
    protected function sendNotifications(Creative $creative): void
    {
        // Send advertiser notification
        $this->notification->send(
            $creative->user,
            new Approved($creative)
        );
    }
}
