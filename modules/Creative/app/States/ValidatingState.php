<?php

namespace Modules\Creative\States;

use Modules\Creative\Models\Creative;
use Modules\Creative\Notifications\Admin\ForReview;

class ValidatingState extends CreativeState
{

    /**
     * @param Creative $creative
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function after(Creative $creative): void
    {
        $this->notification->send(
            $this->userRepository->getAdmin(),
            new ForReview($creative)
        );
    }
}
