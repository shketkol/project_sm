<?php

namespace Modules\Creative\States;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Campaign\Actions\Alerts\SetCampaignAlertsAction;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Models\Creative;
use Modules\Creative\Notifications\Admin\Rejected as AdminRejectedNotification;
use Modules\Creative\Notifications\Advertiser\Rejected as AdvertiserRejectedNotification;
use Modules\User\Repositories\Contracts\UserRepository;

class RejectedState extends CreativeState
{
    /**
     * @var SetCampaignAlertsAction
     */
    protected $setCampaignAlertsAction;

    /**
     * RejectedState constructor.
     * @param Dispatcher $notification
     * @param SetCampaignAlertsAction $setCampaignAlertsAction
     * @param UserRepository $userRepository
     */
    public function __construct(
        Dispatcher $notification,
        SetCampaignAlertsAction $setCampaignAlertsAction,
        UserRepository $userRepository
    ) {
        parent::__construct($notification, $userRepository);

        $this->setCampaignAlertsAction = $setCampaignAlertsAction;
    }

    /**
     * @param Creative $creative
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function after(Creative $creative): void
    {
        if ($creative->manual_validation_failed) {
            $this->sendNotifications($creative);
        }

        $this->createAlerts($creative);
    }

    /**
     * Send notifications (email and on-site) to admin and advertiser.
     * @param Creative $creative
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function sendNotifications(Creative $creative): void
    {
        // Send admin notification
        $this->notification->send(
            $this->userRepository->getAdmin(),
            new AdminRejectedNotification($creative)
        );

        // Send advertiser notification
        $this->notification->send(
            $creative->user,
            new AdvertiserRejectedNotification($creative)
        );
    }

    /**
     * Create alerts for all campaign the creative is attached to.
     *
     * @param Creative $creative
     */
    protected function createAlerts(Creative $creative): void
    {
        $creative->campaigns->each(function (Campaign $campaign) {
            $this->setCampaignAlertsAction->handle($campaign, true);
        });
    }
}
