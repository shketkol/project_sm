<?php

namespace Modules\Creative\Mail\Admin;

use App\Mail\Mail;

class Rejected extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('creative::emails.admin.rejected.subject', [
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('creative::emails.admin.rejected')
            ->with($this->payload);
    }
}
