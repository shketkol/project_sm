<?php

namespace Modules\Creative\Mail\Admin;

use App\Mail\Mail;

class ForReview extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('creative::emails.admin.for_review.subject', [
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('creative::emails.admin.for-review')
            ->with($this->payload);
    }
}
