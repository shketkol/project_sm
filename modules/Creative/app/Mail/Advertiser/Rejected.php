<?php

namespace Modules\Creative\Mail\Advertiser;

use App\Mail\Mail;

class Rejected extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('creative::emails.advertiser.rejected.subject'))
            ->view('creative::emails.advertiser.rejected')
            ->with($this->payload);
    }
}
