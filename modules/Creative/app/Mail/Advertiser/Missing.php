<?php

namespace Modules\Creative\Mail\Advertiser;

use App\Mail\Mail;

class Missing extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('creative::emails.advertiser.missing.subject'))
            ->view('creative::emails.advertiser.missing')
            ->with($this->payload);
    }
}
