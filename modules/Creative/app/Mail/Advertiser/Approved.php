<?php

namespace Modules\Creative\Mail\Advertiser;

use App\Mail\Mail;

class Approved extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('creative::emails.advertiser.approved.subject'))
            ->view('creative::emails.advertiser.approved')
            ->with($this->payload);
    }
}
