<?php

namespace Modules\Creative;

use Illuminate\Support\Facades\Route;
use Modules\Creative\Commands\DeleteUnusedCreativesCommand;
use Modules\Creative\Actions\AwsSignatureAction;
use Modules\Creative\Actions\Contracts\AwsSignatureAction as AwsSignatureActionContract;
use Modules\Creative\Actions\Contracts\ValidateCreativeAction as ValidateCreativeActionContract;
use Modules\Creative\Actions\ParseCreativeAction;
use Modules\Creative\Actions\StoreCreativeAction;
use Modules\Creative\Actions\Contracts\StoreCreativeAction as StoreCreativeActionContract;
use Modules\Creative\Actions\Contracts\ParseCreativeAction as ParseCreativeActionContract;
use Modules\Creative\Actions\ValidateCreativeAction;
use Modules\Creative\DataTable\Repositories\Contracts\CreativesDataTableRepository
    as CreativesDataTableRepositoryContract;
use Modules\Creative\DataTable\Repositories\CreativesDataTableRepository;
use Modules\Creative\Models\Pivot\CampaignCreative;
use Modules\Creative\Observers\CampaignCreativeObserver;
use Modules\Creative\Policies\CreativePolicy;
use Modules\Creative\Repositories\Contracts\CreativeRepository as CreativeRepositoryContract;
use Modules\Creative\Repositories\CreativeRepository;
use App\Providers\ModuleServiceProvider;
use Modules\Creative\States\ApprovedState;
use Modules\Creative\States\RejectedState;
use Modules\Creative\States\ValidatingState;

class CreativeServiceProvider extends ModuleServiceProvider
{
    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'creative' => CreativePolicy::class,
    ];

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        DeleteUnusedCreativesCommand::class,
    ];

    /**
     * @var array
     */
    public $bindings = [
        AwsSignatureActionContract::class           => AwsSignatureAction::class,
        StoreCreativeActionContract::class          => StoreCreativeAction::class,
        ParseCreativeActionContract::class          => ParseCreativeAction::class,
        'creative.action.parse'                     => ParseCreativeAction::class,
        ValidateCreativeActionContract::class       => ValidateCreativeAction::class,
        CreativeRepositoryContract::class           => CreativeRepository::class,
        CreativesDataTableRepositoryContract::class => CreativesDataTableRepository::class,
        'state-machine.creative.states.validating'  => ValidatingState::class,
        'state-machine.creative.states.approved'    => ApprovedState::class,
        'state-machine.creative.states.rejected'    => RejectedState::class,
    ];

    /**
     * Register any services for "Creative" module.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadRoutes();
        $this->loadConfigs(['state-machine', 'rules', 'aws', 'codec', 'throttle']);
        $this->loadComposers();
        $this->loadObservers();
        $this->commands($this->commands);
    }

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'creative';
    }

    /**
     * Load model observers.
     */
    protected function loadObservers(): void
    {
        CampaignCreative::observe(CampaignCreativeObserver::class);
    }

    /**
     * @param string $pluralPrefix
     * @param string $namespace
     *
     * @throws \ReflectionException
     */
    protected function loadApiRoutes(string $pluralPrefix, string $namespace): void
    {
        parent::loadApiRoutes($pluralPrefix, $namespace);

        $this->loadApiRoutesWithCustomThrottle($pluralPrefix, $namespace);
    }

    /**
     * @param string $pluralPrefix
     * @param string $namespace
     *
     * @throws \ReflectionException
     */
    protected function loadApiRoutesWithCustomThrottle(string $pluralPrefix, string $namespace): void
    {
        $path = $this->getDir() . '/../routes/throttle.php';
        if (!file_exists($path)) {
            return;
        }

        Route::middleware('api_with_custom_throttle')
            ->prefix('api/' . $this->getRoutePrefix())
            ->name("api.{$pluralPrefix}.")
            ->namespace("Modules\\{$namespace}\\Http\\Controllers\\Api")
            ->group($path);
    }
}
