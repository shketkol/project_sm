<?php

use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeStatus;

return [
    /**
     * Creative workflow graph
     */
    'creative' => [
        // class of your domain object
        'class'         => Creative::class,

        // name of the graph (default is "default")
        'graph'         => 'creative',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status_id',

        // list of all possible states
        'states'        => [
            [
                'name' => CreativeStatus::ID_DRAFT,
            ],
            [
                'name' => CreativeStatus::ID_PENDING_APPROVAL,
            ],
            [
                'name' => CreativeStatus::ID_APPROVED,
            ],
            [
                'name' => CreativeStatus::ID_REJECTED,
            ],
            [
                'name' => CreativeStatus::ID_PROCESSING,
            ],
        ],

        // list of all possible transitions
        'transitions'   => [
            CreativeStatus::PROCESSING       => [
                'from' => [CreativeStatus::ID_DRAFT],
                'to'   => CreativeStatus::ID_PROCESSING,
            ],
            CreativeStatus::PENDING_APPROVAL => [
                'from' => [CreativeStatus::ID_PROCESSING],
                'to'   => CreativeStatus::ID_PENDING_APPROVAL,
            ],
            CreativeStatus::APPROVED         => [
                'from' => [
                    CreativeStatus::ID_PENDING_APPROVAL,
                    CreativeStatus::ID_PROCESSING,
                    CreativeStatus::ID_REJECTED
                ],
                'to'   => CreativeStatus::ID_APPROVED,
            ],
            CreativeStatus::REJECTED         => [
                'from' => [
                    CreativeStatus::ID_DRAFT,
                    CreativeStatus::ID_PROCESSING,
                    CreativeStatus::ID_PENDING_APPROVAL,
                ],
                'to'   => CreativeStatus::ID_REJECTED,
            ],
        ],

        // list of all callbacks
        'callbacks'     => [
            // will be called when testing a transition
            'guard'  => [],

            // will be called before applying a transition
            'before' => [],

            // will be called after applying a transition
            'after'  => [
                'after_validating' => [
                    // call the callback on a specific transition
                    'on'   => CreativeStatus::PENDING_APPROVAL,
                    // will call the method of this class
                    'do'   => ['state-machine.creative.states.validating', 'after'],
                    // arguments for the callback
                    'args' => ['object'],
                ],
                'after_approved'   => [
                    // call the callback on a specific transition
                    'on'   => CreativeStatus::APPROVED,
                    // will call the method of this class
                    'do'   => ['state-machine.creative.states.approved', 'after'],
                    // arguments for the callback
                    'args' => ['object'],
                ],
                'after_rejected'   => [
                    // call the callback on a specific transition
                    'on'   => CreativeStatus::REJECTED,
                    // will call the method of this class
                    'do'   => ['state-machine.creative.states.rejected', 'after'],
                    // arguments for the callback
                    'args' => ['object'],
                ],
            ],
        ],
    ],
];
