<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Increased and separated throttle for creative upload to the AWS S3 bucket
    |--------------------------------------------------------------------------
    | When creative is uploading to the bucket library that uploading video could call
    | api endpoint for signature more then 60 times per minute
    | and this would cause the "Too Many Requests" error on any api call.
    |
    | This throttle setting would exclude conflict with default api throttle.
    |
    */
    'creative_upload' => env('THROTTLE_CREATIVE_UPLOAD', 120),
];
