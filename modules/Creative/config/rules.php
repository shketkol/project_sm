<?php

use App\Services\ValidationRulesService\ValidationRules;

return [
    'creative' => [
        'max_file_size'          => \App\Helpers\FileSizeHelper::GIGABYTE * 3,
        'min_duration_label'     => ValidationRules::MIN_DURATION_LABEL,
        'max_duration_label'     => ValidationRules::MAX_DURATION_LABEL,
        'max_upload_retries'     => env('CREATIVE_UPLOAD_RETRIES', 5),
        'policies'               => [
            'max_draft' => env('CREATIVE_MAX_DRAFT', 10),
        ],
        'delete_after_if_unused' => env('CREATIVE_DELETE_AFTER_DAYS', 30),
    ],
];
