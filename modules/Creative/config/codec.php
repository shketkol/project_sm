<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Apple ProRes video codec config.
    |--------------------------------------------------------------------------
    |
    */
    'pro_res' => [
        'apch', // Apple ProRes 422 High Quality
        'hcpa', // Apple ProRes 422 High Quality (little endian)
        'apcn', // Apple ProRes 422 Standard Definition
        'ncpa', // Apple ProRes 422 Standard Definition (little endian)
        'apcs', // Apple ProRes 422 LT
        'scpa', // Apple ProRes 422 LT (little endian)
        'apco', // Apple ProRes 422 Proxy
        'ocpa', // Apple ProRes 422 Proxy (little endian)
        'ap4h', // Apple ProRes 4444
        'h4pa', // Apple ProRes 4444 (little endian)
    ],
];
