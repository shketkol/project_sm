<?php

return [
    'js'             => [
        'upload_driver' => env('AWS_JS_UPLOAD_DRIVER', 'evaporate'),
        'cache_hours'   => env('AWS_JS_CACHE_HOURS', 3),
    ],

    'credentials' => [
      'cache_time' =>  env('AWS_CREDENTIALS_CACHE_SECONDS', 86400),
    ],

    /*
    |--------------------------------------------------------------------------
    | AWS S3 link expiration time in minutes.
    |--------------------------------------------------------------------------
    | Videos can have size up to 3GB.
    | We have to make sure that user can view it.
    |
    */
    'url_expiration' => [
        'for_advertiser' => 600,  // 10 hours
        'for_haapi'      => 4320, // 72 hours
    ],
];
