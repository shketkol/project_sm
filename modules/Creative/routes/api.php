<?php

Route::middleware(['auth:web,admin'])->group(function () {

    Route::group([
        'prefix' => '/{creative}',
        'where'  => [
            'creative' => '[0-9]+',
        ],
    ], function () {

        Route::get('/show', 'ShowCreativeController')->name('show')->middleware('can:creative.view,creative');
        Route::get('/errors', 'IndexCreativeErrorsController')
            ->name('errors.index')
            ->middleware('can:creative.view,creative');
        Route::delete('/', 'DeleteCreativeController')
            ->name('delete')
            ->middleware('can:creative.delete,creative');
    });

    Route::get('/upload-configuration', 'CreativeUploadConfigurationController')
        ->name('configuration')
        ->middleware('can:creative.create');
    Route::get('/server-time', 'GetServerTimeController')
        ->name('server-time')
        ->middleware('can:creative.create');
    Route::post('/store', 'StoreCreativeController')->name('store')->middleware('can:creative.create');
    Route::post('/store-rejected', 'StoreRejectedCreativeController')
        ->name('store-rejected')
        ->middleware('can:creative.create');
});
