<?php

Route::middleware(['auth:web,admin'])->group(function () {
    $throttle = config('throttle.creative_upload');

    Route::get('/aws-auth', 'AwsSignatureController')
        ->name('aws.auth')
        ->middleware('can:creative.create', "throttle:{$throttle},1,creative_upload");
});
