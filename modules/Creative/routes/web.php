<?php

Route::middleware(['auth:web,admin'])->group(function () {
    Route::get('/', 'IndexCreativeController')->name('index')->middleware('can:creative.list');
    Route::get('/{creative}', 'IndexCreativeController')->name('show');
});
