<?php

return [
    'creative_gallery'    => 'Ad Gallery',
    'cancel-upload'       => 'Cancel Upload',
    'drag_drop_area'      => 'Drop a video file here',
    'upload_video'        => 'Upload Video',
    'format'              => 'Format',
    'or'                  => 'or',
    'max_file_size'       => 'Max File Size',
    'duration'            => 'Duration',
    'seconds'             => 'Seconds',
    'video_creatives'     => 'Video Ads',
    'cancel_file_warning' => 'Do you want to stop uploading the file?',
    'creative_preview'    => 'Ad Preview',
    'creative_issues'     => 'Ad Issues',
    'how_it_works'        => 'How it Works',
    'success_stories'     => 'Success Stories',
    'resources'           => 'Resources',
    'insights'            => 'Insights',
    'creative_hub'        => 'Creative hub',
];
