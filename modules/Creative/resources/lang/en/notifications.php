<?php

return [
    'advertiser' => [
        'approved' => 'Your Ad :creative_name has been approved and is now ready to be used in campaigns. You can view it in the :creative_gallery. If you have any questions, please use the Contact Us form.',
        'rejected' => 'Your Ad :creative_name has not passed Ad review due to :reason. You can reupload your Ad once you’ve addressed the issues. Please see the Ad gallery for more details.',
        'missing'  => 'Your :campaign_name campaign is scheduled to start soon but it\'s missing an approved Ad. To avoid delays, upload your Ad as soon as possible.',
        'removed'  => 'Your “:creative_name” Ad was deleted because we delete Ads from campaigns that aren\'t submitted within 30 days of their creation. If you plan to submit your campaign, you can re-upload the Ad at any time.',
    ],
];
