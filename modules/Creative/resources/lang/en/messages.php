<?php

return [
    'success_title'                => 'Ad was successfully uploaded',
    'error_title'                  => 'An error occurred, please try again',
    'validation_error'             => 'Something is wrong with your video',
    'learn_more'                   => 'Learn More',
    'creative_review'              => 'Remember to upload an Ad as soon as possible, so we have at least 3 business days to review your Ad before your campaign starts.',
    'external_review'              => 'Please note that all Ads will be reviewed for compliance with Hulu’s <a href=":tech_specifications_link" target="_blank">Technical Specifications</a> and <a href=":advertising_policies" target="_blank">Advertising Policies</a>. Ads must be approved in order for a campaign to run. Ad review may take up to 3 business days. You will be notified if your Ad is approved or rejected.',
    'remind'                       => 'Please remember to upload your Ad as soon as possible,
                                    but no later than 72 hours before your campaign starts!',
    'replace_creative_message'     => 'Your campaign has been approved and is ready to go live. You can replace the Ad with a previously approved Ad, but this may result in a delay to account for the approval process.',
    'rejected_creative_message'    => 'Your Ad must be updated to meet Hulu\'s <a href=":tech_specifications_link" target="_blank">Technical Specifications</a> and <a href=":advertising_policies" target="_blank">Advertising Policies</a>. To avoid missing the scheduled start date, update the Ad as soon as possible, as it may take up to 3 business days for review.',
    'remind_review'                => 'If your Ad is ready, upload it now. Otherwise, skip this step. To avoid missing the scheduled start date, upload your Ad as soon as possible, as it may take up to 3 business days for review.',
    'review'                       => '
        <p>Upload an Ad video file or select one from your Ad Gallery.</p>
        <p>All Ads are reviewed for compliance with Hulu’s <a href=":tech_specifications_link" target="_blank">Technical Specifications</a> and <a href=":advertising_policies" target="_blank">Advertising Policies</a>. Ads must be approved in order for your campaign to run, and you will be notified if your Ad is approved or rejected. <a href=":ad_approval_link" target="_blank">Learn more about the Ad approval process</a></p>
    ',
    'replace_creative'             => 'Remove Ad',
    'replace_creative_question'    => 'Do you want to remove this Ad from the campaign?',
    'replace_creative_notice'      => 'After you remove the Ad, you must upload a new Ad or attach an existing one from the Ad gallery below. Otherwise, your campaign will not go live.',
    'remove_creative'              => 'Remove Ad',
    'remove_creative_question'     => 'Do you want to remove this Ad from the campaign?',
    'remove_creative_notice'       => 'It will remain in your Ad Gallery.',
    'close_note'                   => 'To avoid interrupting the process, please keep the window open.',
    'video_issues'                 => 'Issues we found with your video',
    'delete_file'                  => 'Delete Ad',
    'delete_file_warning'          => 'Do you want to delete this Ad?',
    'delete_file_note'             => 'It will be permanently deleted.',
    'delete_file_success'          => 'Ad was successfully deleted',
    'video_issues_notice'          => 'It looks like you need to update your Ad to meet Hulu\'s <a href=":tech_specifications_link" target="_blank">Technical Specifications</a> and <a href=":advertising_policies" target="_blank">Advertising Policies</a>.',
    'video_issues_warning'         => 'Something\'s not right with your video Ad. To learn more, please review the list below.',
    'there_are_no_creatives'       => 'There are no Ads yet!',
    'create_campaign_to_upload_ad' => 'Create a campaign to upload an Ad',
    'is_unique'                    => 'Ad is unique and can be uploaded',
    'manual_validation'            => 'Manual Validation',
    'campaign_being_processed'     => 'Your Campaign is currently being processed. Please allow a few minutes for processing and then you’ll be able to manage your Ads.',
    'creative_limit'               => 'It looks like you\'ve already uploaded 10 Ads. To add more, you either need to delete and replace an existing draft or associate an Ad with a campaign to send it for review.',
    'creative_limit_title'         => 'Draft Ad Limit',
    'replace_to_approved'          => 'Oops! You can not apply Ad in this state to campaign',
    'replace_to_processing'        => 'Oops! You can not apply Ad in this state to campaign! Please choose another Ad or wait until the processing is complete.',
    'responsibility_warning_p1'    => 'It is your responsibility to comply with all applicable laws and industry regulations or standards.',
    'responsibility_warning_p2'    => 'Hulu does not guarantee the performance of Ads, campaigns, or that Ads will reach the audience targeted.',
    'campaign_must_have_an_ad'     => 'Please note that your campaign must have an approved Ad that meets Hulu\'s technical specifications and advertising policies. To avoid missing the scheduled start date, upload your Ad as soon as possible, as it may take up to 3 business days for review.',
];
