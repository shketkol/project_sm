<?php

return [
    'admin'      => [
        'for_review' => [
            'subject' => ':publisher_company_full_name Notification: Ad pending review',
            'body'    => [
                'advertiser_has_submitted_a_creative'     => ':advertiser has submitted an Ad, which now requires DSCO approval.',
                'for_more_information_about_the_creative' => 'For more information about the Ad, go to :campaign_details.',
            ],
        ],
        'rejected'   => [
            'subject' => ':publisher_company_full_name Notification: Ad rejected',
            'body'    => [
                'the_creative_creative_has_been_rejected' => 'The :creative_name Ad has been rejected by DSCO for the following reason: :reason.',
                'for_more_information_about_the_creative' => 'For more information about the Ad, see :advertiser_details - :creative_details.',
            ],
        ],
    ],
    'advertiser' => [
        'approved' => [
            'subject' => 'Your Ad was approved',
            'title'   => 'Ad Was Approved',
            'body'    => [
                'great_news_we_have_approved_your_creative' => 'Great news! We’ve approved your :creative_name Ad! Your campaign is now ready to go live.',
                'more_information_about_the_creative'       => 'For more information about your Ad, see :creative_gallery.',
            ],
        ],
        'rejected' => [
            'subject' => 'Your Ad requires updates',
            'title'   => 'Ad Requires Updates',
            'body'    => [
                'thank_you_for_submitting_your_creative' => 'Thank you for submitting your :creative_name Ad. After review, it appears that our :ad_policies_link and/or :technical_specifications_link for this Ad have not been met because: :reason.',
                'for_more_information_about_creative'    => 'For more information about the Ad and how you can make changes, see :creative_gallery.',
            ],
        ],
        'missing'  => [
            'subject' => 'Missing Ad',
            'title'   => 'Missing Ad',
            'body'    => [
                'your_campaign_is_scheduled'           => 'Your <i>:campaign_name</i> campaign is scheduled to go live on :start_date, but it seems like we’re still missing your Ad.',
                'to_avoid_any_delays_with_your_launch' => 'To avoid any delays with your launch, go to :campaign_details and upload your Ad as soon as possible. (Please note that it takes about three business days to review Ads).',
            ],
        ],
    ],
];
