<?php

use Modules\Creative\Models\CreativeStatus;

return [
    'creative'                     => [
        /**
         * Creative statuses
         */
        'statuses' => [
            CreativeStatus::DRAFT            => 'Draft',
            CreativeStatus::APPROVED         => 'Approved',
            CreativeStatus::REJECTED         => 'Rejected',
            CreativeStatus::PENDING_APPROVAL => 'Pending Approval',
            CreativeStatus::MISSING          => 'Needs Ad',
            CreativeStatus::PROCESSING       => 'Processing',
        ],

        /**
         * Creative cards titles
         */
        'cards'    => [
            'approved_creatives'       => 'Approved Ads',
            'creatives_pending_review' => 'Ads Pending Review',
            'draft_creatives'          => 'Draft Ads',
        ],

        'filters' => [
            'oldest'      => 'Oldest',
            'newest'      => 'Newest',
            'status'      => 'Ad Status',
            'upload_date' => 'Upload Date',
        ],
    ],
    'select_ad'                    => 'Select from ad gallery',
    'creative_details'             => 'Ad Details',
    'ad_details'                   => 'Ad Details',
    'advertiser_details'           => 'Advertiser Details',
    'status'                       => 'Ad Status',
    'preview_is_not_available'     => 'No video preview available',
    'prores_preview'               => 'Your video Ad has been uploaded successfully, however, you can\'t preview ProRes videos in your browser. We\'ll convert it after it is reviewed.',
    'enter_fullscreen'             => 'Enter Fullscreen',
    'exit_fullscreen'              => 'Exit Fullscreen',
    'technical_specifications'     => 'Technical Specifications',
    'advertising_policies'         => 'Advertising Policies',
    'remove_ad'                    => 'Remove Ad',
    'download'                     => 'Download',
    'validating'                   => 'Validating...',
    'uploading'                    => 'Uploading...',
    'upload_anyway'                => 'Upload Anyway',
    'search_creatives_placeholder' => 'Search Video Ads',
];
