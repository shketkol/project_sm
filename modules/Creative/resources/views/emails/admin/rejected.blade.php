@extends('common.email.layout-admin')

@section('body')
    <p style="font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('emails.hi', ['name' => $firstName]) }}
    </p>

    <p style="font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('creative::emails.admin.rejected.body.the_creative_creative_has_been_rejected', [
            'creative_name' => $creativeName,
            'reason' => $reason,
        ]) }}
    </p>

    <p style="font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('creative::emails.admin.rejected.body.for_more_information_about_the_creative', [
            'advertiser_details' => view('common.email.part.link', [
                'link' => $advertiserDetails,
                'text' => __('creative::labels.advertiser_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
             'creative_details' => view('common.email.part.link', [
                'link' => $creativeDetails,
                'text' => __('creative::labels.ad_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>
@stop
