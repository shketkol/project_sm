@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('creative::emails.advertiser.approved.body.great_news_we_have_approved_your_creative', [
            'creative_name'          => view('common.email.part.link', [
                'link' => $creativeDetails,
                'text' => $creativeName,
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('creative::emails.advertiser.approved.body.more_information_about_the_creative', [
            'creative_gallery' => view('common.email.part.link', [
                'link' => $creativeDetails,
                'text' => __('creative::labels.ad_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    @include('common.email.part.if-you-have-any-questions')
@stop
