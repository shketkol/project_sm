@extends('common.email.layout')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!!   __('creative::emails.advertiser.rejected.body.thank_you_for_submitting_your_creative', [
            'creative_name' => view('common.email.part.link-without-margin', [
                'link' => $creativeDetails,
                'text' => $creativeName,
            ])->render(function ($content) {
                    return trim($content);
                }),
            'reason'                        => e($reason),
            'technical_specifications_link' => view('common.email.part.link-without-margin', [
                'link' => $techSpecificationsLink,
                'text' => __('creative::labels.technical_specifications'),
            ])->render(function ($content) {
                    return trim($content);
                }),
            'ad_policies_link'              => view('common.email.part.link-without-margin', [
                'link' => $adPoliciesLink,
                'text' => __('creative::labels.advertising_policies'),
            ])->render(function ($content) {
                    return trim($content);
                }),

        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!!  __('creative::emails.advertiser.rejected.body.for_more_information_about_creative', [
            'creative_gallery' => view('common.email.part.link', [
                'link' => $creativeDetails,
                'text' => __('creative::labels.ad_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    @include('common.email.part.if-you-have-any-questions')
@stop
