@extends('layouts.app')
@section('content')
    <div class="vue-app">
        <creatives-index />
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/creative.js') }}"></script>
@endpush
