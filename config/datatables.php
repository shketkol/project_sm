<?php

return [
    /*
    | DataTables search options.
    */
    'search'       => [
        /*
        | Multi-term search will explode search keyword using spaces resulting into multiple term search.
        */
        'multi_term' => false,
    ],

    /*
    |--------------------------------------------------------------------------
    | Allowed tags to show
    |--------------------------------------------------------------------------
    */
    'allowed_tags' => implode('', [
        '<a>',
    ]),

    /*
    |--------------------------------------------------------------------------
    | Checking statuses interval
    |--------------------------------------------------------------------------
    |
    | This option defines interval between checking statuses
    |
    */
    'check_statuses_interval' => env('CHECK_STATUSES_INTERVAL', 5000), // In milliseconds
];
