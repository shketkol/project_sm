<?php

use App\Services\LoggerService\Formatter\MaskingFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;
use App\Services\LoggerService\Factory\CloudWatchLoggerFactory;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver'            => 'stack',
            'channels'          => ['daily'],
            'ignore_exceptions' => false,
            'formatter'         => MaskingFormatter::class,
        ],

        'single' => [
            'driver'    => 'single',
            'path'      => storage_path('logs/laravel.log'),
            'level'     => 'debug',
            'formatter' => MaskingFormatter::class,
        ],

        'daily' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/laravel.log'),
            'level'     => 'debug',
            'days'      => 14,
            'formatter' => MaskingFormatter::class,
        ],

        'slack' => [
            'driver'   => 'slack',
            'url'      => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji'    => ':boom:',
            'level'    => 'critical',
        ],

        'papertrail' => [
            'driver'       => 'monolog',
            'level'        => 'debug',
            'handler'      => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver'    => 'monolog',
            'handler'   => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with'      => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level'  => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level'  => 'debug',
        ],

        'cloudwatch' => [
            'driver'    => 'custom',
            'via'       => CloudWatchLoggerFactory::class,
            'formatter' => MaskingFormatter::class,
            'with'      => [
                'region'          => env('AWS_CLOUDWATCH_DEFAULT_REGION', ''),
                'group_name'      => env('AWS_CLOUDWATCH_GROUP_NAME', ''),
                'stream_name'     => env('AWS_CLOUDWATCH_STREAM_NAME', ''),
                'version'         => 'latest',
                'retention'       => env('AWS_CLOUDWATCH_LOG_RETENTION'),
                'batch'           => env('AWS_CLOUDWATCH_BATCH', 10000),
                'format'          => "%level_name%: %message% %context% %extra%\n",
                'allowLineBreaks' => false,
            ],

        ],
    ],

];
