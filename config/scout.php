<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Search Engine
    |--------------------------------------------------------------------------
    |
    | This option controls the default search connection that gets used while
    | using Laravel Scout. This connection is used when syncing all models
    | to the search service. You should adjust this based on your needs.
    |
    | Supported: "algolia", "null", "elasticsearch"
    |
    */

    'driver' => env('SCOUT_DRIVER', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Index Prefix
    |--------------------------------------------------------------------------
    |
    | Here you may specify a prefix that will be applied to all search index
    | names used by Scout. This prefix may be useful if you have multiple
    | "tenants" or applications sharing the same search infrastructure.
    |
    */

    'prefix' => env('SCOUT_PREFIX', ''),

    /*
    |--------------------------------------------------------------------------
    | Queue Data Syncing
    |--------------------------------------------------------------------------
    |
    | This option allows you to control if the operations that sync your data
    | with your search engines are queued. When this is set to "true" then
    | all automatic data syncing will get queued for better performance.
    |
    */

    'queue' => env('SCOUT_QUEUE', false),

    /*
    |--------------------------------------------------------------------------
    | Chunk Sizes
    |--------------------------------------------------------------------------
    |
    | These options allow you to control the maximum chunk size when you are
    | mass importing data into the search engine. This allows you to fine
    | tune each of these chunk sizes based on the power of the servers.
    |
    */

    'chunk' => [
        'searchable'   => 500,
        'unsearchable' => 500,
    ],

    /*
    |--------------------------------------------------------------------------
    | Soft Deletes
    |--------------------------------------------------------------------------
    |
    | This option allows to control whether to keep soft deleted records in
    | the search indexes. Maintaining soft deleted records can be useful
    | if your application still needs to search for the records later.
    |
    */

    'soft_delete' => false,

    /*
    |--------------------------------------------------------------------------
    | Algolia Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure your Algolia settings. Algolia is a cloud hosted
    | search engine which works great with Scout out of the box. Just plug
    | in your application ID and admin API key to get started searching.
    |
    */

    'algolia' => [
        'id'     => env('ALGOLIA_APP_ID', ''),
        'secret' => env('ALGOLIA_SECRET', ''),
    ],

    /*
     * @see https://github.com/yabhq/laravel-scout-mysql-driver
     */
    'mysql' => [
        /*
         * Available modes TLDR:
         * 1. "NATURAL_LANGUAGE"
         *    - requires full text index on string columns
         *    - would found 'West New York, NJ' if you type 'west york'
         *    - would not found 'Washington' if you type not full word for example 'wash' or even 'washingto'
         * 2. "BOOLEAN":
         *    - same as "NATURAL_LANGUAGE" but can include or exclude fields:
         *       "+apple -banana" - would include 'apple' and exclude 'banana' from results
         * 3. "BOOLEAN_WILDCARD":
         *    - same as "BOOLEAN" but query wrapped with '*':
         *       "*wash*" - would search anything before and after 'wash'
         * 2. "LIKE" - query '%west york%'
         *    - would found 'Washington' if you type not full word for example 'wash'
         *    - would not found 'West New York, NJ' if you type 'west york'
         * 4. "LIKE_EXPANDED" - query "'%west%' or '%york%'"
         *    - would found 'Washington' if you type not full word for example 'wash'
         *    - would found 'West New York, NJ' if you type 'west york' but it would be not in first results
         */
        'mode'                         => 'BOOLEAN_WILDCARD',
        'min_search_length'            => 0,
        'min_fulltext_search_length'   => 4,
        'min_fulltext_search_fallback' => 'LIKE',
        'query_expansion'              => false,
    ],
];
