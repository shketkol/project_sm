<?php

/*
|--------------------------------------------------------------------------
| HTML Purifier https://github.com/ezyang/htmlpurifier
| http://htmlpurifier.org/docs
|
| All allowed config options: vendor/ezyang/htmlpurifier/library/HTMLPurifier/ConfigSchema/schema
|--------------------------------------------------------------------------
|
*/
return [
    /*
    |--------------------------------------------------------------------------
    | HTML
    |--------------------------------------------------------------------------
    |
    */
    'HTML.Allowed'         => implode(',', [
        'strong', 'b', 'em', 'i', 's', 'u',
        'a[href|rel]', 'p',
        'h1', 'h2', 'h3',
        'ul', 'li', 'ol',
    ]),
    'HTML.Nofollow'        => true,
    'HTML.TargetBlank'     => true,

    /*
    |--------------------------------------------------------------------------
    | Cache disabled since prod has read-only file system
    |--------------------------------------------------------------------------
    | If performance would be slow, possible to add custom cache driver (for example redis)
    | https://stackoverflow.com/questions/61099566
    |
    */
    'Cache.DefinitionImpl' => null,
];
