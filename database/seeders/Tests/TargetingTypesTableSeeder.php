<?php

namespace Database\Seeders\Tests;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Modules\Targeting\Models\Type;

class TargetingTypesTableSeeder extends Seeder
{
    /**
     * Targeting types array.
     */
    protected $types = [
        Type::HAAPI_AGE_GROUP_NAME,
        Type::HAAPI_GENRE_NAME,
        Type::HAAPI_DEVICE_NAME,
        Type::HAAPI_ZIPCODE_NAME,
        Type::HAAPI_DMA_NAME,
        Type::HAAPI_CITY_NAME,
        Type::HAAPI_STATE_NAME,
        Type::HAAPI_AUDIENCE_NAME,
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        foreach ($this->types as $type) {
            Type::create(['name' => $type, 'external_id' => Str::uuid()]);
        }
    }
}
