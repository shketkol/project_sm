<?php

namespace Database\Seeders\Tests;

use Illuminate\Database\Seeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;

class TestDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(UserStatusesTableSeeder::class);
        $this->call(TargetingTypesTableSeeder::class);
    }
}
