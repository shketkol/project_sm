<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\User\Models\Role;
use Modules\User\Models\Permission;

class BasePermissionsTableSeeder extends Seeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [],
        Role::ID_ADMIN_READ_ONLY => [],
        Role::ID_ADVERTISER      => [],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        foreach ($this->permissions as $roleId => $permissions) {
            /** @var Role $role */
            $role = Role::findById($roleId);

            foreach ($permissions as $name) {
                /** @var Permission $permission */
                $permission = Permission::firstOrCreate(compact('name'));

                if (!$role->hasPermissionTo($permission)) {
                    $role->givePermissionTo($permission);
                }
            }
        }
    }
}
