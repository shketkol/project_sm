<?php

namespace Database\Seeders;

use Modules\User\Models\Role;

class FaqSectionPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    private const READ_FAQ_SECTION = 'read_faq_section';

    /**
     * @var array
     */
    protected $permissions = [
        Role::ID_ADVERTISER      => [self::READ_FAQ_SECTION],
        Role::ID_ADMIN           => [self::READ_FAQ_SECTION],
        Role::ID_ADMIN_READ_ONLY => [self::READ_FAQ_SECTION],
    ];
}
