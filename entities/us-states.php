<?php
use Modules\Location\Models\UsState;

return UsState::all()->pluck('name')->toArray();
