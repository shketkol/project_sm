<?php

use App\Models\Timezone;

return Timezone::all()->toArray();
