<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Modules\User\Models\User;

class Mail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Mail payload.
     *
     * @var array
     */
    protected $payload;

    /**
     * Mail constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload = [])
    {
        $this->payload = array_merge($this->getDefaultPayloadValues(), $payload);
    }

    /**
     * Set the subject of the message.
     *
     * @param string $subject
     * @return $this
     */
    public function subject($subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Set the recipients of the message.
     *
     * @param object|array|string $address
     * @param string|null $name
     * @return $this
     */
    public function to($address, $name = null): self
    {
        if ($address instanceof User && $address->isAdmin()) {
            $address = config('general.admin_support_email');
        }

        return $this->setAddress($address, $name, 'to');
    }

    /**
     * Remove all the recipients.
     */
    public function resetRecipients(): void
    {
        $this->to = [];
    }

    /**
     * Get payload value by key.
     *
     * @param string $key
     * @param string $default
     * @return mixed
     */
    protected function getPayloadValue(string $key, string $default = null)
    {
        return Arr::get($this->payload, $key, $default);
    }

    /**
     * @return array
     */
    protected function getDefaultPayloadValues(): array
    {
        return [
            'titleIcon' => 'marked',
            'title'     => __('emails.default_title'),
        ];
    }
}
