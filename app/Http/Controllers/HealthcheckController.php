<?php

namespace App\Http\Controllers;

use App\Services\HealthCheckService\Contracts\HealthCheckService as HealthCheckServiceInterface;
use Illuminate\Http\Response;

class HealthcheckController extends Controller
{
    protected const MAINTENANCE_RESPONSE_CODE = 250;

    /**
     * Respond with
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function __invoke(HealthCheckServiceInterface $healthService): Response
    {
        $responseCode = app()->isDownForMaintenance()
            ? self::MAINTENANCE_RESPONSE_CODE
            : Response::HTTP_OK;

        if (!app()->isDownForMaintenance()) {
            $healthService->checkResources();
        }

        return response('Healthy!', $responseCode)
            ->header('Content-Type', 'text/plain');
    }
}
