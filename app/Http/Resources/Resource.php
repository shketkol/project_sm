<?php

namespace App\Http\Resources;

use App\Http\Requests\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class Resource extends JsonResource
{
    /**
     * @return array
     */
    public function toRawData(): array
    {
        $data = (array)$this->toResponse(new Request())->getData()->data;

        foreach ($data as &$value) {
            if (is_object($value)) {
                $value = (array)$value;
            }
        }

        return $data;
    }
}
