<?php

namespace App\Http\View\Composers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\View\View;

class AnalyticsComposer
{
    /**
     * View composer will be attached to these views.
     *
     * @var array
     */
    public static $views = [
        'layouts.top.analytics',
    ];

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     * @throws \Exception
     */
    public function compose(View $view)
    {
        $user = app(Authenticatable::class);

        $view->with([
            'tealiumEnabled' =>
                config('analytics.enabled') &&
                config('analytics.services.tealium.enabled') &&
                !($user && $user->isAdmin()),
            'spaMode'        => config('analytics.services.tealium.spa_mode')
        ]);
    }
}
