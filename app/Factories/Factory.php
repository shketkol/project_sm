<?php

namespace App\Factories;

use Illuminate\Database\Eloquent\Factories\Factory as BaseFactory;

abstract class Factory extends BaseFactory
{
    /**
     * Laravel 8 Factories changed the way to override model properties from default
     * This method allows to easily override any model property.
     *
     * CAUTION!
     *
     * Would work as expected.
     * $factory = Report::factory()
     *      ->override($attributes)
     *      ->create();
     *
     * Would not work as expected. Only `create` method attributes would apply.
     * $factory = Report::factory()
     *      ->override($attributes)
     *      ->create($otherAttributes);
     *
     * @param array $override
     *
     * @return $this
     */
    public function override(array $override): self
    {
        return $this->state(function (array $attributes) use ($override): array {
            return array_merge($attributes, $override);
        });
    }
}
