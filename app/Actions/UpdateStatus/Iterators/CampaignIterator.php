<?php

namespace App\Actions\UpdateStatus\Iterators;

use Iterator;
use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Admin\Campaign\AdminCampaignSearch;
use Modules\Haapi\DataTransferObjects\Campaign\AdminCampaignSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Iterators\PaginationIterator;

class CampaignIterator extends PaginationIterator implements Iterator
{
    use ActAsAdmin;

    /**
     * @var AdminCampaignSearch
     */
    private $campaignSearch;

    /**
     * @var int
     */
    private $adminId;

    /**
     * @var string
     */
    private $accountId;

    /**
     * @param AdminCampaignSearch $campaignSearch
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(AdminCampaignSearch $campaignSearch)
    {
        $this->campaignSearch = $campaignSearch;
        $this->adminId = $this->authAsAdmin();
    }

    /**
     * @param string $accountId
     * @return CampaignIterator
     */
    public function getCampaigns(string $accountId): self
    {
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * Send request to load data.
     *
     * @return HaapiResponse
     * @throws \App\Exceptions\BaseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    protected function load(): HaapiResponse
    {
        return $this->campaignSearch->handle(
            $this->createParam(AdminCampaignSearchParam::class, [
                'accountId' => $this->accountId,
            ]),
            $this->adminId
        );
    }

    /**
     * Get key of data that should be iterated.
     *
     * @return string
     */
    protected function getResponseKey(): string
    {
        return 'campaigns';
    }
}
