<?php

namespace App\Actions\UpdateStatus;

use App\Actions\UpdateStatus\Iterators\CreativeIterator;
use App\Actions\UpdateStatus\Traits\GetAdvertisers;
use Illuminate\Support\Arr;
use Modules\Creative\Exceptions\CreativeNotFoundException;
use Modules\Creative\Repositories\CreativeRepository;
use Modules\Creative\Actions\Notifications\UpdateCreativeStatusAction;
use Modules\User\Repositories\UserRepository;
use Psr\Log\LoggerInterface;

class CreativeUpdateAction
{
    use GetAdvertisers;

    /**
     * @var CreativeRepository
     */
    private $creativeRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UpdateCreativeStatusAction
     */
    private $updateStatusAction;

    /**
     * @var CreativeIterator
     */
    private $creativeIterator;

    /**
     * CreativeUpdateAction constructor.
     * @param UserRepository $userRepository
     * @param LoggerInterface $log
     * @param CreativeRepository $creativeRepository
     * @param UpdateCreativeStatusAction $updateStatusAction
     */
    public function __construct(
        UserRepository $userRepository,
        LoggerInterface $log,
        CreativeRepository $creativeRepository,
        UpdateCreativeStatusAction $updateStatusAction
    ) {
        $this->creativeRepository = $creativeRepository;
        $this->updateStatusAction = $updateStatusAction;
        $this->userRepository = $userRepository;
        $this->log = $log;
    }


    /**
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Daapi\Exceptions\AdminNotFoundException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \SM\SMException
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->creativeIterator = app(CreativeIterator::class);
        $advertisers = $this->getAdvertiserList();

        foreach ($advertisers as $advertiser) {
            if (empty($advertiser->creatives)) {
                continue;
            }

            $creatives = $this->creativeIterator->getCreatives($advertiser->account_external_id);

            foreach ($creatives as $creative) {
                try {
                    $this->updateStatusAction->handle(Arr::get($creative, 'id'), Arr::get($creative, 'status'), []);
                } catch (CreativeNotFoundException $exception) {
                    $this->log->warning('Creative not found', [
                        'externail_id' => Arr::get($creative, 'id'),
                    ]);
                }
            }
        }
    }
}
