<?php

namespace App\Actions\File;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Request;

/**
 * Class UploadImageAction
 * Uploads image file from request to storage
 * @package App\Actions\File
 */
class UploadImageAction
{
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var Filesystem
     */
    private $storage;

    /**
     * UploadImageAction constructor.
     * @param \Illuminate\Http\Request $request
     * @param Filesystem $storage
     */
    public function __construct(Request $request, Filesystem $storage)
    {
        $this->request = $request;
        $this->storage = $storage;
    }

    /**
     * Store image in 'images' folder
     * @param string $field
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(string $field)
    {
        $imageContent = $this->request->file($field)->get();
        $imageExtension = $this->request->file($field)->getClientOriginalExtension();
        $imageName = implode('.', [md5(time()), $imageExtension]);
        $imagePath = implode(DIRECTORY_SEPARATOR, ['images', 'faq', $imageName]);
        $this->storage->put($imagePath, $imageContent);

        return $this->storage->url($imagePath);
    }
}
