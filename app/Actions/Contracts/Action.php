<?php

namespace App\Actions\Contracts;

interface Action
{
    /**
     * @return mixed
     */
    public function handle();
}
