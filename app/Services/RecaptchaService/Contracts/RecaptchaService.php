<?php

namespace App\Services\RecaptchaService\Contracts;

interface RecaptchaService
{
    /**
     * Returns true if token is verified and false otherwise.
     *
     * @param string|null $token
     * @return bool
     */
    public function verify(?string $token): bool;

    /**
     * Returns true if ReCaptcha is enabled.
     *
     * @return bool
     */
    public function isEnabled(): bool;
}
