<?php

return [
    'failed' => 'Something\'s not right. Let\'s try again. If you keep receiving this message, please try a different browser or contact Hulu Support.',
];
