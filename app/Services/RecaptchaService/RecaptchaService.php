<?php

namespace App\Services\RecaptchaService;

use App\Services\RecaptchaService\Contracts\RecaptchaService as RecaptchaServiceInterface;
use GuzzleHttp\Client;
use Illuminate\Config\Repository as Config;
use Psr\Log\LoggerInterface;

class RecaptchaService implements RecaptchaServiceInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * RecaptchaService constructor.
     *
     * @param Client $client
     * @param Config $config
     * @param LoggerInterface $log
     */
    public function __construct(Client $client, Config $config, LoggerInterface $log)
    {
        $this->client = $client;
        $this->config = $config;
        $this->log = $log;
    }

    /**
     * Returns true if token is verified and false otherwise.
     *
     * @param string|null $token
     *
     * @return bool
     */
    public function verify(?string $token): bool
    {
        $this->log->info('Verifying recaptcha.', compact('token'));

        if (!$token) {
            $this->log->warning('Recaptcha token is empty.');

            return false;
        }

        $verified = $this->sendRequest($token);

        if (!$verified) {
            $this->log->warning('Recaptcha was not verified.', compact('token'));

            return false;
        }

        $this->log->info('Recaptcha was successfully verified.', compact('token'));

        return true;
    }

    /**
     * Returns true if ReCaptcha is enabled.
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        return config('recaptcha.enabled');
    }

    /**
     * Sends request to the ReCaptcha service to verify the token.
     *
     * @param string|null $token
     *
     * @return bool
     */
    protected function sendRequest(?string $token): bool
    {
        /** @var \Psr\Http\Message\ResponseInterface $response */
        $response = $this->client->post($this->getUrl(), [
            'form_params' => [
                'secret'   => $this->getSecret(),
                'response' => $token,
            ],
        ]);

        $result = json_decode($response->getBody()->getContents(), true);

        return $this->checkResult($result);
    }

    /**
     * Returns true if result is successfull.
     *
     * @param array $result
     *
     * @return bool
     */
    protected function checkResult(array $result): bool
    {
        $success = $result['success'] ?? false;
        $score = $result['score'] ?? 0;

        return $success && $score >= $this->getMinScore();
    }

    /**
     * Get URL for the verification request.
     *
     * @return string
     */
    protected function getUrl(): string
    {
        return $this->config->get('recaptcha.verification_url');
    }

    /**
     * Get secret key for the verification request.
     *
     * @return string
     */
    protected function getSecret(): string
    {
        return $this->config->get('recaptcha.secret_key');
    }

    /**
     * Get minimum score that should be interpreted as a human.
     *
     * @return float
     */
    protected function getMinScore(): float
    {
        return $this->config->get('recaptcha.min_score');
    }
}
