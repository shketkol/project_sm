<?php

namespace App\Services\RecaptchaService;

use App\Services\RecaptchaService\Contracts\RecaptchaService as RecaptchaServiceInterface;
use Illuminate\Support\ServiceProvider;

class RecaptchaServiceProvider extends ServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [
        RecaptchaServiceInterface::class => RecaptchaService::class,
    ];

    /**
     * Boot application.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'recaptcha');
        $this->mergeConfigFrom(__DIR__ . '/config/recaptcha.php', 'recaptcha');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', 'recaptcha');
    }
}
