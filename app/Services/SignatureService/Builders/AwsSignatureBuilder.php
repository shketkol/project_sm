<?php

namespace App\Services\SignatureService\Builders;

use App\Services\CredentialsService\Contracts\AwsCredentials;
use App\Services\SignatureService\HashHmacSignature;
use App\Services\SignatureService\Validator\CanonicalRequestValidator;
use Aws\Credentials\Credentials;
use Carbon\Carbon;
use Modules\Creative\Http\Requests\AwsSignatureRequest;
use App\Services\SignatureService\Contracts\AwsSignatureBuilder as AwsSignatureBuilderContract;

class AwsSignatureBuilder implements AwsSignatureBuilderContract
{
    /**
     * @var HashHmacSignature
     */
    protected $signature;

    /**
     * @var AwsSignatureRequest
     */
    public $request;

    /**
     * @var CanonicalRequestValidator
     */
    protected $canonicalRequestValidator;

    /**
     * @var Credentials
     */
    protected $awsCredentials;

    /**
     * AwsSignatureBuilder constructor.
     *
     * @param HashHmacSignature $signature
     * @param AwsSignatureRequest $request
     * @param CanonicalRequestValidator $canonicalRequestValidator
     * @param AwsCredentials $awsCredentials
     */
    public function __construct(
        HashHmacSignature $signature,
        AwsSignatureRequest $request,
        CanonicalRequestValidator $canonicalRequestValidator,
        AwsCredentials $awsCredentials
    ) {
        $this->signature = $signature;
        $this->request = $request;
        $this->canonicalRequestValidator = $canonicalRequestValidator;
        $this->awsCredentials = $awsCredentials->get();
    }

    /**
     * @return string
     */
    protected function kDate(): string
    {
        $date = $this->request->get('datetime');
        $formattedDate = Carbon::parse($date)->format('Ymd');
        $secret = 'AWS4' . $this->awsCredentials->getSecretKey();
        return $this->signature->makeHash(
            'sha256',
            $formattedDate,
            $secret
        );
    }

    /**
     * @return string
     */
    protected function kRegion(): string
    {
        $defaultRegion = config('filesystems.disks.s3.region');
        return $this->signature->makeHash(
            'sha256',
            $defaultRegion,
            $this->signature->lastValue
        );
    }

    /**
     * @return string
     */
    protected function kService(): string
    {
        return $this->signature->makeHash(
            'sha256',
            's3',
            $this->signature->lastValue
        );
    }

    /**
     * @return string
     */
    protected function kSigning(): string
    {
        return $this->signature->makeHash(
            'sha256',
            'aws4_request',
            $this->signature->lastValue
        );
    }

    /**
     * @return string
     * @throws \App\Services\SignatureService\Exceptions\InvalidToSignException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getSignature(): string
    {
        $this->canonicalRequestValidator->validate($this->request->all());

        $this->kDate();
        $this->kRegion();
        $this->kService();
        $this->kSigning();

        return $this->signature->makeHash(
            'sha256',
            $this->request->get('to_sign'),
            $this->signature->lastValue,
            false
        );
    }
}
