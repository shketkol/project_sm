<?php

namespace App\Services\SignatureService;

use App\Services\SignatureService\Builders\AwsSignatureBuilder;
use App\Services\SignatureService\Contracts\AwsSignatureBuilder as AwsSignatureBuilderContract;
use Illuminate\Support\ServiceProvider;

class SignatureServiceProvider extends ServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [
        AwsSignatureBuilderContract::class => AwsSignatureBuilder::class,
    ];
}
