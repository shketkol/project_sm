<?php

namespace App\Services\SignatureService\Validator;

use App\Services\SignatureService\Exceptions\InvalidToSignException;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Modules\Creative\Actions\GetCreativeUploadPathAction;
use Modules\Creative\Helpers\CreativePathHelper;

class CanonicalRequestValidator
{
    /**
     * Indexes for canonical request data.
     * to_sign string example
     *
     * POST\n
     * /cfece4bfde7f866d7b51dab9185cb5a84a9efa6b23784a9b77799f3d67e8f3eb/creatives/4402f81a-4654-40b4-bea5-1c44f3b8c60b.mp4\n
     * uploads=\n
     * host:da-hulu-demo-usercontent.s3.amazonaws.com\n
     * x-amz-date:20191227T120755Z\n
     * \n
     * host;x-amz-date\n
     * e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
     */
    private const REQUEST_FILE_NAME_INDEX = 1;


    /**
     * @var GetCreativeUploadPathAction
     */
    private $getUploadPathAction;

    /**
     * @var Authenticatable
     */
    private $user;

    /**
     * @var ValidationRules
     */
    private $validationRules;

    /**
     * @var CreativePathHelper
     */
    private $creativePathHelper;

    /**
     * @param GetCreativeUploadPathAction $getUploadPathAction
     * @param Authenticatable             $user
     * @param ValidationRules             $validationRules
     * @param CreativePathHelper          $creativePathHelper
     */
    public function __construct(
        GetCreativeUploadPathAction $getUploadPathAction,
        Authenticatable $user,
        ValidationRules $validationRules,
        CreativePathHelper $creativePathHelper
    ) {
        $this->getUploadPathAction = $getUploadPathAction;
        $this->user = $user;
        $this->validationRules = $validationRules;
        $this->creativePathHelper = $creativePathHelper;
    }

    /**
     * @param array $request
     *
     * @throws InvalidToSignException
     * @throws ValidationException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function validate(array $request): void
    {
        $this->validateHash(Arr::get($request, 'canonical_request'), Arr::get($request, 'to_sign'));
        $this->validateToSign(Arr::get($request, 'canonical_request'));
    }

    /**
     * Check if hash that come from user is equal to hash we calculated ourselves.
     *
     * @param string $canonicalRequest
     * @param string $toSign
     *
     * @throws InvalidToSignException
     */
    protected function validateHash(string $canonicalRequest, string $toSign): void
    {
        $calculatedHash = hash('sha256', $canonicalRequest);
        $inputHash = Arr::last(explode("\n", $toSign));

        if ($calculatedHash !== $inputHash) {
            throw InvalidToSignException::create('Hash is not equal.');
        }
    }

    /**
     * @param string $canonicalRequest
     *
     * @throws InvalidToSignException
     * @throws ValidationException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function validateToSign(string $canonicalRequest): void
    {
        $inputPath = ltrim(Arr::get(explode("\n", $canonicalRequest), self::REQUEST_FILE_NAME_INDEX), '/');
        $this->checkIfCreativeWithPathExists($inputPath);

        $inputParts = explode('/', $inputPath);
        $this->validateParts($inputParts);

        $this->checkIfUserHasAccessToFolder($inputPath);
        $this->validateFileName($inputParts);
    }

    /**
     * @param string $path
     *
     * @throws InvalidToSignException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function checkIfCreativeWithPathExists(string $path): void
    {
        if ($this->creativePathHelper->creativeWithPathExists($path)) {
            throw InvalidToSignException::create('File with this name already exists. Prevented rewrite.');
        }
    }

    /**
     * @param array $inputParts
     *
     * @throws InvalidToSignException
     * @throws ValidationException
     */
    protected function validateFileName(array $inputParts): void
    {
        $parts = explode('.', Arr::get($inputParts, CreativePathHelper::FILE_NAME_INDEX));

        // my_creative.mp4
        if (count($parts) !== 2) {
            throw InvalidToSignException::create('Invalid file name.');
        }

        [$name, $extension] = $parts;

        $validator = Validator::make(
            ['name' => $name, 'extension' => $extension],
            [
                'name'      => $this->validationRules->only(
                    'uuid',
                    ['required', 'string', 'regex']
                ),
                'extension' => $this->validationRules->only(
                    'creative.general.extension',
                    ['required', 'included']
                ),
            ]
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    /**
     * @param array $inputParts
     *
     * @throws InvalidToSignException
     */
    protected function validateParts(array $inputParts): void
    {
        if (count($inputParts) !== CreativePathHelper::PATH_PARTS) {
            throw InvalidToSignException::create('Malformed path.');
        }
    }

    /**
     * @param string $inputPath
     *
     * @throws InvalidToSignException
     */
    protected function checkIfUserHasAccessToFolder(string $inputPath): void
    {
        if (!$this->creativePathHelper->hasUserAccessToCreativePath($this->user, $inputPath)) {
            throw InvalidToSignException::create('Unexpected path.');
        }
    }
}
