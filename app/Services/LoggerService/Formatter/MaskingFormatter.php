<?php

namespace App\Services\LoggerService\Formatter;

use App\Services\LoggerService\Traits\Normalizify;
use App\Services\LoggerService\Traits\Stringify;
use Illuminate\Support\Arr;
use Monolog\Formatter\LineFormatter;

class MaskingFormatter extends LineFormatter
{
    use Stringify, Normalizify;

    /**
     * Fields to mask.
     *
     * @var array
     */
    private $fields = [];

    /**
     * Mask sensitive data with.
     *
     * @var string
     */
    private $mask = '';

    /**
     * MaskingFormatter constructor.
     */
    public function __construct()
    {
        /** @see \Illuminate\Log\LogManager::formatter */
        parent::__construct($this->getFormat(), null, $this->allowLineBreaks(), true);
        $this->fields = config('logger.masking.fields');
        $this->mask = config('logger.mask');
    }

    /**
     * Formats a log record.
     *
     * @param array $record A record to format
     *
     * @return mixed The formatted record
     */
    public function format(array $record): string
    {
        if ($this->isEnabled()) {
            $record = $this->maskRecord($record);
        }

        return parent::format($record);
    }

    /**
     * Formats a set of log records.
     *
     * @param array $records A set of records to format
     *
     * @return mixed The formatted set of records
     */
    public function formatBatch(array $records): string
    {
        $records = array_map(function ($record) {
            return $this->format($record);
        }, $records);

        return parent::formatBatch($records);
    }

    /**
     * Get log format output.
     *
     * @return string|null
     */
    protected function getFormat(): ?string
    {
        $channel = config('logging.default');
        $format = config("logging.channels.{$channel}.with.format");

        return $format;
    }

    /**
     * @return bool
     */
    protected function allowLineBreaks(): bool
    {
        $channel = config('logging.default');
        $lineBreaks = config("logging.channels.{$channel}.with.allowLineBreaks", true);

        return $lineBreaks;
    }

    /**
     * @param mixed $record
     *
     * @return mixed
     */
    private function maskRecord($record)
    {
        if (!$this->isMaskable($record)) {
            return $record;
        }

        foreach ($record as $key => $value) {
            if ($this->isMaskable($value)) {
                $record = $this->doMask($record, $key, $this->maskRecord($value));
            }

            if (in_array($key, $this->fields, true)) {
                $record = $this->doMask($record, $key, $this->mask);
            }
        }

        return $record;
    }

    /**
     * @param iterable|array|object $record
     * @param string                $key
     * @param mixed                 $value
     *
     * @return iterable
     */
    private function doMask($record, string $key, $value)
    {
        if (is_object($record)) {
            $record = $this->doMaskObject($record, $key, $value);
        }

        if (is_array($record)) {
            $record = $this->doMaskArray($record, $key, $value);
        }

        return $record;
    }

    /**
     * @param object $record
     * @param string $key
     * @param mixed  $value
     *
     * @return object
     */
    private function doMaskObject(object $record, string $key, $value): object
    {
        if (method_exists($record, 'set')) {
            $record->set($key, $value);
        }

        return $record;
    }

    /**
     * @param array  $record
     * @param string $key
     * @param mixed  $value
     *
     * @return array
     */
    private function doMaskArray(array $record, string $key, $value): array
    {
        $record[$key] = $value;

        return $record;
    }

    /**
     * @param mixed $record
     *
     * @return bool
     */
    private function isMaskable($record): bool
    {
        $isAccessible = is_object($record) || is_array($record);
        if (!$isAccessible) {
            return false;
        }

        if (Arr::get($record, 'context.message') === trans('validation.invalid')) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function isEnabled(): bool
    {
        return (bool)config('logger.masking.enabled');
    }
}
