<?php

namespace App\Services\LoggerService\Factory;

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Illuminate\Support\Arr;
use Monolog\Handler\HandlerInterface;
use Monolog\Logger;
use Maxbanton\Cwh\Handler\CloudWatch;

class CloudWatchLoggerFactory
{
    /**
     * Logger name.
     */
    private const LOGGER_NAME = 'cloudwatch';

    /**
     * @param array $config
     *
     * @return Logger
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function __invoke(array $config): Logger
    {
        /**
         * @var string $group_name
         * @var string $stream_name
         * @var int    $retention
         * @var int    $batch
         */
        extract($config['with'], null);

        // Instantiate AWS SDK CloudWatch Logs Client
        $client = new CloudWatchLogsClient($config["with"]);

        // Instantiate handler (tags are optional)
        $handler = new CloudWatch($client, $group_name, $stream_name, $retention, $batch);

        $this->setFormatter($handler, $config);

        // Create a log channel
        $logger = new Logger(self::LOGGER_NAME);

        // Set handler
        $logger->pushHandler($handler);

        return $logger;
    }

    /**
     * @param HandlerInterface $handler
     * @param array            $config
     */
    protected function setFormatter(HandlerInterface $handler, array $config): void
    {
        if (!Arr::has($config, 'formatter')) {
            return;
        }

        /** @var \Monolog\Formatter\FormatterInterface $formatter */
        $formatter = app(Arr::get($config, 'formatter'));
        $handler->setFormatter($formatter);
    }
}
