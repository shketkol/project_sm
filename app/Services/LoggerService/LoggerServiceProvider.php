<?php

namespace App\Services\LoggerService;

use App\Helpers\SessionIdentifierHelper;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;

class LoggerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register(): void
    {
        $logger = $this->app->make(LoggerInterface::class);
        $identifierHelper = $this->app->make(SessionIdentifierHelper::class);
        $service = new LoggerService($logger, $identifierHelper);

        $this->app->instance('log', $service);
    }

    /**
     * Boot application.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/logger.php', 'logger');
    }
}
