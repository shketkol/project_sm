<?php

namespace App\Services\LoggerService\Traits;

/**
 * Copy paste \Monolog\Formatter\NormalizerFormatter
 * Rewrite jsonEncode private method so it would encode json with pretty print
 *
 * @see \Monolog\Formatter\NormalizerFormatter::jsonEncode
 * @mixin \Monolog\Formatter\NormalizerFormatter
 */
trait Stringify
{
    /**
     * Return the JSON representation of a value
     *
     * @param mixed $data
     * @param bool  $ignoreErrors
     *
     * @return string
     * @throws \RuntimeException if encoding fails and errors are not ignored
     */
    protected function toJson($data, bool $ignoreErrors = false): string
    {
        // suppress json_encode errors since it's twitchy with some inputs
        if ($ignoreErrors) {
            return @$this->jsonEncode($data);
        }

        $json = $this->jsonEncode($data);

        if ($json === false) {
            $json = $this->handleJsonError(json_last_error(), $data);
        }

        return $json;
    }

    /**
     * @param mixed $data
     *
     * @return string JSON encoded data or null on failure
     */
    protected function jsonEncode($data): string
    {
        return json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    /**
     * Handle a json_encode failure.
     *
     * If the failure is due to invalid string encoding, try to clean the
     * input and encode again. If the second encoding attempt fails, the
     * inital error is not encoding related or the input can't be cleaned then
     * raise a descriptive exception.
     *
     * @param int   $code return code of json_last_error function
     * @param mixed $data data that was meant to be encoded
     *
     * @return string            JSON encoded data after error correction
     * @throws \RuntimeException if failure can't be corrected
     */
    private function handleJsonError($code, $data)
    {
        if ($code !== JSON_ERROR_UTF8) {
            $this->throwEncodeError($code, $data);
        }

        if (is_string($data)) {
            $this->detectAndCleanUtf8($data);
        } elseif (is_array($data)) {
            array_walk_recursive($data, [$this, 'detectAndCleanUtf8']);
        } else {
            $this->throwEncodeError($code, $data);
        }

        $json = $this->jsonEncode($data);

        if ($json === false) {
            $this->throwEncodeError(json_last_error(), $data);
        }

        return $json;
    }

    /**
     * Throws an exception according to a given code with a customized message
     *
     * @param int   $code return code of json_last_error function
     * @param mixed $data data that was meant to be encoded
     *
     * @throws \RuntimeException
     */
    private function throwEncodeError($code, $data)
    {
        switch ($code) {
            case JSON_ERROR_DEPTH:
                $msg = 'Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $msg = 'Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $msg = 'Unexpected control character found';
                break;
            case JSON_ERROR_UTF8:
                $msg = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            default:
                $msg = 'Unknown error';
        }

        throw new \RuntimeException('JSON encoding failed: ' . $msg . '. Encoding: ' . var_export($data, true));
    }
}
