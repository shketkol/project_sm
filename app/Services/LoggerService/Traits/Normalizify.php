<?php

namespace App\Services\LoggerService\Traits;

use Monolog\Utils;

/**
 * Copy paste \Monolog\Formatter\NormalizerFormatter
 * Rewrite normalize method so it would allow bigger depth
 *
 * @see \Monolog\Formatter\NormalizerFormatter::normalize
 * @mixin \Monolog\Formatter\NormalizerFormatter
 */
trait Normalizify
{
    /**
     * Data normalization depth.
     *
     * @var int
     */
    private $normalizeDepth = 15;

    /**
     * @param mixed $data
     * @param int   $depth
     *
     * @return array|string
     * @SuppressWarnings(PHPMD)
     */
    protected function normalize($data, $depth = 0)
    {
        if ($depth > $this->normalizeDepth) {
            return sprintf('Over %d levels deep, aborting normalization', $this->normalizeDepth);
        }

        /** @see \Monolog\Formatter\NormalizerFormatter::normalize */
        if (null === $data || is_scalar($data)) {
            if (is_float($data)) {
                if (is_infinite($data)) {
                    return ($data > 0 ? '' : '-') . 'INF';
                }
                if (is_nan($data)) {
                    return 'NaN';
                }
            }

            return $data;
        }

        if (is_array($data)) {
            $normalized = [];

            $count = 1;
            foreach ($data as $key => $value) {
                if ($count++ > 1000) {
                    $normalized['...'] = 'Over 1000 items (' . count($data) . ' total), aborting normalization';
                    break;
                }

                $normalized[$key] = $this->normalize($value, $depth + 1);
            }

            return $normalized;
        }

        if ($data instanceof \DateTime) {
            return $data->format($this->dateFormat);
        }

        if (is_object($data)) {
            if ($data instanceof \Throwable) {
                return $this->normalizeException($data);
            }

            // non-serializable objects that implement __toString stringified
            if (method_exists($data, '__toString') && !$data instanceof \JsonSerializable) {
                $value = $data->__toString();
            } else {
                // the rest is json-serialized in some way
                $value = $this->toJson($data, true);
            }

            return sprintf("[object] (%s: %s)", Utils::getClass($data), $value);
        }

        if (is_resource($data)) {
            return sprintf('[resource] (%s)', get_resource_type($data));
        }

        return '[unknown(' . gettype($data) . ')]';
    }
}
