<?php

namespace App\Services\HealthCheckService\Listeners;

use App\Services\HealthCheckService\Events\UnhealthyTargets;
use App\Services\HealthCheckService\Metric\Contracts\CloudwatchMetricClient;

/**
 * Class HealthIssueListener
 * @package App\Services\HealthCheckService\Listeners
 */
class HealthIssueListener
{
    /**
     * Instance of AwsMetricClient
     * @object AwsMetricClient
     */
    protected $cloudwatch;

    public function __construct(CloudwatchMetricClient $cloudwatch)
    {
        $this->cloudwatch = $cloudwatch;
    }

    /**
     * @param UnhealthyTargets $event
     *
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function handle(UnhealthyTargets $event): void
    {
        $this->cloudwatch->putUnhealthy();
    }
}
