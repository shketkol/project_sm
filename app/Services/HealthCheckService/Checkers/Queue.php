<?php

namespace App\Services\HealthCheckService\Checkers;

use PragmaRX\Health\Checkers\Base;
use PragmaRX\Health\Support\Result;

class Queue extends Base
{
    /**
     * Check resource.
     *
     * @return Result
     */
    public function check()
    {
        \Queue::size();

        return $this->makeResult(true);
    }
}
