<?php

namespace App\Services\ScoutEngines\MySQL\Modes;

use Laravel\Scout\Builder;
use Yab\MySQLScout\Engines\Modes\Boolean;

class BooleanWildcard extends Boolean
{
    /**
     * Add to end of the query wildcard '*' to be able to use fulltext search for part of word
     * Also wrap each word with "+" so search would work like eager search
     *
     * For example:
     * - without "+" parameter query 'Los Angeles' would search all entries with 'Los' OR 'Angeles'
     * - with "+" parameter query 'Los Angeles' would search all entries with 'Los' AND 'Angeles'
     *
     * If it's more than or equal 4 symbols - would be used this mode (it's min limit for mysql fulltext search index)
     * If it's less than 4 symbols search will fallback to 'like' search
     *
     * @param Builder $builder
     *
     * @return array
     */
    public function buildParams(Builder $builder): array
    {
        $delimiter = ' ';
        $wildcard = '*';
        $andOperator = '+';
        $query = [];

        $parts = explode($delimiter, $builder->query);
        $parts = $this->removeEmptyStrings($parts);

        foreach ($parts as $word) {
            $query[] = $andOperator . $word;
        }

        $this->whereParams[] = implode($delimiter, $query) . $wildcard;

        return $this->whereParams;
    }

    /**
     * @param array $parts
     *
     * @return array
     */
    private function removeEmptyStrings(array $parts): array
    {
        while (($key = array_search('', $parts, true)) !== false) {
            unset($parts[$key]);
        }

        return $parts;
    }
}
