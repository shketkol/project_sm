<?php

namespace App\Services\ScoutEngines\MySQL;

use Illuminate\Support\Str;
use Yab\MySQLScout\Engines\Modes\ModeContainer;
use Yab\MySQLScout\Providers\MySQLScoutServiceProvider;
use Yab\MySQLScout\Services\IndexService;
use Yab\MySQLScout\Services\ModelService;

class MySQLProvider extends MySQLScoutServiceProvider
{
    /**
     * @var string[]
     */
    private $defaultModes = [
        'NATURAL_LANGUAGE',
        'BOOLEAN',
        'LIKE',
        'LIKE_EXPANDED',
    ];

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->singleton(ModelService::class, function ($app) {
            return new ModelService();
        });

        $this->app->singleton(IndexService::class, function ($app) {
            return new IndexService($app->make(ModelService::class));
        });

        $this->app->singleton(ModeContainer::class, function ($app) {
            $engineNamespace = 'Yab\\MySQLScout\\Engines\\Modes\\';
            $mode = $this->getModeClassName($engineNamespace);
            $fallbackMode = $engineNamespace .
                Str::studly(Str::lower(config('scout.mysql.min_fulltext_search_fallback')));

            return new ModeContainer(new $mode(), new $fallbackMode());
        });
    }

    /**
     * @param string $engineNamespace
     *
     * @return string
     */
    private function getModeClassName(string $engineNamespace): string
    {
        $mode = config('scout.mysql.mode');
        $className = Str::studly(Str::lower($mode));

        // default
        if (in_array($mode, $this->defaultModes)) {
            return $engineNamespace . $className;
        }

        // custom
        return 'App\\Services\\ScoutEngines\\MySQL\\Modes\\' . $className;
    }
}
