<?php

namespace App\Services\MediaService;

use App\Services\MediaService\Contracts\VideoDriver;
use Illuminate\Support\ServiceProvider;

/**
 * Class MediaServiceProvider
 * @package App\Services\MediaService
 */
class MediaServiceProvider extends ServiceProvider
{
    /**
     * Bind VideoDriver to class defined in config
     */
    public function register()
    {
        $className = 'App\Services\MediaService\\' . ucfirst(config('media.videos.driver')) . 'Driver';
        $this->app->bind(VideoDriver::class, $className);
    }
}
