<?php

namespace App\Services\ValidationRulesService\Contracts;

interface ValidationRules
{
    /**
     * Get all of the rules.
     *
     * @return array
     */
    public function all(): array;

    /**
     * Retrieve a rule for the field.
     *
     * @param  string  $key
     * @param  string|array|null  $default
     * @return mixed
     */
    public function get(string $key = null, $default = null);

    /**
     * Set a rule.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function set(string $key, $value);

    /**
     * Get a subset of the rules for the field.
     *
     * @param  string  $field
     * @param  array|mixed $keys
     * @param  array $extra
     * @return array
     */
    public function only(string $field, $keys, array $extra = []): array;

    /**
     * Format rules as a string.
     *
     * @param  mixed  $rules
     * @return array
     */
    public function format($rules): array;
}
