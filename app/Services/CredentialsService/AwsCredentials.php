<?php

namespace App\Services\CredentialsService;

use App\Helpers\EnvironmentHelper;
use Aws\Credentials\Credentials;
use Aws\Credentials\InstanceProfileProvider;
use Aws\Sts\StsClient;
use Illuminate\Support\Facades\Cache;
use App\Services\CredentialsService\Contracts\AwsCredentials as AwsCredentialsContract;

class AwsCredentials implements AwsCredentialsContract
{
    /**
     * Request AWS credentials.
     *
     * @return Credentials
     */
    private function requestCredentials(): Credentials
    {
        $credentials = EnvironmentHelper::isLocalEnv() ?
            $this->requestCredentialsForLocalEnv() : $this->requestCredentialsFromEc2();

        Cache::put('aws_credentials', $credentials, config('creative::aws.credentials.cache_time'));

        return $credentials;
    }

    /**
     * Request credentials from the EC2 metadata service.
     *
     * @return Credentials
     */
    private function requestCredentialsFromEc2(): Credentials
    {
        return (new InstanceProfileProvider())()->wait();
    }

    /**
     * Get credentials from .env file.
     *
     * @return Credentials
     */
    private function requestCredentialsForLocalEnv(): Credentials
    {
        return new Credentials(
            config('filesystems.disks.s3.key'),
            config('filesystems.disks.s3.secret')
        );
    }

    /**
     * Returns a set of security credentials that you can use to access AWS resources.
     *
     * @return Credentials
     */
    public function get(): Credentials
    {
        /** @var Credentials|null $cachedCredentials */
        $cachedCredentials = Cache::get('aws_credentials');

        if ($cachedCredentials && !$cachedCredentials->isExpired()) {
            return $cachedCredentials;
        }

        return $this->requestCredentials();
    }
}
