<?php

namespace App\Services\CredentialsService;

use Illuminate\Support\ServiceProvider;
use App\Services\CredentialsService\Contracts\AwsCredentials as AwsCredentialsContract;

class CredentialsServiceProvider extends ServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [
        AwsCredentialsContract::class => AwsCredentials::class,
    ];
}
