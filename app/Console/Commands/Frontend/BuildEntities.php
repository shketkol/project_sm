<?php

namespace App\Console\Commands\Frontend;

use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

/**
 * Class BuildEntities.
 *
 * Create static json files for non-dynamic models or any entity.
 * It also goes through all project modules which are presented in config app providers.
 * Just put needed entity to entity folder in a module and it will generate json file with a file name
 * and module name as a prefix.
 * For example `campaign-targeting.json`.
 *
 * @package App\Console\Commands\Frontend
 */
class BuildEntities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'platform:frontend:entities';

    /**
     * Name of the created file.
     *
     * @var string
     */
    protected $targetFilename = '';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build static entities for frontend (creates json files from all entities folders).';

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $this->buildEntities();
            $this->buildModuleEntities();
        } catch (Exception $e) {
            $this->error('An error occurred while frontend files creation. ' . $e->getMessage());
        }
    }

    /**
     * Build entities files.
     *
     * @throws \Exception
     */
    protected function buildEntities()
    {
        $this->createFiles($this->getEntities());
    }

    /**
     * Build module entities.
     *
     * @throws \Exception
     */
    protected function buildModuleEntities()
    {
        $moduleProviders = $this->getModuleProviders();

        foreach ($moduleProviders as $moduleProvider) {
            $moduleEntities = (new $moduleProvider(app()))->getEntities();
            ['files' => $files, 'name' => $moduleName] = $moduleEntities;
            if (count($files)) {
                $this->createFiles($files, $moduleName);
            }
        }
    }

    /**
     * Create files.
     *
     * @param array $files
     * @param string $moduleName
     * @throws \Exception
     */
    protected function createFiles(array $files = [], $moduleName = '')
    {
        foreach ($files as $file) {
            $entityData       = require $file->getRealPath();
            $moduleNamePrefix = (strlen($moduleName) ? $moduleName . '-' : '');

            try {
                $this
                    ->setTargetFileName("{$moduleNamePrefix}{$file->getBasename('.php')}.json")
                    ->createFile($entityData);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }
        }
    }

    /**
     * Get all of the configuration files for the application.
     *
     * @return array
     */
    protected function getModuleProviders(): array
    {
        $providers = config('app.providers');

        return array_filter($providers, function ($provider) {
            return strpos($provider, 'Modules') !== false;
        });
    }

    /**
     * Get app entities.
     *
     * @return array
     * @throws \Exception
     */
    protected function getEntities(): array
    {
        $entityFiles = [];

        $entityDirectory = base_path('entities');
        if (File::exists($entityDirectory)) {
            $entityFiles = File::files($entityDirectory);
        }

        return $entityFiles;
    }
}
