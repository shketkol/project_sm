<?php

namespace App\Console\Commands\Frontend;

use App\Console\Commands\Frontend\Traits\Config;

class BuildConfig extends Command
{
    use Config;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'platform:frontend:config';

    /**
     * Name of the created file.
     *
     * @var string
     */
    protected $targetFilename = 'config.json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build configs for frontend (put selected configs into a .json file).';

    /**
     * Export data into json file.
     *
     * @return array
     */
    public function export(): array
    {
        return $this->getConfigs();
    }
}
