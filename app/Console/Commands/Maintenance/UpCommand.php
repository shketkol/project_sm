<?php

namespace App\Console\Commands\Maintenance;

use App\Traits\Maintenance;
use Illuminate\Foundation\Console\UpCommand as BaseUpCommand;

class UpCommand extends BaseUpCommand
{
    use Maintenance;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        try {
            if ($this->isUp()) {
                $this->comment('Application is already up.');

                return 1;
            }

            $this->up();
        } catch (\Throwable $e) {
            $this->error('Failed to disable maintenance mode.');
            $this->error($e->getMessage());

            return 1;
        }

        return 0;
    }

    /**
     * Disable maintenance mode.
     */
    private function up(): void
    {
        $this->deleteMaintenanceData();
        $this->info('Application is now live.');
    }
}
