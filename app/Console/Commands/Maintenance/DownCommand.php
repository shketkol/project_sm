<?php

namespace App\Console\Commands\Maintenance;

use App\Traits\Maintenance;
use Illuminate\Foundation\Console\DownCommand as BaseDownCommand;
use Illuminate\Support\Arr;

class DownCommand extends BaseDownCommand
{
    use Maintenance;

    /**
     * Returned command options from before Laravel 8.x version.
     */
    public function __construct()
    {
        $this->signature = $this->signature
            . "\n" . '{--message= : The message for the maintenance mode}'
            . "\n" . '{--allow= : IP or networks allowed to access the application while in maintenance mode}';

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        try {
            if ($this->isDown()) {
                $this->comment('Application is already down.');

                return 1;
            }

            $this->down();
        } catch (\Throwable $e) {
            $this->error('Failed to enter maintenance mode.');
            $this->error($e->getMessage());

            return 1;
        }

        return 0;
    }

    /**
     * Enable maintenance mode.
     */
    private function down(): void
    {
        $this->setMaintenanceData($this->getDownFilePayload());
        $this->comment('Application is now in maintenance mode.');
    }

    /**
     * @return array
     */
    protected function getDownFilePayload(): array
    {
        $payload = parent::getDownFilePayload();

        Arr::set($payload, 'allowed', $this->option('allow'));
        Arr::set($payload, 'message', $this->option('message'));

        return $payload;
    }
}
