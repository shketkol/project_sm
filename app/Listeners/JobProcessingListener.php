<?php

namespace App\Listeners;

use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Arr;
use Psr\Log\LoggerInterface;

class JobProcessingListener
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * @param JobProcessing $event
     */
    public function handle(JobProcessing $event): void
    {
        $this->log->info('Job started processing.', [
            'job_class' => Arr::get($event->job->payload(), 'displayName'),
            'job_id'    => $event->job->getJobId(),
            'queue'     => $event->job->getQueue(),
        ]);
    }
}
