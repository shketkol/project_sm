<?php

namespace App\Providers;

use App\DataTable\Requests\BaseRequest;
use Illuminate\Support\ServiceProvider;

class DatatableServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('datatables.request', function () {
            return new BaseRequest;
        });
    }
}
