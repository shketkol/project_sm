<?php

namespace App\Providers;

use App\Repositories\Contracts\TimezoneRepository as TimezoneRepositoryInterface;
use App\Repositories\TimezoneRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [
        TimezoneRepositoryInterface::class => TimezoneRepository::class,
    ];
}
