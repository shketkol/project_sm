<?php

namespace App\Notifications\Traits;

use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Validator;

trait MailCarbonCopies
{
    /**
     * @var string[]
     */
    protected $cc = [];

    /**
     * @var string[]
     */
    protected $bcc = [];

    /**
     * @param string|array $rawEmails
     *
     * @return void
     */
    public function setCc($rawEmails): void
    {
        $rawEmails = $this->parseEmails($rawEmails);
        $this->cc = [];

        foreach ($rawEmails as $email) {
            if (!$this->isValidEmail($email)) {
                $this->log()->error('Invalid CC email', [
                    'notification_class' => get_class($this),
                    'notification_id'    => $this->id,
                    'bad_value'          => $email,
                ]);
                continue;
            }

            $this->cc[] = $email;
        }

        $this->log()->info('Count of CC emails that were set', [
            'notification_class' => get_class($this),
            'notification_id'    => $this->id,
            'count'              => count($this->cc),
        ]);
    }

    /**
     * @return array
     */
    public function getCc(): array
    {
        return $this->cc;
    }

    /**
     * @return bool
     */
    public function hasCc(): bool
    {
        return !empty($this->cc);
    }

    /**
     * @param string|array $rawEmails
     *
     * @return void
     */
    public function setBcc($rawEmails): void
    {
        $rawEmails = $this->parseEmails($rawEmails);
        $this->bcc = [];

        foreach ($rawEmails as $email) {
            if (!$this->isValidEmail($email)) {
                $this->log()->error('Invalid BCC email', [
                    'notification_class' => get_class($this),
                    'notification_id'    => $this->id,
                    'bad_value'          => $email,
                ]);
                continue;
            }

            $this->bcc[] = $email;
        }

        $this->log()->info('Count of BCC emails that were set', [
            'notification_class' => get_class($this),
            'notification_id'    => $this->id,
            'count'              => count($this->bcc),
        ]);
    }

    /**
     * @return array
     */
    public function getBcc(): array
    {
        return $this->bcc;
    }

    /**
     * @return bool
     */
    public function hasBcc(): bool
    {
        return !empty($this->bcc);
    }

    /**
     * @param string|array $emails
     *
     * @return array
     */
    protected function parseEmails($emails): array
    {
        if (is_string($emails)) {
            $emails = explode(',', $emails);
        }

        if (!is_array($emails)) {
            return [];
        }

        return array_filter(array_map('trim', $emails));
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    protected function isValidEmail(string $email): bool
    {
        $validator = Validator::make(['email' => $email], ['email' => 'email']);

        return $validator->passes();
    }

    /**
     * @return Logger
     */
    private function log(): Logger
    {
        return app(Logger::class);
    }
}
