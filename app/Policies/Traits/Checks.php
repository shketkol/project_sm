<?php

namespace App\Policies\Traits;

use Modules\Campaign\Models\Campaign;
use Modules\User\Models\User;

trait Checks
{
    /**
     * @param User $user
     *
     * @return bool
     */
    protected function isAdmin(User $user): bool
    {
        return $user->isAdmin() || $user->isSuperAdmin();
    }

    /**
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function isOwner(User $user, Campaign $campaign): bool
    {
        return $campaign->user_id === $user->id;
    }
}
