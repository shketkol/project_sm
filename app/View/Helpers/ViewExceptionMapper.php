<?php

namespace App\View\Helpers;

use App\View\Exceptions\HaapiGenericException;
use App\View\Exceptions\HaapiUnauthorizedException;
use App\View\Exceptions\HaapiViewException;
use Illuminate\Support\Arr;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\UnauthorizedException;

trait ViewExceptionMapper
{
    /**
     * Map HaapiException to it's view representation.
     * Needed for case when there is no frontend to process generic HAAPI exception
     *
     * @param HaapiException $exception
     *
     * @return HaapiViewException
     */
    protected function mapToViewException(HaapiException $exception): HaapiViewException
    {
        $map = [
            UnauthorizedException::class => HaapiUnauthorizedException::class,
        ];

        $viewException = Arr::get($map, get_class($exception), HaapiGenericException::class);

        return new $viewException(
            $this->getMessage($exception),
            $exception->getCode(),
            1,
            $exception->getFile(),
            $exception->getLine(),
            $exception
        );
    }
}
