<?php

namespace App\View\Exceptions;

use Illuminate\Http\Response;

class HaapiGenericException extends HaapiViewException
{
    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return abort($this->getCode());
    }
}
