<?php

namespace App\Helpers;

use Carbon\Carbon;
use Carbon\CarbonInterface;

class DateFormatHelper
{
    /**
     * Format string in format mm/dd/yyyy
     *
     * @param string $date
     * @param bool   $useDefaultTimeZone
     *
     * @return string
     */
    public static function toMonthDayYear(string $date, $useDefaultTimeZone = false): string
    {
        $date = Carbon::parse($date);
        if ($useDefaultTimeZone) {
            $date->setTimezone(config('date.default_timezone_full_code'));
        }

        return $date->format(config('date.format.php'));
    }

    /**
     * Format date in format Month dd, yyyy
     *
     * @param string $date
     *
     * @return string
     */
    public static function formatted(string $date): string
    {
        return Carbon::parse($date)->toFormattedDateString();
    }

    /**
     * Format date in format Month dd, yyyy, hh:mm PM/AM
     *
     * @param string $date
     * @param bool   $useDefaultTimeZone
     *
     * @return string
     */
    public static function formattedDateTime(string $date, $useDefaultTimeZone = false): string
    {
        $date = Carbon::parse($date);
        if ($useDefaultTimeZone) {
            $date->setTimezone(config('date.default_timezone_full_code'));
        }

        return $date->rawFormat(config('date.format.date_time'));
    }

    /**
     * Format date in format mm/dd/yyyy hh:mm:ss
     *
     * @param string $date
     * @param bool   $useDefaultTimeZone
     *
     * @return string
     */
    public static function formattedDateTime24h(string $date, $useDefaultTimeZone = false): string
    {
        $date = Carbon::parse($date);
        if ($useDefaultTimeZone) {
            $date->setTimezone(config('date.default_timezone_full_code'));
        }

        return $date->rawFormat(config('date.format.date_time_24h'));
    }

    /**
     * @param string $date
     *
     * @return string
     */
    public static function convertHaapiDate(string $date): string
    {
        return Carbon::parse($date)->format(config('date.format.daapi'));
    }

    /**
     * @param CarbonInterface $date
     *
     * @return string|null
     */
    public static function haapiWithMilliseconds(CarbonInterface $date): ?string
    {
        return $date->format('Y-m-d\TH:i:s.') . '000' . $date->format('O');
    }

    /**
     * @param CarbonInterface $date
     *
     * @return Carbon
     */
    public static function convertToTimezone(CarbonInterface $date): Carbon
    {
        return Carbon::createFromFormat(
            config('date.format.db_date_time'),
            $date,
            config('date.default_timezone_full_code'),
        );
    }

    /**
     * Get now datetime in ET timezone.
     *
     * @return Carbon
     */
    public static function now(): Carbon
    {
        return Carbon::now()->setTimezone(config('date.default_timezone_full_code'));
    }

    /**
     * Get date to utc timezone.
     *
     * @param string $date
     * @param string $timezone
     * @param string $format
     *
     * @return string
     */
    public static function toTimezone(string $date, string $timezone, string $format = 'c'): string
    {
        return Carbon::parse($date)->setTimezone($timezone)->format($format);
    }
}
