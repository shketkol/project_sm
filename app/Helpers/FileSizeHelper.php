<?php

namespace App\Helpers;

class FileSizeHelper
{
    /**
     * @const integer
     */
    public const GIGABYTE = 1073741824;

    /**
     * @const integer
     */
    public const MEGABYTE = 1048576;

    /**
     * @const integer
     */
    public const KILOBYTE = 1024;

    /**
     * @param int $bytes
     * @return float
     */
    public static function toKilobytes(int $bytes): float
    {
        return round($bytes / self::KILOBYTE, 1);
    }

    /**
     * @param int $bytes
     * @return float
     */
    public static function toMegabytes(int $bytes): float
    {
        return round($bytes / self::MEGABYTE, 1);
    }

    /**
     * @param int $bytes
     * @return float
     */
    public static function toGigabytes(int $bytes): float
    {
        return round($bytes / self::GIGABYTE, 1);
    }
}
