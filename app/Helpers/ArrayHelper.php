<?php

namespace App\Helpers;

use Illuminate\Support\Arr;

trait ArrayHelper
{
    /**
     * Sorting array by field
     *
     * @param array $array
     * @param string $field
     *
     * @return array
     */
    public function sortByField(array $array, string $field): array
    {
        return array_values(Arr::sort($array, function ($value) use ($field) {
            return $value[$field];
        }));
    }
}
