<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Modules\User\Models\User;

trait CurrentUser
{
    /**
     * @return User|null
     */
    public function getCurrentUser(): ?User
    {
        return Auth::guard('admin')->user() ?: Auth::guard()->user();
    }

    /**
     * If there is a risk to fall into infinite loop during obtaining user via Guard::user()
     * (for example if we need to log something during fetching current user operation)
     *
     * @return User|null
     */
    public function getCurrentUserCached(): ?User
    {
        return Auth::guard('admin')->getUser() ?: Auth::guard()->getUser();
    }
}
