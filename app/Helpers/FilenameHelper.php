<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class FilenameHelper
{
    /**
     * Max filename length for Windows, Linux, Mac is 255.
     *
     * Even max length is enough to save file some old programs could not read file that long on Windows.
     * And could not move to Trash in Ubuntu filename that long.
     * Not tested on Mac.
     *
     * We took half of this length to fit it to full path and file name.
     *
     * @var integer
     */
    private const OPTIMAL_FILENAME_LENGTH = 120;

    /**
     * @param string $name
     *
     * @return bool
     */
    public static function isNameTooLong(string $name): bool
    {
        return Str::length($name) > self::OPTIMAL_FILENAME_LENGTH;
    }

    /**
     * @param string $name
     *
     * @return int
     */
    public static function getNameAbsoluteExcess(string $name): int
    {
        return abs(self::OPTIMAL_FILENAME_LENGTH - Str::length($name));
    }

    /**
     * @param string $name
     * @param int    $excess
     *
     * @return string
     */
    public static function limit(string $name, int $excess): string
    {
        $end = '...';
        $limit = Str::length($name) - Str::length($end) - $excess;

        return Str::limit($name, $limit, $end);
    }
}
