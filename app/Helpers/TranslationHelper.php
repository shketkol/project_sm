<?php

namespace App\Helpers;

/**
 * Class TranslationHelper
 * @package Helpers
 */
class TranslationHelper
{
    /**
     * @param string $field
     * @return string
     */
    public static function replaceNumbers(string $field): string
    {
        return preg_replace("/\d/", "*", $field);
    }

    /**
     * @param string $field
     * @return string
     */
    public static function multiDimensionalTranslation(string $field) : string
    {
        if (preg_match("/\d|\*/", $field)) {
            $fieldArray = explode('.', $field);
            foreach ($fieldArray as $key => $part) {
                if (preg_match("/\d|\*/", $part)) {
                    array_splice($fieldArray, $key, 1);
                }
            }
            $field = implode('.', $fieldArray);
        }

        return $field;
    }
}
