<?php

namespace App\Helpers\Math;

use App\Helpers\Math\LargestRemainderMethod\LargestRemainder;
use Illuminate\Support\Arr;

class CalculationHelper
{
    /**
     * Calculate budget (cost) of campaign.
     * Rounding is default (up and down).
     *
     * COST = CPM * IMPRESSIONS / 1000
     *
     * @param float $cpm
     * @param int $impressionsQuantity
     * @return float
     */
    public static function calculateBudget(float $cpm, int $impressionsQuantity): float
    {
        return round(($cpm / 1000) * $impressionsQuantity, 2);
    }

    /**
     * Calculate impressions for campaign.
     * Round down, because impressions should be integer.
     *
     * IMPRESSIONS = COST * 1000 / CPM
     *
     * @param float $cpm
     * @param float $cost
     * @return int
     */
    public static function calculateImpressions(float $cpm, float $cost): int
    {
        return floor(round((($cost * 1000) / $cpm), 2));
    }

    /**
     * @param array $percentageArray
     * @param array $options
     * @return array
     */
    public static function largestReminderPercentage(array $percentageArray, array $options = []): array
    {
        $key = Arr::has($options, 'key') ? Arr::get($options, 'key') : 'percentage';
        $multiplier = Arr::has($options, 'multiplier') ? Arr::get($options, 'multiplier') : 1;
        $precision = Arr::has($options, 'precision') ? Arr::get($options, 'precision') : 0;

        $largestReminder = app()->makeWith(LargestRemainder::class, ['numbers' => $percentageArray]);

        if ($precision) {
            $largestReminder->setPrecision($precision);
        }

        return $largestReminder->uround(
            function ($item) use ($multiplier, $key) {
                return $item[$key] * $multiplier;
            },
            function (&$item, $value) use ($multiplier, $key) {
                $item[$key] = $value;
            }
        );
    }
}
