<?php

namespace App\Helpers\Math\LargestRemainderMethod;

use App\Helpers\Math\LargestRemainderMethod\Number as LargestRemainderNumber;

class LargestRemainder
{
    /**
     * @var array
     */
    private $numbers = [];

    /**
     * @var int
     */
    private $precision = 0;

    /**
     * @param array $numbers
     */
    public function __construct(array $numbers)
    {
        $this->numbers = $numbers;
    }

    /**
     * @param int $precision
     */
    public function setPrecision(int $precision): void
    {
        $this->precision = $precision;
    }

    /**
     * @return array
     */
    public function round(): array
    {
        return $this->uround(
            function ($item) {
                return $item;
            },
            function (&$item, $value) {
                $item = $value;
            }
        );
    }

    /**
     * @param callable $get
     * @param callable $set
     * @return array
     */
    public function uround(callable $get, callable $set): array
    {
        if (!count($this->numbers)) {
            return [];
        }

        $originalOrder = array_keys($this->numbers);

        $sum = array_sum(array_map(function ($item) use ($get) {
            return $this->getNumber($get, $item)->floor()->value();
        }, $this->numbers));

        $multiplier = $this->precision ? pow(10, $this->precision) : 1;
        $diff = (int)(100 * $multiplier) - $sum;

        uasort($this->numbers, function ($a, $b) use ($get) {
            $aNumber = $this->getNumber($get, $a);
            $bNumber = $this->getNumber($get, $b);
            return $aNumber->value() - $aNumber->floor()->value() < $bNumber->value() - $bNumber->floor()->value();
        });

        if ($diff) {
            $this->setAdjusted($get, $set, $diff);
        } else {
            $this->setOriginal($get, $set);
        }

        return array_replace(array_flip($originalOrder), $this->numbers);
    }

    /**
     * @param callable $get
     * @param callable $set
     */
    protected function setOriginal(callable $get, callable $set): void
    {
        foreach ($this->numbers as &$item) {
            $number = $this->getNumber($get, $item);
            $this->setNumber($set, $item, $number->floor());
        }
    }

    /**
     * @param callable $get
     * @param callable $set
     * @param int $diff
     */
    protected function setAdjusted(callable $get, callable $set, int $diff): void
    {
        while ($diff != 0) {
            foreach ($this->numbers as &$item) {
                $number = $this->getNumber($get, $item);

                if ($diff > 0) {
                    $this->setNumber($set, $item, $number->add(1)->floor());
                    $diff--;
                    continue;
                }

                if ($diff < 0) {
                    $this->setNumber($set, $item, $number->add(1)->floor());
                    $diff++;
                    continue;
                }

                $this->setNumber($set, $item, $number->floor());
                continue;
            }
        }
    }

    /**
     * @param callable $get
     * @param $val
     * @return LargestRemainderNumber
     */
    private function getNumber(callable $get, $val): LargestRemainderNumber
    {
        $resolved = call_user_func_array($get, [$val]);
        if (false === is_numeric($resolved)) {
            $resolved = 0;
        }
        return (new Number($resolved, $this->precision))->normalize();
    }

    /**
     * @param callable $set
     * @param $item
     * @param LargestRemainderNumber $number
     */
    private function setNumber(callable $set, &$item, LargestRemainderNumber $number): void
    {
        call_user_func_array($set, [&$item, $number->denormalize()->value()]);
    }
}
