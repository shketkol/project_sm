<?php

namespace App\DataTable\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Arr;
use Modules\Broadcast\DataTable\BroadcastDataTable;
use Modules\Broadcast\Http\Resources\BroadcastResource;
use Modules\Broadcast\Repositories\BroadcastRepository;
use Modules\Report\DataTable\AdminReportDataTable;
use Modules\Report\DataTable\ReportDataTable;
use Modules\Report\Http\Resources\ReportResource;
use Modules\Report\Repositories\Contracts\ReportRepository;

class CheckStatusAction
{
    /**
     * @var Authenticatable
     */
    protected $user;

    /**
     * @param Authenticatable  $user
     */
    public function __construct(Authenticatable $user)
    {
        $this->user = $user;
    }

    /**
     * @param string $tableName
     * @param array  $rowsIds
     *
     * @return mixed
     */
    public function handle(string $tableName, array $rowsIds)
    {
        $type = $this->getEntity($tableName);
        $statuses =  $type['repository']
            ->with('status');

        if (Arr::get($type, 'byUser', false)) {
            $statuses = $statuses->byUser([$this->user->id]);
        }

        $statuses = $statuses->findWhereIn('id', $rowsIds);

        return $type['resource']::collection($statuses);
    }

    /**
     * @param string $tableName
     *
     * @return array
     */
    private function getEntity(string $tableName): array
    {
        $types = [
            [
                'tableName'   => ReportDataTable::$name,
                'repository' => app(ReportRepository::class),
                'resource'   => ReportResource::class,
                'byUser'     => true
            ],
            [
                'tableName'  => AdminReportDataTable::$name,
                'repository' => app(ReportRepository::class),
                'resource'   => ReportResource::class,
                'byUser'     => true
            ],
            [
                'tableName'  => BroadcastDataTable::$name,
                'repository' => app(BroadcastRepository::class),
                'resource'   => BroadcastResource::class,
                'byUser'     => false
            ]
        ];

        return Arr::first(Arr::where($types, function ($item) use ($tableName) {
            return $tableName === Arr::get($item, 'tableName');
        }));
    }
}
