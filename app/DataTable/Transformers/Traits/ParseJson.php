<?php

namespace App\DataTable\Transformers\Traits;

use Illuminate\Support\Arr;

trait ParseJson
{
    /**
     * @var array
     */
    protected $textsArray = [];

    /**
     * Strip tags
     * @param $mapped
     * @return string
     */
    protected function addParseJson(string $mapped): string
    {
        $nodes = Arr::get(json_decode($mapped, true), 'content');

        if (!is_array($nodes)) {
            return '';
        }

        foreach ($nodes as $node) {
            $this->collectNodes($node);
        }

        $result = implode(' ', $this->textsArray);
        $this->textsArray = [];
        return $result;
    }

    /**
     * @param $node
     * @return void
     */
    protected function collectNodes($node): void
    {
        if (Arr::has($node, 'text')) {
            array_push($this->textsArray, $node['text']);
            return;
        }

        foreach ($node as $content) {
            if (!is_array($content)) {
                continue;
            }
            $this->collectNodes($content);
        }
    }
}
