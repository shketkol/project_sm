<?php

namespace App\DataTable\Filters;

use App\DataTable\Exceptions\FilterNotFoundException;
use Illuminate\Http\Request;
use Modules\Broadcast\DataTable\Repositories\Filters\BroadcastStatusFilter;
use Modules\Campaign\DataTable\Filters\CampaignStatusFilter;
use Modules\Creative\DataTable\Filters\CreativeStatusFilter;
use Modules\Invitation\DataTable\Repositories\Filters\InvitationStatusFilter;
use Modules\Notification\DataTable\Filters\NotificationCategoryFilter;
use Modules\Payment\DataTable\Filter\OrderStatusFilter;
use Modules\Report\DataTable\Repositories\Filters\ReportRunDateFilter;
use Modules\Report\DataTable\Repositories\Filters\ReportStatusFilter;
use Modules\User\DataTable\Filters\UserStatusFilter;

class DataTableFilterFactory
{
    /**
     * Available filters
     *
     * @var array
     */
    protected static $filters = [
        OrderStatusFilter::class,
        CampaignStatusFilter::class,
        CreativeStatusFilter::class,
        NotificationCategoryFilter::class,
        ReportRunDateFilter::class,
        ReportStatusFilter::class,
        UserStatusFilter::class,
        InvitationStatusFilter::class,
        BroadcastStatusFilter::class
    ];

    /**
     * Create filter from request
     *
     * @param Request $request
     *
     * @return DataTableFilter
     * @throws FilterNotFoundException
     */
    public function createFromRequest(Request $request): DataTableFilter
    {
        if (!$request->filter) {
            throw new FilterNotFoundException('Filter not set');
        }

        $filter = array_filter(static::$filters, function ($filterClass) use ($request) {
            return $request->filter === $filterClass::$name;
        });

        if (empty($filter)) {
            throw new FilterNotFoundException("Filter {$request->filter} not found");
        }

        $filter = array_shift($filter);

        return app($filter);
    }
}
