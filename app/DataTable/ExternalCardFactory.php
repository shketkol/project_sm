<?php

namespace App\DataTable;

use App\DataTable\Cards\Card;
use App\DataTable\Exceptions\ExternalCardNotFoundException;
use Illuminate\Http\Request;
use Modules\Campaign\DataTable\Cards\External\AdminTotalDeliveredImpressionsCard;
use Modules\Campaign\DataTable\Cards\SpendToDateCard;
use Modules\Campaign\DataTable\Cards\External\TotalDeliveredImpressionsCard;

class ExternalCardFactory
{
    /**
     * @var array
     */
    protected static $cards = [
        TotalDeliveredImpressionsCard::class,
        AdminTotalDeliveredImpressionsCard::class,
        SpendToDateCard::class
    ];

    /**
     * @param Request $request
     * @return Card
     * @throws ExternalCardNotFoundException
     */
    public function createFromRequest(Request $request): Card
    {
        $cardName = $request->card;

        if (!$cardName) {
            throw new ExternalCardNotFoundException('Request has no external card name');
        }

        $externalCardClass = array_filter(static::$cards, function ($card) use ($cardName) {
            return $cardName === $card::$name;
        });

        if (empty($externalCardClass)) {
            throw new ExternalCardNotFoundException('Can not find registered card with name:' . $cardName->card);
        }

        $externalCardClass = array_shift($externalCardClass);

        return app($externalCardClass);
    }
}
