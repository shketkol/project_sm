<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Campaign\Repositories\CampaignRepository;
use Prettus\Repository\Exceptions\RepositoryException;

class ValidateCampaignStatus implements Rule
{
    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @var array
     */
    private $statusIds;

    public function __construct(array $statusIds)
    {
        $this->repository = app(CampaignRepository::class);
        $this->statusIds = $statusIds;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param array  $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws RepositoryException
     */
    public function passes($attribute, $value): bool
    {
        return !empty($this->repository->byStatus($this->statusIds)->findWhere(['id' => $value])->first());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.custom.campaign_not_found');
    }
}
