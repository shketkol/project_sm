<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Broadcast\Actions\Traits\ValidateScheduleDate;

class ValidBroadcastDate implements Rule
{
    use ValidateScheduleDate;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        return $this->validDate($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.custom.valid_broadcast_date');
    }
}
