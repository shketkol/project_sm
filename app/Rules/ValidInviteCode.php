<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;
use Modules\Invitation\Models\Invitation;
use Modules\Invitation\Repositories\InvitationRepository;

class ValidInviteCode implements Rule
{
    /**
     * @var InvitationRepository
     */
    private $repository;

    public function __construct()
    {
        $this->repository = app(InvitationRepository::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function passes($attribute, $value): bool
    {
        /** @var Invitation $invitation */
        $invitation = $this->repository->byCode($value)->byAllowedStatusesIds()->first();

        if (is_null($invitation)) {
            return false;
        }

        if (!is_null($invitation->expired_at) && $invitation->expired_at->lt(Carbon::now())) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.valid_invite_code');
    }
}
