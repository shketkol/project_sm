<?php

namespace App\Rules\Traits;

use Modules\Targeting\Repositories\AgeGroupRepository;

trait TargetingAgeGroupAccess
{
    /**
     * @param array $values
     *
     * @return bool
     */
    protected function validateAgeGroupTargetingValues(array $values): bool
    {
        /** @var AgeGroupRepository $repository */
        $repository = app(AgeGroupRepository::class);
        $ageGroups = $repository
            ->onlyAllGenderCriteria()
            ->all()
            ->pluck('id')
            ->toArray();

        return count($ageGroups) === count($values) && !array_diff($values, $ageGroups);
    }
}
