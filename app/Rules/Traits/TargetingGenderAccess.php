<?php

namespace App\Rules\Traits;

use Modules\Targeting\Repositories\GenderRepository;

trait TargetingGenderAccess
{
    /**
     * @param int $value
     *
     * @return bool
     */
    protected function validateGenderTargetingValue(int $value): bool
    {
        $gender = app(GenderRepository::class)->getAllGenderModel();
        return $gender && $gender->id === $value;
    }
}
