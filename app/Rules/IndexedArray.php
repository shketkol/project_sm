<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IndexedArray implements Rule
{
    /**
     * @param string $attribute
     * @param array  $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        return is_array($value) && count(array_filter(array_keys($value), 'is_string')) === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.indexed_array');
    }
}
