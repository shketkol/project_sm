<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class Payment
 * @package App\Rules
 */
class Payment implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        return $value && is_integer($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.payment_method');
    }
}
