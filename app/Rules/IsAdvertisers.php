<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Advertiser\DataTable\Repositories\Criteria\AdvertiserRoleCriteria;
use Modules\User\DataTable\Repositories\Criteria\AddRolesCriteria;
use Modules\User\Models\User;
use Modules\User\Repositories\UserRepository;

class IsAdvertisers implements Rule
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var array
     */
    private $diff;

    public function __construct()
    {
        $this->repository = app(UserRepository::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param array  $ids
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $ids): bool
    {
        return $this->isAdvertisers($ids);
    }

    /**
     * @param array $ids
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function isAdvertisers(array $ids): bool
    {
        $advertisers = $this->repository->pushCriteria(new AddRolesCriteria(new User()))
            ->pushCriteria(new AdvertiserRoleCriteria())
            ->findWhereIn('id', $ids, 'id')
            ->pluck('id')
            ->toArray();

        $this->diff = array_diff($ids, $advertisers);

        return empty($this->diff);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.custom.is_advertisers', [
            'advertisers' => implode(', ', $this->diff),
        ]);
    }
}
