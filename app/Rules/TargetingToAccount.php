<?php

namespace App\Rules;

use App\Rules\Traits\TargetingAgeGroupAccess;
use App\Rules\Traits\TargetingGenderAccess;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\Gender;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Zipcode;

class TargetingToAccount implements Rule
{
    use TargetingAgeGroupAccess,
        TargetingGenderAccess;

    /**
     * @var string
     */
    protected $targetingType;

    /**
     * @param string $targetingType
     */
    public function __construct(string $targetingType)
    {
        $this->targetingType = $targetingType;
    }

    /**
     * Determine if the account of logged user has an access to items of the specified targeting.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        if (Auth::user()->canAccessTargeting($this->targetingType)) {
            return true;
        }

        switch ($this->targetingType) {
            case AgeGroup::TYPE_NAME:
                return $this->validateAgeGroupTargetingValues($value);
            case Gender::TYPE_NAME:
                return $this->validateGenderTargetingValue($value);
            case Audience::TYPE_NAME:
            case Location::TYPE_NAME:
            case Zipcode::TYPE_NAME:
            case Genre::TYPE_NAME:
                return $this->validateTargetingValues($value);
            default:
                return false;
        }
    }

    /**
     * @param array $values
     *
     * @return bool
     */
    protected function validateTargetingValues(array $values): bool
    {
        return !$values;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return trans('validation.targeting_to_account');
    }
}
