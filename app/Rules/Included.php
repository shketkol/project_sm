<?php

namespace App\Rules;

use App\Rules\Traits\CustomValidationMessages;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Lang;

/**
 * Class Included
 *
 * @package App\Rules
 */
class Included implements Rule
{

    use CustomValidationMessages;

    private const PARAMS_INDEX_ALLOWED = 0;
    private const PARAMS_INDEX_DIMENSION = 1;

    /**
     * Allowed values
     * @var array
     */
    private $allowed = [];

    /**
     * @var string
     */
    private $dimension = '';

    /**
     * @var string
     */
    private $attribute;

    /**
     * Included constructor.
     * NOTE: In case of updating logic don't forget update frontend implementation as well.
     *
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->allowed = $this->getAllowed($params);
        $this->dimension = $this->getDimension($params);
    }

    /**
     * Get allowed values.
     *
     * @param array $params
     *
     * @return array|mixed
     */
    private function getAllowed(array $params)
    {
        $allowed = Arr::get($params, self::PARAMS_INDEX_ALLOWED);
        return is_array($allowed) ? $allowed : $params;
    }

    /**
     * Get dimension attribute.
     *
     * @param array $params
     *
     * @return mixed|string
     */
    private function getDimension(array $params)
    {
        $allowed = Arr::get($params, self::PARAMS_INDEX_ALLOWED);
        return is_array($allowed) ? Arr::get($params, self::PARAMS_INDEX_DIMENSION) : '';
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;
        return is_array($value) ? $this->checkAsArray($value) : $this->checkAsString($value);
    }

    /**
     * Check if argument is array
     *
     * @param $values
     *
     * @return bool
     */
    private function checkAsArray(array $values): bool
    {
        return array_reduce($values, function ($result) {
            return $result || $this->allowed;
        });
    }

    /**
     * Check if argument is string
     *
     * @param $value
     *
     * @return string
     */
    private function checkAsString($value): string
    {
        return in_array((string)$value, $this->allowed);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        $attribute = $this->attribute;
        $lowerRule = 'included';
        $customKey = "validation.custom.{$attribute}.{$lowerRule}";
        $customMessage = $this->getCustomMessageFromTranslator($customKey);

        if ($customMessage) {
            return $customMessage;
        }

        $allowedValues = [];
        foreach ($this->allowed as $allowed) {
            if (Lang::has('validation.creative.included_params.' . $allowed)) {
                $allowedValues[] = __('validation.creative.included_params.' . $allowed);
            } else {
                $allowedValues[] = $allowed;
            }
        }

        $translation = $this->dimension ? 'validation.included_with_dimension' : 'validation.included';

        return __($translation, [
            'values'    => implode(', ', $allowedValues),
            'dimension' => $this->dimension,
        ]);
    }

    /**
     * @param array $properties
     *
     * @return self
     */
    public static function __set_state(array $properties)
    {
        return new self([
            $properties['allowed'],
            $properties['dimension'],
        ]);
    }
}
