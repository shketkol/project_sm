<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\User\Repositories\UserRepository;

class ValidateUserStatus implements Rule
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var array
     */
    private $statusIds;

    /**
     * @param array $statusIds
     */
    public function __construct(array $statusIds)
    {
        $this->repository = app(UserRepository::class);
        $this->statusIds = $statusIds;
    }

    /**
     * Determine if the validation rule passes.
     * @param string $attribute
     * @param array  $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        return !empty($this->repository->byStatus($this->statusIds)
            ->findWhere(['account_external_id' => $value])->first());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.custom.user_not_found');
    }
}
