<?php

namespace App\Rules;

use App\Helpers\Math\CalculationHelper;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Modules\User\Repositories\UserRepository;

class MinPerDayImpressions implements Rule
{
    /**
     * @var UserRepository
     */
    private $minPerDay;

    /**
     * Determine if the validation rule passes.
     * @param string $attribute
     * @param array  $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        $duration = Arr::get($value, 'duration');
        $this->minPerDay = Arr::get($value, 'minPerDay');
        $impression = CalculationHelper::calculateImpressions(
            Arr::get($value, 'cpm'),
            Arr::get($value, 'cost')
        );

        return $duration && $impression / $duration >= $this->minPerDay;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('campaign::messages.min_budget_message', [
            'value' => number_format((float)$this->minPerDay, 0, '', ',')
        ]);
    }
}
