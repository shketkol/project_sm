<?php

namespace App\Rules;

use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\SpoofCheckValidation;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class ValidEmail
 * @package App\Rules
 */
class ValidEmail implements Rule
{
    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $domain;

    /**
     * @var string
     */
    protected $email;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        return $this->parseEmail($value) && $this->checkReferences();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.valid_email');
    }

    /**
     * @param string $email
     * @return bool
     */
    protected function parseEmail(string $email): bool
    {
        $emailParts = explode('@', $email);

        if (count($emailParts) !== 2) {
            return false;
        }

        $this->email = $email;
        $this->address = $emailParts[0];
        $this->domain = $emailParts[1];

        return true;
    }

    /**
     * @return bool
     */
    protected function checkDomain(): bool
    {
        $validator = new EmailValidator();
        $validations = new MultipleValidationWithAnd([
            new SpoofCheckValidation(),
            new DNSCheckValidation()
        ]);

        return $validator->isValid($this->email, $validations);
    }

    /**
     * @return bool
     */
    protected function checkReferences(): bool
    {
        return strpos($this->address, '+') !== false ? // check '+' in email address
            $this->domain === 'gmail.com' ? true : false : // check domain is gmail if '+' is present
            true; // there is no '+' in address
    }
}
