<?php

namespace App\Exceptions;

class MethodMustBeOverriddenException extends BaseException
{
    /**
     * @param string $method
     * @param string $class
     *
     * @return self
     */
    public static function create(string $method, string $class): self
    {
        $exception = new self();
        $exception->message = sprintf('Method "%s" must be overridden in class "%s".', $method, $class);

        return $exception;
    }
}
