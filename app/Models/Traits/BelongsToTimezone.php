<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Timezone;

/**
 * Trait HasTimezone
 *
 * @package App\Models\Traits
 * @property Timezone $timezone
 */
trait BelongsToTimezone
{
    /**
     * Has timezone
     * @return BelongsTo
     */
    public function timezone(): BelongsTo
    {
        return $this->belongsTo(Timezone::class)->withDefault([
            'code' => '',
            'name' => '',
        ]);
    }
}
