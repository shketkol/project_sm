<?php

namespace App\Models;

class QueuePriority
{
    public const LOW = 'low';
    public const DEFAULT = 'default';
    public const HIGH = 'high';

    /**
     * @return string
     */
    public static function low(): string
    {
        return config('queue.priority.low');
    }

    /**
     * @return string
     */
    public static function default(): string
    {
        return config('queue.priority.default');
    }

    /**
     * @return string
     */
    public static function high(): string
    {
        return config('queue.priority.high');
    }
}
