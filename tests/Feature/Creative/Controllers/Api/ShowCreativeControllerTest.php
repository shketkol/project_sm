<?php

namespace Tests\Feature\Creative\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Models\Traits\CreateCreative;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;

class ShowCreativeControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCreative;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test show creative.
     */
    public function testShowCreative(): void
    {
        $user = $this->createTestAdvertiser();
        $this->actingAs($user);

        $creative = $this->createTestCreative([
            'status_id'  => CreativeStatus::ID_DRAFT,
            'user_id' => $user->getKey(),
        ]);

        // Invalid
        $response = $this->json(Request::METHOD_GET, route('api.creatives.show', [
            'creative' => $creative->id + 1
        ]));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

        // Valid
        $response = $this->json(Request::METHOD_GET, route('api.creatives.show', [
            'creative' => $creative->id
        ]));
        $response->assertStatus(Response::HTTP_OK);
        $data = $response->json('data');
        $this->assertEquals($creative->id, Arr::get($data, 'id'));
        $this->assertEquals($creative->name, Arr::get($data, 'name'));
        $this->assertEquals($creative->original_key, Arr::get($data, 'original_key'));
    }
}
