<?php

namespace Tests\Feature\Creative\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Models\Traits\CreateCreative;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;

class DeleteCreativeControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCreative;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test delete creative.
     */
    public function testDeleteCreative(): void
    {
        $user = $this->createTestAdvertiser();
        $this->actingAs($user);

        $nonDraftCreative = $this->createTestCreative([
            'status_id'  => CreativeStatus::ID_PENDING_APPROVAL,
            'user_id' => $user->getKey(),
        ]);

        $creative = $this->createTestCreative([
            'status_id'  => CreativeStatus::ID_DRAFT,
            'user_id' => $user->getKey(),
        ]);

       // Invalid
        $response = $this->json(Request::METHOD_DELETE, route('api.creatives.delete', [
            'creative' => $creative->id + 1
        ]));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

        // Invalid
        $response = $this->json(Request::METHOD_DELETE, route('api.creatives.delete', [
            'creative' => $nonDraftCreative->id
        ]));
        $response->assertStatus(Response::HTTP_FORBIDDEN);

        // Valid
        $response = $this->json(Request::METHOD_DELETE, route('api.creatives.delete', [
            'creative' => $creative->id
        ]));
        $response->assertStatus(Response::HTTP_OK);
    }
}
