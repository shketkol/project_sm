<?php

namespace Tests\Feature\Creative\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Helpers\CreativePathHelper;
use Modules\Creative\Models\Traits\CreateCreative;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Tests\TestCase;
use Tests\Traits\Creative\GetCreativePreviewMock;
use Tests\Traits\Creative\GetMediaInfoMock;

class StoreCreativeControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCreative,
        CreateCampaign,
        GetMediaInfoMock,
        GetCreativePreviewMock;

    /**
     * @var User
     */
    private $advertiser;

    /**
     * @var \Modules\Campaign\Models\Campaign
     */
    private $campaign;

    /**
     * Setup the test environment.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->advertiser = $this->createTestAdvertiser();
        $this->campaign = $this->createTestCampaign(['user_id' => $this->advertiser->id]);
        $this->actingAs($this->advertiser);
    }

    /**
     * Test store creative.
     *
     * @dataProvider namesDataProvider
     *
     * @param array $data
     *
     * @return void
     *
     */
    public function testStoreCreative(array $data): void
    {
        $request = Arr::get($data, 'request');
        $expectedResponse = Arr::get($data, 'response');

        Arr::set($request, 's3Key', $this->getCreativePath());
        Arr::set($request, 'campaign_id', $this->campaign->id);

        if (!empty(Arr::get($expectedResponse, 'mockMediaInfo'))) {
            $this->mockParseCreativeActionSuccess(Arr::get($expectedResponse, 'mockMediaInfo'));
            $this->mockCreatePreviewActionSuccess($this->getCreativePath());
        }

        $response = $this->json(Request::METHOD_POST, route('api.creatives.store'), $request);

        $response->assertStatus(Arr::get($expectedResponse, 'code'));

        $content = Arr::get($expectedResponse, 'content');
        if (!empty($content)) {
            $this->assertEquals($response->getContent(), $content);
        }

        if (Arr::get($expectedResponse, 'code') == Response::HTTP_CREATED) {
            $this->assertDatabaseHas('creatives', [
                'original_key' => $this->getCreativePath(),
            ]);
        }
    }

    /**
     * @return string
     */
    public function getCreativePath(): string
    {
        $creativeHelper = app(CreativePathHelper::class);
        $expectedPath = $creativeHelper->getCreativePath($this->advertiser);
        $expectedParts = explode('/', $expectedPath);
        $expectedUserFolder = Arr::get($expectedParts, CreativePathHelper::USER_FOLDER_INDEX);

        return $expectedUserFolder . '/creatives/test.mp4';
    }

    /**
     * @return \Generator
     */
    public function namesDataProvider(): \Generator
    {
        // Without name params
        yield [
            [
                'request' => [
                    'hashSum' => substr(md5(rand()), 0, 15),
                ],
                'response' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'content' => []
                ]
            ],
        ];

        // Invalid video format
        yield [
            [
                'request' => [
                    'hashSum' => substr(md5(rand()), 0, 15),
                    'name' => substr(md5(rand()), 0, 10),
                ],
                'response' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'content' =>  '{"message":"The given data was invalid.","errors":{"video_format":["Make sure the File Video Format of your Ad is one of the following: MPEG-4, MOV, QuickTime."]}}',
                    'mockMediaInfo' => [
                        'video_cnt' => '1',
                        'audio_cnt' => '1',
                        'extension' => 'mp4',
                        'file_size' => 61615437,
                        'duration' => 30.03,
                        'video_format' => 'MKV',
                        'video_bit_rate' => '16414369',
                        'codec' => 'avc1',
                        'width' => 1920,
                        'height' => 1080,
                        'display_aspect_ratio' => '16:9',
                        'frame_rate_mode' => 'CFR',
                        'frame_rate' => '29.970',
                        'color_space' => 'YUV',
                        'chroma_subsampling' => ['4:2:0', '4:2:0'],
                        'bit_depth' => 8,
                        'scan_type' => 'Progressive',
                        'audios' => [
                            [
                                'format' => 'AAC',
                                'duration' => 30.03,
                                'bit_rate' => 317375,
                                'channels' => 2,
                                'sampling_rate' => 48000,
                                'bit_depth' => null
                            ]
                        ]
                    ]
                ]
            ],
        ];

        // Invalid width
        yield [
            [
                'request' => [
                    'hashSum' => substr(md5(rand()), 0, 15),
                    'name' => substr(md5(rand()), 0, 10),
                ],
                'response' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'content' =>  '{"message":"The given data was invalid.","errors":{"width":["Make sure the Width of your Ad is at least 1280 px."]}}',
                    'mockMediaInfo' => [
                        'video_cnt' => '1',
                        'audio_cnt' => '1',
                        'extension' => 'mp4',
                        'file_size' => 61615437,
                        'duration' => 30.03,
                        'video_format' => 'MPEG-4',
                        'video_bit_rate' => '16414369',
                        'codec' => 'avc1',
                        'width' => 720,
                        'height' => 1080,
                        'display_aspect_ratio' => '16:9',
                        'frame_rate_mode' => 'CFR',
                        'frame_rate' => '29.970',
                        'color_space' => 'YUV',
                        'chroma_subsampling' => ['4:2:0', '4:2:0'],
                        'bit_depth' => 8,
                        'scan_type' => 'Progressive',
                        'audios' => [
                            [
                                'format' => 'AAC',
                                'duration' => 30.03,
                                'bit_rate' => 317375,
                                'channels' => 2,
                                'sampling_rate' => 48000,
                                'bit_depth' => null
                            ]
                        ]
                    ]
                ]
            ],
        ];

        // Valid data
        yield [
            [
                'request' => [
                    'hashSum' => substr(md5(rand()), 0, 15),
                    'name' => substr(md5(rand()), 0, 10),
                ],
                'response' => [
                    'code' => Response::HTTP_CREATED,
                    'content' =>  '',
                    'mockMediaInfo' => [
                        'video_cnt' => '1',
                        'audio_cnt' => '1',
                        'extension' => 'mp4',
                        'file_size' => 61615437,
                        'duration' => 30.03,
                        'video_format' => 'MPEG-4',
                        'video_bit_rate' => '16414369',
                        'codec' => 'avc1',
                        'width' => 1920,
                        'height' => 1080,
                        'display_aspect_ratio' => '16:9',
                        'frame_rate_mode' => 'CFR',
                        'frame_rate' => '29.970',
                        'color_space' => 'YUV',
                        'chroma_subsampling' => ['4:2:0', '4:2:0'],
                        'bit_depth' => 8,
                        'scan_type' => 'Progressive',
                        'audios' => [
                            [
                                'format' => 'AAC',
                                'duration' => 30.03,
                                'bit_rate' => 317375,
                                'channels' => 2,
                                'sampling_rate' => 48000,
                                'bit_depth' => null
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }
}
