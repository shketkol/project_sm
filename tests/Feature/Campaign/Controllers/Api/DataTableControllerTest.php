<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Models\Traits\CreateCreative;

class DataTableControllerTest extends TestCase
{
    use DatabaseTransactions, CreateCreative, CreateCampaign, CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test cards request.
     */
    public function testCards(): void
    {
        $advertiser = $this->createTestAdvertiser();

        $this->createTestCampaign([
            'status_id'  => CampaignStatus::ID_READY,
            'user_id' => $advertiser->getKey(),
        ]);

        $this->actingAs($advertiser);
        $response = $this->json(Request::METHOD_GET, route('dataTable.cards', ['table' => 'creatives']));
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test data table has items
     */
    public function testDataTable(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $count = 10;

        $campaigns = $this->createTestCampaign(['user_id' => $advertiser->getKey()], $count);

        $campaigns->each(function (Campaign $campaign) use ($advertiser) {
            $campaign->creatives()->attach($this->createTestCreative(['user_id' => $advertiser->getKey()]));
        });

        $this->actingAs($advertiser);

        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.index', ['table' => 'campaigns'])
        );
        $response->assertStatus(Response::HTTP_OK);
    }
}
