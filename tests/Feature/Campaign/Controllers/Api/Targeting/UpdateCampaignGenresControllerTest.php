<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Database\Seeders\Tests\TargetingTypesTableSeeder;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Targeting\Models\Traits\CreateGenre;
use Modules\User\Models\AccountType;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\Feature\Campaign\Controllers\Api\Targeting\Traits\CreateDraftCampaign;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\CampaignPriceMock;

class UpdateCampaignGenresControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateDraftCampaign,
        CreateGenre,
        CampaignPriceMock;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(TargetingTypesTableSeeder::class);
    }

    /**
     * Test Update Campaign Genres as Common Advertiser.
     */
    public function testUpdateCampaignGenresAsCommonAdvertiser(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $this->mockCampaignPriceActionSuccess($advertiser->id);
        $campaign = $this->CreateDraftCampaign($advertiser);

        $targetings = $this->createTestTargetingGenre([], 5);

        // create payload to attach
        $payload = $this->createTargetingGenresPayload($targetings);

        $this->actingAs($advertiser);

        // attach created genres
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.genres.update', ['campaign' => $campaign->id]),
            ['genres' => $payload->toArray()]
        );

        $response->assertStatus(Response::HTTP_OK);

        foreach ($payload as $key => $value) {
            $genreId = Arr::get($value, 'genre_id');
            $excluded = Arr::get($value, 'excluded');

            $this->assertDatabaseHas('campaign_genres', [
                'genre_id'    => $genreId,
                'excluded'    => $excluded,
                'campaign_id' => $campaign->id,
            ]);

            $this->assertEquals($genreId, $response->json("data.targeting.genres.{$key}.id"));
            $this->assertEquals($excluded, $response->json("data.targeting.genres.{$key}.excluded"));
        }

        $this->assertEquals($campaign->id, $response->json('data.id'));

        // detach half
        $payload = $payload->splice(ceil($payload->count() / 2));

        // detach created genres
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.genres.update', ['campaign' => $campaign->id]),
            ['genres' => $payload->toArray()]
        );

        $response->assertStatus(Response::HTTP_OK);

        foreach ($payload as $key => $value) {
            $genreId = Arr::get($value, 'genre_id');
            $excluded = Arr::get($value, 'excluded');

            $this->assertDatabaseHas('campaign_genres', [
                'genre_id'    => $genreId,
                'excluded'    => $excluded,
                'campaign_id' => $campaign->id,
            ]);

            $this->assertEquals($genreId, $response->json("data.targeting.genres.{$key}.id"));
            $this->assertEquals($excluded, $response->json("data.targeting.genres.{$key}.excluded"));
        }

        // detach all
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.genres.update', ['campaign' => $campaign->id])
        );

        $this->assertDatabaseMissing('campaign_genres', [
            'campaign_id' => $campaign->id,
        ]);

        $this->assertEmpty($response->json('data.targeting.genres'));
    }

    /**
     * Test Update Campaign Genres as Special-Ads Advertiser.
     */
    public function testUpdateCampaignGenresAsSpecialAdsAdvertiser(): void
    {
        $advertiser = $this->createTestAdvertiser([
            'account_type_id' => AccountType::ID_SPECIAL_ADS,
        ]);
        $campaign = $this->CreateDraftCampaign($advertiser);

        $targetings = $this->createTestTargetingGenre([], 5);

        // create payload to attach
        $payload = $this->createTargetingGenresPayload($targetings);

        $this->actingAs($advertiser);

        // attach created genres
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.genres.update', ['campaign' => $campaign->id]),
            ['genres' => $payload->toArray()]
        );

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
