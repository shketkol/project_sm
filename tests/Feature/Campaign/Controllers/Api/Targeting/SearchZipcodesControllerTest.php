<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Targeting\Models\Traits\CreateZipcode;
use Modules\Targeting\Models\Zipcode;
use Modules\User\Models\AccountType;
use Modules\User\Models\Traits\CreateUser;
use Tests\Feature\Campaign\Controllers\Api\Targeting\Traits\CreateDraftCampaign;
use Tests\TestCase;

class SearchZipcodesControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateZipcode,
        CreateDraftCampaign,
        CreateUser;

    /**
     * @var Zipcode
     */
    private $zipCode;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->zipCode = $this->createTestTargetingZipcode();
    }

    /**
     * Test search zip codes as Common Advertiser.
     */
    public function testSearchZipCodesAsCommonAdvertiser(): void
    {
        $user = $this->createTestAdvertiser();
        $this->actingAs($user);

        $campaign = $this->createDraftCampaign($user);

        $response = $this->post(route('api.campaigns.targeting.zipcodes', ['campaign' => $campaign->id]), [
            'zipcodes' => [
                $this->zipCode->name,
            ]
        ]);

        $response->assertOk();
        $data = $response->json();
        $this->assertEquals($this->zipCode->name, Arr::get($data, 'data.found.0.name'));
        $this->assertEmpty(Arr::get($data, 'data.not_found'));
    }

    /**
     * Test search zip codes as SpecialAds Advertiser.
     */
    public function testSearchZipCodesAsSpecialAdsAdvertiser(): void
    {
        $user = $this->createTestAdvertiser([
            'account_type_id' => AccountType::ID_SPECIAL_ADS,
        ]);
        $this->actingAs($user);

        $campaign = $this->createDraftCampaign($user);

        $response = $this->post(route('api.campaigns.targeting.zipcodes', ['campaign' => $campaign->id]), [
            'zipcodes' => [
                $this->zipCode->name,
            ]
        ]);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
