<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Targeting\Models\Traits\CreateZipcode;
use Modules\Targeting\Models\Zipcode;
use Modules\User\Models\Traits\CreateUser;
use Tests\Helpers\CreateFile;
use Tests\TestCase;
use Modules\Campaign\Models\Traits\CreateCampaign;

class UploadCsvControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateCampaign,
        CreateUser,
        CreateZipcode;

    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $user = $this->createTestAdvertiser();
        $this->actingAs($user);

        $this->campaign = $this->createTestCampaign([
            'user_id' => $user->getKey(),
            'status_id'  => CampaignStatus::ID_DRAFT,
        ]);

        $this->createTestTargetingZipcode([], 5);

        Zipcode::where(['name' => '12345'])->delete();

        $this->createTestTargetingZipcode(['name' => '12345'], 1);
    }

    /**
     * Test upload csv file.
     *
     * @dataProvider namesDataProvider
     *
     * @param array $data
     *
     * @return void
     */
    public function testUploadCsv($data): void
    {
        $file = Arr::get($data, 'file');
        $expectedStatusCode = Arr::get($data, 'expected_result.code');
        $expectedResponse = Arr::get($data, 'response');

        $response = $this->json(Request::METHOD_POST, route('api.campaigns.targeting.upload.csv', ['campaign' => $this->campaign->id]), [
            'csv' => $file
        ]);

        $response->assertStatus($expectedStatusCode);

        if ($expectedResponse) {
            $responseContent = $response->json();

            foreach (Arr::get($responseContent, 'data.found') as $value) {
                $this->assertDatabaseHas('zipcodes', [
                    'id' => Arr::get($value, 'id'),
                ]);
            }
        }
    }

    /**
     * @return \Generator
     */
    public function namesDataProvider(): \Generator
    {
        // Max filesize
        yield [
            [
                'file' => UploadedFile::fake()->create('test.csv', 6),
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ]
        ];

        // Invalid format
        yield [
            [
                'file' => CreateFile::createWithContent('test.csv', 'test"'),
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ]
        ];

        // Not found content content
        yield [
            [
                'file' => CreateFile::createWithContent('test.csv', '00000,08500'),
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
                'response' => ['found' => [], 'not_found' => ['00000', '08500']]
            ]
        ];

        // Valid content
        yield [
            [
                'file' => CreateFile::createWithContent('test.csv', '12345'),
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
                'response' => ['found' => [['name' => '12345']], 'not_found' => []]
            ]
        ];
    }
}
