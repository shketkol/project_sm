<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use App\Models\Timezone;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Modules\Campaign\Models\Traits\CreateCampaign;
use TimezoneTableSeeder;

class UpdateCampaignScheduleControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCampaign;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(TimezoneTableSeeder::class);
    }

    /**
     * Test Update Campaign Schedule.
     *
     * @dataProvider scheduleDataProvider
     *
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param int    $code
     */
    public function testUpdateCampaignSchedule(Carbon $startDate, Carbon $endDate, int $code): void
    {
        $startDate = $startDate->format(config('date.format.php'));
        $endDate = $endDate->format(config('date.format.php'));

        $advertiser = $this->createTestAdvertiser();
        $campaign = $this->createTestCampaign([
            'user_id'   => $advertiser->getKey(),
            'status_id' => CampaignStatus::ID_DRAFT,
        ]);

        $this->actingAs($advertiser);

        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.details.schedule.update', ['campaign' => $campaign->id]),
            [
                'date'        => [
                    'start' => $startDate,
                    'end'   => $endDate,
                ],
                'timezone_id' => Timezone::ID_CENTRAL,
            ]
        );

        $response->assertStatus($code);
    }

    /**
     * @return \Generator
     */
    public function scheduleDataProvider(): \Generator
    {
        // test wrong start min
        yield [
            Carbon::now(),
            Carbon::now()->addMonth(),
            Response::HTTP_OK,
        ];

        // test wrong start max
        yield [
            Carbon::now()->addMonths(4),
            Carbon::now()->addMonths(5),
            Response::HTTP_OK,
        ];

        // test wrong end max
        yield [
            Carbon::now()->addDays(4),
            Carbon::now()->addMonths(13),
            Response::HTTP_OK,
        ];

        // test all ok
        yield [
            Carbon::now()->addDays(4),
            Carbon::now()->addMonths(12),
            Response::HTTP_OK,
        ];
    }
}
