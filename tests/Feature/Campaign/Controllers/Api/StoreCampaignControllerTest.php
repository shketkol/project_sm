<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\User\Models\Traits\CreateUser;
use Modules\Targeting\Models\Traits\ScoutIndex;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;

class StoreCampaignControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        ScoutIndex;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test store campaign.
     *
     * @dataProvider namesDataProvider
     *
     * @param array $data
     */
    public function testStoreCampaign(array $data): void
    {
        $name = Arr::get($data, 'name');
        $expectedStatusCode = Arr::get($data, 'expected_result.code');

        $advertiser = $this->createTestAdvertiser();

        $this->actingAs($advertiser);

        $response = $this->json(Request::METHOD_POST, route('api.campaigns.store'), [
            'name' => $name,
        ]);

        $response->assertStatus($expectedStatusCode);
    }

    /**
     * @return \Generator
     */
    public function namesDataProvider(): \Generator
    {
        // english
        yield [
            [
                'name'            => 'test campaign',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // spanish
        yield [
            [
                'name'            => 'campaña de prueba',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // albanian
        yield [
            [
                'name'            => 'fushatë provë',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // amharic
        yield [
            [
                'name'            => 'የሙከራ ዘመቻ።',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // arabic
        yield [
            [
                'name'            => 'حملة اختبار',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // armenian
        yield [
            [
                'name'            => 'թեստային արշավ',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // belorussian
        yield [
            [
                'name'            => 'тэставая кампанія',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // chinese
        yield [
            [
                'name'            => '测试活动',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // czech
        yield [
            [
                'name'            => 'testovací kampaň',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // georgian
        yield [
            [
                'name'            => 'სატესტო კამპანია',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // greek
        yield [
            [
                'name'            => 'δοκιμή καμπάνιας',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // gujarati
        yield [
            [
                'name'            => 'પરીક્ષણ ઝુંબેશ',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // hawaiian
        yield [
            [
                'name'            => 'hōʻike hoʻāʻo',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // hebrew
        yield [
            [
                'name'            => 'קמפיין מבחן',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // hindi
        yield [
            [
                'name'            => 'परीक्षण अभियान',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // icelandic
        yield [
            [
                'name'            => 'próf herferð',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // igbo
        yield [
            [
                'name'            => 'mkpọsa nnwale',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // japanese
        yield [
            [
                'name'            => 'テストキャンペーン',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // kannada
        yield [
            [
                'name'            => 'ಪರೀಕ್ಷಾ ಅಭಿಯಾನ',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // kazakh
        yield [
            [
                'name'            => 'сынақ науқаны',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // khmer
        yield [
            [
                'name'            => 'យុទ្ធនាការសាកល្បង។',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // korean
        yield [
            [
                'name'            => '테스트 캠페인',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // kyrgyz
        yield [
            [
                'name'            => 'сыноо өнөктүгү',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // lao
        yield [
            [
                'name'            => 'ຂະບວນການທົດສອບ',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // latvian
        yield [
            [
                'name'            => 'testa kampaņa',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // macedonian
        yield [
            [
                'name'            => 'тест кампања',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // malayalam
        yield [
            [
                'name'            => 'പരീക്ഷണ കാമ്പെയ്‌ൻ',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // maori
        yield [
            [
                'name'            => 'whakamātautau whakamatautau',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // marathi
        yield [
            [
                'name'            => 'चाचणी मोहीम',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // mayanmar
        yield [
            [
                'name'            => 'စမ်းသပ်မှုမဲဆွယ်စည်းရုံးရေး',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // nepali
        yield [
            [
                'name'            => 'परीक्षण अभियान',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // pashto
        yield [
            [
                'name'            => 'د ازمونې کمپاین',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // samoan
        yield [
            [
                'name'            => 'suega faʻataʻitaʻiga',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // serbian
        yield [
            [
                'name'            => 'тест кампања',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // sinhala
        yield [
            [
                'name'            => 'පරීක්ෂණ ව්‍යාපාරය',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // tajik
        yield [
            [
                'name'            => 'маъракаи санҷишӣ',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // tamil
        yield [
            [
                'name'            => 'சோதனை பிரச்சாரம்',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // telugu
        yield [
            [
                'name'            => 'పరీక్ష ప్రచారం',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // thai
        yield [
            [
                'name'            => 'ทดสอบแคมเปญ',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // urdu
        yield [
            [
                'name'            => 'ٹیسٹ مہم۔',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // vietnamese
        yield [
            [
                'name'            => 'chiến dịch thử nghiệm',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // yiddish
        yield [
            [
                'name'            => 'פּרובירן קאמפאניע',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // emoji
        yield [
            [
                'name'            => '⌛🖥⌨️',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // short name
        yield [
            [
                'name'            => 'a',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // long name would be stripped
        yield [
            [
                'name'            => 'test test test test test test test test test test test test test test test test '
                                     . 'test test test test test test test test test test test test test test test '
                                     . 'test test test test test test test test test test test test test test test '
                                     . 'test test test test campaign',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];

        // symbols
        yield [
            [
                'name'            => '~!@#$%^&*()_-=+/\\',
                'expected_result' => [
                    'code' => Response::HTTP_CREATED,
                ],
            ],
        ];
    }
}
