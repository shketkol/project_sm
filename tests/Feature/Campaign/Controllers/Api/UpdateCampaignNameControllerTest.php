<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\User\Models\Traits\CreateUser;
use Modules\Targeting\Models\Traits\ScoutIndex;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;

class UpdateCampaignNameControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        ScoutIndex,
        CreateCampaign;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test update campaign name.
     *
     * @dataProvider namesDataProvider
     *
     * @param array $data
     */
    public function testUpdateCampaignName(array $data): void
    {
        $name = Arr::get($data, 'name');
        $expectedStatusCode = Arr::get($data, 'expected_result.code');

        $advertiser = $this->createTestAdvertiser();
        $campaign = $this->createTestCampaign([
            'user_id'   => $advertiser->id,
            'status_id' => CampaignStatus::ID_DRAFT,
        ]);

        $this->actingAs($advertiser);

        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.details.name.update', ['campaign' => $campaign->id]),
            ['name' => $name]
        );

        $response->assertStatus($expectedStatusCode);
    }

    /**
     * @return \Generator
     */
    public function namesDataProvider(): \Generator
    {
        // english
        yield [
            [
                'name'            => 'test campaign',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // spanish
        yield [
            [
                'name'            => 'campaña de prueba',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // albanian
        yield [
            [
                'name'            => 'fushatë provë',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // amharic
        yield [
            [
                'name'            => 'የሙከራ ዘመቻ።',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // arabic
        yield [
            [
                'name'            => 'حملة اختبار',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // armenian
        yield [
            [
                'name'            => 'թեստային արշավ',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // belorussian
        yield [
            [
                'name'            => 'тэставая кампанія',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // chinese
        yield [
            [
                'name'            => '测试活动',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // czech
        yield [
            [
                'name'            => 'testovací kampaň',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // georgian
        yield [
            [
                'name'            => 'სატესტო კამპანია',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // greek
        yield [
            [
                'name'            => 'δοκιμή καμπάνιας',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // gujarati
        yield [
            [
                'name'            => 'પરીક્ષણ ઝુંબેશ',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // hawaiian
        yield [
            [
                'name'            => 'hōʻike hoʻāʻo',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // hebrew
        yield [
            [
                'name'            => 'קמפיין מבחן',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // hindi
        yield [
            [
                'name'            => 'परीक्षण अभियान',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // icelandic
        yield [
            [
                'name'            => 'próf herferð',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // igbo
        yield [
            [
                'name'            => 'mkpọsa nnwale',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // japanese
        yield [
            [
                'name'            => 'テストキャンペーン',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // kannada
        yield [
            [
                'name'            => 'ಪರೀಕ್ಷಾ ಅಭಿಯಾನ',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // kazakh
        yield [
            [
                'name'            => 'сынақ науқаны',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // khmer
        yield [
            [
                'name'            => 'យុទ្ធនាការសាកល្បង។',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // korean
        yield [
            [
                'name'            => '테스트 캠페인',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // kyrgyz
        yield [
            [
                'name'            => 'сыноо өнөктүгү',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // lao
        yield [
            [
                'name'            => 'ຂະບວນການທົດສອບ',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // latvian
        yield [
            [
                'name'            => 'testa kampaņa',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // macedonian
        yield [
            [
                'name'            => 'тест кампања',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // malayalam
        yield [
            [
                'name'            => 'പരീക്ഷണ കാമ്പെയ്‌ൻ',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // maori
        yield [
            [
                'name'            => 'whakamātautau whakamatautau',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // marathi
        yield [
            [
                'name'            => 'चाचणी मोहीम',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // mayanmar
        yield [
            [
                'name'            => 'စမ်းသပ်မှုမဲဆွယ်စည်းရုံးရေး',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // nepali
        yield [
            [
                'name'            => 'परीक्षण अभियान',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // pashto
        yield [
            [
                'name'            => 'د ازمونې کمپاین',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // samoan
        yield [
            [
                'name'            => 'suega faʻataʻitaʻiga',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // serbian
        yield [
            [
                'name'            => 'тест кампања',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // sinhala
        yield [
            [
                'name'            => 'පරීක්ෂණ ව්‍යාපාරය',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // tajik
        yield [
            [
                'name'            => 'маъракаи санҷишӣ',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // tamil
        yield [
            [
                'name'            => 'சோதனை பிரச்சாரம்',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // telugu
        yield [
            [
                'name'            => 'పరీక్ష ప్రచారం',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // thai
        yield [
            [
                'name'            => 'ทดสอบแคมเปญ',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // urdu
        yield [
            [
                'name'            => 'ٹیسٹ مہم۔',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // vietnamese
        yield [
            [
                'name'            => 'chiến dịch thử nghiệm',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // yiddish
        yield [
            [
                'name'            => 'פּרובירן קאמפאניע',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // emoji
        yield [
            [
                'name'            => '⌛🖥⌨️',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // short name
        yield [
            [
                'name'            => 'a',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // long name would be stripped
        yield [
            [
                'name'            => 'test test test test test test test test test test test test test test test test '
                                     . 'test test test test test test test test test test test test test test test '
                                     . 'test test test test test test test test test test test test test test test '
                                     . 'test test test test campaign',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // symbols
        yield [
            [
                'name'            => '~!@#$%^&*()_-=+/\\',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];
    }
}
