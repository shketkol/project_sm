<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\Creative\Models\Traits\CreateCreative;
use Modules\User\Models\Role;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminDataTableControllerTest extends TestCase
{
    use DatabaseTransactions, CreateCreative, CreateCampaign, CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test cards request.
     */
    public function testCards(): void
    {
        $this->markTestSkipped('HULU-539');

        $advertiser = $this->createTestAdvertiser();
        $admin = $this->createTestAdmin();
        $budget = 0;
        $previousMonthBudget = 0;
        $currentMonthBudget = 0;
        $now = Carbon::now();

        $this->createTestCampaign([
            'user_id' => $advertiser->getKey(),
            'date_start' => $now->clone()->subMonth()->format('Y-m-d'),
            'date_end' => $now->clone()->subMonth()->format('Y-m-d')
        ], 5);

        $this->createTestCampaign([
            'user_id' => $advertiser->getKey(),
            'date_end' => $now->clone()->format('Y-m-d')
        ], 3);

        $campaigns = Campaign::all();
        foreach ($campaigns as $campaign) {
            $budget += $campaign->budget;

            if ($campaign->date_end >= $now->clone()->startOfMonth() &&
                $campaign->date_end <= $now->clone()->endOfMonth()
            ) {
                $currentMonthBudget += $campaign->budget;
            }

            if ($campaign->date_end >= $now->clone()->subMonth()->startOfMonth() &&
                $campaign->date_end <= $now->clone()->subMonth()->endOfMonth()
            ) {
                $previousMonthBudget += $campaign->budget;
            }
        }

        $this->actingAs($admin, 'admin');

        $response = $this->json(Request::METHOD_GET, route('dataTable.cards', ['table' => 'admin-campaigns']));
        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals(round($currentMonthBudget), round($response->json(0 . '.value')));
        $this->assertEquals(round($previousMonthBudget), round($response->json(1 . '.value')));
        $this->assertEquals(round($budget), round($response->json(3 . '.value')));

        $this->actingAs($admin->assignRole(Role::ID_ADMIN_READ_ONLY), 'admin');
        $response = $this->json(Request::METHOD_GET, route('dataTable.cards', ['table' => 'admin-campaigns']));
        $response->assertStatus(Response::HTTP_OK);

        $this->actingAs($advertiser);
        $response = $this->json(Request::METHOD_GET, route('dataTable.cards', ['table' => 'admin-campaigns']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * Test data table has items
     */
    public function testDataTable(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $secondAdvertiser = $this->createTestAdvertiser();
        $admin = $this->createTestAdmin();

        $this->createTestCampaign(['user_id' => $advertiser->getKey()], 5);
        $this->createTestCampaign(['user_id' => $secondAdvertiser->getKey()], 5);

        $this->actingAs($admin, 'admin');

        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.index', ['table' => 'admin-campaigns'])
        );

        $response->assertStatus(Response::HTTP_OK);
        $this->assertCount(Campaign::all()->count(), $response->json('data'));

        $this->actingAs($admin->assignRole(Role::ID_ADMIN_READ_ONLY), 'admin');
        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.index', ['table' => 'admin-campaigns'])
        );
        $response->assertStatus(Response::HTTP_OK);
        $this->assertCount(Campaign::all()->count(), $response->json('data'));

        $this->actingAs($secondAdvertiser);
        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.index', ['table' => 'admin-campaigns'])
        );

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
