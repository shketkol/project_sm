<?php

namespace Tests\Feature\Invitation\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Models\Traits\CreateInvite;
use Tests\TestCase;

class ValidateInviteCodeControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateInvite;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->createTestInviteCode([
            'email'     => 'validate_invite-test@advertiser.com',
            'code'      => '8XtYBR3v',
            'status_id' => InvitationStatus::ID_INVITED,
        ]);

        $this->createTestInviteCode([
            'email'     => 'sheen-test@advertiser.com',
            'code'      => 'uridun',
            'status_id' => InvitationStatus::ID_PROCESSING,
        ]);
    }

    /**
     * Test validate invite code.
     *
     * @dataProvider namesDataProvider
     *
     * @param array $data
     *
     * @return void
     */
    public function testValidateInviteCode($data): void
    {
        $code = Arr::get($data, 'code');
        $expectedStatusCode = Arr::get($data, 'expected_result.code');

        $response = $this->json(Request::METHOD_POST, route('api.invitations.validateCode'), [
            'code' => $code,
        ]);

        $response->assertStatus($expectedStatusCode);
    }

    /**
     * @return \Generator
     */
    public function namesDataProvider(): \Generator
    {
        // Valid invite code
        yield [
            [
                "code"            => "8XtYBR3v",
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // Invalid invite code
        yield [
            [
                "code"            => "azazer",
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // Invalid invite code
        yield [
            [
                "code"            => "uridun",
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];
    }
}
