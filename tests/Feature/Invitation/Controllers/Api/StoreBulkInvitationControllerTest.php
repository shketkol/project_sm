<?php

namespace Tests\Feature\Invitation\Controllers\Api;

use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Models\Traits\CreateInvite;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;

class StoreBulkInvitationControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateInvite,
        CreateUser;

    /**
     * @var string
     */
    private $advertiserEmail;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $faker = app(Generator::class);
        $this->advertiserEmail = $faker->unique()->email;

        $this->createTestInviteCode([
            'email'     => $this->advertiserEmail,
            'code'      => '8XtYBR3v',
            'status_id' => InvitationStatus::ID_INVITED,
        ]);

        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');
    }

    /**
     * Test store invites by emails.
     *
     * @return void
     */
    public function testValidateInviteCode(): void
    {
        // Valid emails
        $payload = [
            'emails'                => [
                'test+local@gmail.com',
                'test+local1@gmail.com',
            ],
            'account_type_category' => false,
        ];

        $response = $this->json(Request::METHOD_POST, route('api.invitations.storeBulk'), $payload);
        $response->assertStatus(Response::HTTP_OK);

        // Invalid emails (duplicate)
        $payload = [
            'emails' => [
                $this->advertiserEmail,
                'test+local2@gmail.com',
            ],
        ];

        $response = $this->json(Request::METHOD_POST, route('api.invitations.storeBulk'), $payload);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        // Invalid emails (wrong format)
        $payload = [
            'emails' => [
                'test+local3@gmail.com',
                'test',
            ],
        ];

        $response = $this->json(Request::METHOD_POST, route('api.invitations.storeBulk'), $payload);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
