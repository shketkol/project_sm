<?php

namespace Tests\Feature\Invitation\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;

class ValidateCsvControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');
    }

    /**
     * Test valid csv file.
     *
     * @dataProvider namesDataProvider
     *
     * @param array $data
     *
     * @return void
     */
    public function testValidateCsv($data): void
    {
        $file = Arr::get($data, 'file');
        $expectedStatusCode = Arr::get($data, 'expected_result.code');

        $response = $this->json(Request::METHOD_POST, route('api.invitations.validateCsv'), [
            'csv' => $file
        ]);

        $response->assertStatus($expectedStatusCode);
    }

    /**
     * @return \Generator
     */
    public function namesDataProvider(): \Generator
    {
        // Max filesize
        yield [
            [
                "file" => UploadedFile::fake()->create('test.csv', 6),
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ]
        ];

        // Valid content
        yield [
            [
                "file" => UploadedFile::fake()->create('test.csv', 5),
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ]
        ];
    }
}
