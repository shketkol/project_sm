<?php

namespace Tests\Feature\Invitation\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Models\Traits\CreateInvite;
use Modules\Invitation\Repositories\InvitationRepository;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;

class DestroyInvitationControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateInvite,
        CreateUser;

    /**
     * @var InvitationRepository
     */
    private $repository;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->createTestInviteCode([
            'email'     => 'destroy-test@advertiser.com',
            'code'      => '8XtYBR3v',
            'status_id' => InvitationStatus::ID_INVITED,
        ]);

        $this->repository = app(InvitationRepository::class);

        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');
    }

    /**
     * Test destroy invitation code.
     *
     * @param array $data
     *
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function testDestroyInvite(): void
    {
        $invite = $this->repository->byEmail('destroy-test@advertiser.com')->first();

        $response = $this->json(Request::METHOD_DELETE, route('api.invitations.destroy', [
            'invitation' => $invite->id,
        ]));

        $response->assertStatus(Response::HTTP_OK);
    }
}
