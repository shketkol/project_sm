<?php

namespace Tests\Feature\Targeting\Actions;

use DB;
use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Device\ImportDevicesAction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImportDevicesActionTest extends TestCase
{
    use DatabaseTransactions;


    /**
     * Test the number of imported records.
     *
     * @dataProvider importedRecordsDataProvider
     *
     * @param array $rounds
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @return void
     */
    public function testImportedRecords(array $rounds): void
    {
        $totalRecords = DB::table('devices')->count();
        foreach ($rounds as $round) {
            $this->handleImport($round['data']);
            $recordsAfter = DB::table('devices')->count();
            $this->assertEquals($totalRecords += $round['insertedRows'], $recordsAfter);
        }
    }

    /**
     * @return \Generator
     */
    public function importedRecordsDataProvider(): \Generator
    {
        yield [
            [
                [
                    'data'         => [
                        [
                            'id'        => 'd9cf5bd8-9e61-46cc-a8b6-885d607654b3',
                            'hierarchy' => [
                                'tiers' => [
                                    'Computer',
                                    'Hulu',
                                ],
                            ],
                            'name'      => 'Test - Hulu',
                            'visible'   => false,
                        ],
                        [
                            'id'        => 'd9cf5bd8-9e61-46cc-a8b6-885d607654b4',
                            'hierarchy' => [
                                'tiers' => [
                                    'Mobile',
                                    'Tablet',
                                ],
                            ],
                            'name'      => 'Test - Tablet',
                            'visible'   => false,
                        ],
                    ],
                    'insertedRows' => 2,
                ],
                [
                    'data'         => [
                        [
                            'id'        => 'd9cf5bd8-9e61-46cc-a8b6-885d607654b3',
                            'hierarchy' => [
                                'tiers' => [
                                    'Living Room',
                                    'SetTop',
                                ],
                            ],
                            'name'      => 'Test - SetTop',
                            'visible'   => false,
                        ],
                        [
                            'id'        => 'd9cf5bd8-9e61-46cc-a8b6-885d607654b4',
                            'hierarchy' => [
                                'tiers' => [
                                    'Mobile',
                                    'Tablet',
                                ],
                            ],
                            'name'      => 'Test - Tablet',
                            'visible'   => false,
                        ],
                        [
                            'id'        => 'd9cf5bd8-9e61-46cc-a8b6-885d607654b5',
                            'hierarchy' => [
                                'tiers' => [
                                    'Mobile',
                                    'Phone',
                                ],
                            ],
                            'name'      => 'Test - Phone',
                            'visible'   => true,
                        ],
                    ],
                    // Due to the duplicated records.
                    'insertedRows' => 1,
                ],
            ]
        ];
    }

    /**
     * Test data consistency after import.
     *
     * @dataProvider devicesConsistencyDataProvider
     *
     * @param array $data
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function testInsertedDataConsistency($data): void
    {
        $this->handleImport([$data]);

        $this->assertDatabaseHas('devices', [
            'external_id' => Arr::get($data, 'id'),
            'name'        => Arr::get($data, 'name'),
            'visible'     => Arr::get($data, 'visible'),
        ]);
    }

    /**
     * @return \Generator
     */
    public function devicesConsistencyDataProvider(): \Generator
    {
        yield [
            [
                'id'        => 'd9cf5bd8-9e61-46cc-a8b6-885d607654b1',
                'hierarchy' => [
                    'tiers' => [
                        'Computer',
                        'Hulu',
                    ],
                ],
                'name'      => 'Computer',
                'visible'   => true,
            ],
        ];
        yield [
            [
                'id'        => 'd9cf5bd8-9e61-46cc-a8b6-885d607654b2',
                'hierarchy' => [
                    'tiers' => [
                        'Mobile',
                        'Tablet',
                    ],
                ],
                'name'      => 'Tablet',
                'visible'   => false,
            ],
        ];
    }

    /**
     * @param array $data
     *
     * @return void
     */
    private function handleImport(array $data): void
    {
        $import = app(ImportDevicesAction::class);
        $collection = TargetingValuesCollection::create($data);
        $import->handle($collection);
    }
}
