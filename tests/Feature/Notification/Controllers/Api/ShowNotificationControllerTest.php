<?php

namespace Tests\Feature\Notification\Controllers\Api;

use Modules\Notification\Models\Traits\CreateNotification;
use Modules\Notification\Database\Seeders\NotificationPermissionsTableSeeder;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Models\Traits\CreateUser;

class ShowNotificationControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateNotification;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RolesTableSeeder::class);
        $this->seed(NotificationPermissionsTableSeeder::class);
    }

    /**
     * Test advertiser can view notification.
     */
    public function testAdvertiserNotificationView(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $notification = $this->createTestNotification(['notifiable_id' => $advertiser->id]);

        $this->actingAs($advertiser);
        $response = $this->getJson(route('api.notifications.show', ['notification' => $notification->id]));
        $response->assertOk();

        $this->assertCount(6, $response->json('data'));
        $this->assertIsString($response->json('data.id'));
        $this->assertNotEmpty($response->json('data.category'));
        $this->assertNotEmpty($response->json('data.content'));
        $this->assertIsString($response->json('data.content_preview'));
        $this->assertIsBool($response->json('data.is_read'));
        $this->assertNotEmpty($response->json('data.created_at'));

        // another advertiser can not see those notifications
        $advertiser2 = $this->createTestAdvertiser();

        $this->actingAs($advertiser2);
        $response = $this->getJson(route('api.notifications.show', ['notification' => $notification->id]));
        $response->assertForbidden();

        // admin can not see those notifications
        $admin = $this->createTestAdmin();

        $this->actingAs($admin, 'admin');
        $response = $this->getJson(route('api.notifications.show', ['notification' => $notification->id]));
        $response->assertForbidden();
    }
}
