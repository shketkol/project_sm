<?php

namespace Tests\Feature\Notification\Controllers\Api;

use Illuminate\Testing\TestResponse;
use Modules\Notification\Actions\GetNotificationWidgetDataAction;
use Modules\Notification\Models\Traits\CreateNotification;
use Modules\Notification\Database\Seeders\NotificationPermissionsTableSeeder;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Models\Traits\CreateUser;

class NotificationWidgetControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateNotification;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RolesTableSeeder::class);
        $this->seed(NotificationPermissionsTableSeeder::class);
    }

    /**
     * Test advertiser can view notifications widget.
     */
    public function testAdvertiserNotificationView(): void
    {
        $advertiser = $this->createTestAdvertiser();

        $count = 5;
        $this->createTestNotifications(['notifiable_id' => $advertiser->id], $count);

        $this->actingAs($advertiser);
        $response = $this->getJson(route('api.notifications.widget'));
        $response->assertOk();

        $this->assertIsArray($response->json('data.reminders'));

        $this->checkNotoficationsItems($response, $count);

        // another advertiser can not see those notifications
        $advertiser2 = $this->createTestAdvertiser();

        $this->actingAs($advertiser2);
        $response = $this->getJson(route('api.notifications.widget'));
        $response->assertOk();

        $this->assertEquals(0, $response->json('data.notifications.total'));
        $this->assertCount(0, $response->json('data.notifications.list'));
    }

    /**
     * Check notification items from response.
     * @param TestResponse $response
     * @param int $numberOfCreatedNotifications
     */
    private function checkNotoficationsItems(TestResponse $response, int $numberOfCreatedNotifications)
    {
        $this->assertEquals($numberOfCreatedNotifications, $response->json('data.notifications.total'));
        $this->assertCount(GetNotificationWidgetDataAction::NOTIFICATIONS_LIMIT, $response->json('data.notifications.list'));
        $this->assertCount(6, $response->json('data.notifications.list.0'));
        $this->assertIsString($response->json('data.notifications.list.0.id'));
        $this->assertNotEmpty($response->json('data.notifications.list.0.category'));
        $this->assertNotEmpty($response->json('data.notifications.list.0.content'));
        $this->assertIsString($response->json('data.notifications.list.0.content_preview'));
        $this->assertIsBool($response->json('data.notifications.list.0.is_read'));
        $this->assertNotEmpty($response->json('data.notifications.list.0.created_at'));
    }

    /**
     * Test admin can not view notifications widget.
     */
    public function testAdminViewNotification(): void
    {
        $admin = $this->createTestAdmin();
        $this->createTestNotifications();

        $this->actingAs($admin, 'admin');
        $response = $this->getJson(route('api.notifications.widget'));
        $response->assertForbidden();
    }
}
