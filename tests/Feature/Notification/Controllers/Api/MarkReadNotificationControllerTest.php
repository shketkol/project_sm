<?php

namespace Tests\Feature\Notification\Controllers\Api;

use Modules\Notification\Models\Notification;
use Modules\Notification\Models\Traits\CreateNotification;
use Modules\Notification\Database\Seeders\NotificationPermissionsTableSeeder;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Models\Traits\CreateUser;

class MarkReadNotificationControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateNotification;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RolesTableSeeder::class);
        $this->seed(NotificationPermissionsTableSeeder::class);
    }

    /**
     * Test advertiser can mark read notification.
     */
    public function testAdvertiserNotificationMarkRead(): void
    {
        $advertiser = $this->createTestAdvertiser();

        /** @var Notification $notification */
        $notification = $this->createTestNotification([
            'notifiable_id' => $advertiser->id,
            'read_at'       => null,
        ]);

        $this->actingAs($advertiser);
        $response = $this->patchJson(route('api.notifications.read', ['notification' => $notification->id]));
        $response->assertOk();

        $this->assertEquals(__('messages.success'), $response->json('data.message'));

        // another advertiser can not see those notifications
        $advertiser2 = $this->createTestAdvertiser();

        $this->actingAs($advertiser2);
        $response = $this->patchJson(route('api.notifications.read', ['notification' => $notification->id]));
        $response->assertForbidden();

        // admin can not see those notifications
        $admin = $this->createTestAdmin();

        $this->actingAs($admin, 'admin');
        $response = $this->patchJson(route('api.notifications.read', ['notification' => $notification->id]));
        $response->assertForbidden();
    }

    /**
     * Test advertiser can mark read not found notification.
     */
    public function testAdvertiserNotificationMarkReadNotFound(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $notExistedId = 99999999;

        $this->actingAs($advertiser);
        $response = $this->patchJson(route('api.notifications.read', ['notification' => $notExistedId]));
        $response->assertNotFound();
    }
}
