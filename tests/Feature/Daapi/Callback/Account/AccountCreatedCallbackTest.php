<?php

namespace Tests\Feature\Daapi\Callback\Account;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\URL;
use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Models\Traits\CreateInvite;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\UserStatus;
use Modules\User\Notifications\Advertiser\Approved;
use Modules\User\Notifications\Advertiser\Rejected;
use Modules\User\Repositories\UserRepository;
use Tests\TestCase;

class AccountCreatedCallbackTest extends TestCase
{
    use DatabaseTransactions, CreateUser, CreateInvite;

    private const ACCOUNT_EXTERNAL_ID = '8111111j-888y-000t-afaf-8111111j';
    private const EXTERNAL_ID = '8000000j-888y-000t-afaf-8000000j';

    /**
     * Test with correct body.
     *
     * @dataProvider accountCreatedSuccessDataProvider
     *
     * @param array $expectedData
     */
    public function testAccountCreatedCallbackSuccess(array $expectedData): void
    {
        Notification::fake();

        $this->createTestInviteCode([
            'email'     => 'charlie.sheen-test@advertiser.com',
            'status_id' => InvitationStatus::ID_PROCESSING
        ]);

        $payload = $this->preparePayload($expectedData['statusEnum']);
        $response = $this->json(Request::METHOD_POST, Url::signedRoute('callbacks.account.create'), $payload);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('users', [
            'status_id'           => $expectedData['statusId'],
            'external_id'         => $expectedData['externalId'],
            'account_external_id' => $expectedData['accountExternalId'],
        ]);
        $repository = app(UserRepository::class);
        $user = $repository->findByField('external_id', $expectedData['externalId'])->first();
        Notification::assertSentTo(
            $user,
            $expectedData['notification']
        );
    }

    private function preparePayload(string $statusEnum): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "c18d868c-98c2-4578-9c29-6004cfa5541d",
                    "system"       => "danads-api",
                    "haapiVersion" => "1",
                    "type"         => "account/signup",
                    "preSharedKey" => "f2e64f90-624b-4db5-aecd-cf23b3772822",
                    "userToken"    => null,
                    "callbackUrl"  => "http://dummy.url",
                    "payload"      => [
                        "email"          => "charlie.sheen-test@advertiser.com",
                        "firstName"      => "Charlie",
                        "lastName"       => "Sheen",
                        "companyName"    => "Charlie Test MD",
                        "companyAddress" => [
                            "line1"   => "1720 Test Avenue",
                            "line2"   => "",
                            "state"   => "AZ",
                            "city"    => "Test City",
                            "country" => "US",
                            "zipcode" => "90404",
                        ],
                        "phoneNumber"    => "+1 (928) 453-1800",
                    ],
                ],
                "response" => [
                    "id"         => "c621150c-0df3-4b58-ae00-924bb364b34d",
                    "system"     => "haapi",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "payload"    => [
                        "user"    => [
                            "id"           => self::EXTERNAL_ID,
                            "email"        => "charlie.sheen-test@advertiser.com",
                            "firstName"    => "Charlie",
                            "lastName"     => "Sheen",
                            "companyName"  => "Charlie Sheen MD",
                            "userStatus"   => "ACTIVE",
                            "userType"     => "NORMAL",
                            "oneTimeToken" => "aaaaa0000-e714-41e8-9f6f-aaaaa0000666",
                        ],
                        "account" => [
                            "id"             => self::ACCOUNT_EXTERNAL_ID,
                            "companyName"    => "Charlie Test MD",
                            "accountType"    => "ADVERTISER",
                            "phoneNumber"    => "+1 (928) 453-1800",
                            "companyAddress" => [
                                "line1"   => "1720 Test Avenue",
                                "line2"   => "",
                                "state"   => "AZ",
                                "city"    => "Test City",
                                "country" => "US",
                                "zipcode" => "90404",
                            ],
                            "accountStatus"  => $statusEnum,
                        ],
                    ],
                    "received"   => "2019-07-22T10:46:53Z",
                    "duration"   => 74451,
                ],
            ],
        ];
    }

    /**
     * @return \Generator
     */
    public function accountCreatedSuccessDataProvider(): \Generator
    {
        yield [
            [
                'externalId'        => self::EXTERNAL_ID,
                'accountExternalId' => self::ACCOUNT_EXTERNAL_ID,
                'statusId'          => UserStatus::ID_ACTIVE,
                'statusEnum'        => 'ACTIVE',
                'notification'      => Approved::class,
            ],
        ];
        yield [
            [
                'externalId'        => self::EXTERNAL_ID,
                'accountExternalId' => self::ACCOUNT_EXTERNAL_ID,
                'statusId'          => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'statusEnum'        => 'PENDING_MANUAL_REVIEW',
                'notification'      => Rejected::class,
            ],
        ];
    }
}
