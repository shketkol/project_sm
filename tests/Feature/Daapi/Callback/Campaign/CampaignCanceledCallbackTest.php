<?php

namespace Tests\Feature\Daapi\Callback\Campaign;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\URL;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AdminSignInMock;
use Tests\Traits\HaapiMocks\AdminUserGetMock;

class CampaignCanceledCallbackTest extends TestCase
{
    use DatabaseTransactions,
        AdminSignInMock,
        AdminUserGetMock,
        CreateUser,
        CreateCampaign;

    private const EXT_CAMPAIGN_ID = 'u66666662-y333y-y5555y-i6666666';
    private const EXT_ADV_ID      = 'u66666662-y666y-y5555y-i6666666';

    /**
     * Test with correct body.
     *
     * @dataProvider campaignCanceledSuccessDataProvider
     *
     * @param int $statusFrom
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function testCampaignCreatedCallbackSuccess(int $statusFrom): void
    {
        Notification::fake();
        $user = $this->createTestUser([
            'external_id' => self::EXT_ADV_ID,
        ]);
        $campaign = $this->createTestCampaign([
            'user_id'           => $user->id,
            'external_id'       => self::EXT_CAMPAIGN_ID,
            'status_id'         => $statusFrom,
            'status_changed_by' => $user->id,
        ]);
        $payload = $this->preparePayload();
        $this->mockAdminSignInActionSuccess();

        $admin = $this->createTechnicalAdmin();

        $this->mockAdminUserGetActionSuccess(self::EXT_ADV_ID, $admin->id);
        $response = $this->json(Request::METHOD_POST, Url::signedRoute('callbacks.campaign.cancel'), $payload);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->getKey(),
            'status_id' => CampaignStatus::ID_CANCELED,
        ]);
    }

    /**
     * @return array
     */
    private function preparePayload(): array
    {
        return
            [
                "haapi" => [
                    "request"  => [
                        "id"           => "c18d868c-98c2-4578-9c29-6004cfa5541d",
                        "system"       => "danads-api",
                        "haapiVersion" => "1",
                        "type"         => "campaign/cancel",
                        "preSharedKey" => "f2e64f90-624b-4db5-aecd-cf23b3772822",
                        "userToken"    => null,
                        "callbackUrl"  => "http://dummy.url",
                        "payload"      => [
                            "campaignId" => self::EXT_CAMPAIGN_ID,
                        ],
                    ],
                    "response" => [
                        "id"         => "c621150c-0df3-4b58-ae00-924bb364b34d",
                        "system"     => "haapi",
                        "statusCode" => "0",
                        "statusMsg"  => "OK",
                        "payload"    => [
                            "campaignId" => self::EXT_CAMPAIGN_ID,
                        ],
                        "received"   => "2019-07-22T10:46:53Z",
                        "duration"   => 74451,
                    ],
                ],
            ];
    }

    /**
     * @return \Generator
     */
    public function campaignCanceledSuccessDataProvider(): \Generator
    {
        yield [
            CampaignStatus::ID_PROCESSING,
        ];
        yield [
            CampaignStatus::ID_PENDING_APPROVAL,
        ];
        yield [
            CampaignStatus::ID_READY,
        ];
        yield [
            CampaignStatus::ID_LIVE,
        ];
        yield [
            CampaignStatus::ID_PAUSED,
        ];
    }
}
