<?php

namespace Tests\Feature\Daapi\HttpClient;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Arr;
use Modules\Daapi\HttpClient\Builders\Contracts\RequestBuilder;
use Modules\Daapi\HttpClient\Builders\Contracts\ResponseBuilder;
use Tests\TestCase;

class DaapiRequestTest extends TestCase
{
    /**
     * @var RequestBuilder
     */
    protected $daapiRequestBuilder;

    /**
     * @var ResponseBuilder
     */
    protected $daapiResponseBuilder;

    /**
     * Set up environment
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->daapiRequestBuilder = app(RequestBuilder::class);
        $this->daapiResponseBuilder = app(ResponseBuilder::class);
    }

    /**
     * @dataProvider daapiRequestDataProvider
     *
     * @param array $data
     * @throws BindingResolutionException
     */
    public function testDaapiRequestBuilder(array $data)
    {
        $daapiRequest = $this->daapiRequestBuilder->make($data);
        $this->assertEquals($data, $daapiRequest->getBody());
        $this->assertEquals('value', $daapiRequest->get('key'));
        $this->assertEquals('f63215f9-cd8f-4f93-bc46-10be97ec9f95', $daapiRequest->getId());
    }

    /**
     * @dataProvider daapiResponseDataProvider
     *
     * @param array $data
     * @throws BindingResolutionException
     */
    public function testDaapiResponseBuilder(array $data)
    {
        $daapiRequest = $this->daapiRequestBuilder->make($data);
        $payload = ['key2' => 'value2'];
        $uuid = '551d63d0-f3fa-4f40-b046-5e7a89858abe';
        $daapiResponse = $this->daapiResponseBuilder->make($daapiRequest, 200, $payload);
        $daapiResponseBody = $daapiResponse->toArray();
        Arr::set($daapiResponseBody, 'response.id', $uuid);
        $this->assertEquals($data, $daapiResponseBody);
        $this->assertEquals(200, $daapiResponse->getStatusCode());
        $this->assertEquals('value2', $daapiResponse->get('key2'));
        $this->assertNotEquals('value2', $daapiResponse->get('key'));
    }

    /**
     * Data provider for DapiRequest
     *
     * @return array
     */
    public static function daapiRequestDataProvider()
    {
        return [
            [
                [
                    'request' => [
                        'id' => 'f63215f9-cd8f-4f93-bc46-10be97ec9f95',
                        'payload' => [
                            'key' => 'value'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Data provider for DaapiResponse
     */
    public static function daapiResponseDataProvider()
    {
        return [
            [
                [
                    'request' => [
                        'id' => 'f63215f9-cd8f-4f93-bc46-10be97ec9f95',
                        'payload' => [
                            'key' => 'value'
                        ]
                    ],
                    'response' => [
                        'id' => '551d63d0-f3fa-4f40-b046-5e7a89858abe',
                        'statusCode' => 200,
                        'payload' => [
                            'key2' => 'value2'
                        ]
                    ]
                ]
            ]
        ];
    }
}
