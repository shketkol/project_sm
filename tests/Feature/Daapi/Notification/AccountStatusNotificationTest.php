<?php

namespace Tests\Feature\Daapi\Notification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\UserStatus;

class AccountStatusNotificationTest extends StatusNotificationTest
{
    use DatabaseTransactions,
        CreateUser;

    /**
     * @param array $attributes
     *
     * @return Model|\Modules\User\Models\User
     */
    public function createModel(array $attributes): Model
    {
        return $this->createTestAdvertiser($attributes);
    }

    /**
     * @param string $externalId
     * @param string $status
     *
     * @return array
     */
    public function createRequest(string $externalId, string $status): array
    {
        return [
            'request' => [
                'id'      => '8ce65803-f87b-45b5-b7dd-77857a95dfa8',
                'payload' => [
                    'accounts' => [
                        [
                            'id'     => $externalId,
                            'status' => $status,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForInProgress
     *
     * @param array $data
     */
    public function testNotificationAcceptedForInProgress(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return \Generator
     */
    public function statusDataProviderForInProgress(): \Generator
    {
        yield [
            [
                'from_status_id'  => UserStatus::ID_CREATE_IN_PROGRESS,
                'to_status'       => 'CREATE IN PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_CREATE_IN_PROGRESS,
                'to_status'       => 'CREATE_IN_PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_CREATE_IN_PROGRESS,
                'to_status'       => 'PENDING MANUAL REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_CREATE_IN_PROGRESS,
                'to_status'       => 'PENDING_MANUAL_REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_CREATE_IN_PROGRESS,
                'to_status'       => 'ACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_CREATE_IN_PROGRESS,
                'to_status'       => 'INACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_CREATE_IN_PROGRESS,
                'to_status'       => 'INACTIVE UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_CREATE_IN_PROGRESS,
                'to_status'       => 'INACTIVE_UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForPending
     *
     * @param array $data
     */
    public function testNotificationAcceptedForPending(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return \Generator
     */
    public function statusDataProviderForPending(): \Generator
    {
        yield [
            [
                'from_status_id'  => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'to_status'       => 'CREATE IN PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'to_status'       => 'CREATE_IN_PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'to_status'       => 'PENDING MANUAL REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'to_status'       => 'PENDING_MANUAL_REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'to_status'       => 'ACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'to_status'       => 'INACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'to_status'       => 'INACTIVE UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_PENDING_MANUAL_REVIEW,
                'to_status'       => 'INACTIVE_UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForActive
     *
     * @param array $data
     */
    public function testNotificationAcceptedForActive(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return \Generator
     */
    public function statusDataProviderForActive(): \Generator
    {
        yield [
            [
                'from_status_id'  => UserStatus::ID_ACTIVE,
                'to_status'       => 'CREATE IN PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_ACTIVE,
                'to_status'       => 'CREATE_IN_PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_ACTIVE,
                'to_status'       => 'PENDING MANUAL REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_ACTIVE,
                'to_status'       => 'PENDING_MANUAL_REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_ACTIVE,
                'to_status'       => 'ACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_ACTIVE,
                'to_status'       => 'INACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_ACTIVE,
                'to_status'       => 'INACTIVE UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_ACTIVE,
                'to_status'       => 'INACTIVE_UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForInactive
     *
     * @param array $data
     */
    public function testNotificationAcceptedForInactive(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return \Generator
     */
    public function statusDataProviderForInactive(): \Generator
    {
        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE,
                'to_status'       => 'CREATE IN PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE,
                'to_status'       => 'CREATE_IN_PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE,
                'to_status'       => 'PENDING MANUAL REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE,
                'to_status'       => 'PENDING_MANUAL_REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE,
                'to_status'       => 'ACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE,
                'to_status'       => 'INACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE,
                'to_status'       => 'INACTIVE UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE,
                'to_status'       => 'INACTIVE_UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForInactiveUnpaid
     *
     * @param array $data
     */
    public function testNotificationAcceptedForInactiveUnpaid(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return \Generator
     */
    public function statusDataProviderForInactiveUnpaid(): \Generator
    {
        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE_UNPAID,
                'to_status'       => 'CREATE IN PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE_UNPAID,
                'to_status'       => 'CREATE_IN_PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE_UNPAID,
                'to_status'       => 'PENDING MANUAL REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE_UNPAID,
                'to_status'       => 'PENDING_MANUAL_REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE_UNPAID,
                'to_status'       => 'ACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE_UNPAID,
                'to_status'       => 'INACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE_UNPAID,
                'to_status'       => 'INACTIVE UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INACTIVE_UNPAID,
                'to_status'       => 'INACTIVE_UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForIncomplete
     *
     * @param array $data
     */
    public function testNotificationAcceptedForIncomplete(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return \Generator
     */
    public function statusDataProviderForIncomplete(): \Generator
    {
        yield [
            [
                'from_status_id'  => UserStatus::ID_INCOMPLETE,
                'to_status'       => 'CREATE IN PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INCOMPLETE,
                'to_status'       => 'CREATE_IN_PROGRESS',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INCOMPLETE,
                'to_status'       => 'PENDING MANUAL REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INCOMPLETE,
                'to_status'       => 'PENDING_MANUAL_REVIEW',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INCOMPLETE,
                'to_status'       => 'ACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INCOMPLETE,
                'to_status'       => 'INACTIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INCOMPLETE,
                'to_status'       => 'INACTIVE UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => UserStatus::ID_INCOMPLETE,
                'to_status'       => 'INACTIVE_UNPAID',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];
    }
}
