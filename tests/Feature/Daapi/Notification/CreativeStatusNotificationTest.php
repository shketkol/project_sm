<?php

namespace Tests\Feature\Daapi\Notification;

use Generator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Models\Traits\CreateCreative;
use Tests\Traits\HaapiMocks\CreativeGetMock;

class CreativeStatusNotificationTest extends StatusNotificationTest
{
    use DatabaseTransactions,
        CreateCreative,
        CreativeGetMock,
        CreateCampaign;

    /**
     * @param array $attributes
     *
     * @return Model|\Modules\Creative\Models\Creative
     */
    public function createModel(array $attributes): Model
    {
        $creative = $this->createTestCreative($attributes);
        $this->mockCreativeGetActionSuccess($creative);
        $campaign = $this->createTestCampaign();
        $campaign->creatives()->save($creative);

        return $creative;
    }

    /**
     * @param string $externalId
     * @param string $status
     *
     * @return array
     */
    public function createRequest(string $externalId, string $status): array
    {
        return [
            'request' => [
                'id'      => '8ce65803-f87b-45b5-b7dd-77857a95dfb0',
                'payload' => [
                    'creatives' => [
                        [
                            'id'       => $externalId,
                            'status'   => $status,
                            'reason'   => 'test reason',
                            'comments' => '',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForDraft
     *
     * @param array $data
     */
    public function testNotificationAcceptedForDraft(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForDraft(): Generator
    {
        yield [
            [
                'from_status_id'  => CreativeStatus::ID_DRAFT,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_DRAFT,
                'to_status'       => 'SUBMITTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_DRAFT,
                'to_status'       => 'PROCESSING',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_DRAFT,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_DRAFT,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_DRAFT,
                'to_status'       => 'APPROVED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_DRAFT,
                'to_status'       => 'REJECTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForPendingApproval
     *
     * @param array $data
     */
    public function testNotificationAcceptedForPendingApproval(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForPendingApproval(): Generator
    {
        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'SUBMITTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'PROCESSING',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'APPROVED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'REJECTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForApproved
     *
     * @param array $data
     */
    public function testNotificationAcceptedForApproved(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForApproved(): Generator
    {
        yield [
            [
                'from_status_id'  => CreativeStatus::ID_APPROVED,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_APPROVED,
                'to_status'       => 'SUBMITTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_APPROVED,
                'to_status'       => 'PROCESSING',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_APPROVED,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_APPROVED,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_APPROVED,
                'to_status'       => 'APPROVED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_APPROVED,
                'to_status'       => 'REJECTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForRejected
     *
     * @param array $data
     */
    public function testNotificationAcceptedForRejected(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForRejected(): Generator
    {
        yield [
            [
                'from_status_id'  => CreativeStatus::ID_REJECTED,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_REJECTED,
                'to_status'       => 'SUBMITTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_REJECTED,
                'to_status'       => 'PROCESSING',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_REJECTED,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_REJECTED,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_REJECTED,
                'to_status'       => 'APPROVED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_REJECTED,
                'to_status'       => 'REJECTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForProcessing
     *
     * @param array $data
     */
    public function testNotificationAcceptedForProcessing(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForProcessing(): Generator
    {
        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PROCESSING,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PROCESSING,
                'to_status'       => 'SUBMITTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PROCESSING,
                'to_status'       => 'PROCESSING',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PROCESSING,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PROCESSING,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PROCESSING,
                'to_status'       => 'APPROVED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CreativeStatus::ID_PROCESSING,
                'to_status'       => 'REJECTED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }
}
