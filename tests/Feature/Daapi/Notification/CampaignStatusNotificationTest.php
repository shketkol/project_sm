<?php

namespace Tests\Feature\Daapi\Notification;

use Generator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\User\Models\Traits\CreateUser;
use Tests\Traits\HaapiMocks\AdminSignInMock;
use Tests\Traits\HaapiMocks\AdminUserGetMock;

class CampaignStatusNotificationTest extends StatusNotificationTest
{
    use DatabaseTransactions,
        CreateCampaign,
        CreateUser,
        AdminSignInMock,
        AdminUserGetMock;

    /**
     * @param array $attributes
     *
     * @return Model|\Modules\Campaign\Models\Campaign
     */
    public function createModel(array $attributes): Model
    {
        return $this->createTestCampaign($attributes);
    }

    /**
     * @param $model
     */
    protected function mockUserHaapi($model): void
    {
        $admin = $this->createTechnicalAdmin();

        $this->mockAdminSignInActionSuccess();
        $this->mockAdminUserGetActionSuccess($model->user->external_id, $admin->id);
    }

    /**
     * @param string $externalId
     * @param string $status
     *
     * @return array
     */
    public function createRequest(string $externalId, string $status): array
    {
        return [
            'request' => [
                'id'      => '8ce65803-f87b-45b5-b7dd-77857a95dfa7',
                'payload' => [
                    'campaigns' => [
                        [
                            'id'     => $externalId,
                            'status' => $status,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForDraft
     *
     * @param array $data
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function testNotificationAcceptedForDraft(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForDraft(): Generator
    {
        yield [
            [
                'from_status_id'  => CampaignStatus::ID_DRAFT,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_DRAFT,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_DRAFT,
                'to_status'       => 'READY',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_DRAFT,
                'to_status'       => 'LIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_DRAFT,
                'to_status'       => 'PAUSED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_DRAFT,
                'to_status'       => 'COMPLETED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_DRAFT,
                'to_status'       => 'CANCELLED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForPendingApproval
     *
     * @param array $data
     */
    public function testNotificationAcceptedForPendingApproval(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForPendingApproval(): Generator
    {
        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'READY',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'LIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'PAUSED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'COMPLETED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PENDING_APPROVAL,
                'to_status'       => 'CANCELLED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForReady
     *
     * @param array $data
     */
    public function testNotificationAcceptedForReady(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForReady(): Generator
    {

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_READY,
                'to_status'       => 'READY',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_READY,
                'to_status'       => 'LIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_READY,
                'to_status'       => 'PAUSED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_READY,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_READY,
                'to_status'       => 'COMPLETED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_READY,
                'to_status'       => 'CANCELLED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForLive
     *
     * @param array $data
     */
    public function testNotificationAcceptedForLive(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForLive(): Generator
    {
        yield [
            [
                'from_status_id'  => CampaignStatus::ID_LIVE,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_LIVE,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_LIVE,
                'to_status'       => 'READY',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_LIVE,
                'to_status'       => 'LIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_LIVE,
                'to_status'       => 'PAUSED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_LIVE,
                'to_status'       => 'COMPLETED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_LIVE,
                'to_status'       => 'CANCELLED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForPaused
     *
     * @param array $data
     */
    public function testNotificationAcceptedForPaused(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForPaused(): Generator
    {
        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PAUSED,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PAUSED,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PAUSED,
                'to_status'       => 'READY',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PAUSED,
                'to_status'       => 'LIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PAUSED,
                'to_status'       => 'PAUSED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PAUSED,
                'to_status'       => 'COMPLETED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PAUSED,
                'to_status'       => 'CANCELLED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForCompleted
     *
     * @param array $data
     */
    public function testNotificationAcceptedForCompleted(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForCompleted(): Generator
    {
        yield [
            [
                'from_status_id'  => CampaignStatus::ID_COMPLETED,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_COMPLETED,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_COMPLETED,
                'to_status'       => 'READY',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_COMPLETED,
                'to_status'       => 'LIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_COMPLETED,
                'to_status'       => 'PAUSED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_COMPLETED,
                'to_status'       => 'COMPLETED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_COMPLETED,
                'to_status'       => 'CANCELLED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForCanceled
     *
     * @param array $data
     */
    public function testNotificationAcceptedForCanceled(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForCanceled(): Generator
    {
        yield [
            [
                'from_status_id'  => CampaignStatus::ID_CANCELED,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_CANCELED,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_CANCELED,
                'to_status'       => 'READY',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_CANCELED,
                'to_status'       => 'LIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_CANCELED,
                'to_status'       => 'PAUSED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_CANCELED,
                'to_status'       => 'COMPLETED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_CANCELED,
                'to_status'       => 'CANCELLED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider statusDataProviderForProcessing
     *
     * @param array $data
     */
    public function testNotificationAcceptedForProcessing(array $data): void
    {
        $this->check($data);
    }

    /**
     * @return Generator
     */
    public function statusDataProviderForProcessing(): Generator
    {
        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PROCESSING,
                'to_status'       => 'PENDING APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PROCESSING,
                'to_status'       => 'PENDING_APPROVAL',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PROCESSING,
                'to_status'       => 'READY',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PROCESSING,
                'to_status'       => 'LIVE',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PROCESSING,
                'to_status'       => 'PAUSED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PROCESSING,
                'to_status'       => 'COMPLETED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'payload'    => [
                        'field'   => 'reason',
                        'message' => 'Can not apply transition',
                    ],
                ],
            ],
        ];

        yield [
            [
                'from_status_id'  => CampaignStatus::ID_PROCESSING,
                'to_status'       => 'CANCELLED',
                'expected_result' => [
                    'statusCode' => Response::HTTP_OK,
                    'payload'    => [
                        'field'   => 'message',
                        'message' => 'The notification is successfully accepted',
                    ],
                ],
            ],
        ];
    }
}
