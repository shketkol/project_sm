<?php

namespace Tests\Traits\Cognito;

use App\Helpers\Json;
use Carbon\Carbon;

trait CognitoTokensHelper
{
    /**
     * @return array
     */
    protected function makeCognitoTokens()
    {
        return [
            'token' => $this->makeCognitoAccessToken(),
            'refreshToken' => $this->makeCognitoRefreshToken(),
        ];
    }

    /**
     * @return string
     */
    protected function makeCognitoAccessToken()
    {
        $payload = $this->makeCognitoTokenPayload();
        return "ffff.{$payload}.ffff";
    }

    /**
     * @return string
     */
    protected function makeCognitoRefreshToken()
    {
        return 'Dummy.Refresh.Token';
    }

    /**
     * @return string
     */
    private function makeCognitoTokenPayload()
    {
        $payload = [
            'exp' => time() + Carbon::SECONDS_PER_MINUTE * Carbon::MINUTES_PER_HOUR, // Current time + 1 hour (in seconds)
        ];
        $payload = Json::encode($payload);
        return base64_encode($payload);
    }
}
