<?php

namespace Tests\Traits\Cognito;

use Modules\Auth\Services\AuthService\Actions\ValidateCognitoJwtAction;

trait CognitoJwtValidationMock
{
    public function mockJwtValidation(): void
    {
        $this->mock(ValidateCognitoJwtAction::class, function ($mock) {
            $mock->shouldReceive('handle')->andReturn(true);
        });
    }
}
