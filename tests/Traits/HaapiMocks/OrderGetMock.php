<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Payment\OrderGet;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

trait OrderGetMock
{
    /**
     * @param Campaign $campaign
     */
    private function mockOrderGetActionSuccess(Campaign $campaign)
    {
        $mock = Mockery::mock(OrderGet::class, function ($mock) use ($campaign) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getOrderGetResponse($campaign));
        });

        $this->app->instance(OrderGet::class, $mock);
    }

    /**
     * @param Campaign $campaign
     * @return HaapiResponse
     */
    private function getOrderGetResponse(Campaign $campaign): HaapiResponse
    {
        $response = new HaapiResponse();

        $data = $this->getOrderGetSuccessData($campaign);

        $body = Arr::get($data, 'haapi.response');
        $statusCode = Arr::get($body, 'statusCode');
        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $response->setBody($body);
        $response->setPayload(Arr::get($body, 'payload'));
        $response->setId($responseId);
        $response->setStatusCode($statusCode);
        $response->setStatusMsg($statusMsg);
        $response->setDuration(Arr::get($body, 'duration'));
        $response->setReceived(Arr::get($body, 'received'));

        return $response;
    }

    /**
     * @param Campaign $campaign
     * @return array
     */
    private function getOrderGetSuccessData(Campaign $campaign): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/signin",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "accountId" => $campaign->user->account_external_id,
                        "orderId"   => $campaign->order_id,
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "order" => [
                            "orderId"         => $campaign->order_id,
                            "orderStatus"     => "CURRENT",
                            "campaign"        => [
                                "amount"               => 10000.0,
                                "deliveredImpressions" => 0,
                                "bookedImpressions"    => 312500,
                                "costMethod"           => "CPM",
                                "advertiserName"       => "Snacks",
                                "startDate"            => "2020-01-26",
                                "orderDate"            => "2019-12-26",
                                "endDate"              => "2020-01-31",
                                "unitCost"             => 32.0,
                                "campaignName"         => "Test"
                            ],
                            "invoices"        => [
                                [
                                    "invoiceId"      => 100002,
                                    "paymentHistory" => [
                                        [
                                            "transactionDate"    => "2019-12-27",
                                            "billingPeriodStart" => "2019-12-25",
                                            "billingPeriodEnd"   => "2019-12-31",
                                            "transactionId"      => "12",
                                            "amount"             => 45.2,
                                            "status"             => "SUCCESS"
                                        ]
                                    ]
                                ],
                                [
                                    "invoiceId"      => 13,
                                    "paymentHistory" => []
                                ]
                            ],
                            "totalPaidAmount" => 45.2,
                            "billingMessage"  => "Your credit Card was charged on 12/27/2019"
                        ]
                    ],
                ],
            ],
        ];
    }
}
