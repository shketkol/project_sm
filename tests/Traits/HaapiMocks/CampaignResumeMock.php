<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Haapi\Actions\Campaign\CampaignResume;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

trait CampaignResumeMock
{
    protected static $externalId = 'asd-fkjh-13-dfdf-2323-dsdfd';

    /**
     * @param string $campaignExternalId
     * @param int    $userId
     */
    private function mockCampaignResumeActionSuccess(string $campaignExternalId, int $userId)
    {
        $mock = Mockery::mock(CampaignResume::class, function ($mock) use ($campaignExternalId, $userId) {
            $mock
                ->shouldReceive('handle')
                ->with($campaignExternalId, $userId)
                ->andReturn($this->getCampaignResumeResponse($campaignExternalId));
        });

        $this->app->instance(CampaignResume::class, $mock);
    }

    /**
     * @param string $campaignExternalId
     *
     * @return HaapiResponse
     */
    private function getCampaignResumeResponse(string $campaignExternalId): HaapiResponse
    {
        $response = new HaapiResponse();

        $data = $this->getCampaignResumeSuccessData($campaignExternalId);

        $body = Arr::get($data, 'haapi.response');
        $statusCode = Arr::get($body, 'statusCode');
        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $response->setBody($body);
        $response->setPayload(Arr::get($body, 'payload'));
        $response->setId($responseId);
        $response->setStatusCode($statusCode);
        $response->setStatusMsg($statusMsg);
        $response->setDuration(Arr::get($body, 'duration'));
        $response->setReceived(Arr::get($body, 'received'));

        return $response;
    }

    /**
     * @param string $campaignExternalId
     *
     * @return array
     */
    private function getCampaignResumeSuccessData(string $campaignExternalId): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "d6061648-970f-4e27-9791-7dae5f06ec66",
                    "system"       => "danads-api",
                    "haapiVersion" => "1",
                    "type"         => "campaign/pause",
                    "preSharedKey" => "f2e64f90-624b-4db5-aecd-cf23b3772822",
                    "userToken"    => "asdf-asdf-1234-qwer-5678",
                    "callbackUrl"  => null,
                    "payload"      => [
                        "campaignId" => $campaignExternalId,
                    ],
                ],
                "response" => [
                    "id"         => "3bd967da-eb48-44ea-8abc-8211643d1fef",
                    "system"     => "haapi",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "payload"    => [
                        "message" => "Campaign is resumed.",
                    ],
                    "received"   => "2019-07-23T09:37:54Z",
                    "duration"   => 359,
                ],
            ],
        ];
    }
}
