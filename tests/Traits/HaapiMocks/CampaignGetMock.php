<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Campaign\Models\Campaign;
use Modules\Daapi\DataTransferObjects\Types\CampaignData;
use Modules\Haapi\Actions\Campaign\CampaignGet;

trait CampaignGetMock
{
    /**
     * @param Campaign $campaign
     * @param string   $status
     */
    private function mockCampaignGetActionSuccess(Campaign $campaign, string $status = "PENDING APPROVAL")
    {
        $mock = Mockery::mock(CampaignGet::class, function ($mock) use ($campaign, $status) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getCampaignGetResponse($campaign, $status));
        });

        $this->app->instance(CampaignGet::class, $mock);
    }

    /**
     * @param Campaign $campaign
     * @param string   $status
     *
     * @return CampaignData
     */
    private function getCampaignGetResponse(Campaign $campaign, string $status): CampaignData
    {
        $data = $this->getCampaignGetSuccessData($campaign, $status);

        return new CampaignData(
            Arr::get($data, 'haapi.response.payload', [])
        );
    }

    /**
     * @param Campaign $campaign
     * @param string   $status
     *
     * @return array
     */
    private function getCampaignGetSuccessData(Campaign $campaign, string $status): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/signin",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "campaignId" => $campaign->external_id,
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "id"               => $campaign->external_id,
                        "operativeOrderId" => "229436",
                        "sourceId"         => "campaign-20191001",
                        "name"             => "Pancake Specials August 2019",
                        "advertiserId"     => "df41a803-bc5a-4c39-9f28-7921b2da50ca",
                        "agencyId"         => "df41a803-bc5a-4c39-9f28-7921b2da50ca",
                        "status"           => $status,
                        "lineItems"        => [
                            [
                                "id"           => "bac8c4b5-fab3-4d9b-bafc-a5e913142063",
                                "startDate"    => "2019-07-29T09:30:10.000+0000",
                                "endDate"      => "2019-08-30T09:30:10.000+0000",
                                "status"       => "PENDING APPROVAL",
                                "unitCost"     => 39.45,
                                "unitCostType" => "CPM",
                                "quantity"     => 105,
                                "quantityType" => "IMPRESSIONS",
                                "productId"    => "bb540ba2-b990-4c37-befd-c742bc6812d0",
                                "creative"     => null,
                                "targets"      => [],
                            ]
                        ],
                        "promoCodeName"       => null,
                    ],
                ],
            ],
        ];
    }
}
