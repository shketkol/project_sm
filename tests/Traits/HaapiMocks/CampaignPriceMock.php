<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignGetPrice;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

trait CampaignPriceMock
{
    /**
     * @param int      $userId
     */
    private function mockCampaignPriceActionSuccess(int $userId)
    {
        $mock = Mockery::mock(CampaignGetPrice::class, function ($mock) use ($userId) {
            /** @var Mockery\Mock $mock */
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getCampaignPriceResponse());
        });

        $this->app->instance(CampaignGetPrice::class, $mock);
    }

    /**
     * @return HaapiResponse
     */
    private function getCampaignPriceResponse(): HaapiResponse
    {
        $response = new HaapiResponse();

        $data = $this->getCampaignPriceData();

        $body = Arr::get($data, 'haapi.response');
        $statusCode = Arr::get($body, 'statusCode');
        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $response->setBody($body);
        $response->setPayload(Arr::get($body, 'payload'));
        $response->setId($responseId);
        $response->setStatusCode($statusCode);
        $response->setStatusMsg($statusMsg);
        $response->setDuration(Arr::get($body, 'duration'));
        $response->setReceived(Arr::get($body, 'received'));

        return $response;
    }

    /**
     * @return array[][]
     */
    private function getCampaignPriceData(): array
    {
        return [
            'haapi' => [
                "request" => [
                    "id" => "9e03f212-2ba3-4218-b507-34fe425db5a5",
                    "system" => "danads-api-local",
                    "haapiVersion" => "1",
                    "type" => "campaign/price",
                    "preSharedKey" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJkYW5hZHMiLCJpYXQiOjE2MjA4MzY4ODh9.8GqMSlucM0ENUkpZbH70EjAIr7oEwj__1XpcoF9ErJ8",
                    "userToken" => null,
                    "callbackUrl" => null,
                    "payload" => [
                        "targets" => [
                            [
                                "typeId" => "0f13387e-7bfc-11e9-b52b-9b8ffb3c3b74",
                                "values" => [
                                    [
                                        "value" => "8e8ac619-9c3e-11e9-8714-0a3b51d4007e",
                                        "exclusion" => 0
                                    ],
                                    [
                                        "value" => "8e8b9d0b-9c3e-11e9-8714-0a3b51d4007e",
                                        "exclusion" => 0
                                    ],
                                    [
                                        "value" => "8e8bd698-9c3e-11e9-8714-0a3b51d4007e",
                                        "exclusion" => 0
                                    ],
                                    [
                                        "value" => "8e8c4867-9c3e-11e9-8714-0a3b51d4007e",
                                        "exclusion" => 0
                                    ]
                                ]
                            ],
                            [
                                "typeId" => "0f133b3a-7bfc-11e9-b52b-9b8ffb3c3b74",
                                "values" => [
                                    [
                                        "value" => "906ad427-9c3e-11e9-8714-0a3b51d4007e",
                                        "exclusion" => 0
                                    ],
                                    [
                                        "value" => "90d582e5-9c3e-11e9-8714-0a3b51d4007e",
                                        "exclusion" => 0
                                    ]
                                ]
                            ],
                            [
                                "typeId" => "0f1336b2-7bfc-11e9-b52b-9b8ffb3c3b74",
                                "values" => [
                                    [
                                        "value" => "a1db90b0-9c3e-11e9-8714-0a3b51d4007e",
                                        "exclusion" => 0
                                    ]
                                ]
                            ],
                            [
                                "typeId" => "0f1335c9-7bfc-11e9-b52b-9b8ffb3c3b74",
                                "values" => [
                                    [
                                        "value" => "893a0e80-c130-11ea-a2e0-0a3b51d4007e",
                                        "exclusion" => 0
                                    ],
                                    [
                                        "value" => "893a3107-c130-11ea-a2e0-0a3b51d4007e",
                                        "exclusion" => 0
                                    ],
                                    [
                                        "value" => "893a31f4-c130-11ea-a2e0-0a3b51d4007e",
                                        "exclusion" => 0
                                    ],
                                    [
                                        "value" => "893a3465-c130-11ea-a2e0-0a3b51d4007e",
                                        "exclusion" => 0
                                    ],
                                    [
                                        "value" => "893a35c7-c130-11ea-a2e0-0a3b51d4007e",
                                        "exclusion" => 0
                                    ]
                                ]
                            ]
                        ],
                        "productId" => "bb540ba2-b990-4c37-befd-c742bc6812d0"
                    ]
                ],
                "response" => [
                    "id" => "ba583828-e675-473d-afbd-9da9ec51df10",
                    "system" => "haapi",
                    "statusCode" => "0",
                    "statusMsg" => "OK",
                    "payload" => [
                        "minDailyImpressions" => 1000,
                        "costModel" => "CPM",
                        "unitCost" => 38.72
                    ],
                    "received" => "2021-05-12T16:28:36.019+0000",
                    "duration" => 125
                ]
            ]
        ];
    }
}
