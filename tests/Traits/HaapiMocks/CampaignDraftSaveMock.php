<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Campaign\CampaignDraftSave;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Mappers\Campaign\ActionMapper;

trait CampaignDraftSaveMock
{
    /**
     * @param Campaign $campaign
     * @param int      $userId
     */
    private function mockCampaignDraftSaveActionSuccess(Campaign $campaign, int $userId)
    {
        $mock = Mockery::mock(CampaignDraftSave::class, function ($mock) use ($campaign, $userId) {
            /** @var Mockery\Mock $mock */
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getCampaignDraftSaveResponse($campaign));
        });

        $this->app->instance(CampaignDraftSave::class, $mock);
    }

    /**
     * @param Campaign $campaign
     *
     * @return HaapiResponse
     */
    private function getCampaignDraftSaveResponse(Campaign $campaign): HaapiResponse
    {
        $response = new HaapiResponse();

        $data = $this->getCampaignDraftSaveData($campaign);

        $body = Arr::get($data, 'haapi.response');
        $statusCode = Arr::get($body, 'statusCode');
        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $response->setBody($body);
        $response->setPayload(Arr::get($body, 'payload'));
        $response->setId($responseId);
        $response->setStatusCode($statusCode);
        $response->setStatusMsg($statusMsg);
        $response->setDuration(Arr::get($body, 'duration'));
        $response->setReceived(Arr::get($body, 'received'));

        return $response;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array[][]
     */
    private function getCampaignDraftSaveData(Campaign $campaign): array
    {
        return [
            'haapi' => [
                'request'  => [
                    'id'           => '9937056d-07be-4db3-b091-1b8dc0e990db',
                    'system'       => 'danads-api',
                    'haapiVersion' => '1',
                    'type'         => 'campaign/draft_save',
                    'preSharedKey' => null,
                    'userToken'    => null,
                    'callbackUrl'  => null,
                    'payload'      => [
                        'campaign' => [
                            'sourceId'               => $campaign->id,
                            'externalId'             => $campaign->external_id,
                            'name'                   => $campaign->name,
                            'advertiserId'           => $campaign->user->getExternalId(),
                            'displayBudget'          => $campaign->budget,
                            'lineItems'              => [
                                [
                                    'sourceId'           => $campaign->id,
                                    'startDate'          => $campaign->date_start->toString(),
                                    'endDate'            => $campaign->date_end->toString(),
                                    'productId'          => 'bb540ba2-b990-4c37-befd-c742bc6812d0',
                                    'targets'            => [],
                                    'unitCost'           => $campaign->cpm,
                                    'discountedUnitCost' => null,
                                    'costModel'          => 'CPM',
                                    'quantity'           => $campaign->impressions,
                                    'quantityType'       => 'IMPRESSIONS',
                                    'creative'           => null,
                                    'impressionGoal'     => $campaign->impressions,
                                    'startTime'          => $campaign->date_start->toString(),
                                    'endTime'            => $campaign->date_end->toString(),
                                    'assetId'            => null,
                                    'dayPartings'        => [],
                                    'asset'              => null,
                                ],
                            ],
                            'promoCodeName'          => optional($campaign->promocode)->value,
                            'pageLastVisited'        => ActionMapper::CAMPAIGN_DETAILS,
                            'agencyId'               => '6241702e-06e7-4abc-95d8-d3eefc7e1c91',
                            'campaignCreationAction' => ActionMapper::CAMPAIGN_DELETED,
                        ],
                    ],
                ],
                'response' => [
                    'id'         => 'd9a53da1-ff89-40b4-b9a4-506d03876ff8',
                    'system'     => 'haapi',
                    'statusCode' => '0',
                    'statusMsg'  => 'OK',
                    'payload'    => [
                        'campaign' => [
                            'id'                      => $campaign->external_id,
                            'operativeOrderId'        => '338053',
                            'sourceId'                => (string)$campaign->id,
                            'name'                    => $campaign->name,
                            'advertiserId'            => $campaign->user->getExternalId(),
                            'agencyId'                => '6241702e-06e7-4abc-95d8-d3eefc7e1c91',
                            'status'                  => $campaign->status->name,
                            'impressions'             => 0,
                            'lineItems'               => [
                                [
                                    'id'                   => $campaign->external_id,
                                    'startDate'            => $campaign->date_start->toString(),
                                    'endDate'              => $campaign->date_end->toString(),
                                    'status'               => $campaign->status->name,
                                    'unitCost'             => $campaign->cpm,
                                    'discountedUnitCost'   => null,
                                    'costModel'            => 'CPM',
                                    'quantity'             => $campaign->impressions,
                                    'quantityType'         => 'IMPRESSIONS',
                                    'deliveredImpressions' => 0,
                                    'productId'            => 'bb540ba2-b990-4c37-befd-c742bc6812d0',
                                    'creative'             => null,
                                    'targets'              => [],
                                ],
                            ],
                            'promoCode'               => optional($campaign->promocode)->value,
                            'budget'                  => $campaign->budget,
                            'discountedBudget'        => $campaign->discounted_cost,
                            'displayBudget'           => $campaign->budget,
                            'displayDiscountedBudget' => null,
                        ],
                    ],
                    'received'   => '2021-03-18T10:46:16.320+0000',
                    'duration'   => 1642,
                ],
            ],
        ];
    }
}
