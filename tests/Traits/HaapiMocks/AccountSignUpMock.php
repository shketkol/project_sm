<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Haapi\Actions\Account\AccountSignUp;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\User\DataTransferObjects\UserData;

trait AccountSignUpMock
{
    /**
     * @param array $payload
     */
    private function mockUserSignUpActionSuccess(array $payload)
    {
        $userData = $this->emulateUserData($payload);
        $mock = Mockery::mock(AccountSignUp::class, function ($mock) use ($userData) {
            $mock
                ->shouldReceive('handle')
                ->with(Mockery::on(function ($arg) use ($userData) {
                    return $userData->toArray() == $arg->toArray();
                }))
                ->andReturn($this->getSignUpResponse($userData));
        });

        $this->app->instance(AccountSignUp::class, $mock);
    }

    /**
     * @param array $payload
     *
     * @return UserData
     */
    private function emulateUserData(array $payload): UserData
    {
        $userData = UserData::fromPayload($payload);

        return $userData;
    }

    /**
     * @param UserData $userData
     *
     * @return HaapiResponse
     */
    private function getSignUpResponse(UserData $userData): HaapiResponse
    {
        $response = new HaapiResponse();

        $data = $this->getSignUpSuccessData($userData);

        $body = Arr::get($data, 'haapi.response');
        $statusCode = Arr::get($body, 'statusCode');
        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $response->setBody($body);
        $response->setPayload(Arr::get($body, 'payload'));
        $response->setId($responseId);
        $response->setStatusCode($statusCode);
        $response->setStatusMsg($statusMsg);
        $response->setDuration(Arr::get($body, 'duration'));
        $response->setReceived(Arr::get($body, 'received'));

        return $response;
    }

    /**
     * @param UserData $userData
     *
     * @return array
     */
    private function getSignUpSuccessData(UserData $userData): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/SignUp",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => $userData->toArray(),
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => $userData->toArray(),
                ],
            ],
        ];
    }
}
