<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Haapi\Actions\Payment\PaymentMethodGet;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

trait PaymentMethodGetMock
{
    /**
     * @return void
     */
    private function mockPaymentMethodGetActionSuccess()
    {
        $mock = Mockery::mock(PaymentMethodGet::class, function ($mock) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getPaymentMethodGetResponse());
        });

        $this->app->instance(PaymentMethodGet::class, $mock);
    }

    /**
     * @return array
     */
    private function getPaymentMethodGetResponse(): array
    {
        $response = new HaapiResponse();

        $data = $this->getPaymentMethodGetSuccessData();

        $body = Arr::get($data, 'haapi.response');
        $statusCode = Arr::get($body, 'statusCode');
        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $response->setBody($body);
        $response->setPayload(Arr::get($body, 'payload'));
        $response->setId($responseId);
        $response->setStatusCode($statusCode);
        $response->setStatusMsg($statusMsg);
        $response->setDuration(Arr::get($body, 'duration'));
        $response->setReceived(Arr::get($body, 'received'));

        return $response->getPayload();
    }

    /**
     * @return array
     */
    private function getPaymentMethodGetSuccessData(): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/signin",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "paumentMethodId" => self::PAYMENT_METHOD_EXTERNAL_ID,
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "card" => [
                            "brand"          => "Visa",
                            "lastFour"       => "1111",
                            "expMonth"       => "2",
                            "expYear"        => "2022",
                            "billingAddress" => [
                                "address1"    => "123rd St",
                                "address2"    => null,
                                "address3"    => null,
                                "postalCode"  => "61265",
                                "city"        => "Seline",
                                "state"       => null,
                                "countryCode" => "US"
                            ],
                            "cardHolderName" => "Tessst"
                        ]
                    ],
                ],
            ],
        ];
    }
}
