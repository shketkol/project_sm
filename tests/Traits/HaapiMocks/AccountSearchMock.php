<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Haapi\Actions\Admin\Account\AdminAccountSearch;
use Modules\Haapi\DataTransferObjects\Account\AdminAccountSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

trait AccountSearchMock
{
    /**
     * @param AdminAccountSearchParam $params
     * @param int $adminId
     *
     * @return void
     */
    private function mockAccountsSearchActionSuccess(AdminAccountSearchParam $params, int $adminId): void
    {
        $mock = Mockery::mock(AdminAccountSearch::class, function ($mock) use ($params, $adminId) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getAdminAccountSearchResponse());
        });

        $this->app->instance(AdminAccountSearch::class, $mock);
    }

    /**
     * @return HaapiResponse
     */
    private function getAdminAccountSearchResponse(): HaapiResponse
    {
        $data = $this->getAccountsSuccessData();

        $response = new HaapiResponse();

        $body = Arr::get($data, 'haapi.response');
        $statusCode = Arr::get($body, 'statusCode');
        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $response->setBody($body);
        $response->setPayload(Arr::get($body, 'payload'));
        $response->setId($responseId);
        $response->setStatusCode($statusCode);
        $response->setStatusMsg($statusMsg);
        $response->setDuration(Arr::get($body, 'duration'));
        $response->setReceived(Arr::get($body, 'received'));

        return $response;
    }

    /**
     * @return array
     */
    private function getAccountsSuccessData(): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "account/search",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "start"               => 3400,
                        "limit"               => 100,
                        "searchText"          => null,
                        "accountStatusFilter" => null,
                        "sortFieldName"       => null,
                        "sortOrder"           => null,
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "next"     => null,
                        "limit"    => 1,
                        "accounts" => [
                            [
                                "manualReviewReason" => null,
                                "createdAt"          => "2020-03-05T04:08:43.000+0000",
                                "email"              => "hulu@gmail.com",
                                "bookedValue"        => 3200,
                                "name"               => "hulutest",
                                "type"               => "ADVERTISER",
                                "phoneNumber"        => "+1-111-111-1111",
                                "id"                 => "test-uuid",
                                "status"             => "ACTIVE",
                                "campaignCount"      => 1
                            ]
                        ]
                    ],
                ],
            ],
        ];
    }
}
