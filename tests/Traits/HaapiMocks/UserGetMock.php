<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Haapi\Actions\Admin\User\AdminUserGet;
use Modules\Haapi\Actions\User\UserGet;
use Modules\User\Models\User;

trait UserGetMock
{
    /**
     * @param User $user
     */
    private function mockUserGetActionSuccess(User $user)
    {
        $mock = Mockery::mock(UserGet::class, function ($mock) use ($user) {
            $mock
                ->shouldReceive('handle')
                ->with($user->id)
                ->andReturn($this->getUserGetResponse($user));
        });

        $this->app->instance(UserGet::class, $mock);
    }

    /**
     * @param User $user
     *
     * @return UserData
     */
    private function getUserGetResponse(User $user): UserData
    {
        $data = $this->getUserGetSuccessData($user);

        return new UserData(array_merge(
            Arr::get($data, 'haapi.response.payload', []),
            ['id' => $user->id]
        ));
    }

    /**
     * @param User $user
     */
    private function mockAdminUserGetActionSuccess(User $user)
    {
        $mock = Mockery::mock(AdminUserGet::class, function ($mock) use ($user) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getUserGetResponse($user));
        });

        $this->app->instance(AdminUserGet::class, $mock);
    }

    /**
     * @param User $user
     *
     * @return array
     */
    private function getUserGetSuccessData(User $user): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/signin",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "userId" => $user->id,
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "email"      => 'test-adver@mail.com',
                        "firstName"  => 'first',
                        "lastName"   => 'last',
                        "externalId" => $user->external_id,
                        "userStatus" => "PENDING MANUAL REVIEW",
                        "userType"   => "NORMAL",
                        "account"    => [
                            "createdAt"      => date('Y-m-d h:i:s', time()),
                            "updatedAt"      => date('Y-m-d h:i:s', time()),
                            "accountType"    => "ADVERTISER",
                            "companyName"    => $user->company_name,
                            "phoneNumber"    => "1235670000",
                            "id"             => "asdf-asdf-1234-qwer-5678",
                            "companyAddress" => [
                                "line1"   => "0000 Broadway",
                                "line2"   => null,
                                "city"    => "Santa Monica",
                                "state"   => "CA",
                                "country" => "United States",
                                "zipcode" => "99999",
                            ],
                            "accountStatus" => "ACTIVE",
                        ],
                    ],
                ],
            ],
        ];
    }
}
