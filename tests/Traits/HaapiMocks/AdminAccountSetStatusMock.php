<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Haapi\Actions\Admin\Account\AdminAccountSetStatus;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

trait AdminAccountSetStatusMock
{
    protected static $accountId = 'qwer-asdf-1234-tyui';
    /**
     * @param string $accountId
     * @param string $isActive
     * @param int    $adminId
     */
    private function mockAdminAccountSetStatusActionSuccess(string $accountId, string $isActive, int $adminId)
    {
        $mock = Mockery::mock(AdminAccountSetStatus::class, function ($mock) use ($accountId, $isActive, $adminId) {
            $mock
                ->shouldReceive('handle')
                ->with($accountId, $isActive, $adminId)
                ->andReturn($this->getAccountSetStatusResponse($isActive));
        });

        $this->app->instance(AdminAccountSetStatus::class, $mock);
    }

    /**
     * @param string $isActive
     *
     * @return HaapiResponse
     */
    private function getAccountSetStatusResponse(
        string $isActive
    ): HaapiResponse {
        $response = new HaapiResponse();

        $data = $this->getSetStatusSuccessData($isActive);

        $body = Arr::get($data, 'haapi.response');
        $statusCode = Arr::get($body, 'statusCode');
        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $response->setBody($body);
        $response->setPayload(Arr::get($body, 'payload'));
        $response->setId($responseId);
        $response->setStatusCode($statusCode);
        $response->setStatusMsg($statusMsg);
        $response->setDuration(Arr::get($body, 'duration'));
        $response->setReceived(Arr::get($body, 'received'));

        return $response;
    }


    /**
     * @param bool $isActive
     *
     * @return array
     */
    private function getSetStatusSuccessData(bool $isActive): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/signin",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload" => [
                        "accountId" => "d6059179-69ba-45c2-a66f-cdf13b5125aa",
                        "status"    => $isActive ? "INACTIVE" : "ACTIVE",
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "message" => "The account status was successfully changed.",
                    ],
                ],
            ],
        ];
    }
}
