<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Daapi\DataTransferObjects\Types\UserDataPlain;
use Modules\Haapi\Actions\Admin\User\SelfDataGet;

trait SelfDataGetMock
{
    /**
     * @param $email
     */
    private function mockUserGetByTokenActionSuccess(string $email)
    {
        $mock = Mockery::mock(SelfDataGet::class, function ($mock) use ($email) {
            $mock
                ->shouldReceive('handleByToken')
                ->withAnyArgs()
                ->andReturn($this->getUserGetByTokenResponse($email));
        });

        $this->app->instance(SelfDataGet::class, $mock);
    }

    /**
     * @return void
     */
    private function mockSelfDataGetActionSuccess()
    {
        $mock = Mockery::mock(SelfDataGet::class, function ($mock) {
            $mock
                ->shouldReceive('handle')
                ->withAnyArgs()
                ->andReturn($this->getSelfDataGetResponse());
        });

        $this->app->instance(SelfDataGet::class, $mock);
    }

    /**
     * @param $email
     *
     * @return UserDataPlain
     */
    private function getUserGetByTokenResponse(string $email): UserDataPlain
    {
        $data = $this->getUserGetSuccessDataWithEmail($email);

        return new UserDataPlain(Arr::get($data, 'haapi.response.payload', []));
    }

    /**
     * @return UserDataPlain
     */
    private function getSelfDataGetResponse(): UserDataPlain
    {
        $data = $this->getSelfDataGetSuccessData();

        return new UserDataPlain(
            Arr::get($data, 'haapi.response.payload', []),
        );
    }

    /**
     * @return array
     */
    private function getSelfDataGetSuccessData(): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/signin",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "userId" => 1,
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "email"      => 'admin@test.com',
                        "firstName"  => 'Test',
                        "lastName"   => 'Test',
                        "id"         => 'asdf-asdf-asdf-asdf',
                        "userStatus" => "PENDING MANUAL REVIEW",
                        "userType"   => "NORMAL",
                        "account"    => [
                            "createdAt"      => date('Y-m-d h:i:s', time()),
                            "updatedAt"      => date('Y-m-d h:i:s', time()),
                            "accountType"    => "ADVERTISER",
                            "companyName"    => 'test',
                            "phoneNumber"    => "1235670000",
                            "id"             => "asdf-asdf-1234-qwer-5678",
                            "companyAddress" => [
                                "line1"   => "0000 Broadway",
                                "line2"   => null,
                                "city"    => "Santa Monica",
                                "state"   => "CA",
                                "country" => "United States",
                                "zipcode" => "99999",
                            ],
                            "accountStatus"  => "ACTIVE",
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $email
     *
     * @return array
     */
    private function getUserGetSuccessDataWithEmail(string $email): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/signin",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "userId" => 1,
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "email"      => $email,
                        "firstName"  => 'Test',
                        "lastName"   => 'Test',
                        "id"         => self::EXT_ID,
                        "userStatus" => "PENDING MANUAL REVIEW",
                        "userType"   => "NORMAL",
                        "account"    => [
                            "createdAt"      => date('Y-m-d h:i:s', time()),
                            "updatedAt"      => date('Y-m-d h:i:s', time()),
                            "accountType"    => "ADVERTISER",
                            "companyName"    => 'test',
                            "phoneNumber"    => "1235670000",
                            "id"             => "asdf-asdf-1234-qwer-5678",
                            "companyAddress" => [
                                "line1"   => "0000 Broadway",
                                "line2"   => null,
                                "city"    => "Santa Monica",
                                "state"   => "CA",
                                "country" => "United States",
                                "zipcode" => "99999",
                            ],
                            "accountStatus"  => "ACTIVE",
                        ],
                    ],
                ],
            ],
        ];
    }
}
