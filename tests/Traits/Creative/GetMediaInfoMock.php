<?php

namespace Tests\Traits\Creative;

use Mockery;
use Modules\Creative\Actions\ParseCreativeAction;

trait GetMediaInfoMock
{
    /**
     * @param array $response
     *
     * @return void
     */
    private function mockParseCreativeActionSuccess(array $response): void
    {
        $mock = Mockery::mock(ParseCreativeAction::class, function ($mock) use ($response) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($response);
        });

        $this->app->instance(ParseCreativeAction::class, $mock);
    }
}
