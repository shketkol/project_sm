<?php

namespace Tests\Haapi;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Http\Response;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Tests\CreatesApplication;
use Tests\Traits\Haapi\CreateHaapiUser;

abstract class HaapiTestCase extends TestCase
{
    use CreatesApplication, DatabaseTransactions, CreateHaapiUser, CreateUser;

    /*** Advertiser data */
    protected const ADV_EMAIL      = 'hl.integration.testing+1@gmail.com';
    protected const ADV_PASSWORD   = 'MyAwesomePass123!';
    protected const ADV_ID         = '4f7f8e58-5a2b-4b9d-8900-bcad42c32c6e';
    protected const ADV_ACCOUNT_ID = 'eb5d30e8-1091-46bf-abef-16382a1bce06';

    /*** Campaign & Creative data for main test cases. Belongs to hl.integration.testing+1@gmail.com */
    protected const CAMPAIGN_ID = '67b7fe56-730a-42a2-907b-522d81ace375';
    protected const CREATIVE_ID = 'aa34fbde-c506-48a8-ad63-e49daf6c4651';
    protected const ORDER_ID    = '248001';

    /*** Campaign & Creative data for statuses test cases. Belongs to hl.integration.testing+1@gmail.com */
    protected const STATUS_CAMPAIGN_ID = '8d0350f3-15e3-42f8-9a85-192fa46a2b9d'; //TODO: Check

    /*** Campaign & Creative data for update creative test cases. Belongs to hl.integration.testing+1@gmail.com */
    protected const UPDATE_CAMPAIGN_ID = 'c342f558-a5ba-4e60-841a-2201ab0da065';
    protected const UPDATE_CREATIVE_ID = 'aa34fbde-c506-48a8-ad63-e49daf6c4651';

    /*** Advertiser used for statuses testing data */
    protected const ADV_STATUS_EMAIL      = 'hl.integration.testing+3@gmail.com';
    protected const ADV_STATUS_PASSWORD   = 'MyAwesomePass321!';
    protected const ADV_STATUS_ACCOUNT_ID = '8123c21b-1a7d-47f1-bf49-db3bf6946000';

    /*** Admin data */
    protected const ADM_EMAIL    = 'dev.team@danads.se';
    protected const ADM_PASSWORD = 'GBeiUbDZnh0Cbi!';
    /**
     *  Save last response
     *
     * @var Response|null A Response instance
     */
    static public $lastResponse;
    /**
     * User for haapi module testing
     *
     * @var User
     */
    protected $userHaapi;
    /**
     * User for haapi module status testing
     *
     * @var User
     */
    protected $userStatusHaapi;
    /**
     * Admin for haapi module testing
     *
     * @var User
     */
    protected $adminHaapi;

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function setUp(): void
    {
        parent::setUp();

        /*** Create Test Data */
        $this->createMainAdvertiser();
        $this->creatStatusAdvertiser();
        $this->loginAdmin();
    }

    /**
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    private function createMainAdvertiser(): void
    {
        $this->userHaapi = $this->createHaapiTestUser(
            self::ADV_EMAIL,
            self::ADV_PASSWORD,
            [
                'external_id'         => self::ADV_ID,
                'account_external_id' => self::ADV_ACCOUNT_ID,
            ]
        );
    }

    /**
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    private function creatStatusAdvertiser(): void
    {
        $this->userStatusHaapi = $this->createHaapiTestUser(
            self::ADV_STATUS_EMAIL,
            self::ADV_STATUS_PASSWORD,
            [
                'account_external_id' => self::ADV_STATUS_ACCOUNT_ID,
            ],
            false
        );
    }

    /**
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function loginAdmin(): void
    {
        $this->adminHaapi = $this->createTechnicalAdmin();

        $this->login(
            self::ADM_EMAIL,
            self::ADM_PASSWORD,
            $this->adminHaapi->id
        );
    }
}
