<?php

namespace Tests\Haapi\Account;

use Modules\Haapi\Actions\Account\Contracts\AccountUpdateProfile;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\User\DataTransferObjects\UserCompanyUpdateData;
use Modules\User\Models\Traits\CreateUser;
use Tests\Haapi\HaapiTestCase;

/**
 * Class AccountUpdateTest
 *
 * @package Modules\Haapi\tests\Integration\Account
 */
class AccountUpdateTest extends HaapiTestCase
{
    use CreateUser;

    /**
     * Update account profile
     *
     * @var AccountUpdateProfile
     */
    protected $actionUpdate;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->actionUpdate = app(AccountUpdateProfile::class);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testUpdateSuccess(): void
    {
        $data = $this->prepareData(self::ADV_ACCOUNT_ID);
        $response = $this->actionUpdate->handle($data, $this->userHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testUpdateUnauthorizedFail(): void
    {
        $this->expectException(UnauthorizedException::class);
        $data = $this->prepareData(self::ADV_ACCOUNT_ID);
        $this->actionUpdate->handle($data, 0);
    }

    /**
     * @param string $accountId
     *
     * @return UserCompanyUpdateData
     */
    private function prepareData(string $accountId): UserCompanyUpdateData
    {
        $data = [
            'companyName' => 'Wildfire Carryout',
            'phone_number' => '+1 (630) 586-9039',
            'line1'       => '232 Oakbrook Center Mall',
            'line2'       => 'testing',
            'city'        => 'Oak Brook',
            'state'       => 'IL',
            'country'     => 'US',
            'zipcode'     => '60523',
        ];
        $companyUpdateData = new UserCompanyUpdateData($accountId, $data);

        return $companyUpdateData;
    }
}
