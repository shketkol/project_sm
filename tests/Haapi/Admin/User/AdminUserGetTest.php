<?php

namespace Tests\Haapi\Admin\User;

use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Haapi\Actions\Admin\User\Contracts\AdminUserGet;
use Tests\Haapi\HaapiTestCase;

class AdminUserGetTest extends HaapiTestCase
{
    /**
     * @var AdminUserGet
     */
    protected $action;

    /**
     * Set up environment
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(AdminUserGet::class);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testSuccess()
    {
        $response = $this->action->handle($this->userHaapi, $this->adminHaapi->id);
        $this->assertInstanceOf(UserData::class, $response);
    }
}
