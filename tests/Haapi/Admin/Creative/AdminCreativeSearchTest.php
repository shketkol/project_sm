<?php

namespace Tests\Haapi\Admin\Creative;

use Modules\Haapi\Actions\Admin\Creative\Contracts\AdminCreativeSearch;
use Modules\Haapi\DataTransferObjects\Creative\AdminCreativeSearchParam;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Tests\Haapi\HaapiTestCase;

class AdminCreativeSearchTest extends HaapiTestCase
{
    /**
     * @var AdminCreativeSearch
     */
    protected $action;

    /**
     * Account id in Hulu
     *
     * @var string, UUID v4
     */
    protected $accountId;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->action = app(AdminCreativeSearch::class);
        $this->accountId = self::ADV_ACCOUNT_ID;
    }

    /**
     * Test success response
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAdminAccountSearchSuccess()
    {
        $response = $this->action->handle(
            new AdminCreativeSearchParam(['accountId' => $this->accountId]),
            $this->adminHaapi->id
        );
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test use not authorized response
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAdminAccountSearchNotAuthorized()
    {
        $this->expectException(UnauthorizedException::class);
        $response = $this->action->handle(new AdminCreativeSearchParam(['accountId' => $this->accountId]), 0);
        $this->assertEquals('401', $response->getStatusCode());
    }
}
