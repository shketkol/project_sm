<?php

namespace Tests\Haapi\Admin\Account;

use Modules\Haapi\Actions\Admin\Account\Contracts\AdminAccountSearch;
use Modules\Haapi\DataTransferObjects\Account\AdminAccountSearchParam;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Tests\Haapi\HaapiTestCase;

class AdminAccountSearchTest extends HaapiTestCase
{
    /**
     * @var AdminAccountSearch
     */
    protected $action;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->action = app(AdminAccountSearch::class);
    }

    /**
     * Test success response
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAdminAccountSearchSuccess()
    {
        $response = $this->action->handle(new AdminAccountSearchParam([]), $this->adminHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test use not authorized response
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAdminAccountSearchNotAuthorized()
    {
        $this->expectException(UnauthorizedException::class);
        $this->action->handle(new AdminAccountSearchParam([]), 0);
    }
}
