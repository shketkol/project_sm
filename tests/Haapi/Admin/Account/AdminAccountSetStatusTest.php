<?php

namespace Tests\Haapi\Admin\Account;

use Modules\Haapi\Actions\Admin\Account\Contracts\AdminAccountSetStatus;
use Tests\Haapi\HaapiTestCase;

class AdminAccountSetStatusTest extends HaapiTestCase
{
    /**
     * @var AdminAccountSetStatus
     */
    protected $action;

    /**
     * Setup test environment
     */
    protected function setUp(): void
    {
        self::markTestSkipped('Cannot be implemented without full rights admin');
        parent::setUp();
        $this->action = app(AdminAccountSetStatus::class);
    }

    /**
     * Test account status update to INACTIVE successfully
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAdminDeactivateSuccess()
    {
        $response = $this->action->handle(self::ADV_STATUS_ACCOUNT_ID, false, $this->adminHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test account status update to ACTIVE successfully
     * @depends testAdminDeactivateSuccess
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAdminActivateSuccess()
    {
        $response = $this->action->handle(self::ADV_STATUS_ACCOUNT_ID, true, $this->userStatusHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
    }
}
