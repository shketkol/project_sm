<?php

namespace Tests\Haapi\Invoice;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\Payment\Contracts\OrderList;
use Modules\Haapi\DataTransferObjects\Payment\OrderListParams;
use Tests\Haapi\HaapiTestCase;

class OrderListTest extends HaapiTestCase
{
    /**
     * @var OrderList
     */
    protected $action;

    /**
     * Setup environment
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(OrderList::class);
    }

    /**
     * Test orders get successfully
     *
     * @throws GuzzleException
     */
    public function testSuccess()
    {
        $response = $this->action->handle($this->getOrderParams(), $this->userHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
        $this->assertArrayHasKey('orders', $response->getPayload());
    }

    /**
     * @return OrderListParams
     */
    private function getOrderParams(): OrderListParams
    {
        return new OrderListParams([
            'accountId' => self::ADV_ACCOUNT_ID
        ]);
    }
}
