<?php

namespace Tests\Haapi\Invoice;

use GuzzleHttp\Exception\GuzzleException;
use Modules\Haapi\Actions\Payment\Contracts\OrderGet;
use Modules\Haapi\DataTransferObjects\Payment\OrderGetParams;
use Tests\Haapi\HaapiTestCase;

class OrderGetTest extends HaapiTestCase
{
    /**
     * @var OrderGet
     */
    protected $action;

    /**
     * Setup environment
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(OrderGet::class);
    }

    /**
     * Test OrderGet successfully
     *
     * @throws GuzzleException
     */
    public function testOrderGetSuccess()
    {
        $response = $this->action->handle($this->getOrderGetParams(), $this->userHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * @return OrderGetParams
     */
    private function getOrderGetParams(): OrderGetParams
    {
        return new OrderGetParams([
            'accountId' => self::ADV_ACCOUNT_ID,
            'orderId' => self::ORDER_ID
        ]);
    }
}
