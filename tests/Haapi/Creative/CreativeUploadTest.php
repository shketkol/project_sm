<?php

namespace Tests\Haapi\Creative;

use Illuminate\Support\Arr;
use Modules\Haapi\Actions\Creative\Contracts\CreativeUpload;
use Modules\Haapi\DataTransferObjects\Creative\CreativeUploadParam;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Tests\Haapi\HaapiTestCase;

class CreativeUploadTest extends HaapiTestCase
{
    /**
     * @var CreativeUpload
     */
    protected $creativeUpload;

    /**
     * @var CreativeUploadParam
     */
    protected $creativeUploadParam;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->creativeUpload = app(CreativeUpload::class);
    }

    /**
     * Test creative success
     */
    public function testCreativeUploadSuccess()
    {
        $this->setParams();
        $response = $this->creativeUpload->handle($this->creativeUploadParam, $this->userHaapi->id);

        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test creative missing required field
     */
    public function testCreativeUploadMissingField()
    {
        $this->expectException(InvalidRequestException::class);
        $this->setParams([
            'fileName' => '',
            'sourceUrl' => '',
            'contentLength' => 0
        ]);
        $this->creativeUpload->handle($this->creativeUploadParam, $this->userHaapi->id);
    }

    /**
     * Test creative user not authorized
     */
    public function testCreativeUploadNotAuthorized()
    {
        $this->markTestSkipped('Skipped due to HD-364');
        $this->expectException(UnauthorizedException::class);

        $this->setParams();
        $this->creativeUpload->handle($this->creativeUploadParam, 0);
    }

    /**
     * Test creative account or campaign id invalid
     */
    public function testCreativeUploadIdInvalid()
    {
        $this->expectException(InvalidRequestException::class);

        $this->setParams([
            'campaignId' => ''
        ]);
        $this->creativeUpload->handle($this->creativeUploadParam, $this->userHaapi->id);
    }

    /**
     * @param array $params
     */
    protected function setParams(array $params = [])
    {
        $accountId = Arr::get($params, 'accountId', self::ADV_ACCOUNT_ID);
        $campaignId = Arr::get($params, 'campaignId', self::STATUS_CAMPAIGN_ID);
        $fileName = Arr::get($params, 'fileName', 'test.jpg');
        $defaultSourceUrl =
            'https://s3' .
            config('filesystems.disks.s3.region') .
            '.amazonaws.com/' .
            config('filesystems.disks.s3.bucket') .
            '/' . $fileName;
        $sourceUrl = Arr::get($params, 'sourceUrl', $defaultSourceUrl);
        $contentLength = Arr::get($params, 'contentLength', 100);
        $this->creativeUploadParam = new CreativeUploadParam(compact([
            'accountId',
            'campaignId',
            'fileName',
            'sourceUrl',
            'contentLength'
        ]));
    }
}
