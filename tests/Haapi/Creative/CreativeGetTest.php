<?php

namespace Tests\Haapi\Creative;

use Modules\Daapi\DataTransferObjects\Types\CreativeData;
use Modules\Haapi\Actions\Creative\Contracts\CreativeGet;
use Tests\Haapi\HaapiTestCase;

class CreativeGetTest extends HaapiTestCase
{
    /**
     * @var CreativeGet
     */
    protected $action;

    /**
     * @var string
     */
    protected $creativeId;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(CreativeGet::class);
        $this->creativeId = self::CREATIVE_ID;
    }

    /**
     * Test successful get creative
     */
    public function testCreativeGetSuccess()
    {
        $creativeData = $this->action->handle($this->creativeId, $this->userHaapi->id);

        $this->assertInstanceOf(CreativeData::class, $creativeData);
        $this->assertEquals($creativeData->id, self::CREATIVE_ID);
    }
}
