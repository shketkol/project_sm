<?php

namespace Tests\Haapi\Creative;

use Modules\Haapi\Actions\Creative\Contracts\CreativeUpdate;
use Modules\Haapi\DataTransferObjects\Creative\CreativeUpdateParam;
use Modules\Haapi\Exceptions\ObjectNotFoundException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Tests\Haapi\HaapiTestCase;

class CreativeUpdateTest extends HaapiTestCase
{
    /**
     * @var CreativeUpdate
     */
    protected $creativeUpdate;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->creativeUpdate  = app(CreativeUpdate::class);
    }

    /**
     * Test creative success
     */
    public function testCreativeUploadSuccess()
    {
        $creativeUpdateParam = new CreativeUpdateParam([
            'campaignId' => self::UPDATE_CAMPAIGN_ID,
            'creativeId' => self::UPDATE_CREATIVE_ID
        ]);
        $response = $this->creativeUpdate->handle($creativeUpdateParam, $this->userHaapi->id);

        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test creative detach success
     * @depends testCreativeUploadSuccess
     */
    public function testCreativeDetachSuccess()
    {
        $creativeUpdateParam = new CreativeUpdateParam([
            'campaignId' => self::UPDATE_CAMPAIGN_ID,
            'creativeId' => self::UPDATE_CREATIVE_ID
        ]);

        $response = $this->creativeUpdate->handle($creativeUpdateParam, $this->userHaapi->id);

        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test creative user not authorized
     */
    public function testCreativeUpdateUploadNotAuthorized()
    {
        $creativeUpdateParam = new CreativeUpdateParam([
            'campaignId' => self::UPDATE_CAMPAIGN_ID,
            'creativeId' => self::UPDATE_CREATIVE_ID
        ]);
        $this->expectException(UnauthorizedException::class);
        $this->creativeUpdate->handle($creativeUpdateParam, 0);
    }

    /**
     * Test creative account or campaign id invalid
     */
    public function testCreativeUpdateUploadIdInvalid()
    {
        $this->expectException(ObjectNotFoundException::class);
        $creativeUpdateParam = new CreativeUpdateParam([
            'campaignId' => '07e9f9fa-cfeb-11e9-bb65-2a2ae2dbcce4',
            'creativeId' => 'cc3d3e3d-8972-47c5-a68c-c58ee6609f65'
        ]);
        $this->creativeUpdate->handle($creativeUpdateParam, $this->userHaapi->id);
    }
}
