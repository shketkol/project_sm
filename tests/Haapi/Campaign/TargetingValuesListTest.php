<?php

namespace Tests\Haapi\Campaign;

use Modules\Haapi\Actions\Campaign\Contracts\TargetingTypeList;
use Modules\Haapi\Actions\Campaign\TargetingValuesList;
use Tests\Haapi\HaapiTestCase;

class TargetingValuesListTest extends HaapiTestCase
{
    /**
     * @var TargetingValuesList
     */
    protected $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(TargetingValuesList::class);
    }

    public function testListTargetingValuesSuccess()
    {
        $response = $this->action->handle('0f1336b2-7bfc-11e9-b52b-9b8ffb3c3b74', 200, 0);
        $payload = $response->getPayload();
        $this->assertEquals('0', $response->getStatusCode());
        $this->assertArrayHasKey('limit', $payload);
        $this->assertArrayHasKey('next', $payload);
        $this->assertArrayHasKey('targetValues', $payload);
    }
}
