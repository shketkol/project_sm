<?php

namespace Tests\Haapi\Campaign;

use Modules\Haapi\Actions\Campaign\Contracts\CampaignImpressionsDetail;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsDetailParams;
use Tests\Haapi\HaapiTestCase;

class CampaignImpressionsDetailTest extends HaapiTestCase
{
    /**
     * @var CampaignImpressionsDetail
     */
    protected $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(CampaignImpressionsDetail::class);
    }

    /**
     * Test get impressions details successfully
     */
    public function testGetCampaignImpressionsDetailSuccess()
    {
        $response = $this->action->handle($this->getImpressionDetailParams(), $this->userHaapi->id);
        $this->assertIsArray($response);
        $this->assertArrayHasKey('campaigns', $response);
    }

    /**
     * @return ImpressionsDetailParams
     */
    private function getImpressionDetailParams(): ImpressionsDetailParams
    {
        return new ImpressionsDetailParams([
            'advertiserId' => self::ADV_ACCOUNT_ID,
            'campaignIds' => [],
            'rollUpType' => 'LIFETIME'
        ]);
    }
}
