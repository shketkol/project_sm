<?php

namespace Tests\Haapi\Campaign;

use Modules\Haapi\Actions\Campaign\Contracts\CampaignImpressions;
use Tests\Haapi\HaapiTestCase;

class CampaignImpressionsTest extends HaapiTestCase
{
    /**
     * @var CampaignImpressions
     */
    protected $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(CampaignImpressions::class);
    }

    /**
     * Test get campaign impressions success
     */
    public function testGetCampaignImpressionsSuccess()
    {
        $response = $this->action->handle($this->getCampaignIds(), $this->userHaapi->id);
        $this->assertIsInt($response);
    }

    /**
     * @return array
     */
    private function getCampaignIds(): array
    {
        return [
            self::CAMPAIGN_ID
        ];
    }
}
