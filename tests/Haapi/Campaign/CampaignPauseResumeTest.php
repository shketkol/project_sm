<?php

namespace Tests\Haapi\Campaign;

use Modules\Haapi\Actions\Campaign\CampaignPause;
use Modules\Haapi\Actions\Campaign\CampaignResume;
use Modules\Haapi\Exceptions\ForbiddenException;
use Tests\Haapi\HaapiTestCase;

class CampaignPauseResumeTest extends HaapiTestCase
{
    /**
     * @var CampaignPause
     */
    protected $pauseAction;

    /**
     * @var CampaignResume
     */
    protected $resumeAction;

    protected function setUp(): void
    {
        parent::setUp();
        $this->pauseAction = app(CampaignPause::class);
        $this->resumeAction = app(CampaignResume::class);
    }

    /**
     * Test campaign pause success
     */
    public function testCampaignPauseSuccess()
    {
        $this->markTestSkipped('Unskip when campaign will be ready for testing');
        $response = $this->pauseAction->handle(self::STATUS_CAMPAIGN_ID, $this->userHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test campaign resume success
     * @depends testCampaignPauseSuccess
     */
    public function testCampaignResumeSuccess()
    {
        $this->markTestSkipped('Unskip when campaign will be ready for testing');
        $response = $this->resumeAction->handle(self::STATUS_CAMPAIGN_ID, $this->userHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test campaign resume non-existent UUID fail
     */
    public function testCampaignPauseNonExistentIdFail()
    {
        $this->expectException(ForbiddenException::class);
        $this->pauseAction->handle('200bbbab-d3b7-3609-92e5-217027e5ae76', $this->userHaapi->id);
    }

    /**
     * Test campaign resume non-existent UUID fail
     */
    public function testCampaignResumeNonExistentIdFail()
    {
        $this->expectException(ForbiddenException::class);
        $this->resumeAction->handle('200bbbab-d3b7-3609-92e5-217027e5ae76', $this->userHaapi->id);
    }
}
