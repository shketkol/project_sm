<?php

namespace Tests\Unit\Services\ValidationRulesService;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Validation\Factory;
use Tests\TestCase;

class ValidationRulesTest extends TestCase
{
    /**
     * Test password for required symbols.
     *
     * @dataProvider passwordsDataProvider
     *
     * @param array $data
     * @param bool  $expectedResult
     *
     * @return void
     */
    public function testPassword(array $data, bool $expectedResult): void
    {
        /** @var ValidationRules $validationRules */
        $validationRules = app(ValidationRules::class);
        $rules = $validationRules->only(
        'user.password',
            ['required', 'string', 'min', 'confirmed', 'regex', 'max']
        );

        /** @var Factory $validator */
        $validator = app('validator');
        $validator = $validator->make($data, ['password' => $rules]);

        $this->assertEquals($expectedResult, $validator->passes());
    }

    /**
     * @return \Generator
     */
    public function passwordsDataProvider(): \Generator
    {
        yield [
            [
                'password'              => 'passWORD123!@#',
                'password_confirmation' => 'passWORD123!@#',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt-hZwn-Tmm-S23',
                'password_confirmation' => 'pt-hZwn-Tmm-S23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt~hZwmS23',
                'password_confirmation' => 'pt~hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt^hZwmS23',
                'password_confirmation' => 'pt^hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt*hZwmS23',
                'password_confirmation' => 'pt*hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt hZwmS23',
                'password_confirmation' => 'pt hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt"hZwmS23',
                'password_confirmation' => 'pt"hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => "pt'hZwmS23",
                'password_confirmation' => "pt'hZwmS23",
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt+hZwmS23',
                'password_confirmation' => 'pt+hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt,hZwmS23',
                'password_confirmation' => 'pt,hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt.hZwmS23',
                'password_confirmation' => 'pt.hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt/hZwmS23',
                'password_confirmation' => 'pt/hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt:hZwmS23',
                'password_confirmation' => 'pt:hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt;hZwmS23',
                'password_confirmation' => 'pt;hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt<hZwmS23',
                'password_confirmation' => 'pt<hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt=hZwmS23',
                'password_confirmation' => 'pt=hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt>hZwmS23',
                'password_confirmation' => 'pt>hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt?hZwmS23',
                'password_confirmation' => 'pt?hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt[hZwmS23',
                'password_confirmation' => 'pt[hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt\hZwmS23',
                'password_confirmation' => 'pt\hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt]hZwmS23',
                'password_confirmation' => 'pt]hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt^hZwmS23',
                'password_confirmation' => 'pt^hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt_hZwmS23',
                'password_confirmation' => 'pt_hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt`hZwmS23',
                'password_confirmation' => 'pt`hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt{hZwmS23',
                'password_confirmation' => 'pt{hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt|hZwmS23',
                'password_confirmation' => 'pt|hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt}hZwmS23',
                'password_confirmation' => 'pt}hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'pt~hZwmS23',
                'password_confirmation' => 'pt~hZwmS23',
            ],
            true,
        ];

        yield [
            [
                'password'              => 'passWORD123!@#',
                'password_confirmation' => 'passWORD123!@#_another',
            ],
            false,
        ];

        yield [
            [
                'password'              => 'password',
                'password_confirmation' => 'password',
            ],
            false,
        ];

        yield [
            [
                'password'              => '123456789',
                'password_confirmation' => '123456789',
            ],
            false,
        ];

        yield [
            [
                'password'              => '!@#$%^*(',
                'password_confirmation' => '!@#$%^*(',
            ],
            false,
        ];

        yield [
            [
                'password'              => 'PASSWORD',
                'password_confirmation' => 'PASSWORD',
            ],
            false,
        ];

        yield [
            [
                'password'              => 'passWORD',
                'password_confirmation' => 'passWORD',
            ],
            false,
        ];

        yield [
            [
                'password'              => 'PASSword',
                'password_confirmation' => 'PASSword',
            ],
            false,
        ];

        yield [
            [
                'password'              => '1231!@#$%',
                'password_confirmation' => '1231!@#$%',
            ],
            false,
        ];

        yield [
            [
                'password'              => '1pA$',
                'password_confirmation' => '1pA$',
            ],
            false,
        ];

        yield [
            [
                'password'              => 'Password@',
                'password_confirmation' => 'Password@',
            ],
            false,
        ];
    }

    /**
     * Test phone for required symbols.
     *
     * @dataProvider phonesDataProvider
     *
     * @param array $data
     * @param bool  $expectedResult
     *
     * @return void
     */
    public function testPhone(array $data, bool $expectedResult): void
    {
        /** @var ValidationRules $validationRules */
        $validationRules = app(ValidationRules::class);
        $rules = $validationRules->get('user.phone');
        $formattedRules = ['phone_number' => $validationRules->format($rules)];

        /** @var Factory $validator */
        $validator = app('validator');
        $validator = $validator->make($data, $formattedRules);

        $this->assertEquals($expectedResult, $validator->passes());
    }

    /**
     * @return \Generator
     */
    public function phonesDataProvider(): \Generator
    {
        yield [
            [
                'phone_number' => '0631234567',
            ],
            false,
        ];

        yield [
            [
                'phone_number' => '(800) 123-4567',
            ],
            false,
        ];

        yield [
            [
                'phone_number' => '800-123-4567',
            ],
            false,
        ];

        yield [
            [
                'phone_number' => '(800) 12-34-567',
            ],
            false,
        ];

        yield [
            [
                'phone_number' => '+1 (800) 1234-567',
            ],
            true,
        ];

        yield [
            [
                'phone_number' => '+1-800-CHARLIE',
            ],
            false,
        ];

        yield [
            [
                'phone_number' => '+18001234567',
            ],
            false,
        ];
    }
}
